<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.subscription.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<script language="JavaScript" type="text/JavaScript" src="prototype.js"></script>
<%
	if (user == null)
	{
		response.sendRedirect("Login.jsp");
		return;
	}
	String postingId = (String)	session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString());
	List<File> resumeFiles = (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
	List<Double> scores = (List<Double>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
	if (resumeFiles == null)
	{
		System.out.println("No results found");
		return;
	}
	Integer numleft = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
	if (numleft == null) numleft = 0;
	int totalResumes = resumeFiles.size();
	int numProcessed = totalResumes - numleft;
	int numMatched = 0;
	for (int i = 0; i < scores.size(); i++)
		if (scores.get(i) != null) numMatched++;

	if (numMatched == totalResumes)
	{
%>
	<script>window.location="ranked_resumes.jsp";</script>
<%
		return;
	}
	if (numMatched != 0) numProcessed = numMatched;
	long lastAccess = session.getLastAccessedTime();
	long idle = System.currentTimeMillis() - lastAccess;
	int maxInactive = session.getMaxInactiveInterval();
	// If remaining idle time is less than 2 hours, add 2 hours to it (give user time to come back)
	if ((maxInactive - idle/1000) < 60*60*2)
		session.setMaxInactiveInterval(maxInactive + 60*60*2);
	int inc = getInt(request.getParameter("inc"));
%>
<script>
	setTimeout ( refresh, 5*1000 );
	function refresh()
	{
		window.location="stillProcessing.jsp?inc=<%=++inc%>";
	}
</script>
<div class="middle">
<div class="maintext_dashboard">
<div class="processing">
<br/>
<br/>
<center><h1>Status for Job Posting: <%=postingId%></h1>
<br>
<h1><%=numProcessed%> Resumes out of a total of <%=totalResumes%> have been processed. Please wait ...<% for (int i=0; i < inc; i++) {%>.<% } %>
<% if (false && numProcessed > 5) {%>
Click <a href="ranked_resumes.jsp">here</a> to see the ones that have been processed
<% } %>
</h1>
<br/>
<br/>
<br/>
<br/>
</center>
</div>
</div>
</div>
<script>
			//document.getElementById("loading").style.visibility = "visible";
</script>
<%@include file="footer.jsp"%>
</body>
</html>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>