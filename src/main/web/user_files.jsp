<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.*"%>
<style>
.utable {
font-size:12px;
font-weight:normal;
padding:4px;
border-style:solid;
border-width:1px;
border-color:black;
border-collapse:collapse;
}
.utable td {
border-width:1px;
border-color:black;
border-style:solid;
padding:4px;
}
</style>
<%

	if (user == null)
	{
		response.sendRedirect("");
		return;		
	}
	String email = user.getEmail().toLowerCase();
	if (email.indexOf("gude") == -1 && email.indexOf("bandari") == -1)
	{
%>
		<script>alert("No permissions for:<%=email%>")</script>
<%
		return;
	}
	String folder = request.getParameter("folder");
	if (folder == null)
		folder = "";
	String deleteFile = request.getParameter("deleteFile");
	if (deleteFile != null)
	{
		File file = new File(deleteFile);
		if (file.exists() && !file.isDirectory())
			file.delete();
	}
	UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();
	User userToCheck = null;
	int iduser = 0;
	if (request.getParameter("iduser") != null)
	{
		iduser = Integer.parseInt(request.getParameter("iduser"));
		userToCheck = User.getUser(iduser);
	}
	
	if (userToCheck == null)
	{
		userToCheck = user;
		iduser = user.getId();
	}
		
	String path = "uploads" + File.separator + userToCheck.getLogin() + folder;
	File dir = PlatformProperties.getInstance().getResourceFile(path);

	class FileFilter implements FilenameFilter {
		public boolean accept(File dir, String name) {
			boolean regResult = name.endsWith(".zip");
			return regResult;
		}
	}

	String[] ls;
	FilenameFilter filter = new FileFilter();
	//ls = dir.list(filter);
	ls = dir.list();
%>
<script>
	function deleteuser(name, id)
	{
		if (!confirm("Are you sure you want to delete user " + name))
			return -1;
		window.location = "UserManager?delete=&iduser=" + id;
	}
	function openFolder(folder)
	{
		window.location = "user_files.jsp?iduser=<%=iduser%>" + "&folder=" + escape("<%=folder%>/" + folder);
	}
	function deleteFile(fileName)
	{
		window.location = "user_files.jsp?iduser=<%=iduser%>" + "&folder=" + escape("<%=folder%>") + "&deleteFile=" + escape(fileName);
	}
</script>
<div class=central>
<form method="post" action="UserManager">
<br>
<table border="1" class=utable>
	<tr>
		<td bgcolor=lightgrey><b>Name:</b></td>
		<td><%=userToCheck.getName()%></td>
	</tr>
	<tr>
		<td bgcolor=lightgrey><b>Email:</b></td>
		<td><a href="user_files.jsp?iduser=<%=iduser%>"><%=userToCheck.getEmail()%></a></td>
	</tr>
</table>

<input type="hidden" size="10" name="iduser"
	value="<%=userToCheck.getId()%>">

</form>
<hr>
<table border=1 class=utable>
<tr bgcolor=lightgrey>
<td>&nbsp;</td>
<td align=center><b>File Name</b></a></td>
<td align=center><b>Date</b></a></td>
</tr>
<%
	long[] tstamps = new long[ls.length];
	for (int i = 0; i < ls.length; i++)
	{
		File userFile = new File(dir.getPath()+"/"+ls[i]);
		String size = String.valueOf(userFile.length());
		tstamps[i] = userFile.lastModified();
	}
	for (int i = 0; i < ls.length; i++)
	{
		for (int j = i; j < ls.length; j++)
		{
			if (tstamps[i] < tstamps[j])
			{
				long temptime = tstamps[i];
				String tempname = ls[i];
				tstamps[i] = tstamps[j];
				ls[i] = ls[j];
				ls[j] = tempname;
				tstamps[j] = temptime;
			}
		}
	}
	for (int i = 0; i < ls.length; i++)
	{
%>
<tr>
<td><%=i+1%></td>
<% if (ls[i].indexOf(".") != -1 && !ls[i].endsWith(".dir")) { %>
<td nowrap><a href="downloadFile.jsp?fileName=<%=dir.getPath() + "/" + ls[i]%>&download=true" target=_blank><%=ls[i]%></a>&nbsp;<img src=delete.gif onclick="deleteFile('<%=dir.getPath().replace('\\','/') + "/" + ls[i]%>')"></td>
<% } else { %>
<td nowrap><a href="javascript:openFolder('<%=ls[i]%>')"><%=ls[i]%></a></td>
<% } %>
<td nowrap><%=checkNull(new Date(tstamps[i]), "")%></td>
</tr>
<%
	}
%>
</table>
</div>
<%@include file="footer.jsp"%>
  
</body>
</html>
<%!
	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
	String checkNull(Object value, String defval)
	{
		if (value == null)
			return defval;
		else if (value instanceof Date)
			return dateformat.format(value);
		return value.toString();
	}
%>