var log = function (mssg, ttl) {
	document.getElementById('logbox').innerHTML = (typeof mssg === 'string' && mssg.length > 0) ? mssg : '&nbsp;';
	if (typeof ttl === 'number') setTimeout(log, ttl * 1000);
};
var helperActions = function (elem, act) {
	var thisId = (typeof elem === 'object') ? elem.id.split('_')[1] : elem;
	var clearId = document.getElementById('clear_'+thisId);
	if (clearId != null && act === 'show')
	{
		clearId.style.display = 'inline';
	}
	if (clearId != null && act === 'hide')
	{
		clearId.style.display = 'none';
	}
	var targetId = (document.getElementById('required_'+thisId).checked) ? 'clear' : 'clear';
	if (document.getElementById('rating_'+thisId) != null)
		targetId = (document.getElementById('required_'+thisId).checked || document.getElementById('rating_'+thisId).value != 0) ? 'clear' : 'clear';
	if (document.getElementById(targetId+thisId) != null)
		document.getElementById(targetId + thisId).style.display = (act === 'show') ? '' : 'none';
	if (act === 'hide' && document.getElementById(targetId+thisId) != null) document.getElementById((targetId==='clear'? 'clear':'clear') + thisId).style.display = 'none';
};
var starRater = function (elem, act) {
	//if (act != null)
	//	log("starRater:" + elem.id + " act:" + act, 3);
	var targetNode, targetId, i, num;
	targetId = (typeof elem === 'object') ? elem.id.split('_') : elem;
	if (act === 'on') {
		for (i=1; i<6; i++) document.getElementById(targetId[0]+'_'+i).className = i<=targetId[1] ? 'on' : '';
		document.getElementById('actions'+targetId[0]).style.display = 'none';
		targetNode = document.getElementById('status'+targetId[0]);
		targetNode.innerHTML = elem.getAttribute('alt');
		targetNode.style.display = '';

	} else if (act === 'off') {
		//log("off:" + target[1], 3);
		for (i=1; i<=targetId[1]; i++) document.getElementById(targetId[0]+'_'+i).className = '';
		targetNode = document.getElementById('status'+targetId[0]);
		targetNode.style.display = 'none';
		// targetNode.innerHTML = '';
		document.getElementById('actions'+targetId[0]).style.display = '';
	} else if (act === 'save') {
		if (targetId[1] == 6) {
			document.getElementById('required_'+targetId[0]).value = document.getElementById('required_'+targetId[0]).checked ? 'false' : 'true';
			document.getElementById('rating_'+targetId[0]).value = 0;
		} else {
			document.getElementById('rating_'+targetId[0]).value = document.getElementById('rating_'+targetId[0]).value == targetId[1] ? 0 : targetId[1];
			//document.getElementById('required_'+targetId[0]).value = 'false';
			document.getElementById('required_'+targetId[0]).checked = false;
		}
		var clearId = document.getElementById('clear'+targetId[0]);
		if (clearId != null) clearId.style.display = 'inline';
	} else if (typeof targetId === 'number') {
		num = parseInt(document.getElementById('rating_'+targetId).value, 10);
		for (i=1; i<6; i++) document.getElementById(targetId+'_'+i).className = num >= i ? 'on' : '';
	}
};
var clearStarRating = function (id) {
	for (i=1; i<6; i++) document.getElementById(id+'_'+i).className = '';
	document.getElementById('rating_'+id).value = 0;
	document.getElementById('clear' + id).style.display = 'none';
	var targetNode = document.getElementById('status'+id);
	targetNode.innerHTML = "";
	//document.getElementById('remove' + id).style.display = '';
	//document.getElementById('required_'+id).value = 'false';
	//document.getElementById("reqimg_" + id).src = 'star_off.gif';
}
var removeKeyphrase = function (id) {
	document.getElementById('kwd_'+id).parentNode.removeChild(document.getElementById('kwd_'+id));
}
