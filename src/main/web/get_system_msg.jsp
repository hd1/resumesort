<%@ page import="java.util.*"%><%@ page import="java.io.*"%><%@ page import="com.textonomics.*"%><%@ page import="com.textonomics.web.*"%><%@ page import="com.textonomics.user.*"%><%@ page import="com.textonomics.wordnet.model.*"%><%
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (user == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		String sysmsg = ResumeCakeWalkConfigurator.systemMessage;
		String param = request.getParameter("param");
		System.out.println("" + new Date() + " -  User: " + user.getEmail() + " : System message:" + sysmsg + " Param:" + param);
		if (sysmsg != null && sysmsg.trim().length() > 0)
			out.print("<font size=+2 color=red><b><blink>" + sysmsg + "</blink></b></font>");
		return;
%>