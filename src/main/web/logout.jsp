<%@ page import="sun.misc.BASE64Decoder"%>
<%@ page import="java.net.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@include file="header.jsp"%>
<%
	String userName = user.getLogin();
	String uploadDir = (String) session.getAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString());
	if (session != null)
	{
			session.removeAttribute(SESSION_ATTRIBUTE.SUGGESTER.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.TARGET_FILE.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.REQUEST_STAMP.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.FILTER_TYPE.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.OPTIONS_LIST.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.HELP_LIST.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.EXTRAS_INDEX.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.LAST_SUGGESTION_INDEX.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.RANK_MAP.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.KEY_PHRASE_IDENTIFIER.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			session.invalidate();
	}
	String[] questions = {
		"How would you rate the speed of the ranking?",
		"How well were the keywords/phrases selected?",
		"How would you rate your overall experience when using ResumeSort.com?",
		"How likely are you to come back to ResumeSort.com?",
		"How likely are you to recommend ResumeSort.com to a friend?",
	};
%>
<script>

		function submitSurvey()
		{
			var ok = false;
			var all = true;
			var rating = document.getElementsByName("rating");
			for (var i = 0; i < rating.length; i++)
			{
				if (rating[i].value != '0')
				{
					//alert("q:" + i + " val:"+ rating[i].value);
					ok = true;
				}
				else
				{
					all = false;
				}
			}
			if (!ok)
			{
				alert("Please select the 'stars' to fill the survey and then submit");
				return;
			}
			if (!all)
			{
				if (!confirm("You have not answered the all the questions, do you want to submit anyway?"))
				{
					return;
				}
			}
			document.getElementById("surveypopup").style.display = "none";
			var url = "save_survey.jsp?userName=<%=URLEncoder.encode(userName)%>&uploadDir=<%=URLEncoder.encode(uploadDir)%>&random=" + new Date();
			var form = document.survey;
			var formdata = $(form).serialize()
				 $.ajax({         
						url: url,         
						type: 'post',         
						data : formdata,
						async: false,         
						cache: false,         
						timeout: 30000,         
						error: function(){return true;},
						success: function(msg){
							//alert(msg);
							var response = msg;
						}
				});
			document.getElementById("surveydone").style.display = "inline";
		}
		function closeSurvey()
		{
			if (!confirm("Do you want to skip submitting the survey?"))
			{
				return;
			}
			document.getElementById("surveypopup").style.display = "none";

		}

	</script>
<style>

.surveypopup {
	-moz-box-shadow:3px 3px 10px rgba(0,0,0,0.3);
	-webkit-box-shadow:3px 3px 10px rgba(0,0,0,0.3);
	box-shadow:3px 3px 10px rgba(0,0,0,0.3);
	-moz-border-radius:7px;
	-webkit-border-radius:7px;
	border-radius:7px;
	background-color:white;
	border:3px solid #00ace4;
	border:3px solid rgba(0,172,228,0.7);
/*	position: absolute;
	z-index: 4;
	left:250px;
	top:200px */
	font-family: Verdana, Arial, Helvetica, sans-serif;
	width:770px;
	font-size: 10pt;
	font-weight: bold;
	color: black;
	border: solid 2px black;
	background-color: #FFFFFF;
	padding-left: 20px;
	padding-right: 20px;
	text-align: left;
}
.surveytable {
border-collapse: collapse;
border-spacing: 0;
font-weight: normal;
}
.surveytable  td{
padding:3px
font-weight: normal;
vertical-align:top;
}
.surveytable  td a{
color:black;
cursor:pointer;
}
.surveytable a {
	font-weight: bold;
}

</style>
    </head>
<link type="text/css" rel="stylesheet" href="css/phrases.css" />
<script src="phrases.js" type="text/javascript"></script>
<link rel="stylesheet" href="ifiles/stylesheet.css" />
<script type="text/javascript" src="ifiles/javascript.js"></script>
<script type="text/javascript" src="ratingsys.js"></script> 

<div class="middle">
<center>
<div id="surveypopup" class="surveypopup">
<form name="survey" method="post">
	<p>
	<br>
	<b>We are constantly improving ResumeSort, and we want to hear directly from you. Please answer this brief quality survey about your ResumeSort experience.</b>
	<br><br>
	<table cellpadding=5 class="surveytable" width=100%>
<%
	for (int i = 0; i < questions.length; i++)
	{
%>
	<tr class="kwdRecord" id="kwd_<%=i%>" onmouseover="helperActions(this, 'show')" onmouseout="helperActions(this, 'hide')">
	<td class="kwdText">
	&#149; <%=questions[i]%>
	<input type=hidden name="question" id="question_<%=i%>" value="<%=questions[i]%>">
	</td>
	<td class="kwdRating">
	<div class="rateActions">
		<div id="rateMe" style="width:auto;float:left;" onmouseout="starRater(<%=i%>)">
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="ehh..." id="<%=i%>_1" onclick="starRater(this, 'save')" ></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Not Bad" id="<%=i%>_2" onclick="starRater(this, 'save')" ></a>

		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Pretty Good" id="<%=i%>_3" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Outstanding" id="<%=i%>_4" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Awesome" id="<%=i%>_5" onclick="starRater(this, 'save')" class=""></a>
		</div>
		<span style="display:none;" class="actionStatus" id="status<%=i%>"></span>
		<span style="float:right;" id="actions<%=i%>">
		<a onclick="clearStarRating(<%=i%>);return false;" href="#" style="display:none;" id="clear<%=i%>">Clear</a>
		</span>
	</div>
	<input type="hidden" value="0" id="rating_<%=i%>" name="rating"/>
	<input type="hidden" value="0" id="required_<%=i%>" name="required"/>
 </td>
	<!-- td>&nbsp;&nbsp;<a id="clear<%=i%>" style="visibility:hidden;font-size:9px;font-weight:normal" href="#" onclick="clearRating(<%=i%>)">Clear</a> </td -->
	</tr>
<% } %>
	<tr>
	<td align=center colspan=100% style="font-weight:bold">
	<br>
	<b>Additional comments or feedback:</b>
	</td>
	</tr>
	<tr>
	<td align=center colspan=100%>
	<textarea name=comments rows=5 cols=60></textarea>
	</td>
	</tr>
	<tr>
	<td align=center colspan=100% >
	<br>
	If you want to share your comments or questions directly with us please send us an email to 
	<br><a href="mailto:info@textnomics.com">info@textnomics.com</a></b>	</table>
	</td>
	</tr>
	<br><center><input type='button' value='Submit' onclick='submitSurvey()'>&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' value='Close' onclick='closeSurvey()'></center><br><br>
</form>
</div>
</center>
<div id="surveydone" style="display:none">
<br>
<br>
<br>
<br>
<b>Thank you for answering this brief survey. Your feedback is really appreciated. </b>
<br>
<br>
<br>
<br>
<br>
</div>
<%@include file="footer.jsp"%>
</body>
</html>
