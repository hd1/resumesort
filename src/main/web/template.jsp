<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%
	if (user == null)
	{
		response.sendRedirect("");
		return;
	}
	String userdir = ResumeSorter.getUploadDir(user);
    session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString(), userdir);
	String templateId = request.getParameter("templateId");
	if (templateId == null)
	{
%>	<script>alert("Invalid template id");</script> <%
		return;
	}
	UserTemplate template =  (UserTemplate) new UserTemplate().getObject("id=" + templateId); 
	if (template == null)
	{
%>	<script>alert("Template not found");</script> <%
		return;
	}
	List<UserTemplatePhrase> templatePhrases =  new UserTemplatePhrase().getObjects("template_id=" +templateId);
	boolean usestars = true;
	if ("false".equals(request.getParameter("usestars")))
		usestars = false;
	else if (request.getParameter("usestars") == null && user.isSimpleRating())
		usestars = false;
%>
<script>
	function showTemplate(obj)
	{
		window.location = "template.jsp?templateName=" + obj.options[obj.selectedIndex].value;
	}
	function deleteTemplate(name, id)
	{
		if (!confirm("Are you sure you wish delete template: " + name + "?"))
		{
			return;
		}
		window.location = "template_list.jsp?command=deleteTemplate&templateId=" + id;
	}
	function useStarSelection(yesno)
	{
		window.location = "template.jsp?templateId=<%=templateId%>&usestars=" + yesno;
	}
</script>
	<style type="text/css" title="currentStyle">
			@import "js/table/css/demo_page.css";
			@import "js/table/css/demo_table_jui.css";
			@import "js/table/ui-lightness/jquery-ui-1.8.4.custom.css";
		</style>
		<script type="text/javascript" language="javascript" src="js/table/js/jquery.dataTables.js"></script>
<link type="text/css" href="../css/ui/jquery-blue.css" rel="stylesheet" />	
<div class="middle">
<div class="maintext_dashboard">
<%
	String name = user.getName();// What kind of stupid css is this? It screws up without the spaces.
	if (name.length() < 6) name = name + "&nbsp;&nbsp;&nbsp;";
%>
 
 <div class="sky_bar">
 	<h3>Dashboard </h3>
    <p>Welcome back <%=name%> !</p>
	<a href="#" class="selected">JOB TEMPLATES</a>
    <a href="resume_folder_list.jsp">RESUME FOLDERS</a>
    <a href="upload_jobposting.jsp">START NEW JOBS</a>
    <a href="home.jsp">PREVIOUS JOBS</a>
 </div>
 
 <div class="text_dashboard">
<%
	String error = request.getParameter("error");
	if (error != null && error.length() > 0)
	{
%> <span class="error"><%=error%></span><%
	}
%>
<br/><br/>
 
 <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"aoColumns": [
								null,
								null,
								null,
								null,
								{ "bSortable": false }]
				});
			} );
			
	</script>
<center><h1>Job Template: <%=template.getName()%> 
<div style="width:800px">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>Template Phrase</th>
<% if (usestars) { %>
			<th>Importance</th>
			<th>Must Have</th>
<% } else { %>
			<th>Nice to Have</th>
			<th>Should Have</th>
			<th>Must Have</th>
<% } %>
			<th>Tag</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<%
	for (int i = 0; i < templatePhrases.size(); i++)
	{
%>
  <tr style="font-weight:bold"  class="gradeC">
    <td><%=templatePhrases.get(i).getPhrase()%></a></td>
<% if (usestars) { %>
    <td class="center">
	<% for (int j = 0; j < 5 && j < templatePhrases.get(i).getScore() && templatePhrases.get(i).getScore() != 6; j++) { %>
	<img src="star_on.gif">
	<% } %>
	</td>
<% } else { %>
    <td class="center"><%=templatePhrases.get(i).getScore() < 3?"<img src='check.png'>":""%></td>
    <td class="center"><%=templatePhrases.get(i).getScore() == 3?"<img src='check.png'>":""%></td>
<% } %>
    <td class="center"><%=templatePhrases.get(i).getScore() == 6?"<img src='check.png'>":""%></td>
    <td class="center"><%=templatePhrases.get(i).isTag()?"<img src='check.png'>":""%></td>
	<td class="center">  <img src="x.png" onclick='deletePhrase("<%=templatePhrases.get(i).getPhrase()%>", <%=templatePhrases.get(i).getId()%>)'/></td>
  </tr>
<%	} %>
	</tbody>
</table>
<br><br>
<% if (usestars) { %>
&nbsp;&nbsp;&nbsp;<a href="javascript:useStarSelection(false)" class="orangeBtn">Display Simple Rating</a>
<% } else { %>
&nbsp;&nbsp;&nbsp;<a href="javascript:useStarSelection(true)" class="orangeBtn">Display 5 Star Rating</a>
<% } %>
</div>
 
<div class="clear"></div>
</div>
</div>
</div>


<%@include file="footer.jsp"%>
</body>
</html>
<%!
	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
	String checkNull(Object value, String defval)
	{
		if (value == null)
			return defval;
		else if (value instanceof Date)
			return dateformat.format(value);
		return value.toString();
	}
    int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>