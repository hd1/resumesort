<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%
	if ("true".equals(request.getParameter("logout")))
	{
			session.removeAttribute(SESSION_ATTRIBUTE.SUGGESTER.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.TARGET_FILE.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.REQUEST_STAMP.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.FILTER_TYPE.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.OPTIONS_LIST.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.HELP_LIST.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.EXTRAS_INDEX.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.LAST_SUGGESTION_INDEX.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.RANK_MAP.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.KEY_PHRASE_IDENTIFIER.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());	
	}
	else if (session != null)
	{
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		Integer numleft = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
		if (numleft == null) numleft = 0;
		List<File> resumeFiles = (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		List<Double> scores = (List<Double>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
		if (user != null && resumeFiles != null && scores != null)
		{
			int numProcessed = 0;
			int totalResumes = resumeFiles.size();
			for (int i = 0; i < scores.size(); i++)
				if (scores.get(i) != null) numProcessed++;
			// if still processing resumes
			if (numleft > 0)
			{
%>
<script>window.location="stillProcessing.jsp"</script>
<%
			}
			else
			{
%>
<script>window.location="highlightedResumes.jsp"</script>
<%
			}
			return;
		}
	}
	String error = request.getParameter("error");
	if (error != null && error.trim().length() > 0)
	{
%>	<script>alert("<%=error%>");</script> <%
	}
	String redirectURL = request.getParameter("redirectURL");
	if (redirectURL == null) redirectURL = "";
	String login = request.getParameter("login");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ResumeSort - Optimize your resume - the most advanced resume sorter on the planet.</title>
  <meta charset="utf-8" />
  <!--[if lt IE 9]>
   <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="all.css" />
  <link rel="stylesheet" href="colorbox/colorbox.css" />
  <!--[if lt IE 9]>
  <link rel="stylesheet" href="ie.css" />
  <![endif]-->  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
  <script src="colorbox/jquery.colorbox-min.js"></script>
  <script>
	var browser = navigator.userAgent;
	var vendor = navigator.vendor;
	var platform = navigator.platform;
	if (platform == null) platform == "";
	if (vendor == null) vendor = "";
	if (vendor.indexOf("Apple") != -1 || browser.indexOf("Opera") != -1) // Google, Firefox,
	{
		alert("You are using Opera or Safari as your browser.\nWe recommend using Chrome or Firefox instead.");
	}
	var url = window.location;
	url = new String(url);
	if (url.indexOf("50.19.246.224") != -1 || url.indexOf("resumesort.com/ResumeSorter") != -1)
	{
		window.location="http://www.resumesort.com/Login.jsp";
	}
  </script>
<body>
<header id="header" class="central">
  <h1 id="logo">Resume <strong>Sort</strong></h1>
  <nav id="user-nav"></nav>
</header>
<div id="siteTag">
  <h2 class="central">The most advanced resume sorter on the planet</h2>
</div>

<div class="central">

<table border="0" cellpadding="0" width="97%" cellspacing="0">
	<tr>
		<td><br />
		<span style="font-family:Arial, Helvetica, sans-serif ;font-size:16px; "> 
	    Thank you for coming to ResumeSort alpha page.  If you are not currently registered  
    	for the Alpha, you will be registered for the <b>Alpta</b>&#0153; release (between alpha and beta).
    	ResumeSort's patent pending technology allows you to sort and rank resumes submitted for a
    	specific job posting using the relevant keywords and phrases extracted from the job ad.   
		</span>
		</td>
	</tr>
</table>

<!--<table border="0" cellpadding="0" width="97%" cellspacing="0">-->
<!--	<tr>-->
<!--		<td><br />-->
<!--		<span style="font-family:Arial, Helvetica, sans-serif ;font-size:16px; "> -->
<!--		Thank you for coming to ResumeTuner (TM) alpha page.<br /><br /> -->
<!--		ResumeTuner's patent pending technology allows you-->
<!--		to optimize your resume to a specific job posting using the relevant-->
<!--		keywords and phrases extracted from the job ad. This allows you to-->
<!--		optimize your resume for each job posting, highlighting your skills in-->
<!--		the language that of the job Ad, and what is being searched for by the-->
<!--		recruiters, crawlers, and application tracking systems. In effect, you-->
<!--		are helping them find you faster and moving your resumes closer to the-->
<!--		top of the pile :-) It even allows you to enter your own text and-->
<!--		combine it with our suggestions. <br /><br />-->
<!--		If you are not currently registered-->
<!--		for the Alpha, we will register you for the <i><b>Alpta</b></i> (between alpha and-->
<!--		beta), and let you know shortly-->
<!--		</span>-->
<!--		</td>-->
<!--	</tr>-->
<!--</table>-->
<br />
<br />
<br />
<center>
<%
	if (login == null)
	{
		login=(String)request.getSession().getAttribute("login");
		request.getSession().removeAttribute("login");
	}
%>
	
    <div style="padding-left:15px;">
    <form name="login" method='post' action='Authenticator' id="login">
	<table width="30%" border="0" rules="none" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%"><span class="green">Email:</span></td>
		<td width="10%">&nbsp;</td>		
		<% if(login==null){%>
		<td width="70%"><input type="text" name="login" maxlength="50" onkeypress="return checkenter(event)"/></td>
		<%}else{ %>
		<td width="70%"><input type="text" name="login" value="<%out.write(login); %>" maxlength="50" onkeypress="return checkenter(event)"/></td>
		<%} %>
		<input type=hidden name=redirectURL value="<%=redirectURL%>">
	</tr>
     <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
	<tr>
		<td width="20%"><span class="green">Password:</span></td>
		<td width="10%">&nbsp;</td>
		<td width="70%"><input type="password" name="password" maxlength="20" onkeypress="return checkenter(event)"/></td>
	</tr>
	<tr>
    	<td colspan="3">&nbsp;</td>
    </tr>                
    
    <tr>
	<td  width="20%" align="center">
	<input type='button' value='Login' onclick="doSubmit()" style="width: 100px;" />
	</td>
	<td  width="10%">&nbsp;</td>
	<td  width="70%" align="center">
	<input type='reset' value='Reset' style="width: 100px;" />
	</td>
	</tr>
	<tr>
	<td align="center"><a href="Register.jsp?type=1"> Sign Up Here! </a></td>
	<td  width="10%">&nbsp;</td>
	<td align="center"><a href="javascript:sendNewPassword()"> Forgot My Password! </a></td>
	</tr>			
    
</table>

</form>
</div>
</center>
</div>
<script>
function doSubmit()
{
	var login = document.login.login.value
	if (login == "")
	{
		alert("Please enter your email address");
		return;
	}
	if (login.indexOf('@') == -1 && login != 'esfandiar' && login != 'devg')
	{
		alert("Please enter your complete email address (name@email.com)");
		return;
	}
	document.login.submit();
}

function checkenter(event, num)
{
	if (event == null)
		event = window.event;
	var keyCode = event.keyCode
	if (keyCode == null)
		keyCode = event.which;
	if(keyCode == 13)
	{
		doSubmit();
		return true;
	}
	return true;
}
function sendNewPassword()
{
	var login = document.login.login.value
	if (login == "")
	{
		alert("Please enter your email address");
		return;
	}
	if (login.indexOf('@') == -1 && login != 'esfandiar')
	{
		alert("Please enter your complete email address (name@email.com)");
		return;
	}

	window.location="SendPassword?login=" + document.login.login.value;
}
document.login.login.focus();
</script>
<%@include file="rq_footer.jsp"%>
  
</body>
</html>