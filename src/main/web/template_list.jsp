<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%
	if (user == null)
	{
		response.sendRedirect("");
		return;
	}
	String userdir = ResumeSorter.getUploadDir(user);
    session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString(), userdir);
	String templateId = request.getParameter("templateId");
	if ("deleteTemplate".equals(request.getParameter("command")) && templateId != null)
	{
			new UserTemplatePhrase().deleteObjects("TEMPLATE_ID = " + templateId);
			new UserTemplate().deleteObjects("ID = " + templateId);
	}
	List<UserTemplate> templates =  new UserTemplate().getObjects("user_name=" + UserDBAccess.toSQL(user.getLogin())); 
%>
<script>
	function showTemplate(obj)
	{
		window.location = "template.jsp?templateName=" + obj.options[obj.selectedIndex].value;
	}
	function deleteTemplate(name, id)
	{
		if (!confirm("Are you sure you wish delete template: " + name + "?"))
		{
			return;
		}
		window.location = "template_list.jsp?command=deleteTemplate&templateId=" + id;
	}
</script>
	<style type="text/css" title="currentStyle">
			@import "js/table/css/demo_page.css";
			@import "js/table/css/demo_table_jui.css";
			@import "js/table/ui-lightness/jquery-ui-1.8.4.custom.css";
		</style>
		<script type="text/javascript" language="javascript" src="js/table/js/jquery.dataTables.js"></script>
<link type="text/css" href="../css/ui/jquery-blue.css" rel="stylesheet" />	
<div class="middle">
<div class="maintext_dashboard">
<%
	String name = user.getName();// What kind of stupid css is this? It screws up without the spaces.
	if (name.length() < 6) name = name + "&nbsp;&nbsp;&nbsp;";
%>
 
 <div class="sky_bar">
 	<h3>Dashboard </h3>
    <p>Welcome back <%=name%> !</p>
	<a href="#" class="selected">JOB TEMPLATES</a>
    <a href="resume_folder_list.jsp">RESUME FOLDERS</a>
    <a href="upload_jobposting.jsp">START NEW JOBS</a>
    <a href="home.jsp">PREVIOUS JOBS</a>
 </div>
 
 <div class="text_dashboard">
<%
	String error = request.getParameter("error");
	if (error != null && error.length() > 0)
	{
%> <span class="error"><%=error%></span><%
	}
%>
<br/><br/>
 
 <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"aaSorting": [[ 2, "desc" ]],
					"aoColumns": [
								null,
								null,
								{ "bSortable": false }
								]
				});
			} );
			
	</script>
<div style="width:800px">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>Template</th>
			<th>Industry</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<%
	for (int i = 0; i < templates.size(); i++)
	{
%>
  <tr style="font-weight:bold"  class="gradeC">
    <td><a href="template.jsp?templateId=<%=templates.get(i).getId()%>"><%=templates.get(i).getName()%></a></td>
    <td class="center"><%=checkNull(templates.get(i).getIndustry(),"")%></td>
	<td class="center">  <img src="x.png" onclick='deleteTemplate("<%=templates.get(i).getName()%>", <%=templates.get(i).getId()%>)'/></td>
  </tr>
<%	} %>
	</tbody>
</table>
</div>
 
<div class="clear"></div>
</div>
</div>
</div>


<%@include file="footer.jsp"%>
</body>
</html>
<%!
	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
	String checkNull(Object value, String defval)
	{
		if (value == null)
			return defval;
		else if (value instanceof Date)
			return dateformat.format(value);
		return value.toString();
	}
    int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>