<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.mail.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.openoffice.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%

		System.out.println("Entered save scores");
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (user == null) 
		{
%>
Error: Your session has expired.
<%
			return;
		}
		List<File> resumeFiles = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		List<Integer> userScores = (List<Integer>)session.getAttribute(SESSION_ATTRIBUTE.USER_SCORES.toString());
		List<String> emails =  (List<String>) session.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_EMAILS.toString());
		JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
		for (int i = 0; i < resumeFiles.size(); i++)
		{
			String rating = request.getParameter("rating_" + i);
			if (rating == null) continue;
			String fileName = resumeFiles.get(i).getName();
			if (userScores == null)
			{
				userScores = new ArrayList<Integer>(resumeFiles.size());
				for (int j = 0; j < resumeFiles.size(); j++)
					userScores.add(0);
				session.setAttribute(SESSION_ATTRIBUTE.USER_SCORES.toString(), userScores);
			}
			userScores.set(i, getInt(rating));
			List<ResumeScore> oldScores = new ResumeScore().getObjects("RESUME_FILE = " + UserDBAccess.toSQL(fileName)
						+ " and JOB_ID =" + posting.getId() + " and user_name =" + UserDBAccess.toSQL(user.getLogin()));
			if (oldScores.size() > 0)
			{
				ResumeScore rs = oldScores.get(0);
				rs.setUserScore(getInt(rating));
				rs.update();
			}
			if (emails != null)
			{
				Candidate candidate = (Candidate) new Candidate().getObject("email =" + Candidate.toSQL(emails.get(i)));
				if (candidate != null)
				{
					UserCandidate userCandidate = (UserCandidate) new UserCandidate().getObject("candidate_id =" + candidate.getId() + " and user_name=" + UserCandidate.toSQL(user.getLogin()));
					if (userCandidate != null)
					{
						userCandidate.setUserScore(getInt(rating));
						userCandidate.update();
					}
				}
			}
		}
		JobPostingSession jps = new JobPostingSession(user.getLogin(), posting.getId());
		jps.save(session);
%>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>