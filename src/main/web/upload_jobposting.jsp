<%@ page import="java.io.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@include file="header.jsp"%>
<%
	JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	boolean jobinprogress = false;
	Integer numleft = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
	if (numleft == null) numleft = 0;
	if (numleft > 0)
		jobinprogress = true;
    if (session.getAttribute(SESSION_ATTRIBUTE.RANKING_INPROGRESS.toString()) != null)
		jobinprogress = true;
	if (jobinprogress)
	{
		System.out.println("numleft:" + numleft + " Ranking in progress:" + session.getAttribute(SESSION_ATTRIBUTE.RANKING_INPROGRESS.toString()));
%>
<script> alert("Previous job '<%=posting.getPostingId()%>' is still being processed. Please wait and  retry"); window.location = "home.jsp";</script>
<%
		return;
	}
	// Clear all old results
	session.removeAttribute(SESSION_ATTRIBUTE.TARGET_FILE.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_HTMLRESUMES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString());
	List<JobPosting> postings = new JobPosting().getObjects("user_name=" + UserDBAccess.toSQL(user.getLogin()));

	String []jpfileNames=null;
	if (user == null)
	{
		response.sendRedirect("");
		return;
	}
	request.setAttribute("email", user.getEmail());

	//Get existing list of MyResumes
	String uploadDir="uploads" + File.separator + user.getLogin() + File.separator;
	File userFolder = PlatformProperties.getInstance().getResourceFile(
							uploadDir + File.separator + "MyPostings" + File.separator);
	//Content of folder
	if(userFolder.exists()){
			jpfileNames=userFolder.list();
			request.setAttribute("myPostings", jpfileNames);
			System.out.println("Job files:" + jpfileNames.length);
	}
	else
			userFolder.mkdirs();
	
	List<UserTemplate> templates =  new UserTemplate().getObjects("user_name=" + UserDBAccess.toSQL(user.getLogin()) + " order by name");

	//String defaultJobId = user.getCompany();
	//if (defaultJobId == null || defaultJobId.trim().length() == 0)
	//	defaultJobId = user.getName();
	//if (defaultJobId.indexOf(" ") != -1)
	//	defaultJobId = defaultJobId.substring(0, defaultJobId.indexOf(" "));
	//if (defaultJobId.length() > 4)
	//	defaultJobId = defaultJobId.substring(0, 4);
	String resumeFolder = request.getParameter("resumeFolder");
	Integer numResumes = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
	if (numResumes == null && resumeFolder != null)
	{
        File resumeDirectory = new File(PlatformProperties.getInstance().getResourceFile(uploadDir), ResumeSorter.RESUME_FOLDER + "/" + resumeFolder);
        String bserver = PlatformProperties.getInstance().getProperty("com.textonomics.beanstalkserver");
        if (false && "true".equals(bserver))
        {
                //PJM create an BeanstalkClient instant and pass the job
			   //BeanstalkClient bClient =BeanstalkClient.getInstance();
			   //bClient.connect(session, resumeDirectory);
		}
		else
		{
			ZipfileProcessingThread zipThread = new ZipfileProcessingThread(session, resumeDirectory, false);
			zipThread.start();
		}
	}

	String defaultJobId = new SimpleDateFormat("-yyyy-MM-dd-HHmm").format(new Date());
	
	String error = request.getParameter("error");
	if (error == null || error.length() == 0)
	if ((error = (String)request.getSession().getAttribute(SESSION_ATTRIBUTE.ERROR.toString())) != null)
	{
		request.getSession().removeAttribute(SESSION_ATTRIBUTE.ERROR.toString());
	}
	if ("null".equals(error)) error = "";
	String defaultJobText = "";
	String testJobText = "zzzzzzz is working with an established company seeking a Sr. Product Manager for the following PERMANENT opportunity: \n\nThe Sr. Product Manager will be responsible initially for flagship products, Anti-virus and Security Suite. The position requires development of product strategy and, by working with Engineering, the development of Security applications, aimed at consumers and businesses, and is also responsible for the ultimate success of the product in the market. \n\nThis person will analyze end users� needs, marketing research, and the competitive landscape, leading to the creation/enhancement of future versions of the products. The Sr. Product Manager will be held accountable for developing the product strategy, roadmap, specific requirements for each release, product revenue generation, product adoption and overall product usability. This role is a matrix management position that will operate in a cross-functional environment and will interface with Engineering, Marketing, Sales, Technical Support, and Finance. \n\nThe Sr. Product Manager will report directly to the VP of Product Management, must be fluent in English and will be located in Brno, Czech Republic. \n\nEssential Functions: \n� Analyze market data, the competitive landscape, customer needs, etc. -- and understand when it�s time to stop studying and start �doing�; an intuitive sense of what it takes to make your products successful. \n� Create business plans to justify the development of new products/releases, and to define the launch, distribution, marketing and financial strategies for your products. \n� Define product strategy and roadmap for your products. \n� Drive product requirements and their prioritization. Balance needs of the various groups inside and outside of Grisoft, including Sales, Marketing, Finance, Technical Support and Engineering in making product decisions. \n� Define launch strategies and plans and guide ongoing product marketing activities. \n� Conceive, evaluate, develop and manage new products and functionality. \n\nAdditional Responsibilities: \n� Work closely with Project Management during the development phase of your products, and with Sales & Marketing to define launch and ongoing promotional activities. \n� Define and drive necessary consumer research and usability testing. \n\nKnowledge, Skills, and Abilities: \n� Impeccable communicator, a diplomat, a collaborator, and a builder of teams; a builder of a series of positive interactions along the way to successful product implementations; can work with different groups and personality types to achieve success. \n� Strong technical skills are required to understand our technical capabilities, limitations and to gain the respect of the engineering staff. \n� Highly organized with careful attention to detail, excellent planning and priority setting skills. \n� You will �roll up your sleeves� when necessary to get the job done. You are not focused on hierarchy or titles. \n� Understand the product life cycle and the ability to determine, and extend products during their life cycle. \n� Strong leadership and negotiation skills are important. \n� Strong analytical skills necessary to build financial cases and understand consumer and usage data \n\nQualifications, Training, and Experience: \n\nThe candidate must have a minimum of three years experience as a Product Manager for software applications. Bachelor�s degree required, technical degree and/or MBA preferred. \n";
%>
<link href="css/smart_wizard.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
	$(function() {					
		$('#icon *').tooltip();
	});

<% if (error != null && error.length() > 0) { %>
	alert("Error Message: <%=error%>\n Please contact site support at support@textnomics.com");
<% } %>
	var EMPTY_JOB_TEXT = "<%=defaultJobText%>";
	var EMPTY_EMAIL_TEXT = "This is where your results will be emailed to";
	
	function clearDefaultJobText(el) 
	{
		if (el.value == EMPTY_JOB_TEXT)
			el.value = "";
	}

	function setDefaultJobText(el) 
	{
		if (el.value == "")
			el.value = EMPTY_JOB_TEXT;
	}

	function clearDefaultEmailText(el) 
	{
		if (el.value == EMPTY_EMAIL_TEXT)
			el.value = "";
	}

	function clearFileText()
	{
		document.getElementById("selectResume").value = "";
		//document.uploadform.targetfile.value="";
		var filedd = document.getElementById("filedropdown");
		document.getElementById("step1ResumeName").innerHTML = filedd.options[filedd.selectedIndex].text;
		document.getElementById("step1Tick").style.visibility = "visible";
		//document.uploadform.uploadResume.checked="";
	}
	function clearSourceFileText()
	{
		document.getElementById("JobDescFile").value = "";
	}	
	function showFileData()
	{
		if (document.getElementById("filedropdown2").selectedIndex != 0)
		{
			document.getElementById("theJobDescription").value = "";
			var fileName = document.getElementById("filedropdown2").options[document.getElementById("filedropdown2").selectedIndex].value;
			if (fileName.indexOf(".txt") != -1)
			{
				 var url = "get_jobposting.jsp?jobpostingName=" + escape(fileName);
				 $.ajax({         
						url: url,         
						type: 'GET',         
						async: false,         
						cache: false,         
						timeout: 30000,         
						error: function(){return true;},
						success: function(msg){
							//alert(msg);
							document.getElementById("theJobDescription").value = msg;
						}
				});
			}
		}
	}

	function clearFileSelect()
	{
		//document.uploadform.selectedResume.selectedIndex=0;
		//alert("file selected1" + document.uploadform.targetfile.value);
		//var filepath = document.uploadform.targetfile.value;
		//ind = filepath.lastIndexOf("\\");
		//if (ind == -1) filepath.lastIndexOf("/");
		//if (ind != -1)
		//	filepath =  filepath.substr(ind+1));
		//alert("file selected2" + filepath);
		//document.getElementById("step1ResumeName").innerHTML = document.uploadform.targetfile.value;
	}	
	
	function setDefaultEmailText(el) 
	{
		if (el.value == "")
			el.value = EMPTY_EMAIL_TEXT;
	}

	var busy = false;
	function askFriends()
	{
		var assign = document.getElementById("Assign");
		if (assign != null)
			assign.value = "true";
		doSubmit();
	}
	function  doSubmit() 
	{
		if (isValid())
		{
			if (busy)
			{
				alert("Processing request... Please click OK and wait for your results.");
			}
			//document.getElementById("loading").style.visibility = "visible";
			var myvar = "1";

			<% session.setAttribute("listSelected","1"); %>
                        <%-- session.setMaxInactiveInterval(3600); --%>
			document.getElementById("loading").style.visibility = "visible";
			busy = true;
			document.uploadform.submit();
		}		
	}
	function checkPostingId(obj)
	{
		var newPostingId = obj.value;
		//alert("checkPostingId:" + newPostingId);
<% if (postings.size() == 0) { %>
		return true;
<%	} %>
<%
		for(int i = 0 ;i < postings.size();i++)	{
			String oldPosting = postings.get(i).getPostingId();
%>
			if (newPostingId.toLowerCase() == "<%=oldPosting.toLowerCase()%>")
			{
				if (!confirm("This Job Posting Id already exists. Do you want to change it to:" + newPostingId + "-2?"))
					return false;
				newPostingId = newPostingId + "-2";
				obj.value = newPostingId;
			}
<%
		}
%>
		return true;
	}

	var multiUploadUsed = false;
	function checkUpload(data,status)
	{
		if (data != '0' && data != '')
		{
			multiUploadUsed = true;
			document.getElementById("step1Tick").style.visibility = "visible";
		}
	}
	function checkMultiUpload()
	{
		 $.ajax({         
				url: "check_resumes_uploaded.jsp",         
				type: 'GET',         
				async: false,         
				cache: false,         
				timeout: 30000,         
				error: function(){return true;},
				success: function(msg){
					//alert(msg);
					if (parseInt(msg) > 0){                 
						multiUploadUsed = true;
						return true;
					} else {                 
						return false;             
					}         
				}
		});
	}
	function  isValid()
	{				
		if (document.getElementById("theJobDescription").value=="" || document.getElementById("theJobDescription").value==EMPTY_JOB_TEXT )
		{
			if (document.getElementById("JobDescFile").value == "" && (document.getElementById("filedropdown2") == null || document.getElementById("filedropdown2").selectedIndex == 0) && (document.getElementById("selectedTemplate") == null || document.getElementById("selectedTemplate").selectedIndex == 0))
			{
				alert("Please paste or upload a valid job ad.");
				return false;
			}
		}
		var jobtitle = document.getElementById("JobTitle").value;
		if (jobtitle == '')
		{
			alert("Please enter a Job Title");
			document.getElementById("JobTitle").focus();
			return;
		}
		if (document.getElementById("JobPostingId").value == "")
		{
			//if (document.getElementById("filedropdown2") == null || document.getElementById("filedropdown2").selectedIndex == 0)
			{
				var jobId = jobtitle.replace(' ','-').replace("'","-") + "<%=defaultJobId%>";
				if (!confirm("You have not entered a Job Posting Id.\nWe will use a the following generated Id : " + jobId + " instead.\nIf you wish to use a different Id, please press Cancel and\nEnter your Job Posting Id."))
				{
					document.getElementById("JobPostingId").focus();
					return false;
				}
				else
				{
					document.getElementById("JobPostingId").value = jobId;
				}
			}
		}
		else
		{
			var tg1=/[\\\/:\*?"'<>|]/;
			if ( tg1.test(document.getElementById("JobPostingId").value) )
			{
				alert('JobPosting ID cannot contain any of the following characters:\/:*?"<>|');
				return;
			}
			if (!checkPostingId(document.getElementById("JobPostingId")))
				return;
		}
		var joblocation = document.getElementById("JobLocation").value;
		var locint = "" + parseInt(joblocation);
		if (joblocation != '' && (joblocation.length > 5 || !isNumeric(joblocation)))
		{
			alert("Please enter a valid Zipcode");
			document.getElementById("JobLocation").focus();
			return;
		}
		var industries = document.getElementsByName("industry");
		var indok = false;
		for (var i = 0; i < industries.length ; i++ )
		{
			if (industries[i].checked)
			{
				indok = true;
				break;
			}
		}
		if (!indok)
		{
			alert("Please check the industry(ies) your are recruiting for");
			return false;
		}
		return true;
	}

	function isNumeric(input)
	{
		return (input - 0) == input && input.length > 0;
	}
	function generateId()
	{
		var jobtitle = document.getElementById("JobTitle").value;
		if (jobtitle == '')
		{
			alert("Please enter a Job Title first");
			document.getElementById("JobTitle").focus();
			return;
		}
		if (document.getElementById("JobPostingId").value != "")
		{
			if (!confirm("Overwrite the entered ID?"))
			{
				return;
			}
		}
		document.getElementById("JobPostingId").value = jobtitle.replace(' ','-').replace("'","-") + "<%=defaultJobId%>";
		document.getElementById("JobPostingId").focus();
		document.getElementById("JobPostingId").select();
	}
	function goHome()
	{
		if (confirm("Are you sure you want to leave this page and start over?"))
		{
			window.location = "home.jsp";
		}
	}
</script>
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
	<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    
    <script type="text/javascript">
        $(function() {
	        $("a[rel^='prettyPhoto']").prettyPhoto();

        });
    </script>
<div class="middle">
<div class="maintext_dashboard">
 
 <div class="sky_bar">
    <div style="float:right; clear:both;">
    <a href="#"  class="selected">START NEW JOBS</a>
    <a href="javascript:goHome()">PREVIOUS JOBS</a>
    </div>
 </div>
 
 <div class="text_dashboard">
  <div id="loading" style="visibility: hidden">
    <p>Processing...</p>
  </div>
<table align="center" border="0" cellpadding="0" cellspacing="0">
<tr><td> 
<!-- Smart Wizard -->
  		<div id="wizard" class="swMain">
  			<ul class="anchor">
  				<li><a class="selected" href="#step-2">
                <label class="stepNumber">1</label>
                <span class="stepDesc">
                   Upload Job Posting<br />
                   <small>Job Posting Details</small>
                </span>
				</a></li>
				<li><a href="javascript:doSubmit()">
                <label class="stepNumber">2</label>
                <span class="stepDesc">
                   Upload Resumes <br/> 
                   <small>Upload to Resume Folder</small>              
                </span>
            </a></li>
  				<li><a class="mini" href="#" >
                <label class="stepNumber">3</label>
                <span class="stepDesc">
                   Select Phrases
                </span>                   
             </a></li>
  				<li><a class="miniRight" href="#">                
                <span class="stepDesc">
                   Ask My Friends
                </span>                   
            </a></li>
  			</ul>
  			<div class="stepContainer">	
            <div class="content">
            	
<form name="uploadform" enctype='multipart/form-data' method='post' action='ResumeSorter'>
<input type=hidden name="Assign" id="Assign" value="">
<div style="padding:25px;">
<div style="float:left;">
 <div class="jobtitle">
Job Title
<input id="JobTitle" name="JobTitle" type="text" />
<span id="icon"><a title="Enter Job Posting Title" href="#"><img src="images/help.png" width="16" class="helpbtn" height="16" /></a></span></div>
 <div class="jobid">
Job Posting ID / Requisition Number<br>
<input id="JobPostingId" name="JobPostingId" onchange="checkPostingId(this)" type="text" />&nbsp;&nbsp;<a href="javascript:generateId()" class="orangeBtn">Generate ID</a>
	 <span id="icon"><a title="Enter Job Posting ID" href="#">
		<img src="images/help.png" width="16" class="helpbtn" height="16" />
	 </a></span>
</div>
<div class="jobid" style="display:none">
Job Location / Zipcode (optional)<br>
<input id="JobLocation" name="JobLocation"type="text" />&nbsp;&nbsp;
	 <span id="icon"><a title="Enter 5-digit Zipcode of where the Job is" href="#">
		<img src="images/help.png" width="16" class="helpbtn" height="16" />
	 </a></span>
</div>
<br>
<br style="clear:both; ">
    What type of job posting would you like to create?<br />
	<input name="colorselector" type=radio value="upload" onclick="changeType('upload')"> Upload 
<%	if (jpfileNames !=null && jpfileNames.length!=0) { %>
	<input name="colorselector" type=radio value="previousjob" onclick="changeType('previousjob')"> Previous Job Posting 
<%	} %>
	<input name="colorselector" type=radio value="copypaste" onclick="changeType('copypaste')"> Copy / Paste 
<%	if (false && templates != null && templates.size() > 0) { %>
	<input name="colorselector" type=radio value="template" onclick="changeType('template')"> Use A Saved Job Template 
<%	} %>
<br/>
<div style="clear:both; margin-top:30px;">
    <div id="upload" class="colors" style="display:none">
        <div class="jobup">
        Browse for the job description file<br />
        <input id="JobDescFile" name="sourcefile" size="40" type="file" />
        </div>
    </div>
    <div id="previousjob" class="colors" style="display:none">
        Select a previous posting<br/>
	<%
		String[] fileNames = (String[])request.getAttribute("myPostings");
		if(fileNames !=null && fileNames.length!=0){
	%>
	<select  class="jobtype" name='selectedPosting' onchange="clearSourceFileText();showFileData()" id="filedropdown2">
				<option value ='' >-- Select Saved Job Posting -- </option>
<%
		for(int i=0;i<fileNames.length;i++){
			if (!fileNames[i].endsWith(".Object"))
				out.write("<option>"+fileNames[i]+"</option>");
		}
%>
		</select>
<%	} %>
    </div>
    <div id="copypaste" class="colors" style="display:none">
        Copy and paste job description here: <br/>
        <textarea id="theJobDescription" name="sourcetext" cols="60" rows="25"></textarea>
    </div>
<%
	if(templates != null && templates.size() != 0){
%>
	<b>And / Or</b><br><br>
    <div id="template" class="colors2" style="display:inline">
        Select a Saved Job Template<br/>
	<select  class="jobtype" name='selectedTemplate'  id="selectedTemplate">
				<option value ='' >-- Select Saved Template -- </option>
<%
		for(int i = 0;i < templates.size();i++){
				out.write("<option>"+templates.get(i).getName()+"</option>");
		}
%>
		</select></h3>
	</div>
	<br>
	<br>
<%	} %>
	</div>
	</div>
	 <div style="width:120px;float:right;margin-top:10px">
		<a href="http://youtu.be/hqnpg-9j1uM" rel="prettyPhoto" title=""><img src="images/demo_icon.png" width=120px  /></a>
	</div>
<%
	if (false && resumeFolder != null && resumeFolder.trim().length() > 0) { %>
	<input type=hidden name="selectedResume" value="<%=resumeFolder%>">
<%	} %><div class="jobcheck">
    Type of industry you are recruiting for<br />
  <table>
  <tr>
<%
	String userIndustry = user.getIndustry();
	if (userIndustry == null) userIndustry = "";
	userIndustry = "";  // Keep unchecked for now
	String checked = "";
	String industry = "";
	boolean craigslist = true;
	if (!craigslist)
	{
	industry = "Accountant | CPA | Controller";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Financial Analyst";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Banker";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Administrative Assistant";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr><tr>
<%
	industry = "Architecture | Interior Design";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Art | Media";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Graphic Designer";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Business | Management";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr><tr>
<%
	industry = "Marketing | Public Relations | Advertising";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Marketing Associate";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Retail | Wholesale";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Sales | Business Development";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr><tr>
<%
	industry = "Business Development";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Sales Representative";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Product Manager - IT | non-IT";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Customer Service Representative";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr><tr>
<%
	industry = "Biotech";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Construction | Civil Engineering";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Mechanical Engineering";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Electrical Engineering";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr><tr>
<%
	industry = "Internet Engineering";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "WEB Design";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Software | QA | DBA | OO";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Systems | Network";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr><tr>
<%
	industry = "Programmer";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Education";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "General Labor";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Government | Federal | State";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr><tr>
<%
	industry = "Human Resources";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Security";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Legal | Paralegal";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Manufacturing";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr><tr>
<%
	industry = "Medical | Health";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Real Estate";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Restaurant | Food | Beverage | Hospitality";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Skilled Trade | Craft";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr><tr>
<%
	industry = "Social Services | Nonprofit";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Transportation";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "TV | Film | Video";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Writing | Editing";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr><tr>
<%
	industry = "Other";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
		</td><td nowrap>
		</td><td nowrap>
		</td><td nowrap>
<%  
	}
	else
	{
	industry = "accounting+finance";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>

<td nowrap><input type="checkbox"  value="accounting+finance" name="industry"/> accounting+finance</td>

<td nowrap><input type="checkbox"  value="customer service" name="industry"/> customer service</td>

<td nowrap><input type="checkbox"  value="internet engineers" name="industry"/> internet engineers</td>

<td nowrap><input type="checkbox"  value="real estate" name="industry"/> real estate</td>

<td nowrap><input type="checkbox"  value="software / qa / dba" name="industry"/> software / qa / dba</td>

<td nowrap><input type="checkbox"  value="writing / editing" name="industry"/> writing / editing</td>
</tr><tr>

<td nowrap><input type="checkbox"  value="admin / office" name="industry"/> admin / office</td>

<td nowrap><input type="checkbox"  value="education" name="industry"/> education</td>

<td nowrap><input type="checkbox"  value="legal / paralegal" name="industry"/> legal / paralegal</td>

<td nowrap><input type="checkbox"  value="retail / wholesale" name="industry"/> retail / wholesale</td>

<td nowrap><input type="checkbox"  value="systems / network" name="industry"/> systems / network</td>

<td nowrap><input type="checkbox"  value="[ETC]" name="industry"/> [ETC] </td>
</tr><tr>

<td nowrap><input type="checkbox"  value="arch / engineering" name="industry"/> arch / engineering</td> 

<td nowrap><input type="checkbox"  value="food / bev / hosp" name="industry"/> food / bev / hosp</td>

<td nowrap><input type="checkbox"  value="manufacturing" name="industry"/> manufacturing</td>

<td nowrap><input type="checkbox"  value="sales / biz dev" name="industry"/> sales / biz dev</td>

<td nowrap><input type="checkbox"  value="technical support" name="industry"/> technical support</td>
</tr><tr>

<td nowrap><input type="checkbox"  value="art / media / design" name="industry"/> art / media / design</td> 

<td nowrap><input type="checkbox"  value="general labor" name="industry"/> general labor</td>

<td nowrap><input type="checkbox"  value="marketing / pr / ad" name="industry"/> marketing / pr / ad</td>

<td nowrap><input type="checkbox"  value="salon / spa / fitness" name="industry"/> salon / spa / fitness</td>

<td nowrap><input type="checkbox"  value="transport" name="industry"/> transport</td>
</tr><tr>

<td nowrap><input type="checkbox"  value="biotech / science" name="industry"/> biotech / science</td>

<td nowrap><input type="checkbox"  value="government" name="industry"/> government</td>

<td nowrap><input type="checkbox"  value="medical / health" name="industry"/> medical / health</td>

<td nowrap><input type="checkbox"  value="security" name="industry"/> security</td>

<td nowrap><input type="checkbox"  value="tv / film / video" name="industry"/> tv / film / video</td>
</tr><tr>

<td nowrap><input type="checkbox"  value="business / mgmt" name="industry"/> business / mgmt</td>

<td nowrap><input type="checkbox"  value="human resources" name="industry"/> human resources</td>

<td nowrap><input type="checkbox"  value="nonprofit sector" name="industry"/> nonprofit sector</td>

<td nowrap><input type="checkbox"  value="skilled trade / craft" name="industry"/> skilled trade / craft</td>

<td nowrap><input type="checkbox"  value="web / info design" name="industry"/> web / info design</td>
<%  } %>
		</td></tr></table>
			<a href="javascript:doSubmit()" class="orangeBtn">Upload Resumes</a>
    </div>
      		 </div>

  		</div>
</form>
<!-- End SmartWizard Content -->  		
</td></tr>
</table> 
 		
 <script type="text/javascript">
	$(function() {    // Makes sure the code contained doesn't run until
					  //     all the DOM elements have loaded
	
		$('#colorselector').change(function(){
			$('.colors').hide();
			$('#' + $(this).val()).show();
		});
	
	});
	function changeType(showdiv)
	{
		if (document.getElementById("filedropdown2") != null)
			document.getElementById("filedropdown2").selectedIndex = 0;
		document.getElementById("theJobDescription").value = "";
		document.getElementById("JobDescFile").value = "";
		$('.colors').hide();
		$('#' + showdiv).show();
	};
</script>
<div class="clear"></div>
</div>
</div>
    </div>
<%@include file="footer.jsp"%>
</body>
</html>
