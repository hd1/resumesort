<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.wordnet.model.WordNetDataProviderPool"%>
<%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
	if (user == null)
	{
		return;
	}
	JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	//
	// Get selected phrases
	//
	boolean usestars = true;
	if ("false".equals(request.getParameter("usestars")))
		usestars = false;
	List<String> userSelectedPhrases = new ArrayList<String>();
	List<String> userEnteredPhrases = new ArrayList<String>();
	List<String> resumePhrases = new ArrayList<String>();
	List<String> templatePhrases = new ArrayList<String>();
	Map<String, Integer> phraseImportance = new TreeMap<String,Integer>();
	Set<String> tags = new HashSet<String>();
 	Map<String, String[]> paramMap = request.getParameterMap();
	List<UserTemplatePhrase> utps = new ArrayList<UserTemplatePhrase>();
	for (String param: paramMap.keySet())
	{
		UserTemplatePhrase utp = new UserTemplatePhrase();
		if (param.startsWith("suggphrases"))
		{
			String[] values = paramMap.get(param);
			String ind = param.substring(param.indexOf("_") + 1);

			if (request.getParameter("required_" + ind) == null
					&& getInt(request.getParameter("rating_" + ind)) == 0
					&& request.getParameter("tag_" + ind) == null)
					continue;
			userSelectedPhrases.add(values[0]);
			phraseImportance.put(values[0], getInt(request.getParameter("rating_" + ind)));

			if (request.getParameter("required_" + ind) != null && "true".equals(request.getParameter("required_" + ind)))
			{
				phraseImportance.put(values[0], 6);
				utp.setRequired(true);
			}
			else
			{
				if ("maybe1".equals(request.getParameter("required_" + ind)))
				{
					phraseImportance.put(values[0], 1);
				}
				else if ("maybe3".equals(request.getParameter("required_" + ind)))
				{
					phraseImportance.put(values[0], 3);
				}
			}

			if (request.getParameter("tag_" + ind) != null)
			{
				tags.add(values[0]);
				if (phraseImportance.get(values[0]) == 0)
					phraseImportance.put(values[0], 0);
				utp.setTag(true);
			}
			System.out.println("Selected phrase:" + values[0] + " rating:" + request.getParameter("rating_" + ind) + " required:" + request.getParameter("required_" + ind));
			utp.setPhrase(values[0].trim());
			utp.setScore(phraseImportance.get(values[0]));
			utps.add(utp);
		}
		else if (param.startsWith("tmplphrase"))
		{
			String[] values = paramMap.get(param);
			String ind = param.substring(param.indexOf("_") + 1);
			if (request.getParameter("required_" + ind) == null
					&& getInt(request.getParameter("rating_" + ind)) == 0
					&& request.getParameter("tag_" + ind) == null)
					continue;
			for (String value: values)
			{
				value = value.trim();
				if (value.length() > 0)
				{
					templatePhrases.add(value);
					//System.out.println("Template phrase:" + value);
					phraseImportance.put(value, getInt(request.getParameter("rating_" + ind)));
					if ("maybe3".equals(request.getParameter("required_" + ind)))
					{
						phraseImportance.put(value, 3);
					}
					else if ("maybe1".equals(request.getParameter("required_" + ind)))
					{
						phraseImportance.put(value, 1);
					}
					else if("true".equals(request.getParameter("required_" + ind)))
					{
						phraseImportance.put(value, 6);
						utp.setRequired(true);
					}
					if (request.getParameter("tag_" + ind) != null)
					{
						tags.add(value);
						utp.setTag(true);
					}
					//System.out.println("user Phrase:" + value.trim() + " Importance:" + request.getParameter("rating_" + ind));
					utp.setPhrase(value);
					utp.setScore(phraseImportance.get(values[0]));
					utps.add(utp);
				}
			}
		}
		else if (param.startsWith("userphrase"))
		{
			String[] values = paramMap.get(param);
			String ind = param.substring(param.indexOf("_") + 1);
			for (String value: values)
			{
				value = value.trim();
				if (value.length() > 0)
				{
					userEnteredPhrases.add(value);
					phraseImportance.put(value, getInt(request.getParameter("rating_" + ind)));
					if ("maybe3".equals(request.getParameter("required_" + ind)))
					{
						phraseImportance.put(value, 3);
					}
					else if ("maybe1".equals(request.getParameter("required_" + ind)))
					{
						phraseImportance.put(value, 1);
					}
					else if("true".equals(request.getParameter("required_" + ind)))
					{
						phraseImportance.put(value, 6);
						utp.setRequired(true);
					}
					if (request.getParameter("tag_" + ind) != null)
					{
						tags.add(value);
						utp.setTag(true);
					}
					//System.out.println("user Phrase:" + value.trim() + " Importance:" + request.getParameter("rating_" + ind));
					utp.setPhrase(value);
					utp.setScore(phraseImportance.get(values[0]));
					utps.add(utp);
				}
			}
		}
		else if (param.startsWith("resumephrase"))
		{
			String[] values = paramMap.get(param);
			String ind = param.substring(param.indexOf("_") + 1);
			String required = request.getParameter("required_" + ind);
			String tag = request.getParameter("tag_" + ind);
			if (request.getParameter("required_" + ind) == null
					&& getInt(request.getParameter("rating_" + ind)) == 0
					&& request.getParameter("tag_" + ind) == null)
					continue;
			for (String value: values)
			{
				value = value.trim();
				//System.out.println("Resume phrase:" + value + " rating:" + request.getParameter("rating_" + ind) + " required:" + required + " tag:" + tag);
				if (value.length() > 0)
				{
					resumePhrases.add(value);
					phraseImportance.put(value, getInt(request.getParameter("rating_" + ind)));
					if ("maybe1".equals(required))
					{
						phraseImportance.put(value, 1);
					}
					else if("maybe3".equals(required))
					{
						phraseImportance.put(value, 3);
					}
					else if("true".equals(required))
					{
						phraseImportance.put(value, 6);
						utp.setRequired(true);
					}
					if (tag != null && tag.equals("tag"))
					{
						tags.add(value);
						utp.setTag(true);
					}
					System.out.println("Resume Phrase:" + value.trim() + " Importance:" + request.getParameter("rating_" + ind));
					utp.setPhrase(value);
					utp.setScore(phraseImportance.get(values[0]));
					utps.add(utp);
				}
			}
		}
	}
	if ("true".equals(request.getParameter("save")))
	{
		try
		{
			boolean overwrite = "true".equals(request.getParameter("overwrite"));
			String templateName = request.getParameter("templateName");
			List<UserTemplate> templates =  new UserTemplate().getObjects("user_name=" + UserDBAccess.toSQL(user.getLogin()) + " and name ="  + UserDBAccess.toSQL(templateName));
			if (templates.size() > 0  && !overwrite)
				throw new Exception("Template already exists");
			if (utps.size() == 0)
				throw new Exception("No phrases selected");
			UserTemplate ut = new UserTemplate();
			if (templates.size() == 0)
			{
				ut.setUserName(user.getLogin());
				ut.setName(templateName);
				ut.setIndustry(posting.getIndustry());
				ut.insert();
			}
			else
			{
				ut = templates.get(0);
				new UserTemplatePhrase().deleteObjects("TEMPLATE_ID = " + ut.getId());
			}

			List<UserTemplatePhrase> tmplPhrases = new ArrayList<UserTemplatePhrase>();
			Set<String> uniquePhrases = new HashSet<String>();
			for (UserTemplatePhrase utp: utps)
			{
				if (utp.getPhrase() != null && utp.getPhrase().trim().length() > 0 && 
					!uniquePhrases.contains(utp.getPhrase().trim().toLowerCase()))
				{
					System.out.println("Inserting " +  utp.getPhrase());
					utp.setTemplateId(ut.getId());
					utp.insert();
					tmplPhrases.add(utp);
					uniquePhrases.add(utp.getPhrase().trim().toLowerCase());
				}
			}
			if (templateName.equals(posting.getTemplateName()))
			{
				session.setAttribute(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString(), tmplPhrases);
			}
		}
		catch (Exception x)
		{
			x.printStackTrace();
			throw x;
		}
	}

%>
	<b><!--input type=button onclick="clearPostingPhrases()" value="Clear selection"--></b>
	<div>
<%
	for(String rphrase: phraseImportance.keySet())
	{
%>
	<div class="kwdRecord">
	<div class="kwdTools">
<%
	String img = "";
	if (userEnteredPhrases.contains(rphrase))
		img = "<img src='tinyInfoU.gif'>";
	else if (resumePhrases.contains(rphrase))
		img = "<img src='tinyInfoRU.gif'>";
	else if (templatePhrases.contains(rphrase))
		img = "<img src='tinyInfoT.gif'>";
	else
		img = "<img src='tinyInfoJ.gif'>";
%>
	<%=img%>
	&nbsp;</div>
	<div class="kwdText" align=left>
		<%=rphrase%>
	</div>
<% 
	String check = "<img src='check.png'>";
	if (usestars) { %>
<div class="kwdRating" style="width:150px">
<%
	Integer rating = phraseImportance.get(rphrase);
	if (rating == null || rating == 6) rating = 0;
	int k = 0;
	for (k = 0; k < rating; k++)
	{
%>		<img src="star_on.gif"> <%
	}
	for (;k < 5; k++)
	{
%>		<img src="star_clear.gif"> <%
	}
%>
</div>
<div align=center class="kwdCbx">
<%=phraseImportance.get(rphrase) == 6?check:"&nbsp;"%>
</div>
<div align=center class="kwdCbx"><%=tags.contains(rphrase)?check:"&nbsp;"%></div>
<% } else { %>
<div align=center class="kwdCbx"><%=phraseImportance.get(rphrase) == 1?check:"&nbsp;"%></div>
<div align=center  class="kwdCbx"><%=phraseImportance.get(rphrase) == 3?check:"&nbsp;"%></div>
<div align=center  class="kwdCbx"><%=phraseImportance.get(rphrase) == 6?check:"&nbsp;"%></div>
<div align=center  class="kwdCbx"><%=tags.contains(rphrase)?check:"&nbsp;"%></div>
<div align=center class="kwdClear"></div>
<% } %>
</div>
<% 
} 
if (phraseImportance.keySet().size() == 0)
{
	out.print("No phrases selected");
}
%>
</div>



<%!
	int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>