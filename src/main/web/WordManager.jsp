<%@include file="header.lay"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>

<div id="wordlist">
<form method="post" action="WordManager"><input
	style="width: 372px" name="lookupword"> <input type="hidden"
	name="op" value="lookupword"> <input type="submit"
	value="Lookup Word"></form>

<%

	Word bean = (Word) session.getAttribute("lookupword");
	if (bean != null)
	{
		out.println("<h3>");
		out.println("Results for Word: ");
		out.println("<u>");
		out.println("<font color=\"red\">");
		out.println(bean.toString());
		out.println("</font>");
		out.println("</u>");
		out.println("</h3>");
		SynsetIterator it = Word.getSynsets(bean);
		Synset synset;
		while (it.hasNext())
		{
			synset = it.next();
			out.println("<font color=\"red\">");
			out.println("(");
			out.println(synset.getId());
			out.println(":");
			out.println(synset.getPos());
			out.println(")");
			out.println("</font>");
			out.println("  ");
			out.println("<b>");
			out.println(synset.getGloss());
			out.println("</b>");
			out.println("<br>");
			out.println("<form method=\"post\" action=\"SynsetManager\">");
			out.println("<input type=\"hidden\" name=\"op\" value=\"lookupsynset\">");
			out.print("<input type=\"hidden\" name=\"lookupsynset\" value=\"");
			out.print(synset.getId());
			out.println("\">");
			out.println("<input type=\"submit\" value=\"Lookup Synset\">");
			out.println("</form>");
			out.println("<form method=\"post\" action=\"WordManager\">");
			out.println("<input type=\"hidden\" name=\"op\" value=\"removesynset\">");
			out.print("<input type=\"hidden\" name=\"removesynset\" value=\"");
			out.print(synset.getId());
			out.println("\">");
			out.println("<input type=\"submit\" value=\"Remove from Synset\">");
			out.println("</form>");
			out.println("<br>");
			out.println("<br>");
		}

		it.close();
		out.println("<form method=\"post\" action=\"WordManager\">");
		out.println("<input type=\"hidden\" name=\"op\" value=\"addanothersynset\">");
		out.println("<input style=\"width: 372px\" name=\"addanothersynset\">");
		out.println("<input type=\"submit\" value=\"Add Another Synset\">");
		out.println("</form>");
		out.println("<br>");

		out.println("<form method=\"post\" action=\"WordManager\">");
		out.println("<input type=\"hidden\" name=\"op\" value=\"deleteword\">");
		out.println("<input type=\"submit\" value=\"Delete Word\">");
		out.println("</form>");
		out.println("<br>");
	}
%>
</div>
<hr>
<div>
<form method="post" action="WordManager"><input
	style="width: 372px" name="addword"> <input type="hidden"
	name="op" value="addword"> <input type="submit"
	value="Add New Word"></form>
</div>
<%@include file="footer.lay"%>