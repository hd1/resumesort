
<div class="footer">
      <div class="middle">
    <ul class="footerlinks">
          <li><a href="home.jsp">HOME</a></li>
          <li><a href="#">FEATURES</a></li>
          <li><a href="#">SUPPORT</a></li>
          <li><a href="http://blog.resumesort.com">BLOG</a></li>
          <li><a href="#">PRICING & SIGNUP</a></li>
        </ul>
    <div class="footerleft">
      <div style="clear:both; margin-left:5px; font-size:14px; font-weight:bold; ">2012 &copy; Textnomics Inc </div><br/>

          <ul>
        <!--li><a href="#">Site Map</a></li-->
        <!--li><a href="#">Legal</a></li-->
        <li><a href="Textnomics_Terms_of_Service.pdf" target=_blank>Terms & Policy</a></li>
        <li><a href="Contactus.jsp" target=_blank>Contact</a></li>
      </ul>
       <div style="clear:both; margin-left:5px; "> 
       Website design by: <a href="http://www.nikalabs.com/">Nika Labs</a>
       
       </div>
        <div class="likebox">
       <br/>
           Like Us: &nbsp;
           <!-- Place this tag where you want the +1 button to render. -->
            <div class="g-plusone" data-size="small" data-href="https://plus.google.com/110951603174450179619"></div>
            <!-- Place this tag after the last +1 button tag. -->
            <script type="text/javascript">
            (function() {
                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                po.src = 'https://apis.google.com/js/plusone.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
            </script>
            <a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.resumesort.com/" data-counturl="https://www.resumesort.com/">Tweet</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<%--            <a href="https://twitter.com/ResumeSort" class="twitter-share-button" data-size="small">Tweet</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
--%>
			<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
			<script type="IN/Share" data-url="https://www.linkedin.com/company/textnomics-inc-" data-counter="right"></script>
			<div class="fb-like" data-href="https://www.facebook.com/ResumeSort" data-send="false" data-layout="button_count" data-width="100" data-show-faces="true"></div>
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
        </div>
        </div>
    <div class="footerright">
         
          <div class="connectbox"> 
           connect &nbsp;
				<a href="https://www.facebook.com/ResumeSort"><img src="images/facebook.png" width="26" height="26" /></a>
                <a href="https://www.linkedin.com/company/textnomics-inc-"><img src="images/linkedin.png" width="26" height="26" /></a>
                <a href="https://plus.google.com/110951603174450179619"><img src="images/gplus.png" width="26" height="26" /></a>
                <a href="https://twitter.com/ResumeSort"><img src="images/twitter.png" width="26" height="26" />  </a>
                <!--a href="#"><img src="images/youtube.png" width="26" height="26" /></a>                
				<a href="#"><img src="images/rss.png" width="26" height="26" /></a-->
          </div>
  </div>
          
    </div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34492321-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>  