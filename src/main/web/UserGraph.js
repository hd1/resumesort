{
   "id": "565768400",
   "name": "Dev Gude",
   "first_name": "Dev",
   "last_name": "Gude",
   "link": "http://www.facebook.com/profile.php?id=565768400",
   "birthday": "09/07/1964",
   "education": [
      {
         "school": {
            "id": "108187865869206",
            "name": "University of Houston"
         },
         "concentration": [
            {
               "id": "124764794262413",
               "name": "Electrical Engineering"
            }
         ],
         "type": "Graduate School"
      },
      {
         "school": {
            "id": "105504499484518",
            "name": "Indian Institute of Technology Bombay"
         },
         "concentration": [
            {
               "id": "124764794262413",
               "name": "Electrical Engineering"
            }
         ],
         "type": "College"
      },
      {
         "school": {
            "id": "108114549221911",
            "name": "St. Vincent's High School, Pune, India"
         },
         "type": "High School"
      }
   ],
   "gender": "male",
   "relationship_status": "It's complicated",
   "political": "Very Liberal",
   "website": "http://www.sentforlife.com",
   "timezone": -7,
   "locale": "en_US",
   "verified": true,
   "updated_time": "2011-04-20T18:47:07+0000"
}