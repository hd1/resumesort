<%@ page import="java.util.*"%><%@ page import="java.io.*"%><%@ page import="com.textonomics.*"%><%@ page import="com.textonomics.web.*"%><%@ page import="com.textonomics.user.*"%><%@ page import="com.textonomics.user.dataprovider.*"%><%@ page import="com.textonomics.mail.*"%><%@ page import="com.textonomics.db.*"%><%@ page import="com.textnomics.data.*"%><%@ page import="com.textonomics.subscription.*"%>
<%
		String[] emails = request.getParameterValues("shareemail");
		System.out.println("share_job_posting: num emails =" + emails.length);
		if (emails == null || emails.length == 0) return;
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (user == null) 
		{
%>
Error: Your session has expired.
<%
			return;
		}
		String postingId = (String)	session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString());
		List<File> resumeFiles = (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
		if (posting == null)
		{
%>
Error: Job Posting: <%=postingId%> not found.
<%
	   }
	   if (!posting.isPaid())
	   {
           Integer freePostings = user.getFreePostings();
           if (freePostings == null) freePostings = 0;
           boolean validSubscription = false;
           if (freePostings > 0)
           {
               validSubscription = true;
               freePostings--;
               user.setFreePostings(freePostings);
               UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();
               userDataProvider.updateUserField(user, "freePostings");
               userDataProvider.commit();
               UserDataProviderPool.getInstance().releaseProvider();
           }
           if (!validSubscription)
                validSubscription = SubscriptionService.isValidSubscription(user, posting, resumeFiles.size(), true);
           if (validSubscription)
           {
               posting.setPaid(true);
               int runs = posting.getNumRuns();
               posting.setNumRuns(runs+1);
               posting.update();
           }
	   }
	   int sent = posting.getNumInvited();
	   sent = sent + emails.length;
	   if (sent > posting.MAX_INVITEES)
	   {
		   out.print("Error: Maximum " + posting.MAX_INVITEES + " friend requests allowed");
		   return;
	   }
	   posting.setNumInvited(sent);
	   posting.update();
		try
		{
			UserDataProvider userProvider = UserDataProviderPool.getInstance().acquireProvider();
			for (int i = 0; i < emails.length; i++)
			{
				if (emails[i].indexOf("@") == -1) continue;
				String email = emails[i].trim();
				User shareUser = userProvider.getUser(email);
				boolean created = false;
				if (shareUser == null)
				{
					created = true;
					shareUser = new User();
					shareUser.setEmail(email);
					shareUser.setLogin(email);
					shareUser.setName(email);
					shareUser.setCompany(user.getCompany());
					shareUser.setParticipationCode("Invited By " + user.getLogin());
					shareUser.setPassword(SendPassword.generatePassword());
					shareUser.setState(1);
					userProvider.insertUser(shareUser);
					userProvider.updateUserField(shareUser, "company");
					userProvider.commit();
				}
				String subject = "Resume Evaluation Invitation from Resume Sort";
				StringBuffer bodyBuf = new StringBuffer("You have been invited by " + user.getName() + " to evaluate and analyze resumes received for Job Posting " + postingId + "<br><br>\n");
				bodyBuf.append("Please log in to Resume Sort using the link below to perform the analysis:<br><br>\n");
				bodyBuf.append("<a href='" + request.getRequestURL().toString().replace("share_job_posting.jsp","") + "ResumeSocial?postingId=" + postingId + "&inviter=" + user.getLogin() + "&login=" + shareUser.getLogin() +"'>Resume Sort - Advanced Resume Analysis</a><br><br>\n");
				if (created)
				{
					bodyBuf.append("Your username for the site: " + shareUser.getLogin() + "<br>\n");
					bodyBuf.append("Your password for the site: " + shareUser.getPassword() + "<br><br>\n");
				}
				bodyBuf.append("Thank you for using Resume Sort<br><br>\n");
				SharedJobPosting sjp = new SharedJobPosting();
				List<SharedJobPosting> oldSjps =  new SharedJobPosting().getObjects("JOB_ID=" + posting.getId() + " and IDUSER = " + shareUser.getId());
				if (oldSjps.size() > 0)
					sjp = oldSjps.get(0);
				sjp.setJobId(posting.getId());
				sjp.setUserId(shareUser.getId());
				sjp.setPerm1(false);
				sjp.setPerm2(false);
				sjp.setPerm3(false);
				if ("1".equals(request.getParameter("perm1_"+i)))
					sjp.setPerm1(true);
				if ("1".equals(request.getParameter("perm2_"+i)))
					sjp.setPerm2(true);
				if ("1".equals(request.getParameter("perm3_"+i)))
					sjp.setPerm3(true);
				if (sjp.getId() > 0)
					sjp.update();
				else
					sjp.insert();
				GenericMailer.getInstance().sendMail(email, subject, bodyBuf.toString());
			}
		}
		catch (Exception x)
		{
			x.printStackTrace();
		}
		finally {
			UserDataProviderPool.getInstance().releaseProvider();
		}
%>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>