<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<style>
.utable {
font-size:12px;
font-weight:normal;
padding:4px;
border-style:solid;
border-width:1px;
border-color:black;
border-collapse:collapse;
}
.utable td {
border-width:1px;
border-color:black;
border-style:solid;
padding:4px;
}
</style>
<%

	if (user == null)
	{
		response.sendRedirect("");
		return;		
	}
	String email = user.getEmail().toLowerCase();
	if (email.indexOf("gude") == -1 && email.indexOf("bandari") == -1)
	{
%>
		<script>alert("No permissions for:<%=email%>")</script>
<%
		return;
	}
	String direc = request.getParameter("direc");
	if (direc == null) direc = "asc";
	
	UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();
	User userToModify = null;
	if (request.getParameter("iduser") != null)
	{
		int iduser = Integer.parseInt(request.getParameter("iduser"));
		userToModify = User.getUser(iduser);
	}
	String sortBy = request.getParameter("sortBy");
	if (sortBy == null) sortBy = "NAME";
	UserIterator users = provider.getUsers(sortBy + " " + direc);
	if (direc.equals("asc")) 
		direc = "desc";
	else
		direc = "asc";
%>
<script>
	function deleteuser(name, id)
	{
		if (!confirm("Are you sure you want to delete user " + name))
			return -1;
		window.location = "UserManager?delete=&iduser=" + id;
	}
	function loginAs(name, email)
	{
		if (!confirm("Are you sure you want to login as user " + name))
			return -1;
		window.location = "SwitchUser.jsp?email=" + email;
	}

</script>
<div class=central>
<form method="post" action="UserManager">
<br>
<table border="1" class=utable>
	<tr>
		<td bgcolor=lightgrey><b>Name:</b></td>
		<td><input type="text" size="200" name="name"
			style="width: 500px"
			value="<%=userToModify != null ? userToModify.getName() : ""%>" maxlength="200"></td>
	</tr>
	<tr>
		<td bgcolor=lightgrey><b>Email:</b></td>
		<td><input type="text" size="150" name="email"
			style="width: 500px"
			value="<%=userToModify != null ? userToModify.getEmail() : ""%>" maxlength="150"></td>
	</tr>
	<tr>
		<td bgcolor=lightgrey><b>Login:</b></td>
		<td><input type="text" size="50" name="login"
			value="<%=userToModify != null ? userToModify.getLogin() : ""%>" maxlength="50"></td>
	</tr>
	<tr>
		<td bgcolor=lightgrey><b>Password:</b></td>
		<td><input type="text" size="20" name="password"
			value="<%=userToModify != null ? userToModify.getPassword() : ""%>" maxlength="20"></td>
	</tr>
	<tr>
		<td bgcolor=lightgrey><b>State:</b></td>
		<td><input type="text" size="20" name="state"
			value="<%=userToModify != null ? userToModify.getState() : ""%>"></td>
	</tr>
	<tr>
		<td bgcolor=lightgrey><b>Admin:</b></td>
		<td><input type="text" size="20" name="state"
			value="<%=userToModify != null ? userToModify.isAdmin() : ""%>"></td>
	</tr>
	<tr>
		<td bgcolor=lightgrey><b>Free Postings:</b></td>
		<td><input type="text" size="20" name="freePostings"
			value="<%=userToModify != null ? userToModify.getFreePostings() : ""%>"></td>
	</tr>
</table>

<input type="hidden" size="10" name="iduser"
	value="<%=userToModify != null ? userToModify.getId() : ""%>">
<br><INPUT TYPE='submit' VALUE='Update'>
<% if (userToModify != null) { %>
&nbsp;&nbsp;&nbsp;<input type=button value="Login as <%=userToModify.getName()%>" onclick='loginAs("<%=userToModify.getName()%>", "<%=userToModify.getEmail()%>")'>
<% } %>

</form>
<hr>
<%
	out.print("<a href=\"" + response.encodeRedirectURL("UserManager.jsp") + "\">");
	out.print("Clear the Form");
	out.print("</a>");
	out.println("<br>");
	out.println("<br>");
%>
<table border=1 class=utable>
<tr bgcolor=lightgrey>
<td>&nbsp;</td>
<td align=center><b><a href='UserManager.jsp?sortBy=NAME&direc=<%=direc%>'>Name</a></b></a></td>
<td align=center><b><a href='UserManager.jsp?sortBy=EMAIL&direc=<%=direc%>'>Email</a></b></td>
<td align=center><b><a href='UserManager.jsp?sortBy=STATE&direc=<%=direc%>'>Status</a></b></td>
<td align=center><b><a href='UserManager.jsp?sortBy=STAMP&direc=<%=direc%>'>Created</a></b></td>
<td align=center><b>Delete</b></td>
<td align=center><b><a href='UserManager.jsp?sortBy=LAST_LOGIN&direc=<%=direc%>'>Last Login</a></b></td>
<td align=center><b><a href='UserManager.jsp?sortBy=NUM_PROCESSED&direc=<%=direc%>'>Resumes Processed</a></b></td>
<td align=center><b><a href='UserManager.jsp?sortBy=NUM_EMAILED&direc=<%=direc%>'>Resumes Emailed</a></b></td>
<td align=center><b><a href='UserManager.jsp?sortBy=PARTICIPATION_CODE&direc=<%=direc%>'>Code</a></b></td>
<td align=center width=200><b>Company</b></td>
<td align=center width=200><b>Industry</b></td>
</tr>
<%
	User current;
	int i = 0;
	while (users.hasNext())
	{
		try
		{
			current = users.next();
		}
		catch (Exception x)
		{
			x.printStackTrace();
			i++;
			continue;
		}
		System.out.println("User:" + current.getName());
		i++;
		String industry = checkNull(current.getIndustry(),"").replace("\n","; ");
		String indshort = industry;
		if (indshort.length() > 20) 
			indshort = indshort.substring(0,17) + "...";
%>
<tr>
<td><%=i%></td>
<td nowrap><a href="UserManager.jsp?iduser=<%=current.getId()%>" ><%=current.getName()%></a></td>
<td nowrap><%=current.getEmail()%></td>
<td align=center><%=current.getState()%></td>
<td nowrap><%=checkNull(current.getCreationDate(),"")%></td>
<td><a href="#" onclick="deleteuser('<%=current.getName()%>', <%=current.getId()%>)">delete</a></td>
<td nowrap><%=checkNull(current.getLastLogin(),"")%></td>
<td align=center><a href="user_files.jsp?iduser=<%=current.getId()%>"><%=checkNull(current.getNumProcessed(),"")%></a></td>
<td align=center><%=checkNull(current.getNumEmailed(),"")%></td>
<td align=center><%=checkNull(current.getParticipationCode(),"")%></td>
<td align=center><%=checkNull(current.getCompany(),"")%></td>
<td align=left nowrap><span title="<%=industry%>"><%=indshort%></span></td>
</tr>
<%
	}

	users.close();
	UserDataProviderPool.getInstance().releaseProvider();
%>
</table>
</div>
<%@include file="footer.jsp"%>
  
</body>
</html>
<%!
	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
	String checkNull(Object value, String defval)
	{
		if (value == null)
			return defval;
		else if (value instanceof Date)
			return dateformat.format(value);
		return value.toString();
	}
%>