<%@ page import="java.util.*"%><%@ page import="java.io.*"%><%@ page import="java.text.*"%><%@ page import="com.textonomics.*"%><%@ page import="com.textonomics.web.*"%><%@ page import="com.textonomics.user.*"%><%@ page import="com.textonomics.user.*"%><%@ page import="com.textnomics.data.*"%><%@ page import="com.textonomics.db.*"%><%
		String comment = request.getParameter("comment");
		System.out.println("save_comment: " + comment);
		List<File> resumeFiles = (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (user == null || resumeFiles == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		int resumeIndex = getInt(request.getParameter("resumeIndex"));
		String fileName = resumeFiles.get(resumeIndex).getName();
		if (fileName.indexOf("_") != -1)
			fileName = fileName.substring(fileName.indexOf("_") + 1);
		if (fileName.lastIndexOf("/") != -1)
			fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
		String sql = "resume_file = " + DBAccess.toSQL(fileName) + " and user_name = " +  DBAccess.toSQL(user.getLogin());
		File resumeFolder = (File) session.getAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString());
		String resumeFolderName = null;
		if (resumeFolder != null)
		{
			resumeFolderName = resumeFolder.getName();
			sql = sql + " and (resume_folder = " + DBAccess.toSQL(resumeFolderName) + " or resume_folder is null)";
		}
 		System.out.println("------------- Comments for " + fileName);
		List<ResumeComments> history = new ResumeComments().getObjects(sql);
		ResumeComments resComm = new ResumeComments();
		if (history.size() > 0)
		{
			resComm = history.get(0);
			resComm.setResumeFolder(resumeFolderName);
		}
		else
		{
			resComm.setUserName(user.getLogin());
			resComm.setResumeFile(fileName);
			resComm.setResumeFolder(resumeFolderName);
		}
		String text = resComm.getComments();
		if (comment != null && comment.trim().length() > 0)
		{
			if (text == null) text = "";
			text = dateformat.format(new Date()) + "-" + user.getName() + ":" + "\n" + comment + "\n" + text;
			resComm.setComments(text);
			if (resComm.getId() == 0)
				resComm.insert();
			else
				resComm.update();
		}
		out.print(text);
%><%!
	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>