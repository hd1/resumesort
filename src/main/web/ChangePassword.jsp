<%@include file="header.jsp"%>
<%@ page import="java.security.MessageDigest"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.dataprovider.*"%>

<script type="text/javascript">
	
	function  doSubmit() 
	{		
		if (isValid())
		{
			document.changepw.command.value ='save';
			document.changepw.submit();
		}		
	}

	function  isValid()
	{				
		if (document.changepw.passwordconfirm.value!=document.changepw.password.value)
		{
			alert("Your passwords do not match!");
			return false;
		}

		var password = document.changepw.password.value;
		password = trim(password);
		if (password=="")
		{
			alert("Please enter your password.");
			return false;
		}
		if (password.length < 5)
		{
			alert("Password should be at least 5 characters long.");
			return false;
		}
		
		return true;
	}
	function trim(str) 
	{
		return str.replace(/^\s+|\s+$/g,"");
	}
</script>

<div class="central">
<br>
<br>

<%
	final String INVOLDPASS = "Invalid Old Password";
	if (user == null)
	{
		response.sendRedirect("");
		return;
	}
	System.out.println("Change password command:" + request.getParameter("command"));
	if ("save".equals(request.getParameter("command")))
	{
		System.out.println("Changing password");
		UserDataProvider userProvider = null;

		String password = request.getParameter("password").trim();
		String oldpassword = request.getParameter("oldpassword").trim();
		String encryptedPW = password;
		String encryptedOldPW2 = password;
		String encryptedOldPW = password;
		sun.misc.BASE64Encoder baseEncoder = new  sun.misc.BASE64Encoder();
		try
		{
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.reset();
			md.update((password+user.getEmail()).getBytes());
			byte[] b = md.digest();
			encryptedPW = new String( baseEncoder.encode(b) );
			md.reset();
			md.update(oldpassword.getBytes());
			b = md.digest();
			encryptedOldPW = new String( baseEncoder.encode(b) );
			md.reset();
			md.update((oldpassword+user.getEmail()).getBytes());
			b = md.digest();
			encryptedOldPW2 = new String( baseEncoder.encode(b) );
			if (!user.getPassword().equals(encryptedOldPW2) &&!user.getPassword().equals(encryptedOldPW) && !user.getPassword().equals(oldpassword))
			{
				throw new Exception(INVOLDPASS);
			}
			userProvider = UserDataProviderPool.getInstance().acquireProvider();
			user.setPassword(encryptedPW); // save encrypted password
			userProvider.updateUser(user);
			userProvider.commit();
%>
<h1>Your password has been changed</h1><br><br>
<%
		}
		catch (Exception x)
		{
			x.printStackTrace();
			if (x.getMessage().equals(INVOLDPASS))
			{
%>
	<script>alert("Invalid Old Password")</script>
<%
			}
		}
		finally
		{
			if (userProvider != null)
				UserDataProviderPool.getInstance().releaseProvider();
		}
	}
	else
	{
%> 
<br>
<br>

<h1>Change your password:</h1>
<br>
<br>
<form method="post" action="" name="changepw">
<table border="0" cellpadding="0" cellspacing="0" width="60%">
	<tr>
		<td><span class="orange">Name:</span></td>
		<td><%=user.getName()%></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Old Password:</span></td>
		<td><input type="password" size="25" name="oldpassword" value=""
			maxlength="20"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">New Password:</span></td>
		<td><input type="password" size="25" name="password" value=""
			maxlength="20"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Confirm Password:</span></td>
		<td><input type="password" size="25" name="passwordconfirm"
			value="" maxlength="20"> <span class="green">
		</span></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan=2 align=center><a href="javascript:doSubmit()" class="orangeBtn">Save</a></td>
	</tr>
</table>
<input type='hidden' name='command' value=''>
<br/>
<br/>
</form>
<%
	}
%> 
</div>
<%@include file="footer.jsp"%>
  
</body>
</html>