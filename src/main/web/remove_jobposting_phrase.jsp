<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%@ page import="com.textnomics.data.*"%>
<%
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		System.out.println("Entered Remove jobposting phrase, delete=" + request.getParameter("delete"));
		Map<String,Integer> resumePhrases = (Map<String,Integer>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
		boolean isAdmin = false;
		if (user.isAdmin() || user.isTester()) isAdmin = true;
		if (resumePhrases == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		String phrase = request.getParameter("phrase");
		if (phrase == null) return;
		if ("false".equals(request.getParameter("delete")))
		{
			System.out.println("Undelete job posting phrase:" + phrase);
			new DeletedPhrase().deleteObjects("phrase=" + DeletedPhrase.toSQL(phrase.trim()));
			System.out.println("Undelete job posting phrase:" + phrase);
			new UserDeletedPhrase().deleteObjects("phrase=" + DeletedPhrase.toSQL(phrase.trim()) + " and user_name=" + DeletedPhrase.toSQL(user.getLogin()));
		}
		else if (isAdmin)
		{
			System.out.println("Removing phrase:" + phrase);
			resumePhrases.remove(phrase);
			DeletedPhrase deleted = new DeletedPhrase();
			deleted.setPhrase(phrase.trim());
			deleted.setUserName(user.getLogin());
			deleted.insert();
		}
		else
		{
			resumePhrases.remove(phrase);
			System.out.println("Removing  phrase:" + phrase);
			UserDeletedPhrase deleted = new UserDeletedPhrase();
			deleted.setPhrase(phrase.trim());
			deleted.setUserName(user.getLogin());
			deleted.insert();
		}
		return;
%>