<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textnomics.data.*"%>

<script type="text/javascript">

function update_user_box(){
	var user_box = document.getElementById("user");
	// add in some XFBML. note that we set useyou=false so it doesn't display "you"
        //This is to use full multi-friend connector
        user_box.innerHTML = "<span>"
            + "<fb:profile-pic uid='loggedinuser' facebook-logo='true'></fb:profile-pic>"
            + "Welcome, <fb:name uid='loggedinuser' useyou='false'></fb:name>. You are signed in with your Facebook account."
            + "<fb:serverfbml style=\"width: 755px;\">  <script type=\"text/fbml\">"
            + "<fb:request-form action=\"http://www.resumetuner.com\" method=\"POST\" invite=\"true\" type=\"ResumeTuner\" content=\"Hello, discover this brand new application! <fb:req-choice url='http://www.resumetuner.com' label='Start Using ResumeTuner!' />  \" >"
            + "<fb:multi-friend-selector actiontext=\"Select the friends you want to invite. (All of them.)\""
            + "rows=\"3\" cols=\"3\"/>"
            + "</fb:request-form>"
            + "<\/script>  <\/fb:serverfbml>"
            + "</span>";

          //This is to use condensed multi-friend connector
          //user_box.innerHTML = "<span>"
            //+ "<fb:profile-pic uid='loggedinuser' facebook-logo='true'></fb:profile-pic>"
            //+ "Welcome, <fb:name uid='loggedinuser' useyou='false'></fb:name>. You are signed in with your Facebook account."
            //+ "<fb:serverfbml style=\"width: 700px;\">  <script type=\"text/fbml\">"
            //+"<fb:request-form method=\"post\" action=\"http://www.resumetuner.com\" content=\"Hello, discover this brand new application! <fb:req-choice url='http://www.resumetuner.com' label='Start Using ResumeTuner!' />\" type=\"ResumeTuner\" invite=\"true\">"
            //+"<div class=\"clearfix\" style=\"padding-bottom: 10px;\">"
            //+"<fb:multi-friend-selector condensed=\"true\" style=\"width: 200px;\" />"
            //+"</div>"
            //+"<fb:request-form-submit />"
            //+"</fb:request-form>"
            //+ "<\/script>  <\/fb:serverfbml>"
            //+ "</span>";

	// because this is XFBML, we need to tell Facebook to re-process the document
    FB.XFBML.Host.parseDomTree();
}
</script>

<div class="central">
 
<br /><br /><br />
<%
	JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	List<File> resumeFiles = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
%>
<font size=+1>Thank you for selecting phrases for evaluation of the resumes. 
<% if (resumeFiles != null && resumeFiles.size() > 0) { %>
The processing of the resumes has been completed.
<% } %>
<% if (posting != null && posting.getEmailAddress() != null && posting.getEmailAddress().length() > 0) { %>
 <br/>
 <br/>
Resumes may be mailed to <b><a href='mailto:<%=posting.getEmailAddress()%>@resumesort.com'><%=posting.getEmailAddress()%>@resumesort.com</a></b> for processing and evaluation. <br/>
<% } %>
   <br /><br />
<br>
<br>
<br>

<center>
<br />
<br />
<!-- ADDTHIS BUTTON BEGIN -->
<script type="text/javascript"> 
var addthis_pub = "ebbanari ";
</script> 
 
<a href="http://www.addthis.com/bookmark.php?v=20" 
    onmouseover="return addthis_open(this, '', 'http%3A%2F%2Fwww.resumetuner.com', 'Optimize your resume with ResumeTuner - the most advanced resume tuner on the planet.');" 
    onmouseout="addthis_close();" 
    onclick="return addthis_sendto();"><img 
    src="http://s7.addthis.com/static/btn/lg-share-en.gif" 
    width="125" height="16" border="0" alt="Share" /></a> 
 
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script> 
 
<!-- ADDTHIS BUTTON END --> 
<!-- ADD TO ANY BUTTON BEGIN -->
	<a class="a2a_dd" 
	href="http://www.addtoany.com/share_save?linkname=ResumeTuner&amp;linkurl=http%3A%2F%2Fwww.resumetuner.com">
	<img src="http://static.addtoany.com/buttons/share_save_171_16.png" width="171" height="16" border="0" 
	alt="Share/Save/Bookmark"/></a>
	<script type="text/javascript">
	var a2a_linkname="ResumeTuner";
	var a2a_linkurl="http://www.resumetuner.com";
	var a2a_num_services=12;
	var a2a_config = a2a_config || {};
	a2a_config.templates = {
		twitter: "Optimize your resume with at ${link} - the most advanced resume tuner on the planet."
	}	a2a_prioritize=["twitter","facebook","linkedin","myspace","bebo","google_bookmarks","blogger_post","digg","delicious","stumbleupon","newsvine","yahoo_mail"];
	</script>
	<script type="text/javascript" 
	src="http://static.addtoany.com/menu/page.js">
	</script>
<!--  ADD TO ANY BUTTON END -->
		
		<br />
		<script src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php" type="text/javascript">
		</script>

		
		<!--  <div style="margin-top: 33px;"> -->
		<!-- Include the Google Friend Connect javascript library. -->
		<!--<script type="text/javascript"
			src="http://www.google.com/friendconnect/script/friendconnect.js"></script> -->
		<!-- Define the div tag where the gadget will be inserted. -->
		<!-- <div id="div-7175201989134265598"
			style="width: 282px; border: 1px solid #FFFFFF"></div> -->
		<!-- Render the gadget into a div.
		 <script type="text/javascript">
		var skin = {};
		skin['BORDER_COLOR'] = '#FFFFFF;';
		skin['ENDCAP_BG_COLOR'] = '#FFFFFF;';
		skin['ENDCAP_TEXT_COLOR'] = '#333333';
		skin['ENDCAP_LINK_COLOR'] = '#0000cc';
		skin['ALTERNATE_BG_COLOR'] = '#ffffff';
		skin['CONTENT_BG_COLOR'] = '#ffffff';
		skin['CONTENT_LINK_COLOR'] = '#0000cc';
		skin['CONTENT_TEXT_COLOR'] = '#333333';
		skin['CONTENT_SECONDARY_LINK_COLOR'] = '#7777cc';
		skin['CONTENT_SECONDARY_TEXT_COLOR'] = '#666666';
		skin['CONTENT_HEADLINE_COLOR'] = '#333333';
		skin['DEFAULT_COMMENT_TEXT'] = '- add your review here -';
		skin['HEADER_TEXT'] = 'Please Rate Cake Walk';
		skin['POSTS_PER_PAGE'] = '5';
		google.friendconnect.container
				.setParentUrl('/ResumeTuner/' /* location of rpc_relay.html and canvas.html */);
		google.friendconnect.container.renderReviewGadget( {
			id : 'div-7175201989134265598',
			site : '05593951509473682919',
			'view-params' : {
				"disableMinMax" : "false",
				"scope" : "SITE",
				"startMaximized" : "true"
			}
		}, skin);
		</script>
		</div> -->

<br />
<br />		
<% 
	if (user == null) {
%>
		<div id="user" align="center" ">
		<fb:login-button onlogin="update_user_box();"></fb:login-button>
		</div>				
<%	} %>
<br />
<br />		

<script type="text/javascript"> FB.init("42b3d947b7eaa06cb5e21465f51d1e6d", "xd_receiver.htm",{"ifUserConnected" : update_user_box});
</script>
</center>
</div>
<%@include file="footer.jsp"%>
  
</body>
</html>