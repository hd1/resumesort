<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%
		String phrase = request.getParameter("phrase");
		System.out.println("get_synonyms: phrase=" + phrase);
		if (phrase == null) return;
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		String userName = user.getLogin();
		Map<String,String> phrasesSynonyms = (Map<String,String>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());
		Map<String,Set<String>> deletedSynonyms = (Map<String,Set<String>>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_DELETED_SYNONYMS.toString());
		Object scores = session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
		if (phrasesSynonyms == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		if (deletedSynonyms == null)
		{
			deletedSynonyms = new HashMap<String,Set<String>>();
			session.setAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_DELETED_SYNONYMS.toString(),deletedSynonyms);
		}
		String synonymList = phrasesSynonyms.get(phrase.trim());
		if (synonymList == null)
		{
			synonymList = ResumeSorter.getWikiSynonyms(user, phrase);
			System.out.println("Synonyms for " + phrase + ":" + synonymList);
			phrasesSynonyms.put(phrase.trim(), synonymList);
		}
		else
			System.out.println("Cached Synonyms for " + phrase + ":" + synonymList);
		String[] synonyms = new String[0];
		Set<String> deleted = deletedSynonyms.get(phrase.trim());
		if (phrasesSynonyms.get(phrase.trim()) != null)
		{
			String synonymsStr = phrasesSynonyms.get(phrase.trim()).trim();
			if (synonymsStr.length() > 1)
			{
				synonyms = synonymsStr.split("\\|");
			}
		}
		String title = "Equivalent phrases for " + phrase;
		if (synonyms.length == 0 && scores == null) 
			title = " No equivalent phrases found for " + phrase + "<br>Please add your own";
		String note = "(These phrases will also be matched)";
		if (scores != null) note = "(These phrases have also been matched)";
		if (synonyms.length == 0 && scores != null) 
		{
			title = " There are no equivalent phrases for " + phrase;
			note = "";
		}
		// Sort synonyms
		//for (int i = 0; i < synonyms.length; i++)
		//{
		//	for (int j = i; j < synonyms.length; j++)
		//	{
		//		if (synonyms[i].compareTo(synonyms[j]) > 0)
		//		{
		//			String temp = synonyms[i];
		//			synonyms[i] = synonyms[j];
		//			synonyms[j] = temp;
		//		}
		//	}
		//}
%>
<img id="synDel2" onclick="tthide();stthide();" class="close">
<table border=1 width="300" id="tttable" cellpaddng=2 cellspacing=0 class="ttipBody"><TBODY id="ttBody">
<input type=hidden id=synonymList name=synonymList value="<%=synonymList%>">
<tr>
<td align=center style="font-weight:bold"><b><%=title%><br><%=note%></b></td>
<td nowrap valign=middle>
</td>
</tr>
<tr>
<td colspan=100%><hr></td>
</tr>
<%
	for (int i = 0; i < synonyms.length; i++)
	{
		String img = "x.png";
		String checked = "checked";
		String style ="style='text-decoration:none'";
		if ((deleted != null && deleted.contains(synonyms[i])) || RejectedSynonym.isRejected(userName, phrase.trim(), synonyms[i]))
		{
			img = "check.png";
			style ="style='text-decoration:line-through'";
			checked = "";
		}
		if (synonyms[i] == null || synonyms[i].length() == 0) continue;
%>
	<tr id="syntr<%=i%>" <%=style%> onMouseOver="this.style.backgroundColor='#CCCCCC'" onMouseOut="this.style.backgroundColor=''">
	<td><%=synonyms[i]%></td>
	<td nowrap>
	<input type=checkbox id="synDel2_<%=i%>" <%=checked%> title='Remove Invalid Synonym' onclick="deleteSynonym('<%=phrase%>',<%=i+1%>)">
	<!--img id="synDel2_<%=i%>" src="<%=img%>" title='Remove Invalid Synonym' onclick="deleteSynonym('<%=phrase%>',<%=i+1%>)"-->
	</td></tr>
<%
	}
	if (scores == null) { //analysis/scoring not yet done
%>
	<tr>
	<td><input id="AddSynonym" type="text" name="AddSynonym" size="30" onkeypress="return checkAddenter(event)"></td>
	<td nowrap>
	<img  src="plus.png" title='Add Synonym' onclick="addSynonym('<%=phrase%>')">
	</td></tr>
	<tr>
<%	} %>
	<td colspan=100% align=center style="font-weight:bold"><br><input type=button value="Clear All" title='Remove All Synonym' onclick="deleteSynonym('<%=phrase%>',0)"><br>&nbsp;</td>
	</tr>
	</TBODY></table>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>