<%@include file="header.lay"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="java.io.*" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.textonomics.PlatformProperties" %>

<%! private String name; %>
<%! private String resume; %>
<%! private String[] fileNames; %>
<%! private String fullPath; %>
<%! private	String jobType; %>
<%! private	String sourceFileName; %>
<%! private	String cvFileName; %>
<%! private	String finalDocumentFileName; %>
<%! private	String tagFileName; %>
<%! private	String modificationsFileName; %>
<%! private	String feedbackFileName; %>
<%! private	String debugFileName; %>
<%! private	String htmlFileName; %>
<%@page import="java.util.Date"%>

<script type="text/javascript">
    function changeUser()
    {
        obj = document.getElementById("selectUser").value;
        window.location = 'Analyser.jsp?name=' + obj;
    }

    function changeResume(name,resume){
        //obj = document.getElementById("selectResume").value;
        window.location = 'Analyser.jsp?name=' + name+"&resume="+resume;		
    }
    
</script>

<%!
public String countFeedbackErrors(String submissionId,String user,String absolutePath){
	StringBuffer strb=new StringBuffer();
	
	File file = new File(absolutePath+submissionId+"_tag.txt");
	
    BufferedReader reader = null;

    try
    {
        reader = new BufferedReader(new FileReader(file));
        String text = null;
        //Ignore first line because contains the counter
        text = reader.readLine();
        strb.append("<font color=\"blue\">"+text+" errors"+"</font>");
        
        reader.close();
        
    }catch (FileNotFoundException e){
    	//e.printStackTrace();
    }catch (IOException e)
    {
        //e.printStackTrace();
    }
    
	file = new File(absolutePath+submissionId+"_feedback.txt");
	
    reader = null;

    try
    {
        reader = new BufferedReader(new FileReader(file));
        String text = null;
        //Ignore first line because contains the counter
        text = reader.readLine();
        
        strb.append(", "+"<font color=\"red\">"+text+" feedbacks"+"</font>");
        
        reader.close();
        
    }catch (FileNotFoundException e){
    	//e.printStackTrace();
    }catch (IOException e)
    {
        //e.printStackTrace();
    }

	return strb.toString();	
}


//Bubble sort
public static void bubbleSort(ArrayList<String> a) {
  for (int i = 0; i < a.size()-1; i++) {
     for (int j = 0; j < a.size()-1; j++) {
        if (Long.parseLong(a.get(j)) > Long.parseLong(a.get(j+1))) {
           swap(a, j, j+1);
        }
     }
  }
  
  
}

private static void swap(ArrayList<String> a, int i, int j) {
  String temp = a.get(i);
  a.set(i,a.get(j));
  a.set(j,temp);
}

//Verify if submission is checked
private boolean isCheckedSubmission(String submissionID,HttpSession session){
	Hashtable<String,String> checked=(Hashtable<String,String>)session.getAttribute("checked");
		
	if(checked.containsKey(submissionID))
		return true;
	else
		return false;

}

//Load checked hashtable to memory
public void loadChecked(String fullPath,HttpSession session){
	Hashtable<String,String> checked=(Hashtable<String,String>)session.getAttribute("checked");
	if(checked==null){
		checked=new Hashtable<String,String>();
	}
	
	boolean exists=false;
	File file = new File(fullPath+"IS_CHECKED.txt");
	//System.out.println("*****************************"+file.getAbsolutePath());
	try{
		if(!file.exists()){
			file.createNewFile();
		}
		
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String text;
		while((text= buffer.readLine())!=null){
			checked.put(text,"");	
		}
		
		buffer.close();
		
	}catch(IOException ex){
		ex.printStackTrace();
	}
	
	session.setAttribute("checked",checked);
	
}

%>

<%

	try {
	    name = "";
	    name = request.getParameter("name");
	    //num = Integer.parseInt(request.getParameter("id").trim());
	
	} catch (Exception e) {
	    //e.printStackTrace();
	}
	
	try{
		resume = "";
		resume = request.getParameter("resume");
		
	}catch (Exception e){
		//e.printStackTrace();
	}

	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());

	if (user == null)
	{
		response.sendRedirect("");		
		return;		
	}

	
	UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();
	User userToModify = null;
	if (request.getParameter("iduser") != null)
	{
		int iduser = Integer.parseInt(request.getParameter("iduser"));
		userToModify = User.getUser(iduser);
		
	}
	
	String path = "uploads" + File.separator;

	File dir = PlatformProperties.getInstance().getResourceFile(path);

	String[] folderNames = dir.list();
	
	/*ObjectFile objectFile = new ObjectFile("content.dat");
	
	AnalyserContent content = objectFile.readAnalyserContent();
	if(content==null){
		content = new AnalyserContent();
		objectFile.writeAnalyserContent(content);
	}*/

	Hashtable<String,Hashtable<String,String>> ht = (Hashtable<String,Hashtable<String,String>>)session.getAttribute(SESSION_ATTRIBUTE.ANALYSER.toString());
	if(ht==null)
		ht= new Hashtable<String,Hashtable<String,String>>();
	
	
%>
	<h2>Resume Chooser Menu</h2>
	<div id="selections"  >
	<table border="0">
	<tr>
	<td width="400px">   <b>Users</b>
	    <br>
	    <select size="10" style="width:97%;" id="selectUser" onchange="changeUser()">
	    <%for(int i=0;i<folderNames.length;i++){
	    	if(!ht.containsKey(folderNames[i])){
	    			ht.put(folderNames[i],new Hashtable<String,String>());
	    	}
	    	
	    	%><option value="<% out.write(folderNames[i]); %>" <%if(name!=null && name.equals(folderNames[i])) out.write("selected"); %>> <% out.write(folderNames[i]); %></option>
	    	<%
	    }
	    
	    %>
	    </select>
	    </td> 
	    <td>
	    <b>Submissions</b>
	    <br>
	    <div id="submissionsDiv" style="width:750px; height:165px; overflow: scroll;">
	    <table border="1" style="width:97%;">
	    <!--  <select size=10 style="" id="selectResume" onchange="changeResume(<%out.write("'"+name+"'");%>)"> -->
	    <%
	    //If user has been selected
	    if(name!=null){
	    	
	    	fullPath=path+name+File.separator;
	    	File userFolder = PlatformProperties.getInstance().getResourceFile(fullPath);
	    	//Content of folder
	    	if(userFolder.exists()){
	    		
	    		//Get existing submissions of user
		    	Hashtable<String,String> submissions = ht.get(name);
		    	
	    		fileNames=userFolder.list();
	    		if(fileNames.length==0){
	    			%> <tr>
	    			<td style="color: red"> Empty Folder</td> 
	    			</tr>
	    			<%
	    		}
    			else{
    				String[] split;
    				List<String> inserted=new LinkedList<String>();
    				boolean NAN;
    				
    				String uploadDir = "uploads" + File.separator + name + File.separator;
    				File temp = PlatformProperties.getInstance().getResourceFile(uploadDir);
    				String absolutePath = temp.getAbsolutePath()+File.separator;
    				ArrayList<String> arrayTimestamp = new ArrayList<String>();
    				
    				//Load checked to memory
    				loadChecked(absolutePath,session);
    				//For each file get name
		    		for(int j=0;j<fileNames.length;j++){
		    			split=fileNames[j].split("_");
			    		NAN = false;
		    			//Identify if timestamp is a number
					      try {
					      	Double.parseDouble(split[0]);
					      } catch (NumberFormatException e) {
					    	  //System.out.println(split[0]+ " is not a number!");
					      	NAN=true;
					      }
		    			//Get unique timestamp to identify resumes
		    			if(!NAN){
			    			if(!inserted.contains(split[0])){
			    				inserted.add(split[0]);
			    				if(!submissions.contains(split[0]))
			    					submissions.put(split[0],"0");
			    				

			    				arrayTimestamp.add(split[0]);	
		    				}
		    			}
		    		}
    				
    				bubbleSort(arrayTimestamp);
    				for(int i=0;i<arrayTimestamp.size();i++){
	    				java.util.Date date = new java.util.Date(Long.parseLong(arrayTimestamp.get(i)));
	    				String dateStr= date.toGMTString();
	    				dateStr=dateStr.substring(0,dateStr.length()-3);
	    				String count=countFeedbackErrors(arrayTimestamp.get(i),name,absolutePath);
	    				if(resume!=null){
	    					//If resume is selected
		    				if(resume.equals(arrayTimestamp.get(i))){
		    					if(isCheckedSubmission(arrayTimestamp.get(i),session)){
									%> <tr><td> <span style="background-color:yellow; color:green;" onclick="changeResume(<%out.write("'"+name+"'");%>,<%out.write("'"+arrayTimestamp.get(i)+"'"); %>)" id="<% out.write("sp"+i); %>"> <%out.write(dateStr+" ("+count+")"); %> </span></td></tr>
							    	<%
		    					}
		    					else{
							    	%> <tr><td> <span style="background-color:yellow;" onclick="changeResume(<%out.write("'"+name+"'");%>,<%out.write("'"+arrayTimestamp.get(i)+"'"); %>)" id="<% out.write("sp"+i); %>"> <%out.write(dateStr+" ("+count+")"); %> </span></td></tr>
							    	<%
		    					}
		    				}
	    					//if resume is not selected
		    				else{
		    					if(isCheckedSubmission(arrayTimestamp.get(i),session)){
							    	%> <tr><td> <span style="color:green;" onclick="changeResume(<%out.write("'"+name+"'");%>,<%out.write("'"+arrayTimestamp.get(i)+"'"); %>)" id="<% out.write("sp"+i); %>"> <%out.write(dateStr+" ("+count+")"); %> </span></td></tr>	    	
							    	<%	
		    					}
		    					else{
							    	%> <tr><td> <span onclick="changeResume(<%out.write("'"+name+"'");%>,<%out.write("'"+arrayTimestamp.get(i)+"'"); %>)" id="<% out.write("sp"+i); %>"> <%out.write(dateStr+" ("+count+")"); %> </span></td></tr>
							    	<%	
		    					}
		    				}
	    				}
	    				else{
	    					if(isCheckedSubmission(arrayTimestamp.get(i),session)){
						    	%> <tr><td> <span style="color:green;" onclick="changeResume(<%out.write("'"+name+"'");%>,<%out.write("'"+arrayTimestamp.get(i)+"'"); %>)" id="<% out.write("sp"+i); %>"> <%out.write(dateStr+" ("+count+")"); %> </span></td></tr>
						    	<%	
	    					}
	    					else{
						    	%> <tr><td> <span onclick="changeResume(<%out.write("'"+name+"'");%>,<%out.write("'"+arrayTimestamp.get(i)+"'"); %>)" id="<% out.write("sp"+i); %>"> <%out.write(dateStr+" ("+count+")"); %> </span></td></tr>
						    	<%		
	    					}
	    				}
    				}
    			}
	    	}
	    	else{ %> <tr><td style="color: red" > Invalid user </td></tr>
	    	<%
	    	}
	    	} 
    	else{%> <tr><td style="color: red" > Select a user </td></tr>
    	<%} %>
    	<!-- </select> -->
	    </table>
	    </div>
	    </td>
	    </tr>
	    </table>
	</div>
	
	<% 

	session.setAttribute(SESSION_ATTRIBUTE.ANALYSER.toString(),ht);
	
	if(resume!=null){
		String[] split;
		String[] extensionSplit;
		
		jobType="";
		sourceFileName="";
		cvFileName="";
		finalDocumentFileName="";
		tagFileName="";
		modificationsFileName="";
		feedbackFileName="";
		debugFileName="";
		
		for(int i=0;i<fileNames.length;i++){
			
			if (fileNames[i].equals("MyResumes"))
				continue;
			
			//System.out.println("FILENAMES:"+fileNames[i]);
			split=fileNames[i].split("_");
			if(split[0].equals(resume) || split[1].equals(resume)){
				if(split.length>1){
					//Get all files
					if(split[0].equals("HIGHLIGHTED")){
						String[] tmp=fileNames[i].split("\\.");
						if(!tmp[tmp.length-1].equals("html")){
							finalDocumentFileName=fileNames[i];
							//System.out.println("***************Final:"+finalDocumentFileName);
						}

					}
					else if(split[1].equals("source.txt")){
						sourceFileName=fileNames[i];
						//System.out.println("Source Name="+fileNames[i]);
					}
					else if(split[1].equals("tag.txt")){
						tagFileName=fileNames[i];
						//System.out.println("Tag file name="+tagFileName);
					}
					else if(split[1].equals("feedback.txt")){
						feedbackFileName=fileNames[i];
						//System.out.println("Feedback file name="+feedbackFileName);
					}
					else if(split[1].equals("modifications.txt")){
						modificationsFileName=fileNames[i];
						//System.out.println("Modifications file name="+modificationsFileName);
					}
					else if(split[1].equals("Other.txt")){
						//do nothing
					}
					else if(split[1].endsWith(".algo")){
						//do nothing
					}
					else{
						extensionSplit=fileNames[i].split("\\.");
						//If as 3 tokens it is the final document html
						if(extensionSplit[extensionSplit.length-1].equals("vertical")){
							jobType=fileNames[i].substring(resume.length(),extensionSplit[0].length());
							//System.out.println("Job Type="+jobType);
							
						}	
						else if(extensionSplit.length>=3 && extensionSplit[extensionSplit.length-1].equals("html")){
							htmlFileName=fileNames[i];
							//System.out.println("***************HTML:"+htmlFileName);
							//File before was the resume file 
						}
						else if(extensionSplit.length>=3 && extensionSplit[extensionSplit.length-1].equals("debug")){
							debugFileName=fileNames[i];
							//System.out.println("Debug file name="+debugFileName);
						}
						else{
							cvFileName=fileNames[i];
							//System.out.println("CV file name="+cvFileName);
						}
					}
	
				}
			}
		}
		
		String uploadDir = "uploads" + File.separator + name + File.separator;

		File temp = PlatformProperties.getInstance().getResourceFile(uploadDir);
		
		
		String absolutePath = temp.getAbsolutePath()+File.separator;

		%>
		
		<form method="post" action="CheckSubmission">
		<input type="hidden" name="name" value="<%out.write(name); %>"></input>
		<input type="hidden" name="resume" value="<%out.write(resume); %>"></input>
		<input type="hidden" name="path" value="<%out.write(absolutePath); %>"></input>
		<%if(isCheckedSubmission(resume,session)){ %>
		<button  type="submit"> Mark as Checked </button>
		<%}
		else{
		%>
		<button type="submit"> Mark as Checked </button>
		<%} %>
		</form>
		<br>
		<h2>Resume Available Files</h2>
		<table border="0">
		<tr><td><b>Job Type: </b><a><%out.write(jobType); %></a></td>
		</tr>
		<tr>
		<td><a href="SendFile?filename=<%out.write(cvFileName);%>&absolutepath=
		<% out.write(absolutePath);%>">Submitted CV</a></td>
		</tr>
		<tr><td><a href="SendFile?filename=<%out.write(sourceFileName);%>&absolutepath=
		<% out.write(absolutePath);%>">Source Proposal</a></td>
		</tr>
		<tr><td><a href="SendFile?filename=<%out.write(finalDocumentFileName);%>&absolutepath=
		<% out.write(absolutePath);%>">Final Document</a></td>
		</tr>
				<tr><td><a href="SendFile?name=<%out.write(name); %>&filename=<%out.write("all");
				session.setAttribute("allFiles",cvFileName+"|"+sourceFileName+"|"+finalDocumentFileName);%>
				&absolutepath=<% out.write(absolutePath);%>">Get all in .zip</a></td>
		</tr>		
		<tr><td><a href="ResumeCreator?type=2" target="_blank">View Full HTML</a></td></tr>
		</table>
		
		<br>
		<h2>Resume Informations</h2>
		
    <p></p>

	<div id="feedbackerrors">
	<%
	//System.out.println("DEBUG="+debugFileName);
	//Attributes used in ResumeCreator.java
	session.setAttribute("tagFileName",tagFileName);
	session.setAttribute("absolutePath",absolutePath);
	session.setAttribute("resume",resume);
	session.setAttribute("debugFileName",debugFileName);
	session.setAttribute("cvFileName",cvFileName);
	session.setAttribute("finalDocumentFileName",finalDocumentFileName);
	session.setAttribute("htmlFileName",htmlFileName);
	session.setAttribute("modificationsFileName",modificationsFileName);
	
	%>
	
	<iframe id="iframe_services" src="ResumeCreator?type=1" style="width:97%; height:600px; overflow: scroll;" ></iframe>

	</div>
    <p></p>
		
	<% } %>
<%@include file="footer.lay"%>