<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.wordnet.model.WordNetDataProviderPool"%>
<%@include file="header.jsp"%>
<%
	try
	{
	if (user == null)
	{
		response.sendRedirect("");
		return;
	}
	// Clear scores from last run
	session.removeAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
	JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	if (posting == null)
	{
		response.sendRedirect("upload_jobposting.jsp");
		return;
	}
	List<File> resumeFiles = (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
	if (resumeFiles == null) resumeFiles = new ArrayList<File>();
	String command = request.getParameter("command");
	String sessStamp = request.getParameter("sessStamp");
	if ("saveSession".equals(command))
	{
		new UserPhraseSelection(request, response).save();
	}
	if ("deleteSession".equals(command))
	{
		new SortPhrase().deleteObjects("JOB_ID = " + posting.getId() + " and USER_NAME = " + DBAccess.toSQL(user.getLogin()) + " and saved_time=" + sessStamp);
	}
	boolean usestars = true;
	if ("false".equals(request.getParameter("usestars")))
		usestars = false;
	else if (request.getParameter("usestars") == null && session.getAttribute(SESSION_ATTRIBUTE.USE_STARS.toString()) != null)
	   usestars = (Boolean) session.getAttribute(SESSION_ATTRIBUTE.USE_STARS.toString());
	else if (request.getParameter("usestars") == null && user.isSimpleRating())
		usestars = false;
	boolean showPosting = true;
	boolean owner = true;
	if (!posting.getUserName().equals(user.getLogin()))
	{
		showPosting = false;
		owner = false;
		SharedJobPosting sjp = (SharedJobPosting)session.getAttribute(SESSION_ATTRIBUTE.SHARED_POSTING.toString());
		if (sjp != null)
		{
			if (sjp.isPerm1() || sjp.isPerm3())
				showPosting = true;
		}
	}
	Collection<PhraseList> phraseList = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	Collection<PhraseList> nonLemmaPhraseLists = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString());
	DocumentRS sourceDocument = (DocumentRS) session.getAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString());
	Map<String,String> phrasesSynonyms = (Map<String,String>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());

	Collection<PhraseList> lemmaPhraseLists = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString());
	SortPhrase sp = new SortPhrase();
	List<Object[]> savedSessions = new UserDBAccess().getQueryResults("select distinct saved_time from " + sp.getDB_TABLE() + " where user_name =" + DBAccess.toSQL(user.getEmail()) + " and JOB_ID = " + posting.getId() + " order by saved_time desc");
	List<SortPhrase> prevPhrases = new ArrayList<SortPhrase>();
	int MAX_SAVED_SESSIONS = 10;
	if (savedSessions.size() > 0)
	{
		long last = (Long)savedSessions.get(0)[0];
		System.out.println("Num of sessions:" + savedSessions.size() + " Last:" + last);
		if ((sessStamp == null || "deleteSession".equals(command)) && !"restoreSession".equals(command)) sessStamp = "" + last;
		// Only get prev phrases if this is not a refresh
		if (request.getParameter("usestars") == null)
			prevPhrases = sp.getObjects("JOB_ID = " + posting.getId() + " and USER_NAME = " + DBAccess.toSQL(user.getEmail()) + " and saved_time= " + sessStamp + " order by saved_time");
		for (int i = savedSessions.size()-1; i >= MAX_SAVED_SESSIONS; i--)
		{
			new SortPhrase().deleteObjects("JOB_ID = " + posting.getId() + " and USER_NAME = " + DBAccess.toSQL(user.getLogin()) + " and saved_time=" + savedSessions.get(i)[0]);
			savedSessions.remove(i--);
		}
	}
	System.out.println("Number of prev phrases:" +  prevPhrases.size());
	String postingId = posting.getPostingId();
	Collection<PhraseList> jobPhraseList = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	Set<String> jobPhrases = new HashSet<String>();
	for(PhraseList p : jobPhraseList)
	{   
		String phrase = p.iterator().next().getPhrase().getBuffer().toString();
		//if (phrase.toLowerCase().startsWith("shell"))
		//	System.out.println("job phrase:" + phrase);
		jobPhrases.add(phrase.toLowerCase().trim());
	}
	for(PhraseList p : lemmaPhraseLists)
	{   
		String phrase = p.iterator().next().getPhrase().getBuffer().toString().trim();
		//if (phrase.toLowerCase().startsWith("shell"))
		//	System.out.println("lemma phrase:" + phrase);
		phrase = phrase.toLowerCase();
		if (!jobPhrases.contains(phrase))
			jobPhrases.add(phrase);
	}
	String templateName = posting.getTemplateName();
	List<UserTemplatePhrase> templatePhrases = new ArrayList<UserTemplatePhrase>();
	if (templateName != null && templateName.trim().length() > 0)
	{
		UserTemplate template = (UserTemplate) new UserTemplate().getObject("user_name=" + UserDBAccess.toSQL(posting.getUserName()) + " and name ="  + UserDBAccess.toSQL(templateName));
		templatePhrases = new UserTemplatePhrase().getObjects("template_id=" + template.getId() + " order by phrase");
	}
	session.setAttribute(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString(), templatePhrases);
	System.out.println("Number of template phrases:" +  templatePhrases.size() + " job posting phrases:" + jobPhrases.size());
	boolean systemTemplate = false;
	String ttype = "template";
	if (templateName == null || templateName.length() == 0)
	{
		String[] industry = posting.getIndustry().split("\n");
		templateName = industry[0];
		systemTemplate = true;
		ttype = "system template";
		Vertical vert = (Vertical) new Vertical().getObject("industry = " + UserDBAccess.toSQL(templateName));
		if (vert != null && false)
		{
			List<VerticalPhrase> vertPhrases = new VerticalPhrase().getObjects("vertical_id = " + vert.getId() + " order by phrase");
			for (VerticalPhrase vp: vertPhrases)
			{
				UserTemplatePhrase utp = new UserTemplatePhrase();
				utp.setPhrase(vp.getPhrase());
				templatePhrases.add(utp);
				for (SortPhrase prevPhrase: prevPhrases)
				{
					if (vp.getPhrase().trim().equals(prevPhrase.getLemmatizedPhrase().trim()))
					{
						if (prevPhrase.isRequired())
						{
							utp.setRequired(true);
						}
						else if (prevPhrase.getStars() != null)
						{
							utp.setScore(prevPhrase.getStars().intValue());
						}
						if (prevPhrase.isTag())
						{
							utp.setTag(true);
						}
						break;
					}
				}
			}
		}
	}
%>
<style>

.ttip {
	display: none;
	background-color: white;
	border: 1px solid #999;
	cursor: default;
	position: absolute;
	text-align: left;
	width: 350px;
	z-index: 50;
	padding: 15px;
	max-height:300px;
	overflow-y:auto;
}

.ttipBody {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 13px;
	padding: 0;
	margin: 0;
	max-height: 500px;
}

.rpop {
	display: none;
	z-index: 3;
	border: solid 1px black;
	background-color: #DDFFFF;
	padding: 1;
	padding-left: 5;
	padding-right: 5;
	min-width: 300px;
	max-height: 400px;
	overflow: auto;
	float: right;
}

.resumePhrases {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 13px;
	padding: 0;
	margin: 0;
	height: 400px;
    overflow: auto;
}

</style>
<!--[if IE]>
<style type="text/css">body {behavior: url(ifiles/iehoverfix.htc);}</style>
<![endif]-->
<link type="text/css" rel="stylesheet" href="css/phrases.css" />
<script src="phrases.js" type="text/javascript"></script>
<script type="text/javascript" src="ratingsys.js"></script> 
<script type="text/javascript" src="default.js"></script> 
<link rel="stylesheet" href="ifiles/stylesheet.css" />
<script type="text/javascript" src="ifiles/javascript.js"></script>
<script type="text/javascript">
	var isChrome = navigator.appVersion.indexOf("Chrome") >= 0;
	var synPhrase;
	$(function() {					
		$('#accordion').easyAccordion({
		slideNum:false
	});			

	$('#icon *').tooltip();

		// Dialog 
		$('#dialog').dialog({
			autoOpen: false,
			width: 460
			//,buttons: {
			//		'Yes': function(){
			//			$(this).dialog('close');
			//			callback(true);
			//		},
			//		'No': function(){
			//			$(this).dialog('close');
			//			callback(false);
			//		}
			//	}			
		});
		
		// Dialog Link
		$('#dialog_link').click(function(){
			$('#dialog').dialog('open');
			return false;
		});
		
		$('#phrasespop').dialog({
			autoOpen: false,
			width: 460
	});
	

		
        $('#pharasepop_link').click(function(){
			$('#phrasespop').dialog('open');
            return false;
        });


    });

    $.fn.slideFadeToggle = function(easing, callback) {
        return this.animate({ opacity: 'toggle', height: 'toggle' }, "fast", easing, callback);
    };
	
	var currentPanel = "";
	var companies = false;
	function openCloseCompanies()
	{
		var compdiv = document.getElementById("companies_div");
		if (compdiv != null)
		{
			if (companies)
			{
				compdiv.style.display = "none";
				companies = false;
			}
			else
			{
				compdiv.style.display = "inline";
				companies = true;
			}
		}
	}

	var universities = false;
	function openCloseUniversities()
	{
		var compdiv = document.getElementById("universities_div");
		if (compdiv != null)
		{
			if (universities)
			{
				compdiv.style.display = "none";
				universities = false;
			}
			else
			{
				compdiv.style.display = "inline";
				universities = true;
			}
		}
	}
	
	var resumePhrases = false;
	var rbusy = false;
	function refreshResumePhrases()
	{
		currentPanel = "r";
		if (resumePhrases) return;
		if (rbusy) return;
		rbusy = true;
		document.getElementById("loading").style.visibility = "visible";
		var url = "resume_phrases.jsp?startIndex=<%=(phraseList.size()+templatePhrases.size()+50)%>&usestars=<%=usestars%>&random=" + new Date();
		 $.ajax({         
				url: url,         
				type: 'post',         
				async: false,         
				cache: false,         
				timeout: 30000,         
				error: function(){
					alert("Error getting resume phrases");
					document.getElementById("loading").style.visibility = "hidden";
					rbusy = false;
					return true;},
				success: function(response){
					rbusy = false;
					document.getElementById("loading").style.visibility = "hidden";
					//alert(response);
					if(response != null)
					{
						var notdone = response.length < 100 && (response.indexOf("Still Processing") != -1);
						var resume_div= document.getElementById("resumephrases");
						resume_div.innerHTML = response;
						var compdiv = document.getElementById("companies_div");
						if (compdiv != null)
						{
							compdiv.style.display = "none";
						}
						compdiv = document.getElementById("universities_div");
						if (compdiv != null)
						{
							compdiv.style.display = "none";
						}
						if (!notdone)
						{
							resumePhrases = true;
						}
						else
						{
							alert("Resumes have not been processed yet");
						}
						initResumePhraseSelections();
					}
					else
					{
						alert('Error refreshing resume phrases' );
					}
				}
		});
	}

	//
	// This function is very delicate
	// Be careful if you mess with it
	function initResumePhraseSelections()
	{
		var y = <%=phraseList.size()%>+<%=templatePhrases.size()%>+100;
		for (var i = 0; i < 50; i++)
		{
			var phrase = document.getElementsByName("resumephrase_" + y);
			var rating = document.getElementsByName("rating_" + y);
			var required = document.getElementsByName("required_" + y);
			var tag = document.getElementsByName("tag_" + y);
			y++;
			if (phrase == null)
			{
				alert("resumephrase_" +y + " is missing");
				continue;
			}
			if (phrase.length == 0 || phrase[0].value == "") continue;
			//alert(phrase[0].value + " rating:" + rating[0].value + " required:" + required[0].value + " tag:" + tag[0].value);
			for (var x = <%=phraseList.size()%>+<%=templatePhrases.size()%>+150; x < 1000; x++)
			{
				var resphrase = document.getElementById("resumephrase_" + x);
				var resrating = document.getElementsByName("rating_" + x);
				var resrequired = document.getElementsByName("required_" + x);
				var restag = document.getElementById("tag_" + x);
				//alert(resphrase.value);
				if (resphrase == null) break;
				if (resphrase.value == phrase[0].value)
				{
					//alert(resphrase.value + ":" + resrating.length + ":" + rating.length);
					if (resrating != null && resrating.length > 0)
						resrating[0].value = rating[0].value;
					//alert("1:" + resphrase.value +":" +resrequired.length);
					if (resrequired != null && resrequired.length == 1 && required[0].value == 'true')
						resrequired[0].checked = true;
					//alert("2:" + resphrase.value +":" +resrequired.length);
					if (restag != null && tag[0].value == 'tag')
						restag.checked = true;
					//alert(resphrase.value +":" +resrequired.length);
					if (resrequired != null && resrequired.length == 3)
					{
						//alert(rating[0].value);
						if (required[0].value == 'true')
							resrequired[2].checked = true;
						if (rating[0].value == '1')
							resrequired[0].checked = true;
						if (rating[0].value == '2')
							resrequired[0].checked = true;
						if (rating[0].value == '3')
							resrequired[1].checked = true;
						if (rating[0].value == '4')
							resrequired[1].checked = true;
						if (rating[0].value == '5')
							resrequired[1].checked = true;
					}
					if (<%=usestars%>)
					{
						var idStr = "" + x + "_" + rating[0].value;
						//alert(idStr);
						var img = document.getElementById(idStr);
						if (img != null)
						{
							starRater(img, 'on')
							starRater(img, 'save');
							document.getElementById("rating_" + x).value = rating[0].value; // Why is this need? the 'save' should do it
							//alert("x:" + x +":" + document.getElementById("rating_" + x).value);
						}
					}
					break;
				}
			}
		}

	}
	
	function downloadPhrases()
	{
<%		if (user.isAdmin() || user.isTester()) { %>
		window.open("download_resume_phrases.jsp");
<%		} %>
	}
	
	function showJobPosting()
	{
		window.open("highlighted_jobposting.jsp?random=" + new Date(), "Job Posting", "toolbar=no,status=yes,locationbar=no,scrollbars=yes,resizable=yes,top=25,left=100,width=625,height=600")		
	}
	
	function gotoUploadResumes()
	{
<%	// If an invited resume evaluator, he can not go to	resume folder
	if (!owner) { %>
		return;
<%	} %>
		if (confirm("Are you sure you want to leave this page and upload more resumes?"))
		{
			window.location = "upload_resumes.jsp?resumeFolder=<%=posting.getResumeFolder()%>";
		}
	}
	
	function gotoUploadJobPosting()
	{
		if (confirm("Are you sure you want to leave this page and start over?"))
		{
			window.location = "upload_jobposting.jsp";
		}
	}
	
	function goHome()
	{
		if (confirm("Are you sure you want to leave this page and start over?"))
		{
			window.location = "home.jsp";
		}
	}
	
	function goUpload()
	{
		if (confirm("Are you sure you want to leave this page and start over?"))
		{
			window.location = "upload_resumes.jsp";
		}
	}
</script>
<script type="text/javascript" src="js/custom-form-elements.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>   

<link href="css/smart_wizard.css" rel="stylesheet" type="text/css">
    
	<div id="dialog" style="display:none">
		<span id=dialogtext></span>
        <div style="clear:both; margin-top:30px; float:right;">
        <a href="javascript:closeDialog()" class="orangeBtn">Cancel</a>
        <a href="javascript:deletePhrase2()" class="orangeBtn">Ok</a>
        </div>
	</div>    
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
	<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    
    <script type="text/javascript">
        $(function() {
	        $("a[rel^='prettyPhoto']").prettyPhoto();

        });
    </script>
<div id="phrasespop" style="display:none;"><p><b>Equivalent phrases for Security</b><br/>(These phrases will also be matched)<input type="text" size="30" name="email" id="email" /></p><p><input type="submit" value="Clear Phrases" name="commit" id="message_submit"/> </p></div>    

<div class="middle">

<div class="maintext_dashboard">
 
 <div class="sky_bar">
    <div style="float:right; clear:both;">
    <a href="javascript:gotoUploadJobPosting()">START NEW JOBS</a>
    <a href="javascript:goHome()">PREVIOUS JOBS</a>
    </div>
</div>
 
 <div class="text_dashboard">
  <div id="loading" style="visibility: hidden">
    <p>Processing...</p>
  </div>
<table align="center" border="0" cellpadding="0" cellspacing="0">
<tr><td> 
<!-- Smart Wizard -->
  		<div id="wizard" class="swMain">
  			<ul class="anchor">
  				<li><a href="javascript:gotoUploadJobPosting()">
                <label class="stepNumber">1</label>
                <span class="stepDesc">
                   Upload Job Posting<br />
                   <small>Job Posting Details</small>
                </span>
            </a></li>
   				<li><a href="javascript:gotoUploadResumes()">
                <label class="stepNumber">2</label>
                <span class="stepDesc">
                   Upload Resumes <br/> 
                   <small>Upload to Resume Folder</small>              
                </span>
            </a></li>
 				<li><a class="mini selected" href="#step-3">
                <label class="stepNumber">3</label>
                <span class="stepDesc">
                   Select Phrases
                </span>                   
             </a></li>
  				<li><a href="javascript:doSubmit()">
                <label class="stepNumber">4</label>
                <span class="stepDesc">
<%	if (resumeFiles.size() > 0) { %>
				   Rank Resumes
<%	} else { %>
				   Save Phrases
<%	} %>
                </span>                   
             </a></li>
  			</ul>
  			<div class="stepContainer">	
            <div id="content0" class="content">


<form method='post' action='Analyzer' name="phrasesform">
        <div id="accordion">
            <dl>
<% // ------------- begin job posting phrases -------------------------------------------------------------// %>
<% 
	int i = 0;
	List<String> displayedJobPhrases = new ArrayList<String>();
	if (sourceDocument != null) { %>
                <dt onclick="currentPanel=''">Job Posting Phrases &nbsp; &nbsp; V</dt>
                <dd> <p>

<% if (usestars) { %>
<table class="tableHeaderFive" >
<tbody>
<tr>
	<th colspan="2" class="kwdText">From Job Posting</th>
	<th class="kwdRating">Importance<br><input type=checkbox onclick="selectAll(this)">Good to have</th>
	<th nowrap class="kwdCbx">Must<br>Have</th>
	<th class="kwdCbx">Tag</th>
<% } else { %>
<table class="tableHeader" >
<tr>
	<th colspan="2" class="kwdText">From Job Posting</th>
	<th nowrap align=center class="kwdCbx">Nice to<br>Have</th>
	<th nowrap align=center class="kwdCbx">Should<br>Have<br><input type=checkbox onclick="selectAll(this)">All</th>
	<th nowrap align=center class="kwdCbx">Must<br>Have</th>
	<th class="kwdCbx">Tag</th>
	<th class="kwdClear">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
<% } %>
</tr>
</tbody>
</table>
<div class="scrollbar1">
<div id='tt' class="ttip">
<table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>
<div id='popup' class="ttip"><table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>            	
<div id="logbox" style="border:solid 1px #00f;font-size:16px;height:20px;display:none">message bar for event logs</div>
<div id ="phrases">
<input type=hidden name="usestars" value="<%=usestars%>"/>
   <div id="suggestedphrases1">
	<b><!--input type=button onclick="clearPostingPhrases()" value="Clear selection"--></b>
	<div id="jobphraselist" >
<%
	WordNetDataProviderPool.getInstance().acquireProvider();
	Map<String, String> contexts = ResumeReport.getPhrasesToContextMap(sourceDocument, nonLemmaPhraseLists);
	WordNetDataProviderPool.getInstance().releaseProvider();
	//System.out.println("Contexts Map:" + contexts);
	Set<String> uniquePhrases = new HashSet<String>();
	for(PhraseList p : phraseList)
	{   
		//System.out.println("Phrase Buffer: " + p.iterator().next().getPhrase().getBuffer());
		//System.out.println("POS Sequence for phrase: " + p.iterator().next().getPhrase().getNgram().getPosSequence());
		String phrase = p.iterator().next().getPhrase().getBuffer().toString();
		if (uniquePhrases.contains(phrase.toLowerCase().trim())) continue;
		if (phrase.toUpperCase().equals(phrase) && phrase.length() > 5) continue;
		uniquePhrases.add(phrase.toLowerCase().trim());
		displayedJobPhrases.add(phrase);
		String sentence = "";
		Iterator<PhraseList> iteratorUn = nonLemmaPhraseLists.iterator();
		Iterator<PhraseList> iteratorLem = lemmaPhraseLists.iterator();
		while(iteratorUn.hasNext() && iteratorLem.hasNext())
		{
			PhraseList lemmaPhraseList = iteratorLem.next();
			PhraseList unlemma = iteratorUn.next();
			String unlemmaText = unlemma.iterator().next().getPhrase().getBuffer();
			if (lemmaPhraseList.iterator().next().getPhrase().getBuffer().equalsIgnoreCase(phrase))
			{
				if (contexts != null)
					sentence = contexts.get(unlemmaText.toLowerCase().trim());
				if (sentence == null)
					sentence = contexts.get(phrase.toLowerCase().trim());
			//System.out.println("phrase:" + phrase + " unlemma:" + unlemmaText.toLowerCase().trim() + " sentence:" + sentence + "'");
			}
		}

		if (sentence == null) sentence = "";
		String srccount = "" + sentence.split("_LIMIT_").length;
		if (srccount.length() > 10) srccount = "10";
%>
	<div class="kwdRecord" id="kwd_<%=100+i%>" onmouseover="helperActions(this, 'show')" onmouseout="helperActions(this, 'hide')">
	<div class="kwdTools" nowrap>
		<input type="hidden" value="<%=phrase%>" id="suggphrases_<%=100+i%>" name="suggphrases_<%=100+i%>"/>
<%	if (showPosting) { %>
           	<span id="icon">
             <a title="<center>Text from the Job Posting</center><%="\u2022" + sentence.replace('\'', '`').replace('"', '`').replace("_LIMIT_","<br>\u2022")%>" href="#">
                <img src="tinyInfo.gif" alt="" />
             </a>
     	   </span>    
<%	}
		if (phrasesSynonyms.get(phrase.trim()) != null)
		{
			String synonymsStr = phrasesSynonyms.get(phrase.trim()).trim();
			if (synonymsStr.length() > 1)
			{
				String[] synonymns = synonymsStr.split("\\|");
				synonymsStr = synonymsStr.replace("|","_LIMIT_");
				int syncnt = synonymns.length;
				if (syncnt > 9) syncnt = 10;
%>
	<span class="iconSynonyms" id="syn_<%=100+i%>" onclick='getSynonymsPop(event, "<%=phrase%>", this, "tt");synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%			} else { %>
	<span class="iconGSynonyms" id="syn_<%=100+i%>" onclick='getSynonymsPop(event, "<%=phrase%>", this, "tt");synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%			} } else { %>
	<span class="iconGSynonyms" id="syn_<%=100+i%>" onclick='getSynonymsPop(event, "<%=phrase%>", this, "tt");synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%		} %>
<%	if (true || user.isAdmin() || user.isTester()) { %>
	<span class="iconDelete" onclick='deletePhrase(this, "<%=phrase%>", <%=100+i%>);' title="Remove Phrase"></span>
<%		} %>
	</div>
	<div class="kwdText" id="phrase<%=100+i%>" align=left>
		<%=phrase%>
	</div>
<% if (usestars) { %>
<div class="kwdRating">
	<div class="rateActions">
		<div id="rateMe" style="width:auto;float:left;" onmouseout="starRater(<%=100+i%>)">
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Plus to have" id="<%=100+i%>_1" onclick="starRater(this, 'save');setPhraseRating('suggphrases_', <%=100+i%>, 1)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Nice to have" id="<%=100+i%>_2" onclick="starRater(this, 'save');setPhraseRating('suggphrases_', <%=100+i%>, 2)" class=""></a>

		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Good to have" id="<%=100+i%>_3" onclick="starRater(this, 'save');setPhraseRating('suggphrases_', <%=100+i%>, 3)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Should have" id="<%=100+i%>_4" onclick="starRater(this, 'save');setPhraseRating('suggphrases_', <%=100+i%>, 4)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Important" id="<%=100+i%>_5" onclick="starRater(this, 'save');setPhraseRating('suggphrases_', <%=100+i%>, 5)" class=""></a>
		<!--a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Must have" id="100_6" onclick="starRater(this, 'save')" class=""></a-->
		</div>
		<span style="display:none;" class="actionStatus" id="status<%=100+i%>"></span>
		<span style="float:right;" id="actions<%=100+i%>">
		<a onclick="clearStarRating(<%=100+i%>);setPhraseRating('suggphrases_', <%=100+i%>, 0);return false;" href="#" style="display:none;" id="clear<%=100+i%>">Clear</a>

		<!-- a onclick="removeKeyphrase(100);return false;" href="#" style="display:none;" id="remove<%=100+i%>">Remove</a -->
		</span>
	</div>
	<input type="hidden" value="0" id="rating_<%=100+i%>" name="rating_<%=100+i%>"/>
	<!--input type="hidden" value="false" id="required_<%=100+i%>" name="required_<%=100+i%>" /-->
</div>
<div align=center class="kwdCbx"><input type="checkbox" value="true" id="required_<%=100+i%>" name="required_<%=100+i%>" onclick="checkRequired(this,<%=100+i%>,'suggphrases_')"/><!--img id="reqimg_<%=100+i%>" src="star_off.gif" onclick="setRequired(this, <%=100+i%>)"/ --></div>
<div align=center class="kwdCbx"><input type="checkbox" value="tag" id="tag_<%=100+i%>" name="tag_<%=100+i%>" /></div>
<script>
<%
	int rating = getInt(request.getParameter("rating_" + (100+i)));
	String required = request.getParameter("required_" + (100+i));
	if ("maybe1".equals(required)) rating = 1;
	if ("maybe3".equals(required)) rating = 3;
	String tag = request.getParameter("tag_" + (100+i));
	if (rating > 0)
	{
%>
		var idStr = "" + (100 + <%=i%>) + "_" + <%=rating%>;
		var img = document.getElementById(idStr);
		if (img != null)
		{
			starRater(img, 'on')
			starRater(img, 'save');
		}
<%		
	}
	if ("true".equals(required))
	{
%>
	document.getElementById("required_<%=100+i%>").checked = true;
<%		
	}
	if ("tag".equals(tag))
	{
%>
	document.getElementById("tag_<%=100+i%>").checked = true;
<%		
	}
%>
</script>
<% } else { %>
<div align=center class="kwdCbx"><input type="radio" value="maybe1" id="maybe1_<%=100+i%>" name="required_<%=100+i%>" onclick="setPhraseRating('suggphrases_', <%=100+i%>, 1)"/></div>
<div align=center class="kwdCbx"><input type="radio" value="maybe3" id="maybe3_<%=100+i%>" name="required_<%=100+i%>" onclick="setPhraseRating('suggphrases_', <%=100+i%>, 3)"/></div>
<div align=center class="kwdCbx"><input type="radio" value="true" id="required_<%=100+i%>" name="required_<%=100+i%>" onclick="setPhraseRating('suggphrases_', <%=100+i%>, 6)"/></div>
<div align=center class="kwdCbx"><input type="checkbox" value="tag" id="tag_<%=100+i%>" name="tag_<%=100+i%>" /></div>
<div align=center class="kwdClear"><a href="javascript:deselect(<%=100+i%>)" id="clear_<%=100+i%>" style="display:none">Clear</a></div>
<script>
<%
	String required = request.getParameter("required_" + (100+i));
	int rating = getInt(request.getParameter("rating_" + (100+i)));
	if (rating == 2 || rating == 1) required = "maybe1";
	if (rating >= 3) required = "maybe3";
	String tag = request.getParameter("tag_" + (100+i));
	if ("maybe1".equals(required))
	{
%>
	document.getElementById("maybe1_<%=100+i%>").checked = true;
<%		
	}
	else if ("maybe3".equals(required))
	{
%>
	document.getElementById("maybe3_<%=100+i%>").checked = true;
<%		
	}
	else if ("true".equals(required))
	{
%>
	document.getElementById("required_<%=100+i%>").checked = true;
<%		
	}
	if ("tag".equals(tag))
	{
%>
	document.getElementById("tag_<%=100+i%>").checked = true;
<%		
	}
%>
</script>
<% } %>
</div>
<%  
	i++;
	} %>
<br>
<br>&nbsp;
    </div>
</div>
  
 </div><br><br>                
                                
                </p></dd>
<% } %>
<% // ------------- end job posting phrases -------------------------------------------------------------// %>
<% // ------------- begin template phrases -------------------------------------------------------------// %>
<%	if (templateName != null && templateName.length() > 0 && templatePhrases.size() > 0) { %>
                <dt onclick="currentPanel=''"><%=templateName%> Phrases &nbsp; &nbsp; V</dt>
                <dd> <p>

<% if (usestars) { %>
<table class="tableHeaderFive" >
<tbody>
<tr>
	<th colspan="2" class="kwdText">From '<%=templateName%>' <%=ttype%></th>
	<th class="kwdRating">Importance</th>
	<th nowrap class="kwdCbx">Must<br>Have</th>
	<th class="kwdCbx">Tag</th>
<% } else { %>
<table class="tableHeader" >
<tr>
	<th colspan="2" class="kwdText">From '<%=templateName%>' <%=ttype%></th>
	<th nowrap align=center class="kwdCbx">Nice to<br>Have</th>
	<th nowrap align=center class="kwdCbx">Should<br>Have</th>
	<th nowrap align=center class="kwdCbx">Must<br>Have</th>
	<th class="kwdCbx">Tag</th>
	<th class="kwdClear">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
<% } %>
</tr>
</tbody>
</table>
<div class="scrollbar1">
<div id='tt' class="ttip">
<table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>
<div id='popup' class="ttip"><table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>            	
<div id="logbox" style="border:solid 1px #00f;font-size:16px;height:20px;display:none">message bar for event logs</div>
<div id ="phrases">
<input type=hidden name="usestars" value="<%=usestars%>"/>
   <div id="suggestedphrases1">
	<b><!--input type=button onclick="clearPostingPhrases()" value="Clear selection"--></b>
	<div id="jobphraselist" >
<%
	for(UserTemplatePhrase utp : templatePhrases)
	{   
		String phrase = utp.getPhrase();
		//System.out.println("Template Phrase: " + phrase);
%>
	<div class="kwdRecord" id="kwd_<%=100+i%>" onmouseover="helperActions(this, 'show')" onmouseout="helperActions(this, 'hide')">
	<div class="kwdTools" nowrap>
		<input type="hidden" value="<%=phrase%>" id="tmplphrases_<%=100+i%>" name="tmplphrases_<%=100+i%>"/>
<%
		if (phrasesSynonyms != null && phrasesSynonyms.get(phrase.trim()) != null)
		{
			String synonymsStr = phrasesSynonyms.get(phrase.trim()).trim();
			if (synonymsStr.length() > 1)
			{
				String[] synonymns = synonymsStr.split("\\|");
				synonymsStr = synonymsStr.replace("|","_LIMIT_");
				int syncnt = synonymns.length;
				if (syncnt > 9) syncnt = 10;
%>
	<span class="iconSynonyms" id="syn_<%=100+i%>" onclick='getSynonymsPop(event, "<%=phrase%>", this, "tt");synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%			} else { %>
	<span class="iconGSynonyms" id="syn_<%=100+i%>" onclick='getSynonymsPop(event, "<%=phrase%>", this, "tt");synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%			} } else { %>
	<span class="iconGSynonyms" id="syn_<%=100+i%>" onclick='getSynonymsPop(event, "<%=phrase%>", this, "tt");synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%		} %>
<%	if (true || user.isAdmin() || user.isTester()) { %>
	<span class="iconDelete" onclick='deletePhrase(this, "<%=phrase%>", <%=100+i%>);' title="Remove Phrase"></span>
<%		} %>
	</div>
	<div class="kwdText" id="phrase<%=100+i%>" align=left>
		<%=phrase%>
	</div>
<% if (usestars) { %>
<div class="kwdRating">
	<div class="rateActions">
		<div id="rateMe" style="width:auto;float:left;" onmouseout="starRater(<%=100+i%>)">
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Plus to have" id="<%=100+i%>_1" onclick="starRater(this, 'save');setPhraseRating('tmplphrases_', <%=100+i%>, 1)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Nice to have" id="<%=100+i%>_2" onclick="starRater(this, 'save');setPhraseRating('tmplphrases_', <%=100+i%>, 2)" class=""></a>

		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Good to have" id="<%=100+i%>_3" onclick="starRater(this, 'save');setPhraseRating('tmplphrases_', <%=100+i%>, 3)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Should have" id="<%=100+i%>_4" onclick="starRater(this, 'save');setPhraseRating('tmplphrases_', <%=100+i%>, 4)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Important" id="<%=100+i%>_5" onclick="starRater(this, 'save');setPhraseRating('tmplphrases_', <%=100+i%>, 5)" class=""></a>
		<!--a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Must have" id="100_6" onclick="starRater(this, 'save')" class=""></a-->
		</div>
		<span style="display:none;" class="actionStatus" id="status<%=100+i%>"></span>
		<span style="float:right;" id="actions<%=100+i%>">
		<a onclick="clearStarRating(<%=100+i%>);setPhraseRating('tmplphrases_', <%=i%>, 0);return false;" href="#" style="display:none;" id="clear<%=100+i%>">Clear</a>

		<!-- a onclick="removeKeyphrase(100);return false;" href="#" style="display:none;" id="remove<%=100+i%>">Remove</a -->
		</span>
	</div>
	<input type="hidden" value="0" id="rating_<%=100+i%>" name="rating_<%=100+i%>"/>
	<!--input type="hidden" value="false" id="required_<%=100+i%>" name="required_<%=100+i%>" /-->
</div>
<div align=center class="kwdCbx"><input type="checkbox" value="true" id="required_<%=100+i%>" name="required_<%=100+i%>" onclick="checkRequired(this,<%=100+i%>,'tmplphrases_')"/><!--img id="reqimg_<%=100+i%>" src="star_off.gif" onclick="setRequired(this, <%=100+i%>)"/ --></div>
<div align=center class="kwdCbx"><input type="checkbox" value="tag" id="tag_<%=100+i%>" name="tag_<%=100+i%>" /></div>
<script>
<%
	int rating = utp.getScore();
	boolean required = utp.isRequired();
	boolean tag = utp.isTag();
	if (rating > 0 && rating < 6)
	{
%>
		var idStr = "" + (100 + <%=i%>) + "_" + <%=rating%>;
		var img = document.getElementById(idStr);
		if (img != null)
		{
			starRater(img, 'on')
			starRater(img, 'save');
		}
<%		
	}
	if (required)
	{
%>
	document.getElementById("required_<%=100+i%>").checked = true;
<%		
	}
	if (tag)
	{
%>
	document.getElementById("tag_<%=100+i%>").checked = true;
<%		
	}
%>
</script>
<% } else { %>
<div align=center class="kwdCbx"><input type="radio" value="maybe1" id="maybe1_<%=100+i%>" name="required_<%=100+i%>" onclick="setPhraseRating('tmplphrases_', <%=i%>, 1)"/></div>
<div align=center class="kwdCbx"><input type="radio" value="maybe3" id="maybe3_<%=100+i%>" name="required_<%=100+i%>" onclick="setPhraseRating('tmplphrases_', <%=i%>, 3)"/></div>
<div align=center class="kwdCbx"><input type="radio" value="true" id="required_<%=100+i%>" name="required_<%=100+i%>" onclick="setPhraseRating('tmplphrases_', <%=i%>, 6)"/></div>
<div align=center class="kwdCbx"><input type="checkbox" value="tag" id="tag_<%=100+i%>" name="tag_<%=100+i%>" /></div>
<div align=center class="kwdClear"><a href="javascript:deselect(<%=100+i%>)" id="clear_<%=100+i%>" style="display:none">Clear</a></div>
<script>
<%
	int rating = utp.getScore();
	boolean required = utp.isRequired();
	boolean tag = utp.isTag();
	if (rating < 3 && rating > 0)
	{
%>
	document.getElementById("maybe1_<%=100+i%>").checked = true;
<%		
	}
	else if (rating < 6 && rating > 0)
	{
%>
	document.getElementById("maybe3_<%=100+i%>").checked = true;
<%		
	}
	else if (required)
	{
%>
	document.getElementById("required_<%=100+i%>").checked = true;
<%		
	}
	if (tag)
	{
%>
	document.getElementById("tag_<%=100+i%>").checked = true;
<%		
	}
%>
</script>
<% } %>
</div>
<%  
	i++;
	} %>
<div colspan=4 align=center nowrap>
<br>
<br>
<% if (!systemTemplate) { %>
<a href="javascript:saveTemplate()" class="orangeBtn">Save Template</a>
<!--input type="button" id="resPhrasesBut" value="Select Resume Phrases" onclick="showResumePhrases(event, this)"  / -->
<!-- input type="button" id="keyNamesBut" value="Select Key Names" onclick="showTagList(event, this)"  / -->
<% } %>
</div>
<br>
<br>&nbsp;
    </div>
</div>
  
 </div>                
                                
                </p></dd>
<% } %>
<% // ------------- end template phrases -------------------------------------------------------------// %>
<%
	// Hidden fields for resumephrases
	for (int x = 0; x < 50; x++)
	{
%>
	<input type="hidden" value="" name="resumephrase_<%=100+i%>"/>
	<input type="hidden" value="0" name="rating_<%=100+i%>"/>
	<input type="hidden" name="required_<%=100+i%>" value="">
	<input type="hidden" name="tag_<%=100+i%>" value="" >
<%
		i++;
	}
%>
<% if (resumeFiles.size() > 0) { %>
                <dt onclick="refreshResumePhrases()">Resume Phrases &nbsp; &nbsp; V</dt>
               <dd>
<% if (usestars) { %>
	<table class="tableHeaderFive">
	<tr>
	<th class="kwdText">From Resumes</th>
	<th class="kwdRating">Importance</th>
	<th nowrap class="kwdCbx">Must<br>Have</th>
	<th class="kwdCbx">Tag</th>
<% } else { %>
	<table class="tableHeader">
	<tr>
	<th class="kwdText">From Resumes</th>
	<th nowrap align=center class="kwdCbx">Nice to<br>Have</th>
	<th nowrap align=center class="kwdCbx">Should<br>Have</th>
	<th nowrap align=center class="kwdCbx">Must<br>Have</th>
	<th class="kwdCbx">Tag</th>
	<th class="kwdClear">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
<% } %>
	</tr>
	</table>
<div class="scrollbar1">
<div id='rtt' class="ttip"><table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>
<div id ="phrases">
   <div id="resumephrases">
</div>
</div>

				</dd>
<% }  // if (resumeFiles.size() > 0) %>
                
				<dt onclick="currentPanel='u'">Your Phrases &nbsp; &nbsp; V</dt>
                <dd>
<div id='utt' class="ttip"><table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>
<% if (usestars) { %>
	<table class="tableHeaderFive">
	<tr>
	<th colspan="2" class="kwdText">Your Phrase</th>
	<th class="kwdRating">Importance</th>
	<th nowrap class="kwdCbx">Must<br>Have</th>
	<th class="kwdCbx">Tag</th>
<% } else { %>
	<table class="tableHeader">
	<tr>
	<th colspan="2" class="kwdText">Your Phrase</th>
	<th nowrap align=center class="kwdCbx">Nice to<br>Have</th>
	<th nowrap align=center class="kwdCbx">Should<br>Have</th>
	<th nowrap align=center class="kwdCbx">Must<br>Have</th>
	<th class="kwdCbx">Tag</th>
<% } %>
	</tr>
	</table>
<div class="scrollbar1">
<%	
	String disp = "inline";
	for (i = 0; i < 99; i++) 
	{
		if (i > 0) disp = "none";
%>
	<div id="userphraselist" width=100%>
	<div  class="kwdRecord" id="uptr_<%=i%>" style="display:<%=disp%>" onmouseover="helperActions(this, 'show')" onmouseout="helperActions(this, 'hide')">
	<div   class="kwdTools" align=left nowrap><img src="x.png" title="Remove Phrase" onclick="deleteUserPhrase(<%=i%>)">&nbsp;<img src="plus-gsquare.png" title="Equivalent Phrases" onclick="getUserSynonyms(event, <%=i%>, this);"></div>
	<!--span class="iconGSynonyms" onclick='getUserSynonyms(event, <%=i%>, this);' title="Equivalent Phrases"></span-->
	&nbsp;
	<div class="kwdText">
	<input type=text name="userphrase_<%=i%>" id="userphrase_<%=i%>" size=25  onkeypress="return checkenter(event, <%=i%>)" ></div>
<% if (usestars) { %>
<div class="kwdRating">
	<div class="rateActions">
		<div id="rateMe" style="width:auto;float:left;" onmouseout="starRater(<%=i%>)">
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Plus to have" id="<%=i%>_1" onclick="starRater(this, 'save');setPhraseRating('userphrase_', <%=i%>, 1)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Nice to have" id="<%=i%>_2" onclick="starRater(this, 'save');setPhraseRating('userphrase_', <%=i%>, 2)" class=""></a>

		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Good to have" id="<%=i%>_3" onclick="starRater(this, 'save');setPhraseRating('userphrase_', <%=i%>, 3)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Should have" id="<%=i%>_4" onclick="starRater(this, 'save');setPhraseRating('userphrase_', <%=i%>, 4)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Important" id="<%=i%>_5" onclick="starRater(this, 'save');setPhraseRating('userphrase_', <%=i%>, 5)" class=""></a>
		<!--a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Must have" id="100_6" onclick="starRater(this, 'save')" class=""></a-->
		</div>
		<span style="display:none;" class="actionStatus" id="status<%=i%>"></span>
		<span style="float:right;" id="actions<%=i%>">
		<a onclick="clearStarRating(<%=i%>);setPhraseRating('userphrase_', <%=i%>, 0);return false;" href="#" style="display:none;" id="clear<%=i%>">Clear</a>

		<!-- a onclick="removeKeyphrase(100);return false;" href="#" style="display:none;" id="remove<%=i%>">Remove</a -->
		</span>
	</div>
	<input type="hidden" value="0" id="rating_<%=i%>" name="rating_<%=i%>"/>
	<!--input type="hidden" value="false" id="required_<%=i%>" name="required_<%=i%>" /-->
</div>
	<div align=center class="kwdCbx">
	<input type=checkbox name="required_<%=i%>" id="required_<%=i%>" value="true"  onclick="checkRequired(this,<%=i%>,'userphrase_')"></div>
	<div class="kwdCbx" ><input type=checkbox name="tag_<%=i%>" id="tag_<%=i%>" value="tag" ></div>
<% } else { %>
<div  class="kwdCbx" align=center><input type="radio" value="maybe1" id="maybe1_<%=i%>" name="required_<%=i%>" onclick="setPhraseRating('userphrase_', <%=i%>, 1)"/></div>
<div  class="kwdCbx" align=center><input type="radio" value="maybe3" id="maybe3_<%=i%>" name="required_<%=i%>" onclick="setPhraseRating('userphrase_', <%=i%>, 3)"/></div>
<div  class="kwdCbx" align=center><input type="radio" value="true" id="required_<%=i%>" name="required_<%=i%>" onclick="setPhraseRating('userphrase_', <%=i%>, 6)"/></div>
<div  class="kwdCbx" align=center><input type="checkbox" value="tag" id="tag_<%=i%>" name="tag_<%=i%>" /></div>
<% } %>
	</div>
<% } %>
	<div>
	<div colspan=4 align=center nowrap>
	<a href="javascript:addUserPhrase()" class="orangeBtn">Add Phrase</a>
	<!--input type="button" id="resPhrasesBut" value="Select Resume Phrases" onclick="showResumePhrases(event, this)"  / -->
	<!-- input type="button" id="keyNamesBut" value="Select Key Names" onclick="showTagList(event, this)"  / -->
	</div>
	</div>
	</div>
	</div>

				</dd>
                
				
				<!-- dt>Other User Phrases &nbsp; &nbsp; V</dt>
                <dd><h2>From other users</h2><p>Cum sociis natoque penatibus et donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p></dd>
                
				<dt>Friends Phrases &nbsp; &nbsp; V</dt>
                <dd><h2>Your friends suggestions </h2><p>Cum sociis natoque penatibus et donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p></dd -->                
				<dt onclick="refreshAllPhrases()">All Phrases &nbsp; &nbsp; V</dt>
                <dd>
<% if (usestars) { %>
	<table class="tableHeaderFive">
	<tr>
	<th class="kwdText">All Selected Phrases</th>
	<th class="kwdRating">Importance</th>
	<th nowrap class="kwdCbx">Must<br>Have</th>
	<th class="kwdCbx">Tag</th>
<% } else { %>
	<table class="tableHeader">
	<tr>
	<th class="kwdText">All Selected Phrases</th>
	<th nowrap align=center class="kwdCbx">Nice to<br>Have</th>
	<th nowrap align=center class="kwdCbx">Should<br>Have</th>
	<th nowrap align=center class="kwdCbx">Must<br>Have</th>
	<th class="kwdCbx">Tag</th>
	<th class="kwdClear">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
<% } %>
	</tr>
	</table>
	<div class="scrollbar1">
	<div id ="phrases">
	   <div id="allphrases">
	</div>
	</div>
	<div colspan=4 align=center nowrap>
	<br>
	<br>
	&nbsp;<a href="javascript:refreshAllPhrases(true)" class="orangeBtn">Save As Template</a><input type=text id="templateName" name="templateName" value=""><span id="namemsg" style="display:none">Please Enter a Name for the Template</span><input type=checkbox id="overwriteTemplate"> Overwrite
	<!--input type="button" id="resPhrasesBut" value="Select Resume Phrases" onclick="showResumePhrases(event, this)"  / -->
	<!-- input type="button" id="keyNamesBut" value="Select Key Names" onclick="showTagList(event, this)"  / -->
	<br>
	<br>
	</div>
	</div>
				</dd>                
           </dl>
        </div>

<div id="ownphrase" style="clear:both; float:left; margin-top:30px;">
<% if (sourceDocument != null && showPosting) { %><font color=red>See the job posting for</font> : <a href="javascript:showJobPosting()" style="color:blue"><%=postingId%></a><br><% } %>

<% if (usestars) { %>
	<table class="tableHeaderFive" id="userphraselist2" >
	<tr>
	<th align=left class="kwdText">Enter your own phrase</th>
	<th align=left class="kwdRating">Importance</th>
	<th align=right class="kwdCbx">Must<br>Have</th>
	<th align=right class="kwdCbx">Tag</th>
<% } else { %>
	<table class="tableHeader" id="userphraselist2" >
	<tr>
	<th align=left class="kwdText">Enter your own phrase</th>
	<th align=center nowrap class="kwdCbx">Nice to<br>Have</th>
	<th align=center nowrap class="kwdCbx">Should<br>Have</th>
	<th align=center nowrap class="kwdCbx">Must<br>Have</th>
	<th align=center class="kwdCbx">Tag</th>
<% } %>
	</tr>
	</table>
<%	
	disp = "inline";
	for (i = 99; i < 100; i++) 
	{
%>
	<div  id="uptr_<%=i%>" onmouseover="helperActions(this, 'show')" onmouseout="helperActions(this, 'hide')">
	<div class="kwdTools" align=left valign=top nowrap>
	<!--span class="iconAddPhrase"  alt="Add" onclick='addToUserPhrase(<%=i%>)"' title="Add Phrase"></span -->

	<img align=top src="images/add.png" alt="Add" title="Add Phrase" onclick="addToUserPhrase(<%=i%>)"></div>
	<div class="kwdText"><input type=text name="userphrase_<%=i%>" id="userphrase_<%=i%>" size=25  onkeypress="return checkenter(event, <%=i%>)" ></div>
<% if (usestars) { %>
<div class="kwdRating">
	<div class="rateActions">
		<div id="rateMe" style="width:auto;float:left;" onmouseout="starRater(<%=i%>)">
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Plus to have" id="<%=i%>_1" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Nice to have" id="<%=i%>_2" onclick="starRater(this, 'save')" class=""></a>

		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Good to have" id="<%=i%>_3" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Should have" id="<%=i%>_4" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Important" id="<%=i%>_5" onclick="starRater(this, 'save')" class=""></a>
		<!--a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Must have" id="100_6" onclick="starRater(this, 'save')" class=""></a-->
		</div>
		<span style="display:none;" class="actionStatus" id="status<%=i%>"></span>
		<span style="float:right;" id="actions<%=i%>">
		<a onclick="clearStarRating(<%=i%>);return false;" href="#" style="display:none;" id="clear<%=i%>">Clear</a>

		<!-- a onclick="removeKeyphrase(100);return false;" href="#" style="display:none;" id="remove<%=i%>">Remove</a -->
		</span>
	</div>
	<input type="hidden" value="0" id="rating_<%=i%>" name="rating_<%=i%>"/>
	<!--input type="hidden" value="false" id="required_<%=i%>" name="required_<%=i%>" /-->
</div>
	<div align=center class="kwdCbx"><!--img id="reqimg_<%=i%>" src="star_off.gif" onclick="setRequired(this, <%=i%>)"/-->
	<input type=checkbox name="required_<%=i%>" id="required_<%=i%>" value="true"  onclick="checkRequired(this)"></div>
	<div align=center  class="kwdCbx"><input type=checkbox name="tag_<%=i%>" id="tag_<%=i%>" value="tag" ></div>
<% } else { %>
<div   class="kwdCbx" align=center><input type="radio" value="maybe1" id="maybe1_<%=i%>" name="required_<%=i%>"/></div>
<div   class="kwdCbx" align=center><input type="radio" value="maybe3" id="maybe3_<%=i%>" name="required_<%=i%>"/></div>
<div   class="kwdCbx" align=center><input type="radio" value="true" id="required_<%=i%>" name="required_<%=i%>"/></div>
<div   class="kwdCbx" align=center><input type="checkbox" value="tag" id="tag_<%=i%>" name="tag_<%=i%>" /></div>
<% } %>
	</div>
<% } %>
</div>
	 <div style="width:120px;float:right;margin-top:10px">
		<a href="http://youtu.be/DSlmXM0eEoA" rel="prettyPhoto" title=""><img src="images/demo_icon.png" width=120px  /></a>
	</div>

                
			</div>
      		 </div>
  			                      

  		</div>
<!-- End SmartWizard Content -->
</td></tr>
</table> 
<div class="clear"></div>
<div>
<br/>
<!--a href="javascript:saveSession()" class="orangeBtn">Save Session</a -->
<% if (usestars) { %>
&nbsp;&nbsp;&nbsp;<a href="javascript:useStarSelection(false)" class="orangeBtn">Simple Rating</a>
<% } else { %>
&nbsp;&nbsp;&nbsp;<a href="javascript:useStarSelection(true)" class="orangeBtn">5 Star Rating</a>
<% } %>
	<a href="javascript:doSubmit()" class="orangeBtn"><% if (resumeFiles.size() > 0) { %>Rank Resumes<%	} else { %>Save Phrases<% } %></a>
<% if (savedSessions.size() > 0) { %>
<b>Saved Sessions:&nbsp;</b>
<% 
	int j = 0;
	for (int s = savedSessions.size()-1; s >= 0; s--)
	{
		long stamp = (Long)savedSessions.get(s)[0];
		Date time = new Date(stamp);
		j++;
%>
	<a href="#" onclick="restoreSession(<%=stamp%>)" style="border:1px solid black;display:inline-block;padding:2px" title="<%=time%>"><%=j%></a><a href="#" onclick="deleteSession(<%=stamp%>,<%=j%>)" ><img src=x.png></a>&nbsp;
<% 
	}
%>
</h2>
<% } %>
</div>
<div class="clear"></div>
</div>
</div>
    </div>
</form>
<%@include file="footer.jsp"%>
<script>
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g,"");
	}
	var nophrases = 1;
	function addUserPhrase()
	{
		var num = addNewPhrase();
	}
	function addToUserPhrase()
	{
		var phrase = document.getElementById("userphrase_99").value;
		var addrating = document.getElementById("rating_99");
		var addmaybe1 = document.getElementById("maybe1_99");
		var addmaybe3 = document.getElementById("maybe3_99");
		var addrequired = document.getElementById("required_99");
		var addtag = document.getElementById("tag_99");
		var rated = false;
		var rating = 0;
		if (addrating != null && addrating.value != "0")
		{
			rated = true;
			rating = addrating.value;
		}
		if (addmaybe1 != null && addmaybe1.checked)
		{
			rated = true;
			rating = 1;
		}
		if (addmaybe3 != null && addmaybe3.checked)
		{
			rated = true;
			rating = 3;
		}
		if (addrequired != null && addrequired.checked)
		{
			rated = true;
			rating = 6;
		}
		if (!rated && !addrequired.checked && !addtag.checked)
		{
			alert("Please rate or tag the phrase to add");
			return;
		}
		var num = addNewPhrase(phrase);
		if (addrating != null) document.getElementById("rating_" + num).value = addrating.value;
		if (addrequired.checked) document.getElementById("required_" + num).checked = true;
		if (addmaybe1 != null && addmaybe1.checked) document.getElementById("maybe1_" + num).checked = true;
		if (addmaybe3 != null && addmaybe3.checked) document.getElementById("maybe3_" + num).checked = true;
		if (addtag.checked) document.getElementById("tag_" + num).checked = true;
		if (<%=usestars%>)
		{
			var idStr = "" + num + "_" + addrating.value;
			img = document.getElementById(idStr);
			if (img != null)
			{
				starRater(img, 'on')
				starRater(img, 'save');
			}
			if (addrating != null) document.getElementById("rating_" + num).value = addrating.value;
			var idStr = "" + 99 + "_" + addrating.value;
			img = document.getElementById(idStr);
			if (img != null)
			{
				starRater(img, 'off')
				starRater(img, 'save');
			}
		}
		setPhraseRating('userphrase_', num, rating);
		document.getElementById("userphrase_99").value = "";
		if (addmaybe1 != null) document.getElementById("maybe1_99").checked = false;
		if (addmaybe3 != null) document.getElementById("maybe3_99").checked = false;
		if (addrequired != null) document.getElementById("required_99").checked = false;
		if (addtag != null) document.getElementById("tag_99").checked = false;
	}
	function addNewPhrase(phrase)
	{
		var uptr = document.getElementById("uptr_" + (nophrases-1));
		var inuptr = document.getElementById("userphrase_" + (nophrases-1));
		if (uptr.style.display == 'none' || inuptr.value == "")
		{
			uptr.style.display = 'inline';
			inuptr.value = "";
			if (phrase != null)
			{
				inuptr.value = phrase;
			}
			inuptr.focus();
			return (nophrases-1);
		}
		uptr = document.getElementById("uptr_" + nophrases);
		if (uptr != null)
		{
			uptr.style.display = 'inline';
			var inuptr = document.getElementById("userphrase_" + nophrases);
			inuptr.value = "";
			if (phrase != null)
			{
				inuptr.value = phrase;
			}
			inuptr.focus();
			nophrases++;
			return (nophrases-1);
		}
	}
	function deleteUserPhrase(num)
	{
		var uptr = document.getElementById("uptr_" + num);
		if (uptr != null)
		{
			uptr.style.display = 'none';
			var inuptr = document.getElementById("userphrase_" + num);
			inuptr.value = "";
		}
		clearStarRating(num);
	}

	function checkenter(event, num)
	{
		if (event == null)
			event = window.event;
		var keyCode = event.keyCode
		if (keyCode == null)
			keyCode = event.which;
		if(keyCode == 13)
		{
			if (num == nophrases-1)
			{
				addNewPhrase();
			}
			else
			{
				var uptr = document.getElementById("uptr_" + (num+1));
				var inuptr = document.getElementById("userphrase_" + (num+1));
				if (uptr != null)
				{
					uptr.style.display = 'inline';
				}
				if (inuptr != null)
				{
					inuptr.focus();
				}
			}
			return true;
		}
		return true;
	}

	function addResumePhrase(phrase, idx)
	{
		var uptr = document.getElementById("uptr_" + (nophrases-1));
		var inuptr = document.getElementById("userphrase_" + (nophrases-1));
		if (uptr.style.display != 'none' && inuptr.value == '')
		{
			inuptr.value = phrase;
		}
		else
			addNewPhrase(phrase);
		var resPhraseTr = document.getElementById("resPhrase" + idx);
		if (resPhraseTr != null)
		{
			resPhraseTr.style.display = 'none';
		}
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_resume_phrase.jsp?phrase=" + escape(phrase);
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
	}

	function addKeyName(phrase, idx)
	{
		var uptr = document.getElementById("uptr_" + (nophrases-1));
		var inuptr = document.getElementById("userphrase_" + (nophrases-1));
		var idx = 0;
		if (uptr.style.display != 'none' && inuptr.value == '')
		{
			inuptr.value = phrase;
			idx = nophrases-1;
		}
		else
		{
			idx = addNewPhrase(phrase);
		}
		var tagptr = document.getElementById("tag_" + idx);
		if (tagptr != null)
		{
			tagptr.checked = true;
		}
	}
	function deleteResumePhrase(phrase, idx)
	{
		if (!confirm("Do you want to mark  '" + phrase + "' as invalid and remove it from processing in the future?"))
		{
			return;
		}
		var resPhraseTr = document.getElementById("resPhrase" + idx);
		if (resPhraseTr != null)
		{
			resPhraseTr.style.display = 'none';
		}
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_resume_phrase.jsp?permanent=true&phrase=" + escape(phrase);
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
	}
	var saveObj;
	var savePhrase;
	var saveIdx;
	function deletePhrase(obj, phrase, idx)
	{
		var delet = true;
		var phraseTd = document.getElementById("phrase" + idx);
		if (phraseTd != null && (phraseTd.style.textDecoration == 'none' || phraseTd.style.textDecoration == ''))
		{
		}
		else
		{
			delet = false;
		}
		document.getElementById("dialogtext").innerHTML = "Do you want to mark  '" + phrase + "' as invalid and remove it from processing in the future?";
		//if (delet && !confirm("Do you want to mark  '" + phrase + "' as invalid and remove it from processing in the future?"))
		//{
		//	return;
		//}
		saveObj = obj;
		savePhrase = phrase;
		saveIdx = idx;
		if (delet)
		{
			$('#dialog').dialog('open');
		}
		else
		{
			deletePhrase2();
		}
	}
	function closeDialog()
	{
		$('#dialog').dialog('close');
	}
	
	function deletePhrase2()
	{
		closeDialog();
		obj = saveObj;
		phrase = savePhrase;
		idx = saveIdx;
		var delet = true;
		var phraseTd = document.getElementById("phrase" + idx);
		if (phraseTd != null && (phraseTd.style.textDecoration == 'none' || phraseTd.style.textDecoration == ''))
		{
			phraseTd.style.textDecoration = 'line-through';
			clearSelection(idx);
		}
		else
		{
			phraseTd.style.textDecoration = 'none';
			delet = false;
		}
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_jobposting_phrase.jsp?phrase=" + escape(phrase) + "&delete=" + delet;
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
		//alert(obj.style + ":" + obj.style.name);
		//obj.class = 'iconAdd';

	}
	
	function getUserSynonyms(event, idx, img)
	{
		var phrase = document.getElementById("userphrase_" + idx);
		if (phrase == null) return;
		if (phrase.value == '')
		{
			alert("Please enter a phrase");
			return;
		}
		synPhrase = phrase.value;
		getSynonymsPop(event, phrase.value, this, 'utt');
	}

	function checkAddenter(event)
	{
		if (event == null)
			event = window.event;
		var keyCode = event.keyCode
		if (keyCode == null)
			keyCode = event.which;
		if(keyCode == 13)
		{
			addSynonym(synPhrase);
			return true;
		}
		return true;
	}
	var synonyms;
	function addSynonym(phrase) {
		var inp = document.getElementById("AddSynonym");
		if (inp == null) return;
		var synonym = inp.value;
		if (synonym == '')
		{
			alert("Please enter a synonym");
			return;
		}
		var inv = synonym.replace(/[^0-9a-z ]/gi,'') 
		if (inv != synonym)
		{
			alert("Please enter letters or numbers only");
			return;
		}
		if (phrase.toLowerCase() == synonym.toLowerCase())
		{
			alert("Synonym is same as the Phrase");
			return;
		}
		var list = document.getElementById("synonymList");
		if (list != null)
		{
			var oldsynonyms = list.value;
			oldsynonyms = oldsynonyms.split("|");
			for (var i = 0; i < oldsynonyms.length ; i++)
			{
				if (oldsynonyms[i].toLowerCase() == synonym.toLowerCase())
				{
					alert("Equivalent phrase already exists");
					return;
				}
			}
		}
		var command = "add";
		var url = "add_synonym.jsp";
		url = url + "?phrase=" + phrase;
		url = url + "&synonym=" + synonym;
		url = url + "&command=" + command;
		if (xmlhttp == null)
		{
			xmlhttp = GetXmlHttpObject();
		}
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
		var resp = xmlhttp.responseText;
		if (resp != null)
		{
			inp.value = "";
		}
		getSynonymsPop(null, phrase, null, currentPanel + 'tt');
	}

	function getSynonymsPop(evt, phrase, image, panel)
	{
		//if (!isIE && (evt == null))
		//	return;
		var tt = document.getElementById(panel);
		if (tt == null)
			return false;
		var x = tt.style.left;
		var y = tt.style.top;
		var event = isIE ? window.event : evt;
		if (event != null)
		{
			x = document.body.scrollLeft + (isIE ? event.x : event.clientX);
			y = document.body.scrollTop + (isIE ? event.y : event.clientY);
			if (isIE) {
				x = event.clientX + document.body.scrollLeft;
				y = event.clientY + document.body.scrollTop;
			}
			var eventx = x;
			var eventy = y;

			if (!isIE)
			{
				x = x - 25 - 120;
				y = y - 80 - 125;
				if (!isChrome)
					y = y + window.pageYOffset;
			}
			else
			{
				x = x - 20;
				y = y - 75;
				y = y + getScrollXY()[1];
			}
		}
		var url = "synonyms_for_phrase.jsp?random=" + new Date() + "&phrase=" + escape(phrase);
		$.ajax({         
			url: url,         
			type: 'GET',         
			async: false,         
			cache: false,         
			timeout: 30000,         
			error: function(){return true;},
			success: function(response){
				//alert(response);
				if(response != null)
				{
					$('#phrasespop').html(response);
					$('#phrasespop').dialog('open');
					return response;
					panelTooltip(panel, response, x, y);
					if (image != null)
					{
						resetSynDelImg();
						synDelImg = image;
						synDelImgSrc = image.src;
						var ind = synDelImgSrc.indexOf("-");
						if (ind != -1)
							image.src = "minus" + synDelImgSrc.substring(ind);
					}
				}
				else
				{
					alert('server response is null' );
				}

			}
		});
	}

	function closeSynonymPopups()
	{
		var tt = document.getElementById('tt');
		if (tt != null)
		{
			tt.innerHTML = "";
			tt.style.display = "none";
		}
		tt = document.getElementById('rtt');
		if (tt != null)
		{
			tt.innerHTML = "";
			tt.style.display = "none";
		}
		tt = document.getElementById('utt');
		if (tt != null)
		{
			tt.innerHTML = "";
			tt.style.display = "none";
		}
	}

	function panelTooltip(panel, tableHtml, x, y) {
		closeSynonymPopups();
		var tt = document.getElementById(panel);
		if (tt == null)
			return false;
		tt.innerHTML = tableHtml;
		tt.style.left = x +'px';
		tt.style.top = y + 'px';
		tt.style.display = "inline";
		try {             
			//tt.style.display = "inline";
			//var xpos = tt.style.left;
			//xpos = xpos.replace("px","");
			//xpos = parseInt(xpos);
			//xpos = xpos - 325;
			//xpos = "" + xpos + "px";
			//tt.style.left = xpos;
		}catch(e) {
		}
		//alert(tt.innerHTML);
	}
	function stthide() {
		var tt = document.getElementById('tt');
		if (tt != null)
			tt.style.display = "none";
		tt = document.getElementById('popup');
		if (tt != null)
			tt.style.display = "none";
		tt = document.getElementById('rtt');
		if (tt != null)
			tt.style.display = "none";
		tt = document.getElementById('utt');
		if (tt != null)
			tt.style.display = "none";
	}

	//var resumePhrases = false;
	function showResumePhrases(event, button)
	{
		if (resumePhrases)
		{
			 closeResumePhrases();
			 return;
		}
		resumePhrases = true;
		tagType = "";
		showResumePhrasesPopup(event);
		button.value="Close Pop Up";
	}

	function showResumePhrasesPopup(evt)
	{

		var url = "get_resume_phrases.jsp?openTag=" + tagType + "&random=" + new Date();
		new Ajax.Request(url, { 
			method:'get',
			parameters : '',
			onSuccess: function(transport){
				
				var response = transport.responseText;
				if(response != null)
				{
					var respop = document.getElementById('respop');
					if (respop != null)
					{
						respop.innerHTML = response;
						respop.style.display = "inline";
						respop.style.maxHeight = '400px';
						respop.style.overflow = 'auto';
					}
					//alert(response);
				}
				else
				{
					alert('server response is null' );
				}
			}
		});
	}

	function openCompanies(hide)
	{
		if (hide)
			tagType = "";
		else
			tagType = "Company";
		showResumePhrasesPopup();
	}
	function openUniversities(hide)
	{
		if (hide)
			tagType = "";
		else
			tagType = "University";
		showResumePhrasesPopup();
	}
	function closeResumePhrases()
	{
		var button = document.getElementById("resPhrasesBut");
		button.value="Select from Resume Phrases";
		var rpop = document.getElementById('respop');
		rpop.style.display = "none";
		resumePhrases = false;
	}
	var tagList = false;
	function showTagList(event, button)
	{
		if (tagList)
		{
			 closeResumePhrases();
			 return;
		}
		if (resumePhrases)
		{
			 closeResumePhrases();
		}
		tagList = true;
		showTagListPopup(event);
		button.value="Close Pop Up";
	}
	function deselect(num)
	{
		var musthave = document.getElementById("required_" + num);
		if (musthave != null) musthave.checked = false;
		var maybe = document.getElementById("maybe1_" + num);
		if (maybe != null) maybe.checked = false;
		maybe = document.getElementById("maybe3_" + num);
		if (maybe != null) maybe.checked = false;
		var tag = document.getElementById("tag_" + num);
		if (tag != null) tag.checked = false;
	}

	function clearPostingPhrases()
	{
		if (!confirm("Are you sure you want to clear all selected Job Posting phrases and their ratings?"))
		{
			return;
		}
		for (var x = 0; x < 100 ; x++)
		{
			var id = 100+x;
			var idStr = "suggphrases_" + id;
			var tr = document.getElementById(idStr);
			if (tr != null)
			{
				clearStarRating(id)
				var req = document.getElementById("required_" + id);
				if (req != null) req.checked = false;
				var tag = document.getElementById("tag_" + id);
				if (tag != null) tag.checked = false;
			}
			else
				break;
		}
		//var button = document.getElementById("restoreButton");
		//if (button != null)
		//	button.style.display = 'inline';
	}
	function clearUserPhrases()
	{
		if (!confirm("Are you sure you want to clear all Your Entered phrases and their ratings?"))
		{
			return;
		}
		for (var x = 0; x < 100 ; x++ )
		{
			var tr = document.getElementById("uptr_" + x);
			if (tr != null)
			{
				clearStarRating(x);
				var inuptr = document.getElementById("userphrase_" + x);
				if (inuptr != null)
					inuptr.value = "";
				if (x > 0)
					tr.style.display = 'none';
				var req = document.getElementById("required_" + x);
				if (req != null) req.checked = false;
				var tag = document.getElementById("tag_" + x);
				if (tag != null) tag.checked = false;
			}
			else
				break;
		}
		//var button = document.getElementById("restoreButton");
		//if (button != null)
		//	button.style.display = 'inline';
	}
	function restoreSession(stamp)
	{
		window.location="select_phrases.jsp?command=restoreSession&sessStamp=" +stamp;
	}
	function deleteSession(stamp, x)
	{
		if (!confirm("Are you sure you want to delete session " + x + "?"))
		{
			return;
		}
		window.location="select_phrases.jsp?command=deleteSession&sessStamp=" +stamp;
	}
	function saveSession()
	{
		document.phrasesform.action = "select_phrases.jsp?command=saveSession" ;
		var result = submitSelection();
		if (!result)
		{
			document.phrasesform.action = "Analyzer" ; // restore on error
		};
	}
	var rallbusy = false;
	function saveTemplate()
	{
		if (!confirm("Note: Selected Phrases from Other Panels will also be added to this Template.\nClick OK to Continue"))
		{
			return;
		}
		document.getElementById("templateName").value = '<%=templateName%>';
		document.getElementById("overwriteTemplate").checked = true;
		refreshAllPhrases(true);
		document.getElementById("templateName").value = '';
		document.getElementById("overwriteTemplate").checked = false;
	}
	var nonselected = true;
	function refreshAllPhrases(save)
	{
		if (rallbusy) return;
		var name = document.getElementById("templateName").value;
		if (save)
		{
			if (name == "")
			{
				alert("Please enter a name");
				document.getElementById("namemsg").style.display = "inline";
				document.getElementById("templateName").focus();
				return;
			}
			document.getElementById("namemsg").style.display = "none";
		}
		rallbusy = true;
		updateResumePhraseSelections();
		document.getElementById("loading").style.visibility = "visible";
		var form = document.phrasesform;
		var url = "all_phrases.jsp?usestars=<%=usestars%>&random=" + new Date();
		if (save)
		{
			url = url + "&save=true&templateName=" + name;
			if (document.getElementById("overwriteTemplate").checked)
			{
				url = url + "&overwrite=true";
			}
		}
		var formdata = $(form).serialize()
			//alert(formdata);
		 $.ajax({         
				url: url,         
				type: 'post',
				data : formdata,
				async: false,         
				cache: false,         
				timeout: 30000,         
				error: function(){
					if (save)
					{
						alert('Error Saving Template, one with same name may already exist' );
					}
					else
					{
						alert('Error Refreshing All Phrases' );
					}
					document.getElementById("loading").style.visibility = "hidden";
					rallbusy = false;
					return true;},
				success: function(response){
					rallbusy = false;
					document.getElementById("loading").style.visibility = "hidden";
					//alert(response);
					if(response != null)
					{
						nonselected = response.length < 100 || (response.indexOf("No phrases selected") != -1);
						if (nonselected)
						{
							alert("No phrases have been selected");
						}
						else if (save)
						{
							alert("Template was saved");
						}
						var phrase_div= document.getElementById("allphrases");
						phrase_div.innerHTML = response;
					}
					else
					{
						if (save)
						{
							alert('Error Saving Template' );
						}
						else
							alert('Error refreshing all phrases' );
					}
				}
		});
	}
	function useStarSelection(yesno)
	{
		document.phrasesform.action = "select_phrases.jsp?usestars=" + yesno;
		var result = submitSelection();
		//alert(result);
		if (!result)
		{
			document.phrasesform.action = "Analyzer" ; // restore on error
		};
	}
	function refresh()
	{
		document.phrasesform.action = "select_phrases.jsp?usestars=<%=usestars%>";
		var result = submitSelection();
		//alert(result);
		if (!result)
		{
			document.phrasesform.action = "Analyzer" ; // restore on error
		};
	}
	function unSelect(ind)
	{
		var req =document.getElementById("required_" + ind);
		if (req != null)
			req.checked=false;
		var reqimg =document.getElementById("reqimg_" + ind);
		if (reqimg != null)
			reqimg.src="star_off.gif";
		clearRating(ind);
	}
	function checkRequired(cbx, idx, type)
	{
		var id = cbx.id.split("_");
		var ind = id[1];
		if (cbx.checked)
		{
			cbx.value = true;
			clearStarRating(ind);
			if (idx != null)
			{
				setPhraseRating(type, idx, 6);
			}
		}
		else
		{
		}
	}
	function setRequired(image, ind)
	{
		var url = image.src;
		if (url.indexOf("star_off") != -1)
		{
			image.src = "star_on.gif";
			var req =document.getElementById("required_" + ind);
			req.checked=true;
			clearStarRating(ind);
		}
		else
		{
			image.src = "star_off.gif";
			var req =document.getElementById("required_" + ind);
			req.checked=false;
		}
	}
	function clearRequired2(ind)
	{
		return;
		var image = document.getElementById("reqimg2_" + ind);
		if (image == null) return;
		var url = image.src;
		if (url.indexOf("star_on") != -1)
		{
			image.src = "star_off.gif";
			var req =document.getElementById("required2_" + ind);
			req.value="false";
		}
	}
	function setRequired2(ind)
	{
		return;
		var image = document.getElementById("reqimg2_" + ind);
		if (image == null) return;
		var url = image.src;
		if (url.indexOf("star_off") != -1)
		{
			image.src = "star_on.gif";
			var req =document.getElementById("required2_" + ind);
			req.value="true";
		}
	}
	function clearSelection(ind)
	{
<% if (usestars) { %>
		clearStarRating(ind);
<% } else { %>
		deselect(ind);
<% } %>
		var image = document.getElementById("reqimg_" + ind);
		if (image != null)
			image.src = "star_off.gif";
		var req =document.getElementById("required_" + ind);
		if (req != null)
			req.checked=false;
		var clear = document.getElementById("clear" + ind);
		if (clear != null)
		{
			clear.style.visibility='hidden';
		}
		var tag = document.getElementById("tag_" + num);
		if (tag != null) tag.checked = false;
	}

	function selectAll(obj)
	{
		var check = true;
		if (!obj.checked)
			check = false;

<%	
	for (int j = 0; j < displayedJobPhrases.size() ; j++)
	{
		if (usestars)
		{
%>
		var idStr = "" + (100 + <%=j%>) + "_3";
		var img = document.getElementById(idStr);
		if (img != null)
		{
			if(check)
				starRater(img, 'on')
			else
				starRater(img, 'off')
			starRater(img, 'save');
		}
<%
		}
		else
		{
%>
		var maybe = document.getElementById("maybe3_" + (100+<%=j%>));
		if (maybe != null) maybe.checked = check;
<%
		}
	}
%>
	}

	function setPhraseRating(type, idx, value)
	{
		//alert("type=" + type + " idx=" + idx + " value=" + value);
		var pobj = document.getElementById(type + idx);
		if (pobj == null || true) return;
		var phrase = pobj.value;
		if (phrase == "") return;
		// Job Phrases
		if (type != "suggphrases_")
		{
			for (var i = 100;;i++)
			{
				var jobphrase = document.getElementById("suggphrases_" + i);
				if (jobphrase == null) break;
				if (phrase == jobphrase.value || (phrase + " ") == jobphrase.value)
				{
	<%		if (usestars) {%>
					if (value > 0 && value < 6)
					{
						var idStr = "" + i + "_" + value;
						var img = document.getElementById(idStr);
						if (img != null)
						{
							starRater(img, 'on')
							starRater(img, 'save');
						}
					}
					else
					{
						clearStarRating(i);
					}
					if (value == 6)
					{
						document.getElementById("required_" + i).checked = true;
					}
	<%		} else { %>
					if (value == 1)
					{
						document.getElementById("maybe1_" + i).checked = true;
					}
					if (value == 3)
					{
						document.getElementById("maybe3_" + i).checked = true;
					}
					if (value == 6)
					{
						document.getElementById("required_" + i).checked = true;
					}
					
	<%		} %>
					break;
				}
			}
		}
		// User's Phrases
		//	alert("type=" + type + " phrase:" + phrase + " value:" + value);
		if (type != "userphrase_")
		{
			for (var i = 0; i < 100;i++)
			{
				var userphrase = document.getElementById("userphrase_" + i);
				if (userphrase == null) break;
				//alert("phrase:" + phrase + " userphrase:" + userphrase.value + " value:" + value);
				if (phrase == userphrase.value || (phrase + " ") == userphrase.value || phrase == (userphrase.value + ' '))
				{
	<%		if (usestars) {%>
					if (value > 0 && value < 6)
					{
						var idStr = "" + i + "_" + value;
						var img = document.getElementById(idStr);
						if (img != null)
						{
							starRater(img, 'on')
							starRater(img, 'save');
						}
					}
					else
					{
						clearStarRating(i);
					}
					if (value == 6)
					{
						document.getElementById("required_" + i).checked = true;
					}
	<%		} else { %>
					if (value == 1)
					{
						document.getElementById("maybe1_" + i).checked = true;
					}
					if (value == 3)
					{
						document.getElementById("maybe3_" + i).checked = true;
					}
					if (value == 6)
					{
						document.getElementById("required_" + i).checked = true;
					}
					
	<%		} %>
					break;
				}
			}
		}
		// Resume Phrases
		if (type != "resumephrase_")
		{
			var start = <%=phraseList.size()%>+<%=templatePhrases.size()%>+150;
			for (var i = start;;i++)
			{
				var resumephrase = document.getElementById("resumephrase_" + i);
				if (resumephrase == null) break;
				if (phrase == resumephrase.value || (phrase + " ") == resumephrase.value || phrase == (resumephrase.value + ' '))
				{
	<%		if (usestars) {%>
					if (value > 0 && value < 6)
					{
						var idStr = "" + i + "_" + value;
						var img = document.getElementById(idStr);
						if (img != null)
						{
							starRater(img, 'on')
							starRater(img, 'save');
						}
					}
					else
					{
						clearStarRating(i);
					}
					if (value == 6)
					{
						document.getElementById("required_" + i).checked = true;
					}
	<%		} else { %>
					if (value == 1)
					{
						document.getElementById("maybe1_" + i).checked = true;
					}
					if (value == 3)
					{
						document.getElementById("maybe3_" + i).checked = true;
					}
					if (value == 6)
					{
						document.getElementById("required_" + i).checked = true;
					}
					
	<%		} %>
					break;
				}
			}
		}
	}


	function contains(a, obj) 
	{     
		for (var i = 0; i < a.length; i++) 
		{         
			if (a[i] === obj) 
				return true;         
		}     
		return false; 
	}
	function doSubmit()
	{
		var fail = submitSelection();
	}
	function submitSelection()
	{
		var selPhrases = new Array();
		var elems = document.phrasesform.elements;
		var selected = false;
		var unrated = "";
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name.indexOf("suggphrases_") == 0 || name.indexOf("tmplphrases_") == 0)
			{
				var id =  elems[i].id.split('_');
				var rating = document.getElementById('rating_'+id[1]);
				var tag = document.getElementById('tag_'+id[1]);
				var maybe1 = document.getElementById('maybe1_'+id[1]);
				var maybe3 = document.getElementById('maybe3_'+id[1]);
				if (document.getElementById('required_'+id[1]).checked 
					|| (rating != null && rating.value != 0) 
					|| (maybe1 != null && maybe1.checked)
					|| (tag != null && tag.checked)
					|| (maybe3 != null && maybe3.checked))
				{
					selected = true;
					var str = new String(elems[i].value).toLowerCase();
					selPhrases.push(str);
				}
			}
			if (name.indexOf("userphrase_") == 0 && elems[i].value != "")
			{
				var str = new String(elems[i].value).toLowerCase();
				if (contains(selPhrases, str) || contains(selPhrases, str + " "))
				{
					alert("Phrase:'" + elems[i].value +"' is already in the list. Please remove it.");
					return false;
				}
				var idx = name.substring(11);
				var rating = document.getElementById("rating_" + idx);
				var maybe1 = document.getElementById('maybe1_' + idx);
				var maybe3 = document.getElementById('maybe3_' + idx);
				var required = document.getElementById("required_" + idx);
				var tagged = document.getElementById("tag_" + idx);
				var rated = false;
				if (rating != null && rating.value != "0")
				{
					rated = true;
				}
				if (maybe1 != null && maybe1.checked) rated = true;
				if (maybe3 != null && maybe3.checked) rated = true;
				if (tagged != null && tagged.checked) rated = true;
				//alert(elems[i].value + " rated=" +  rated + " required=" + required.checked + " tagged:" + tagged.checked);
				if (!rated && !required.checked && !required.checked)
				{
					unrated = unrated + "," + "'" + elems[i].value +"'";
				}
				selPhrases.push(str);
			}
		}
		updateResumePhraseSelections();
		var action = document.phrasesform.action;
		if (unrated != '' && action.indexOf('Analyzer') != -1)
		{
			alert("Please rate phrases:" + unrated.substring(1) + "\n(in Your Phrases)");
			return false;
		}
		if (!selected && action.indexOf('Analyzer') != -1)
		{
			alert("Please select some phrases")
			return false;
		}
		if (selected && action.indexOf('Analyzer') == -1)
		{
			if (!confirm("Your phrase and rating selection will be lost if you switch rating mode. Continue?"))
				return false;
		}
		document.getElementById("loading").style.visibility = "visible";
		document.phrasesform.submit();
		return true;
	}
	function updateResumePhraseSelections()
	{
		var x = <%=phraseList.size()%>+<%=templatePhrases.size()%>+150;
		var y = <%=phraseList.size()%>+<%=templatePhrases.size()%>+100;
		var z = <%=phraseList.size()%>+<%=templatePhrases.size()%>+100;
		for (var i = 0; i < 50; i++)
		{
			if (document.getElementsByName("resumephrase_" + z) == null || document.getElementsByName("resumephrase_" + z).length == 0)
				continue;
			document.getElementsByName("resumephrase_" + z)[0].value = "";
			document.getElementsByName("rating_" + z)[0].value = "0";
			document.getElementsByName("required_" + z)[0].value = "";
			document.getElementsByName("tag_" + z)[0].value = "";
			z++;
		}
		for (var i = 0; i < 1000; i++)
		{
			var phrase = document.getElementById("resumephrase_" + x);
			var rating = document.getElementById("rating_" + x);
			var required = document.getElementsByName("required_" + x);
			var tag = document.getElementById("tag_" + x);
			x++;
			if (phrase == null) break;
			if (rating != null)
			{
				rating = rating.value;
			}
			else
				rating = "";
			if (tag.checked)
			{
				tag = tag.value;
			}
			else
			{
				tag = "";
			}
			var requiredval = "";
			for (var j = 0; j < required.length; j++ )
			{
				if (required[j].checked)
				{
					///alert(phrase.value + ":" + required[j].value);
					requiredval = required[j].value;
				}
			}
			//alert("#:" + x + ":" + phrase.value + " rating:" + rating);
			if ((rating == "" || rating == "0") && tag == "" && requiredval == "")
			{
				continue;
			}
			//alert(phrase.value + " rating:" + rating + " tag:" + tag + " requiredval:" + requiredval + " y:" + y);
			document.getElementsByName("resumephrase_" + y)[0].value = phrase.value;
			document.getElementsByName("rating_" + y)[0].value = rating;
			document.getElementsByName("required_" + y)[0].value = requiredval;
			document.getElementsByName("tag_" + y)[0].value = tag;
			y++;
		}
	}

<% 
	int resumeIdx = 100 + phraseList.size()+templatePhrases.size();
	for (SortPhrase prevPhrase: prevPhrases)
	{
		boolean found = false;
		int idx = 0;
		for(String phrase : displayedJobPhrases)
		{
		//System.out.println("Phrase Buffer: " + p.iterator().next().getPhrase().getBuffer());
		//System.out.println("POS Sequence for phrase: " + p.iterator().next().getPhrase().getNgram().getPosSequence());
			if (phrase.trim().equals(prevPhrase.getLemmatizedPhrase().trim()))
			{
				found = true;
				if (prevPhrase.isRequired())
				{
%>
		var req = document.getElementById("required_<%=100+idx%>");
		if (req != null) req.checked = true;
<%
				}
				if (prevPhrase.isTag())
				{
%>
		var tag = document.getElementById("tag_<%=100+idx%>");
		if (tag != null) tag.checked = true;
<%
				}
				if (usestars)
				{
%>
		var idStr = "" + (100 + <%=idx%>) + "_" + <%=prevPhrase.getStars().intValue()%>;
		var img = document.getElementById(idStr);
		if (img != null)
		{
			starRater(img, 'on')
			starRater(img, 'save');
		}
<%
				}
				else
				{
					if (prevPhrase.getStars().intValue() > 2 && !prevPhrase.isRequired())
					{
%>
		var maybe = document.getElementById("maybe3_<%=100+idx%>");
		if (maybe != null) maybe.checked = true;
<%
					}
					else if (prevPhrase.getStars().intValue() > 0 && !prevPhrase.isRequired())
					{
%>
		var maybe = document.getElementById("maybe1_<%=100+idx%>");
		if (maybe != null) maybe.checked = true;
<%
					}
				}
			}
			idx++;
		}
		if (prevPhrase.isResumePhrase())
		{
			String requiredval = "";
			String tagval = "";
			int rating = prevPhrase.getStars().intValue();
			if (prevPhrase.isRequired()) requiredval = "true";
			if (prevPhrase.isTag()) tagval = "tag";
%>
			resphrase = "<%=prevPhrase.getOriginalPhrase().trim()%>";
			resphraseptr = document.getElementsByName("resumephrase_<%=resumeIdx%>");
			ratingptr = document.getElementsByName("rating_<%=resumeIdx%>");
			requiredptr = document.getElementsByName("required_<%=resumeIdx%>");
			tagptr = document.getElementsByName("tag_<%=resumeIdx%>");
			resphraseptr[0].value = resphrase;
			ratingptr[0].value = "<%=rating%>";
			tagptr[0].value = "<%=tagval%>";
			requiredptr[0].value = "<%=requiredval%>";
<%
			resumeIdx++;
		}
		else if (prevPhrase.isUserentered())
		{
%>
		uphrase = "<%=prevPhrase.getOriginalPhrase().trim()%>";
		uptr = document.getElementById("uptr_" + (nophrases-1));
		inuptr = document.getElementById("userphrase_" + (nophrases-1));
		if (uptr.style.display != 'none' && inuptr.value == '')
		{
			inuptr.value = uphrase;
		}
		else
		{
			addNewPhrase(uphrase);
		}
		idx = nophrases-1;
<%
		if (usestars)
		{
%>
		idStr = "" + idx + "_" + <%=prevPhrase.getStars().intValue()%>;
		img = document.getElementById(idStr);
		if (img != null)
		{
			starRater(img, 'on')
			starRater(img, 'save');
		}
<%
		}
		else
		{
			if (prevPhrase.getStars().intValue() > 2 && !prevPhrase.isRequired())
			{
%>
		var maybe = document.getElementById("maybe3_" + idx);
		if (maybe != null) maybe.checked = true;
<%
			}
			else if (prevPhrase.getStars().intValue() > 0 && !prevPhrase.isRequired())
			{
%>
		var maybe = document.getElementById("maybe1_" + idx);
		if (maybe != null) maybe.checked = true;
<%
			}
		}
				if (prevPhrase.isRequired())
				{
%>
		var req = document.getElementById("required_" + idx);
		if (req != null) req.checked = true;
<%
				}
				if (prevPhrase.isTag())
				{
%>
		var tag = document.getElementById("tag_" + idx);
		if (tag != null) tag.checked = true;
<%
				}
		}
	}
		if (sourceDocument != null && posting.getTemplateName() != null && posting.getTemplateName().length() > 0)
		{
%>
			document.getElementById("content0").style.width = '980px';
<%
		}
		List<UserDeletedPhrase> udps = new UserDeletedPhrase().getObjects("USER_NAME =" + UserDBAccess.toSQL(user.getLogin()));
		Set<String> userDeletedPhrases = new HashSet<String>();
		for (int k = 0; k < udps.size(); k++)
		{
			userDeletedPhrases.add(udps.get(k).getPhrase().toLowerCase());
		}
		for (i = 0; i < displayedJobPhrases.size(); i++)
		{
			if (userDeletedPhrases.contains(displayedJobPhrases.get(i).trim().toLowerCase()))
			{
%>
				var phraseTd = document.getElementById("phrase" + (100 +<%=i%>));
				if (phraseTd != null && (phraseTd.style.textDecoration == 'none' || phraseTd.style.textDecoration == ''))
				{
					phraseTd.style.textDecoration = 'line-through';
				}
<%
			}
		}
	}
	catch (Exception x)
	{
		x.printStackTrace();
		throw x;
	}
%>

</script>
</body>
</html>
<%!
	int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>