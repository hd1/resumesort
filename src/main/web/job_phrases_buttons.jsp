<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.wordnet.model.WordNetDataProviderPool"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <!--[if lt IE 9]>
   <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="all.css" />
  <!--[if lt IE 9]>
  <link rel="stylesheet" href="ie.css" />
  <![endif]-->  
<link type="text/css" rel="stylesheet" href="css/phrases.css" />
<script src="phrases.js" type="text/javascript"></script>
<script type="text/javascript" src="ratingsys.js"></script> 
<script language="JavaScript" type="text/JavaScript" src="prototype.js"></script>
<script type="text/javascript" src="default.js"></script> 
<style>

.ttip {
	display: none;
	position: absolute;
	z-index: 3;
	border: solid 1px black;
	background-color: #DDFFFF;
	padding: 1;
	padding-left: 5;
	padding-right: 5;
	min-width: 300px;
}

.ttipBody {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 13px;
	padding: 0;
	margin: 0;
	max-height: 500px;
}

.resumePhrases {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 13px;
	padding: 0;
	margin: 0;
	height: 400px;
    overflow: auto;
}

</style>
<!--[if IE]>
<style type="text/css">body {behavior: url(ifiles/iehoverfix.htc);}</style>
<![endif]-->
<link rel="stylesheet" href="ifiles/stylesheet.css" />
<script type="text/javascript" src="ifiles/javascript.js"></script>
</head>

<body>

<header id="header" class="central2">
  <h1 id="logo">Resume <strong>Sort</strong></h1>
  <nav id="user-nav"></nav>
</header>
<div id="siteTag">
  <h2 class="central2">The most advanced resume sorter on the planet</h2>
</div>

<%!private String login=""; %>
<div class="central2">
<div id='tt' class="ttip"><table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>
<div id='popup' class="ttip"><table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>
<div id="logbox" style="border:solid 1px #00f;font-size:16px;height:20px;display:none">message bar for event logs</div>
<div id ="phrases">
<form method='post' action='Analyzer' name="phrasesform">

   <div id="suggestedphrases">
	<p><b>Select phrases for sorting/scoring resumes</b></p>
	<Table id="suggestedphraselist" >
	<tr>
	<th align=center style="display:none">Clear</th>
	<th colspan=2>Phrases from Job Posting</th>
	<th>Importance</th>
	<th>Must Have</th>
	<th>Tag</th>
	</tr>
<%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
	if (user == null) return;
	JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	if (posting == null) return;
	boolean showPosting = true;
	if (!posting.getUserName().equals(user.getLogin()))
	{
		showPosting = false;
		SharedJobPosting sjp = (SharedJobPosting)session.getAttribute(SESSION_ATTRIBUTE.SHARED_POSTING.toString());
		if (sjp != null)
		{
			if (sjp.isPerm1() || sjp.isPerm3())
				showPosting = true;
		}
	}
	Collection<PhraseList> phraseList = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	Collection<PhraseList> nonLemmaPhraseLists = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString());
	DocumentRS sourceDocument = (DocumentRS) session.getAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString());
	Map<String,String> phrasesSynonyms = (Map<String,String>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());

	WordNetDataProviderPool.getInstance().acquireProvider();
	Map<String, String> contexts = ResumeReport.getPhrasesToContextMap(sourceDocument, nonLemmaPhraseLists);
	WordNetDataProviderPool.getInstance().releaseProvider();
	System.out.println("Contexts:" + contexts);
	int i = 0;
	Collection<PhraseList> lemmaPhraseLists = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString());
	for(PhraseList p : phraseList)
	{   
		//System.out.println("Phrase Buffer: " + p.iterator().next().getPhrase().getBuffer());
		//System.out.println("POS Sequence for phrase: " + p.iterator().next().getPhrase().getNgram().getPosSequence());
		String phrase = p.iterator().next().getPhrase().getBuffer().toString();
		String sentence = "";
		Iterator<PhraseList> iterator = nonLemmaPhraseLists.iterator();
		for(PhraseList lemmaPhraseList : lemmaPhraseLists)
		{
			PhraseList unlemma = iterator.next();
			String unlemmaText = unlemma.iterator().next().getPhrase().getBuffer();
			if (lemmaPhraseList.iterator().next().getPhrase().getBuffer().equalsIgnoreCase(phrase))
			{
				if (contexts != null)
					sentence = contexts.get(unlemmaText.toLowerCase().trim());
			}
		}

		if (sentence == null) sentence = "";
		String srccount = "" + sentence.split("_LIMIT_").length;
		if (srccount.length() > 10) srccount = "10";
%>
	<tr class="kwdRecord" id="kwd_<%=100+i%>" onmouseover="helperActions(this, 'show')" onmouseout="helperActions(this, 'hide')">
	<td class="kwdTools">
		<input type="hidden" value="<%=phrase%>" id="suggphrases_<%=100+i%>" name="suggphrases_<%=100+i%>"/>
<%	if (showPosting) { %>
		<span class="iconTinyInfo" onmouseover='handleTooTip2(event, "<%=sentence.replace('\'', '`')%>");'></span>
<%	}
		if (phrasesSynonyms.get(phrase.trim()) != null)
		{
			String synonymsStr = phrasesSynonyms.get(phrase.trim()).trim();
			if (synonymsStr.length() > 1)
			{
				String[] synonymns = synonymsStr.split("\\|");
				synonymsStr = synonymsStr.replace("|","_LIMIT_");
				int syncnt = synonymns.length;
				if (syncnt > 9) syncnt = 10;
%>
	<span class="iconSynonyms" onclick='getSynonyms(event, "<%=phrase%>", this);synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%			} else { %>
	<span class="iconGSynonyms" g onclick='getSynonyms(event, "<%=phrase%>", this);synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%			} } else { %>
	<span class="iconGSynonyms" g onclick='getSynonyms(event, "<%=phrase%>", this);synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%		} %>
<%	if (user.isAdmin() || user.isTester()) { %>
	<span class="iconDelete" onclick='deletePhrase(this, "<%=phrase%>", <%=100+i%>);' title="Remove Phrase"></span>
<%		} %>
	</td>
	<td class="kwdText" id="phrase<%=100+i%>">
		<%=phrase%>
	</td>
<td class="kwdRating">
	<div class="rateActions">
		<div id="rateMe" style="width:auto;float:left;" onmouseout="starRater(<%=100+i%>)">
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Optionally nice" id="<%=100+i%>_1" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Nice to have" id="<%=100+i%>_2" onclick="starRater(this, 'save')" class=""></a>

		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Good to have" id="<%=100+i%>_3" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Should have" id="<%=100+i%>_4" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Important" id="<%=100+i%>_5" onclick="starRater(this, 'save')" class=""></a>
		<!--a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Must have" id="100_6" onclick="starRater(this, 'save')" class=""></a-->
		</div>
		<span style="display:none;" class="actionStatus" id="status<%=100+i%>"></span>
		<span style="float:right;" id="actions<%=100+i%>">
		<a onclick="clearStarRating(<%=100+i%>);return false;" href="#" style="display:none;" id="clear<%=100+i%>">Clear</a>

		<!-- a onclick="removeKeyphrase(100);return false;" href="#" style="display:none;" id="remove<%=100+i%>">Remove</a -->
		</span>
	</div>
	<input type="hidden" value="0" id="rating_<%=100+i%>" name="rating_<%=100+i%>"/>
	<!--input type="hidden" value="false" id="required_<%=100+i%>" name="required_<%=100+i%>" /-->
</td>
<td align=center><input type="checkbox" value="true" id="required_<%=100+i%>" name="required_<%=100+i%>" onclick="checkRequired(this)"/><!--img id="reqimg_<%=100+i%>" src="star_off.gif" onclick="setRequired(this, <%=100+i%>)"/ --></td>
<td align=center><input type="checkbox" value="tag" id="tag_<%=100+i%>" name="tag_<%=100+i%>" /></td>
</tr>
<%  
	i++;
	} %>
    </table>
	</div>
<div id="userphrases">
	<p><b>Please enter your own phrases</b></p>
	<table id="userphraselist">
	<tr  style="display:inline">
	<th align=left width=50%>Your Phrase</th>
	<th align=left width=40%>Importance</th>
	<th align=right width=5%>Must Have</th>
	<th align=right width=5%>Tag</th>
	</tr>
<%	
	String disp = "inline";
	for (i = 0; i < 100; i++) 
	{
		if (i > 0) disp = "none";
%>
	<tr  class="kwdRecord" id="uptr_<%=i%>" style="display:<%=disp%>" onmouseover="helperActions(this, 'show')" onmouseout="helperActions(this, 'hide')">
	<td width=60% nowrap><img src="x.png" title="Remove Phrase" onclick="deleteUserPhrase(<%=i%>)">
	<span class="iconGSynonyms" g onclick='getUserSynonyms(event, <%=i%>, this);' title="Equivalent Phrases"></span>
	&nbsp;
	<input type=text name="userphrase_<%=i%>" id="userphrase_<%=i%>" size=25  onkeypress="return checkenter(event, <%=i%>)" ></td>
<td class="kwdRating">
	<div class="rateActions">
		<div id="rateMe" style="width:auto;float:left;" onmouseout="starRater(<%=i%>)">
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Optionally nice" id="<%=i%>_1" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Nice to have" id="<%=i%>_2" onclick="starRater(this, 'save')" class=""></a>

		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Good to have" id="<%=i%>_3" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Should have" id="<%=i%>_4" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Important" id="<%=i%>_5" onclick="starRater(this, 'save')" class=""></a>
		<!--a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Must have" id="100_6" onclick="starRater(this, 'save')" class=""></a-->
		</div>
		<span style="display:none;" class="actionStatus" id="status<%=i%>"></span>
		<span style="float:right;" id="actions<%=i%>">
		<a onclick="clearStarRating(<%=i%>);return false;" href="#" style="display:none;" id="clear<%=i%>">Clear</a>

		<!-- a onclick="removeKeyphrase(100);return false;" href="#" style="display:none;" id="remove<%=i%>">Remove</a -->
		</span>
	</div>
	<input type="hidden" value="0" id="rating_<%=i%>" name="rating_<%=i%>"/>
	<!--input type="hidden" value="false" id="required_<%=i%>" name="required_<%=i%>" /-->
</td>
	<td width=15%>&nbsp;&nbsp;<a id="clear<%=i%>" style="visibility:hidden;font-size:9px;font-weight:normal" href="#" onclick="clearSelection(<%=i%>);setRequired2(<%=i%>)">Clear</a> </td>
	<td align=center><!--img id="reqimg_<%=i%>" src="star_off.gif" onclick="setRequired(this, <%=i%>)"/--><input type=checkbox name="required_<%=i%>" id="required_<%=i%>" value="true"  onclick="checkRequired(this)"></td>
	<td align=center><input type=checkbox name="tag_<%=i%>" id="tag_<%=i%>" value="tag" ></td>
	</tr>
<% } %>
	<tr>
	<td colspan=4 align=center nowrao>
	<a class="cssbutton" onclick="addNewPhrase()" href="#"><span>Add Phrase</span></a>
	<!--a class="cssbutton" onclick="submitSelection()" href="#"><span>Add Phrase</span></a>
	<input type="button" value="Add Phrase" onclick="addNewPhrase()"  /-->
	<input type="button" id="resPhrasesBut" value="Select Resume Phrases" onclick="showResumePhrases(event, this)"  />
	<!-- input type="button" id="keyNamesBut" value="Select Key Names" onclick="showTagList(event, this)"  / -->
	</td>
	</tr>
	</table>
</div>
<script>
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g,"");
	}
	var nophrases = 1;
	function addNewPhrase(phrase)
	{
		var uptr = document.getElementById("uptr_" + (nophrases-1));
		if (uptr.style.display == 'none')
		{
			uptr.style.display = 'inline';
			var inuptr = document.getElementById("userphrase_" + (nophrases-1));
			inuptr.value = "";
			if (phrase != null)
			{
				inuptr.value = phrase;
			}
			inuptr.focus();
			return (nophrases-1);
		}
		uptr = document.getElementById("uptr_" + nophrases);
		if (uptr != null)
		{
			uptr.style.display = 'inline';
			var inuptr = document.getElementById("userphrase_" + nophrases);
			inuptr.value = "";
			if (phrase != null)
			{
				inuptr.value = phrase;
			}
			inuptr.focus();
			nophrases++;
			return (nophrases-1);
		}
	}
	function deleteUserPhrase(num)
	{
		var uptr = document.getElementById("uptr_" + num);
		if (uptr != null)
		{
			uptr.style.display = 'none';
			var inuptr = document.getElementById("userphrase_" + num);
			inuptr.value = "";
		}
		clearStarRating(num);
	}

	function checkenter(event, num)
	{
		if (event == null)
			event = window.event;
		var keyCode = event.keyCode
		if (keyCode == null)
			keyCode = event.which;
		if(keyCode == 13)
		{
			if (num == nophrases-1)
			{
				addNewPhrase();
			}
			return true;
		}
		return true;
	}

	function addResumePhrase(phrase, idx)
	{
		var uptr = document.getElementById("uptr_" + (nophrases-1));
		var inuptr = document.getElementById("userphrase_" + (nophrases-1));
		if (uptr.style.display != 'none' && inuptr.value == '')
		{
			inuptr.value = phrase;
		}
		else
			addNewPhrase(phrase);
		var resPhraseTr = document.getElementById("resPhrase" + idx);
		if (resPhraseTr != null)
		{
			resPhraseTr.style.display = 'none';
		}
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_resume_phrase.jsp?phrase=" + escape(phrase);
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
	}

	function addKeyName(phrase, idx)
	{
		var uptr = document.getElementById("uptr_" + (nophrases-1));
		var inuptr = document.getElementById("userphrase_" + (nophrases-1));
		var idx = 0;
		if (uptr.style.display != 'none' && inuptr.value == '')
		{
			inuptr.value = phrase;
			idx = nophrases-1;
		}
		else
		{
			idx = addNewPhrase(phrase);
		}
		var tagptr = document.getElementById("tag_" + idx);
		if (tagptr != null)
		{
			tagptr.checked = true;
		}
	}
	function deleteResumePhrase(phrase, idx)
	{
		if (!confirm("Do you want to mark  '" + phrase + "' as invalid and remove it from processing in the future?"))
		{
			return;
		}
		var resPhraseTr = document.getElementById("resPhrase" + idx);
		if (resPhraseTr != null)
		{
			resPhraseTr.style.display = 'none';
		}
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_resume_phrase.jsp?permanent=true&phrase=" + escape(phrase);
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
	}
	function deletePhrase(obj, phrase, idx)
	{
		var delet = true;
		var phraseTd = document.getElementById("phrase" + idx);
		if (phraseTd != null && (phraseTd.style.textDecoration == 'none' || phraseTd.style.textDecoration == ''))
		{
			phraseTd.style.textDecoration = 'line-through';
		}
		else
		{
			phraseTd.style.textDecoration = 'none';
			delet = false;
		}
		if (delet && !confirm("Do you want to mark  '" + phrase + "' as invalid and remove it from processing in the future?"))
		{
			return;
		}
		clearSelection(idx);
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_jobposting_phrase.jsp?phrase=" + escape(phrase) + "&delete=" + delet;
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
		//alert(obj.style + ":" + obj.style.name);
		//obj.class = 'iconAdd';

	}
	
	var synPhrase;
	function getUserSynonyms(event, idx, img)
	{
		var phrase = document.getElementById("userphrase_" + idx);
		if (phrase == null) return;
		if (phrase.value == '')
		{
			alert("Please enter a phrase");
			return;
		}
		synPhrase = phrase.value;
		getSynonyms(event, phrase.value, this);
	}

	function checkAddenter(event)
	{
		if (event == null)
			event = window.event;
		var keyCode = event.keyCode
		if (keyCode == null)
			keyCode = event.which;
		if(keyCode == 13)
		{
			addSynonym(synPhrase);
			return true;
		}
		return true;
	}

	var resumePhrases = false;
	function showResumePhrases(event, button)
	{
		if (resumePhrases)
		{
			 closeResumePhrases();
			 return;
		}
		if (tagList)
		{
			 closeResumePhrases();
		}
		resumePhrases = true;
		tagType = "";
		showResumePhrasesPopup(event);
		button.value="Close Pop Up";
	}
	function openCompanies(hide)
	{
		if (hide)
			tagType = "";
		else
			tagType = "Company";
		showResumePhrasesPopup();
	}
	function openUniversities(hide)
	{
		if (hide)
			tagType = "";
		else
			tagType = "University";
		showResumePhrasesPopup();
	}
	function closeResumePhrases()
	{
		var button = document.getElementById("resPhrasesBut");
		button.value="Select from Resume Phrases";
		//button = document.getElementById("keyNamesBut");
		//button.value="Select from Key Names";
		tthide();
		resumePhrases = false;
		tagList = false;
	}
	var tagList = false;
	function showTagList(event, button)
	{
		if (tagList)
		{
			 closeResumePhrases();
			 return;
		}
		if (resumePhrases)
		{
			 closeResumePhrases();
		}
		tagList = true;
		showTagListPopup(event);
		button.value="Close Pop Up";
	}
</script>
<div align="right" style="display:block; width:50%;">
		&nbsp;<p><br><br><a class="cssbutton" onclick="submitSelection()" href="#"><span>Analyze/Sort Resumes</span></a> <!--input type="button" onclick="submitSelection()" value="Analyze/Sort Resumes" / --></div>
    </form>
  <div id="loading" style="visibility: hidden">
    <p>Processing...</p>
  </div>
</div>
</div>
<script>
	function unSelect(ind)
	{
		var req =document.getElementById("required_" + ind);
		if (req != null)
			req.checked=false;
		var reqimg =document.getElementById("reqimg_" + ind);
		if (reqimg != null)
			reqimg.src="star_off.gif";
		clearRating(ind);
	}
	function checkRequired(cbx)
	{
		var id = cbx.id.split("_");
		var ind = id[1];
		if (cbx.checked)
		{
			cbx.value = true;
			clearStarRating(ind);
		}
		else
		{
		}
	}
	function setRequired(image, ind)
	{
		var url = image.src;
		if (url.indexOf("star_off") != -1)
		{
			image.src = "star_on.gif";
			var req =document.getElementById("required_" + ind);
			req.checked=true;
			clearStarRating(ind);
		}
		else
		{
			image.src = "star_off.gif";
			var req =document.getElementById("required_" + ind);
			req.checked=false;
		}
	}
	function clearRequired2(ind)
	{
		return;
		var image = document.getElementById("reqimg2_" + ind);
		if (image == null) return;
		var url = image.src;
		if (url.indexOf("star_on") != -1)
		{
			image.src = "star_off.gif";
			var req =document.getElementById("required2_" + ind);
			req.value="false";
		}
	}
	function setRequired2(ind)
	{
		return;
		var image = document.getElementById("reqimg2_" + ind);
		if (image == null) return;
		var url = image.src;
		if (url.indexOf("star_off") != -1)
		{
			image.src = "star_on.gif";
			var req =document.getElementById("required2_" + ind);
			req.value="true";
		}
	}
	function clearSelection(ind)
	{
		clearRating(ind);
		var image = document.getElementById("reqimg_" + ind);
		if (image != null)
			image.src = "star_off.gif";
		var req =document.getElementById("required_" + ind);
		if (req != null)
			req.checked=false;
		var clear = document.getElementById("clear" + ind);
		if (clear != null)
		{
			clear.style.visibility='hidden';
		}
	}
	function contains(a, obj) 
	{     
		for (var i = 0; i < a.length; i++) 
		{         
			if (a[i] === obj) 
				return true;         
		}     
		return false; 
	}
	function submitSelection()
	{
		var selPhrases = new Array();
		var elems = document.phrasesform.elements;
		var selected = false;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name.indexOf("suggphrases_") == 0)
			{
				var id =  elems[i].id.split('_');
				if (document.getElementById('required_'+id[1]).checked || document.getElementById('rating_'+id[1]).value != 0)
				{
					selected = true;
					var str = new String(elems[i].value).toLowerCase();
					selPhrases.push(str);
				}
			}
			if (name.indexOf("userphrase_") == 0 && elems[i].value != "")
			{
				var str = new String(elems[i].value).toLowerCase();
				if (contains(selPhrases, str) || contains(selPhrases, str + " "))
				{
					alert("Phrase:'" + elems[i].value +"' is already in the list. Please remove it.");
					return;
				}
				var idx = name.substring(11);
				var rating = document.getElementById("rating_" + idx);
				var required = document.getElementById("required_" + idx);
				var tagged = document.getElementById("tag_" + idx);
				if (rating.value == "0" && !required.checked && !tagged.checked)
				{
					alert("Please rate phrase:'" + elems[i].value +"'");
					return;
				}
				selPhrases.push(str);
			}
		}
		if (!selected)
		{
			alert("Please select some phrases")
			return;
		}
		document.getElementById("loading").style.visibility = "visible";
		document.phrasesform.submit();
	}

<% 
	// This is for testing only
	if (false && user != null && user.getEmail().startsWith("dev.gude@text")) { 
%>
	var j = 5;
	for (var i = 0; i < 15 ; i++ )
	{
		var idStr = "" + (100 + i) + "_" + j;
		var img = document.getElementById(idStr);
		if (img != null)
		{
			starRater(img, 'on')
			starRater(img, 'save');
		}
		j--;
		if (j < 1) j = 5;
	}
<%	} %>
</script>
  
<%@include file="rq_footer.jsp"%>
</body>
</html>