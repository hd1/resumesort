<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="cache-control" content="no-store"/>
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
	<meta name="keywords" content="Recruitment Software, Online Recruiting Software, Applicant Tracking System, Resume Screening, Resume Parsing, Applicant Management, Hiring and Staffing, semantic search" /> 
	<meta name="description" content="ResumeSort is the most advanced resume screening and recruitment software in the world.  Our SaaS allows you to find the most qualified applicant screening not just for keywords but for the meaning associated with those key phrases in their resumes.  Never lose the most qualified candidate again.  Try our software for free." /> 
	<title>ResumeSort - the most advanced resume sorting technology on the planet.</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="js/jquery.lavalamp.min.js"></script>
	<script type="text/javascript" src="js/jquery.easyAccordion.js"></script>
    <script type="text/javascript" src="js/jquery.tooltip.js"></script>
    <script type="text/javascript">
<% if (user != null) { %>	
        $(function() {
            $("#1, #2, #3").lavaLamp({
                fx: "backout",
                speed: 700
            });
        });
		$(document).ready(function(){

			$("#btn-slide").click(function(){
				$("#panel").slideToggle("slow");
				$(this).toggleClass("active"); return false;
			});	
			 
		});
 <% } %>
    </script>
    </head>

    <body>
	<div class="topbg_dashboard">
      <div class="middle">
    <div class="logo"></div>
    <div class="logotext">We read resumes, so you don't have to!</div>
    <div class="logg">
<% if (user == null && request.getRequestURL().indexOf("sign") == -1) { %>	
		  <div class="register"><a href="signup.jsp">REGISTER</a></div>
          <div class="login"><a href="signin.jsp">LOGIN</a></div>
<% } else if (user != null) { 
	String _name = user.getName(); // What kind of stupid css is this? It screws up without the spaces.
	if (_name.length() < 9) _name = _name + "&nbsp;&nbsp;&nbsp;&nbsp;";
%>
		  <div class="user">
          <a href="#" id="btn-slide"><%=_name%>&nbsp;<img src="images/arrow_w.png" width="14" height="14" /></a>
          <ul id="panel">
          	<li><a href="ChangePassword.jsp">Settings</a></li>
          	<li><a href="#">Support</a></li>
          	<li><a href="logout.jsp?logout=true">Logout</a></li>
          </ul>
          </div>
   <% } %>
      </div>
    <ul class="lavaLampNoImage" id="2">
          <li><a href="home.jsp">Home</a></li>
          <li><a href="#">SUPPORT</a></li>
          <li><a href="http://blog.resumesort.com">BLOG</a></li>
          <li><a href="#">PRICING & SIGNUP</a></li>
        </ul>
    <div class="clear"></div>


  </div>
    </div>
