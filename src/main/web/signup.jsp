<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="net.tanesha.recaptcha.*" %>
<%@ page import="net.tanesha.recaptcha.*" %>
<%
	String code = request.getParameter("code");
	if (code == null) code = "";
	String codereadonly = "";
	if (code.trim().length() > 0) codereadonly = "readonly";
	String type = request.getParameter("type");
	if (type == null) type = "";
	String error = request.getParameter("error");
	if (error != null && error.trim().length() > 0)
	{
%>	<script>alert("<%=error%>");</script> <%
	}
	String email = (String) request.getAttribute("email");
	if (email == null) email = "";
	String name = (String) request.getAttribute("name");
	if (name == null) name = "";
	String company = (String) request.getAttribute("company");
	if (company == null) company = "";
	String betacode = (String) request.getAttribute("betacode");
	if (betacode == null) betacode = "";
	if (code.length() == 0) code = betacode;
%>
<%@include file="header.jsp"%>
    <script type="text/javascript">
$(document).ready(function(){
	
	$(".accordion li:first").addClass("active");
	$(".accordion li.d:not(:first)").hide();

	$(".accordion li").click(function(){
		$(this).next("li.d").slideToggle("slow")
		.siblings("li.d:visible").slideUp("slow");
		$(this).toggleClass("active");
		$(this).siblings("li").removeClass("active");
	});

});
</script>

<script type="text/javascript">

	
	function enableSignUp()
	{		
		document.register.signup.disabled=(!document.register.privacy.checked);
	}
	
	function  doSubmit() 
	{		
		if (isValid())
		{
			document.register.submit();
		}		
	}

	function  isValid()
	{				
		if (document.register.passwordconfirm.value!=document.register.password.value)
		{
			alert("Your passwords do not match!");
			return false;
		}
		var name = document.register.name.value;
		name = trim(name);
		if (name =="" || name.length < 4 || name.indexOf(" ") == -1)
		{
			alert("Please enter your first name and last name.");
			return false;
		}

		if (document.register.email.value=="")
		{
			alert("Please enter your email.");
			return false;
		}

		if (!validateEmail(document.register.email.value))
		{
			alert("Please enter a valid email address.");
			return false;
		}

		/*if (document.register.login.value=="")
		{
			alert("Please enter your login.");
			return false;
		}*/

		var password = document.register.password.value;
		password = trim(password);
		if (password=="")
		{
			alert("Please enter your password.");
			return false;
		}
		if (password.length < 5)
		{
			alert("Password should be at least 5 characters long.");
			return false;
		}

		if (document.register.betacode.value=="")
		{
			alert("A participation code is required.");
			document.register.betacode.focus();
			return false;
		}
		
		return true;
	}
	function validateEmail(email) 
	{ 
		 var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|us|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs)\b/
		 return email.match(re) 
	}	
	function trim(str) 
	{
		return str.replace(/^\s+|\s+$/g,"");
	}
	function copyToCompany(obj)
	{
		return; //temporary
		var name = obj.value;
		if (name.indexOf(" ") != -1)
		{
			name = name.substring(0, name.indexOf(" "));
		}
		var company = document.register.company;
		if (company.value == '')
		{
			company.value = name;
		}
	}
</script>

<script type="text/javascript" src="https://platform.linkedin.com/in.js">
		//api_key: hprez6o7wi7e
		//api_key: spktcpuor5s9
		api_key: pny9h114v0rg
</script>

<script type="text/javascript">
  // 2. Runs when the JavaScript framework is loaded
    function linkedInLogin()
  {
    IN.ENV.js.scope = new Array();
    IN.ENV.js.scope[0] = 'r_emailaddress';
    IN.User.authorize(linkedInLogin2);
  }
  function linkedInLogin2()
  {
                   
			IN.API.Profile("me")
			.fields(["id", "firstName", "lastName", "pictureUrl","headline","emailAddress"])
			.result(function (result){
			 profile = result.values[0];
                   
                          var xmlhttp;
				if (xmlhttp == null)
					xmlhttp = GetXmlHttpObject();
				if (xmlhttp != null) {
					var url = "linkedin_login.jsp";
					url = url + "?id=" + profile.id;
					url = url + "&name=" + profile.firstName + " " + profile.lastName;
					url = url + "&email=" + profile.emailAddress;
					url = url + "&code=" + "<%=code%>";
					xmlhttp.open("POST", url, false);
					xmlhttp.send(null);
					var resp = xmlhttp.responseText;
					if (resp.indexOf("newuser") != -1)
					{
						linkedinId = profile.id;
						linkedinName = profile.firstName + " " + profile.lastName;
                                                linkedinEmail = profile.emailAddress;
					}
					else if (resp == "")
					{
						window.location = "home.jsp"; // existing user, refresh to get file list
					}
					else
						alert(resp);
				}
			});
                }

<%--		function closeDialog()
		{
			$('#dialog').dialog('close');
		}
--%>		var linkedinId;
		var linkedinName;
                var linkedinEmail;
		function updateLinkedIn()
		{
				if (!validateEmail(document.getElementById("linkedinEmail").value))
				{
					alert("Please enter a valid email address.");
					return;
				}
				var url = "linkedin_login.jsp";
				url = url + "?id=" + linkedinId;
				url = url + "&name=" + linkedinName;
				url = url + "&email=" + linkedinEmail; //document.getElementById("linkedinEmail").value;
				xmlhttp.open("POST", url, false);
				xmlhttp.send(null);
				var resp = xmlhttp.responseText;
				if (resp.indexOf("newuser") != -1)
				{
					alert(resp);
				}
				else if (resp == "")
				{
					window.location = "home.jsp"; // existing user, refresh to get file list
				}
		}
		function validateEmail(email)
		{
			 var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs)\b/
			 return email.match(re)
		}
                		function GetXmlHttpObject() {
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				return new XMLHttpRequest();
			}
			if (window.ActiveXObject) {
				// code for IE6, IE5
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
			return null;
		}
  </script>

<div id="dialog" style="display:none">
	<span id=dialogtext></span>
	<input type=text id=linkedinEmail size=40>
	<div style="clear:both; margin-top:30px; float:right;">
	<a href="javascript:closeDialog()" class="orangeBtn">Cancel</a>
	<a href="javascript:updateLinkedIn()" class="orangeBtn">Ok</a>
	</div>
</div>
<div class="middle">
<div class="maintext_dashboard">
<div class="text_dashboard">
 <div class="signin_left">
 	<h3>Sign In</h3><br />
    Sign in with Facebook or Twitter or LinkedIn or Google<br /><br />
	<div class="sociallogin">
		<div id="fb-root"></div>
		<span id="fblogin"></span>
	<!--a href="#"><img src="images/fb.jpg" width="150" height="22" alt="" /></a -->
	</div>
    	<div class="sociallogin">
	<a href="#"><img src="images/ld.jpg" onclick="linkedInLogin()" width="150" height="22" alt="" /></a>
	</div>
	<!--div class="sociallogin">
	<div class="sociallogin">
            <script type="in/Login">
	Hello, <?js= firstName ?> <?js= lastName ?>.
	</script>
	<a href="#"><img src="images/ld.jpg" width="150" height="22" alt="" /></a>
	</div> -->
	<!--div class="sociallogin">
	<a href="javascript:alert('Not available yet')"><img src="images/tw.jpg" width="150" height="22" alt="" /></a>
	</div -->
	<div class="sociallogin">
	<a href="javascript:google_login()"><img src="images/signinwithgoogle.jpg" width="150" height="22" alt="" /></a><br />
	<script type="text/javascript" src="js/jquery-1.5b1.js"></script>
	</div>	
	<div id="profile"></div>
	<hr class="hr"/><br />
	<form method="post" action="Register" name="register">
	<br />Create Your Account<br /><br />
	Name*<br />
	<input type="text" name="name" value="<%=name%>" onchange="copyToCompany(this)" maxlength="200"><br />
	Email*<br />
	<input type="text" name="email" value="<%=email%>" maxlength="128"><br />
	Company<br />
	<input type="text" name="company" value="<%=company%>" maxlength="128"><br />
	Password*<br />
	<input type="password" name="password" value="" maxlength="20"><br /><br />
	Confirm Password*<br />
	<input type="password" name="passwordconfirm" value="" maxlength="20"><br /><br />
	Participation Code*<br />
	<input type="text" name="betacode" <%=codereadonly%> value="<%=code%>" maxlength="150">
	<br>
	<br>
<%
	  ReCaptcha c = ReCaptchaFactory.newSecureReCaptcha("6Lfp3tUSAAAAADCua0ehDbF_jP02il1mi6oOLb0f", "6Lfp3tUSAAAAALcyNKpPaTmDkosS-8yeJjwg7OEI", false);
	  out.print(c.createRecaptchaHtml(null, null));
%>
	<INPUT type='button' name="signup" VALUE='REGISTER' onclick="doSubmit()"  class="signin"><br />
	By clicking Register, you agree to the<a href="Textnomics_Terms_of_Service.pdf"> Terms of Service.</a><br />
	<input type="hidden" size="20" name="type" value='<%= type%>' maxlength="5">
	</form>
 </div>
 
 
 <div class="signin_right">
 
 	<!--div class="box">
    <h3>Try now - it's free</h3><br />
	Lorem ipsum dolor sit amet, consectetuer 
adipiscing elit. Aenean commodo ligula 
eget dolor. Aenean massa. Cum sociis 
natoque penatibus et magnis dis parturi.Lorem ipsum dolor sit amet, consectetuer 
adipiscing elit. Aenean commodo ligula 
eget dolor. Aenean massa. Cum sociis 
natoque penatibus et magnis dis parturi.
    </div-->
 
 </div>
 
 
 
<div class="clear"></div>
</div>
</div>
    </div>
<%@include file="footer.jsp"%>

<% if (true) { %>
<script type="text/javascript" src="js/custom-form-elements.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>   
      <script src="https://connect.facebook.net/en_US/all.js"></script>
      <script>
		var usingFB = false;
	    var xmlhttp;

		$(function() {					
			$('#dialog').dialog({
				autoOpen: false,
				width: 460			
			//,buttons: {
			//		'Yes': function(){
			//			$(this).dialog('close');
			//			callback(true);
			//		},
			//		'No': function(){
			//			$(this).dialog('close');
			//			callback(false);
			//		}
			//	}			
			});
		});

window.fbAsyncInit = function () {
		FB.init({ 
			appId:'236063299838716', cookie:true,
			channelUrl : '//'+window.location.hostname+'/signin.jsp',
			status:true, xfbml:true 
		});
        FB.Event.subscribe('auth.statusChange', function(response) {
          if (response.authResponse) {
			//alert("Session Change:" + response.authResponse);
			get_fb_userinfo();
		  }
		});

		FB.getLoginStatus(function(response) {
			// alert(response.status);
			if (response.status === 'connected') {
				//alert("Logged In");
				get_fb_userinfo();
				usingFB = true;
				//window.location = 'home.jsp';
				//document.getElementById('fblogin').innerHTML ='<a href="#" onclick="FB.logout();">FB Logout</a><br/>';
			} else {
				//alert("Not Logged In");
				document.getElementById('fblogin').innerHTML ='<fb:login-button perms="email">' + '</fb:login-button>';
				FB.XFBML.parse();
			}
		});
};
		function get_fb_userinfo() {
			FB.api('/me', function(response) {
				var userGraph = response;
				if (userGraph.email == null || userGraph.email == '')
				{
					document.getElementById('fblogin').innerHTML ='<fb:login-button data-width=150perms="email">' + '</fb:login-button>';
				}
				else
				{
					if (document.getElementById('fblogin') != null)
						document.getElementById('fblogin').innerHTML ='<a href="#" onclick="FB.logout();">FB Logout</a><br/>';
					if (document.getElementById('register') != null)
						document.getElementById('register').innerHTML ='';
					if (document.getElementById('changepw') != null)
						document.getElementById('changepw').innerHTML ='';
					if (document.getElementById('rtlogin') != null)
						document.getElementById('rtlogin').innerHTML ='';
					//alert("Name1:" + userGraph.name + " email1:" + userGraph.email);
					if (xmlhttp == null)
						xmlhttp = GetXmlHttpObject();
					if (xmlhttp != null) {
						var url = "facebook_login.jsp";
						url = url + "?email=" + userGraph.email;
						url = url + "&name=" + userGraph.name;
						url = url + "&code=" + "<%=code%>";
						xmlhttp.open("POST", url, false);
						xmlhttp.send(null);
						var resp = xmlhttp.responseText;
						if (resp == "")
						{
							window.location = "home.jsp"; // existing user, refresh to get file list
						}
					}
					//alert("Name2:" + userGraph.name + " email2:" + userGraph.email);
					//var emailInp = document.getElementById("emailResumeTo");
					//if (emailInp == null)
					//	emailInp = document.getElementById("email");
					//emailInp.value = userGraph.email;
				}
			  });
		}
		function GetXmlHttpObject() {
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				return new XMLHttpRequest();
			}
			if (window.ActiveXObject) {
				// code for IE6, IE5
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
			return null;
		}     
		function linkedInLoginX() {
		  IN.API.Profile("me")
			.fields(["id", "firstName", "lastName", "pictureUrl","headline","emailAddress"])
			.result(function(result) {
			 profile = result.values[0];
			  //profHTML = "<p><a href=\"" + profile.publicProfileUrl + "\">";
			  //profHTML += "<img class=img_border align=\"left\" src=\"" + profile.pictureUrl + "\"></a>";     
			  //profHTML += "<a href=\"" + profile.publicProfileUrl + "\">";
			  //profHTML += "<h2 class=myname>" + profile.firstName + " " + profile.lastName + "</a> </h2>";
			  //profHTML += "<span class=myheadline>" + profile.headline + "</span>";
			  //profHTML += "<span >" + profile.email + "</span>";
			  //$("#profile").html(profHTML);
			  //alert(profile.id);
				if (xmlhttp == null)
					xmlhttp = GetXmlHttpObject();
				if (xmlhttp != null) {
					var url = "linkedin_login.jsp";
					url = url + "?id=" + profile.id;
					url = url + "&name=" + profile.firstName + " " + profile.lastName;
                                        url = url + "&email=" + profile.emailAddress;
					xmlhttp.open("POST", url, false);
					xmlhttp.send(null);
					var resp = xmlhttp.responseText;
					if (resp.indexOf("newuser") != -1)
					{
						linkedinId = profile.id;
						linkedinName = profile.firstName + " " + profile.lastName;
                                                linkedinEmail = profile.emailAddress;
						<%--document.getElementById("dialogtext").innerHTML = "Please enter your email address?";
						$('#dialog').dialog('open');--%>
					}
					else if (resp == "")
					{
						window.location = "home.jsp"; // existing user, refresh to get file list
					}
				}
			});
		}
        	function closeDialog()
		{
			$('#dialog').dialog('close');
		}
		var linkedinId;
		var linkedinName;
                var linkedinEmail;
		function updateLinkedIn()
		{
				if (!validateEmail(document.getElementById("linkedinEmail").value))
				{
					alert("Please enter a valid email address.");
					return;
				}
				var url = "linkedin_login.jsp";
				url = url + "?id=" + linkedinId;
				url = url + "&name=" + linkedinName;
				url = url + "&email=" + linkedinEmail; //document.getElementById("linkedinEmail").value;
				xmlhttp.open("POST", url, false);
				xmlhttp.send(null);
				var resp = xmlhttp.responseText;
				if (resp.indexOf("newuser") != -1)
				{
					alert(resp);
				}
				else if (resp == "")
				{
					window.location = "home.jsp"; // existing user, refresh to get file list
				}
		}
		function validateEmail(email) 
		{ 
			 var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs)\b/
			 return email.match(re) 
		}	
			 
		function google_login()
		{
			window.location = "https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&state=%2Fprofile&redirect_uri=https%3A%2F%2Fwww.resumesort.com%2Foauth2callback&response_type=token&client_id=1022693856183.apps.googleusercontent.com";
		}
	</script>
<% } %>
</body>
</html>
