<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		System.out.println("Entered get taglist");
		if (user == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}

%>
<table border=1 width="100%" id="tttable" bgcolor="#FFFFBB" cellpaddng=2 cellspacing=0 class="resumePhrases"><TBODY id="ttBody">
<tr>
<td colspan=2 align=center style="font-weight:bold"><b>Companies/Universities<br>(Select to add to your list)</b><hr></td><td><img src="icon_close.gif" onclick="closeResumePhrases()"></td>
</tr>
<%
	Collection<PhraseList> jobPhraseList = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	Set<String> jobPhrases = new HashSet<String>();
	for(PhraseList p : jobPhraseList)
	{   
		String phrase = p.iterator().next().getPhrase().getBuffer().toString();
		jobPhrases.add(phrase.toLowerCase().trim());
	}

	List<TagList> tags = new TagList().getObjects("1 = 1 order by type,tag_name");
	int i = 0;
	for (TagList tag: tags)
	{
		if (jobPhrases.contains(tag.getTagName().toLowerCase())) continue;
%>
	<tr id="resPhrase<%=i%>" onMouseOver="this.style.backgroundColor='#FFFF00'" onMouseOut="this.style.backgroundColor=''">
	<td><a href="#" title='Select Phrase' onclick="addKeyName('<%=tag.getTagName()%>', <%=i%>)"><%=tag.getTagName()%></a></td>
	</tr>
<%
		i++;
	}
%>
	<tr>
	<td colspan=100% ><br>&nbsp;</td>
	</tr>	
	<tr>
	<td colspan=100% align=center style="font-weight:bold"><input type=button value=Close onclick="closeResumePhrases()">&nbsp;<input type=button value=Refresh onclick="showResumePhrases()"></td>
	</tr>
	<tr>
	<td colspan=100% height=80%><br>&nbsp;</td>
	</tr>	
	</TBODY></table>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>