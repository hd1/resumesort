<%@ page import="java.io.*"%><%@ page import="java.util.*"%><%@ page import="com.textonomics.*"%><%@ page import="com.textonomics.web.*"%><%@ page import="com.textonomics.user.*"%><%@ page import="com.textonomics.wordnet.model.*"%><%@ page import="com.textnomics.data.*"%><%
		String jobpostingName = request.getParameter("jobpostingName");
		System.out.println("Entered get job posting:" + jobpostingName);
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (user == null) 
		{
%>
Your session has expired.
<%
			return;
		}
		if (jobpostingName == null) return;
		File jobpostingFile = PlatformProperties.getInstance().getResourceFile(ResumeSorter.getUploadDir(user) + "/" + ResumeSorter.JOBPOSTING_FOLDER +"/" + jobpostingName);
		String download = request.getParameter("download");
		InputStream is = new FileInputStream(jobpostingFile); 
		if (download != null && download.equalsIgnoreCase("true"))
		{
			String mimeType = java.net.URLConnection.guessContentTypeFromStream(is);
			response.setContentType (mimeType) ;
			response.setHeader("Content-disposition", "attachment;filename=" + jobpostingName) ;
		}
		ServletOutputStream outStream = response.getOutputStream() ;
		int chunk = 1024;
		byte[] buffer = new byte[chunk] ;
		int length = -1 ;
		int i = 0 ;

		while ((length = is.read(buffer)) != -1)
		{
			outStream.write(buffer, 0, length) ;
			if (length < chunk)
				break;
		}
		is.close();
		outStream.flush();
		outStream.close();
		is = null;

		return;
%>