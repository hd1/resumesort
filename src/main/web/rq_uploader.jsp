<%@ page import="java.io.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());	
	String []jpfileNames=null;
	if (user != null)
	{
		request.setAttribute("email", user.getEmail());
	
		//Get existing list of MyResumes
		String uploadDir="uploads" + File.separator + user.getLogin() + File.separator;
		File userFolder = PlatformProperties.getInstance().getResourceFile(
								uploadDir + File.separator + "MyResumes" + File.separator);
		//Content of folder
		File []files=null;
		if(userFolder.exists()){
				files=userFolder.listFiles();
				request.setAttribute("myResumes", files);
				System.out.println("Resume files:" + files.length);
		}
		else
				userFolder.mkdirs();
		
		userFolder = PlatformProperties.getInstance().getResourceFile(
								uploadDir + File.separator + "MyPostings" + File.separator);
		//Content of folder
		if(userFolder.exists()){
				jpfileNames=userFolder.list();
				request.setAttribute("myPostings", jpfileNames);
				System.out.println("Job files:" + jpfileNames.length);
		}
		else
				userFolder.mkdirs();
	}
	else // For Beta only
	{
		response.sendRedirect("");
	}

	// Clear all old results
	session.removeAttribute(SESSION_ATTRIBUTE.TARGET_FILE.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_HTMLRESUMES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString());
	session.removeAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());

	String defaultJobId = user.getCompany();
	if (defaultJobId == null || defaultJobId.trim().length() == 0)
		defaultJobId = user.getName();
	if (defaultJobId.indexOf(" ") != -1)
		defaultJobId = defaultJobId.substring(0, defaultJobId.indexOf(" "));
	if (defaultJobId.length() > 4)
		defaultJobId = defaultJobId.substring(0, 4);
	defaultJobId = defaultJobId.trim().toUpperCase() + new SimpleDateFormat("-yyyy-MM-dd-HHmm").format(new Date());
	
	String error = request.getParameter("error");
	if (error == null || error.length() == 0)
	if ((error = (String)request.getSession().getAttribute(SESSION_ATTRIBUTE.ERROR.toString())) != null)
	{
		request.getSession().removeAttribute(SESSION_ATTRIBUTE.ERROR.toString());
	}
	if ("null".equals(error)) error = "";
	String defaultJobText = "";
	String testJobText = "zzzzzzz is working with an established company seeking a Sr. Product Manager for the following PERMANENT opportunity: \n\nThe Sr. Product Manager will be responsible initially for flagship products, Anti-virus and Security Suite. The position requires development of product strategy and, by working with Engineering, the development of Security applications, aimed at consumers and businesses, and is also responsible for the ultimate success of the product in the market. \n\nThis person will analyze end users� needs, marketing research, and the competitive landscape, leading to the creation/enhancement of future versions of the products. The Sr. Product Manager will be held accountable for developing the product strategy, roadmap, specific requirements for each release, product revenue generation, product adoption and overall product usability. This role is a matrix management position that will operate in a cross-functional environment and will interface with Engineering, Marketing, Sales, Technical Support, and Finance. \n\nThe Sr. Product Manager will report directly to the VP of Product Management, must be fluent in English and will be located in Brno, Czech Republic. \n\nEssential Functions: \n� Analyze market data, the competitive landscape, customer needs, etc. -- and understand when it�s time to stop studying and start �doing�; an intuitive sense of what it takes to make your products successful. \n� Create business plans to justify the development of new products/releases, and to define the launch, distribution, marketing and financial strategies for your products. \n� Define product strategy and roadmap for your products. \n� Drive product requirements and their prioritization. Balance needs of the various groups inside and outside of Grisoft, including Sales, Marketing, Finance, Technical Support and Engineering in making product decisions. \n� Define launch strategies and plans and guide ongoing product marketing activities. \n� Conceive, evaluate, develop and manage new products and functionality. \n\nAdditional Responsibilities: \n� Work closely with Project Management during the development phase of your products, and with Sales & Marketing to define launch and ongoing promotional activities. \n� Define and drive necessary consumer research and usability testing. \n\nKnowledge, Skills, and Abilities: \n� Impeccable communicator, a diplomat, a collaborator, and a builder of teams; a builder of a series of positive interactions along the way to successful product implementations; can work with different groups and personality types to achieve success. \n� Strong technical skills are required to understand our technical capabilities, limitations and to gain the respect of the engineering staff. \n� Highly organized with careful attention to detail, excellent planning and priority setting skills. \n� You will �roll up your sleeves� when necessary to get the job done. You are not focused on hierarchy or titles. \n� Understand the product life cycle and the ability to determine, and extend products during their life cycle. \n� Strong leadership and negotiation skills are important. \n� Strong analytical skills necessary to build financial cases and understand consumer and usage data \n\nQualifications, Training, and Experience: \n\nThe candidate must have a minimum of three years experience as a Product Manager for software applications. Bachelor�s degree required, technical degree and/or MBA preferred. \n";
%>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>ResumeSort - the most advanced resume sorter technology on the planet.</title>
  <meta charset="utf-8" />
  <meta http-equiv="cache-control" content="no-store"/>
  <meta http-equiv="pragma" content="no-cache"/>
  <meta http-equiv="expires" content="0"/>
  <!--[if lt IE 9]>
   <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="all.css" />
  <link rel="stylesheet" href="colorbox/colorbox.css" />
  <!--[if lt IE 9]>
  <link rel="stylesheet" href="ie.css" />
  <![endif]-->  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
  <script src="colorbox/jquery.colorbox-min.js"></script>
  <script>
    $(function() {
	  $("#step1").colorbox({width:"370px", inline:true, href:"#uploadResume", opacity: '0.5', transition: 'none'});
      $("#step2").colorbox({width:"900px", inline:true, href:"#jobDescriptionPopUp", opacity: '0.5', transition: 'none'});
	  $(".new_user_email").colorbox({width:"400px", inline:true, href:"#emailAddressPopUp", opacity: '0.5', transition: 'none'});
	 // $(".returning_user_email #emailResumeTo").colorbox({width:"400px", inline:true, href:"#emailAddressPopUp", opacity: '0.5', transition: 'none'});
	  $("#hideJobDescription").click(function() {
	    $("#jobDescription").fadeOut();
		return false;
	  });
	  $(".popupClose").click(function() {
	    $.colorbox.close();
	  });
	  $('#selectResume').change(function(e){
		$("#step1ResumeName").html($(this).val());
		document.getElementById("step1Tick").style.visibility = "visible";
		checkMultiUpload();
		if (multiUploadUsed)
		{
			if (confirm("You have already uploaded files using multiple file upload. Do you want to use this zip file instead?"))
			{
				return;
			}
			document.getElementById("selectResume").value = "";
		}
	  });
	  $('#JobDescFile').change(function(e){
		//$("#step1ResumeName").html($(this).val());
		document.getElementById("step2Tick").style.visibility = "visible";
	  });
	  
	  $("#logo").click(function() {
	    window.location="http://www.resumesort.com";
	  });

	});
  </script>
<script type="text/javascript">
<% if (error != null && error.length() > 0) { %>
	alert("Error Message: <%=error%>\n Please contact site support at support@textnomics.com");
<% } %>
	var EMPTY_JOB_TEXT = "<%=defaultJobText%>";
	var EMPTY_EMAIL_TEXT = "This is where your results will be emailed to";
	
	function clearDefaultJobText(el) 
	{
		if (el.value == EMPTY_JOB_TEXT)
			el.value = "";
	}

	function setDefaultJobText(el) 
	{
		if (el.value == "")
			el.value = EMPTY_JOB_TEXT;
	}

	function clearDefaultEmailText(el) 
	{
		if (el.value == EMPTY_EMAIL_TEXT)
			el.value = "";
	}

	function clearFileText()
	{
		document.getElementById("selectResume").value = "";
		//document.uploadform.targetfile.value="";
		var filedd = document.getElementById("filedropdown");
		document.getElementById("step1ResumeName").innerHTML = filedd.options[filedd.selectedIndex].text;
		document.getElementById("step1Tick").style.visibility = "visible";
		//document.uploadform.uploadResume.checked="";
	}
	function clearSourceFileText()
	{
		document.getElementById("JobDescFile").value = "";
	}	
	function showCheck2()
	{
		if (document.getElementById("theJobDescription").value != "" || document.getElementById("filedropdown2").selectedIndex != 0 || document.getElementById("JobDescFile").value != "")
		{
			document.getElementById("step2Tick").style.visibility = "visible";
		}
		else
		{
			document.getElementById("step2Tick").style.visibility = "hidden";
		}
		if (document.getElementById("filedropdown2").selectedIndex != 0)
		{
			document.getElementById("theJobDescription").value = "";
			var fileName = document.getElementById("filedropdown2").options[document.getElementById("filedropdown2").selectedIndex].value;
			if (fileName.indexOf(".txt") != -1)
			{
				 var url = "get_jobposting.jsp?jobpostingName=" + escape(fileName);
				 $.ajax({         
						url: url,         
						type: 'GET',         
						async: false,         
						cache: false,         
						timeout: 30000,         
						error: function(){return true;},
						success: function(msg){
							//alert(msg);
							document.getElementById("theJobDescription").value = msg;
						}
				});
			}
		}
	}

	function clearFileSelect()
	{
		//document.uploadform.selectedResume.selectedIndex=0;
		//alert("file selected1" + document.uploadform.targetfile.value);
		//var filepath = document.uploadform.targetfile.value;
		//ind = filepath.lastIndexOf("\\");
		//if (ind == -1) filepath.lastIndexOf("/");
		//if (ind != -1)
		//	filepath =  filepath.substr(ind+1));
		//alert("file selected2" + filepath);
		//document.getElementById("step1ResumeName").innerHTML = document.uploadform.targetfile.value;
	}	
	
	function setDefaultEmailText(el) 
	{
		if (el.value == "")
			el.value = EMPTY_EMAIL_TEXT;
	}

	var busy = false;
	function doAssign()
	{
		var assign = document.getElementById("Assign");
		if (assign != null)
			assign.value = "true";
		doSubmit();
	}
	function  doSubmit() 
	{
		if (isValid())
		{
			if (busy)
			{
				alert("Processing request... Please click OK and wait for your results.");
			}
			document.getElementById("loading").style.visibility = "visible";
			var myvar = "1";

			<% session.setAttribute("listSelected","1"); %>
                        <%-- session.setMaxInactiveInterval(3600); --%>
			busy = true;
			document.uploadform.submit();
		}		
	}
	function checkPostingId(obj)
	{
		var newPostingId = obj.value;
		//alert("checkPostingId:" + newPostingId);
		<% if (jpfileNames == null) { %>
			return;
		<%	} %>
	<%
		if(jpfileNames !=null && jpfileNames.length!=0){
		for(int i=0;i<jpfileNames.length;i++){
			String oldPosting = jpfileNames[i];
			if (oldPosting.indexOf(".") != -1)
				oldPosting = oldPosting.substring(0, oldPosting.lastIndexOf("."));
	%>
		if (newPostingId.toLowerCase() == '<%=oldPosting.toLowerCase()%>')
		{
			if (!confirm("This Job Posting Id already exists. Do you want to change it to:" + newPostingId + "-2?"))
				return false;
			newPostingId = newPostingId + "-2";
			obj.value = newPostingId;
		}
<%
		}
	} %>
		return true;
	}

	var multiUploadUsed = false;
	function checkUpload(data,status)
	{
		if (data != '0' && data != '')
		{
			multiUploadUsed = true;
			document.getElementById("step1Tick").style.visibility = "visible";
		}
	}
	function checkMultiUpload()
	{
		 $.ajax({         
				url: "check_resumes_uploaded.jsp",         
				type: 'GET',         
				async: false,         
				cache: false,         
				timeout: 30000,         
				error: function(){return true;},
				success: function(msg){
					//alert(msg);
					if (parseInt(msg) > 0){                 
						multiUploadUsed = true;
						return true;
					} else {                 
						return false;             
					}         
				}
		});
	}
	function  isValid()
	{				
		//if (document.getElementById("selectResume").value.indexOf(".pdf") != -1)
		//{
		//	alert("Sorry, pdf's are not supported");
		//	return false;
		//}
		checkMultiUpload();

		if (document.getElementById("selectResume").value=="" && (document.getElementById("filedropdown") == null || document.getElementById("filedropdown").selectedIndex == 0) && multiUploadUsed == false)
		{
			alert("Please upload you resume files");
			return false;
		}
		if (document.getElementById("theJobDescription").value=="" || document.getElementById("theJobDescription").value==EMPTY_JOB_TEXT )
		{
			if (document.getElementById("JobDescFile").value == "" && (document.getElementById("filedropdown2") == null || document.getElementById("filedropdown2").selectedIndex == 0))
			{
				alert("Please paste or upload a valid job ad.");
				return false;
			}
		}
		if (document.getElementById("JobPostingId").value == "")
		{
			if (document.getElementById("filedropdown2") == null || document.getElementById("filedropdown2").selectedIndex == 0)
			{
				if (!confirm("You have not entered a Job Posting Id in Step 2.\nWe will use a the following generated Id : <%=defaultJobId%> instead.\nIf you wish to use a different Id, please press Cancel and\nEnter your Job Posting Id in step 2."))
				{
					document.getElementById("JobPostingId").focus();
					return false;
				}
				else
				{
					document.getElementById("JobPostingId").value = "<%=defaultJobId%>";
				}
			}
		}
		else
		{
			if (!checkPostingId(document.getElementById("JobPostingId")))
				return;
		}


		var emailNew = document.getElementById("email");
		var	email = document.getElementById("emailResumeTo");
		if (email.value == '' && emailNew.value == '')
		{
			alert("Please enter your email!");
			return false;
		}

		if (emailNew.value != '' && document.getElementById("emailconfirm").value != emailNew.value)
		{
			alert("Your emails do not match! ");
			return false;
		}
		if (emailNew.value != '')
			email.value = emailNew.value;
		var industries = document.getElementsByName("industry");
		var indok = false;
		for (var i = 0; i < industries.length ; i++ )
		{
			if (industries[i].checked)
			{
				indok = true;
				break;
			}
		}
		if (!indok)
		{
			alert("Please check the industry(ies) your are recruiting for in Step 2");
			return false;
		}
		return true;
	}

	function uploadMultiFiles()
	{
		if (document.getElementById("selectResume").value != "")
		{
			if (confirm("You have already selected a zip file. Do you want to upload multiple files instead?"))
			{
				document.getElementById("selectResume").value = "";
			}
			else
				return;
		}
		var link = "<%=request.getContextPath()%>/multi_upload.jsp";
		window.open(link,"multiupload", "toolbar=no,statusbar=no,locationbar=no,scrollbars=yes,resizable=yes,top=25,left=100,width=900,height=400");
	}
	function multiUploadDone()
	{
		$.get("check_resumes_uploaded.jsp","",checkUpload);
	    $.colorbox.close();
	}
	
</script>
</head>
<body>
      <div id="fb-root"></div>
<% if (user == null) { %>
      <script src="http://connect.facebook.net/en_US/all.js"></script>
      <script>
		var usingFB = false;
	    var xmlhttp;
		FB.init({ 
			appId:'108463942572426', cookie:true, 
			status:true, xfbml:true 
		});
		FB.Event.subscribe('auth.sessionChange', function(response) {
		  window.location = 'rt_uploader.jsp';
		});
		FB.getLoginStatus(function(response) {
			if (response.session) {
				//alert("Logged In");
				get_fb_userinfo();
				usingFB = true;
				//document.getElementById('fblogin').innerHTML ='<a href="#" onclick="FB.logout();">FB Logout</a><br/>';
			} else {
				//alert("Not Logged In");
				document.getElementById('fblogin').innerHTML ='<fb:login-button perms="email">' + '</fb:login-button>';
				FB.XFBML.parse();
			}
		});
		function get_fb_userinfo() {
			FB.api('/me', function(response) {
				var userGraph = response;
				if (userGraph.email == null || userGraph.email == '')
				{
					document.getElementById('fblogin').innerHTML ='<fb:login-button perms="email">' + '</fb:login-button>';
				}
				else
				{
					if (document.getElementById('fblogin') != null)
						document.getElementById('fblogin').innerHTML ='<a href="#" onclick="FB.logout();">FB Logout</a><br/>';
					if (document.getElementById('register') != null)
						document.getElementById('register').innerHTML ='';
					if (document.getElementById('changepw') != null)
						document.getElementById('changepw').innerHTML ='';
					if (document.getElementById('rtlogin') != null)
						document.getElementById('rtlogin').innerHTML ='';
					//alert("Name1:" + userGraph.name + " email1:" + userGraph.email);
					if (xmlhttp == null)
						xmlhttp = GetXmlHttpObject();
					if (xmlhttp != null) {
						var url = "facebook_login.jsp";
						url = url + "?email=" + userGraph.email;
						url = url + "&name=" + userGraph.name;
						xmlhttp.open("POST", url, false);
						xmlhttp.send(null);
						var resp = xmlhttp.responseText;
						if (resp == "")
						{
							window.location = "rt_uploader.jsp"; // existing user, refresh to get file list
						}
					}
					//alert("Name2:" + userGraph.name + " email2:" + userGraph.email);
					var emailInp = document.getElementById("emailResumeTo");
					if (emailInp == null)
						emailInp = document.getElementById("email");
					emailInp.value = userGraph.email;
					document.getElementById("emailconfirm").value = userGraph.email;
					document.getElementById('step3').class = "step returning_user_email";
				}
			  });
		}
		function GetXmlHttpObject() {
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				return new XMLHttpRequest();
			}
			if (window.ActiveXObject) {
				// code for IE6, IE5
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
			return null;
		}     
	</script>
<% } %>

<header id="header" class="central">
  <h1 id="logo">Resume <strong>Sort</strong></h1>
  <nav id="user-nav">
	<% if (user == null) { %>
	  <span id="rtlogin"><a href="Login.jsp">Login</a></span>
	  <span id="fblogin"></span>
	  <span id="register"><a href="Register.jsp">Register</a></span>
	<% } else { %>
		<% if (user.isAdmin()) { %>
		  <a href="job_posting_report.jsp">Job Postings</a>
		  <a href="resume_folders.jsp">Resume Folders</a>
		<% } %>
		<% if (user.isTester()) { %>
		  <a href="wikicleanup.jsp">Wiki DB</a>
		  <a href="oxfordcleanup.jsp">Oxford DB</a>
		<% } %>
	  <a href="Login.jsp?logout=true">Logout</a>
  	  <a href="ChangePassword.jsp">Change Password</a>
	<% } %>
  </nav>
</header>

<div id="siteTag">
  <h2 class="central">The most advanced resume sorter on the planet</h2>
</div>
<form name="uploadform" enctype='multipart/form-data' method='post' action='ResumeSorter'>
<div class="central">
  <div id="socialLinks" align=right>
    <!-- facebook -->
    <iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fresumetuner&amp;layout=button_count&amp;show_faces=false&amp;width=100&amp;action=like&amp;font&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
    <!-- twitter -->
	<a href="http://twitter.com/share" class="twitter-share-button" data-url="http://www.resumetuner.com" data-text="Optimize your resume with resumetuner.com - the most advanced resume tuner on the planet." data-count="none" data-via="resumetuner">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <!-- linkedin -->
	<!-- script type="text/javascript" src="http://platform.linkedin.com/in.js"></script><script type="in/share" data-url="http://www.resumetuner.com"></script -->
	<!-- ADDTHIS BUTTON BEGIN -->
	<script type="text/javascript"> 
	var addthis_pub = "ebbanari ";
	</script> 
	 
	<a href="http://www.addthis.com/bookmark.php?v=20" 
		onmouseover="return addthis_open(this, '', 'http%3A%2F%2Fwww.resumetuner.com', 'Optimize your resume with ResumeTuner - the most advanced resume tuner on the planet.');" 
		onmouseout="addthis_close();" 
		onclick="return addthis_sendto();"><img 
		src="http://s7.addthis.com/static/btn/lg-share-en.gif" 
		width="125" height="16" border="0" alt="Share" /></a> 
	 
	<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script> 
	 
	<!-- ADDTHIS BUTTON END -->
	</div>
  <h3 id="homeTag">Use our sorting technology to quickly and easily score and sort your resumes to find the best matches for your Job posting.</h3>
  <section id="steps">
    <div class="step" id="step1">
      <h3>Step 1</h3>
      <p>Upload your resume file</p>
	  <span id="step1ResumeName">Please select a zip file</span>
	  <img style="visibility:hidden" src="images/green_arrow_transparent.png" alt="" id="step1Tick" />
    </div>
    <div class="step" id="step2">
      <h3>Step 2</h3>
      <p>Copy &amp; Paste job description</p>
      <img src="images/green_arrow_transparent.png" alt="" style="visibility:hidden" id="step2Tick" />                           
    </div>
<%  
	String ecls = "step new_user_email";
	boolean newuser = true;
	if (request.getAttribute("email") != null && request.getAttribute("email").toString().trim().length() > 0)
	{
		ecls = "step returning_user_email";
		newuser = false;
	}
%>
    <div class="<%=ecls%>" id="step3">
      <h3>Step 3</h3>
<%	if (!newuser) { %>
	  <input type="text" name="email" value="<%out.write((String)request.getAttribute("email")); %>" id="emailResumeTo"  class="placeHolder"/>
<%		if (!user.isAdmin()) { %>
	  <input type="button" value="Ready, Let's sort!" id="step3Submit" onclick="doSubmit()"/>
<%		} else { %>
	  <input type="button" value="Ready, Let's sort!" id="step3Sort" onclick="doSubmit()"/>
	  <input type="button" value="Assign!" id="step3Assign" onclick="doAssign()"/>
<%		} %>
	  <input type="hidden" id="Assign" name="Assign" value="false"/>
<%	} %>

    </div>
  </section>
  
  
  <div id="homeVideo">
    <iframe title="YouTube video player" width="300" height="255" src="http://www.youtube.com/embed/KJJW7EF5aVk?wmode=transparent" frameborder="0" allowfullscreen></iframe>
  </div>
  
  <!-- popups -->
  <div style="display:none">
    <!-- step1 popup -->
    <div id="uploadResume" class="popup">
	  <h3>Select or Add Multiple Resumes</h3>
	  <p style="color:#333"><b>We accept .doc, .docx, .rtf, .odt, .txt and .pdf* files or a zip file containing them</b>
	  <br />
	  <h3><input type=button onclick="javascript:uploadMultiFiles()" value="Click to Upload Multiple Files"></h3>
	  <br />
	  <div id=zipfileupload style="display:none">
	  <h3>Or Upload a single Zip File</h3-->
	  <p><input type="file" name="targetfile" size="30" value="" id="selectResume"/></p>
	  <br />
	  </div>
	<%
		File[] files = (File[])request.getAttribute("myResumes");
		if(files != null && files.length != 0){
	%>
	  <h3> Or for <u>Faster</u> Processing Select a Previous Folder**:</h3>
	<select name='selectedResume' onchange="clearFileText()" id="filedropdown">
				<option value ='' >-- Select A Folder -- </option>
<%
		for(int i=0;i<files.length;i++){
			if ((false && user != null && user.getEmail().equals("dev.gude@textnomics.com") && files[i].getName().endsWith(".doc")) || files[i].isDirectory() )
				out.write("<option>"+files[i].getName()+"</option>");
		}
%>
		</select>
<%	} %>
	  <div style="text-align:right">
	    <input type="submit" value="Continue" class="button blue big popupClose continueButton" />
	  </div>
	  <br>
	  *Formatting of pdf files may be lost in the processing
	  <br>
	  <br>
	  **Please note that we do not maintain Resume files for the one year period that is federally mandated for ATS systems
	</div>
	<!-- step2 popup -->
	<div id="jobDescriptionPopUp" class="popup" style="padding:20px">
	  <div id="jobDescription" style="padding:0 0 20px">
	    <h3>Job Posting ID:  <input id="JobPostingId" type="text" name="JobPostingId" onchange="checkPostingId(this)" size="40"></h3>
		<br>
	    <h3>Browse for the job description file:
		<input id="JobDescFile" type="file" name="sourcefile" size="40" onchange="showCheck2()"></h3>
		<br>
	<%
		String[] fileNames = (String[])request.getAttribute("myPostings");
		if(fileNames !=null && fileNames.length!=0){
	%>
	<br><strong>OR</strong><br><br>
	  <h3>Select a previous posting:
	<select name='selectedPosting' onchange="clearSourceFileText();showCheck2()" id="filedropdown2">
				<option value ='' >-- Select Saved Job Posting -- </option>
<%
		for(int i=0;i<fileNames.length;i++){
			if (!fileNames[i].endsWith(".Object"))
				out.write("<option>"+fileNames[i]+"</option>");
		}
%>
		</select></h3>
<%	} %>
	<br><strong>OR</strong><br><br>
	    <h3>Copy and paste job description here:</h3>
	    <textarea rows="8" cols="50" name="sourcetext" id="theJobDescription" onchange="showCheck2();" onfocus="clearDefaultJobText(this)"
					onblur="setDefaultJobText(this)">
<% if (false && user != null && user.getEmail().startsWith("dev.gude@text")) {
		out.print( testJobText); 
	} else {
		out.print( defaultJobText); 
	}
%>				</textarea>
	    <p><a href="#" id="hideJobDescription" style="display:none;color:#666">Click here if you don't have a job description</a></p>
	  </div>
	  <h3>Type of industry you are recruiting for:</h3>
	  <div class="checkboxes" style="width:600px">
	  <table>
	  <tr>
<%
	String userIndustry = user.getIndustry();
	if (userIndustry == null) userIndustry = "";
	String checked = "";
	String industry = "";
	boolean craigslist = true;
	if (!craigslist)
	{
	industry = "Accountant | CPA | Controller";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Financial Analyst";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Banker";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Administrative Assistant";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "Architecture | Interior Design";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Art | Media";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Graphic Designer";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Business | Management";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "Marketing | Public Relations | Advertising";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Marketing Associate";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Retail | Wholesale";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Sales | Business Development";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "Business Development";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Sales Representative";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Product Manager - IT | non-IT";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Customer Service Representative";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "Biotech";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Construction | Civil Engineering";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Mechanical Engineering";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Electrical Engineering";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "Internet Engineering";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "WEB Design";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Software | QA | DBA | OO";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Systems | Network";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "Programmer";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Education";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "General Labor";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Government | Federal | State";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "Human Resources";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Security";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Legal | Paralegal";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Manufacturing";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "Medical | Health";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Real Estate";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Restaurant | Food | Beverage | Hospitality";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Skilled Trade | Craft";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "Social Services | Nonprofit";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Transportation";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "TV | Film | Video";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "Writing | Editing";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "Other";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
		</td><td nowrap>
		</td><td nowrap>
		</td><td nowrap>
<%  
	}
	else
	{
	industry = "accounting+finance";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "admin / office";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "arch / engineering";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "art / media / design";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "biotech / science";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "business / mgmt";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "customer service";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "education";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "food / bev / hosp";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "general labor";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "government";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "human resources";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "internet engineers";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "legal / paralegal";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "manufacturing";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "marketing / pr / ad";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "medical / health";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "nonprofit sector";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "real estate";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "retail / wholesale";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "sales / biz dev";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "salon / spa / fitness";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "security";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "skilled trade / craft";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "software / qa / dba";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "systems / network";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "technical support";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "transport";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "tv / film / video";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%
	industry = "web / info design";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
</tr></tr>
<%
	industry = "writing / editing";
	checked = ""; if (userIndustry.indexOf(industry + "\n") != -1) checked = "checked";
%>
<td nowrap><input type="checkbox" <%=checked%> value="<%=industry%>" name="industry"/> <%=industry%></td>
<%  } %>
		</td></tr></table>
      </div>
	  <div style="text-align:right">
	    <input type="submit" value="Continue" class="button blue big popupClose continueButton" />
	  </div>
	</div>
	<!-- step3 popup -->
	<div id="emailAddressPopUp" class="popup">
	  <h3>Email my tuned resume to:</h3>
	  <p><input type="text" id="email" name="email" value="" style="width:177px" /></p>
	  <br />
	  <h3>Re-type your email address:</h3>
	  <p><input type="text" id="emailconfirm" name="emailconfirm" value="<%=request.getAttribute("email")%>" style="width:177px" /></p>
	  <p><input type="checkbox" value="" id="receiveUpdates" /> Receive updates from ResumeSort</p>
	  <!--input type="submit" value="Ready, Let's tune!" id="step3Submit" /-->
	  <div style="text-align:right">
	    <input type="button" value="Ready, Let's sort!" class="button blue big popupClose continueButton"  onclick="doSubmit()" />
	  </div>
	</div>
  </div>
  
  <div class="clearThis"></div>
  
  <div id="loading" style="visibility: hidden">
    <p>Processing...</p>
  </div>
</div>

<table style="display: none">
	<tr>
		<td colspan="3">
		<select name='algo'>
			<option >Selective On Choices</option>
			<option>More Comprehensive (feeling lucky?)</option>
			<option selected>WN - jiang_conrath</option>
			<option>WN - adapted_lesk</option>
			<option>WN - adapted_lesk_tanimoto</option>
			<option>WN - adapted_lesk_tanimoto_hyponyms</option>
			<option>WN - hirst_stonge</option>
			<option>WN - leacock_chodorow</option>
			<option>WN - lin</option>
			<option>WN - path</option>
			<option>WN - resnik</option>
			<option>WN - wu_palmer</option>
			<option>WN - synset_word_count</option>
			<option>WN - iic_jcn</option>
			<option>Thesaurus Overlap</option>
			<option>WN - hyponyms</option>
			<option>GENERATE_QUICK_MATRICES</option>
			<option>GENERATE_ALL_MATRICES</option>
		</select>
		<input style="display: none" type="text" name="partitionFactor" value="4.5"/>
		</td>				
	</tr>
	<tr style="display: none">
		<td><b>Similarity Threshold</b></td>

		<td><select name='similaritythreshold'>
			<option>0.5</option>
			<option>0.55</option>
			<option>0.60</option>
			<option>0.65</option>
			<option selected="selected">0.70</option>
			<option>0.75</option>
			<option>0.80</option>
			<option>0.85</option>
			<option>0.90</option>
			<option>0.95</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>
	<tr style="display: none">
		<td><b>Frequent Word Filter (Source)</b></td>
		<td><select name='sourcefrequentwordfilter'>
			<option>NONE</option>
			<option>Default 100</option>
			<option>Default 300</option>

			<option>Default 600</option>
			<option selected="selected">Default All</option>
		</select></td>

		<td>&nbsp;</td>
	</tr>
	<tr style="display: none">
		<td><b>Information Content Filter (Source)</b></td>

		<td><select name='sourceinformationcontentfilter'>

			<option>0.40</option>
			<option>0.45</option>
			<option>0.50</option>
			<option>0.55</option>
			<option selected="selected">0.60</option>
			<option>0.65</option>
			<option>0.70</option>
			<option>0.75</option>
			<option>0.80</option>
			<option>0.85</option>
			<option>0.90</option>
			<option>0.95</option>
		</select></td>

		<td>&nbsp;</td>
	</tr>
	<tr style="display: none">

		<td><b>Frequent Word Filter (Target)</b></td>

		<td><select name='targetfrequentwordfilter'>
			<option>NONE</option>
			<option>Default 100</option>
			<option selected="selected">Default 300</option>
			<option>Default 600</option>

			<option>Default All</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>
	<tr style="display: none">
		<td><b>Information Content Filter (Target)</b></td>

		<td><select name='targetinformationcontentfilter'>
			<option>0.40</option>
			<option>0.45</option>
			<option>0.50</option>
			<option>0.55</option>
			<option selected="selected">0.60</option>
			<option>0.65</option>
			<option>0.70</option>
			<option>0.75</option>
			<option>0.80</option>
			<option>0.85</option>
			<option>0.90</option>
			<option>0.95</option>
		</select></td>
		<td>&nbsp;</td>

	</tr>
	<tr style="display: none">
		<td><b>Parser (Source)</b></td>

		<td><select name='sourceparser'>
			<option>OPENNLP_NONOVERLAPPING</option>
			<option selected>OPENNLP_OVERLAPPING</option>
			<option >OPENNLP_CHUNKS</option>
			<option >STANFORD</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>
	<tr style="display: none">
		<td><b>Parser (Target)</b></td>

		<td><select name='targetparser'>
			<option selected>OPENNLP_NONOVERLAPPING</option>
			<option >OPENNLP_OVERLAPPING</option>
			<option>OPENNLP_CHUNKS</option>
			<option >STANFORD</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>

	<tr style="display: none">
		<td><b>Style Filter (Target)</b></td>

		<td><select name='targetstylefilter'>
			<option>NONE</option>
			<option selected="selected">Default</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>

	<tr style="display: none">
		<td colspan="3">&nbsp;</td>
	</tr>

	<tr style="display: none">

		<td><b>Numeric Filter (Target)</b></td>
		<td><select name='targetnumericfilter'>
			<option>NONE</option>
			<option selected="selected">Default</option>
		</select></td>
		<td>&nbsp;</td>

	</tr>
	<tr style="display: none">
		<td><b>Numeric Filter (Source)</b></td>

		<td><select name='sourcenumericfilter'>

			<option>NONE</option>
			<option selected="selected">Default</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>
	<tr style="display: none">
		<td><b>Max. NGram Size</b></td>
		<td><select name='ngramsize'>
			<option>1</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
			<option>5</option>
			<option>6</option>
			<option>7</option>
			<option>8</option>
			<option selected="selected">9</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>

	<tr style="display: none">
		<td><b>Keyword Tokenizer</b></td>
		<td><select name='keywordtokenizer'>
			<option>NONE</option>
			<option selected="selected">Default</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>
	<tr style="display: none">

		<td><b>Wordnet Tokenizer</b></td>


		<td><select name='wordnettokenizer'>
			<option>NONE</option>
			<option selected="selected">Default</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>
	<tr style="display: none">
		<td><b>POS Tokenizer</b></td>


		<td><select name='postokenizer'>
			<option>NONE</option>
			<option selected="selected">Default</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>
	<tr style="display: none">
		<td><b>DropDown Output</b></td>

		<td><input type="checkbox" name="dropdownoutput"
			value="dropdownoutput" checked="checked" /></td>
		<td>&nbsp;</td>
	</tr>

	<tr style="display: none">
		<td><b>WordNet Phrase Filter (Target)</b></td>
		<td><select name='targetwordnetfilter'>
			<option>NONE</option>			
			<option selected="selected">Default</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>
	<tr style="display: none">
			<td colspan="3">&nbsp;</td>
	</tr>
	<tr style="display: none">
		<td><b>WordNet Phrase Filter (Source)</b></td>
		<td><select name='sourcewordnetfilter'>
			<option>NONE</option>			
			<option selected="selected">Default</option>
		</select></td>
		<td>&nbsp;</td>
	</tr>

	<!--------------------------END HIDDEN PARAMETERS-------------------------------------->

</table>
</form>
<script>
 if (!navigator.javaEnabled()) 
	document.getElementById("zipfileupload").style.display = "inline";
</script>
<%@include file="rq_footer.jsp"%>
  
</body>
</html>