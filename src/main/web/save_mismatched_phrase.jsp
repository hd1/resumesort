<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%
		String phraseIdx = request.getParameter("idx");
		String suggestionNum = request.getParameter("suggestionNum");
		String suggestionText = request.getParameter("suggestionText");
		String command = request.getParameter("command");
		System.out.println("command:" + command + " suggestionNum:" + suggestionNum + " suggestionText:" + suggestionText+ " phraseIdx:" + phraseIdx);
		if (phraseIdx == null || (suggestionNum == null && suggestionText == null)) return;
		List<String> optionsList = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.OPTIONS_LIST.toString());
		if (optionsList == null) return;
		List<String> extrasIndex = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.EXTRAS_INDEX.toString());
		List<String> deletedList = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.DELETED_LIST.toString());
		if (deletedList == null)
		{
			deletedList = new ArrayList<String>();
			for (int i = 0; i < optionsList.size(); i++)
				deletedList.add("");
			session.setAttribute(SESSION_ATTRIBUTE.DELETED_LIST.toString(), deletedList);
		}
		String options = optionsList.get(getInt(phraseIdx));
		String[] list = optionsList.get(getInt(phraseIdx)).split(" _LIMIT_ ");
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		try
		{
			PhraseSuggester<? extends SuggestionCluster> suggester = 
				(PhraseSuggester<? extends SuggestionCluster>) session.getAttribute(SESSION_ATTRIBUTE.SUGGESTER.toString());
			Map<Phrase, SuggestionSet> suggestionMap = suggester.getSuggestionMap();
			Set<Phrase> targetPhrases = suggestionMap.keySet();
			int i = 0;
			String targetPhrase = "";
			String suggestedPhrase = "";
			String targetSentence = "";
			String sourceSentence = "";
			for (Phrase phrase : targetPhrases)
			{
				if (i == getInt(phraseIdx))
				{
					SuggestionSet suggSet = suggestionMap.get(phrase);
					int j = 1;
					int idx = 0;
					Suggestion markSugg = null;
					for (Suggestion suggestion : suggSet)
					{
						if ((suggestionNum != null && j == getInt(suggestionNum)) || 
							(suggestionText != null && suggestion.getNonInflectedPhrase().replace('\'',' ').replace('"',' ').equalsIgnoreCase(suggestionText)))
						{
							targetPhrase = phrase.getBuffer();
							targetSentence = suggester.getTarget().getSentences().get(phrase.getSentence()).toString();
							//System.out.println("Target sentence:" + targetSentence);
							suggestedPhrase = suggestion.toString();
							sourceSentence = suggestion.getContexts().get(0);
							//System.out.println("Source sentence:" + sourceSentence);
							markSugg = suggestion;
							idx = j;
							break;
						}
						j++;
					}
					if (markSugg != null) 
					{
						if ("delete".equals(command) || "deleteAll".equals(command))
						{
							System.out.println("Removed:" + markSugg.toString());
							//suggSet.remove(remove);
							options = "";
							String delim = "";
							int suggestNum = 0;
							for (int k = 0; k < list.length; k++)
							{
								if (list[k].equals(markSugg.toString()))
								{
									suggestNum = k;
									continue;
								}
								options = options + delim + list[k];
								delim = " _LIMIT_ ";
							}
							//optionsList.set(getInt(phraseIdx), options);
							String deleted = deletedList.get(getInt(phraseIdx));
							String[] deletedArray = deleted.split(",");
							String newDeleted = "";
							delim = "";
							for (int k = 0; k < deletedArray.length; k++)
							{
								if (deletedArray[k].trim().length() == 0 || deletedArray[k].trim().equals(""+suggestNum)) continue;
								newDeleted = newDeleted + delim + deletedArray[k];
								delim = ",";
							}
							deletedList.set(getInt(phraseIdx), newDeleted + delim + suggestNum);
							System.out.println("New DeletedList1:" + deletedList.get(getInt(phraseIdx)));
						}
						else // undelete
						{
							System.out.println("Undelete:" + markSugg.toString());
							int suggestNum = 0;
							for (int k = 0; k < list.length; k++)
							{
								System.out.println("List:" + k + ":" + list[k] + " sugg:" + markSugg.toString());
								if (list[k].equals(markSugg.toString()))
								{
									suggestNum = k;
									continue;
								}
							}
							//optionsList.set(getInt(phraseIdx), options);
							String deleted = deletedList.get(getInt(phraseIdx));
							String[] deletedArray = deleted.split(",");
							String newDeleted = "";
							String delim = "";
							for (int k = 0; k < deletedArray.length; k++)
							{
								if (deletedArray[k].trim().length() == 0 || deletedArray[k].trim().equals(""+suggestNum)) continue;
								newDeleted = newDeleted + delim + deletedArray[k];
								delim = ",";
							}
							deletedList.set(getInt(phraseIdx), newDeleted);
							System.out.println("New DeletedList2:" + deletedList.get(getInt(phraseIdx)));
						}
					}
				}
				i++;
			}
			if ("delete".equals(command) && targetPhrase.length() > 0)
			{
				UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();
				provider.insertMismatchedPhrase(targetPhrase, suggestedPhrase, targetSentence, sourceSentence, user.getLogin());
				provider.commit();
			}
			else if ("undelete".equals(command) && targetPhrase.length() > 0)
			{
				UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();
				provider.deleteMismatchedPhrase(targetPhrase, suggestedPhrase, user.getLogin());
				provider.commit();
			}
		}
		catch (UserDataProviderException e)
		{
			e.printStackTrace();
		}
		finally
		{
			UserDataProviderPool.getInstance().releaseProvider();//Always release the provider
		}
%>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>