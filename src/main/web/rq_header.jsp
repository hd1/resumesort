<!DOCTYPE html>
<html lang="en">
<head>
  <title>ResumeSort - the most advanced resume sorting technology on the planet.</title>
  <meta charset="utf-8" />
  <meta http-equiv="cache-control" content="no-store"/>
  <meta http-equiv="pragma" content="no-cache"/>
  <meta http-equiv="expires" content="0"/>
  <!--[if lt IE 9]>
   <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="all.css" />
  <link rel="stylesheet" href="colorbox/colorbox.css" />
  <!--[if lt IE 9]>
  <link rel="stylesheet" href="ie.css" />
  <![endif]-->  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
  <script src="colorbox/jquery.colorbox-min.js"></script>
  <script>
    //$(function() {
	//  $("#logo").click(function() {
	//    window.location="http://www.resumesort.com";
	//  });
//
	//});
  </script>
</head>
<body>

<header id="header" class="central">
  <h1 id="logo">Resume <strong>Sort</strong></h1>
  <nav id="user-nav">
<% if (session.getAttribute(com.textonomics.web.SESSION_ATTRIBUTE.CURRENT_USER.toString()) == null) { %>	
  <a href="Login.jsp">Login</a><a href="javascript:alert('Not implemented for Alpha/Beta');"><img src="images/login_via_facebook.png" alt="" /> Login via facebook</a><a href="Register.jsp">Register</a>
<% } else { %>
		<% if (((com.textonomics.user.User)session.getAttribute(com.textonomics.web.SESSION_ATTRIBUTE.CURRENT_USER.toString())).isAdmin()) { %>
		  <a href="job_posting_report.jsp">Job Postings</a>
		  <a href="resume_folders.jsp">Resume Folders</a>
		<% } %>
	  <a href="Login.jsp?logout=true">Logout</a>
  	  <a href="ChangePassword.jsp">Change Password</a>
<% } %>
	</nav>
</header>

<div id="siteTag">
  <h2 class="central">The most advanced resume sorter on the planet</h2>
</div>

