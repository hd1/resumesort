<%@ page import="java.io.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@include file="header.jsp"%>
<%
	session.removeAttribute(SESSION_ATTRIBUTE.UPLOAD_ERROR.toString());
	String previousFolder = request.getParameter("resumeFolder");
	String []jpfileNames=null;
	if (user != null)
	{
		request.setAttribute("email", user.getEmail());
	
		//Get existing list of MyResumes
		String uploadDir="uploads" + File.separator + user.getLogin() + File.separator;
		File userFolder = PlatformProperties.getInstance().getResourceFile(
								uploadDir + File.separator + "MyResumes" + File.separator);
		//Content of folder
		File []files=null;
		if(userFolder.exists()){
				files=userFolder.listFiles();
				request.setAttribute("myResumes", files);
				System.out.println("Resume files:" + files.length);
		}
		else
				userFolder.mkdirs();
		
		userFolder = PlatformProperties.getInstance().getResourceFile(
								uploadDir + File.separator + "MyPostings" + File.separator);
		//Content of folder
		if(userFolder.exists()){
				jpfileNames=userFolder.list();
				request.setAttribute("myPostings", jpfileNames);
				System.out.println("Job files:" + jpfileNames.length);
		}
		else
				userFolder.mkdirs();
	}
	else // For Beta only
	{
		response.sendRedirect("");
		return;
	}
	JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	if (posting == null)
	{
		response.sendRedirect("upload_jobposting.jsp");
		return;
	}
	Boolean html5upload = (Boolean) session.getAttribute(SESSION_ATTRIBUTE.HTLM5_UPLOAD.toString());
	System.out.println("html5upload: " + html5upload);
%>
    <script type="text/javascript">
$(document).ready(function(){
	
	$(".accordion li:first").addClass("active");
	$(".accordion li.d:not(:first)").hide();

	$(".accordion li").click(function(){
		$(this).next("li.d").slideToggle("slow")
		.siblings("li.d:visible").slideUp("slow");
		$(this).toggleClass("active");
		$(this).siblings("li").removeClass("active");
	});
	if (!navigator.javaEnabled() && navigator.userAgent.indexOf("MSIE") > 0) 
		document.getElementById("zipfileupload").style.display = "inline";

});
</script>    
<script type="text/javascript">
	var numfilesUploaded = 0;
	function copyFolderName()
	{
		var ind = document.uploadform.PrevFolder.selectedIndex;
		var name = document.uploadform.PrevFolder.options[ind].value;
		if (name != '')
		{
			document.uploadform.ResumeFolder.value = name;
		}
		if (folderFiles.length > ind)
		{
			numfilesUploaded = folderFiles[ind];
			//alert("ind:" + ind + " numfilesUploaded=" + numfilesUploaded);
		}
	}
	function clearFileText()
	{
	}

	var multiUploadUsed = false;
	function checkUpload(data,status)
	{
		if (data != '0' && data != '')
		{
			multiUploadUsed = true;
			document.getElementById("step1Tick").style.visibility = "visible";
		}
	}
	function checkMultiUpload()
	{
		 $.ajax({         
				url: "check_resumes_uploaded.jsp",         
				type: 'GET',         
				async: false,         
				cache: false,         
				timeout: 30000,         
				error: function(){return true;},
				success: function(msg){
					//alert(msg);
					if (parseInt(msg) > 0){                 
						multiUploadUsed = true;
						return true;
					} else {                 
						return false;             
					}         
				}
		});
	}
	function checkUploadError()
	{
		 $.ajax({         
				url: "getSessionAttribute.jsp?attribute=UPLOAD_ERROR",         
				type: 'GET',         
				async: false,         
				cache: false,         
				timeout: 30000,         
				error: function(){return true;},
				success: function(msg){
					if (msg.length > 0 && msg != 'null'){
						alert(msg);
					} else {                 
					}         
				}
		});
	}
	
	var dragUpload = false;
	var newFolderSelected = false;
	var busy = false;
	var uploadBusy = false;
	function gotoStep3(askfriends)
	{
		if (busy)
		{
			alert("Processing....");
			return;
		}
		if (!multiUploadUsed)
		{
			checkMultiUpload();
		}
		checkUploadError();
		var mailbox = document.getElementById("Mailbox");
		var mailboxCreated = false;
		if (mailbox != null && mailbox.checked)
		{
			mailboxCreated = true;
		}
		mailboxCreated = <%=posting.getEmailAddress() != null && posting.getEmailAddress().length() > 0%> || mailboxCreated;
		if (!multiUploadUsed && !dragUpload && document.uploadform.targetfile.value == "" && document.uploadform.PrevFolder.selectedIndex == 0)
		{
			if (mailboxCreated)
			{
				if (!confirm("Do you want to proceed to Phrase Selection without uploading resumes (to be emailed later)?" ))
				{
					return;
				}
			}
			else
			{
				alert("Please select Type and Upload Resumes or Create a Mailbox");
				return;
			}
		}
		if (document.uploadform.ResumeFolder.value == '')
		{
			alert("Please select folder to use");
			return;
		}
		if (document.getElementById("radioAddPrevFolder") != null && document.getElementById("radioAddPrevFolder").checked && !multiUploadUsed && !dragUpload  && document.uploadform.targetfile.value == "")
		{
			if (!confirm("Do you want to proceed to Phrase Selection without uploading any more resumes?" ))
			{
				return;
			}
		}
		var assign = "";
		if (askfriends)
		{
			assign = "&Assign=true"
		}
		var createmail = "";
		if (mailbox != null && mailbox.checked)
		{
			createmail = "&mailbox=true"
		}
		if (!multiUploadUsed && document.uploadform.targetfile.value == "" || dragUpload)
			window.location="ProcessResumes?resumeFolder=" + escape(document.uploadform.ResumeFolder.value) + assign + createmail;
		else
			window.location="ProcessResumes?resumeFolder=" + escape(document.uploadform.ResumeFolder.value) + assign + createmail;
		busy = true;
	}

	function checkNewFolder()
	{
		if (newFolderSelected)
		{
			var dropdown = document.getElementById("filedropdown");
			if (dropdown != null)
			{
				for (i = 1; i < dropdown.options.length ; i++ )
				{
					if (dropdown.options[i].value == document.uploadform.ResumeFolder.value)
					{
						alert("Please select a different folder name. " + document.uploadform.ResumeFolder.value + " already exists");
						return;
					}
				}
			}
		}
	}

	function selPrevFolder()
	{
		newFolderSelected = false;
		document.getElementById("filedropdown").style.display = 'inline';
		var dragdrop = document.getElementById("dragdrop");
		if (dragdrop != null)
			dragdrop.style.display = 'none';
		document.getElementById("newFolder").style.display = 'none';
		document.getElementById("filedropdown").selectedIndex = 0;
		document.getElementById("ResumeFolder").value = '';
	}

	function addPrevFolder()
	{
		newFolderSelected = false;
		document.getElementById("filedropdown").style.display = 'inline';
		var dragdrop = document.getElementById("dragdrop");
		if (dragdrop != null)
			dragdrop.style.display = 'inline';
		document.getElementById("newFolder").style.display = 'none';
		document.getElementById("filedropdown").selectedIndex = 1;
		document.getElementById("ResumeFolder").value = document.getElementById("filedropdown").options[1].value;
		if (folderFiles.length > 1)
		{
			numfilesUploaded = folderFiles[1];
			//alert("ind:" + ind + " numfilesUploaded=" + numfilesUploaded);
		}
	}
	function newFolder()
	{
		var filedrop = document.getElementById("filedropdown");
		if (filedrop != null)
			filedrop.style.display = 'none';
		var dragdrop = document.getElementById("dragdrop");
		if (dragdrop != null)
			dragdrop.style.display = 'inline';
		document.getElementById("newFolder").style.display = 'inline';
		var filedrop = document.getElementById("filedropdown");
		if (filedrop != null)
			filedrop.selectedIndex = 0;
		document.getElementById("ResumeFolder").value = '<%=posting.getPostingId().replace(' ','_')%>';
		document.getElementById("ResumeFolder").focus();
		document.getElementById("ResumeFolder").select();
		newFolderSelected = true;
	}
	function setTikaProperty(value)
	{
		 $.ajax({         
				url: "setUserProperty.jsp?propertyName=com.textnomics.tika_sentence_detector&propertyValue=" + value,         
				type: 'GET',         
				async: false,         
				cache: false,         
				timeout: 30000,         
				error: function(){return true;},
				success: function(msg){
					//alert(msg);
				}
		});
	}
	function gotoUploadJobPosting()
	{
		if (confirm("Are you sure you want to leave this page and start over?"))
		{
			window.location = "upload_jobposting.jsp";
		}
	}
	function goHome()
	{
		if (confirm("Are you sure you want to leave this page and start over?"))
		{
			window.location = "home.jsp";
		}
	}
	var folderFiles = new Array();

	function createMailbox(cbx)
	{
		var reqnum = "<%=posting.getPostingId().toLowerCase()%>";
		var name = document.getElementById("MailboxName");
		if (cbx.checked)
		{
			name.innerHTML = " <b>Address:</b> " + reqnum.replace(' ','_').toLowerCase() + "@resumesort.com";
		}
		else
		{
			name.innerHTML = '';
		}
	}

</script>
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
	<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    
    <script type="text/javascript">
        $(function() {
	        $("a[rel^='prettyPhoto']").prettyPhoto();

        });
    </script>

<link href="css/smart_wizard.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" media="all" href="fileupload.css" />
<div class="middle">
<div class="maintext_dashboard">
 
 <div class="sky_bar">
    <div style="float:right; clear:both;">
    <a href="javascript:gotoUploadJobPosting()"  class="selected">START NEW JOBS</a>
    <a href="javascript:goHome()">PREVIOUS JOBS</a>
    </div>
 </div>
 
 <div class="text_dashboard">
  <div id="loading" style="visibility: hidden">
    <p>Processing...</p>
  </div>
<table align="center" border="0" cellpadding="0" cellspacing="0">
<tr><td> 
<!-- Smart Wizard -->
  		<div id="wizard" class="swMain">
  			<ul class="anchor">
  				<li><a href="javascript:gotoUploadJobPosting()">
                <label class="stepNumber">1</label>
                <span class="stepDesc">
                   Upload Job Posting<br />
                   <small>Job Posting Details</small>
                </span>
            </a></li>
   				<li><a href="#step-1" class="selected">
                <label class="stepNumber">2</label>
                <span class="stepDesc">
                   Upload Resumes <br/> 
                   <small>Upload to Resume Folder</small>              
                </span>
            </a></li>
 				<li><a class="mini" href="javascript:gotoStep3()">
                <label class="stepNumber">3</label>
                <span class="stepDesc">
                   Select Phrases
                </span>                   
             </a></li>
  				<li><a class="miniRight" href="#step-4">                
                <span class="stepDesc">
                   Ask My Friends
                </span>                   
            </a></li>
  			</ul>
  			<div class="stepContainer">	
            <div class="content">
                <div style="text-align:center; margin-top:20px; margin-bottom:20px;">
 <form id="upload" name="uploadform" action="<%=request.getContextPath()%>/SingleFileUpload" method="POST" enctype="multipart/form-data">
 <% if (false && user.isAdmin()) { 
	String value = PlatformProperties.getInstance().getUserProperty("com.textnomics.tika_sentence_detector");
	String checked = "";
	if ("true".equals(value)) checked = "checked";
	%>
	<input type=checkbox name=useTika <%=checked%> onclick="setTikaProperty(this.checked)"> Use Tika to process Resumes<br>
 <%	} %>
	<div style="float:left;">
<%
		File[] files = (File[])request.getAttribute("myResumes");
		if(files != null && files.length != 0){
%>
                 <h2 style="text-align:left;margin-left:300px"><input type=radio name=prev onclick="selPrevFolder()" id="radioSelPrevFolder"><font color="#ff6600">For FASTER Processing Select a Previous Folder of Saved Resumes</font><br/>
                 <input type=radio name=prev onclick="addPrevFolder()" id="radioAddPrevFolder"><font color="gray">Add New Resumes to Previous Folder of Saved Resumes</font><br/>
                 <input type=radio name=prev onclick="newFolder()" id="radioNewFolder"><font color="gray">Upload Resumes to a New Folder</font></h2><br/>
                <select class="jobtype" style="float:none;display:none"  name='PrevFolder' onchange="copyFolderName();clearFileText();" id="filedropdown">
				<option value ='' >-- Select A Folder -- </option>
<%
		int k = 1;
		for(int i = files.length-1;i >=0; i--){
			if (files[i].isDirectory())
			{
				List<File> resumeFiles = ProcessResumes.getResumeFiles(files[i]);
				out.write("<option>"+files[i].getName()+"</option>\n");
				out.write("<script> folderFiles[" + k + "] = " + resumeFiles.size() + ";</script>\n");
				k++;
			}
		}
%>
		</select>
<%	}  else {%>
		 <h2><input type=radio name=prev onclick="newFolder()" id="radioNewFolder" checked><font color="gray">Upload Resumes to a new Folder</font></h2>
<%	} %>
	<span id="newFolder" style="display:none"><br/><br/>Folder Name To Save Uploaded Resumes : <input type=text name="ResumeFolder" id="ResumeFolder" size=30 value="" onchange="checkNewFolder()"><br/></span>
	</div>
	 <div style="width:120px;float:right;margin-top:10px">
		<a href="http://youtu.be/Dg2AmkUpsig" rel="prettyPhoto" title=""><img src="images/demo_icon.png" width=120px  /></a>
	<br><br>
	</div>
	<br><br>
	<div style="margin-top:100px;">
<%	if (posting.getEmailAddress() != null && posting.getEmailAddress().length() > 0) { %>
	<b>Note:</b> Resumes emailed to <u><%=posting.getEmailAddress().toLowerCase()%>@resumesort.com</u> will be downloaded to the selected Folder
<%	}  else { %>
	<br>
	<span>
	<input type="checkbox" name="Mailbox" id="Mailbox" onclick="createMailbox(this)"> Create Mailbox
		 <span id="icon"><a title="Create a Mailbox for emailing resumes" href="#">
			<img src="images/help.png" width="16"  height="16" />
		 </a></span><span id="MailboxName" style="white-space:nowrap;"></span>
	</span>
<%	} %>
	<br/><br/>
		  <a href="javascript:gotoStep3()" class="orangeBtn">Select Phrases</a>&nbsp;&nbsp;&nbsp;<a href="javascript:gotoStep3(true)" class="orangeBtn">Ask Friends</a><br/>
		</div>
	  <div id=zipfileupload style="display:none">
	  Or Upload a single Zip File
	  <p><input type="file" name="targetfile" size="30" value="" id="selectResume"/></p>
	  <br />
	  </div>
<% if (html5upload == null || !html5upload) { %>
</form>
<center>
<script language="javascript" type="text/javascript"src="common.js" />
<div id="dragdrop2" style="display:none">
<script language="javascript" type="text/javascript">

	documentWriteEmptyApplet();
</script>
<table width="640" cellpadding="3" cellspacing="0" border="0">
    <tr>
        <td colspan="2" align="center">
<!-- --------------------------------------------------------------------------------------------------- -->
<!-- --------     A DUMMY APPLET, THAT ALLOWS THE NAVIGATOR TO CHECK THAT JAVA IS INSTALLED   ---------- -->
<!-- --------               If no Java: Java installation is prompted to the user.            ---------- -->
<!-- --------                                                                                 ---------- -->
<!-- --------               THIS IS NOT THE JUpload APPLET TAG !   See below                  ---------- -->
<!-- --------------------------------------------------------------------------------------------------- -->
<!--"CONVERTED_APPLET"-->
<!-- HTML CONVERTER -->
<script language="JavaScript" type="text/javascript"><!--
    var _info = navigator.userAgent;
    var _ns = false;
    var _ns6 = false;
    var _ie = (_info.indexOf("MSIE") > 0 && _info.indexOf("Win") > 0 && _info.indexOf("Windows 3.1") < 0);
//--></script>
    <comment>
        <script language="JavaScript" type="text/javascript"><!--
        var _ns = (navigator.appName.indexOf("Netscape") >= 0 && ((_info.indexOf("Win") > 0 && _info.indexOf("Win16") < 0 && java.lang.System.getProperty("os.version").indexOf("3.5") < 0) || (_info.indexOf("Sun") > 0) || (_info.indexOf("Linux") > 0) || (_info.indexOf("AIX") > 0) || (_info.indexOf("OS/2") > 0) || (_info.indexOf("IRIX") > 0)));
        var _ns6 = ((_ns == true) && (_info.indexOf("Mozilla/5") >= 0));
//--></script>
    </comment>

<script language="JavaScript" type="text/javascript"><!--
    if (_ie == true) document.writeln('<object classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" WIDTH = "0" HEIGHT = "0" NAME = "JUploadApplet"  codebase="http://java.sun.com/update/1.5.0/jinstall-1_5-windows-i586.cab#Version=5,0,0,3"><noembed><xmp>');
    else if (_ns == true && _ns6 == false) document.writeln('<embed ' +
	    'type="application/x-java-applet;version=1.5" \
            CODE = "wjhk.jupload2.EmptyApplet" \
            ARCHIVE = "jupload-5.0.7.jar" \
            NAME = "JUploadApplet" \
            WIDTH = "0" \
            HEIGHT = "0" \
            type ="application/x-java-applet;version=1.6" \
            scriptable ="false" ' +
	    'scriptable=false ' +
	    'pluginspage="http://java.sun.com/products/plugin/index.html#download"><noembed><xmp>');
//--></script>
<applet  CODE = "wjhk.jupload2.EmptyApplet" ARCHIVE = "jupload-5.0.7.jar" WIDTH = "0" HEIGHT = "0" NAME = "JUploadApplet"></xmp>
    <PARAM NAME = CODE VALUE = "wjhk.jupload2.EmptyApplet" >
    <PARAM NAME = ARCHIVE VALUE = "jupload-5.0.7.jar" >
    <PARAM NAME = NAME VALUE = "JUploadApplet" >
    <param name="type" value="application/x-java-applet;version=1.5">
    <param name="scriptable" value="false">
    <PARAM NAME = "type" VALUE="application/x-java-applet;version=1.6">
    <PARAM NAME = "scriptable" VALUE="false">

</xmp>
    
Java 1.5 or higher plugin required.
</applet>
</noembed>
</embed>
</object>

<!--
<APPLET CODE = "wjhk.jupload2.EmptyApplet" ARCHIVE = "jupload-5.0.7.jar" WIDTH = "0" HEIGHT = "0" NAME = "JUploadApplet">
	<PARAM NAME = "type" VALUE="application/x-java-applet;version=1.6">
	<PARAM NAME = "scriptable" VALUE="false">
	</xmp>
Java 1.5 or higher plugin required.
</APPLET>
-->
<!-- "END_CONVERTED_APPLET" -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- --------------------------     END OF THE DUMMY APPLET TAG            ------------------------------ -->
<!-- ---------------------------------------------------------------------------------------------------- -->


<!---------------------------------------------------------------------------------------------------------
-------------------     A SIMPLE AND STANDARD APPLET TAG, to call the JUpload applet  --------------------- 
----------------------------------------------------------------------------------------------------------->
        <applet
	            code="wjhk.jupload2.JUploadApplet"
	            name="JUpload"
	            archive="jupload-5.0.7.jar"
	            width="640"
	            height="300"
	            mayscript="true"
	            alt="The java plugin must be installed.">
            <!-- param name="CODE"    value="wjhk.jupload2.JUploadApplet" / -->
            <!-- param name="ARCHIVE" value="jupload-5.0.7.jar" / -->
            <!-- param name="type"    value="application/x-java-applet;version=1.5" /  -->
            <param name="postURL" value="<%=request.getContextPath()%>/MultiUpload" />
            <!-- Optionnal, see code comments -->
            <param name="showLogWindow" value="false" />
            <param name="allowedFileExtensions" value="doc/docx/rtf/txt/odt/pdf/zip" />
			<param name="showStatusBar" value="false" />
			<param name="formdata" value="uploadform" />
			<param name="uploadPolicy" value="TextnomicsUploadPolicy" />
			<!--param name="afterUploadURL" value="ProcessResumes" /-->

            Java 1.5 or higher plugin required. 
        </applet>
<!-- --------------------------------------------------------------------------------------------------------
----------------------------------     END OF THE APPLET TAG    ---------------------------------------------
---------------------------------------------------------------------------------------------------------- -->
        </td>
    </tr>
</table>
</center>
</div>
<% } else { %>
<div id="dragdrop" style="display:none">
	<input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="500000" /> 
	<input type="hidden" id="MAX_ZIP_SIZE" name="MAX_ZIP_SIZE" value="20000000" /> 
    <input type="file" id="fileselect" name="fileselect[]" multiple="multiple" style="display:none;"/>  
		<div class="filedrag" id="filedrag">
				<span id="dragtext">or<br/>Drag/Drop resumes here</span><br/>
				or<br/>
                <a href="#" onClick="document.getElementById('fileselect').click()" class="orangeBtn">Select Files</a>
                </div>
<div id="submitbutton">
	<button type="submit">Upload Files</button>
</div>
<div id="progress"></div>

<div id="messages">
</div>
</div>
</form>
<script src="fileupload.js"></script>
<% } 
	if(files == null || files.length == 0) {
%>
	<script>newFolder();
	</script>
<% } %>
			</div>
      		 </div>
  			                      

  		</div>
<!-- End SmartWizard Content -->  		
 		
</td></tr>
</table> 
<div class="clear"></div>
</div>
</div>
    </div>
<%	if (previousFolder != null) { %>
<script>
	document.getElementById("radioAddPrevFolder").checked = true;
	document.getElementById("radioAddPrevFolder").disabled = true;
	document.getElementById("radioSelPrevFolder").disabled = true;
	document.getElementById("radioNewFolder").disabled = true;
	addPrevFolder();
	var dropdown = document.getElementById("filedropdown");
	for (var i = 0; i < dropdown.options.length ; i++)
	{
		if (dropdown.options[i].value == '<%=previousFolder%>')
		{
			dropdown.selectedIndex = i;
			dropdown.disabled = true;
			if (folderFiles.length > i)
			{
				numfilesUploaded = folderFiles[i];
				//alert("ind:" + ind + " numfilesUploaded=" + numfilesUploaded);
			}
			break;
		}
	}
	document.getElementById("ResumeFolder").value = '<%=previousFolder%>';
</script>
<%	} else {%>
<script>
    if (_ie == true)
	{
		document.getElementById("radioNewFolder").checked = true;
		newFolder();
	}
</script>
<%	} %>
<%@include file="footer.jsp"%>
</body>
</html>
