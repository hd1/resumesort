<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.mail.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.openoffice.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%

		String index = request.getParameter("resumeIdx");
		String userscore = request.getParameter("userscore");
		System.out.println("Entered save score:" + index);
		if (index == null) return;
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (user == null)
		{
%>
Error: Your session has expired.
<%
			return;
		}
		List<File> resumeFiles = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		List<Integer> userScores = (List<Integer>)session.getAttribute(SESSION_ATTRIBUTE.USER_SCORES.toString());
		JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
		int i = getInt(index);
		String fileName = resumeFiles.get(i).getName();
		if (userScores == null)
		{
			userScores = new ArrayList<Integer>(resumeFiles.size());
			for (int j = 0; j < resumeFiles.size(); j++)
				userScores.add(0);
			session.setAttribute(SESSION_ATTRIBUTE.USER_SCORES.toString(), userScores);
		}
		userScores.set(i, getInt(userscore));
		List<ResumeScore> oldScores = new ResumeScore().getObjects("RESUME_FILE = " + UserDBAccess.toSQL(fileName)
					+ " and JOB_ID =" + posting.getId() + " and user_name =" + UserDBAccess.toSQL(user.getLogin()));
		if (oldScores.size() > 0)
		{
			ResumeScore rs = oldScores.get(0);
			rs.setUserScore(getInt(userscore));
			rs.update();
		}
%>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>