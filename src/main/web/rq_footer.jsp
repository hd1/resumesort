  <footer id="footer">
    <div class="central">
      <span>Copyright &copy; 2011 Textnomics<sup><small>TM</small></sup> Inc</span>
	  <a href="Aboutus.jsp" target=_blank>About</a>
	  <a href="Contactus.jsp" target=_blank>Contact</a>
  	  <a href="Textnomics_Terms_of_Service.pdf" target=_blank>Terms &amp; Policy</a>
	  <a href="Blogs.jsp" target=_blank>Blog</a>
	  <a href="FAQ.jsp" target=_blank>FAQ</a>
	</div>
  </footer>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23001092-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
