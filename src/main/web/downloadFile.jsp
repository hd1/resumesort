<%@ page import="java.util.*"%><%@ page import="java.util.zip.*"%><%@ page import="java.io.*"%><%@ page import="java.net.*"%><%@ page import="com.textonomics.*"%><%@ page import="com.textonomics.user.*"%><%@ page import="com.textonomics.web.*"%><%@ page import="com.textonomics.db.*"%><%@ page import="com.textonomics.wordnet.model.*"%><%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());	
	if (user == null)
	{
%>
		<script>alert("Your session has expired");</script>
<%
		return;
	}
	if (!user.isAdmin() && !user.isTester())
	{
%>
		<script>alert("You have no permissions to download");</script>
<%
		return;
	}
	try
	{
		
	String fileName = request.getParameter("fileName");
	String download = request.getParameter("download");
	if (fileName == null)
		return;
	File file = new File(fileName);
	if (!fileName.startsWith("/") && !fileName.startsWith("\\"))
		 file = PlatformProperties.getInstance().getResourceFile(fileName);
	InputStream is = new BufferedInputStream(new FileInputStream(file)); 
	String mimeType = URLConnection.guessContentTypeFromStream(is);
	response.setContentType (mimeType) ;
	if (download == null || download.equalsIgnoreCase("true"))
		response.setHeader("Content-disposition", "attachment;filename=" + fileName) ;
	else
		response.setHeader("Content-disposition", "filename=" + fileName) ;
	response.setHeader("pragma", "no-cache");
	OutputStream outStream = null;
	String encoding = request.getHeader("Accept-Encoding");    
	  
	if (false && encoding != null && encoding.indexOf("gzip") != -1)
	{
	   response.setHeader("Content-Encoding" , "gzip");
	   outStream = new GZIPOutputStream(response.getOutputStream());
	}
	else if (false && encoding != null && encoding.indexOf("compress") != -1)
	{
	   response.setHeader("Content-Encoding" , "compress");
	   outStream = new ZipOutputStream(response.getOutputStream());
	} 
	else
	{
	   outStream = response.getOutputStream();

	}
	int chunk = 1024 ;

	byte[] buffer = new byte[chunk] ;
	int length = -1 ;
	int i = 0 ;

	while ((length = is.read(buffer)) != -1)
	{
		outStream.write(buffer, 0, length) ;
		if (length < chunk)
			break;
	}
	is.close();
	is = null;
	}
	catch (Exception x)
	{
		x.printStackTrace();
	}%>