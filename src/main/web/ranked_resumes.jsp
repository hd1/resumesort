<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.subscription.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<!--script language="JavaScript" type="text/JavaScript" src="prototype.js"></script-->
<%
	try
	{
	if (user == null)
	{
		response.sendRedirect("index.jsp");
		return;
	}
	JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	if (posting == null)
	{
		response.sendRedirect("home.jsp");
		return;
	}
	String postingId = posting.getPostingId();
	if (!posting.isPaid())
	{
		posting = (JobPosting) new JobPosting().getObject("id=" + posting.getId());
		if (posting.isPaid())
			session.setAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString(), posting);
	}
	boolean showPosting = true;
	if ("false".equals(request.getParameter("showPosting"))) showPosting = false;
	List<File> resumeFiles = (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
	List<Double> scores = (List<Double>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
	List<Integer> userScores = (List<Integer>)session.getAttribute(SESSION_ATTRIBUTE.USER_SCORES.toString());
	List<String> addresses = (List<String>)session.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_ADDRESSES.toString());
	List<Double> distances = (List<Double>)session.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_DISTANCES.toString());
	List<Boolean> blockedResumes = new ArrayList<Boolean>();
	for (int i = 0; i < resumeFiles.size(); i++)
	{
		blockedResumes.add(false);
	}
	session.setAttribute(SESSION_ATTRIBUTE.BLOCKED_RESUMES.toString(), blockedResumes);
	if (resumeFiles == null)
	{
		System.out.println("No results found");
		return;
	}
	String error = request.getParameter("error");
	if (error == null || error.trim().length() == 0)
		error = (String) session.getAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());
	if (false && error != null && error.length() > 0)
	{
%> 
	<script>alert("<%=error.replace("\n","\\n")%>");</script> 
	<!--br><font color=red><b><%=error.replace("\n","<br>")%></b></font><br-->
<%
	}
	boolean paidSubscriber = true;
	//boolean paidSubscriber = SubscriptionService.isValidSubscription(user);
	if (false && !paidSubscriber)
	{
%>
	<script>window.location="rt_request_payment.jsp?type=0";</script>
<%	
		return;
	}
	//temporary
	 paidSubscriber = true;
	String defValue = "Enter Email Address";
	List<String> sortedFiles = new ArrayList<String>();
	for (File resumeFile: resumeFiles)
		sortedFiles.add(resumeFile.getName());
	List<Double> sortedScores = new ArrayList<Double>();
	if (scores != null) sortedScores.addAll(scores);
	List<Integer> indexes = new ArrayList<Integer>();
	for (int i = 0; i < sortedFiles.size(); i++)
		indexes.add(i);
	for (int i = 0; i < sortedScores.size(); i++)
	{
		for (int j = i; j < sortedScores.size(); j++)
		{
			Double scorej = sortedScores.get(j);
			if (scorej == null) scorej = 0.0;
			Double scorei = sortedScores.get(i);
			if (scorei == null) scorei = 0.0;
			if (scorej > scorei)
			{
				Double score = sortedScores.get(i);
				String resume = sortedFiles.get(i);
				Integer index = indexes.get(i);

				sortedScores.set(i, sortedScores.get(j));
				sortedFiles.set(i, sortedFiles.get(j));
				indexes.set(i, indexes.get(j));

				sortedScores.set(j, score);
				sortedFiles.set(j, resume);
				indexes.set(j, index);
			}
		}
	}
	System.out.println("SortedFiles:" + sortedFiles.size() + " sortedScores:" + sortedScores.size());
	List<Map<Phrase,Integer>> matchedPhraseList = (List<Map<Phrase,Integer>>) session.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
	Set<String> tags = (Set<String>) session.getAttribute(SESSION_ATTRIBUTE.TAGS.toString());
	if (tags == null) tags = new HashSet<String>();
	DecimalFormat decformat = new DecimalFormat("########0.00");
	DecimalFormat decformat2 = new DecimalFormat("########0");
	boolean procError = false;
	int rank = 0;
	int numResumes = sortedFiles.size();
	int numToShow = numResumes;
	if (!paidSubscriber)
	{
		numToShow = (numToShow*2)/5;
		if (numToShow == 0) numToShow = 1;
		if (numToShow > 10) numToShow = 10;
	}
	int index = -1;
	int skip = numResumes/5;
	if (skip <= 1) skip = 2;
	List<String> unRatedResumes = new ArrayList<String>();
	boolean showDistance = false;
	if (posting.getLocation() != null && posting.getLocation().length() > 0 && distances != null)
		showDistance = false;
%>
<script>
	function showResume(ind, error)
	{
		if (error)
		{
			alert("There was an error processing this resume");
			return;
		}
		var link = "ResumeViewer?viewFileIndex=" + ind;
		var newwin = window.open(link, "HighLightedResume" + ind);
		newwin.focus();
	}
	var busy = false;
	function sendResumes()
	{
		if (busy)
		{
			alert("Function in progress, please wait...")
			return;
		}
		var elems = document.resumeListForm.elements;
		var selected = false;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name == "emailResumeIdx" && elems[i].checked)
			{
				selected = true;
			}
		}
		if (!selected)
		{
			alert("Please select some resumes")
			return;
		}
		busy = true;
		var button = document.getElementById("emailResumes");
		button.innerHTML = "Your email is being sent";
		var url = "email_resumes.jsp?random=" + new Date();
		var form = document.resumeListForm;
		//alert("" + $(form).serialize());
		$.ajax({         
			url: url,         
			type: 'post',         
			data : $(form).serialize(),
			async: true,         
			cache: false,         
			timeout: 30000,         
			error: function(){return true;},
			success: function(response){
				//alert(response);
				if (response != null)
					alert("Email has been sent");
				else
					alert("Error sending email");
				busy = false;
				button.innerHTML = "Email Selected Resumes";
			}
		});
	}
	function selectAll(obj)
	{
		var selcbx = document.getElementById("selall");
		var elems = document.resumeListForm.elements;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name == "emailResumeIdx")
			{
				if (selcbx.checked)
				{
					elems[i].checked = true;
				}
				else
				{
					elems[i].checked = false;
				}
			}
		}
	}
	function saveScore(index)
	{
		var elems = document.resumeListForm.elements;
		var form = document.resumeListForm;
		var rating = document.getElementById("rating_" + index);
		if (rating == null)
		{
			alert("Rating not found");
			return;
		}
		//alert("index=" + index + " rating=" + rating.value);
		var url = "save_userscore.jsp?random=" + new Date() + "&userscore=" + rating.value + "&resumeIdx=" + index;
		//alert("" + $(form).serialize());
		$.ajax({         
			url: url,         
			type: 'get',         
			async: true,         
			cache: false,         
			timeout: 30000,         
			error: function(){
				alert("Error Saving scores");
				return false;
				},
			success: function(response){
				if (response == null)
					alert("Error Saving scores");
			}
		});
	}
	function saveScores()
	{
		var elems = document.resumeListForm.elements;
		var form = document.resumeListForm;
		var url = "save_userscores.jsp?random=" + new Date();
		//alert("" + $(form).serialize());
		$.ajax({         
			url: url,         
			type: 'post',         
			data : $(form).serialize(),
			async: true,         
			cache: false,         
			timeout: 30000,         
			error: function(){
				alert("Error Saving scores");
				return false;
				},
			success: function(response){
				if (response == null)
					alert("Error Saving scores");
				else
					alert("Scores have been saved");
			}
		});
	}
	function clearDefault(inp)
	{
		if (inp.value == '<%=defValue%>')
		{
			inp.value = '';
		}
	}
	var sentEmails = new Array();
	function sharePosting()
	{
		var elems = document.resumeListForm.elements;
		var entered = false;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			var value = elems[i].value;
			if (name == "shareemail" && value != '<%=defValue%>' && value != '')
			{
				entered = true;
				if (value == '<%=user.getEmail()%>')
				{
					alert("You can not assign this job posting to yourself");
					elems[i].focus();
					return;
				}
				if (value.indexOf("@") == -1 || value.indexOf(".") == -1)
				{
					alert("Please enter a valid email address");
					elems[i].focus();
					return;
				}
				for (var j = 0; j < sentEmails.length; j++)
				{
					if (value == sentEmails[j])
					{
						alert(value + " has already been notified");
						elems[i].focus();
						return;
					}
				}
			}
		}
		if (!entered)
		{
			alert("Please enter email addresses")
			return;
		}
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			var value = elems[i].value;
			if (name == "shareemail" && value != '<%=defValue%>' && value != '')
			{
				sentEmails.push(value);
			}
		}
		var button = document.getElementById("sendEmail");
		button.innerHTML = "Your email is being sent";
		var url = "share_job_posting.jsp?random=" + new Date();
		var form = document.resumeListForm;
		var form = document.resumeListForm;
		//alert("" + $(form).serialize());
		$.ajax({         
			url: url,         
			type: 'post',         
			data : $(form).serialize(),
			async: true,         
			cache: false,         
			timeout: 30000,         
			error: function(){return true;},
			success: function(response){
				//alert(response);
				if (response != null)
				{
					if (response.indexOf("Error") != -1)
					{
						alert("Error sending emails:" + response);
					}
					else
						alert("Emails have been sent");
				}
				else
					alert("Error sending emails");
				button.innerHTML = "Send more emails";
			}
		});
		//new Ajax.Request(url, { 
		//	method:'post',
		//	parameters : Form.serialize(form),
		//	onSuccess: function(transport){
				
		//		var response = transport.responseText;
				// ignore response
		//		if (response != null)
		//		{
		//			if (response.indexOf("Error") != -1)
		//			{
		//				alert("Error sending emails:" + response);
		//			}
		//			else
		//				alert("Emails have been sent");
		//		}
		//		else
		//			alert("Error sending emails");
		//		button.value = "Send more emails";
		//	}
		//});
	}
	
	function gotoUploadResumes()
	{
		if (confirm("Are you sure you want to leave this page and upload more resumes?"))
		{
			window.location = "upload_resumes.jsp?resumeFolder=<%=posting.getResumeFolder()%>";
		}
	}
	
	function gotoUploadJobPosting()
	{
		if (confirm("Are you sure you want to leave this page and start over?"))
		{
			window.location = "upload_jobposting.jsp";
		}
	}
	
	function showJobPosting()
	{
		window.open("highlighted_jobposting.jsp?random=" + new Date(), "Job Posting", "toolbar=no,status=yes,locationbar=no,scrollbars=yes,resizable=yes,top=25,left=100,width=625,height=600")		
	}
	
	function goHome()
	{
		if (confirm("Are you sure you want to leave this page and start over?"))
		{
			window.location = "home.jsp";
		}
	}
</script>
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
	<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    
    <script type="text/javascript">
        $(function() {
	        $("a[rel^='prettyPhoto']").prettyPhoto();

        });
    </script>
<link type="text/css" href="../css/ui/jquery-blue.css" rel="stylesheet" />	
<link href="css/smart_wizard.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="css/phrases.css" />
<script src="phrases.js" type="text/javascript"></script>
<link rel="stylesheet" href="ifiles/stylesheet.css" />
<style>
.blurry {
	color: transparent;
	text-shadow: 0 0 2px #FFF;
}
</style>
<script type="text/javascript" src="ifiles/javascript.js"></script>
<script type="text/javascript" src="ratingsys.js"></script> 
<div class="middle">
<div class="maintext_dashboard">
 
 <div class="sky_bar">
    <div style="float:right; clear:both;">
    <a href="javascript:gotoUploadJobPosting()">START NEW JOBS</a>
    <a href="javascript:goHome()">PREVIOUS JOBS</a>
 </div>
 </div>
 
 <div class="text_dashboard">
<table align="center" border="0" cellpadding="0" cellspacing="0">
<tr><td> 
<!-- Smart Wizard -->
  		<div id="wizard" class="swMain">
  			<ul class="anchor">
  				<li><a href="javascript:gotoUploadJobPosting()">
                <label class="stepNumber">1</label>
                <span class="stepDesc">
                   Upload Job Posting<br />
                   <small>Upload Job Posting</small>
                </span>
            </a></li>
  				<li><a href="javascript:gotoUploadResumes()">
                <label class="stepNumber">2</label>
                <span class="stepDesc">
                   Upload Resumes <br/> 
                   <small>Upload to Resume Folder</small>              
                </span>
            </a></li>
  				<li><a class="mini" href="select_phrases.jsp" >
                <label class="stepNumber">3</label>
                <span class="stepDesc">
                   Select Phrases
                </span>                   
             </a></li>
  				<li><a class="mini selected" href="#step-4">                
				 <label class="stepNumber">4</label>
				   <span class="stepDesc">
<% if (scores != null) { %>
				   Rank Resumes
<% } else { %>
				   Assign
<% }%>
					</span>                   
            </a></li>
  			</ul>
  			<div class="stepContainer">	
<%
	if (error != null && error.length() > 0)
	{
		if (error.startsWith("\n")) error = error.substring(1);
%> <span class="error"><%=error.replace("\n","<br>")%></span><%
	}
%>
            <div class="content">            	
			<div style="margin-left:30px; margin-top:30px; margin-bottom:30px;">
				<center><h2>Resumes for Job Posting: <% if (showPosting) { %><u><a href="javascript:showJobPosting()"><% } %><%=postingId%></a></u></h2></center>
				<form name=resumeListForm>
				<div style="display:none" id="paymsg">
				<br>
				<table width=85% align=center>
				<tr>
				<td>
				<b>Since you are not a paid subscriber, some of the top resumes may have been blocked out. Please click <a href="request_payment.jsp?type=1&postingId=<%=posting.getId()%>" target=_blank><b><u>here</u></b></a> to purchase a subscription in order to view all processed resumes or to assign them to someone else.</b>
				</td>
				</tr>
				</table>
				<br>
				<br>
				</div>
				<div class="table_01">
                	<div class="resumeRow">
                        <div class="resumeCheck">&nbsp;</div>
                        <div class="resumeNameTitle">Resume File</div>
<%		if (showDistance) { %><div class="resumeDistanceTitle">Distance (approx miles)</div><% } %>
                        <div class="resumeScoreTitle">System Score</div>
                        <div class="resumeScoreTitle">Your Score</div>
                    </div>
<%
	int hidetop = 3;
	int hiderand = 5;
	boolean hidden = false;
	if (sortedFiles.size() < 11)
	{
		hidetop = 2;
		hiderand = 3;
	}
	if (user.isAdmin() || posting.isPaid())
	{
		hidetop = 0;
		hiderand = 0;
	}
	for (String resume : sortedFiles)
	{
		index++;
		rank++;
		if (!paidSubscriber && (numToShow <= 0 || index < (numResumes*2)/5 || rank%skip != 0))
		{
			unRatedResumes.add(resume);
			continue;
		}
		int slash = resume.lastIndexOf("\\");
		if (slash != -1) resume = resume.substring(slash+1);
		slash = resume.lastIndexOf("/");
		if (slash != -1) resume = resume.substring(slash+1);
		String scoreStr = "";
		if (sortedScores.size() > index)
		{
			if(sortedScores.get(index) == null) continue;
			scoreStr = "&nbsp;&nbsp;(Score:" + decformat.format(sortedScores.get(index)) +")";
			if (scoreStr.indexOf("999") != -1)
			{
				scoreStr = "Error";
				procError = true;
			}
			//else if (!user.isAdmin())
			//	scoreStr = ""; 
		}
		scoreStr = "" + rank + " / " + sortedFiles.size() + " " + scoreStr;
		if (sortedScores.size() == 0)
			scoreStr = "";
		int ind = indexes.get(index);
		String tagMatch = "";
		if (matchedPhraseList != null && matchedPhraseList.size() > ind && matchedPhraseList.get(ind) != null)
		{	
			Set<Phrase> matchedPhrases = matchedPhraseList.get(ind).keySet();
			for (Phrase phrase: matchedPhrases)
			{
				String pstr = phrase.getBuffer().trim();
				boolean tagged = false;
				for (String tag: tags)
					if (tag.trim().equalsIgnoreCase(pstr)) tagged = true;
				if (tagged && tagMatch.indexOf("," + pstr) == -1)
				{	
					tagMatch = tagMatch + "," + pstr;
				}
			}
			if (tagMatch.length() > 1) tagMatch = "("+ tagMatch.substring(1) + ")";
		}
		String fullname = resume;
		if (resume.length() > 50) resume = resume.substring(0,47) + "...";
		int i = indexes.get(index);
		int yourScore = 0;
		if (userScores != null && userScores.size() > i &&  userScores.get(i) != null) yourScore = userScores.get(i);
		String address = "";
		//if (user.getEmail().indexOf("gude") != -1 && addresses != null && addresses.size() > i && addresses.get(i) != null) 	address = addresses.get(i);
		boolean hide = false;
		Random randomGenerator = new Random();
		if (hidetop > 0)
		{
			ResumeScore savedScore = (ResumeScore) new ResumeScore().getObject("RESUME_FILE = " + UserDBAccess.toSQL(resume)
							+ " and JOB_ID =" + posting.getId() + " and user_name =" + UserDBAccess.toSQL(user.getLogin()));
			//if (savedScore != null && (!posting.isPaid() || !savedScore.isPaid()))
			if (!posting.isPaid())
			{
				hidetop--;
				hide = true;
				hidden = true;
				blockedResumes.set(indexes.get(index), true);
%> <script> document.getElementById("paymsg").style.display = 'inline'; </script> <%
			}
		}
		else if (hiderand > 0)
		{
			int randomInt = randomGenerator.nextInt(3);
			//System.out.println("Random:" + randomInt);
			if (randomInt == 2 && !posting.isPaid())
			{
				hiderand--;
				hide = true;
				blockedResumes.set(indexes.get(index), true);
			}
		}
		String distStr = "N/A";
		if (distances != null)
		{
			Double distance = distances.get(i);
			if (distance != null)
				distStr = decformat2.format(distance.doubleValue()*0.000621371/2);
		}
%>
                	<div class="resumeRow">

<%	if (!hide) {%>
                        <div class="resumeCheck"><input name="emailResumeIdx" type="checkbox" value="<%=indexes.get(index)+1%>" /></div>
                        <div class="resumeName"><a href = "javascript:showResume(<%=i%>, <%=procError%>)" title="<%=fullname%>"><%=resume%></a> <font color=red size=-2><%=tagMatch%></font></div>
<%		if (showDistance) { %><div class="resumeDistance"><%=distStr%></div><% } %>
                       <div class="resumeScore"><%=scoreStr%> <%=address%></div>
<%	} else {%>
                        <div class="resumeCheck">&#x2588;</div>
                        <div class="resumeName">&#x2588;&#x2588;&#x2588;&#x2588;&#x2588;&#x2588;&#x2588;&#x2588;</div>
<%		if (showDistance) { %><div class="resumeDistance"><%=distStr%></div><% } %>
                        <div class="resumeScore"><%=scoreStr%> <%=address%></div>
<%	} %>
	<div class="yourScore">
		<div id="rateMe" style="width:auto;float:left;" onmouseout="starRater(<%=i%>)">
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Reject" id="<%=i%>_1" onclick="starRater(this, 'save');saveScore(<%=i%>)" ></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Not Bad" id="<%=i%>_2" onclick="starRater(this, 'save');saveScore(<%=i%>)" ></a>

		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Pretty Good" id="<%=i%>_3" onclick="starRater(this, 'save');saveScore(<%=i%>)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Outstanding" id="<%=i%>_4" onclick="starRater(this, 'save');saveScore(<%=i%>)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Awesome" id="<%=i%>_5" onclick="starRater(this, 'save');saveScore(<%=i%>)" class=""></a>
		</div>
		&nbsp;<span style="display:none;" class="actionStatus" id="status<%=i%>"></span>
		<span style="float:right;" id="actions<%=i%>">
		<a onclick="clearStarRating(<%=i%>);return false;" href="#" style="display:none;" id="clear_<%=i%>">Clear</a>
		</span>
		</div>
		<input type="hidden" value="0" id="rating_<%=i%>" name="rating_<%=i%>"/>
		<input type="hidden" value="0" id="required_<%=i%>" name="required"/>
					</div>
<%
		if (yourScore > 0)
		{
%>
		<script>
		var idStr = "" + <%=i%> + "_" + <%=yourScore%>;
		var img = document.getElementById(idStr);
		if (img != null)
		{
			starRater(img, 'on')
			starRater(img, 'save');
		}
		</script>
<%		
		}

	}
	Collections.sort(unRatedResumes);
	for (String resume : unRatedResumes)
	{
%>
                    <div class="resumeRow">
                        <div class="resumeCheck">&nbsp;</div>
                        <div class="resumeName"><font color=gray><%=resume%></font></div>
                        <div class="resumeScore"></div>
                    </div>
<%	} %>
				</div>
 	 <div style="width:120px;float:left;margin-top:35px">
		<a href="http://youtu.be/x7lDmTTcP4Y" rel="prettyPhoto" title=""><img src="images/demo_icon.png" width=120px  /></a>
	</div>
                   <div style="clear:both;"></div>
                    <br/>
					<center>
<% if (sortedScores.size() > 0) { %>
					<a class="orangeBtn" id="emailResumes" href="javascript:sendResumes()">Email Selected Resumes</a>
<% } %>
					<a class="orangeBtn" id="saveScores" href="javascript:saveScores()">Save Your Scores</a>
                    </center>
                    <div style="clear:both;"></div>
<br/><br/>
<% //if (paidSubscriber) { %>
<%		if (user.isTester()) { %>
		<a href="download_resume_phrases.jsp"  class="orangeBtn" target=_blank>Joel's Download Resume Phrases</a><br><br>
<%		} %>
<%
	boolean validSubscription = false;
	Integer free = user.getFreePostings();
	if (free == null) free = 0;
	if (posting.isPaid() || free > 0 || SubscriptionService.isValidSubscription(user, posting, resumeFiles.size(), false))
		validSubscription = true;
	if (!validSubscription)
	{
%> <script> document.getElementById("paymsg").style.display = 'inline'; </script> <%
	}
	if (!hidden && validSubscription && user.getLogin().equals(posting.getUserName())) { %>
                <h2>Assign Resume Evaluation</h2>
<div style="float:left">
<table border=1>
<tbody>
<tr><th width="118" rowspan="2" align="center"></th><th colspan="3" align="center">Permissions</th></tr>
<tr><td width="90">Job Posting</td><td width="92">Resumes</td><td width="93">All Details</td></tr>
<%
	for (int i = 0; i < 2; i++)
	{
%>
	<tr>
	<td align=left><input name=shareemail type=text size=20 value="Enter Email Address" onfocus="clearDefault(this)"></td>
	<td><input name=perm1_<%=i%> type=checkbox size=30 value="1" ></td>
	<td><input name=perm2_<%=i%> type=checkbox size=30 value="1" ></td>
	<td><input name=perm3_<%=i%> type=checkbox size=30 value="1" ></td>
	</tr>
<%
	}
%>

<tr><td colspan="100%"><br><a id="sendEmail" class="orangeBtn" href="javascript:sharePosting()">Email Friends</a>&nbsp;<a class="orangeBtn" href="javascript:document.resumeListForm.reset()">Reset</a></td></tr>
</tbody></table>
</div>
	 <div style="width:120px;float:left;margin-top:10px">
		<a href=" http://youtu.be/WTD_l9fhr7U" rel="prettyPhoto" title=""><img src="images/demo_icon.png" width=120px  /></a>
	</div>
<% } %>
                
            </div>                                
			</div>
      		 </div>
  			                      

  		</div>
<!-- End SmartWizard Content -->  		
 		
</td></tr>
</table> 
 <script type="text/javascript">
	$(function() {    // Makes sure the code contained doesn't run until
					  //     all the DOM elements have loaded
	
		$('#colorselector').change(function(){
			$('.colors').hide();
			$('#' + $(this).val()).show();
		});
	
	});
 </script>
<div class="clear"></div>
</div>
</div>
    </div>

</form>
<%@include file="footer.jsp"%>
</body>
</html>
<%
	}
catch (Exception ex)
{
	ex.printStackTrace();
}
%>