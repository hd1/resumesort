<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
	String propertyName = request.getParameter("propertyName");
	String propertyValue = request.getParameter("propertyValue");
	if (propertyName == null)
	{
		System.out.println("Property com.textnomics.tika_sentence_detector value: " + PlatformProperties.getInstance().getUserProperty(user.getLogin(), "com.textnomics.tika_sentence_detector"));
		return;
	}
	
	if (propertyValue == null)
	{
		System.out.println("Property " + propertyName + " value: " + PlatformProperties.getInstance().getUserProperty(user.getLogin(), propertyName));
		return;
	}
	System.out.println("Setting " + propertyName + " to: " + propertyValue);
	PlatformProperties.getInstance().setUserProperty(user.getLogin(), propertyName, propertyValue);
%>