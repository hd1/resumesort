<%@include file="rq_header.jsp"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>

<script type="text/javascript">

	
	function enableSignUp()
	{		
		document.register.signup.disabled=(!document.register.privacy.checked);
	}
	
	function  doSubmit() 
	{		
		if (isValid())
		{
			document.register.submit();
		}		
	}

	function  isValid()
	{				
		if (document.register.passwordconfirm.value!=document.register.password.value)
		{
			alert("Your passwords do not match!");
			return false;
		}
		var name = document.register.name.value;
		name = trim(name);
		if (name =="" || name.length < 4 || name.indexOf(" ") == -1)
		{
			alert("Please enter your full name.");
			return false;
		}

		if (document.register.email.value=="")
		{
			alert("Please enter your email.");
			return false;
		}

		if (!validateEmail(document.register.email.value))
		{
			alert("Please enter a valid email address.");
			return false;
		}

		/*if (document.register.login.value=="")
		{
			alert("Please enter your login.");
			return false;
		}*/

		var password = document.register.password.value;
		password = trim(password);
		if (password=="")
		{
			alert("Please enter your password.");
			return false;
		}
		if (password.length < 5)
		{
			alert("Password should be at least 5 characters long.");
			return false;
		}

		if (document.register.betacode.value=="")
		{
			alert("A participation code is required.");
			document.register.betacode.focus();
			return false;
		}
		
		return true;
	}
	function validateEmail(email) 
	{ 
		 var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs)\b/
		 return email.match(re) 
	}	
	function trim(str) 
	{
		return str.replace(/^\s+|\s+$/g,"");
	}
	function copyToCompany(obj)
	{
		return; //temporary
		var name = obj.value;
		if (name.indexOf(" ") != -1)
		{
			name = name.substring(0, name.indexOf(" "));
		}
		var company = document.register.company;
		if (company.value == '')
		{
			company.value = name;
		}
	}
</script>

<div class="central">
<br>
<br>

<%

	String type = request.getParameter("type");
	if (false && type == null)
	{
		out.println("<font size=+1><b>Sorry, requested page was not found!</b></font>");
		out.println("<br>");
		out.println("<br>");
		return;
	}
	else
	{
		out.println("<font size=+1><b>Please enter your information</b></font>");
		out.println("<br>");
		out.println("<br>");
	}


	String error;
	if ((error = (String) session.getAttribute(SESSION_ATTRIBUTE.ERROR.toString())) != null)
	{
		out.println("<FONT style=\"BACKGROUND-COLOR: RED\">" + error + "</FONT>");
		out.println("<br>");
		out.println("<br>");
		session.removeAttribute(SESSION_ATTRIBUTE.ERROR.toString());
	}
	
%> <br>
<br>

<form method="post" action="Register" name="register">
<table border="0" cellpadding="0" cellspacing="0" width="97%">
	<tr>
		<td><span class="orange">Name:</span></td>
		<td><input type="text" size="75" name="name" value="" onchange="copyToCompany(this)" maxlength="200"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Email:</span></td>
		<td><input type="text" size="75" name="email" value="" maxlength="128"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Company:</span></td>
		<td><input type="text" size="75" name="company" value="" maxlength="128"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Password:</span></td>
		<td><input type="password" size="25" name="password" value=""
			maxlength="20"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Password:</span></td>
		<td><input type="password" size="25" name="passwordconfirm"
			value="" maxlength="20"> <span class="green">
		(Confirm)</span></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Beta Participation Code:</span></td>
		<td><input type="text" size="25" name="betacode" value="" maxlength="150"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><input type="checkbox" size="25" name="privacy" onchange=""
			onclick="enableSignUp()"> I have read and agreed to the <b><a
			href="Textnomics_Terms_of_Service.pdf">Terms of Service.</a></b>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<input type="hidden" size="20" name="type" value='<%= type%>'
	maxlength="5">
<center><INPUT type='button' name="signup" VALUE='Sign Up'
	onclick="doSubmit()" disabled="disabled"></center>
</form>
</div>
<%@include file="rq_footer.jsp"%>
  
</body>
</html>