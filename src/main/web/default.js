/*ADDED BY IGO*/
var xmlhttp;
var spanIdx = null;
var optIdx = null;
var elemToRaise = null;
var elemToShowHelp = null;
var xToHelp = null;
var yToHelp = null;
var wantsExtraToSet = null;
/* FINISH ADDED BY IGO */

// ADDED FOR SCROLL MENU
var maxNumChoices;
var numStepChoices;
var previousStepChoices;
var selectedMenu = 0;
var moreSugg;
var maxSuggReached = 0;
var suggReachedArray = new Array();
var highlights = new Array();

// FINISH ADDED FOR SCROLL MENU
var ENABLE_FEATURE_INFO = true;
var ENABLE_FEATURE_MORE = true;

var hiLiteArr = new Array();
var selections = new Array();
var feedback = new Array();
var tagArray = new Array();
var manualEntry = new Array();
var helpList = new Array();
var optionList = new Array();
var extrasIndex = new Array();
var importance = new Array();
var suggestionList = new Array(); // list of non-inflected suggestion phrases from source
var suggestionUsage = null; // count where above phrases have been selected
var suggestionListIds = new Array(); // highlight indexes which have the above suggestions
var suggestionListCount = new Array(); // count of indexes which have the above suggestions
var suggestionListDelCount = null; // count of indexes which above suggestions deleted
var deletedList = new Array(); // comma separated options that have been deleted for each highlight
var turboSelectedList = new Array();
var debug = false;

// var EMPTY_USER_OPTION_TEXT = "&gt;&gt; Enter Text &lt;&lt;";
var EMPTY_USER_OPTION_TEXT = "[ Enter Text ]";
var generic_span_onClick = new Function("return span_onClick(this);");
var isIE = navigator.appName.indexOf("Microsoft Internet Explorer") >= 0;
var isSafari = navigator.vendor != null
		&& navigator.vendor.indexOf("Apple Computer") >= 0;
var isChrome = navigator.appVersion.indexOf("Chrome") >= 0;
//alert(navigator.appVersion);
var raisedWindowIndex = null;
var raisedWindowSpan = null;
var raisedWindowDiv = null;
var raisedWindowExtra = null;

var timeOutId = null;
var TIMEOUT1 = 20; //minutes
var TIMEOUT2 = 5; //minutes
document
		.write("<form method='post' name='getDocumentForm' action='DocumentCreator'><input type='hidden' id='submittedSelections' name='submittedSelections' value='"
				+ selections + "'>");
document
		.write("<input type=\"hidden\" name=\"AllFeedback\" id=\"AllFeedback\" value=\"\"/><input type=\"hidden\" name=\"FeedbackIndices\" id=\"FeedbackIndices\" value=\"\"/>"
				+ "<input type=\"hidden\" name=\"TagArray\" id=\"TagArray\" value=\"\"/>"
				+ "<input type=\"hidden\" name=\"maxSuggestions\" id=\"maxSuggestions\" value=\"\"/>"
				+ "<input type=\"hidden\" name=\"command\" value=\"sendResume\" id=\"command\"/>"
				+ "<input type=\"hidden\" name=\"timeout\" value=\"\" id=\"timeout\"/>"
				+ "</form>");
function init() {
	raisedWindowIndex = null;
	raisedWindowSpan = null;
	raisedWindowDiv = null;
	raisedWindowExtra = null;

	var table = document.getElementById("tableResume"); //The table holding the resume text
	var width = table.offsetWidth;
	table.style.minWidth = width + "px";
	//alert(width);
	if (width > 700)
	{
		var maindiv = document.getElementById("maindiv");
		maindiv.style.width = (width+316) + "px";
		var tunerdiv = document.getElementById("tuner");
		tunerdiv.style.width = (width+40) + "px";
		var resumediv = document.getElementById("theResume");
		resumediv.style.width = width + "px";

	}
	saveComment(false); // refresh comment history

	// Ajax call to update elems number.
	useAjaxFunctions(5, 0); // Obtains the number of phrases highlighted and initializes arrays accordingly
	//setChoiceParameters(); //Obtains again (??) the number of phrases and the step size used in the left aligned scroll menu

	//showMenu(false); //This is a call to the scrollMenu.js
	//getMoreSuggestions(numStepChoices + numStepChoices); //Sets the initial number of suggestions to two times the step

	return;
	// -- alt way;
	//	var errElems = getElementsByTagAndClass("span", "spErr");
	//
	//	for (i = 0; i < errElems.length; i++) {
	//		errElems[i].onClick = generic_span_onClick;
	//	}
}
String.prototype.endsWith = function(str) {     
	var lastIndex = this.lastIndexOf(str);     
	return (lastIndex != -1) && (lastIndex + str.length == this.length); 
}


function span_onClick(obj) {
	return false;
}

function setInnerText(elem, txt) {
	if (isIE) {
		elem.innerText = txt;
	} else {
		elem.innerHTML = txt.replace("\>\>",">><u>").replace("\<\<","</u><<");
	}
}

//Called every time a mouse click occurs inside the HTML body
function handleClick(evt) {
	if (!isIE && (evt == null))
		return;
	var elem = isIE ? window.event.srcElement : evt.target;
	var elemId = elem.id;
	if (elemId == "testlink" || elemId == "savecomment")
	{
		return true;
	}
	var tagName = elem.tagName;
	if ((tagName != null) && (tagName == "SPAN") && (elemId != null)
			&& (elemId.indexOf("spErr") == 0) && (elem.className != "")){
		//On a highlighted phrase in the HTML body 
		var index = elem.id.substring(5);
		elemToRaise = elem;
		spanIdx = index;
		//alert("Click, elem:" + elem  + " idx:" + index);
		raiseSelectionWindow(elem, index, false);//Show the dropdown
	} else if (elemId.indexOf("manualEntry") == 0
			|| ((tagName != null) && (tagName == "A"))) {		
		//clicked on user supplied area; do nothing
	} else if ((tagName == "SPAN") && (elemId != null)
			&& (elemId.indexOf("spMenu") == 0)) {
		//Clicked in the scroll menu
		var index = elem.id.substring(6);
		getMoreSuggestions(parseInt(index));//Get the number of suggestions requested
	} else {
		//Clicked somewhere else in the body
		if (elemId == null) elemId = '';
		if (elemId != "feedbackTextArea" && elemId.indexOf('delete') != 0 && elemId.indexOf('plus') != 0)//Close the feedback pane if opened
		{
			hideLastMenu();
			tthide();
		}
	}

	if (isIE) {
		window.event.returnValue = false;
	} else {
		evt.returnValue = false;
	}
	return false;
}

function raiseSelectionWindow(obj, idx, wantsExtra) {
	//Opens the dropdown menu on top of the 
	// span (spErr) that was selected
	if (obj == null)
		alert(" raiseSelectionWindow( obj is NULL!!!");
	hideLastMenu();
	// deselect turbo highlight
	if (!turboSelectedList[idx])
		selectHighLight(null);

	var menuId = "dropMenu" + idx;
	if (wantsExtra)
		menuId += "e";

	// added by igo
	wantsExtraToSet = wantsExtra;

	if (!wantsExtra)
	{
		//makes an AJAX call to the server to get the data for the drop downs
		useAjaxFunctions(1, idx);
	}
	else {
		//Just clicking on the more
		var dropMenu = document.getElementById(menuId);

		if (dropMenu == null)
			alert("dropmenu with id= " + menuId + " does not exist!");

		var x = getLeftPos(obj) + 70;
		var y = getTopPos(obj);
		// dropMenu.style.left = x;
		// dropMenu.style.top = y;

		dropMenu.style.visibility = "visible";
		raisedWindowIndex = idx;
		raisedWindowSpan = obj;
		raisedWindowDiv = dropMenu;
		raisedWindowExtra = wantsExtra;
	}
	return false;
}

function getElementsByTagAndClass(tagName, className) {
	var elems = document.body.getElementsByTagName(tagName);
	var result = new Array();
	for (index = 0; index < elems.length; index++) {
		var elem = elems[index];
		if (elem.className == className) {
			result[result.length] = elem;
		}
	}
	return result;
}

function hidemenu(obj) {
	if (obj != null) {
		obj.style.visibility = "hidden";
		raisedWindowIndex = null;
		raisedWindowSpan = null;
		raisedWindowDiv = null;

		hideFeedback();
	}
}

function hideLastMenu() {
	//alert("hideLastMenu");
	if (raisedWindowIndex != null) {
		//--if the user has entered manual txt, and clicks outside the menu, accept their manual entry
		var userField = document.getElementById("manualEntry"
				+ raisedWindowIndex + (raisedWindowExtra ? "e" : ""));
		
		if (userField != null && userField.value != EMPTY_USER_OPTION_TEXT
				&& userField.value.length > 0) {
			var list = optionList[raisedWindowIndex].split(",");
			var sameAsOption = false;
			for (var i = 0 ; i < list.length ; i++ )
			{
				if (list[i] == userField.value)
				{
					sameAsOption = true;
				}
			}
			if (!sameAsOption)
			{
				handleManualEntry(userField, true);
			}
		}

		hidemenu(raisedWindowDiv);

	}
}

//Sets the text box inside the dropdown
function setUserField(errIdx, txt) {
	var manualEntryElem1 = document.getElementById("manualEntry" + errIdx);

	manualEntryElem1.value = txt;

	var manualEntryElem2 = document
			.getElementById("manualEntry" + errIdx + "e");

	if (manualEntryElem2 != null) {
		manualEntryElem2.value = txt;
	}
}

//Sets the inner HTML of the selected span with the word from the dropdown
function selectWord(obj, optId) {

	optIdx = optId;

	var errIdx = raisedWindowIndex;
	var errSpan = document.getElementById("spErr" + errIdx);
	var deletedRows = deletedList[errIdx];
	deletedRows = deletedRows.split(",");
	for (var i = 0; i < deletedRows.length; i++)
	{
		if (optId != '' && optId != 0 && deletedRows[i] == optId)
		{
			alert("This suggestion has been removed")
			return;
		}
	}
	var list = optionList[errIdx].split(",");
	var oldText = errSpan.innerHTML;
	setInnerText(errSpan, list[optIdx]);//Inner HTML is set
	changeSuggListIcon(list[optIdx], oldText, errIdx);
	//setUserField(errIdx, EMPTY_USER_OPTION_TEXT);//Text box is reset
	setUserField(errIdx, list[optIdx]);//Text box is reset
	manualEntry[raisedWindowIndex] = "";
	var extraIndex = extrasIndex[errIdx];
	errSpan.className = (optIdx == 0) ? "spErr_reselected"
			: optIdx < extraIndex || extraIndex == 0 ? "spErr_corrected"
					: "spErr_corrected_more";

	// Added for Scroll Menu
	hiLiteArr[errIdx] = 3;
	highlights[errIdx] = errSpan.className;

	selections[errIdx] = optIdx;
	hideLastMenu();
	updateCounts();
	startTimer();
}

// Adds phrase to the manual entry
function addWord(obj, optId) {
	optIdx = optId;

	var errIdx = raisedWindowIndex;
	var errSpan = document.getElementById("spErr" + errIdx);
	var image = document.getElementById("plus_" + errIdx + "_" + optId);
	var add = true;
	if (image != null && image.src != undefined && image.src.indexOf('plus.png') == -1)
	{
		add = false;
		image.src = 'plus.png';
		image.title = 'Add to text box';
	}
	else
	{
		image.src = 'minus.png';
		image.title = 'Remove from text box';
	}
	var deletedRows = deletedList[errIdx];
	deletedRows = deletedRows.split(",");
	for (var i = 0; i < deletedRows.length; i++)
	{
		if (optId != '' && optId != 0 && deletedRows[i] == optId)
		{
			alert("This suggestion has been removed")
			return;
		}
	}
	var list = optionList[errIdx].split(",");
	var addText = list[optIdx];
	if (addText.indexOf("and ") == 0)
	{
		addText = addText.substring(4);
	}
	var manualEntry = document.getElementById("manualEntry" + errIdx);
	if (manualEntry != null)
	{
		var oldText = manualEntry.value;
		if (add)
		{
			if (oldText == "")
			{
				manualEntry.value = addText;
			}
			else if (oldText.indexOf(addText) == -1)
			{
				manualEntry.value = oldText + ", " + addText;
			}
		}
		else if (oldText == addText)
		{
			manualEntry.value = "";
		}
		else
		{
			manualEntry.value = oldText.replace(", " + addText,"");
		}
		manualEntry.focus();
	}
	changeSuggListIcon(list[optIdx], "", errIdx);
	startTimer();
}

function changeSuggListIcon(suggestion, oldText, idx)
{
	suggestion = new String(suggestion);
	suggestion = suggestion.toLowerCase();
	var singular  = suggestion;
	var present  = suggestion;
	if (suggestion.endsWith("ies"))
	{
		singular = suggestion.substring(0, suggestion.length-3) + "y";
	}
	else if (suggestion.endsWith("es"))
	{
		singular = suggestion.substring(0, suggestion.length-2);
	}
	else if (suggestion.endsWith("s"))
	{
		singular = suggestion.substring(0, suggestion.length-1);
	}
	if (suggestion.endsWith("d"))
	{
		present = suggestion.substring(0, suggestion.length-1);
	}
	if (suggestionUsage == null)
	{
		suggestionUsage = new Array();
		for (var i = 0;i < suggestionList.length ;i++)
		{
			suggestionUsage[i] = 0;
		}
	}
	for (var i = 0;i < suggestionList.length ;i++)
	{
		if (suggestion == suggestionList[i] || singular == suggestionList[i] || present == suggestionList[i])
		{
			var image = document.getElementById("turboIcon" + i);
			if (image != null)
				image.src = 'tinycheck.png';
			suggestionUsage[i] = suggestionUsage[i] + 1;
		}
	}
	if (oldText == suggestion || oldText == "")
		return;
	oldText = new String(oldText);
	oldText = oldText.toLowerCase();
	singular = oldText;
	present = oldText;
	if (oldText.endsWith("ies"))
	{
		singular = oldText.substring(0, oldText.length-3) + "y";
	}
	else if (oldText.endsWith("es"))
	{
		singular = oldText.substring(0, oldText.length-2);
	}
	else if (oldText.endsWith("s"))
	{
		singular = oldText.substring(0, oldText.length-1);
	}
	if (oldText.endsWith("d"))
	{
		present = oldText.substring(0, oldText.length-1);
	}
	for (var i = 0;i < suggestionList.length ;i++)
	{
		if (oldText == suggestionList[i] || singular == suggestionList[i] || present == suggestionList[i])
		{
			var image = document.getElementById("turboIcon" + i);
			suggestionUsage[i] = suggestionUsage[i] - 1;
			if (suggestionUsage[i] < 0) suggestionUsage[i] = 0;
			if (suggestionUsage[i] == 0 && image != null)
				image.src = 'tinyInfo.gif';
		}
	}
}

// Selects high lights using the turboTuner on the right
var prevClckObj = null;
function selectHighLight(clckObj, idList) {
	if (prevClckObj != null)
	{
		prevClckObj.style.fontWeight = "normal";
		prevClckObj.style.fontStyle = "italic";
		prevClckObj = null;
	}
	for ( var i = 0; i < hiLiteArr.length; i++) {
		var elem = document.getElementById("spErr" + i);
		if (elem != null)
			elem.className = highlights[i];
		turboSelectedList[i] = false;
	}
	//if (clckObj == null)
	//	return;
	pingServer();
	if (clckObj != null)
	{
		if (clckObj.style.textDecoration == 'line-through')
		{
			return;
		}
		clckObj.style.fontWeight = "bold";
		prevClckObj = clckObj;
	}
	var list = idList.split(", ");
	//alert(idList);
	for ( var i = 0; i < list.length; i++) {
		turboSelectedList[parseInt(list[i])] = true;
		elem = document.getElementById("spErr" + parseInt(list[i]));
		//alert(elem.id + ":" + elem.className);
		var prevClass = elem.className;
		if (prevClass.indexOf("Selected") == -1)
		{
			elem.className = prevClass + "Selected";
		}
		//else
		//{
		//	elem.className = "spErr_turbo2";
		//}
	}
	//hideLastMenu();
	var elem = document.getElementById('spErr' + list[0]);
	//if (elem != null && debug)
	if (elem != null)
	{
		elem.scrollIntoView();
		window.scrollBy(0,-150); 
	}
	if (list.length == 1)
	{
		//useAjaxFunctions(1, parseInt(list[0]))
	}
	//startTimer();
}
function dummy()
{
}

function pingServer()
{
	var url = "dummy.html";
	if (xmlhttp == null)
	{
		xmlhttp = GetXmlHttpObject();
	}
	xmlhttp.onreadystatechange = dummy;
	xmlhttp.open("POST", url, false);
	xmlhttp.send(null);
}

var numselected = 0;
function updateCounts()
{
	var nummanual = 0;
	var numignore = 0;
	var numremaining = 0;
	numselected = 0;
	for (var i = 0; i < hiLiteArr.length ; i++)
	{
		if (manualEntry[i] != "")
		{
			nummanual++;
			numselected++;  // ???
		}
		else if (hiLiteArr[i] == 3)
		{
			if (selections[i] == 0)
			{
				numignore++;
			}
			else
			{
				numselected++;
			}
		}
		else
		{
			numremaining++;
		}
		pingServer();
	}
	var tuned = document.getElementById("NUMTUNED");
	if (tuned != null)
	{
		tuned.innerHTML = "" + numselected;
	}
	var ignored = document.getElementById("NUMIGNORED");
	if (ignored != null)
	{
		ignored.innerHTML = "" + numignore;
	}
	var yourtext = document.getElementById("NUMYOURTEXT");
	if (yourtext != null)
	{
		yourtext.innerHTML = "" + nummanual;
	}
	var remaining = document.getElementById("NUMREMAINING");
	if (remaining != null)
	{
		remaining.innerHTML = "" + numremaining;
	}
}
function handleKeyDown(evt) {
	if (!isIE && (evt == null))
		return;
	if (isIE)
		evt = window.event;
	var elem = isIE ? evt.srcElement : evt.target;
	var keyCode = isIE ? event.keyCode : isSafari ? evt.keyCode : evt.which;


	var index = new Number(elem.id.substring(25));

	return handleManualEntry(elem, false, evt);
}

function handleManualEntry(obj, force, evt) {
	if (!isIE && !force && (evt == null))
		return;
	
	var keyCode = force ? 13 : isIE || isSafari ? evt.keyCode : evt.which;
	if (force || keyCode == 13) { //(isSafari && enterKey ) || 
		var text = obj.value;
		text = text.replace("\,", "<comma/>");
		if (text.length != 0) {
			selections[raisedWindowIndex] = "<ME>" + text + "</ME>";// +selections.length + 1; one higherthan the given options
			var errSpan = document.getElementById("spErr" + raisedWindowIndex);
			setInnerText(errSpan, obj.value);
			errSpan.className = "spErr_manualChoice";
			highlights[raisedWindowIndex] = errSpan.className;
			hiLiteArr[raisedWindowIndex] = 3;
			setUserField(raisedWindowIndex, obj.value);
			manualEntry[raisedWindowIndex] = obj.value;
		}
		hidemenu(raisedWindowDiv);
		updateCounts();
		startTimer();
		return false;
	}
}

//Clear the text box when the user puts the cursor inside 
function handleUserOptionClick(obj) {
	if (obj.value == EMPTY_USER_OPTION_TEXT) {
		obj.value = "";
	}
	return true;
}

function getSubmitParameters()
{

	//Create aux sugg
	var auxSugg = new Array();
	for (i = 0; i < selectedMenu; i++) {
		auxSugg[i] = suggReachedArray[i];
	}


	// Request to replace 0 selections (original) with -1, if they were
	// re-selected. This way we know when the user went back to the original selection
	for ( var i = 0; i < selections.length; i++) {
		var errSpan = document.getElementById("spErr" + i);
		if (errSpan == null) continue;
		if (errSpan.className == "spErr_reselected") {
			selections[i] = -1;
		}
		//Replace spErr_dim; change all class names back to the original spErr (so that the rest does not break)  
		else if (errSpan.className == "spErr_dim") {
			errSpan.className == "spErr";
		}
	}

	var formElem = document.getElementById("submittedSelections");
	formElem.value = "" + selections;


	var fbElem = document.getElementById("AllFeedback");
	var fbIdxElem = document.getElementById("FeedbackIndices");
	var allFB = "";
	var fbIdx = "";

	for ( var i = 0; i < feedback.length; i++) {
		var fb = feedback[i];
		// if (fb != null && fb.length > 0) {
		if (fb != null && fb.length > 0) { //indexOf
			if (allFB.length > 0) {
				allFB += "|\n";
				fbIdx += ",";
			}
			allFB += "Feedback[" + i + "]=" + fb;
			fbIdx += i;
		}
	}
	fbElem.value = allFB;
	fbIdxElem.value = fbIdx;
	// TESTING NEW FEATURE, FeedbackIndices: alert( fbIdxElem.value );

	var tagArrayElem = document.getElementById("TagArray");

	tagArrayElem.value = "" + tagArray;

	// Insert array of maxSubmitted
	var maxSuggArray = new Array();
	for (i = 0; i < maxSuggReached; i++) {
		maxSuggArray[i] = suggReachedArray[i];
	}

	var eMaxSugg = document.getElementById("maxSuggestions");
	eMaxSugg.value = "" + maxSuggArray;
}

var busy = false;
function submit(check) {
	if (busy)
	{
		alert("Processing request... Please click OK and wait for the system response.");
	}
	if (numselected == 0)
	{
		alert("You have not done any tuning! Please click on the high lighted phrases and choose from the selections to tune your resume.");
		return;
	}
	if (check && numselected < hiLiteArr.length/5)
	{
		if (!confirm("You have not done much tuning! Are you sure you are done and want the resume emailed to you?"))
			return;
	}
	var command = document.getElementById("command");
	command.value = "SendResult";
	getSubmitParameters();
	busy = true;
	// Before submit clear Scroll Menu to prevent showing in resume html file
	document.getElementById("divStayTopLeft").innerHTML = "";
	document.getDocumentForm.submit();// Submit all arrays back to the server
}

function saveResult() {
	if (busy)
	{
		alert("Processing request... Please click OK and wait for the system response.");
	}
	getSubmitParameters();
	var command = document.getElementById("command");
	command.value = "SaveResult";  // Save for later
	// Before submit clear Scroll Menu to prevent showing in resume html file
	busy = true;
	document.getElementById("divStayTopLeft").innerHTML = "";
	document.getDocumentForm.submit();// Submit all arrays back to the server
}
//-- New Features
var feedBackIndex = null;

//Shows the feedback pane for the current span (using the index)
function showFeedback(index) {

	feedBackIndex = index;
	var fbElem = document.getElementById('feedbackPane');
	var fbTextElem = document.getElementById('feedbackTextArea');
	var fbRegdElem = document.getElementById('feedbackRegarding');
	fbTextElem.value = feedback[index];
	var list = optionList[index].split(",");
	setInnerText(fbRegdElem, list[0]);
	fbElem.style.display = "inline";

	var menusElem = document.getElementById("theResume");
	fbElem.style.top = Math.max(document.body.scrollTop, getTopPos(menusElem));
	fbElem.style.left = Math.max(document.body.scrollLeft,
			getLeftPos(menusElem));

	return false;
}

//Hides the feedback pane
function hideFeedback() {
	var fbElem = document.getElementById('feedbackPane');
	var fbTextElem = document.getElementById('feedbackTextArea');
	if (feedBackIndex != null && fbTextElem.value != "") {
		feedback[feedBackIndex] = fbTextElem.value;
		fbTextElem.value = "";
		var url = "save_feedback.jsp";
		url = url + "?idx=" + feedBackIndex;
		url = url + "&feedback=" + escape(feedback[feedBackIndex]);
		url = url + "&command=save";
		xmlhttp.open("POST", url, false);
		xmlhttp.onreadystatechange = null;
		xmlhttp.send(null);
		var resp = xmlhttp.responseText;

		// UNCOMMENT TO PRESERVE TAGGED/FEEDBACK highlights
		// Give feedback number 4 regarding the highlight feature
		/*
		 * if(feedback[feedBackIndex]!=""){ if(hiLiteArr[feedBackIndex]!=3 &&
		 * hiLiteArr[feedBackIndex]!=4){ hiLiteArr[feedBackIndex]=4; elem =
		 * document.getElementById("spErr" + feedBackIndex);
		 * 
		 * elem.className="spErr"; } }
		 */

		feedBackIndex = null;
	}
	fbElem.style.display = "none";
	return false;
}

//Hides the tooltips -- why are there two of these???
function tthide() {
	var tt = document.getElementById('tt');
	tt.style.display = "none";
	tt = document.getElementById('popup');
	tt.style.display = "none";
	ttObj = null;
	resetSynDelImg();
}

//Shows/Hides the bottom part of the dropdown
function toggleMore(elem, j) {
	if (!ENABLE_FEATURE_MORE) {
		alert("The 'More Options' feature is reserved for subscribers only.");
		return false;
	}

	var saveCurSpan = raisedWindowSpan;
	var state = (isIE) ? elem.innerText : elem.textContent;
	var wantsExtra = (state == "More");
	// alert(wantsExtra);
	// alert( state );

	raiseSelectionWindow(saveCurSpan, j, wantsExtra);
	return false;
}

//Changes the dropdown by (un)flagging it as an error 
function toggleTag(elem, j) {

	tagArray[j] = tagArray[j] == 0 ? 1 : 0;
	var newText = tagArray[j] == 0 ? "Tag as Error" : "Tagged, Thank You";

	// UNCOMMENT TO PRESERVE TAGGED/FEEDBACK highlights
	// Give tag error number 4 regarding the highlight feature
	/*
	 * if(hiLiteArr[j]!=3 && hiLiteArr[j]!=4){ hiLiteArr[j]=4;
	 * 
	 * var elem2 = document.getElementById("spErr" + j);
	 * 
	 * elem2.className="spErr"; }
	 */

	if (isIE) {
		elem.innerText = newText;
	} else {
		elem.textContent = newText;
	}

	return false;
}

//Show the context from where a suggestion came from
function handleTooTip(evt) {
	if (!isIE && (evt == null))
		return;
	var elem = isIE ? window.event.srcElement : evt.target;
	var event = isIE ? window.event : evt;
	var x = document.body.scrollLeft + (isIE ? event.x : event.clientX);
	var y = document.body.scrollTop + (isIE ? event.y : event.clientY);
	if (isIE) {
		x = event.clientX + document.body.scrollLeft;
		y = event.clientY + document.body.scrollTop;
	}
	var eventx = x;
	var eventy = y;
	var j = raisedWindowIndex;
	var k = new Number(elem.id.substring(4));

	var txt = "The 'Additional Information' feature\nis reserved for subscribers only.";
	if (ENABLE_FEATURE_INFO) {
		var helpItems = helpList[j].split("|");
		txt = helpItems[k];
	}
	if (!isIE)
	{
		x = x - 25;
		y = y - 80;
		if (!isChrome)
			y = y + window.pageYOffset;
	}
	else
	{
		x = x - 20;
		y = y - 75;
		y = y + getScrollXY()[1];
	}

	var debugmsg = "......... eventx=" + eventx + " eventy=" + eventy + " tt x=" + x + " y=" + y + " top:"  + window.pageYOffset + " left:" + window.pageYOffset;
	if (isIE)
	{
		debugmsg = "......... eventx=" + eventx + " eventy=" + eventy + " tt x=" + x + " y=" + y + " top:"  + document.body.scrollTop;;
	}
	tipType ="Text from the Job Posting";
	var ttable = document.getElementById("tttable");
	if (ttable != null)
		ttable.style.backgroundColor="#DDFFFF";
	//var debugdiv = document.getElementById('debug1');
	//debugdiv.innerHTML = debugmsg;
	toolTip(elem, x, y, txt);
	return false;
}

var tipType = "";
function handleTooTip2(evt, text) {
	tipType ="Text from the Job Posting";
	var ttable = document.getElementById("tttable");
	if (ttable != null)
		ttable.style.backgroundColor="#DDFFFF";
	handleToolTip(evt, text);
}
function handleTooTip3(evt, text) {
	tipType ="Synonyms for the Phrase";
	var ttable = document.getElementById("tttable");
	if (ttable != null)
		ttable.style.backgroundColor="#FFFFBB";
	handleToolTip(evt, text);
    try {             
		//var tt = document.getElementById('tt');
		//var xpos = tt.style.left;
		//xpos = xpos.replace("px","");
		//xpos = parseInt(xpos);
		//xpos = xpos - 325;
		//xpos = "" + xpos + "px";
		//tt.style.left = xpos;
	}catch(e) {
	}
}
function handleToolTip(evt, text) {
	if (!isIE && (evt == null))
		return;
	var elem = isIE ? window.event.srcElement : evt.target;
	var event = isIE ? window.event : evt;
	var x = document.body.scrollLeft + (isIE ? event.x : event.clientX);
	var y = document.body.scrollTop + (isIE ? event.y : event.clientY);
	if (isIE) {
		x = event.clientX + document.body.scrollLeft;
		y = event.clientY + document.body.scrollTop;
	}
	var eventx = x;
	var eventy = y;
	var j = raisedWindowIndex;
	var k = new Number(elem.id.substring(4));

	var txt = "The 'Additional Information' feature\nis reserved for subscribers only.";
	if (ENABLE_FEATURE_INFO) {
		txt = text.replace(/_LIMIT_/g, "\\n");
	}
	if (!isIE)
	{
		x = x - 25;
		y = y - 80;
		if (!isChrome)
			y = y + window.pageYOffset;
	}
	else
	{
		x = x - 20;
		y = y - 75;
		y = y + getScrollXY()[1];
	}

	var debugmsg = "......... eventx=" + eventx + " eventy=" + eventy + " tt x=" + x + " y=" + y + " top:"  + window.pageYOffset + " left:" + window.pageYOffset;
	if (isIE)
	{
		debugmsg = "......... eventx=" + eventx + " eventy=" + eventy + " tt x=" + x + " y=" + y + " top:"  + document.body.scrollTop;;
	}
	//var debugdiv = document.getElementById('debug1');
	//debugdiv.innerHTML = debugmsg;
	toolTip(elem, x, y, txt);
	return false;
}
function getScrollXY() {
  var scrOfX = 0, scrOfY = 0;
  if( typeof( window.pageYOffset ) == 'number' ) {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
  }
  return [ scrOfX, scrOfY ];
}
var ttObj; // elem tt is about
var generic_tthide = new Function("tthide();");
function tthide() {
	var tt = document.getElementById('tt');
	tt.style.display = "none";
	tt = document.getElementById('popup');
	if (tt != null) tt.style.display = "none";
	ttObj = null;
	resetSynDelImg();
}

function toolTip2(obj, j, k) {
	var helpItems = helpList[j].split(" _LIMIT_ ");
	txt = helpItems[k];
	if (obj == null)
		return; // useful mechanism to disable ttips, just pass 'null' instead
	// of 'this'
}

function toolTip(obj, x, y, txt) {
	var tt = document.getElementById('tt');
	tt.innerHTML = "<table border=0 width='300' id='tttable' cellpaddng=0 cellspacing=0 class='ttipBody'><TBODY id='ttBody'></TBODY></table>";
	var ttBody = document.getElementById('ttBody');
	if (tt == null)
		return false;
	// --clear away any previous content
	while (ttBody.firstChild != null) {
		ttBody.removeChild(ttBody.firstChild);
	}
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	var temp = tipType;
	td.style.fontWeight = "bold";
	td.style.textAlign = "center";
	setInnerText(td, temp);
	tr.appendChild(td);
	ttBody.appendChild(tr);
	//-- parse text into rows
	var rows = txt.split("\\n");
	for ( var r = 0; r < rows.length; r++) {
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		var temp = rows[r];
		if (temp.length > 0)
		{
			if (r == 0)
				temp = "\u2022" + temp;
			else
				temp = "\u2022" + " " + temp;
		}
		//temp = temp.substring(0, temp.length - 2);
		setInnerText(td, temp);
		tr.appendChild(td);
		ttBody.appendChild(tr);
	}

	tt.style.left = x +'px';
	tt.style.top = y + 'px';
	tt.style.display = "inline";
	obj.onmouseout = generic_tthide;
	pingServer();
	//alert(tt.innerHTML);
}

function getManualEntryStr(index) {

	return "<tr><td colspan='2'><input type='text' id='manualEntry"
			+ index
			+ "' class='userSuppliedText' maxlenth='100' value='"
			+ EMPTY_USER_OPTION_TEXT
			+ "' onkeydown='return handleKeyDown(event)' onclick='return handleUserOptionClick(this)'/></td></tr>";

}

function getBottomLinksStr(index, hasExtras, moreOrLessStr) {

	var bottomLinksStr = "<tr height='10px'><td width='100%' colspan='2'><table width='100%'><tr>";
	bottomLinksStr += "<td width='40%' class=feedbackLink><a href='void(0);' onclick='return showFeedback("
			+ index + ");'>Feedback</a></td>";
	bottomLinksStr += "</td><td width='20%' class=tagLink>";

	bottomLinksStr += "<a href='void(0);' onclick='return toggleTag(this,"
			+ index + ");'>" + (tagArray[index] == 0 ? "tag" : "untag")
			+ "</a>";

	bottomLinksStr += "</td><td width='40%' class=moreLink>";
	if (hasExtras) {
		bottomLinksStr += "<a href='void(0);' onclick='return toggleMore(this,"
				+ index + ");'>" + moreOrLessStr + "</a>";
	}
	bottomLinksStr += "</td></tr></table></td></tr>";

	return bottomLinksStr;
}

/*AJAX FUNCTIONS*/

function setChoiceParameters() {

	xmlhttp = GetXmlHttpObject();

	if (xmlhttp == null) {
		alert("Your browser does not support XMLHTTP!");
		return;
	}

	var url = "SuggestionsRetriever?";
	url += "type=1";
	// xmlhttp.onreadystatechange = setChoiceParametersResp;

	// Synchronous call
	xmlhttp.open("POST", url, false);
	xmlhttp.send(null);

	var resp = xmlhttp.responseText;
	var rows = resp.split(",");
	maxNumChoices = parseInt(rows[0]);//Sets the maximum number of phrases
	numStepChoices = parseInt(rows[1]);//Sets the step value used in scroll menu

}

//Returns a list of suggestions up to the number specified
//Highlights the new phrases and dims the ones that were previously shown
function getMoreSuggestions(numSuggestions) {
	xmlhttp = GetXmlHttpObject();

	if (xmlhttp == null) {
		alert("Your browser does not support XMLHTTP!");
		return;
	}

	if (numSuggestions > selectedMenu) {
		moreSugg = 1;
	} else if (numSuggestions < selectedMenu) {
		moreSugg = 0;
	}
	//If it is the same option do nothing
	else
		return;

	var url = "SuggestionsRetriever?";
	url += "numS=" + numSuggestions;
	url += "&type=2";
	url += "&more=" + moreSugg;
	xmlhttp.onreadystatechange = highlightPhrases; //Call back function

	// selectedMenu is in scrollMenu.js
	previousStepChoices = selectedMenu;
	selectedMenu = numSuggestions;

	xmlhttp.open("POST", url, true);
	xmlhttp.send(null);
}

//Function call backed by the AJAX (getMoreSuggestions)
//
function highlightPhrases() {
	if (xmlhttp.readyState == 4) {
		var resp = xmlhttp.responseText;
		// alert(resp);
		var elem;
		var rows = resp.split(", ");
		//alert("hlPhrases:" + rows);
		if (rows[0] != "") {
			//alert("ROWS LENGTH="+rows.length);
			if (moreSugg == 1) {


				for ( var r = 0; r < rows.length; r++) {

					//hiLiteArr
					if (hiLiteArr[parseInt(rows[r])] == 3) {
						//do nothing
					}
					//UNCOMMENT NEXT IF TO LEAVE TAGGED/FEEDBACK HIGHLIGHTED
					// else if ( hiLiteArr [parseInt(rows[r])] == 4){
					// do nothing
					// }
					// Make all equals to 5 to distinguish from old 2
					else {
						hiLiteArr[parseInt(rows[r])] = 5;
					}
					elem = document.getElementById("spErr" + rows[r]);
					if (elem != null)
						elem.className = highlights[parseInt(rows[r])];
					if (selectedMenu > maxSuggReached) {
						suggReachedArray[maxSuggReached + r] = parseInt(rows[r]);
					}
				}

				//dimm the ones equal to 2
				for ( var i = 0; i < maxNumChoices; i++) {
					elem = document.getElementById("spErr" + i);
					if (elem == null) continue;
					// Put old highlighted equal to 1
					if (hiLiteArr[i] == 2) {
						hiLiteArr[i] = 1;
						// put 75% bright
						// elem.style.backgroundColor=lowBright;
						elem.className = "spErr_dim";
					}
					//Put new highlighted equal to 2
					else if (hiLiteArr[i] == 5) {
						hiLiteArr[i] = 2;
						// put 100% bright
						elem.className = "spErr";
						// elem.style.backgroundColor=normalBright;
					} else if (hiLiteArr[i] == 4) {
						elem.className = "spErr";
					}
				}

				//alert(hiLiteArr);

				// ReachedSuggestions
				// MaxReached
				if (selectedMenu > maxSuggReached) {
					maxSuggReached = selectedMenu;
				}
			} else if (moreSugg == 0) {
				for ( var r = 0; r < rows.length; r++) {
					elem = document.getElementById("spErr" + rows[r]);

					// Only turnoff if no action was made with that suggestion
					if (hiLiteArr[parseInt(rows[r])] == 3) {
						//do nothing
					}
					//UNCOMMENT NEXT IF TO LEAVE TAGGED/FEEDBACK HIGHLIGHTED
					// else if(hiLiteArr[parseInt(rows[r])]==4){
					// do nothing
					// }
					else {
						hiLiteArr[parseInt(rows[r])] = 0;
						// put 100% bright
						elem.className = "";
					}

				}

				//light up only the necessary
				for ( var i = 0; i < maxNumChoices; i++) {

					if (hiLiteArr[i] == 3) {
						//do nothing
					}
					//UNCOMMENT NEXT IF TO LEAVE TAGGED/FEEDBACK HIGHLIGHTED
					// else if(hiLiteArr[i]==4){
					// do nothing
					// }
					else if (hiLiteArr[i] != 0) {
						//turn on
						hiLiteArr[i] = 2;
						elem = document.getElementById("spErr" + i);
						elem.className = "spErr";
					}
				}
			}
			getInnerMenu();
		} else {
			getInnerMenu();
			// alert("No more suggestions to offer... Sorry.");
		}
	}
}

function useAjaxFunctions(type, idx) {
	xmlhttp = GetXmlHttpObject();

	if (xmlhttp == null) {
		alert("Your browser does not support XMLHTTP!");
		return;
	}

	//Set url the open
	var url = "GetDropMenu";
	url = url + "?type=" + type;
	url = url + "&idx=" + idx;
	url = url + "&viewFileIndex=" + viewFileIndex;

	// Drop Menu
	if (type == 1) {
		//alert("Drop down:" + type + " id:" + idx);
		xmlhttp.onreadystatechange = stateChangedDropMenu;
		xmlhttp.open("POST", url, true);
		xmlhttp.send(null);
	}
	// Get Options List size
	else if (type == 5) {
		//	xmlhttp.onreadystatechange=stateChangedInitArrays;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);

		var xmlDoc = xmlhttp.responseXML.documentElement;
		var response = xmlDoc.getElementsByTagName("optionsListSize")[0].childNodes[0].nodeValue;
		var fewPopup = document.getElementById("fewSuggestions");

		//alert("Number of phrases:" + response);
		for ( var j = 0; j < response; j++) 
		{
			manualEntry[j] = "";
			feedback[j] = "";
			helpList[j] = "";
			optionList[j] = "";
			extrasIndex[j] = "";
			hiLiteArr[j] = 0;
			selections[j] = 0; // the current selection is the 0'th entry
								// (first word)
			tagArray[j] = 0; // all selections start off untagged
			suggReachedArray[j] = "";
			deletedList[j] = "";
			turboSelectedList[j] = false;
			var elem = document.getElementById("spErr" + j);
			if (elem != null)
			{
				var clas = "spErr";
				if (importance.length > j)
				{
					clas = clas + importance[j]
				}
				highlights[j] = clas;
				elem.className = clas;
			}
		}
		var numremaining = document.getElementById("NUMREMAINING");
		if (numremaining != null)
		{
			numremaining.innerHTML = "" + response;
		}
		startTimer();
	}
}

function startTimer()
{
	return; // No timer for ResumeSort
	var warning = document.getElementById("timeOutWarning");

	if (warning != null)
	{
		warning.innerHTML = "";
		warning.style.visibility = "hidden";
	}
	if (timeOutId != null)
	{
		clearTimeout(timeOutId);
	}
	timeOutId = setTimeout ( timeout_warning, TIMEOUT1*60*1000 );
}

function timeout_warning()
{
	var warning = document.getElementById("timeOutWarning");
	if (warning != null)
	{
		warning.innerHTML = 
			"<br><br>Your session has been idle for " + TIMEOUT1 + " minutes. <br>It will soon timeout in " + TIMEOUT2 + " more minutes.<br>You will then be emailed your resume in its current form<br><br><input type='button' value='OK' onclick='startTimer()'><br><br>&nbsp;"
		warning.style.visibility = "visible";
		warning.style.color = "red";

	}
	timeOutId = setTimeout ( auto_submit, TIMEOUT2*60*1000 );
}

function auto_submit()
{
	var timeout = document.getElementById("timeout");
	timeout.value = "true";
	var warning = document.getElementById("timeOutWarning");
	if (numselected == 0)
	{
		warning.innerHTML = 
			"<br><br>Your session has timed out. No tuning has been done...<br>We will try to close this window...<br>&nbsp;"
		warning.style.visibility = "visible";
		warning.style.color = "red";
		window.close();
		return;
	}
	if (warning != null)
	{
		warning.innerHTML = 
			"<br><br>Your session has timed out.<br>Your resume is being emailed to you...<br><br>&nbsp;"
		warning.style.visibility = "visible";
		warning.style.color = "red";

	}
	submit(false);
}

var addSuggestionNum = 0;
function stateChangedDropMenu() {

	if (xmlhttp.readyState == 4) {
		//get response from servlet
		// var response=xmlhttp.responseText;
		//alert("response=" + response);
		var xmlDoc = null;
		try
		{
			xmlDoc = xmlhttp.responseXML.documentElement;
		}
		catch (e)
		{
		}
		if (xmlDoc == null || xmlDoc.length == 0)
		{
			//alert("These resume tuning results have expired. Please resubmit your resume and job posting");
			return;
		}
		var response = xmlDoc.getElementsByTagName("menu")[0].firstChild.data;
		//alert(response);   // This is the drop down table or ul
		var helpListResp = xmlDoc.getElementsByTagName("helplist")[0].firstChild.data;
		var optionListResp = xmlDoc.getElementsByTagName("optionlist")[0].firstChild.data;
		var deletedListResp = xmlDoc.getElementsByTagName("deletedlist")[0].firstChild.data;
		//alert(deletedListResp);
		var extrasIndexResp = xmlDoc.getElementsByTagName("extrasindex")[0].firstChild.data;
		helpList[spanIdx] = helpListResp;
		optionList[spanIdx] = optionListResp;
		deletedList[spanIdx] = deletedListResp;
		extrasIndex[spanIdx] = extrasIndexResp;
		// put helplist
		var menuId = "dropMenu" + spanIdx;
		if (wantsExtraToSet)
			menuId += "e";

		if (response == null)
			alert("dropmenu with id= " + menuId + " does not exist!");

		var dropMenu = document.getElementById(menuId);
		if (dropMenu == null) {
			//Create new div element
			newDiv = document.createElement("div");
			newDiv.setAttribute("id", menuId);
			newDiv.setAttribute("class", "dropMenu");
			newDiv.innerHTML = response;
			document.getElementById("theResume").appendChild(newDiv);
			dropMenu = document.getElementById(menuId);
			//alert(response);
		}

		var x = getLeftPos(elemToRaise) - 17;
		var y = getTopPos(elemToRaise) - 72;
		//alert("x="+x+" y="+y);
		dropMenu.style.visibility = "visible";
		dropMenu.style.position = "absolute";
		dropMenu.style.left = x + 'px';
		dropMenu.style.top = y + 'px';
		raisedWindowIndex = spanIdx;
		raisedWindowSpan = elemToRaise;
		raisedWindowDiv = dropMenu;
		raisedWindowExtra = wantsExtraToSet;

		var manualEntry = document.getElementById("manualEntry" + spanIdx);
		if (manualEntry != null)
		{
			var oldText = manualEntry.value;
			if (addSuggestionNum != 0)
			{
				var list = optionList[spanIdx].split(",");
				addText = list[addSuggestionNum];
				if (oldText == "")
				{
					manualEntry.value = addText;
				}
				else if (oldText.indexOf(addText) == -1)
				{
					manualEntry.value = oldText + ", " + addText;
				}
				var image = document.getElementById("plus_" + spanIdx + "_" + addSuggestionNum);
				if (image != null)
				{
					image.src = 'minus.png';
					image.title = 'Remove from text box';
				}
			}
			manualEntry.select();
		}
		//alert(manualEntry[spanIdx]);
		//if (manualEntry[spanIdx] == '' && selections[spanIdx] != 0)
		//{
		//	var manualEntryElem1 = document.getElementById("manualEntry" + spanIdx);
		//	var list = optionList[spanIdx].split(",");
		//	manualEntryElem1.value = list[optIdx];
		//}
		if (deletedListResp != '')
		{
			markDeletedSuggestions(spanIdx);
		}

	}
}

function GetXmlHttpObject() {
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		return new XMLHttpRequest();
	}
	if (window.ActiveXObject) {
		// code for IE6, IE5
		return new ActiveXObject("Microsoft.XMLHTTP");
	}
	return null;
}

//-- Fns for computing the position of an element relative to the entire page
function getLeftPos(ctrl) {
	var ret = 0;
	while (ctrl != null) {
		ret += ctrl.offsetLeft;
		ctrl = ctrl.offsetParent;
	}
	return ret;
}

function getTopPos(ctrl) {
	var ret = 0;
	while (ctrl != null) {
		ret += ctrl.offsetTop;
		ctrl = ctrl.offsetParent;
	}
	return ret;
}

function deleteWord(obj, suggestionNum) {
	var idx = raisedWindowIndex;
	var image = document.getElementById("delete_" + idx + "_" + suggestionNum);
	// Checking image probably not a good way to do it
	var deleteSugg = true;
	var msg = "Are you sure you want to delete this suggestion?";
	if (suggestionNum == 0)
		msg = "Are you sure you want to delete all suggestions?";
	if (image != null && image.src.indexOf('check.png') != -1)
	{
		deleteSugg = false;
		if (suggestionNum != 0)
			msg = "Are you sure you want to undelete/reinstate this suggestion?";
		else
			msg = "Are you sure you want to undelete all suggestions?";
	}
	if (!confirm(msg))
	{
		return;
	}
	delUndelSugg(idx, suggestionNum, deleteSugg, image);
}

function delUndelSugg(idx, suggestionNum, deleteSugg, image)
{
	xmlhttp = GetXmlHttpObject();
	//alert("Delete: idx=" + idx + " suggestion=" + suggestionNum + " calling jsp");
	if (xmlhttp == null) {
		alert("Your browser does not support XMLHTTP!");
		return;
	}
	var suggText = "";
	var list = optionList[idx].split(",");
	suggText = list[suggestionNum];
	var command = "delete";
	if (!deleteSugg)
		command = "undelete";
	var url = "save_mismatched_phrase.jsp";
	url = url + "?idx=" + idx;
	url = url + "&suggestionNum=" + suggestionNum;
	url = url + "&command=" + command;
	xmlhttp.open("POST", url, false);
	xmlhttp.send(null);
	var resp = xmlhttp.responseText;
    try {             
		var mtable = document.getElementById("dropdowntable_" + idx);
		if (mtable != null)
		{
			var rows = mtable.rows;
			if (rows == null)
			{
				rows = mtable.childNodes; // must be <ul>
				for (var i = 0; i < rows.length ; i++)
				{
					if (rows[i].id == 'dropdownrow_' + idx + '_' + suggestionNum || suggestionNum == 0)
					{
						//mtable.removeChild(rows[i]);
						if (deleteSugg)
						{
							rows[i].style.textDecoration = 'line-through';
							if (image != null)
							{
								image.src="check.png";
								image.title="Undelete suggestion";
								deletedList[idx] = deletedList[idx] + "," + suggestionNum;
							}
							if (suggestionNum == 0)
							{
								var errSpan = document.getElementById("spErr" + idx);
								errSpan.className = "spErr_deleted";
								highlights[idx] = "spErr_deleted";
							}
						}
						else
						{
							if (suggestionNum == 0)
							{
								var errSpan = document.getElementById("spErr" + idx);
								errSpan.className = "spErr";
								highlights[idx] = "spErr";
							}
							rows[i].style.textDecoration = 'none';
							if (image != null)
							{
								image.src="x.png";
								image.title="Remove suggestion";
								var delarray = deletedList[idx].split(",");
								var dellist = "";
								for (var d=0; d < delarray.length ; d++)
								{
									if (delarray != suggestionNum)
									{
										dellist = "," + delarray;
									}
								}
								deletedList[idx] = "";
								if (dellist.length > 1)
								{
									deletedList[idx] = dellist.substring(1);
								}
							}
						}
					}
				}
			}
			else
			{
				for (var i = 0; i < rows.length ; i++)
				{
					if (rows[i].id == 'dropdownrow_' + idx + '_' + suggestionNum)
					{
						mtable.deleteRow(i);
					}
				}
			}
		}
		//mtable.deleteRow(parseInt(suggestionNum));
		changeSuggListStatus(suggText, idx, deleteSugg)
	}catch(e) {
		alert(e);
	}
}
function markDeletedSuggestions(idx)
{
	var deletedRows = deletedList[idx].split(",");
	if (deletedRows.length == 0) return;
    try {             
		var mtable = document.getElementById("dropdowntable_" + idx);
		if (mtable == null) return;
		var rows = mtable.rows;
		if (rows == null)
		{
			rows = mtable.childNodes; // must be <ul>
			for (var i = 0; i < rows.length ; i++)
			{
				for (var j = 0; j < deletedRows.length ; j++)
				{
					if (rows[i].id == 'dropdownrow_' + idx + '_' + deletedRows[j])
					{
						rows[i].style.textDecoration = 'line-through';
						var image = document.getElementById("delete_" + idx + "_" + deletedRows[j]);
						if (image != null)
						{
							image.src="check.png";
							image.title="Undelete Suggestion";
						}
					}
				}
			}
		}
		else
		{
			for (var i = 0; i < rows.length ; i++)
			{
				if (rows[i].id == 'dropdownrow_' + idx + '_' + suggestionNum)
				{
					mtable.deleteRow(i);
				}
			}
		}
	}catch(e) {
		alert(e);
	}
}
function changeSuggListStatus(suggestion, idx, deleteSugg)
{
	suggestion = new String(suggestion);
	suggestion = suggestion.toLowerCase();
	var singular  = suggestion;
	var present  = suggestion;
	if (suggestion.endsWith("ies"))
	{
		singular = suggestion.substring(0, suggestion.length-3) + "y";
	}
	else if (suggestion.endsWith("es"))
	{
		singular = suggestion.substring(0, suggestion.length-2);
	}
	else if (suggestion.endsWith("s"))
	{
		singular = suggestion.substring(0, suggestion.length-1);
	}
	if (suggestion.endsWith("d"))
	{
		present = suggestion.substring(0, suggestion.length-1);
	}
	if (suggestionListDelCount == null)
	{
		suggestionListDelCount = new Array();
		for (var i = 0;i < suggestionList.length ;i++)
		{
			suggestionListDelCount[i] = 0;
		}
	}
	for (var i = 0; i < suggestionList.length ;i++)
	{
		if (suggestion == suggestionList[i] || singular == suggestionList[i] || present == suggestionList[i])
		{
			if (deleteSugg)
			{
				suggestionListDelCount[i] = suggestionListDelCount[i] + 1;
				if (suggestionListDelCount[i] > suggestionListCount[i])
					suggestionListDelCount[i] = suggestionListCount[i];
				if (suggestionListDelCount[i] == suggestionListCount[i])
					suggestionStrike(i, deleteSugg);
			}
			else
			{
				suggestionListDelCount[i] = suggestionListDelCount[i] - 1;
				if (suggestionListDelCount[i] < 0)
					suggestionListDelCount[i] = 0;
				if (suggestionListDelCount[i] < suggestionListCount[i])
					suggestionStrike(i, deleteSugg);
			}
		}
	}

}

function deleteAll(suggestionNum, indices, suggestion) {
	var image = document.getElementById("turboDel" + suggestionNum);
	// Checking image probably not a good way to do it
	var deleteSugg = true;
	var msg = "Are you sure you want to delete this suggestion?";
	if (image != null && image.src.indexOf('check.png') != -1)
	{
		deleteSugg = false;
	}
	else
	{
		//if (!confirm("Are you sure you want to delete suggestion : " + suggestion))
		//{
		//	return;
		//}
	}
	hideLastMenu();
	var idx = indices.split(", ");
	for (var i = 0; i < idx.length ; i++)
	{
		if (idx[i] == '') continue;
		xmlhttp = GetXmlHttpObject();
		//alert("Delete: idx=" + idx + " suggestion=" + suggestionNum + " calling jsp");
		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}

		//alert("Removing:" + idx[i] + ":" + suggestion + ":" + suggestionNum);
		//Do we need to do this for deleteAll???
		var command = "deleteAll";
		if (!deleteSugg)
		{
			command = "undeleteAll";
			suggestionListDelCount[suggestionNum] = 0;
		}
		var serverRemove = true;
		if (serverRemove)
		{
			var url = "save_mismatched_phrase.jsp";
			url = url + "?idx=" + idx[i];
			url = url + "&suggestionText=" + escape(suggestion);
			url = url + "&command=" + command;
			xmlhttp.open("POST", url, false);
			xmlhttp.send(null);
			var resp = xmlhttp.responseText;
		}
		try {
			var inflsuggestion = suggestion;
			if (inflsuggestion.endsWith("y"))
			{
				inflsuggestion = inflsuggestion.substring(0, inflsuggestion.length-1);
			}
			var mtable = document.getElementById("dropdowntable_" + idx[i]);
			if (mtable == null) continue;
			var rows = mtable.rows;
			var idp = parseInt(idx[i]);
			if (rows == null)
			{
				rows = mtable.childNodes; // must be <ul>
				for (var j = 0; j < rows.length ; j++)
				{
					//alert(rows[j].innerHTML);
					if (rows[j].innerHTML.indexOf(">" + inflsuggestion) != -1)
					{
						//mtable.removeChild(rows[i]);
						//alert("phrase:" + i + " + sugg#:" + j);
						if (deleteSugg)
						{
							rows[j].style.textDecoration = 'line-through';
							if (image != null)
							{
								image.src="check.png";
								image.title="Undelete suggestion";
								deletedList[idp] = deletedList[idp] + "," + suggestionNum;
							}
							var image2 = document.getElementById("delete_" + idp + "_" + j);
							if (image2 != null)
							{
								image2.src="check.png";
								image2.title="Undelete Suggestion";
							}
						}
						else
						{
							rows[j].style.textDecoration = 'none';
							if (image != null)
							{
								image.src="x.png";
								image.title="Remove suggestion";
								var image2 = document.getElementById("delete_" + idp + "_" + j);
								if (image2 != null)
								{
									image2.src="x.png";
									image2.title="Remove Suggestion";
								}
								var delarray = deletedList[idp].split(",");
								var dellist = "";
								for (var d=0; d < delarray.length ; d++)
								{
									if (delarray[d] != j)
									{
										dellist = "," + delarray[d];
									}
								}
								deletedList[idp] = "";
								if (dellist.length > 1)
								{
									deletedList[idp] = dellist.substring(1);
								}

							}
						}
					}
				}
			}
			else
			{
				for (var j = 0; j < rows.length ; j++)
				{
					if (rows[j].innerText == suggestion)
					{
						mtable.deleteRow(j);
					}
				}
			}
		}catch(e) {
			alert(e);
		}
	}
	suggestionStrike(suggestionNum, deleteSugg);
	return;
	try {             
		var ttable = document.getElementById("turboTable");
		var rows = ttable.rows;
		for (var i = 0; i < rows.length ; i++)
		{
			if (rows[i].id == ('turboTR' + suggestionNum))
			{
				//ttable.deleteRow(i);
				break;
			}
		}		
	}catch(e) {
		alert(e);
	}
}

function suggestionStrike(suggestionNum, deleteSugg)
{
	var sugges = document.getElementById("turboLi" + suggestionNum);
	if (sugges != null)
	{
		if (deleteSugg)
			sugges.style.textDecoration = 'line-through';
		else
			sugges.style.textDecoration = 'none';
	}
	var image = document.getElementById("turboDel" + suggestionNum);
	if (image != null)
	{
		if (deleteSugg)
		{
			image.src="check.png";
			image.title="Undelete Suggestion";
		}
		else
		{
			image.src="x.png";
			image.title="Remove Suggestion";
		}
	}
}

function swapTables()
{
	var turbo = document.getElementById("turboTable");
	var missing = document.getElementById("missingTable");
	if (turbo.style.display == 'none')
	{
		turbo.style.display = 'inline';
		missing.style.display = 'none';
	}
	else
	{
		turbo.style.display = 'none';
		missing.style.display = 'inline';
	}
}
function setDebugText(text)
{
	var deb = document.getElementById("debug1");
	deb.innerHTML = text;
}

function deleteAdditionalPhrase(phraseNum, phrase) {
	//alert("" + phraseNum + " " + phrase);
	var image = document.getElementById("missingDel" + phraseNum);
	// Checking image probably not a good way to do it
	var deletePhrase = true;
	var msg = "Are you sure you want to delete this phrase?";
	if (image != null && image.src.indexOf('check.png') != -1)
	{
		deletePhrase = false;
	}
	else
	{
		//if (!confirm("Are you sure you want to delete suggestion : " + suggestion))
		//{
		//	return;
		//}
	}
	xmlhttp = GetXmlHttpObject();
	//alert("Delete: num=" + phraseNum + " phrase=" + phrase + " calling jsp");
	if (xmlhttp == null) {
		alert("Your browser does not support XMLHTTP!");
		return;
	}

	var command = "delete";
	if (!deletePhrase)
	{
		command = "undelete";
	}
	var url = "delete_additional_phrase.jsp";
	url = url + "?additional_phrase=" + escape(phrase);
	url = url + "&command=" + command;
	xmlhttp.open("POST", url, false);
	xmlhttp.send(null);
	var resp = xmlhttp.responseText;
	var phraseTD = document.getElementById("missingTD" + phraseNum);
	if (phraseTD != null)
	{
		if (deletePhrase)
			phraseTD.style.textDecoration = 'line-through';
		else
			phraseTD.style.textDecoration = 'none';
	}
	var image = document.getElementById("missingDel" + phraseNum);
	if (image != null)
	{
		if (deletePhrase)
		{
			image.src="check.png";
			image.title="Undelete Phrase";
		}
		else
		{
			image.src="x.png";
			image.title="Remove Phrase";
		}
	}
}

var synDelImg;
var synDelImgSrc;
function resetSynDelImg()
{
    try {
		if (synDelImg != null && synDelImgSrc != null)
		{
			synDelImg.src = synDelImgSrc;
			synDelImg = null;
			synDelImgSrc = null;
		}
	}catch(e) {
	}
}

function getSynonyms(evt, phrase, image)
{
	//if (!isIE && (evt == null))
	//	return;
	var tt = document.getElementById('tt');
	if (tt == null)
		return false;
	var x = tt.style.left;
	var y = tt.style.top;
	var event = isIE ? window.event : evt;
	if (event != null)
	{
		x = document.body.scrollLeft + (isIE ? event.x : event.clientX);
		y = document.body.scrollTop + (isIE ? event.y : event.clientY);
		if (isIE) {
			x = event.clientX + document.body.scrollLeft;
			y = event.clientY + document.body.scrollTop;
		}
		var eventx = x;
		var eventy = y;

		if (!isIE)
		{
			x = x - 25;
			y = y - 80;
			if (!isChrome)
				y = y + window.pageYOffset;
		}
		else
		{
			x = x - 20;
			y = y - 75;
			y = y + getScrollXY()[1];
		}
	}
	var url = "synonyms_for_phrase.jsp?random=" + new Date() + "&phrase=" + escape(phrase);
	new Ajax.Request(url, { 
		method:'get',
		parameters : '',
		onSuccess: function(transport){
			
			var response = transport.responseText;
			if(response != null)
			{
				turboTooltip(response, x, y);
				if (image != null)
				{
					resetSynDelImg();
					synDelImg = image;
					synDelImgSrc = image.src;
					var ind = synDelImgSrc.indexOf("-");
					if (ind != -1)
						image.src = "minus" + synDelImgSrc.substring(ind);
				}
			}
			else
			{
				alert('server response is null' );
			}
		}
	});
}

var tagType = "";
function showResumePhrasesPopupOld(evt)
{
	//if (!isIE && (evt == null))
	//	return;
	var tt = document.getElementById('popup');
	if (tt == null)
		return false;
	var x = tt.style.left;
	var y = tt.style.top;

	var event = isIE ? window.event : evt;
	if (event != null)
	{
		x = document.body.scrollLeft + (isIE ? event.x : event.clientX);
		y = document.body.scrollTop + (isIE ? event.y : event.clientY);
		if (isIE) {
			x = event.clientX + document.body.scrollLeft;
			y = event.clientY + document.body.scrollTop;
		}
		var eventx = x;
		var eventy = y;

		if (!isIE)
		{
			x = x - 25;
			y = y - 70;
			if (!isChrome)
				y = y + window.pageYOffset;
		}
		else
		{
			x = x - 20;
			y = y - 65;
			y = y + getScrollXY()[1];
		}
	}

	var url = "get_resume_phrases.jsp?openTag=" + tagType + "&random=" + new Date();
	 $.ajax({         
			url: url,         
			type: 'GET',         
			async: false,         
			cache: false,         
			timeout: 30000,         
			error: function(){return true;},
			success: function(response){
				alert(response);
				if(response != null)
				{
					turboTooltip(response, x, y);
					var tt = document.getElementById('tt');
					if (tt != null)
					{
						tt.style.maxHeight = '400px';
						tt.style.overflow = 'auto';
					}
				}
				else
				{
					alert('server response is null' );
				}
			}
	});
}

function showTagListPopup(evt)
{
	//if (!isIE && (evt == null))
	//	return;
	var tt = document.getElementById('popup');
	if (tt == null)
		return false;
	var x = tt.style.left;
	var y = tt.style.top;

	var event = isIE ? window.event : evt;
	if (event != null)
	{
		x = document.body.scrollLeft + (isIE ? event.x : event.clientX);
		y = document.body.scrollTop + (isIE ? event.y : event.clientY);
		if (isIE) {
			x = event.clientX + document.body.scrollLeft;
			y = event.clientY + document.body.scrollTop;
		}
		var eventx = x;
		var eventy = y;

		if (!isIE)
		{
			x = x - 25;
			y = y - 70;
			if (!isChrome)
				y = y + window.pageYOffset;
		}
		else
		{
			x = x - 20;
			y = y - 65;
			y = y + getScrollXY()[1];
		}
	}

	var url = "get_taglist.jsp?random=" + new Date();
	new Ajax.Request(url, { 
		method:'get',
		parameters : '',
		onSuccess: function(transport){
			
			var response = transport.responseText;
			if(response != null)
			{
				turboTooltip(response, x, y);
				var tt = document.getElementById('tt');
				if (tt != null)
				{
					tt.style.maxHeight = '300px';
					tt.style.overflow = 'auto';
				}
			}
			else
			{
				alert('server response is null' );
			}
		}
	});
}

function turboTooltip(tableHtml, x, y) {
	var tt = document.getElementById('tt');
	if (tt == null)
		return false;
	tt.innerHTML = tableHtml;
	tt.style.left = x +'px';
	tt.style.top = y + 'px';
	tt.style.display = "inline";
    try {             
		//tt.style.display = "inline";
		//var xpos = tt.style.left;
		//xpos = xpos.replace("px","");
		//xpos = parseInt(xpos);
		//xpos = xpos - 325;
		//xpos = "" + xpos + "px";
		//tt.style.left = xpos;
	}catch(e) {
	}
	//alert(tt.innerHTML);
}
function deleteSynonym(phrase, idx) {
	var deleteSyn = true;
	var lastIdx = idx;
	if (idx == 0)
	{
		if (!confirm("Are you sure you want to mark all equivalent phrases as invalid and disable their use?"))
		{
			return;
		}
		idx = 1;
		lastIdx = 20;
		deleteSyn = true;
	}
	else
	{
		var cbx = document.getElementById("synDel2_" + (idx-1));
		// Checking image probably not a good way to do it
		if (cbx != null && cbx.checked)
		{
			deleteSyn = false;
		}
	}
	for (var i = idx; i <= lastIdx ; i++)
	{
		cbx = document.getElementById("synDel2_" + (i-1));
		if (cbx == null)
			break;
		//if (deleteSyn && image.src.indexOf('check.png') != -1)
		//{
		//	continue;
		//}
		var tr = document.getElementById("syntr" + (i-1));
		var command = "delete";
		if (!deleteSyn)
			command = "undelete";
		var url = "save_rejected_synonym.jsp";
		url = url + "?idx=" + i;
		url = url + "&phrase=" + phrase;
		url = url + "&command=" + command;
		if (xmlhttp == null)
		{
			xmlhttp = GetXmlHttpObject();
		}
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
		var resp = xmlhttp.responseText;
		if (resp != null)
		{
			if (tr != null)
			{
				//tr.style.display='none';
				if (deleteSyn)
					tr.style.textDecoration = 'line-through';
				else
					tr.style.textDecoration = 'none';
			}
		}
		if (deleteSyn)
		{
			//image.src="check.png";
			cbx.checked = false;
		}
		//else
		//{
		//	image.src="x.png";
		//}
	}
}

function getTargetSentences(evt, phrase)
{
	if (!isIE && (evt == null))
		return;
	var event = isIE ? window.event : evt;
	var x = document.body.scrollLeft + (isIE ? event.x : event.clientX);
	var y = document.body.scrollTop + (isIE ? event.y : event.clientY);
	if (isIE) {
		x = event.clientX + document.body.scrollLeft;
		y = event.clientY + document.body.scrollTop;
	}
	var eventx = x;
	var eventy = y;

	if (!isIE)
	{
		x = x - 28;
		y = y - 125;
		if (!isChrome)
			y = y + window.pageYOffset;
	}
	else
	{
		x = x - 23;
		y = y - 120;
		y = y + getScrollXY()[1];
	}
	var url = "get_resume_sentences.jsp?random=" + escape("" + new Date()) + "&phrase=" + escape(phrase) + "&resumeIdx=" + viewFileIndex;
	new Ajax.Request(url, { 
		method:'get',
		parameters : '',
		onSuccess: function(transport){
			
			var response = transport.responseText;
			if(response != null)
			{
				turboTooltip(response, x, y);
			}
			else
			{
				alert('server response is null' );
			}
		}
	});
}
function getDropDown(idx, add, suggestNum)
{
	tthide();
	if (add)
	{
		addSuggestionNum = suggestNum;
	}
	var highLight = document.getElementById("spErr" + idx);
	//highLight.scrollIntoView();
	//window.scrollBy(0,-30); 
	elemToRaise = highLight;
	spanIdx = idx;
	raiseSelectionWindow(highLight, idx, false);
}
