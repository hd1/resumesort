<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>



    <script type="text/javascript">
        var EMPTY_JOB_TEXT = "COPY AND PASTE THE RELEVANT PART OF JOB AD IN THIS TEXT BOX!!";
        var EMPTY_EMAIL_TEXT = "This is where your results will be emailed to";

        function clearDefaultJobText(el)
        {
                if (el.value == EMPTY_JOB_TEXT)
                        el.value = "";
        }

        function setDefaultJobText(el)
        {
                if (el.value == "")
                        el.value = EMPTY_JOB_TEXT;
        }

        function clearDefaultEmailText(el)
        {
                if (el.value == EMPTY_EMAIL_TEXT)
                        el.value = "";
        }

        function clearFileText()
        {
                document.uploadform.targetfile.value="";
                //document.uploadform.uploadResume.checked="";
        }


        function clearFileSelect()
        {
                //document.uploadform.selectedResume.selectedIndex=0;
        }

        function setDefaultEmailText(el)
        {
                if (el.value == "")
                        el.value = EMPTY_EMAIL_TEXT;
        }

        function  doSubmit()
        {
                //if (isValid())
                {
                        var myvar = "1";

                        <% session.setAttribute("listSelected","1"); %>
                        <%-- session.setMaxInactiveInterval(3600); --%>
                        document.uploadform.submit();
                }
        }

        function  isValid()
        {
                if (document.uploadform.emailconfirm.value!=document.uploadform.email.value)
                {
                        alert("Your emails do not match!");
                        return false;
                }

                if (document.uploadform.sourcetext.value=="" || document.uploadform.sourcetext.value==EMPTY_JOB_TEXT )
                {
                        alert("Please paste a valid job ad.");
                        return false;
                }

                if (document.uploadform.targetfile.value=="" && (document.uploadform.selectedResume == null || document.uploadform.selectedResume.selectedIndex == 0))
                {
                        alert("Please select you resume");
                        return false;
                }


                return true;
        }

    </script>

    <form enctype='multipart/form-data' method='post' action='SuggestorVinod'
        name="uploadform">

        <div style="float: left;position: relative;margin-left: 3%; width:30%">Copy and paste the Job Posting!!
            <textarea name="sourcetext" cols="50" onfocus="clearDefaultJobText(this)" style="overflow:auto;"
        onblur="setDefaultJobText(this)" rows="30">COPY AND PASTE THE RELEVANT PART OF JOB AD IN THIS TEXT BOX!!</textarea>
        </div>

        <div style="float: left;position: relative;margin-left: 3%; margin-bottom: 5%; height: 70%;width:25%">
            <div style="position:relative;margin-top: 50%">Upload your resume(.docx, .doc, .odt, .rtf, .txt).
                <input name='targetfile' type="file" size="40" onchange="clearFileSelect()" /><br/>
            </div>
        </div>

        <div align ="center" style="float: left;position: relative;margin-left: 3%;margin-bottom: 5%;height: 80%; width:20%">
            <input type='button' value='Get Phrases' style="position:relative;width: 150px;margin-top: 70%;" onclick="doSubmit()"/>
        </div>
    </form>
                        
