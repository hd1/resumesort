<%@include file="rq_header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<script language="JavaScript" type="text/JavaScript" src="prototype.js"></script>
<%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
	if (user == null)
	{
		response.sendRedirect("Login.jsp");
		return;
	}
	String userdir = ResumeSorter.getUploadDir(user);
    session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString(), userdir);
	String postingId = request.getParameter("postingId");
	List<JobPosting> postings = new JobPosting().getObjects("user_name=" + UserDBAccess.toSQL(user.getLogin()) + " and id in (select distinct job_id from resume_score) order by posting_id");
	List<SharedJobPosting> sjps =  new SharedJobPosting().getObjects("perm3 = 1 and IDUSER = " + user.getId());
	JobPosting posting = null;
	boolean owner = false;
	for (JobPosting jp: postings)
	{
		if (String.valueOf(jp.getId()).equals(postingId))
		{
			posting = jp;
			owner = true;
		}
	}
	if ("delete".equals(request.getParameter("command")) && posting != null)
	{
		new SortPhrase().deleteObjects("JOB_ID = " + postingId);
		new SharedJobPosting().deleteObjects("JOB_ID = " + postingId);
		new ResumeScore().deleteObjects("JOB_ID = " + postingId);
		File postingFile = PlatformProperties.getInstance().getResourceFile(
                                        ResumeSorter.getUploadDir(user) + File.separator + ResumeSorter.JOBPOSTING_FOLDER + File.separator + posting.getPostingFile());
		if (postingFile.exists())
			postingFile.delete();
		postingFile = PlatformProperties.getInstance().getResourceFile(
                                        ResumeSorter.getUploadDir(user) + File.separator + ResumeSorter.JOBPOSTING_FOLDER + File.separator + posting.getPostingFile() + ".Object");
		if (postingFile.exists())
			postingFile.delete();
		posting.delete();
		postings.remove(posting);
		posting = null;
		postingId = null;
	}

	for (SharedJobPosting sjp: sjps)
	{
		List<JobPosting> postings2 = new JobPosting().getObjects("ID =" + sjp.getJobId());
		postings.addAll(postings2);
		if (posting == null)
		{
			for (JobPosting jp: postings2)
			{
				if (String.valueOf(jp.getId()).equals(postingId))
				{
					posting = jp;
					userdir = userdir.replace(user.getLogin(), jp.getUserName());
					break;
				}
			}
		}
	}
	if (posting == null && postingId != null && user.isAdmin())
	{
		List<JobPosting> postings2 = new JobPosting().getObjects("ID =" + postingId);
		if (postings2.size() > 0)
			posting = postings2.get(0);
	}
	if (posting == null && postingId != null )
	{
	%>
		<script>alert("Job Posting: <%=postingId%> not found")</script>;
	<%
	}
	if (posting == null && postings.size() > 0)
	{
		posting = postings.get(0);
	}
	if (posting == null)
	{
	%>
			<script>alert("No Job Postings found");
	<%
		return;
	}
	if ("restore".equals(request.getParameter("command")))
	{
		String userName = request.getParameter("userName");
		if (userName == null) userName = user.getLogin();
		String directory = ResumeSorter.getUploadDir(userName);
		try
		{
			JobPostingSession jps = new JobPostingSession(userName, posting.getId());
			jps.restore(session, directory);
%>
	<script>window.location="highlightedResumes.jsp"</script>
<%
		}
		catch (Exception x)
		{
%>
	<script>alert("Error restoring session:<%=x.getMessage().replace("\n","\\n")%>");</script>
<%
		}
	}
	List<ResumeScore> scores = new ResumeScore().getObjects("JOB_ID=" + posting.getId());
	Map<String, Map<String, ResumeScore>> resumeScores = new TreeMap<String, Map<String,ResumeScore>>();
	Map<String, ResumeScore> firstResume = null;
	for (ResumeScore score : scores)
	{
		String resume = score.getResumeFile();
		Map<String, ResumeScore> userScores = resumeScores.get(resume);
		if (userScores == null)
		{
			userScores = new TreeMap<String, ResumeScore>();
			if (firstResume == null)
			{
				firstResume =  userScores;
			}
			resumeScores.put(resume, userScores);
		}
		String userName = score.getUserName();
		userScores.put(userName, score); 
	}
	String error = request.getParameter("error");
	if (error != null && error.length() > 0)
	{
%> <script>alert("<%=error%>");</script> <%
	}
	String defValue = "Enter Email Address";
%>
<script>
	function showResume(ind)
	{
		var link = "ResumeViewer?viewFileIndex=" + ind;
		var newwin = window.open(link, "HighLightedResume");
		newwin.focus();
	}
	var busy = false;
	function sendResumes()
	{
		if (busy)
		{
			alert("Function in progress, please wait...")
			return;
		}
		var elems = document.resumeListForm.elements;
		var selected = false;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name == "emailResumeIdx" && elems[i].checked)
			{
				selected = true;
			}
		}
		if (!selected)
		{
			alert("Please select some resumes")
			return;
		}
		busy = true;
		var url = "email_resumes.jsp?random=" + new Date();
		var form = document.resumeListForm;
		new Ajax.Request(url, { 
			method:'post',
			parameters : Form.serialize(form),
			onSuccess: function(transport){
				
				var response = transport.responseText;
				// ignore response
				if (response != null)
					alert("Email has been sent");
				else
					alert("Error sending email");
				busy = false;
			}
		});
	}
	function selectAll(obj)
	{
		var selcbx = document.getElementById("selall");
		var elems = document.resumeListForm.elements;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name == "emailResumeIdx")
			{
				if (selcbx.checked)
				{
					elems[i].checked = true;
				}
				else
				{
					elems[i].checked = false;
				}
			}
		}
	}

	function clearDefault(inp)
	{
		if (inp.value == '<%=defValue%>')
		{
			inp.value = '';
		}
	}

	function showPosting(obj)
	{
		window.location = "job_posting_report.jsp?postingId=" + obj.options[obj.selectedIndex].value;
	}
	function deletePosting(id)
	{
		if (!confirm("Are you sure you wish delete all information for this posting including Resume scores, Selected phrases, Posting file?"))
		{
			return;
		}
		window.location = "job_posting_report.jsp?command=delete&postingId=" + id;
	}

	function restoreSession(userName)
	{
		window.location = "job_posting_report.jsp?postingId=<%=postingId%>&command=restore&userName=" + escape(userName);
	}
</script>
<br>
<h1>Select Posting:
<select name=jobposting onchange="showPosting(this)">
<%
	for (int i = 0; i < postings.size(); i++)
	{
		String selected = "";
		if (posting.getId() == postings.get(i).getId())
			selected = "selected";
%>
<option value="<%=postings.get(i).getId()%>" <%=selected%>><%=postings.get(i).getPostingId()%></option>
<%	} %>
</select>
</h1>
<br>
<br>
<%
%>
<center><h1>Resume Scores for Job Posting: <%=posting.getPostingId()%> <% if (owner) { %> <input type=button value="Delete Posting" onclick="deletePosting(<%=posting.getId()%>)"><% } %>
</h1>
<% if (firstResume == null) { %>
<center><b>No scores found</b></center>
<%	} else { %>
<br>
<form name=resumeListForm>
<table class=scorestbl>
<tr><th align=center rowspan=2><!-- input id="selall" type="checkbox" onclick="selectAll(this)"--></th><th align=center rowspan=2>Resume File Name</th>
<%
		for (String userName: firstResume.keySet())
		{
			JobPostingSession jps = new JobPostingSession(userName, posting.getId());
			String directory = ResumeSorter.getUploadDir(userName);
			//if (userName.equals(user.getLogin())) userName = "By You";
			System.out.println("User:" + userName + " Directory:" + directory);
%>
		<th colspan=2><%=userName%><br>
		<% if (jps.exists(directory)) { %>
		<input type=button onclick="restoreSession('<%=userName%>')" value="Restore Session">
		<% } %>
		</th>
<%		} %>
		<th colspan=2>Overall</th>
</tr>
<tr>
<%
		for (String userName: firstResume.keySet())
		{
%>
		<th align=center>Score</th><th align=center>Rank</th>
<%		} %>
		<th align=center>Score</th><th align=center>Rank</th>
</tr>
<%
	int num = firstResume.keySet().size();
	DecimalFormat decformat2 = new DecimalFormat("00000.00");
	for (String resume : resumeScores.keySet())
	{
		Map<String, ResumeScore> userScores = resumeScores.get(resume);
		double avgscore = 0;
		double avgrank = 0;
		Map<String,Double> mscores = new TreeMap<String, Double>();
		for (String userName: userScores.keySet())
		{
			if (userName.equals("zzzAverage")) continue;
			ResumeScore score = userScores.get(userName);
			avgscore = avgscore + score.getScore();
			avgrank = avgrank + score.getRank();
			mscores.put(decformat2.format(score.getScore())+ "_" + userName, score.getScore());
		} 
		avgscore = avgscore/num;
		avgrank = avgrank/num;
		int i = 0;
		int mid1 = num/2;
		int mid2 = 0;
		if (num%2 == 0)
			mid2 = mid1+1;
		else
			mid1 = mid1+1;
		double median = 0;
		for (String mscore: mscores.keySet())
		{
			i++;
			if (i == mid1)
				median = mscores.get(mscore);
			if (i == mid2)
				median = (median + mscores.get(mscore))/2;
		} 

		ResumeScore score = new ResumeScore();
		score.setScore(avgscore);
		score.setScore(median);
		userScores.put("zzzAverage",score);
	}
	TreeMap<String, Map<String, ResumeScore>> sortedScores = new TreeMap<String, Map<String,ResumeScore>>();
	for (String resume : resumeScores.keySet())
	{
		Map<String, ResumeScore> userScores = resumeScores.get(resume);
		ResumeScore score = userScores.get("zzzAverage");
		sortedScores.put(decformat2.format(score.getScore())+ "_" + resume, userScores);
	}
	int index = 0;
	DecimalFormat decformat = new DecimalFormat("########0.##");
	List<File> resumes = new ArrayList<File>();
	for (String resume : sortedScores.descendingKeySet())
	{
		Map<String, ResumeScore> userScores = sortedScores.get(resume);
		int underscore = resume.indexOf("_");
		if (underscore != -1) 
			resume = resume.substring(underscore+1);
		if (resume.length() > 50) resume = resume.substring(0,47) + "...";
%>
	<tr><td align=center><!-- input type=checkbox value="<%=index%>" name="emailResumeIdx" --></td><td align=left><a href = "#" onclick="showResume(<%=index%>)"> <%=resume%> </td>
<%
		for (String userName: userScores.keySet())
		{
			ResumeScore score = userScores.get(userName);
			int rank = score.getRank();
			int total = score.getTotal();
			if (userName.indexOf("Average") != -1)
			{
				rank = index+1;
				total = sortedScores.keySet().size();
			}
			String scoreStr = decformat.format(score.getScore());
			if (scoreStr.indexOf("999") != -1)
			{
				scoreStr = "Error";
			}
%>
		<td><%=scoreStr%></td><td><%=rank%> / <%=total%></td>
<%		}
%>
	</tr>
<%
		index++;
        File resumeFile = PlatformProperties.getInstance().getResourceFile(
                                    userdir + File.separator + "MyResumes" + File.separator + resume);
		resumes.add(resumeFile);
		//if (index >= 10) continue;
	}
    session.setAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString(), resumes);                    
%>
<!--tr><td align=center colspan=2><br><input type=button onclick="sendResumes()" value="Email Selected Resumes"></td></tr-->
</table>

</form>
<%	} %>
<br>
<br>
<br>
<a href="rq_uploader.jsp">Go back to the upload page</a></b></font>

</center>
<%@include file="rq_footer.jsp"%>
</body>
</html>