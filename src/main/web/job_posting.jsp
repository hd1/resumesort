<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<!-- script language="JavaScript" type="text/JavaScript" src="prototype.js"></script-->
<%
	if (user == null)
	{
		response.sendRedirect("");
		return;
	}
	String userdir = ResumeSorter.getUploadDir(user);
    session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString(), userdir);
	String postingId = request.getParameter("postingId");
	JobPosting posting = (JobPosting) new JobPosting().getObject("id=" + postingId);
	String error = request.getParameter("error");
	boolean owner = false;
	if (posting.getUserName().equals(user.getLogin()))
	{
		owner = true;
	}
	else
	{
		List<SharedJobPosting> sjps =  new SharedJobPosting().getObjects("perm3 = 1 and IDUSER = " + user.getId() + " and job_id=" + posting.getId());
		if (sjps.size() == 0)
			return;
	}
	if ("delete".equals(request.getParameter("command")) && posting != null)
	{
		new SortPhrase().deleteObjects("JOB_ID = " + postingId);
		new SharedJobPosting().deleteObjects("JOB_ID = " + postingId);
		new ResumeScore().deleteObjects("JOB_ID = " + postingId);
		File postingFile = PlatformProperties.getInstance().getResourceFile(
                                        ResumeSorter.getUploadDir(user) + File.separator + ResumeSorter.JOBPOSTING_FOLDER + File.separator + posting.getPostingFile());
		if (postingFile.exists())
			postingFile.delete();
		postingFile = PlatformProperties.getInstance().getResourceFile(
                                        ResumeSorter.getUploadDir(user) + File.separator + ResumeSorter.JOBPOSTING_FOLDER + File.separator + posting.getPostingFile() + ".Object");
		if (postingFile.exists())
			postingFile.delete();
		posting.delete();
		posting = null;
		postingId = null;
	}
	if ("deleteResume".equals(request.getParameter("command")) && posting != null)
	{
		String resume = request.getParameter("resume");
		if (resume != null && resume.trim().length() > 0)
			new ResumeScore().deleteObjects("JOB_ID=" + posting.getId() + " and resume_file = " + UserDBAccess.toSQL(resume));
	}
	if (posting == null && postingId != null )
	{
	%>
		<script>alert("Job Posting: <%=postingId%> not found")</script>;
	<%
	}
	boolean jobinprogress = false;
	Integer numleft = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
	if (numleft == null) numleft = 0;
	if (numleft > 0)
		jobinprogress = true;
    if (session.getAttribute(SESSION_ATTRIBUTE.RANKING_INPROGRESS.toString()) != null)
		jobinprogress = true;
 
	if (!jobinprogress && "restore".equals(request.getParameter("command")))
	{
		// Clear all old results
		session.removeAttribute(SESSION_ATTRIBUTE.TARGET_FILE.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.USER_SCORES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_HTMLRESUMES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());
		String userName = request.getParameter("userName");
		if (userName == null) userName = user.getLogin();
		String directory = ResumeSorter.getUploadDir(userName);
		JobPostingSession jps = new JobPostingSession(userName, posting.getId());
		try
		{
			System.out.println("Restoring:" + posting.getId());
			if (jps != null)
				jps.restore(session, directory);
			else
				throw new Exception("Error Restoring session");
			session.setAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString(), posting);
			Map<String,Integer> resumePhrasesMap = (Map<String,Integer>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
            File resumeDirectory = (File) session.getAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString());
			// If there are no resume phrases then start thread to extract them
			if (resumeDirectory != null && (resumePhrasesMap == null || resumePhrasesMap.keySet().size() == 0))
			{
				ZipfileProcessingThread zipThread = new ZipfileProcessingThread(session, resumeDirectory, false);
				zipThread.start();
			}
%>
	<script>window.location="ranked_resumes.jsp"</script>
<%
			return;
		}
		catch (Exception x)
		{
			String msg = x.getMessage();
			if (msg != null) msg = msg.replace("\n","\\n");
%>
	<script>alert("Error Restoring Session :<%=msg%>");</script>
<%
			error = x.getMessage();
			if (error != null && error.indexOf("incompatible") != -1)
				error = "Sorry! Saved session is incompatible with the latest ResumeSort software. Please rerun session";
			jps.delete(directory);
		}
	}
	if ("rerun".equals(request.getParameter("command")))
	{
		String resumeFolder = posting.getResumeFolder();
		String postingFile = posting.getPostingFile();
		String jobPostingId = posting.getPostingId();
%>
	<script>window.location="ResumeSorter?selectedPosting=<%=postingFile%>&JobPostingId=<%=jobPostingId%>&command=rerun"</script>
<%
		return;
	}
	List<ResumeScore> scores = new ResumeScore().getObjects("JOB_ID=" + posting.getId());
	Map<String, Map<String, ResumeScore>> resumeScores = new TreeMap<String, Map<String,ResumeScore>>();
	Map<String, ResumeScore> firstResume = null;
	for (ResumeScore score : scores)
	{
		String resume = score.getResumeFile();
		Map<String, ResumeScore> userScores = resumeScores.get(resume);
		if (userScores == null)
		{
			userScores = new TreeMap<String, ResumeScore>();
			if (firstResume == null)
			{
				firstResume =  userScores;
			}
			resumeScores.put(resume, userScores);
		}
		String userName = score.getUserName();
		userScores.put(userName, score); 
	}
	if (error != null && error.length() > 0)
	{
%> <script>alert("<%=error%>");</script> <%
	}
	String defValue = "Enter Email Address";
%>
<script>
	function showResume(ind)
	{
		var link = "ResumeViewer?viewFileIndex=" + ind;
		var newwin = window.open(link, "HighLightedResume");
		newwin.focus();
	}
	var busy = false;
	function sendResumes()
	{
		if (busy)
		{
			alert("Function in progress, please wait...")
			return;
		}
		var elems = document.resumeListForm.elements;
		var selected = false;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name == "emailResumeIdx" && elems[i].checked)
			{
				selected = true;
			}
		}
		if (!selected)
		{
			alert("Please select some resumes")
			return;
		}
		busy = true;
		var url = "email_resumes.jsp?random=" + new Date();
		var form = document.resumeListForm;
		new Ajax.Request(url, { 
			method:'post',
			parameters : Form.serialize(form),
			onSuccess: function(transport){
				
				var response = transport.responseText;
				// ignore response
				if (response != null)
					alert("Email has been sent");
				else
					alert("Error sending email");
				busy = false;
			}
		});
	}
	function selectAll(obj)
	{
		var selcbx = document.getElementById("selall");
		var elems = document.resumeListForm.elements;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name == "emailResumeIdx")
			{
				if (selcbx.checked)
				{
					elems[i].checked = true;
				}
				else
				{
					elems[i].checked = false;
				}
			}
		}
	}

	function clearDefault(inp)
	{
		if (inp.value == '<%=defValue%>')
		{
			inp.value = '';
		}
	}

	function showPosting(obj)
	{
		window.location = "job_posting.jsp?postingId=" + obj.options[obj.selectedIndex].value;
	}
	function deletePosting(id)
	{
		if (!confirm("Are you sure you wish delete all information for this Job Posting - including Resume scores, Selected phrases, Posting file?"))
		{
			return;
		}
		window.location = "job_posting.jsp?command=delete&postingId=" + id;
	}

	function restoreSession(userName)
	{
		window.location = "job_posting.jsp?postingId=<%=postingId%>&command=restore&userName=" + escape(userName);
	}
	function rerunSession()
	{
		window.location = "job_posting.jsp?postingId=<%=postingId%>&command=rerun";
	}
	function deleteScore(resumeName)
	{
		if (!confirm("Are you sure you wish delete all scores for this resume: " + resumeName + " for the Job Posting?"))
		{
			return;
		}
		window.location = "job_posting.jsp?command=deleteResume&postingId=<%=postingId%>&resume=" + escape(resumeName);
	}
</script>
	<style type="text/css" title="currentStyle">
			@import "js/table/css/demo_page.css";
			@import "js/table/css/demo_table_jui.css";
			@import "js/table/ui-lightness/jquery-ui-1.8.4.custom.css";
		</style>
		<script type="text/javascript" language="javascript" src="js/table/js/jquery.dataTables.js"></script>
<link type="text/css" href="../css/ui/jquery-blue.css" rel="stylesheet" />	
<div class="middle">
<div class="maintext_dashboard">
 
 <div class="sky_bar">
    <div style="float:right; clear:both;">
    <a href="upload_jobposting.jsp">START NEW JOBS</a>
    <a href="home.jsp" class="selected">PREVIOUS JOBS</a>
    </div>
 </div>
 
 <div class="text_dashboard">
<%
	if (error != null && error.length() > 0)
	{
%> <span class="error"><%=error%></span><%
	}
%><%
	int users = 1;
	if (firstResume != null)
		users = firstResume.keySet().size();
	int sortCol = 2;
	if (users > 1) sortCol = users+1;
  %>
 <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"aaSorting": [[ <%=sortCol%>, "asc" ]],
					"aoColumns": [
								null,
								null,
	<% 
		for (int i = 0; firstResume != null && i <firstResume.keySet().size() && firstResume.keySet().size() > 1; i++) 
		{
	%>
					null,
	<% } %>
								{ "bSortable": false }
								]
					});
			} );
			
	</script>
<br>
<center><h1>Resume Scores for Job Posting: <%=posting.getPostingId()%> <% if (false && owner) { %> <input type=button value="Delete Posting" onclick="deletePosting(<%=posting.getId()%>)"><% } %>
</h1>
<% if (firstResume == null) { %>
<center><b>No scores found</b></center>
<script>alert("No scores found"); rerunSession();</script>
<%	} else { %>
<br>
<form name=resumeListForm>
<table cellpadding="0" cellspacing="0" border="0"  class="display" id="example">
	<thead>
	<tr><th align=center >Resume File Name</th>
<%
		int numusers = firstResume.keySet().size();
		for (String userName: firstResume.keySet())
		{
			JobPostingSession jps = new JobPostingSession(userName, posting.getId());
			String directory = ResumeSorter.getUploadDir(userName);
			String name = userName;
			UserDataProvider userProvider = UserDataProviderPool.getInstance().acquireProvider();
			try
			{
				User reviewer = userProvider.getUser(userName);
				if (reviewer != null)
					name = reviewer.getName();
			}
			catch (Exception x) {
			}
			finally {
				UserDataProviderPool.getInstance().releaseProvider();
			}
			//if (userName.equals(user.getLogin())) userName = "By You";
			System.out.println("User:" + userName + " Directory:" + directory);
			String buttontext = "View";
			if (numusers == 1)
			{
				name = "Resume Scores";
				buttontext = "Highlighted Resumes";
			}
%>
		<th><%=name%>&nbsp;&nbsp;&nbsp;
		<% if (!jobinprogress && jps.exists(directory)) { %>
		<a onclick="javascript:restoreSession('<%=userName%>')" class="smallBtn"><%=buttontext%></a>
		<% } else if (!jobinprogress && userName.equals(user.getLogin()) && posting.getResumeFolder() != null) { %>
		<a onclick="javascript:rerunSession()" class="smallBtn">Rerun Scores</a>
		<% } %>
		</th>
<%		} %>
<%		if (numusers > 1) { %>
		<th>Overall Scores</th>
<%		} %>
		<th >&nbsp;</th>
</tr>
	</thead>
	<tbody>
<%
	int num = firstResume.keySet().size();
	DecimalFormat decformat2 = new DecimalFormat("00000.00");
	for (String resume : resumeScores.keySet())
	{
		Map<String, ResumeScore> userScores = resumeScores.get(resume);
		double avgscore = 0;
		double avgrank = 0;
		Map<String,Double> mscores = new TreeMap<String, Double>();
		for (String userName: userScores.keySet())
		{
			if (userName.equals("zzzAverage")) continue;
			ResumeScore score = userScores.get(userName);
			avgscore = avgscore + score.getScore();
			if (score.getScore() == -999)
				score.setRank(999);
			avgrank = avgrank + score.getRank();
			mscores.put(decformat2.format(score.getScore())+ "_" + userName, score.getScore());
		} 
		avgscore = avgscore/num;
		avgrank = avgrank/num;
		int i = 0;
		int mid1 = num/2;
		int mid2 = 0;
		if (num%2 == 0)
			mid2 = mid1+1;
		else
			mid1 = mid1+1;
		double median = 0;
		for (String mscore: mscores.keySet())
		{
			i++;
			if (i == mid1)
				median = mscores.get(mscore);
			if (i == mid2)
				median = (median + mscores.get(mscore))/2;
		} 

		ResumeScore score = new ResumeScore();
		score.setScore(avgscore);
		score.setScore(median);
		score.setScore(avgrank);
		score.setRank((int)avgrank);
		userScores.put("zzzAverage",score);
	}
	TreeMap<String, Map<String, ResumeScore>> sortedScores = new TreeMap<String, Map<String,ResumeScore>>();
	for (String resume : resumeScores.keySet())
	{
		Map<String, ResumeScore> userScores = resumeScores.get(resume);
		ResumeScore score = userScores.get("zzzAverage");
		sortedScores.put(decformat2.format(score.getScore())+ "_" + resume, userScores);
	}
	System.out.println("Sorted scores:" + sortedScores.keySet());
	int index = 0;
	DecimalFormat decformat = new DecimalFormat("########0.##");
	List<File> resumes = new ArrayList<File>();
	String format = "0";
	if (sortedScores.keySet().size() > 9) format = "00";
	if (sortedScores.keySet().size() > 99) format = "000";
	if (sortedScores.keySet().size() > 999) format = "0000";
	//for (String resume : sortedScores.descendingKeySet())
	for (String resume : sortedScores.keySet())
	{
		Map<String, ResumeScore> userScores = sortedScores.get(resume);
		int underscore = resume.indexOf("_");
		if (underscore != -1) 
			resume = resume.substring(underscore+1);
		String resumeShort = resume;
		if (resumeShort.length() > 50) resumeShort = resumeShort.substring(0,47) + "...";
		String firstOne = firstResume.keySet().iterator().next();
		ResumeScore scoreOne = userScores.get(firstOne);
		//if (!user.isAdmin() && !posting.isPaid() || !scoreOne.isPaid())
		if (!user.isAdmin() && !posting.isPaid())
		{
%>
	<tr><td align=left>&#x2588;&#x2588;&#x2588;&#x2588;&#x2588;&#x2588;&#x2588;&#x2588;</td><td></td><td></td><td></td></tr>
<%
			continue;
		}
		else
		{
%>
	<tr><td align=left><!-- a href = "#" onclick="showResume(<%=index%>)" --> <%=resumeShort%></td>
<%
		}
		String resumeFolder = null;
		for (String userName: firstResume.keySet())
		{
			ResumeScore score = userScores.get(userName);
			if (numusers == 1 && userName.startsWith("zzz")) continue;

			if (score == null)
			{
%>
			<td class="center"> - </td>
<%			
			  continue;
			}
			int rank = score.getRank();
			int total = score.getTotal();
			if (resumeFolder == null)
				resumeFolder = score.getResumeFolder();
			if (userName.indexOf("Average") != -1)
			{
				rank = index+1;
				total = sortedScores.keySet().size();
			}
			String scoreStr = decformat.format(score.getScore());

			String rankStr = new DecimalFormat(format).format(rank);
			if (scoreStr.indexOf("999") != -1)
			{
				scoreStr = "Error";
				rankStr = "_";
			}
			//else if (!user.isAdmin() || userName.indexOf("Average") != -1)
			else if (userName.indexOf("Average") != -1)
				scoreStr = "";
			else
				scoreStr = "(" + scoreStr + ")";
			String tagsFound = score.getTagsFound();
			if (tagsFound == null) tagsFound = "";
			if (tagsFound.length() > 0) tagsFound = "(<font color='red'>" + tagsFound + "</font>)";
			int userScore = score.getUserScore();

%>
		<td class="center"><%=rankStr%> / <%=total%> <%=scoreStr%> <%=tagsFound%>
		<% for (int k = 0; k < userScore; k++) { %>
		<img src="star_on.gif">
		<% } %>
		</td>
<%		}
%>
		<td class="center">  <img src="x.png" onclick='deleteScore("<%=resume%>")'/></td>
	</tr>
<%
		index++;
		String folder = "";
		if (resumeFolder != null && resumeFolder.length() > 0) folder = File.separator + resumeFolder;
        File resumeFile = PlatformProperties.getInstance().getResourceFile(
                                    userdir + File.separator + "MyResumes" + folder + File.separator + resume);
		resumes.add(resumeFile);
		//if (index >= 10) continue;
	}
    //session.setAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString(), resumes);                    
%>
<!--tr><td align=center colspan=2><br><input type=button onclick="sendResumes()" value="Email Selected Resumes"></td></tr-->
	</tbody>
</table>

</form>
<%	} %>
<br>
<br>
<br>
</center>
</div>
</div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>