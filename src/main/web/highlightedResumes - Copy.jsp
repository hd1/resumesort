<%@include file="rq_header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<script language="JavaScript" type="text/JavaScript" src="prototype.js"></script>
<%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
	if (user == null) return;
	String userdir = ResumeSorter.getUploadDir(user);
    session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString(), userdir);
	String postingId = request.getParameter("postingId");
	List<JobPosting> postings = new JobPosting().getObjects("user_name=" + UserDBAccess.toSQL(user.getLogin()) + " order by posting_id");
	List<SharedJobPosting> sjps =  new SharedJobPosting().getObjects("perm3 = 1 and IDUSER = " + user.getId());
	JobPosting posting = null;
	for (JobPosting jp: postings)
	{
		if (String.valueOf(jp.getId()).equals(postingId))
		{
			posting = jp;
			break;
		}
	}
	for (SharedJobPosting sjp: sjps)
	{
		List<JobPosting> postings2 = new JobPosting().getObjects("ID =" + sjp.getJobId());
		postings.addAll(postings2);
		if (posting == null)
		{
			for (JobPosting jp: postings2)
			{
				if (String.valueOf(jp.getId()).equals(postingId))
				{
					posting = jp;
					userdir = userdir.replace(user.getLogin(), jp.getUserName());
					break;
				}
			}
		}
	}
	if (posting == null && postingId != null )
	{
	%>
		<script>alert("Job Posting: <%=postingId%> not found")</script>;
	<%
	}
	if (posting == null && postings.size() > 0)
	{
		posting = postings.get(0);
	}
	if (posting == null)
	{
	%>
			<script>alert("No Job Postings found");
	<%
		return;
	}
	List<ResumeScore> scores = new ResumeScore().getObjects("JOB_ID=" + posting.getId());
	Map<String, Map<String, ResumeScore>> resumeScores = new TreeMap<String, Map<String,ResumeScore>>();
	Map<String, ResumeScore> firstResume = null;
	for (ResumeScore score : scores)
	{
		String resume = score.getResumeFile();
		Map<String, ResumeScore> userScores = resumeScores.get(resume);
		if (userScores == null)
		{
			userScores = new TreeMap<String, ResumeScore>();
			if (firstResume == null)
			{
				firstResume =  userScores;
			}
			resumeScores.put(resume, userScores);
		}
		String userName = score.getUserName();
		userScores.put(userName, score); 
	}
	String error = request.getParameter("error");
	if (error != null && error.length() > 0)
	{
%> <script>alert("<%=error%>");</script> <%
	}
	String defValue = "Enter Email Address";
%>
<script>
	function showResume(ind)
	{
		var link = "ResumeViewer?viewFileIndex=" + ind;
		var newwin = window.open(link, "HighLightedResume");
		newwin.focus();
	}
	var busy = false;
	function sendResumes()
	{
		if (busy)
		{
			alert("Function in progress, please wait...")
			return;
		}
		var elems = document.resumeListForm.elements;
		var selected = false;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name == "emailResumeIdx" && elems[i].checked)
			{
				selected = true;
			}
		}
		if (!selected)
		{
			alert("Please select some resumes")
			return;
		}
		busy = true;
		var url = "email_resumes.jsp?random=" + new Date();
		var form = document.resumeListForm;
		new Ajax.Request(url, { 
			method:'post',
			parameters : Form.serialize(form),
			onSuccess: function(transport){
				
				var response = transport.responseText;
				// ignore response
				if (response != null)
					alert("Email has been sent");
				else
					alert("Error sending email");
				busy = false;
			}
		});
	}
	function selectAll(obj)
	{
		var selcbx = document.getElementById("selall");
		var elems = document.resumeListForm.elements;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name == "emailResumeIdx")
			{
				if (selcbx.checked)
				{
					elems[i].checked = true;
				}
				else
				{
					elems[i].checked = false;
				}
			}
		}
	}

	function clearDefault(inp)
	{
		if (inp.value == '<%=defValue%>')
		{
			inp.value = '';
		}
	}

	function showPosting(obj)
	{
		window.location = "job_posting_report.jsp?postingId=" + obj.options[obj.selectedIndex].value;
	}
</script>
<br>
<h1>Select Posting:
<select name=jobposting onchange="showPosting(this)">
<%
	for (int i = 0; i < postings.size(); i++)
	{
		String selected = "";
		if (posting.getId() == postings.get(i).getId())
			selected = "selected";
%>
<option value="<%=postings.get(i).getId()%>" <%=selected%>><%=postings.get(i).getPostingId()%></option>
<%	} %>
</select>
</h1>
<br>
<br>
<center><h1>Resume Scores for Job Posting: <%=posting.getPostingId()%></h1>
<% if (firstResume == null) { %>
<center><b>No scores found</b></center>
<%	} else { %>
<br>
<form name=resumeListForm>
<table class=scorestbl>
<tr><th align=center rowspan=2><!-- input id="selall" type="checkbox" onclick="selectAll(this)"--></th><th align=center rowspan=2>Resume File Name</th>
<%
		for (String userName: firstResume.keySet())
		{
			if (userName.equals(user.getLogin())) userName = "By You";
%>
		<th colspan=2><%=userName%></th>
<%		} %>
		<th colspan=2>Average</th>
</tr>
<tr>
<%
		for (String userName: firstResume.keySet())
		{
%>
		<th align=center>Score</th><th align=center>Rank</th>
<%		} %>
		<th align=center>Score</th><th align=center>Rank</th>
</tr>
<%
	int index = 0;
	DecimalFormat decformat = new DecimalFormat("########0.00");
	List<File> resumes = new ArrayList<File>();
	for (String resume : resumeScores.keySet())
	{
		Map<String, ResumeScore> userScores = resumeScores.get(resume);
%>
	<tr><td align=center><!-- input type=checkbox value="<%=index%>" name="emailResumeIdx" --></td><td align=left><a href = "#" onclick="showResume(<%=index%>)"> <%=resume%> </td>
<%
		double avgscore = 0;
		double avgrank = 0;
		for (String userName: userScores.keySet())
		{
			ResumeScore score = userScores.get(userName);
			avgscore = avgscore + score.getScore();
			avgrank = avgrank + score.getRank();
%>
		<td><%=decformat.format(score.getScore())%></td><td><%=score.getRank()%> / <%=score.getTotal()%></td>
<%		} 
		avgscore = avgscore/firstResume.keySet().size();
		avgrank = avgrank/firstResume.keySet().size();
%>
		<td><%=decformat.format(avgscore)%></td><td><%=decformat.format(avgrank)%></td>
	</tr>
<%
		index++;
        File resumeFile = PlatformProperties.getInstance().getResourceFile(
                                    userdir + File.separator + "MyResumes" + File.separator + resume);
		resumes.add(resumeFile);
	}
    session.setAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString(), resumes);                    
%>
<!--tr><td align=center colspan=2><br><input type=button onclick="sendResumes()" value="Email Selected Resumes"></td></tr-->
</table>

</form>
<%	} %>
<br>
<br>
<br>
<a href="rq_uploader.jsp">Go back to the upload page</a></b></font>

</center>
<%@include file="rq_footer.jsp"%>
</body>
</html>