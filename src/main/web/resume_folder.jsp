<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%
	if (user == null)
	{
		response.sendRedirect("");
		return;
	}
	String userdir = ResumeSorter.getUploadDir(user);
    session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString(), userdir);
	String resumeFolderName = request.getParameter("resumeFolderName");
    File resumeFolder = PlatformProperties.getInstance().getResourceFile(userdir + File.separator + ResumeSorter.RESUME_FOLDER + "/" + resumeFolderName);
	if ("deleteResume".equals(request.getParameter("command")) && resumeFolderName != null)
	{
		String resume = request.getParameter("resume");
		if (resume != null && resume.trim().length() > 0)
		{
			new ResumeScore().deleteObjects("resume_folder = " + UserDBAccess.toSQL(resumeFolderName) +  " and resume_file = " + UserDBAccess.toSQL(resume));
			File resumeFile = PlatformProperties.getInstance().getResourceFile(userdir + File.separator + ResumeSorter.RESUME_FOLDER + "/" + resumeFolderName + "/" + resume);
			if (resumeFile.exists())
			{
				resumeFile.delete();
				resumeFile = PlatformProperties.getInstance().getResourceFile(userdir + File.separator + ResumeSorter.RESUME_FOLDER + "/" + resumeFolderName + "/" + resume + ".Object");
				if (resumeFile.exists()) resumeFile.delete();
				resumeFile = PlatformProperties.getInstance().getResourceFile(userdir + File.separator + ResumeSorter.RESUME_FOLDER + "/" + resumeFolderName + "/" + resume + ".tikaObject");
				if (resumeFile.exists()) resumeFile.delete();
			}
		}
	}
	else if ("reset".equals(request.getParameter("command")) && resumeFolderName != null)
	{
		File[] resumes = resumeFolder.listFiles();
		for (File resume: resumes)
		{
			if (resume.getName().endsWith(".Object"))
				resume.delete();
			if (resume.getName().endsWith(".tikaObject"))
				resume.delete();
		}
	}
	File[] resumes = resumeFolder.listFiles();
	if (resumes == null)
	{
%>		<script>alert("Folder not found")</script> <%
		response.sendRedirect("resume_folder_list.jsp");
		return;
	}
%>
<script>
	function showFolder(obj)
	{
		window.location = "resume_folder.jsp?resumeFolderName=" + obj.options[obj.selectedIndex].value;
	}
	function resetFolder()
	{
		if (!confirm("Are you sure you wish delete all processed (.Object) files?"))
		{
			return;
		}
		window.location = "resume_folder.jsp?command=reset&resumeFolderName=<%=resumeFolderName%>";
	}
	function deleteResume(name)
	{
		if (!confirm("Are you sure you wish delete file: " + name + "?\n(This will delete it from all JobPostings)"))
		{
			return;
		}
		window.location = "resume_folder.jsp?command=deleteResume&resumeFolderName=<%=resumeFolderName%>&resume=" + escape(name);
	}
</script>
	<style type="text/css" title="currentStyle">
			@import "js/table/css/demo_page.css";
			@import "js/table/css/demo_table_jui.css";
			@import "js/table/ui-lightness/jquery-ui-1.8.4.custom.css";
		</style>
		<script type="text/javascript" language="javascript" src="js/table/js/jquery.dataTables.js"></script>
<link type="text/css" href="../css/ui/jquery-blue.css" rel="stylesheet" />	
<div class="middle">
<div class="maintext_dashboard">
<%
	String name = user.getName();// What kind of stupid css is this? It screws up without the spaces.
	if (name.length() < 6) name = name + "&nbsp;&nbsp;&nbsp;";
%>
 
 <div class="sky_bar">
 	<h3>Dashboard </h3>
    <p>Welcome back <%=name%> !</p>
    <a href="resume_folder_list.jsp" class="selected">RESUME FOLDERS</a>
    <a href="upload_jobposting.jsp">START NEW JOBS</a>
    <a href="home.jsp">PREVIOUS JOBS</a>
 </div>
 
 <div class="text_dashboard">
<%
	String error = request.getParameter("error");
	if (error != null && error.length() > 0)
	{
%> <span class="error"><%=error%></span><%
	}
%>
 
 <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"aaSorting": [[ 0, "asc" ]],
					"aoColumns": [
								null,
								null,
								null,
								{ "bSortable": false }
								]
				});
			} );
			
	</script>
	<center><h1>Resume Folder: <%=resumeFolderName%> 
<% if (user.isAdmin()) { %>
		&nbsp;&nbsp;<a onclick="javascript:resetFolder()" class="orangeBtn">Cleanup</a>
<% } %>
	</h1></center><br>

<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>Resume</th>
			<th>Size</th>
			<th>Date </th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<%
	for (int i = 0; i < resumes.length; i++)
	{
		if (resumes[i].isDirectory()) continue;
		if (!user.isAdmin() && (resumes[i].getName().endsWith("Object") || resumes[i].getName().startsWith(".") || resumes[i].getName().startsWith("_CONV"))) continue;
		long size = resumes[i].length();
		String path = resumes[i].getAbsolutePath();
		int ind = path.indexOf("uploads");
		path = path.substring(ind);
%>
  <tr style="font-weight:bold"  class="gradeC">
    <td><a href="downloadFile.jsp?download=true&fileName=<%=path%>" target=_blank><%=resumes[i].getName()%></a></td>
	<td align="right"><%=size%></td>
    <td class="center"><%=checkNull(new Date(resumes[i].lastModified()),"")%></td>
	<td class="center">  <img src="x.png" onclick='deleteResume("<%=resumes[i].getName()%>")'/></td>
  </tr>
<%	} %>
	</tbody>
</table>
 
 
<div class="clear"></div>
</div>
</div>
</div>


<%@include file="footer.jsp"%>
</body>
</html>
<%!
	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
	String checkNull(Object value, String defval)
	{
		if (value == null)
			return defval;
		else if (value instanceof Date)
			return dateformat.format(value);
		return value.toString();
	}
    int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>