/*
Author: Addam M. Driver
Date: 10/31/2006
*/

var sMax;	// Isthe maximum number of stars
var holder; // Is the holding pattern for clicked state
var preSet = new Array(); // Is the PreSet value onces a selection has been made
var rated = new Array();

// Rollover for image Stars //
function rating(num){
	var id = num.id;
	var prefix = id.substring(0,id.indexOf("_"));
	var ind = parseInt(prefix);
	var clear = document.getElementById("clear" + ind);
	if (clear != null)
	{
		if (clear.style.visibility == 'visible')
			return;
	}
	if (rated.length < ind)
	{
		for (var i=rated.length; i <= ind ; i++)
		{
			rated[i] = 0;
			preSet[i] = null;
		}
	}
	sMax = 0;	// Isthe maximum number of stars
	for(n=0; n<num.parentNode.childNodes.length; n++){
		if(num.parentNode.childNodes[n].nodeName == "A"){
			sMax++;	
		}
	}
	
	if(!rated[ind]){
		s = num.id.replace(prefix + "_", ''); // Get the selected star
		a = 0;
		for(i=1; i<=sMax; i++){		
			if(i<=s){
				document.getElementById(prefix + "_"+i).className = "on";
				//document.getElementById("rateStatus").innerHTML = num.title;	
				holder = a+1;
				a++;
			}else{
				document.getElementById(prefix + "_"+i).className = "";
			}
		}
	}
}

// For when you roll out of the the whole thing //
function off(me){
	var id = me.id;
	var prefix = id.substring(0,id.indexOf("_"));
	var ind = parseInt(prefix);
	if(!rated[ind]){
		if(preSet.length <= ind || !preSet[ind]){	
			for(i=1; i<=sMax; i++){		
				document.getElementById(prefix + "_"+i).className = "";
				//document.getElementById("rateStatus").innerHTML = me.parentNode.title;
			}
		}else{
			rating(preSet[ind]);
			//document.getElementById("rateStatus").innerHTML = document.getElementById("ratingSaved").innerHTML;
		}
	}
}

// When you actually rate something //
function rateIt(me){
	var id = me.id;
	var prefix = id.substring(0,id.indexOf("_"));
	var value = id.substring(id.indexOf("_")+1);
	var ind = parseInt(prefix);
	if(!rated[ind]){
		//document.getElementById("rateStatus").innerHTML = document.getElementById("ratingSaved").innerHTML + " :: "+me.title;
		preSet[ind] = me;
		rated[ind]=1;
		sendRate(me);
		rating(me);
		var starcount = document.getElementById("rating_" + ind);
		if (starcount != null) starcount.value = value;
		var checkbox = document.getElementById("suggphrases_" + ind);
		if (checkbox != null) checkbox.checked = true;
	}
	var clear = document.getElementById("clear" + ind);
	if (clear != null)
	{
		clear.style.visibility='visible';
	}
}

// Send the rating information somewhere using Ajax or something like that.
function sendRate(sel){
	//alert("Your rating was: "+sel.title);
}

function clearRating(ind)
{
	if (rated.length >= ind)
	{
		rated[ind] = 0;
		preSet[ind] = null;
		for ( var i = 1; i <= 5; i++)
		{
			var star = document.getElementById("" + ind + "_" + i);
			if (star != null)
			{
				off(star);
			}
		}
		var starcount = document.getElementById("rating_" + ind);
		if (starcount != null) starcount.value = "0";
	}
}
function setRating(ind)
{
	for ( var i = 1; i <= 5; i++)
	{
		var star = document.getElementById("" + ind + "_" + i);
		if (star != null)
		{
			if (i <=3)
				star.className = "on";
			else
				star.className = "";
		}
	}
	var star = document.getElementById("" + ind + "_" + 3);
	rateIt(star);
}

