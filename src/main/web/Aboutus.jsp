<%@include file="rq_header.jsp"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>

<div class="central">
<div class="about">
<br>
<p>ResumeTuner<sup><small>TM</small></sup> is the first product of Textnomics<sup><small>TM</small></sup> Inc, an advanced data mining, machine learning and natural language processing company which originates from NASA Ames Research Centre. Textnomics<sup><small>TM</small></sup> aims to be the #1 online site for job seekers, recruiters and employers, creating a one-stop shop to serve the careers market across the board. Driven by the belief that every individual deserves job satisfaction and every company should have access to the most qualified applicants possible, ResumeTuner<sup><small>TM</small></sup> mutually benefits job seekers and employers. 
<p>
Now that thousands of job seekers are applying to jobs online, the majority of employers have turned to Applicant Tracking Systems (ATS) to filter through the overwhelming number of applicants to only select resumes containing essential keywords. 
<p>
The #1 recommendation of career coaches is to change your resume for each specific job posting, but this process can be cumbersome and error-prone; ResumeTuner<sup><small>TM</small></sup> is the first and only automated resume-editing service that helps you with this process in a highly efficient and affordable manner. By enabling candidates to tailor their resumes to each specific job posting, ResumeTuner<sup><small>TM</small></sup> synchronizes the communication between job seekers and employers. This allows the most qualified candidates to properly differentiate themselves and ensures that YOUR resume won't get lost in the pile. 
<p>
Try ResumeTuner<sup><small>TM</small></sup> today, and put YOUR career on top.
<br>
<br>
</div>
</div>
<%@include file="rq_footer.jsp"%>
  
</body>
</html>