/*
filedrag.js - HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
*/
(function() {

	// getElementById
	function $id(id) {
		return document.getElementById(id);
	}


	// output information
	function Output(msg) {
		var m = $id("messages");
		m.innerHTML = msg + m.innerHTML;
	}


	// file drag hover
	function FileDragHover(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "hover" : "");
	}


	// file selection
	function FileSelectHandler(e) {

		// cancel event and hover styling
		FileDragHover(e);

		// fetch FileList object
		var files = e.target.files || e.dataTransfer.files;

		// process all File objects
		uploadBusy = true;
		for (var i = 0, f; f = files[i]; i++) {
			disableFolderChange();
			ParseFile(f);
			UploadFile(f);
		}
		uploadBusy = false;
	}

	function disableFolderChange()
	{
		dragUpload = true;
		var folder = $id("ResumeFolder");
		if (folder != null) folder.disabled = true;
		var prevfolder = $id("PrevFolder");
		if (prevfolder != null) prevfolder.disabled = true;
		if (document.getElementById("radioAddPrevFolder") != null)
			document.getElementById("radioAddPrevFolder").disabled = true;
		if (document.getElementById("radioSelPrevFolder") != null)
			document.getElementById("radioSelPrevFolder").disabled = true;
		if (document.getElementById("radioNewFolder") != null)
			document.getElementById("radioNewFolder").disabled = true;
	}

	// output file information
	function ParseFile(file) {

		//Output(
		//	"<p>File information: <strong>" + file.name +
		//	"</strong> type: <strong>" + file.type +
		//	"</strong> size: <strong>" + file.size +
		//	"</strong> bytes</p>"
		//);

		// display an image
		if (false && file.type.indexOf("image") == 0) {
			var reader = new FileReader();
			reader.onload = function(e) {
				Output(
					"<p><strong>" + file.name + ":</strong><br />" +
					'<img src="' + e.target.result + '" /></p>'
				);
			}
			reader.readAsDataURL(file);
		}

		// display text
		if (false && file.type.indexOf("text") == 0) {
			var reader = new FileReader();
			reader.onload = function(e) {
				Output(
					"<p><strong>" + file.name + ":</strong></p><pre>" +
					e.target.result.replace(/</g, "&lt;").replace(/>/g, "&gt;") +
					"</pre>"
				);
			}
			reader.readAsText(file);
		}

	}


	// upload JPEG files
	var uploading = false;
	var warning = true;
	function UploadFile(file) {

		// following line is not necessary: prevents running on SitePoint servers
		if (location.host.indexOf("sitepointstatic") >= 0) return
		if (numfilesUploaded >= 200)
		{
			if (warning)
				alert("You can upload a maximum of 200 files only");
			warning = false;
			return;
		}
		uploading = true;
		var xhr = new XMLHttpRequest();
		if (xhr.upload && (file.size <= $id("MAX_FILE_SIZE").value || file.name.indexOf("zip") != -1) && file.size <= $id("MAX_ZIP_SIZE").value) {

			// create progress bar
			var o = $id("progress");
			//o.innerHTML = "<p><b>Uploaded</b> " + file.name + ", <b>" + file.type + "</b>:" + file.size + " bytes</p>";
			var progress = o.appendChild(document.createElement("p"));
			var s1 = document.createElement("span");
			var t1 = document.createTextNode(" Uploaded ");
			s1.className = "boldtext";
			s1.appendChild(t1);
			var t2 = document.createTextNode(file.name + ", ");
			var s3 = document.createElement("span");
			var t3 = document.createTextNode(file.type);
			s3.className = "boldtext";
			s3.appendChild(t3);
			var t4 = document.createTextNode(", " + file.size + " bytes");
			progress.appendChild(s1);
			progress.appendChild(t2);
			progress.appendChild(s3);
			progress.appendChild(t4);


			// progress bar
			xhr.upload.addEventListener("progress", function(e) {
				var pc = parseInt(100 - (e.loaded / e.total * 100));
				progress.style.backgroundPosition = pc + "% 0";
			}, false);

			// file received/failed
			xhr.onreadystatechange = function(e) {
				if (xhr.readyState == 4) {
					progress.className = (xhr.status == 200 ? "success" : "failure");
					uploading = false;
				}
			};

			// start upload
			var url = $id("upload").action;
			var folder = $id("ResumeFolder");
			if (folder != null)
			{
				if (url.indexOf("?") ==-1)
					url = url + "?ResumeFolder=" + escape(folder.value);
				else
					url = url + "&ResumeFolder=" + escape(folder.value);
			}
			//alert(url);
			xhr.open("POST", url, true);
			//xhr.setRequestHeader("X_FILENAME", file.name);
			//xhr.send(file);
			var formData = new FormData();
			formData.append("file", file);
			xhr.send(formData);
			numfilesUploaded++;
		}
		else
			alert("File " + file.name + " is too large.");

	}


	// initialize
	function Init() {

		var fileselect = $id("fileselect"),
			filedrag = $id("filedrag"),
			submitbutton = $id("submitbutton");

		// file select
		fileselect.addEventListener("change", FileSelectHandler, false);

		// is XHR2 available?
		var xhr = new XMLHttpRequest();
		if (xhr.upload) {
			// file drop
			filedrag.addEventListener("dragover", FileDragHover, false);
			filedrag.addEventListener("dragleave", FileDragHover, false);
			filedrag.addEventListener("drop", FileSelectHandler, false);
			filedrag.style.display = "block";

			// remove submit button
			submitbutton.style.display = "none";
		}

	}

	// call initialization file
	if (window.File && window.FileList && window.FileReader) {
		Init();
	}


})();