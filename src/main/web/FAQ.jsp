<%@include file="rq_header.jsp"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<script>
	function showhide(num)
	{
		var faqdiv = document.getElementById("faq"+num);
		if (faqdiv != null && faqdiv.style.visibility == "visible")
		{
			faqdiv.style.visibility = "hidden";
			faqdiv.style.display = 'none';
		}
		else if (faqdiv != null)
		{
			faqdiv.style.visibility = "visible";
			faqdiv.style.display = 'inline';
		}
	}
</script>
<div class="central">
<br/>
<br/>
<center><font size=+1><strong>Frequently Asked Questions</strong></font></center>
<br/>
<ul style="list-style-type: disc;">
<li>
<a href="javascript:showhide(1)"><strong>What does Textnomics<sup><small>TM</small></sup> mean?</strong></a>
<div id="faq1" style="visibility:hidden;display:none"><br/><br/>The word Textnomics<sup><small>TM</small></sup> comes from the words �text� and �nomic�, which is the Greek word for the systematized knowledge, arrangement, or organization of something.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(2)"><strong>What does ResumeTuner<sup><small>TM</small></sup> do?</strong></a>
<div id="faq2" style="visibility:hidden;display:none"><br/><br/>  
ResumeTuner<sup><small>TM</small></sup> is a web-based program that allows job seekers to tailor their resumes to each individual job posting by incorporating relevant phrases from the job Ad, in a highly efficient, affordable, and user-friendly manner.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(3)"><strong>What kinds of job formats can I upload my resume in?</strong></a>
<div id="faq3" style="visibility:hidden;display:none"><br/><br/>You can upload your resume in the following formats: .doc, .docx, .rtf, .odt and .txt*
<br/><br/>*Please avoid MSWord tables as content in your resume for now. We are currently fixing this issue.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(4)"><strong>Can I upload more than one file?</strong></a>
<div id="faq4" style="visibility:hidden;display:none"><br/><br/>Yes. You can upload as many as 5 different files and we will store them for you.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(5)"><strong>How can I add the job posting?</strong></a>
<div id="faq5" style="visibility:hidden;display:none"><br/><br/>You can copy and paste any job posting from a job board as simple text.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(6)"><strong>Do I need to finish editing my resume at one time?</strong></a>
<div id="faq6" style="visibility:hidden;display:none"><br/><br/>Yes. You need to finish the process completely in order to get your tuned resume emailed to you.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(7)"><strong>Can I save it and go back later?</strong></a>
<div id="faq7" style="visibility:hidden;display:none"><br/><br/>No. We currently don�t have a save feature but are working on it. Make sure you save all tuned resumes to your computer or personal storage device.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(8)"><strong>How much time do I need to tune my resume?</strong></a>
<div id="faq8" style="visibility:hidden;display:none"><br/><br/>An average resume can take from 5-15 minutes to tune.  There is a small learning curve.  But after two or three times, most users take only a few minutes tuning, especially when using the TurboTuner<sup><small>TM</small></sup> feature which prescreens the key phrases from each job Ad.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(9)"><strong>How soon do I receive my tuned resume?</strong></a>
<div id="faq9" style="visibility:hidden;display:none"><br/><br/>You will receive your tuned resume by email in the next 5 minutes.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(10)"><strong>Can I use ResumeTuner<sup><small>TM</small></sup> to tune a cover letter?</strong></a>
<div id="faq10" style="visibility:hidden;display:none"><br/><br/>Yes. You can tune a cover letter the same way you tuned your resume. Just add the job posting you are applying to.  We also recommend you use the additional key phrase in Job Ad that we email you, in your cover letter.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(11)"><strong>Do you take care of format, design, layout?</strong></a>
<div id="faq11" style="visibility:hidden;display:none"><br/><br/>Yes. We take care of grammar and formatting but you will need to take care of the images and tables and page breaks.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(12)"><strong>Do you spell check my resume before sending it back?</strong></a>
<div id="faq12" style="visibility:hidden;display:none"><br/><br/>No. ResumeTuner<sup><small>TM</small></sup> but almost every browser available will automatically do the spellcheck. 
<br/><br/></div></li>
<li>
<a href="javascript:showhide(13)"><strong>Can I use Acronyms in my resume?</strong></a>
<div id="faq13" style="visibility:hidden;display:none"><br/><br/>Yes. ResumeTuner<sup><small>TM</small></sup> can detect and understand many acronyms.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(14)"><strong>What web browsers can I use?</strong></a>
<div id="faq14" style="visibility:hidden;display:none"><br/><br/>Any browser on PCs or Macs will work, with the exception of Safari on Mac computers. This issue is under construction.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(15)"><strong>How can I contact the ResumeTuner<sup><small>TM</small></sup> team?</strong></a>
<div id="faq15" style="visibility:hidden;display:none"><br/><br/>You can leave us a comment in our contact page, or send us an email on contact@resumetuner.com.
<br/><br/></div></li>
<li>
<a href="javascript:showhide(16)"><strong>I was in the middle of tuning my resume but the site crashed. Do I need to start over?</strong></a>
<div id="faq16" style="visibility:hidden;display:none"><br/><br/>Yes. We are in our Alpta<sup><small>TM</small></sup> testing phase and you might come across technical difficulties. Please bear with us and send us an email with the specific details if this happens. We will try to fix each issue ASAP.
<br/><br/></div></li>
</div>
<%@include file="rq_footer.jsp"%>
  
</body>
</html>