<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.mail.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.openoffice.*"%>
<%

		String[] indexes = request.getParameterValues("emailResumeIdx");
		System.out.println("Entered email_resumes:" + indexes);
		if (indexes == null || indexes.length == 0) return;
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (user == null) 
		{
%>
Error: Your session has expired.
<%
			return;
		}
		List<File> resumeFiles = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
        List<File> highlightedFiles = (List<File>)  session.getAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString());
		List<Map<Phrase,Integer>> matchedPhraseList = (List<Map<Phrase,Integer>>) session.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
		List<Boolean> processedByTika = (List<Boolean>) session.getAttribute(SESSION_ATTRIBUTE.PROCESSED_BY_TIKA.toString());
		long start = System.currentTimeMillis();
  		List<File> filesToSend = new ArrayList<File>();
		for (int i = 0; i < indexes.length; i++)
		{
			int index = getInt(indexes[i]);
			if (index != 0)
			{
				if (matchedPhraseList != null) // This is null if resumes have not beem analyzed/matched
				{
					if (processedByTika.get(index-1)) // If processed by tika, we need to redo using open office
					{
						ResumeProcessingThread rthread = new ResumeProcessingThread(resumeFiles.get(index-1), session, index-1, true);
						rthread.run();
					}
					File highlightedDoc = new HighlightedDocGenerator(request, index-1).processResume();
					filesToSend.add(highlightedDoc);
					if (highlightedFiles != null) highlightedFiles.set(index-1, highlightedDoc);
				}
				else
				{
					filesToSend.add(resumeFiles.get(index-1));
				}
			}
		}
		if (filesToSend.size() == 0) return;

		String uploadDir = (String) session.getAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString());
		File outputFile = PlatformProperties.getInstance().getResourceFile(uploadDir + File.separator + "HIGHLIGHTED_" + "results-ADD_DOT-ZIP");
		if (filesToSend.size() > 1)
			DocumentAnalyzer.zipFiles(filesToSend, outputFile);
		else
			outputFile = filesToSend.get(0);
		String email = (String) session.getAttribute(SESSION_ATTRIBUTE.EMAIL.toString());
		if (email == null) email = user.getEmail();
		String body = "Greetings from <a href='https://www.resumesort.com'>resumesort.com</a>:<br><br>";
		if (filesToSend.size() > 1)
			body = body + "Enclosed please find the highlighted resumes you requested in zip format.  After saving the file, please rename and add a '.zip' extension to the filename in order to process the files properly.   We did not add the '.zip' extension because some mail servers flag it as potentially dangerous and will not accept the email.<br><br>";
		else
			body = body + "Enclosed please find the highlighted resume that you requested.<br><br>";

		body = body + "Thank you for using resumesort.com.";

		GenericMailer.getInstance().sendMail(email, "Highlighted Resumes from Resume Sort", body, outputFile);
		long end = System.currentTimeMillis();
		System.out.println("" + filesToSend.size() + " Resume took " + (end - start) + " ms to mail the zip file");
        // Increment num of resumes emailed
		try
		{
			Integer num = user.getNumEmailed();
			if (num == null) num = 0;
			user.setNumEmailed(num + filesToSend.size());
			UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();
			userDataProvider.updateUserField(user, "numEmailed");
			userDataProvider.commit();
		}
		catch (Exception x)
		{
			x.printStackTrace();
		}
        finally {
            UserDataProviderPool.getInstance().releaseProvider();
        }
%>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>