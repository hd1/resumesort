<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="sun.misc.BASE64Decoder"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%!private final static String COOKIENAME = "rcwCookie"; %>
<%
	String code = request.getParameter("code");
	if (code == null) code = "";
	if ("true".equals(request.getParameter("logout")))
	{
			session.removeAttribute(SESSION_ATTRIBUTE.SUGGESTER.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.TARGET_FILE.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.REQUEST_STAMP.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.FILTER_TYPE.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.OPTIONS_LIST.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.HELP_LIST.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.EXTRAS_INDEX.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.LAST_SUGGESTION_INDEX.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.RANK_MAP.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.KEY_PHRASE_IDENTIFIER.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());	
	}
	else if (session != null)
	{
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		Integer numleft = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
		if (numleft == null) numleft = 0;
		List<File> resumeFiles = (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		List<Double> scores = (List<Double>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
		if (user != null && resumeFiles != null && scores != null)
		{
			int numProcessed = 0;
			int totalResumes = resumeFiles.size();
			for (int i = 0; i < scores.size(); i++)
				if (scores.get(i) != null) numProcessed++;
			// if still processing resumes
			if (numleft > 0)
			{
%>
<script>window.location="stillProcessing.jsp"</script>
<%
			}
			else
			{
%>
<script>window.location="ranked_resumes.jsp"</script>
<%
			}
			return;
		}
	}
	String error = request.getParameter("error");
	if (error != null && error.trim().length() > 0)
	{
%>	<script>alert("<%=error%>");</script> <%
	}
	String redirectURL = request.getParameter("redirectURL");
	if (redirectURL == null) redirectURL = "";
	String login = request.getParameter("login");
	if (login == null)
	{
		login=(String)request.getSession().getAttribute("login");
		if (login == null)
		{
			// Cookies
			Cookie cookies[] = request.getCookies();
			Cookie myCookie = null;
			boolean existsCookie = false;
			if (cookies != null)
			{
				for (int i = 0; i < cookies.length; i++)
				{
					if (cookies[i].getName().equals(COOKIENAME))
					{
						myCookie = cookies[i];
						existsCookie = true;
						break;
					}
				}
			}

			int cookieCount;
			// Cookie exists
			if (existsCookie)
			{
				try
				{
						BASE64Decoder decoder = new BASE64Decoder();
					String value = new String(decoder.decodeBuffer(myCookie.getValue()));
					String[] split = value.split(":");
					login = split[0];
					myCookie.setMaxAge(Integer.MAX_VALUE);
					response.addCookie(myCookie);
				}
				catch (Exception ex)
				{
					myCookie.setMaxAge(0);
					response.addCookie(myCookie);
					ex.printStackTrace();
				}
			}

		}
		request.getSession().removeAttribute("login");
	}
%>
<%@include file="header.jsp"%>

    <script type="text/javascript">
$(document).ready(function(){
	

	if (window.File && window.FileList && window.FileReader) {
		var xhr = new XMLHttpRequest();
		if (xhr.upload) {
			document.login.html5upload.value = "true";
		} else {
			document.login.html5upload.value = "false";
		}
	} else {
		document.login.html5upload.value = "false";
	}
	//alert(document.login.html5upload.value);

});
</script>
<script type="text/javascript" src="https://platform.linkedin.com/in.js">
	//api_key: hprez6o7wi7e
	//api_key: spktcpuor5s9
	api_key: pny9h114v0rg
</script>

<script type="text/javascript">
  // 2. Runs when the JavaScript framework is loaded

	var xmlhttp;
  
  function linkedInLogin()
  {
    IN.ENV.js.scope = new Array();
    IN.ENV.js.scope[0] = 'r_emailaddress';
    IN.User.authorize(linkedInLogin2);
  }
  function linkedInLogin2()
  {
			IN.API.Profile("me")
			.fields(["id", "firstName", "lastName", "pictureUrl","headline","emailAddress"])
			.result(function (result){
			 var profile = result.values[0];
				//alert(profile.firstName + " " + profile.lastName + " email=" + profile.emailAddress);		
				if (xmlhttp == null)
					xmlhttp = GetXmlHttpObject();
				if (xmlhttp != null) {
					var url = "linkedin_login.jsp";
					url = url + "?id=" + profile.id;
					url = url + "&name=" + profile.firstName + " " + profile.lastName;
					url = url + "&email=" + profile.emailAddress;
					url = url + "&code=" + "<%=code%>";
	
					xmlhttp.open("POST", url, false);
					xmlhttp.send(null);
					var resp = xmlhttp.responseText;
					if (resp.indexOf("newuser") != -1)
					{
						linkedinId = profile.id;
						linkedinName = profile.firstName + " " + profile.lastName;
                                                linkedinEmail = profile.emailAddress;
					}
					else if (resp == "")
					{
						window.location = "home.jsp"; // existing user, refresh to get file list
					}
					else
						alert(resp);

				}
			});
                }
		
		var linkedinId;
		var linkedinName;
		var linkedinEmail;
		function updateLinkedIn()
		{
				if (!validateEmail(document.getElementById("linkedinEmail").value))
				{
					alert("Please enter a valid email address.");
					return;
				}
				var url = "linkedin_login.jsp";
				url = url + "?id=" + linkedinId;
				url = url + "&name=" + linkedinName;
				url = url + "&email=" + linkedinEmail; //document.getElementById("linkedinEmail").value;
				xmlhttp.open("POST", url, false);
				xmlhttp.send(null);
				var resp = xmlhttp.responseText;
				if (resp.indexOf("newuser") != -1)
				{
					<%--alert(resp);--%>
				}
				else if (resp == "")
				{
					window.location = "home.jsp"; // existing user, refresh to get file list
				}
		}
		function validateEmail(email)
		{
			 var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs)\b/
			 return email.match(re)
		}
                		function GetXmlHttpObject() {
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				return new XMLHttpRequest();
			}
			if (window.ActiveXObject) {
				// code for IE6, IE5
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
			return null;
		}


 </script>

<div id="dialog" style="display:none">
	<span id=dialogtext></span>
	<input type=text id=linkedinEmail size=40>
	<div style="clear:both; margin-top:30px; float:right;">
	<a href="javascript:closeDialog()" class="orangeBtn">Cancel</a>
	<a href="javascript:updateLinkedIn()" class="orangeBtn">Ok</a>
	</div>
</div>    
<div class="middle">
<div class="maintext_dashboard">
<div class="text_dashboard">
 <div class="signin_left">
 	<h3>Sign In</h3><br />
    Sign in with Facebook or Twitter or LinkedIn or Google<br /><br />
	<div class="sociallogin">
		<div id="fb-root"></div>
		<span id="fblogin"></span>
	<!--a href="#"><img src="images/fb.jpg" width="150" height="22" alt="" /></a -->
	</div>
	<div class="sociallogin">
	<a href="#"><img src="images/ld.jpg" onclick="linkedInLogin()" width="150" height="22" alt="" /></a>
	</div>
	<!--div class="sociallogin">
	<a href="javascript:alert('Not available yet')"><img src="images/tw.jpg" width="150" height="22" alt="" /></a>
	</div-->
	<div class="sociallogin">
	<a href="javascript:google_login()"><img src="images/signinwithgoogle.jpg" width="150" height="22" alt="" /></a><br />
	<script type="text/javascript" src="js/jquery-1.5b1.js"></script>
	</div>	
	<div id="profile"></div>
	<hr class="hr"/><br />
	Or, with your resumeSort account<br /><br />
    <form name="login" method='post' action='Authenticator' id="login">
	Email *<br />
		<% if(login==null){%>
		<input type="text" name="login" maxlength="50" onkeypress="return checkenter(event)"/><br />
		<%}else{ %>
		<input type="text" name="login" value="<%out.write(login); %>" maxlength="50" onkeypress="return checkenter(event)"/><br />
		<%} %>
	Password<br />
	<input type="password" name="password" maxlength="20" onkeypress="return checkenter(event)"/><br /><br />
	<input type="checkbox" />Remember me <br />
	PROPRIETARY and CONFIDENTIAL: The information you are about to access is covered under NDA and not for distribution.<br />
	<input type="button" onclick="doSubmit()" value="SIGN IN" class="signin"/><br />
	<input type=hidden name=redirectURL value="<%=redirectURL%>">
	<input type=hidden name=html5upload value="true">
	</form>
	<a href="javascript:sendNewPassword()">Forgot your password?</a><br />
<% if (error != null && error.trim().length() > 0) { %>	
	<div class="error">FAILURE: <%=error%></div>
<% } %>
 </div>
 
 
 <div class="signin_right">
 
 	<div class="box">
    <h3>Not yet registered?</h3><br />
	<a href="signup.jsp?code=<%=code%>" class="reg">&nbsp;</a>
    </div>
 
 </div>
 
 
 
<div class="clear"></div>
</div>
</div>
    </div>
<script>
function doSubmit()
{
	var login = document.login.login.value
	if (login == "")
	{
		alert("Please enter your email address");
		return;
	}
	if (login.indexOf('@') == -1 && login != 'esfandiar' && login != 'devg')
	{
		alert("Please enter your complete email address (name@email.com)");
		return;
	}
	document.login.submit();
}

function checkenter(event, num)
{
	if (event == null)
		event = window.event;
	var keyCode = event.keyCode
	if (keyCode == null)
		keyCode = event.which;
	if(keyCode == 13)
	{
		doSubmit();
		return true;
	}
	return true;
}
function sendNewPassword()
{
	var login = document.login.login.value
	if (login == "")
	{
		alert("Please enter your email address");
		return;
	}
	if (login.indexOf('@') == -1 && login != 'esfandiar')
	{
		alert("Please enter your complete email address (name@email.com)");
		return;
	}

	window.location="SendPassword?login=" + document.login.login.value;
}
document.login.login.focus();

</script>
<% if (true) { %>
<script type="text/javascript" src="js/custom-form-elements.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>   
      <script src="https://connect.facebook.net/en_US/all.js"></script>
      <script>
		var usingFB = false;

		$(function() {					
			$('#dialog').dialog({
				autoOpen: false,
				width: 460			
			//,buttons: {
			//		'Yes': function(){
			//			$(this).dialog('close');
			//			callback(true);
			//		},
			//		'No': function(){
			//			$(this).dialog('close');
			//			callback(false);
			//		}
			//	}			
			});
		});

window.fbAsyncInit = function () {
		FB.init({ 
			appId:'236063299838716', cookie:true,
			channelUrl : '//'+window.location.hostname+'/signin.jsp',
			status:true, xfbml:true 
		});
        FB.Event.subscribe('auth.statusChange', function(response) {
          if (response.authResponse) {
			//alert("Session Change:" + response.authResponse);
			get_fb_userinfo();
		  }
		});

		FB.getLoginStatus(function(response) {
			// alert(response.status);
			if (response.status === 'connected') {
				//alert("Logged In");
				get_fb_userinfo();
				usingFB = true;
				//window.location = 'home.jsp';
				//document.getElementById('fblogin').innerHTML ='<a href="#" onclick="FB.logout();">FB Logout</a><br/>';
			} else {
				//alert("Not Logged In");
				document.getElementById('fblogin').innerHTML ='<fb:login-button perms="email">' + '</fb:login-button>';
				FB.XFBML.parse();
			}
		});
};
		function get_fb_userinfo() {
			FB.api('/me', function(response) {
				var userGraph = response;
				if (userGraph.email == null || userGraph.email == '')
				{
					document.getElementById('fblogin').innerHTML ='<fb:login-button data-width=150perms="email">' + '</fb:login-button>';
				}
				else
				{
					if (document.getElementById('fblogin') != null)
						document.getElementById('fblogin').innerHTML ='<a href="#" onclick="FB.logout();">FB Logout</a><br/>';
					if (document.getElementById('register') != null)
						document.getElementById('register').innerHTML ='';
					if (document.getElementById('changepw') != null)
						document.getElementById('changepw').innerHTML ='';
					if (document.getElementById('rtlogin') != null)
						document.getElementById('rtlogin').innerHTML ='';
					//alert("Name1:" + userGraph.name + " email1:" + userGraph.email);
					if (xmlhttp == null)
						xmlhttp = GetXmlHttpObject();
					if (xmlhttp != null) {
						var url = "facebook_login.jsp";
						url = url + "?email=" + userGraph.email;
						url = url + "&name=" + userGraph.name;
						xmlhttp.open("POST", url, false);
						xmlhttp.send(null);
						var resp = xmlhttp.responseText;
						if (resp == "")
						{
							window.location = "home.jsp"; // existing user, refresh to get file list
						}
					}
					//alert("Name2:" + userGraph.name + " email2:" + userGraph.email);
					//var emailInp = document.getElementById("emailResumeTo");
					//if (emailInp == null)
					//	emailInp = document.getElementById("email");
					//emailInp.value = userGraph.email;
				}
			  });
		}
		function GetXmlHttpObject() {
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				return new XMLHttpRequest();
			}
			if (window.ActiveXObject) {
				// code for IE6, IE5
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
			return null;
		}     
		function linkedInLoginX() 
                {
                    IN.ENV.js.scope = new Array();
    IN.ENV.js.scope[0] = 'r_emailaddress';
    IN.User.authorize();
                    
                                        IN.API.Profile("me")
			.fields(["id", "firstName", "lastName", "pictureUrl","headline","emailAddress"])
			.result(function(result) {
			 profile = result.values[0];
			  //profHTML = "<p><a href=\"" + profile.publicProfileUrl + "\">";
			  //profHTML += "<img class=img_border align=\"left\" src=\"" + profile.pictureUrl + "\"></a>";     
			  //profHTML += "<a href=\"" + profile.publicProfileUrl + "\">";
			  //profHTML += "<h2 class=myname>" + profile.firstName + " " + profile.lastName + "</a> </h2>";
			  //profHTML += "<span class=myheadline>" + profile.headline + "</span>";
			  //profHTML += "<span >" + profile.email + "</span>";
			  //$("#profile").html(profHTML);
			  //alert(profile.id);
				if (xmlhttp == null)
					xmlhttp = GetXmlHttpObject();
				if (xmlhttp != null) {
					var url = "linkedin_login.jsp";
					url = url + "?id=" + profile.id;
					url = url + "&name=" + profile.firstName + " " + profile.lastName;
                                        url = url + "&email=" + profile.emailAddress;
					xmlhttp.open("POST", url, false);
					xmlhttp.send(null);
					var resp = xmlhttp.responseText;
					if (resp.indexOf("newuser") != -1)
					{
						linkedinId = profile.id;
						linkedinName = profile.firstName + " " + profile.lastName;
                                                linkedinEmail = profile.emailAddress;
					//	document.getElementById("dialogtext").innerHTML = "Please enter your email address?";
					//	$('#dialog').dialog('open');
					}
					else if (resp == "")
					{
						window.location = "home.jsp"; // existing user, refresh to get file list
					}
				}
			});
		}
<%--		function closeDialog()
		{
			$('#dialog').dialog('close');
		}
--%>		
		var linkedinId;
		var linkedinName;
		var linkedinEmail;
		function updateLinkedIn()
		{
				if (!validateEmail(document.getElementById("linkedinEmail").value))
				{
					alert("Please enter a valid email address.");
					return;
				}
				var url = "linkedin_login.jsp";
				url = url + "?id=" + linkedinId;
				url = url + "&name=" + linkedinName;
				url = url + "&email=" + linkedinEmail; //document.getElementById("linkedinEmail").value;
				xmlhttp.open("POST", url, false);
				xmlhttp.send(null);
				var resp = xmlhttp.responseText;
				if (resp.indexOf("newuser") != -1)
				{
					alert(resp);
				}
				else if (resp == "")
				{
					window.location = "home.jsp"; // existing user, refresh to get file list
				}
		}
		function validateEmail(email) 
		{ 
			 var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs)\b/
			 return email.match(re) 
		}
			 
		function google_login()
		{
			window.location = "https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&state=%2Fprofile&redirect_uri=https%3A%2F%2Fwww.resumesort.com%2Foauth2callback&response_type=token&client_id=1022693856183.apps.googleusercontent.com";
		}
	</script>
<% } %>
<%@include file="footer.jsp"%>
</body>
</html>
