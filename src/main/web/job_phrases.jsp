<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.wordnet.model.WordNetDataProviderPool"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <!--[if lt IE 9]>
   <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="all.css" />
  <!--[if lt IE 9]>
  <link rel="stylesheet" href="ie.css" />
  <![endif]-->  
<link type="text/css" rel="stylesheet" href="css/phrases.css" />
<script src="phrases.js" type="text/javascript"></script>
<script type="text/javascript" src="ratingsys.js"></script> 
<script language="JavaScript" type="text/JavaScript" src="prototype.js"></script>
<script type="text/javascript" src="default.js"></script> 
<style>

.ttip {
	display: none;
	position: absolute;
	z-index: 3;
	border: solid 1px black;
	background-color: #DDFFFF;
	padding: 1;
	padding-left: 5;
	padding-right: 5;
	min-width: 300px;
}

.ttipBody {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 13px;
	padding: 0;
	margin: 0;
	max-height: 500px;
}

.rpop {
	display: none;
	z-index: 3;
	border: solid 1px black;
	background-color: #DDFFFF;
	padding: 1;
	padding-left: 5;
	padding-right: 5;
	min-width: 300px;
	max-height: 400px;
	overflow: auto;
	float: right;
}

.resumePhrases {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 13px;
	padding: 0;
	margin: 0;
	height: 400px;
    overflow: auto;
}

</style>
<!--[if IE]>
<style type="text/css">body {behavior: url(ifiles/iehoverfix.htc);}</style>
<![endif]-->
<link rel="stylesheet" href="ifiles/stylesheet.css" />
<script type="text/javascript" src="ifiles/javascript.js"></script>
</head>

<body>

<header id="header" class="central2">
  <h1 id="logo">Resume <strong>Sort</strong></h1>
  <nav id="user-nav"></nav>
</header>
<div id="siteTag">
  <h2 class="central2">The most advanced resume sorter on the planet</h2>
</div>
<%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
	if (user == null)
	{
		response.sendRedirect("Login.jsp");
		return;
	}
	// Clear scores from last run
	session.removeAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
	JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	if (posting == null)
	{
		response.sendRedirect("rq_uploader.jsp");
		return;
	}
	String command = request.getParameter("command");
	String sessStamp = request.getParameter("sessStamp");
	if ("saveSession".equals(command))
	{
		new UserPhraseSelection(request, response).save();
	}
	if ("deleteSession".equals(command))
	{
		new SortPhrase().deleteObjects("JOB_ID = " + posting.getId() + " and USER_NAME = " + DBAccess.toSQL(user.getLogin()) + " and saved_time=" + sessStamp);
	}
	boolean usestars = true;
	if ("false".equals(request.getParameter("usestars")))
		usestars = false;
	else if (request.getParameter("usestars") == null && user.isSimpleRating())
		usestars = false;
	boolean showPosting = true;
	if (!posting.getUserName().equals(user.getLogin()))
	{
		showPosting = false;
		SharedJobPosting sjp = (SharedJobPosting)session.getAttribute(SESSION_ATTRIBUTE.SHARED_POSTING.toString());
		if (sjp != null)
		{
			if (sjp.isPerm1() || sjp.isPerm3())
				showPosting = true;
		}
	}
	Collection<PhraseList> phraseList = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	Collection<PhraseList> nonLemmaPhraseLists = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString());
	DocumentRS sourceDocument = (DocumentRS) session.getAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString());
	Map<String,String> phrasesSynonyms = (Map<String,String>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());

	WordNetDataProviderPool.getInstance().acquireProvider();
	Map<String, String> contexts = ResumeReport.getPhrasesToContextMap(sourceDocument, nonLemmaPhraseLists);
	WordNetDataProviderPool.getInstance().releaseProvider();
	//System.out.println("Contexts Map:" + contexts);
	Collection<PhraseList> lemmaPhraseLists = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString());
	SortPhrase sp = new SortPhrase();
	List<Object[]> savedSessions = new UserDBAccess().getQueryResults("select distinct saved_time from " + sp.getDB_TABLE() + " where user_name =" + DBAccess.toSQL(user.getEmail()) + " and JOB_ID = " + posting.getId() + " order by saved_time desc");
	List<SortPhrase> prevPhrases = new ArrayList<SortPhrase>();
	int MAX_SAVED_SESSIONS = 10;
	if (savedSessions.size() > 0)
	{
		long last = (Long)savedSessions.get(0)[0];
		System.out.println("Num of sessions:" + savedSessions.size() + " Last:" + last);
		if ((sessStamp == null || "deleteSession".equals(command)) && !"restoreSession".equals(command)) sessStamp = "" + last;
		prevPhrases = sp.getObjects("JOB_ID = " + posting.getId() + " and USER_NAME = " + DBAccess.toSQL(user.getEmail()) + " and saved_time= " + sessStamp + " order by saved_time");
		for (int i = savedSessions.size()-1; i >= MAX_SAVED_SESSIONS; i--)
		{
			new SortPhrase().deleteObjects("JOB_ID = " + posting.getId() + " and USER_NAME = " + DBAccess.toSQL(user.getLogin()) + " and saved_time=" + savedSessions.get(i)[0]);
			savedSessions.remove(i--);
		}
	}
	String postingId = posting.getPostingId();
%>
<div class="central2">
<center><h1>Phrase Selection for Job Posting: <% if (showPosting) { %><a href="get_jobposting.jsp?download=true&jobpostingName=<%=posting.getPostingFile()%>"><% } %><%=postingId%></a></h1>
<% if (savedSessions.size() > 0) { %>
<h2>Saved Sessions:&nbsp;
<% 
	int j = 0;
	for (int i = savedSessions.size()-1; i >= 0; i--)
	{
		long stamp = (Long)savedSessions.get(i)[0];
		Date time = new Date(stamp);
		j++;
%>
	<a href="#" onclick="restoreSession(<%=stamp%>)" style="border:1px solid black;display:inline-block;padding:2px" title="<%=time%>"><%=j%></a><a href="#" onclick="deleteSession(<%=stamp%>,<%=j%>)" ><img src=x.png></a>&nbsp;
<% 
	}
%>
</h2>
<% } %>
<div id='tt' class="ttip"><table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>
<div id='popup' class="ttip"><table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>
<div id="logbox" style="border:solid 1px #00f;font-size:16px;height:20px;display:none">message bar for event logs</div>
<div id ="phrases">
<form method='post' action='Analyzer' name="phrasesform">
<input type=hidden name="usestars" value="<%=usestars%>"/>
   <div id="suggestedphrases">
	<b>Select phrases for sorting/scoring resumes <input type=button onclick="clearPostingPhrases()" value="Clear selection"></b>
	<Table id="suggestedphraselist" >
	<tr>
	<th align=center style="display:none">Clear</th>
	<th colspan=2>Phrases from Job Posting</th>
<% if (usestars) { %>
	<th>Importance</th>
	<th nowrap>Must Have</th>
	<th>Tag</th>
<% } else { %>
	<th nowrap align=center>Nice to<br>Have</th>
	<th nowrap align=center>Should<br>Have</th>
	<th nowrap align=center>Must<br>Have</th>
	<th>Tag</th>
	<th>&nbsp;</th>
<% } %>
	</tr>
<%
	int i = 0;
	for(PhraseList p : phraseList)
	{   
		//System.out.println("Phrase Buffer: " + p.iterator().next().getPhrase().getBuffer());
		//System.out.println("POS Sequence for phrase: " + p.iterator().next().getPhrase().getNgram().getPosSequence());
		String phrase = p.iterator().next().getPhrase().getBuffer().toString();
		String sentence = "";
		Iterator<PhraseList> iterator = nonLemmaPhraseLists.iterator();
		for(PhraseList lemmaPhraseList : lemmaPhraseLists)
		{
			PhraseList unlemma = iterator.next();
			String unlemmaText = unlemma.iterator().next().getPhrase().getBuffer();
			if (lemmaPhraseList.iterator().next().getPhrase().getBuffer().equalsIgnoreCase(phrase))
			{
				if (contexts != null)
					sentence = contexts.get(unlemmaText.toLowerCase().trim());
				if (sentence == null)
					sentence = contexts.get(phrase.toLowerCase().trim());
			//System.out.println("phrase:" + phrase + " unlemma:" + unlemmaText.toLowerCase().trim() + " sentence:" + sentence + "'");
			}
		}

		if (sentence == null) sentence = "";
		String srccount = "" + sentence.split("_LIMIT_").length;
		if (srccount.length() > 10) srccount = "10";
%>
	<tr class="kwdRecord" id="kwd_<%=100+i%>" onmouseover="helperActions(this, 'show')" onmouseout="helperActions(this, 'hide')">
	<td class="kwdTools">
		<input type="hidden" value="<%=phrase%>" id="suggphrases_<%=100+i%>" name="suggphrases_<%=100+i%>"/>
<%	if (showPosting) { %>
		<span class="iconTinyInfo" onmouseover='handleTooTip2(event, "<%=sentence.replace('\'', '`')%>");'></span>
<%	}
		if (phrasesSynonyms.get(phrase.trim()) != null)
		{
			String synonymsStr = phrasesSynonyms.get(phrase.trim()).trim();
			if (synonymsStr.length() > 1)
			{
				String[] synonymns = synonymsStr.split("\\|");
				synonymsStr = synonymsStr.replace("|","_LIMIT_");
				int syncnt = synonymns.length;
				if (syncnt > 9) syncnt = 10;
%>
	<span class="iconSynonyms" id="syn_<%=100+i%>" onclick='getSynonyms(event, "<%=phrase%>", this);synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%			} else { %>
	<span class="iconGSynonyms" id="syn_<%=100+i%>" onclick='getSynonyms(event, "<%=phrase%>", this);synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%			} } else { %>
	<span class="iconGSynonyms" id="syn_<%=100+i%>" onclick='getSynonyms(event, "<%=phrase%>", this);synPhrase="<%=phrase%>"' title="Equivalent Phrases"></span>
<%		} %>
<%	if (user.isAdmin() || user.isTester()) { %>
	<span class="iconDelete" onclick='deletePhrase(this, "<%=phrase%>", <%=100+i%>);' title="Remove Phrase"></span>
<%		} %>
	</td>
	<td class="kwdText" id="phrase<%=100+i%>">
		<%=phrase%>
	</td>
<% if (usestars) { %>
<td class="kwdRating">
	<div class="rateActions">
		<div id="rateMe" style="width:auto;float:left;" onmouseout="starRater(<%=100+i%>)">
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Optionally nice" id="<%=100+i%>_1" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Nice to have" id="<%=100+i%>_2" onclick="starRater(this, 'save')" class=""></a>

		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Good to have" id="<%=100+i%>_3" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Should have" id="<%=100+i%>_4" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Important" id="<%=100+i%>_5" onclick="starRater(this, 'save')" class=""></a>
		<!--a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Must have" id="100_6" onclick="starRater(this, 'save')" class=""></a-->
		</div>
		<span style="display:none;" class="actionStatus" id="status<%=100+i%>"></span>
		<span style="float:right;" id="actions<%=100+i%>">
		<a onclick="clearStarRating(<%=100+i%>);return false;" href="#" style="display:none;" id="clear<%=100+i%>">Clear</a>

		<!-- a onclick="removeKeyphrase(100);return false;" href="#" style="display:none;" id="remove<%=100+i%>">Remove</a -->
		</span>
	</div>
	<input type="hidden" value="0" id="rating_<%=100+i%>" name="rating_<%=100+i%>"/>
	<!--input type="hidden" value="false" id="required_<%=100+i%>" name="required_<%=100+i%>" /-->
</td>
<td align=center><input type="checkbox" value="true" id="required_<%=100+i%>" name="required_<%=100+i%>" onclick="checkRequired(this)"/><!--img id="reqimg_<%=100+i%>" src="star_off.gif" onclick="setRequired(this, <%=100+i%>)"/ --></td>
<td align=center><input type="checkbox" value="tag" id="tag_<%=100+i%>" name="tag_<%=100+i%>" /></td>
<% } else { %>
<td align=center><input type="radio" value="maybe1" id="maybe1_<%=100+i%>" name="required_<%=100+i%>"/></td>
<td align=center><input type="radio" value="maybe3" id="maybe3_<%=100+i%>" name="required_<%=100+i%>"/></td>
<td align=center><input type="radio" value="true" id="required_<%=100+i%>" name="required_<%=100+i%>"/></td>
<td align=center><input type="checkbox" value="tag" id="tag_<%=100+i%>" name="tag_<%=100+i%>" /></td>
<td align=center><img src="delete.gif" onclick="deselect(<%=100+i%>)"/></td>
<% } %>
</tr>
<%  
	i++;
	} %>
    </table>
	</div>
<div id="userphrases">
	<b>Please enter your own phrases <input type=button onclick="clearUserPhrases()" value="Clear entries"></b>
	<table id="userphraselist">
	<tr  style="display:inline">
	<th align=left width=60%>Your Phrase</th>
<% if (usestars) { %>
	<th align=left width=40%>Importance</th>
	<th align=right width=5%>Must Have</th>
	<th align=right width=5%>Tag</th>
<% } else { %>
	<th align=center nowrap width=13%>Nice to<br>Have</th>
	<th align=center nowrap width=13%>Good to<br>Have</th>
	<th align=center nowrap width=13%>Must Have</th>
	<th align=center width=10%>Tag</th>
<% } %>
	</tr>
<%	
	String disp = "inline";
	for (i = 0; i < 100; i++) 
	{
		if (i > 0) disp = "none";
%>
	<tr  class="kwdRecord" id="uptr_<%=i%>" style="display:<%=disp%>" onmouseover="helperActions(this, 'show')" onmouseout="helperActions(this, 'hide')">
	<td width=60% align=left nowrap><img src="x.png" title="Remove Phrase" onclick="deleteUserPhrase(<%=i%>)">&nbsp;<img src="plus-gsquare.png" title="Equivalent Phrases" onclick="getUserSynonyms(event, <%=i%>, this);">
	<!--span class="iconGSynonyms" onclick='getUserSynonyms(event, <%=i%>, this);' title="Equivalent Phrases"></span-->
	&nbsp;
	<input type=text name="userphrase_<%=i%>" id="userphrase_<%=i%>" size=25  onkeypress="return checkenter(event, <%=i%>)" ></td>
<% if (usestars) { %>
<td class="kwdRating">
	<div class="rateActions">
		<div id="rateMe" style="width:auto;float:left;" onmouseout="starRater(<%=i%>)">
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Optionally nice" id="<%=i%>_1" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Nice to have" id="<%=i%>_2" onclick="starRater(this, 'save')" class=""></a>

		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Good to have" id="<%=i%>_3" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Should have" id="<%=i%>_4" onclick="starRater(this, 'save')" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Important" id="<%=i%>_5" onclick="starRater(this, 'save')" class=""></a>
		<!--a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Must have" id="100_6" onclick="starRater(this, 'save')" class=""></a-->
		</div>
		<span style="display:none;" class="actionStatus" id="status<%=i%>"></span>
		<span style="float:right;" id="actions<%=i%>">
		<a onclick="clearStarRating(<%=i%>);return false;" href="#" style="display:none;" id="clear<%=i%>">Clear</a>

		<!-- a onclick="removeKeyphrase(100);return false;" href="#" style="display:none;" id="remove<%=i%>">Remove</a -->
		</span>
	</div>
	<input type="hidden" value="0" id="rating_<%=i%>" name="rating_<%=i%>"/>
	<!--input type="hidden" value="false" id="required_<%=i%>" name="required_<%=i%>" /-->
</td>
	<td width=15%>&nbsp;&nbsp;<a id="clear<%=i%>" style="visibility:hidden;font-size:9px;font-weight:normal" href="#" onclick="clearSelection(<%=i%>);setRequired2(<%=i%>)">Clear</a> </td>
	<td align=center><!--img id="reqimg_<%=i%>" src="star_off.gif" onclick="setRequired(this, <%=i%>)"/--><input type=checkbox name="required_<%=i%>" id="required_<%=i%>" value="true"  onclick="checkRequired(this)"></td>
	<td align=center><input type=checkbox name="tag_<%=i%>" id="tag_<%=i%>" value="tag" ></td>
<% } else { %>
<td width=13% align=center><input type="radio" value="maybe1" id="maybe1_<%=i%>" name="required_<%=i%>"/></td>
<td width=13% align=center><input type="radio" value="maybe3" id="maybe3_<%=i%>" name="required_<%=i%>"/></td>
<td width=13% align=center><input type="radio" value="true" id="required_<%=i%>" name="required_<%=i%>"/></td>
<td width=10% align=center><input type="checkbox" value="tag" id="tag_<%=i%>" name="tag_<%=i%>" /></td>
<% } %>
	</tr>
<% } %>
	<tr>
	<td colspan=4 align=center nowrao>
	<input type="button" value="Add Phrase" onclick="addNewPhrase()"  />
	<input type="button" id="resPhrasesBut" value="Select Resume Phrases" onclick="showResumePhrases(event, this)"  />
	<!-- input type="button" id="keyNamesBut" value="Select Key Names" onclick="showTagList(event, this)"  / -->
	</td>
	</tr>
	</table>
	<div id='respop' class="rpop" align=right>
	<table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
	</div>
</div>
<script>
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g,"");
	}
	var nophrases = 1;
	function addNewPhrase(phrase)
	{
		var uptr = document.getElementById("uptr_" + (nophrases-1));
		if (uptr.style.display == 'none')
		{
			uptr.style.display = 'inline';
			var inuptr = document.getElementById("userphrase_" + (nophrases-1));
			inuptr.value = "";
			if (phrase != null)
			{
				inuptr.value = phrase;
			}
			inuptr.focus();
			return (nophrases-1);
		}
		uptr = document.getElementById("uptr_" + nophrases);
		if (uptr != null)
		{
			uptr.style.display = 'inline';
			var inuptr = document.getElementById("userphrase_" + nophrases);
			inuptr.value = "";
			if (phrase != null)
			{
				inuptr.value = phrase;
			}
			inuptr.focus();
			nophrases++;
			return (nophrases-1);
		}
	}
	function deleteUserPhrase(num)
	{
		var uptr = document.getElementById("uptr_" + num);
		if (uptr != null)
		{
			uptr.style.display = 'none';
			var inuptr = document.getElementById("userphrase_" + num);
			inuptr.value = "";
		}
		clearStarRating(num);
	}

	function checkenter(event, num)
	{
		if (event == null)
			event = window.event;
		var keyCode = event.keyCode
		if (keyCode == null)
			keyCode = event.which;
		if(keyCode == 13)
		{
			if (num == nophrases-1)
			{
				addNewPhrase();
			}
			return true;
		}
		return true;
	}

	function addResumePhrase(phrase, idx)
	{
		var uptr = document.getElementById("uptr_" + (nophrases-1));
		var inuptr = document.getElementById("userphrase_" + (nophrases-1));
		if (uptr.style.display != 'none' && inuptr.value == '')
		{
			inuptr.value = phrase;
		}
		else
			addNewPhrase(phrase);
		var resPhraseTr = document.getElementById("resPhrase" + idx);
		if (resPhraseTr != null)
		{
			resPhraseTr.style.display = 'none';
		}
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_resume_phrase.jsp?phrase=" + escape(phrase);
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
	}

	function addKeyName(phrase, idx)
	{
		var uptr = document.getElementById("uptr_" + (nophrases-1));
		var inuptr = document.getElementById("userphrase_" + (nophrases-1));
		var idx = 0;
		if (uptr.style.display != 'none' && inuptr.value == '')
		{
			inuptr.value = phrase;
			idx = nophrases-1;
		}
		else
		{
			idx = addNewPhrase(phrase);
		}
		var tagptr = document.getElementById("tag_" + idx);
		if (tagptr != null)
		{
			tagptr.checked = true;
		}
	}
	function deleteResumePhrase(phrase, idx)
	{
		if (!confirm("Do you want to mark  '" + phrase + "' as invalid and remove it from processing in the future?"))
		{
			return;
		}
		var resPhraseTr = document.getElementById("resPhrase" + idx);
		if (resPhraseTr != null)
		{
			resPhraseTr.style.display = 'none';
		}
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_resume_phrase.jsp?permanent=true&phrase=" + escape(phrase);
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
	}
	function deletePhrase(obj, phrase, idx)
	{
		var delet = true;
		var phraseTd = document.getElementById("phrase" + idx);
		if (phraseTd != null && (phraseTd.style.textDecoration == 'none' || phraseTd.style.textDecoration == ''))
		{
			phraseTd.style.textDecoration = 'line-through';
		}
		else
		{
			phraseTd.style.textDecoration = 'none';
			delet = false;
		}
		if (delet && !confirm("Do you want to mark  '" + phrase + "' as invalid and remove it from processing in the future?"))
		{
			return;
		}
		clearSelection(idx);
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_jobposting_phrase.jsp?phrase=" + escape(phrase) + "&delete=" + delet;
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
		//alert(obj.style + ":" + obj.style.name);
		//obj.class = 'iconAdd';

	}
	
	var synPhrase;
	function getUserSynonyms(event, idx, img)
	{
		var phrase = document.getElementById("userphrase_" + idx);
		if (phrase == null) return;
		if (phrase.value == '')
		{
			alert("Please enter a phrase");
			return;
		}
		synPhrase = phrase.value;
		getSynonyms(event, phrase.value, this);
	}

	function checkAddenter(event)
	{
		if (event == null)
			event = window.event;
		var keyCode = event.keyCode
		if (keyCode == null)
			keyCode = event.which;
		if(keyCode == 13)
		{
			addSynonym(synPhrase);
			return true;
		}
		return true;
	}
	var synonyms;
	function addSynonym(phrase) {
		var inp = document.getElementById("AddSynonym");
		if (inp == null) return;
		var synonym = inp.value;
		if (synonym == '')
		{
			alert("Please enter a synonym");
			return;
		}
		if (phrase.toLowerCase() == synonym.toLowerCase())
		{
			alert("Synonym is same as the Phrase");
			return;
		}
		var list = document.getElementById("synonymList");
		if (list != null)
		{
			var oldsynonyms = list.value;
			oldsynonyms = oldsynonyms.split("|");
			for (var i = 0; i < oldsynonyms.length ; i++)
			{
				if (oldsynonyms[i].toLowerCase() == synonym.toLowerCase())
				{
					alert("Equivalent phrase already exists");
					return;
				}
			}
		}
		var command = "add";
		var url = "add_synonym.jsp";
		url = url + "?phrase=" + phrase;
		url = url + "&synonym=" + synonym;
		url = url + "&command=" + command;
		if (xmlhttp == null)
		{
			xmlhttp = GetXmlHttpObject();
		}
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
		var resp = xmlhttp.responseText;
		if (resp != null)
		{
			inp.value = "";
		}
		getSynonyms(null, phrase);
	}
	var resumePhrases = false;
	function showResumePhrases(event, button)
	{
		if (resumePhrases)
		{
			 closeResumePhrases();
			 return;
		}
		resumePhrases = true;
		tagType = "";
		showResumePhrasesPopup(event);
		button.value="Close Pop Up";
	}

	function showResumePhrasesPopup(evt)
	{

		var url = "get_resume_phrases.jsp?openTag=" + tagType + "&random=" + new Date();
		new Ajax.Request(url, { 
			method:'get',
			parameters : '',
			onSuccess: function(transport){
				
				var response = transport.responseText;
				if(response != null)
				{
					var respop = document.getElementById('respop');
					if (respop != null)
					{
						respop.innerHTML = response;
						respop.style.display = "inline";
						respop.style.maxHeight = '400px';
						respop.style.overflow = 'auto';
					}
					//alert(response);
				}
				else
				{
					alert('server response is null' );
				}
			}
		});
	}

	function openCompanies(hide)
	{
		if (hide)
			tagType = "";
		else
			tagType = "Company";
		showResumePhrasesPopup();
	}
	function openUniversities(hide)
	{
		if (hide)
			tagType = "";
		else
			tagType = "University";
		showResumePhrasesPopup();
	}
	function closeResumePhrases()
	{
		var button = document.getElementById("resPhrasesBut");
		button.value="Select from Resume Phrases";
		var rpop = document.getElementById('respop');
		rpop.style.display = "none";
		resumePhrases = false;
	}
	var tagList = false;
	function showTagList(event, button)
	{
		if (tagList)
		{
			 closeResumePhrases();
			 return;
		}
		if (resumePhrases)
		{
			 closeResumePhrases();
		}
		tagList = true;
		showTagListPopup(event);
		button.value="Close Pop Up";
	}
	function deselect(num)
	{
		var musthave = document.getElementById("required_" + num);
		if (musthave != null) musthave.checked = false;
		var maybe = document.getElementById("maybe1_" + num);
		if (maybe != null) maybe.checked = false;
		maybe = document.getElementById("maybe3_" + num);
		if (maybe != null) maybe.checked = false;
		var tag = document.getElementById("tag_" + num);
		if (tag != null) tag.checked = false;
	}

	function clearPostingPhrases()
	{
		if (!confirm("Are you sure you want to clear all selected Job Posting phrases and their ratings?"))
		{
			return;
		}
		for (var x = 0; x < 100 ; x++)
		{
			var id = 100+x;
			var idStr = "suggphrases_" + id;
			var tr = document.getElementById(idStr);
			if (tr != null)
			{
				clearStarRating(id)
				var req = document.getElementById("required_" + id);
				if (req != null) req.checked = false;
				var tag = document.getElementById("tag_" + id);
				if (tag != null) tag.checked = false;
			}
			else
				break;
		}
		//var button = document.getElementById("restoreButton");
		//if (button != null)
		//	button.style.display = 'inline';
	}
	function clearUserPhrases()
	{
		if (!confirm("Are you sure you want to clear all Your Entered phrases and their ratings?"))
		{
			return;
		}
		for (var x = 0; x < 100 ; x++ )
		{
			var tr = document.getElementById("uptr_" + x);
			if (tr != null)
			{
				clearStarRating(x);
				var inuptr = document.getElementById("userphrase_" + x);
				if (inuptr != null)
					inuptr.value = "";
				if (x > 0)
					tr.style.display = 'none';
				var req = document.getElementById("required_" + x);
				if (req != null) req.checked = false;
				var tag = document.getElementById("tag_" + x);
				if (tag != null) tag.checked = false;
			}
			else
				break;
		}
		//var button = document.getElementById("restoreButton");
		//if (button != null)
		//	button.style.display = 'inline';
	}
	function restoreSession(stamp)
	{
		window.location="job_phrases.jsp?command=restoreSession&sessStamp=" +stamp;
	}
	function deleteSession(stamp, x)
	{
		if (!confirm("Are you sure you want to delete session " + x + "?"))
		{
			return;
		}
		window.location="job_phrases.jsp?command=deleteSession&sessStamp=" +stamp;
	}
	function saveSession()
	{
		document.phrasesform.action = "job_phrases.jsp?command=saveSession" ;
		var result = submitSelection();
		if (!result)
		{
			document.phrasesform.action = "Analyzer" ; // restore on error
		};
	}
	function useStarSelection(yesno)
	{
		document.phrasesform.action = "job_phrases.jsp?usestars=" + yesno;
		var result = submitSelection();
		//alert(result);
		if (!result)
		{
			document.phrasesform.action = "Analyzer" ; // restore on error
		};
	}
</script>

	<div align=left style="display:block; width:50%;white-space:nowrap;float:left">
		&nbsp;&nbsp;&nbsp;<p><br><br><input type="button" onclick="submitSelection()" value="Analyze/Sort Resumes" />&nbsp;&nbsp;&nbsp;<input type="button" onclick="saveSession()" value="Save Session" />
<% if (usestars) { %>
		&nbsp;&nbsp;&nbsp;<input type="button" onclick="useStarSelection(false)" value="Simple Rating" />
<% } else { %>
		&nbsp;&nbsp;&nbsp;<input type="button" onclick="useStarSelection(true)" value="5 Star Rating" />
<% } %>
		</div>
    </form>
  <div id="loading" style="visibility: hidden">
    <p>Processing...</p>
  </div>
</div>
</div>
<script>
	function unSelect(ind)
	{
		var req =document.getElementById("required_" + ind);
		if (req != null)
			req.checked=false;
		var reqimg =document.getElementById("reqimg_" + ind);
		if (reqimg != null)
			reqimg.src="star_off.gif";
		clearRating(ind);
	}
	function checkRequired(cbx)
	{
		var id = cbx.id.split("_");
		var ind = id[1];
		if (cbx.checked)
		{
			cbx.value = true;
			clearStarRating(ind);
		}
		else
		{
		}
	}
	function setRequired(image, ind)
	{
		var url = image.src;
		if (url.indexOf("star_off") != -1)
		{
			image.src = "star_on.gif";
			var req =document.getElementById("required_" + ind);
			req.checked=true;
			clearStarRating(ind);
		}
		else
		{
			image.src = "star_off.gif";
			var req =document.getElementById("required_" + ind);
			req.checked=false;
		}
	}
	function clearRequired2(ind)
	{
		return;
		var image = document.getElementById("reqimg2_" + ind);
		if (image == null) return;
		var url = image.src;
		if (url.indexOf("star_on") != -1)
		{
			image.src = "star_off.gif";
			var req =document.getElementById("required2_" + ind);
			req.value="false";
		}
	}
	function setRequired2(ind)
	{
		return;
		var image = document.getElementById("reqimg2_" + ind);
		if (image == null) return;
		var url = image.src;
		if (url.indexOf("star_off") != -1)
		{
			image.src = "star_on.gif";
			var req =document.getElementById("required2_" + ind);
			req.value="true";
		}
	}
	function clearSelection(ind)
	{
		clearRating(ind);
		var image = document.getElementById("reqimg_" + ind);
		if (image != null)
			image.src = "star_off.gif";
		var req =document.getElementById("required_" + ind);
		if (req != null)
			req.checked=false;
		var clear = document.getElementById("clear" + ind);
		if (clear != null)
		{
			clear.style.visibility='hidden';
		}
	}
	function contains(a, obj) 
	{     
		for (var i = 0; i < a.length; i++) 
		{         
			if (a[i] === obj) 
				return true;         
		}     
		return false; 
	}
	function submitSelection()
	{
		var selPhrases = new Array();
		var elems = document.phrasesform.elements;
		var selected = false;
		var unrated = "";
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name.indexOf("suggphrases_") == 0)
			{
				var id =  elems[i].id.split('_');
				var rating = document.getElementById('rating_'+id[1]);
				var maybe1 = document.getElementById('maybe1_'+id[1]);
				var maybe3 = document.getElementById('maybe3_'+id[1]);
				if (document.getElementById('required_'+id[1]).checked 
					|| (rating != null && rating.value != 0) 
					|| (maybe1 != null && maybe3.checked)
					|| (maybe3 != null && maybe3.checked))
				{
					selected = true;
					var str = new String(elems[i].value).toLowerCase();
					selPhrases.push(str);
				}
			}
			if (name.indexOf("userphrase_") == 0 && elems[i].value != "")
			{
				var str = new String(elems[i].value).toLowerCase();
				if (contains(selPhrases, str) || contains(selPhrases, str + " "))
				{
					alert("Phrase:'" + elems[i].value +"' is already in the list. Please remove it.");
					return false;
				}
				var idx = name.substring(11);
				var rating = document.getElementById("rating_" + idx);
				var required = document.getElementById("required_" + idx);
				var tagged = document.getElementById("tag_" + idx);
				if (rating != null && rating.value == "0" && !required.checked && !tagged.checked)
				{
					unrated = unrated + "," + "'" + elems[i].value +"'";
				}
				selPhrases.push(str);
			}
		}
		var action = document.phrasesform.action;
		if (unrated != '' && action.indexOf('Analyzer') != -1)
		{
			alert("Please rate phrases:" + unrated.substring(1));
			return false;
		}
		if (!selected && action.indexOf('Analyzer') != -1)
		{
			alert("Please select some phrases")
			return false;
		}
		document.getElementById("loading").style.visibility = "visible";
		document.phrasesform.submit();
		return true;
	}

<% 
	for (SortPhrase prevPhrase: prevPhrases)
	{
		boolean found = false;
		int idx = 0;
		for(PhraseList p : phraseList)
		{
			String phrase = p.iterator().next().getPhrase().getBuffer().toString();
			if (phrase.trim().equals(prevPhrase.getLemmatizedPhrase().trim()))
			{
				found = true;
				if (prevPhrase.isRequired())
				{
%>
		var req = document.getElementById("required_<%=100+idx%>");
		if (req != null) req.checked = true;
<%
				}
				if (prevPhrase.isTag())
				{
%>
		var tag = document.getElementById("tag_<%=100+idx%>");
		if (tag != null) tag.checked = true;
<%
				}
				if (usestars)
				{
%>
		var idStr = "" + (100 + <%=idx%>) + "_" + <%=prevPhrase.getStars().intValue()%>;
		var img = document.getElementById(idStr);
		if (img != null)
		{
			starRater(img, 'on')
			starRater(img, 'save');
		}
<%
				}
				else
				{
					if (prevPhrase.getStars().intValue() > 2 && !prevPhrase.isRequired())
					{
%>
		var maybe = document.getElementById("maybe3_<%=100+idx%>");
		if (maybe != null) maybe.checked = true;
<%
					}
					else if (prevPhrase.getStars().intValue() > 0 && !prevPhrase.isRequired())
					{
%>
		var maybe = document.getElementById("maybe1_<%=100+idx%>");
		if (maybe != null) maybe.checked = true;
<%
					}
				}
			}
			idx++;
		}
		if (!found || prevPhrase.isUserentered())
		{
%>
		uphrase = "<%=prevPhrase.getOriginalPhrase().trim()%>";
		uptr = document.getElementById("uptr_" + (nophrases-1));
		inuptr = document.getElementById("userphrase_" + (nophrases-1));
		if (uptr.style.display != 'none' && inuptr.value == '')
		{
			inuptr.value = uphrase;
		}
		else
		{
			addNewPhrase(uphrase);
		}
		idx = nophrases-1;
<%
		if (usestars)
		{
%>
		idStr = "" + idx + "_" + <%=prevPhrase.getStars().intValue()%>;
		img = document.getElementById(idStr);
		if (img != null)
		{
			starRater(img, 'on')
			starRater(img, 'save');
		}
<%
		}
		else
		{
			if (prevPhrase.getStars().intValue() > 2 && !prevPhrase.isRequired())
			{
%>
		var maybe = document.getElementById("maybe3_" + idx);
		if (maybe != null) maybe.checked = true;
<%
			}
			else if (prevPhrase.getStars().intValue() > 0 && !prevPhrase.isRequired())
			{
%>
		var maybe = document.getElementById("maybe1_" + idx);
		if (maybe != null) maybe.checked = true;
<%
			}
		}
				if (prevPhrase.isRequired())
				{
%>
		var req = document.getElementById("required_" + idx);
		if (req != null) req.checked = true;
<%
				}
				if (prevPhrase.isTag())
				{
%>
		var tag = document.getElementById("tag_" + idx);
		if (tag != null) tag.checked = true;
<%
				}
		}
	}
%>
</script>
  
<%@include file="rq_footer.jsp"%>
</body>
</html>