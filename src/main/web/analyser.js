/*ADDED BY IGO*/
var xmlhttp;
var spanIdx = null;
var optIdx = null;
var elemToRaise = null;
var elemToShowHelp = null;
var xToHelp = null;
var yToHelp = null;
var wantsExtraToSet = null;

var helpList=null;
var feedbackText=null;
var phrase=null;
/*FINISH ADDED BY IGO*/

var ENABLE_FEATURE_INFO = true;
var ENABLE_FEATURE_MORE = true;

var selections  = new Array();
var feedback    = new Array();
var tagArray    = new Array();
var manualEntry = new Array();

// var EMPTY_USER_OPTION_TEXT = "&gt;&gt; Enter Text &lt;&lt;";
var EMPTY_USER_OPTION_TEXT = "[ Enter Text ]";
var generic_span_onClick = new Function("return span_onClick(this);");
var isIE = navigator.appName.indexOf("Microsoft Internet Explorer") >= 0;
var isSafari = navigator.vendor != null
		&& navigator.vendor.indexOf("Apple Computer") >= 0;

var raisedWindowIndex = null;
var raisedWindowSpan = null;
var raisedWindowDiv = null;
var raisedWindowExtra = null;

/*for ( var j = 0; j < optionList.length; j++) {
	var hasExtras = extrasIndex[j] > 0;
	var list = optionList[j].split(",");
	manualEntry[j] = "";
	feedback[j] = ""
	selections[j] = 0; // the current selection is the 0'th entry (first word)
	tagArray[j] = 0;   // all selections start off untagged
	// of every list

	// -- Create drop-menu set 1
	var menuTableStr = "<div class='dropMenu' id='dropMenu" + j
			+ "'><table class='menuTable' cellpadding=0 cellspacing=0>";

	var nSet1 = hasExtras ? extrasIndex[j] : list.length;
	for ( var k = 0; k < nSet1; k++) {

		menuTableStr += "<tr><td><a class=errOption"
		if (k == 0)
			menuTableStr += "1";
		menuTableStr += " href='javascript:void(0);' onclick='javascript:selectWord(this,"
				+ k + ")'>" + list[k] + "</a></td>";

		// -- new feature Help Info
		// -- only include an icon, if help exists for option j,k
		if (j < helpList.length) {
			var helpItems = helpList[j].split("|");
			if (k < helpItems.length) {
				var helpMsg = helpItems[k];
				if (helpMsg.length > 0) {
					menuTableStr += "<td align='right'><img id='info"
							+ k
							+ "' src='tinyInfo.gif' onmouseover='handleTooTip(event);'></td>";
				}
			}
		}
	}

	menuTableStr += getManualEntryStr(j);
	menuTableStr += getBottomLinksStr(j, hasExtras, "More");
	menuTableStr += "</table></div>"; // end the 1st set

	if (hasExtras) {
		//-- Create drop-menu set 2 (for extras)
		menuTableStr += "<div class='dropMenu' id='dropMenu" + j
				+ "e'><table class='menuTable'  cellpadding=0 cellspacing=0>";
		for ( var k = 0; k < list.length; k++) {
			//-- new feature Help Info
			// -- only include an icon, if help exists for option j,k
			// -- need to know if we have a help msg, for proper column spacing
			var helpMsg = "";
			if (j < helpList.length) {
				var helpItems = helpList[j].split("|");
				if (k < helpItems.length) {
					helpMsg = helpItems[k];
				}
			}

			if (k == nSet1)
				menuTableStr += "<tbody class=errOptionExtra>";
			menuTableStr += "<tr><td";
			if (helpMsg.length == 0)
				menuTableStr += " colspan='2'";
			if (k == nSet1)
				menuTableStr += " style='border-top: solid 1px black;'";
			menuTableStr += "><a class=errOption";
			if (k == 0)
				menuTableStr += "1";
			menuTableStr += " href='javascript:void(0);' onclick='javascript:selectWord(this,"
					+ k + ")'>" + list[k] + "</a></td>";

			if (helpMsg.length > 0) {
				menuTableStr += "<td align='right'";
				if (k == nSet1)
					menuTableStr += " style='border-top: solid 1px black;'";
				menuTableStr += "><img id='info"
						+ k
						+ "' src='tinyInfo.gif' onmouseover='handleTooTip(event);'></td>";
			}
		}
		menuTableStr += "</tbody>";
		// -- Do bottom links (Feedback & More/Less)
		menuTableStr += getManualEntryStr("" + j + "e");
		menuTableStr += getBottomLinksStr(j, hasExtras, "Less");
		menuTableStr += "</table></div>"; // end the 2nd set
	}

	document.write(menuTableStr);
}*/

document
		.write("<form method='POST' action='DocumentCreator'><input type='hidden' id='submittedSelections' name='submittedSelections' value='"
				+ selections + "'>");
document
		.write("<input type=\"hidden\" name=\"AllFeedback\" id=\"AllFeedback\" value=\"\"/><input type=\"hidden\" name=\"FeedbackIndices\" id=\"FeedbackIndices\" value=\"\"/><input type=\"hidden\" name=\"TagArray\" id=\"TagArray\" value=\"\"/></form>");

function init() {
	raisedWindowIndex = null;
	raisedWindowSpan = null;
	raisedWindowDiv = null;
	raisedWindowExtra = null;
	
	//Ajax call to update elems number.
	useAjaxFunctions(5,0);
	
	return;
	// -- alt way;
	var errElems = getElementsByTagAndClass("span", "spErr");

	for (i = 0; i < errElems.length; i++) {
		errElems[i].onClick = generic_span_onClick;
	}

}

function span_onClick(obj) {
	//alert( obj.innerText );
	return false;
}

function setInnerText(elem, txt) {
	if (isIE) {
		elem.innerText = txt;
	} else {
		elem.textContent = txt;
	}
}

function handleClick(evt) {

	if (!isIE && (evt == null))
		return;
	var elem = isIE ? window.event.srcElement : evt.target;

	var tagName = elem.tagName;
	var elemId = elem.id;
	
	if ((tagName != null) && (tagName == "SPAN") && (elemId != null)
			&& (elemId.indexOf("spErr") == 0)) {
		//have spErr text span
		var index = elem.id.substring(5);	
		
		elemToRaise=elem;
		//alert(spanId);
		spanIdx=index;
		
		raiseSelectionWindow(elem, index, false);
	} else if (elemId.indexOf("manualEntry") == 0) {
		//clicked on user supplied area; do nothing
	} else {
		//alert("about to hide last menu");
		hideLastMenu();
	}

	if (isIE) {
		window.event.returnValue = false;
	} else {
		evt.returnValue = false;
	}
	return false;
}

function raiseSelectionWindow(obj, idx, wantsExtra) {
	if (obj == null)
		alert(" raiseSelectionWindow( obj is NULL!!!");
	hideLastMenu();
	
	var menuId = "dropMenu" + idx;
	if (wantsExtra)
		menuId += "e";
	
	//added by igo
	wantsExtraToSet=wantsExtra;
	//alert("dropMenu= "+menuId);
	if(!wantsExtra)
		useAjaxFunctions(1,idx);
	else{
		var dropMenu = document.getElementById(menuId);
		if (dropMenu == null)
			alert("dropmenu with id= " + menuId + " does not exist!");
	
		var x = obj.offsetLeft;
		var y = obj.offsetTop;

		var temp=obj;
		//Recursively getElement because is inside table
	    while (temp = temp.offsetParent) {
	            x += temp.offsetLeft;
	            y += temp.offsetTop;
	    }
	
		dropMenu.style.visibility = "visible";
		dropMenu.style.position = "absolute";
		raisedWindowIndex = idx;
		raisedWindowSpan = obj;
		raisedWindowDiv = dropMenu;
		raisedWindowExtra = wantsExtra;
	}
	return false;
}

function getElementsByTagAndClass(tagName, className) {
	var elems = document.body.getElementsByTagName(tagName);
	var result = new Array();
	for (index = 0; index < elems.length; index++) {
		var elem = elems[index];
		if (elem.className == className) {
			result[result.length] = elem;
		}
	}
	return result;
}

function hidemenu(obj) {
	if (obj != null) {
		obj.style.visibility = "hidden";
		raisedWindowIndex = null;
		raisedWindowSpan = null;
		raisedWindowDiv = null;
		
	    /*var d = document.getElementById("menus");
	    var olddiv = document.getElementById("dropMenu"+spanIdx);
	    d.removeChild(olddiv);*/

	}
}

function hideLastMenu() {
	if (raisedWindowIndex != null) {
		//--if the user has entered manual txt, and clicks outside the menu, accept their manual entry
		var userField = document.getElementById("manualEntry"
				+ raisedWindowIndex + (raisedWindowExtra ? "e" : ""));
		if (userField != null && userField.value != EMPTY_USER_OPTION_TEXT
				&& userField.value.length > 0) {
			handleManualEntry(userField, true);
		}

		hidemenu(raisedWindowDiv);

	}
}

function setUserField(errIdx, txt) {
	var userField = manualEntry[errIdx];
	userField.value = txt;
	var manualEntryElem1 = document.getElementById("manualEntry" + errIdx);

	manualEntryElem1.value = txt;

	var manualEntryElem2 = document.getElementById("manualEntry" + errIdx + "e");

	if (manualEntryElem2 != null) {
		manualEntryElem2.value = txt;
	}
}

//raisedWindowIndex= id of the selected span/window
//optIdx= id of the selected word
function selectWord( obj, optId){
	optIdx=optId;
	useAjaxFunctions(2,raisedWindowIndex);
/*	
  var errIdx = raisedWindowIndex;
  var errSpan =  document.getElementById( "spErr" + errIdx);
  var list = optionList[errIdx].split(",");
  setInnerText( errSpan, list[ optIdx ]);
  setUserField(errIdx, EMPTY_USER_OPTION_TEXT);
  var extraIndex = extrasIndex[errIdx];
  errSpan.className = (optIdx == 0) ? 		"spErr_reselected" : 
                       optIdx < extraIndex || 
		       extraIndex == 0 ? 	"spErr_corrected" :
						"spErr_corrected_more";

  selections[errIdx] = optIdx;
  hideLastMenu();*/
  
}

function handleKeyDown(evt) {
	if (!isIE && (evt == null))
		return;
	if (isIE)
		evt = window.event;
	var elem = isIE ? evt.srcElement : evt.target;
	var keyCode = isIE ? event.keyCode : isSafari ? evt.keyCode : evt.which;
	// alert( keyCode );
	// alert( elem.value);

	var index = new Number(elem.id.substring(25));

	return handleManualEntry(elem, false, evt);
}

function handleManualEntry(obj, force, evt){
 if ( !isIE && !force && (evt == null)) return;
 var keyCode = force ? 13 : isIE || isSafari ? evt.keyCode : evt.which;
  if ( force || keyCode == 13 ){  //(isSafari && enterKey ) || 
    var text = obj.value;
    text = text.replace( "\," , "<comma/>" );
    if ( text.length != 0 ) {
    	selections[raisedWindowIndex] = "<ME>"+text+"</ME>";//+selections.length + 1;//one higher than the given options

    	var errSpan =  document.getElementById( "spErr" + raisedWindowIndex);
    	setInnerText( errSpan, obj.value);
    	errSpan.className = "spErr_manualChoice";
    	setUserField(raisedWindowIndex, obj.value );
    }
    hidemenu(raisedWindowDiv);
    return false;
  }
}

function handleUserOptionClick(obj) {
	if (obj.value == EMPTY_USER_OPTION_TEXT) {
		obj.value = "";
	}
	return true;
}

function submit() {
  //-- Request to replace 0 selections (original) with -1, if they were re-selected
  for (var i=0;i<selections.length;i++){
     var errSpan =  document.getElementById( "spErr" + i);
     if (errSpan.className == "spErr_reselected" ){
        selections[i] = -1;
     }
  }

	var formElem = document.getElementById("submittedSelections");
	formElem.value = "" + selections;
	//alert(" You are going to submit back to the server the following selected options [ "
	//		+ formElem.value + "]");

	var fbElem    = document.getElementById("AllFeedback");
	var fbIdxElem = document.getElementById("FeedbackIndices");
	var allFB = "";
        var fbIdx = "";
	for ( var i = 0; i < feedback.length; i++) {
		var fb = feedback[i];
		if (fb != null && fb.length > 0) {
			if (allFB.length > 0) {
				allFB += "|\n";
                                fbIdx += ",";
                        }
			allFB += "Feedback[" + i + "]=" + fb;
                        fbIdx += i;
		}
	}
	fbElem.value    = allFB; 
	fbIdxElem.value = fbIdx;
//TESTING NEW FEATURE,  FeedbackIndices:  alert( fbIdxElem.value );


  var tagArrayElem   = document.getElementById("TagArray");
  tagArrayElem.value =  "" + tagArray;
  
//TESTING NEW FEATURE2, TagArray:         alert( tagArrayElem.value );

	//alert(" You are going to submit back to the server the following feedback [ "
	//		+ fbElem.value + "]");

	// NOTE: disable the previous two "alerts" and enable the following submit
	// line, when you are ready to actually submit something back to the server;
	// What you are submitting back is a comma-delimited string of indexes, one
	// for each selected word; In other words, if you don't make any spelling
	// revisions, then the string will just be a bunch of comma-delimited zeroes
	// NOTE2: You are now also submitting a field "AllFeedback" which lists all
	// the feedback values;
	document.forms[0].submit();
}

//-- New Features
var feedBackIndex = null;
function showFeedback(index) {
	
	feedBackIndex = index;
	//useAjaxFunctions(4,index);
	
	var fbElem = document.getElementById('feedbackPane');
	var fbTextElem = document.getElementById('feedbackTextArea');
	var fbRegdElem = document.getElementById('feedbackRegarding');
	fbTextElem.value = feedbackText;
		
	setInnerText(fbRegdElem, phrase);
	fbElem.style.display = "inline";
	fbElem.style.top = document.body.scrollTop;
	fbElem.style.left = document.body.scrollLeft;
	return false;
}

function hideFeedback() {
	var fbElem = document.getElementById('feedbackPane');
	if (feedBackIndex != null) {
		var fbTextElem = document.getElementById('feedbackTextArea');
		feedback[feedBackIndex] = fbTextElem.value;
	}
	fbElem.style.display = "none";
	return false;
}

function tthide() {
	var tt = document.getElementById('tt');
	tt.style.display = "none";
	ttObj = null;
}

function toggleMore(elem, j) {
	if (!ENABLE_FEATURE_MORE) {
		alert("The 'More Options' feature is reserved for subscribers only.");
		return false;
	}

	var saveCurSpan = raisedWindowSpan;
	var state = (isIE) ? elem.innerText : elem.textContent;
	var wantsExtra = (state == "More");
	//alert(wantsExtra);
	//alert( state );

	raiseSelectionWindow(saveCurSpan, j, wantsExtra);
	return false;
}

function toggleTag(elem, j) {

	tagArray[j] = tagArray[j]==0 ? 1:0;
	var newText = tagArray[j]==0  ? "Tag as Error" : "Tagged, Thank You";

	if (isIE) {
	   elem.innerText   = newText;
        }else{
	   elem.textContent= newText;
        }
	
	return false;
}

function handleTooTip(evt) {
	if (!isIE && (evt == null))
		return;
	var elem = isIE ? window.event.srcElement : evt.target;
	var event = isIE ? window.event : evt;
	var x = document.body.scrollLeft + (isIE ? event.x : event.clientX);
	var y = document.body.scrollTop + (isIE ? event.y : event.clientY);
	var j = raisedWindowIndex;
	var k = new Number(elem.id.substring(4));
	var txt = "The 'Additional Information' feature\nis reserved for subscribers only.";
	if (ENABLE_FEATURE_INFO) {

		var helpItems = helpList.split("|");
		txt = helpItems[k];
	}
	toolTip(elem, x + 15, y, txt);
	return false;
	
	/*if (!isIE && (evt == null))
		return;
	elemToShowHelp = isIE ? window.event.srcElement : evt.target;
	var event = isIE ? window.event : evt;
	xToHelp = document.body.scrollLeft + (isIE ? event.x : event.clientX);
	yToHelp = document.body.scrollTop + (isIE ? event.y : event.clientY);

	var j = raisedWindowIndex;
	optIdx = new Number(elemToShowHelp.id.substring(4));

	//useAjaxFunctions(3,raisedWindowIndex);
	var txt = "The 'Additional Information' feature\nis reserved for subscribers only.";
	if (ENABLE_FEATURE_INFO) {
		var helpItems = helpList[j].split("|");
		txt = helpItems[k];
	}

	toolTip(elem, x + 15, y, txt);

	return false;*/
}

var ttObj; // elem tt is about
var generic_tthide = new Function("tthide();");
function tthide() {
	var tt = document.getElementById('tt');
	tt.style.display = "none";
	ttObj = null;
}

function toolTip2(obj, j, k) {
	var helpItems = helpList[j].split(" _LIMIT_ ");
	txt = helpItems[k];
	if (obj == null)
		return; // useful mechanism to disable ttips, just pass 'null' instead
				// of 'this'
}

function toolTip(obj, x, y, txt) {
	var tt = document.getElementById('tt');
	var ttBody = document.getElementById('ttBody');
	if (tt == null)
		return false;
	// --clear away any previous content
	while (ttBody.firstChild != null) {
		ttBody.removeChild(ttBody.firstChild);
	}
	//-- parse text into rows
	var rows = txt.split("\n");
	for ( var r = 0; r < rows.length; r++) {
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		var temp = rows[r];
		temp=temp.substring(0,temp.length);
		setInnerText(td, temp);
		tr.appendChild(td);
		ttBody.appendChild(tr);
	}
	tt.style.left = x;
	tt.style.top = y;
	tt.style.display = "inline";
	obj.onmouseout = generic_tthide;
}

function getManualEntryStr(index) {

	return "<tr><td colspan='2'><input type='text' id='manualEntry"
			+ index
			+ "' class='userSuppliedText' maxlenth='100' value='"
			+ EMPTY_USER_OPTION_TEXT
			+ "' onkeydown='return handleKeyDown(event)' onclick='return handleUserOptionClick(this)'/></td></tr>";

}

function getBottomLinksStr(index, hasExtras, moreOrLessStr) {

	var bottomLinksStr = "<tr height='10px'><td width='100%' colspan='2'><table width='100%'><tr>";
	bottomLinksStr += "<td width='40%' class=feedbackLink><a href='void(0);' onclick='return showFeedback("
			+ index + ");'>Feedback</a></td>";
	bottomLinksStr += "</td><td width='20%' class=tagLink>";

	bottomLinksStr += "<a href='void(0);' onclick='return toggleTag(this,"
				+ index + ");'>" + (tagArray[index]==0? "tag" : "untag") + "</a>";

	bottomLinksStr += "</td><td width='40%' class=moreLink>";
	if (hasExtras) {
		bottomLinksStr += "<a href='void(0);' onclick='return toggleMore(this,"
				+ index + ");'>" + moreOrLessStr + "</a>";
	}
	bottomLinksStr += "</td></tr></table></td></tr>";

	return bottomLinksStr;
}

/*AJAX FUNCTIONS*/

function useAjaxFunctions(type,idx)
{
xmlhttp=GetXmlHttpObject();

if (xmlhttp==null)
  {
  alert ("Your browser does not support XMLHTTP!");
  return;
  }

//Set url the open
var url="GetDropMenuAnalyser";
url=url+"?type="+type;
url=url+"&idx="+idx;

//Drop Menu
if(type==1){
	xmlhttp.onreadystatechange=stateChangedDropMenu;
}
		
xmlhttp.open("GET",url,true);
xmlhttp.send(null);
}

function stateChangedDropMenu()
{

if (xmlhttp.readyState==4)
  {
	//get response from servlet
	  var xmlDoc=xmlhttp.responseXML.documentElement;
	  var response=xmlDoc.getElementsByTagName("menu")[0].firstChild.data;
	  feedbackText=xmlDoc.getElementsByTagName("feedback")[0].firstChild.data;
	  var helpListResp=xmlDoc.getElementsByTagName("helplist")[0].firstChild.data;
	  phrase=xmlDoc.getElementsByTagName("phrase")[0].firstChild.data;
	  helpList=helpListResp;
	  
	var menuId = "dropMenu" + spanIdx;
	if (wantsExtraToSet)
		menuId += "e";

 	if (response == null)
		alert("dropmenu with id= " + menuId + " does not exist!");

 	
 	var dropMenu=document.getElementById(menuId);
 	if(dropMenu==null){
	    //Create new div element
		newDiv = document.createElement("div");
		newDiv.setAttribute("id", menuId);
		newDiv.setAttribute("class","dropMenu");
		newDiv.innerHTML=response;
		document.getElementById("menus").appendChild(newDiv);
		dropMenu = document.getElementById(menuId);
 	}

	var x = elemToRaise.offsetLeft;
	var y = elemToRaise.offsetTop;

	var temp=elemToRaise;
	//Recursively getElement because is inside table
    while (temp = temp.offsetParent) {
            x += temp.offsetLeft;
            y += temp.offsetTop;
    }

	dropMenu.style.left = x;
	dropMenu.style.top = y;
	dropMenu.style.visibility = "visible";
	dropMenu.style.position = "absolute";
	raisedWindowIndex = spanIdx;
	raisedWindowSpan = elemToRaise;
	raisedWindowDiv = dropMenu;
	raisedWindowExtra = wantsExtraToSet;
  
  }
}

function GetXmlHttpObject()
{
if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
return null;
}