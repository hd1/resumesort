<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%
	if (user == null)
	{
		response.sendRedirect("");
		return;
	}

	String userdir = ResumeSorter.getUploadDir(user);
    session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString(), userdir);
	String postingId = request.getParameter("postingId");
	int offset = getInt(request.getParameter("offset"));
	int max = getInt(request.getParameter("max"));
	if (max == 0) max = 1000;
	//List<JobPosting> postings = new JobPosting().getObjects("user_name=" + UserDBAccess.toSQL(user.getLogin()) + " and id in (select distinct job_id from resume_score) order by posting_date desc,posting_id", offset, max);
	List<UserTemplate> templates =  new UserTemplate().getObjects("user_name=" + UserDBAccess.toSQL(user.getLogin())); 
	List<JobPosting> postings = new JobPosting().getObjects("user_name=" + UserDBAccess.toSQL(user.getLogin()) + " order by posting_date desc,posting_id", offset, max);
	List<SharedJobPosting> sjps =  new SharedJobPosting().getObjects("perm3 = 1 and IDUSER = " + user.getId());
	JobPosting posting = null;
	boolean owner = false;
	for (JobPosting jp: postings)
	{
		if (String.valueOf(jp.getId()).equals(postingId))
		{
			posting = jp;
			owner = true;
		}
	}
	JobPosting currPosting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	boolean jobinprogress = false;
	Integer numleft = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
	if (numleft == null) numleft = 0;
	if (numleft > 0)
		jobinprogress = true;
    if (session.getAttribute(SESSION_ATTRIBUTE.RANKING_INPROGRESS.toString()) != null)
		jobinprogress = true;
	System.out.println("Deleting posting, id="+ postingId + " posting:" + posting);
	if ("delete".equals(request.getParameter("command")) && posting == null)
	{
%>	<script>alert("This posting can not be deleted by you.")</script> <%
	}
	if ("delete".equals(request.getParameter("command")) && posting != null)
	{
		if (currPosting != null && currPosting.getId() == posting.getId() && jobinprogress)
		{
%>	<script>alert("This posting can not be deleted. Still being processed")</script> <%
		}
		else
		{
			new SortPhrase().deleteObjects("JOB_ID = " + postingId);
			new SharedJobPosting().deleteObjects("JOB_ID = " + postingId);
			new ResumeScore().deleteObjects("JOB_ID = " + postingId);
			File postingFile = PlatformProperties.getInstance().getResourceFile(
											ResumeSorter.getUploadDir(user) + File.separator + ResumeSorter.JOBPOSTING_FOLDER + File.separator + posting.getPostingFile());
			// File may be used by other postings
			//if (postingFile.exists())
			//	postingFile.delete(); 
			postingFile = PlatformProperties.getInstance().getResourceFile(
											ResumeSorter.getUploadDir(user) + File.separator + ResumeSorter.JOBPOSTING_FOLDER + File.separator + posting.getPostingFile() + ".Object");
			if (postingFile.exists())
				postingFile.delete();
			posting.delete();
			postings.remove(posting);
			posting = null;
			postingId = null;
		}
	}
	if ("deleteResume".equals(request.getParameter("command")) && posting != null)
	{
		String resume = request.getParameter("resume");
		if (resume != null && resume.trim().length() > 0)
			new ResumeScore().deleteObjects("JOB_ID=" + posting.getId() + " and resume_file = " + UserDBAccess.toSQL(resume));
	}

	for (SharedJobPosting sjp: sjps)
	{
		List<JobPosting> postings2 = new JobPosting().getObjects("ID =" + sjp.getJobId());
		postings.addAll(postings2);
		if (posting == null)
		{
			for (JobPosting jp: postings2)
			{
				if (String.valueOf(jp.getId()).equals(postingId))
				{
					posting = jp;
					userdir = userdir.replace(user.getLogin(), jp.getUserName());
					break;
				}
			}
		}
	}
	if (posting == null && postingId != null && user.isAdmin())
	{
		List<JobPosting> postings2 = new JobPosting().getObjects("ID =" + postingId);
		if (postings2.size() > 0)
			posting = postings2.get(0);
	}
	if (posting == null && postingId != null )
	{
	%>
		<script>alert("Job Posting: <%=postingId%> not found")</script>;
	<%
	}
	if (posting == null && postings.size() > 0)
	{
		posting = postings.get(0);
	}
	if (posting == null)
	{
	%>
			<script>
				//alert("No Job Postings found");
			</script>
	<%
	}
	else
		postingId = "" + posting.getId();
	if ("restore".equals(request.getParameter("command")))
	{
		String userName = request.getParameter("userName");
		if (userName == null) userName = user.getLogin();
		String directory = ResumeSorter.getUploadDir(userName);
		try
		{
			System.out.println("Restoring:" + posting.getId());
			JobPostingSession jps = new JobPostingSession(userName, posting.getId());
			jps.restore(session, directory);
%>
	<script>window.location="ranked_resumes.jsp"</script>
<%
		return;
		}
		catch (Exception x)
		{
%>
	<script>alert("Error restoring session:<%=x.getMessage().replace("\n","\\n")%>");</script>
<%
		}
	}
	try
	{
	   String synonyms = ResumeSorter.getWikiSynonyms(user, "Test");
	}
	catch (Exception x)
	{
	   x.printStackTrace();
	}

	if (false && postings.size() == 0)
	{
%>
	<script>window.location="upload_jobjosting.jsp"</script>
<%
		return;
	}
	String error = request.getParameter("error");
	if (error != null && error.length() > 0)
	{
%> <script>alert("<%=error.replace("\n","\\n")%>");</script> <%
	}
%>
<script>

	function showPosting(obj)
	{
		window.location = "job_posting_report.jsp?postingId=" + obj.options[obj.selectedIndex].value;
	}
	function deletePosting(id, postingId)
	{
		if (!confirm("Are you sure you wish delete all information for the Requisition '" + postingId + "' - including Resume scores, Selected phrases, All files and Data?"))
		{
			return;
		}
		window.location = "home.jsp?command=delete&postingId=" + id;
	}
</script>
	<style type="text/css" title="currentStyle">
			@import "js/table/css/demo_page.css";
			@import "js/table/css/demo_table_jui.css";
			@import "js/table/ui-lightness/jquery-ui-1.8.4.custom.css";
		</style>
		<script type="text/javascript" language="javascript" src="js/table/js/jquery.dataTables.js"></script>
<link type="text/css" href="../css/ui/jquery-blue.css" rel="stylesheet" />	
<div class="middle">
<div class="maintext_dashboard">
<%
	String name = user.getName();// What kind of stupid css is this? It screws up without the spaces.
	if (name.length() < 6) name = name + "&nbsp;&nbsp;&nbsp;";
%>
 <div class="sky_bar">
 	<h3>Dashboard </h3>
    <p>Welcome back <%=name%> !</p>
<% if (templates.size() > 0) { %>
	<a href="template_list.jsp">JOB TEMPLATES</a>
<% } %>
	<a href="resume_folder_list.jsp">RESUME FOLDERS</a>
    <a href="upload_jobposting.jsp">START NEW JOBS</a>
    <a href="#" class="selected">PREVIOUS JOBS</a>
 </div>
 
 <div class="text_dashboard">
<br />
<%
	if (error != null && error.length() > 0)
	{
%> <span class="error"><%=error%></span><%
	}
%>
<br/><br/>
 
 <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"aaSorting": [[ 2, "desc" ]],
					"aoColumns": [
								null,
								null,
								null,
								null,
								{ "bSortable": false }
								]
				});
			} );
			
	</script>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>Job Title</th>
			<th>Resumes</th>
			<th>Date </th>
			<th>Requisition / Job Posting</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<%
	for (int i = 0; i < postings.size(); i++)
	{
		List<ResumeScore> scores = new ResumeScore().getObjects("JOB_ID=" + postings.get(i).getId());
		Map<String, Map<String, ResumeScore>> resumeScores = new TreeMap<String, Map<String,ResumeScore>>();
		Map<String, ResumeScore> firstResume = null;
		for (ResumeScore score : scores)
		{
			String resume = score.getResumeFile();
			Map<String, ResumeScore> userScores = resumeScores.get(resume);
			if (userScores == null)
			{
				userScores = new TreeMap<String, ResumeScore>();
				if (firstResume == null)
				{
					firstResume =  userScores;
				}
				resumeScores.put(resume, userScores);
			}
			String userName = score.getUserName();
			userScores.put(userName, score); 
		}
		int count = resumeScores.keySet().size();
%>
  <tr align="center" style="font-weight:bold"  class="gradeC">
    <td><a href="job_posting.jsp?postingId=<%=postings.get(i).getId()%>" title="Resume Folder: <%=checkNull(postings.get(i).getResumeFolder(), "NA")%>"><%=checkNull(postings.get(i).getJobTitle(), postings.get(i).getPostingId())%></a></td>
	<td><%=count%></td>
    <td><%=checkNull(postings.get(i).getPostingDate(), "Unknown")%></td>
    <td><a href="get_jobposting.jsp?download=true&jobpostingName=<%=postings.get(i).getPostingFile()%>"><%=postings.get(i).getPostingId()%></a></td>
	<td class="center">  <img src="x.png" onclick='deletePosting(<%=postings.get(i).getId()%>,"<%=postings.get(i).getPostingId()%>")'/></td>
  </tr>
<%	} %>
	</tbody>
</table>
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
	<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    <script type="text/javascript">
        $(function() {
			
	        $("a[rel^='prettyPhoto']").prettyPhoto();

        });
    </script>
	<center>
<div style="width:480px;text-align: center;">
<b>Demo</b>
	<iframe width="480" height="270" src="https://www.youtube.com/embed/x7lDmTTcP4Y?feature=player_embedded" frameborder="0" allowfullscreen></iframe> 
	 <center>
<div class="clear"></div>
</div>
</div>
</div>


<%@include file="footer.jsp"%>
<br>
	 <!--div style="width:120px;float:left;margin-top:0px">
		<a href="http://youtu.be/x7lDmTTcP4Y" rel="prettyPhoto" title=""><img src="images/demo_icon.png" width=120px  /></a>
	</div-->
</body>
</html>
<%!
	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
	String checkNull(Object value, String defval)
	{
		if (value == null)
			return defval;
		else if (value instanceof Date)
			return dateformat.format(value);
		return value.toString();
	}
    int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>