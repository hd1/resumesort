<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.wordnet.model.WordNetDataProviderPool"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <!--[if lt IE 9]>
   <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="all.css" />
  <!--[if lt IE 9]>
  <link rel="stylesheet" href="ie.css" />
  <![endif]-->  
<link type="text/css" rel="stylesheet" href="css/phrases.css" />
<script src="phrases.js" type="text/javascript"></script>
<script type="text/javascript" src="ratingsys.js"></script> 
<script language="JavaScript" type="text/JavaScript" src="prototype.js"></script>
<script type="text/javascript" src="default.js"></script> 
<style>

.ttip {
	display: none;
	position: absolute;
	z-index: 3;
	border: solid 1px black;
	background-color: #DDFFFF;
	padding: 1;
	padding-left: 5;
	padding-right: 5;
	min-width: 300px;
}

.ttipBody {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 13px;
	padding: 0;
	margin: 0;
	max-height: 500px;
}

.resumePhrases {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 13px;
	padding: 0;
	margin: 0;
	height: 400px;
    overflow: auto;
}

</style>
</head>

<body>

<header id="header" class="central2">
  <h1 id="logo">Resume <strong>Sort</strong></h1>
  <nav id="user-nav"></nav>
</header>
<div id="siteTag">
  <h2 class="central2">The most advanced resume sorter on the planet</h2>
</div>

<%!private String login=""; %>
<div class="central2">
<div id='tt' class="ttip"><table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>
<div id='popup' class="ttip"><table border=0 width="300" id="tttable" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody"></TBODY></table>
</div>
<div id ="phrases">
<form method='post' action='Analyzer' name="phrasesform">

   <div id="suggestedphrases">
	<p><b>Select phrases for sorting/scoring resumes</b></p>
	<Table id="suggestedphraselist" >
	<tr>
	<th align=center style="display:none">Clear</th>
	<th colspan=2>Phrases from Job Posting</th>
	<th>Importance</th>
	<th>Must Have</th>
	</tr>
<%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
	if (user == null) return;
	JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	if (posting == null) return;
	boolean showPosting = true;
	if (!posting.getUserName().equals(user.getLogin()))
	{
		showPosting = false;
		SharedJobPosting sjp = (SharedJobPosting)session.getAttribute(SESSION_ATTRIBUTE.SHARED_POSTING.toString());
		if (sjp != null)
		{
			if (sjp.isPerm1() || sjp.isPerm3())
				showPosting = true;
		}
	}
	Collection<PhraseList> phraseList = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	Collection<PhraseList> nonLemmaPhraseLists = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString());
	DocumentRS sourceDocument = (Document) session.getAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString());
	Map<String,String> phrasesSynonyms = (Map<String,String>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());

	WordNetDataProviderPool.getInstance().acquireProvider();
	Map<String, String> contexts = ResumeReport.getPhrasesToContextMap(sourceDocument, nonLemmaPhraseLists);
	WordNetDataProviderPool.getInstance().releaseProvider();
	System.out.println("Contexts:" + contexts);
	int i = 0;
	Collection<PhraseList> lemmaPhraseLists = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString());
	for(PhraseList p : phraseList)
	{   
		//System.out.println("Phrase Buffer: " + p.iterator().next().getPhrase().getBuffer());
		//System.out.println("POS Sequence for phrase: " + p.iterator().next().getPhrase().getNgram().getPosSequence());
		String phrase = p.iterator().next().getPhrase().getBuffer().toString();
		String sentence = "";
		Iterator<PhraseList> iterator = nonLemmaPhraseLists.iterator();
		for(PhraseList lemmaPhraseList : lemmaPhraseLists)
		{
			PhraseList unlemma = iterator.next();
			String unlemmaText = unlemma.iterator().next().getPhrase().getBuffer();
			if (lemmaPhraseList.iterator().next().getPhrase().getBuffer().equalsIgnoreCase(phrase))
			{
				if (contexts != null)
					sentence = contexts.get(unlemmaText.toLowerCase().trim());
			}
		}

		if (sentence == null) sentence = "";
		String srccount = "" + sentence.split("_LIMIT_").length;
		if (srccount.length() > 10) srccount = "10";
%>
	<tr>
    <td align=center style="display:none">
    <input name="suggphrases_<%=100+i%>" id="suggphrases_<%=100+i%>" type="checkbox" onclick="if (!this.checked) unSelect(<%=100+i%>);if (this.checked) setRating(<%=100+i%>)" value="<%=phrase%>" />
	</td>
	<td nowrap>
<%	if (showPosting) { %>
	<img  src='tinyInfo.gif' onmouseover='handleTooTip2(event, "<%=sentence.replace('\'', '`')%>");'>
<%	}
		if (phrasesSynonyms.get(phrase.trim()) != null)
		{
			String synonymsStr = phrasesSynonyms.get(phrase.trim()).trim();
			if (synonymsStr.length() > 1)
			{
				String[] synonymns = synonymsStr.split("\\|");
				synonymsStr = synonymsStr.replace("|","_LIMIT_");
				int syncnt = synonymns.length;
				if (syncnt > 9) syncnt = 10;
%>
	<img  src='plus-square.png' height=12 title="Equivalent Phrases" onclick='getSynonyms(event, "<%=phrase%>", this);synPhrase="<%=phrase%>"'>
<%			} else { %>
	<img  src='plus-gsquare.png' height=12 title="Equivalent Phrases" onclick='getSynonyms(event, "<%=phrase%>", this);synPhrase="<%=phrase%>"'>
<%			} } else { %>
	<img  src='plus-gsquare.png' height=12 title="Equivalent Phrases" onclick='getSynonyms(event, "<%=phrase%>", this);synPhrase="<%=phrase%>"'>
<%		} %>
<%	if (user.isAdmin() || user.isTester()) { %>
	<img  src='x.png' height=12 onclick='deletePhrase("<%=phrase%>", <%=100+i%>);'>
<%		} %>
	</td>
	<td id="phrase<%=100+i%>">&nbsp;<%=phrase%>
	</td>
	<td>
	<div id="rateMe" title="Rate Me...">
	<a onclick="rateIt(this)" id="<%=100+i%>_1" title="Optionally nice" onmouseover="rating(this)" onmouseout="off(this)"></a>
    <a onclick="rateIt(this)" id="<%=100+i%>_2" title="Nice to have" onmouseover="rating(this)" onmouseout="off(this)"></a>
    <a onclick="rateIt(this)" id="<%=100+i%>_3" title="Good to have" onmouseover="rating(this)" onmouseout="off(this)"></a>
    <a onclick="rateIt(this)" id="<%=100+i%>_4" title="Should have" onmouseover="rating(this)" onmouseout="off(this)"></a>
    <a onclick="rateIt(this)" id="<%=100+i%>_5" title="Important" onmouseover="rating(this)" onmouseout="off(this)"></a>
	</div>
	<input type=hidden name="rating_<%=100+i%>" id="rating_<%=100+i%>" value="0">
    </td>
	<td align=center><img id="reqimg_<%=100+i%>" src="star_off.gif" onclick="setRequired(this, <%=100+i%>)"/><input type=hidden name="required_<%=100+i%>" id="required_<%=100+i%>" value="false" ></td>
	<td>&nbsp;&nbsp;<a id="clear<%=100+i%>" style="visibility:hidden;font-size:9px;font-weight:normal" href="#" onclick="clearSelection(<%=100+i%>)">Clear</a> </td>
    </tr>
<%  
	i++;
	} %>
    </table>
	</div>
<div id="userphrases">
	<p><b>Please enter your own phrases</b></p>
	<table id="userphraselist">
	<tr  style="display:inline">
	<th align=left width=70%>Your Phrase</th>
	<th align=right width=10%>Importance</th>
	<th width=10%>&nbsp;</th>
	<th align=left width=10%>Must Have</th>
	<th align=left width=10%>Tag</th>
	</tr>
<%	
	String disp = "inline";
	for (i = 0; i < 100; i++) 
	{
		if (i > 0) disp = "none";
%>
	<tr id="uptr<%=i%>" style="display:<%=disp%>">
	<td width=60% nowrap><img src="x.png" title="Remove Phrase" onclick="deleteUserPhrase(<%=i%>)">
	<img  src='plus-gsquare.png' height=12 title="Equivalent Phrases" onclick='getUserSynonyms(event, <%=i%>, this);'>&nbsp;
	<input type=text name="userphrase_<%=i%>" id="userphrase_<%=i%>" size=30  onkeypress="return checkenter(event, <%=i%>)" ></td>
	<td nowrap width=25%>
	<div id="rateMe" title="Rate Me...">
	<a onclick="rateIt(this);clearRequired2(<%=i%>)" id="<%=i%>_1" title="Optionally nice" onmouseover="rating(this)" onmouseout="off(this)"></a>
    <a onclick="rateIt(this);clearRequired2(<%=i%>)" id="<%=i%>_2" title="Nice to have" onmouseover="rating(this)" onmouseout="off(this)"></a>
    <a onclick="rateIt(this);clearRequired2(<%=i%>)" id="<%=i%>_3" title="Good to have" onmouseover="rating(this)" onmouseout="off(this)"></a>
    <a onclick="rateIt(this);clearRequired2(<%=i%>)" id="<%=i%>_4" title="Should have" onmouseover="rating(this)" onmouseout="off(this)"></a>
    <a onclick="rateIt(this);clearRequired2(<%=i%>)" id="<%=i%>_5" title="Important" onmouseover="rating(this)" onmouseout="off(this)"></a>
	</div>
	<input type=hidden name="rating_<%=i%>" id="rating_<%=i%>" value="0">
    </td>
	<td width=15%>&nbsp;&nbsp;<a id="clear<%=i%>" style="visibility:hidden;font-size:9px;font-weight:normal" href="#" onclick="clearSelection(<%=i%>);setRequired2(<%=i%>)">Clear</a> </td>
	<td align=center><img id="reqimg_<%=i%>" src="star_off.gif" onclick="setRequired(this, <%=i%>)"/><input type=hidden name="required_<%=i%>" id="required_<%=i%>" value="false" ></td>
	<td align=center><input type=checkbox name="tag_<%=i%>" id="tag_<%=i%>" value="tag" ></td>
	</tr>
<% } %>
	<tr>
	<td colspan=4 align=center nowrao>
	<input type="button" value="Add Phrase" onclick="addNewPhrase()"  />
	<input type="button" id="resPhrasesBut" value="Select From Resume Phrases" onclick="showResumePhrases(event, this)"  />
	<input type="button" id="keyNamesBut" value="Select From Key Names" onclick="showTagList(event, this)"  />
	</td>
	</tr>
	</table>
</div>
<script>
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g,"");
	}
	var nophrases = 1;
	function addNewPhrase(phrase)
	{
		var uptr = document.getElementById("uptr" + (nophrases-1));
		if (uptr.style.display == 'none')
		{
			uptr.style.display = 'inline';
			var inuptr = document.getElementById("userphrase_" + (nophrases-1));
			inuptr.value = "";
			if (phrase != null)
			{
				inuptr.value = phrase;
			}
			inuptr.focus();
			return (nophrases-1);
		}
		uptr = document.getElementById("uptr" + nophrases);
		if (uptr != null)
		{
			uptr.style.display = 'inline';
			var inuptr = document.getElementById("userphrase_" + nophrases);
			inuptr.value = "";
			if (phrase != null)
			{
				inuptr.value = phrase;
			}
			inuptr.focus();
			nophrases++;
			return (nophrases-1);
		}
	}
	function deleteUserPhrase(num)
	{
		var uptr = document.getElementById("uptr" + num);
		if (uptr != null)
		{
			uptr.style.display = 'none';
			var inuptr = document.getElementById("userphrase_" + num);
			inuptr.value = "";
		}
	}

	function checkenter(event, num)
	{
		if (event == null)
			event = window.event;
		var keyCode = event.keyCode
		if (keyCode == null)
			keyCode = event.which;
		if(keyCode == 13)
		{
			if (num == nophrases-1)
			{
				addNewPhrase();
			}
			return true;
		}
		return true;
	}

	function addResumePhrase(phrase, idx)
	{
		var uptr = document.getElementById("uptr" + (nophrases-1));
		var inuptr = document.getElementById("userphrase_" + (nophrases-1));
		if (uptr.style.display != 'none' && inuptr.value == '')
		{
			inuptr.value = phrase;
		}
		else
			addNewPhrase(phrase);
		var resPhraseTr = document.getElementById("resPhrase" + idx);
		if (resPhraseTr != null)
		{
			resPhraseTr.style.display = 'none';
		}
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_resume_phrase.jsp?phrase=" + escape(phrase);
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
	}

	function addKeyName(phrase, idx)
	{
		var uptr = document.getElementById("uptr" + (nophrases-1));
		var inuptr = document.getElementById("userphrase_" + (nophrases-1));
		var idx = 0;
		if (uptr.style.display != 'none' && inuptr.value == '')
		{
			inuptr.value = phrase;
			idx = nophrases-1;
		}
		else
		{
			idx = addNewPhrase(phrase);
		}
		var tagptr = document.getElementById("tag_" + idx);
		if (tagptr != null)
		{
			tagptr.checked = true;
		}
	}
	function deleteResumePhrase(phrase, idx)
	{
		if (!confirm("Do you want to mark  '" + phrase + "' as invalid and remove it from processing in the future?"))
		{
			return;
		}
		var resPhraseTr = document.getElementById("resPhrase" + idx);
		if (resPhraseTr != null)
		{
			resPhraseTr.style.display = 'none';
		}
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_resume_phrase.jsp?permanent=true&phrase=" + escape(phrase);
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
	}
	function deletePhrase(phrase, idx)
	{
		if (!confirm("Do you want to mark  '" + phrase + "' as invalid and remove it from processing in the future?"))
		{
			return;
		}
		clearSelection(idx);
		var phraseTd = document.getElementById("phrase" + idx);
		if (phraseTd != null)
		{
			phraseTd.style.textDecoration = 'line-through';
		}
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();

		if (xmlhttp == null) {
			alert("Your browser does not support XMLHTTP!");
			return;
		}
		var url = "remove_jobposting_phrase.jsp?phrase=" + escape(phrase);
		xmlhttp.onreadystatechange = dummy;
		xmlhttp.open("POST", url, false);
		xmlhttp.send(null);
	}
	
	var synPhrase;
	function getUserSynonyms(event, idx, img)
	{
		var phrase = document.getElementById("userphrase_" + idx);
		if (phrase == null) return;
		if (phrase.value == '')
		{
			alert("Please enter a phrase");
			return;
		}
		synPhrase = phrase.value;
		getSynonyms(event, phrase.value, this);
	}

	function checkAddenter(event)
	{
		if (event == null)
			event = window.event;
		var keyCode = event.keyCode
		if (keyCode == null)
			keyCode = event.which;
		if(keyCode == 13)
		{
			addSynonym(synPhrase);
			return true;
		}
		return true;
	}

	var resumePhrases = false;
	function showResumePhrases(event, button)
	{
		if (resumePhrases)
		{
			 closeResumePhrases();
			 return;
		}
		if (tagList)
		{
			 closeResumePhrases();
		}
		resumePhrases = true;
		showResumePhrasesPopup(event);
		button.value="Close Pop Up";
	}
	function closeResumePhrases()
	{
		var button = document.getElementById("resPhrasesBut");
		button.value="Select from Resume Phrases";
		button = document.getElementById("keyNamesBut");
		button.value="Select from Key Names";
		tthide();
		resumePhrases = false;
		tagList = false;
	}
	var tagList = false;
	function showTagList(event, button)
	{
		if (tagList)
		{
			 closeResumePhrases();
			 return;
		}
		if (resumePhrases)
		{
			 closeResumePhrases();
		}
		tagList = true;
		showTagListPopup(event);
		button.value="Close Pop Up";
	}
</script>
<div align="right" style="display:block; width:50%;">
		&nbsp;<p><br><br><input type="button" onclick="submitSelection()" value="Analyze/Sort Resumes" /></div>
    </form>
  <div id="loading" style="visibility: hidden">
    <p>Processing...</p>
  </div>
</div>
</div>
<script>
	function unSelect(ind)
	{
		var req =document.getElementById("required_" + ind);
		if (req != null)
			req.value="false";
		var reqimg =document.getElementById("reqimg_" + ind);
		if (reqimg != null)
			reqimg.src="star_off.gif";
		clearRating(ind);
	}
	function setRequired(image, ind)
	{
		var url = image.src;
		if (url.indexOf("star_off") != -1)
		{
			image.src = "star_on.gif";
			var req =document.getElementById("required_" + ind);
			req.value="true";
			var sel =document.getElementById("suggphrases_" + ind);
			if (sel != null)
				sel.checked=true;
			clearRating(ind);
		}
		else
		{
			image.src = "star_off.gif";
			var req =document.getElementById("required_" + ind);
			req.value="false";
		}
	}
	function clearRequired2(ind)
	{
		return;
		var image = document.getElementById("reqimg2_" + ind);
		if (image == null) return;
		var url = image.src;
		if (url.indexOf("star_on") != -1)
		{
			image.src = "star_off.gif";
			var req =document.getElementById("required2_" + ind);
			req.value="false";
		}
	}
	function setRequired2(ind)
	{
		return;
		var image = document.getElementById("reqimg2_" + ind);
		if (image == null) return;
		var url = image.src;
		if (url.indexOf("star_off") != -1)
		{
			image.src = "star_on.gif";
			var req =document.getElementById("required2_" + ind);
			req.value="true";
		}
	}
	function clearSelection(ind)
	{
		clearRating(ind);
		var checkbox = document.getElementById("suggphrases_" + ind);
		if (checkbox != null)
		{
			checkbox.checked = false;
		}
		var image = document.getElementById("reqimg_" + ind);
		if (image != null)
			image.src = "star_off.gif";
		var req =document.getElementById("required_" + ind);
		if (req != null)
			req.value="false";
		var clear = document.getElementById("clear" + ind);
		if (clear != null)
		{
			clear.style.visibility='hidden';
		}
	}
	function contains(a, obj) 
	{     
		for (var i = 0; i < a.length; i++) 
		{         
			if (a[i] === obj) 
				return true;         
		}     
		return false; 
	}
	function submitSelection()
	{
		var selPhrases = new Array();
		var elems = document.phrasesform.elements;
		var selected = false;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name.indexOf("suggphrases_") == 0 && elems[i].checked)
			{
				selected = true;
				var str = new String(elems[i].value).toLowerCase();
				selPhrases.push(str);
			}
			if (name.indexOf("userphrase_") == 0 && elems[i].value != "")
			{
				var str = new String(elems[i].value).toLowerCase();
				if (contains(selPhrases, str) || contains(selPhrases, str + " "))
				{
					alert("Phrase:'" + elems[i].value +"' is already in the list. Please remove it.");
					return;
				}
				var idx = name.substring(11);
				var rating = document.getElementById("rating_" + idx);
				var required = document.getElementById("required_" + idx);
				var tagged = document.getElementById("tag_" + idx);
				if (rating.value == "0" && required.value == 'false' && !tagged.checked)
				{
					alert("Please rate phrase:'" + elems[i].value +"'");
					return;
				}
				selPhrases.push(str);
			}
		}
		if (!selected)
		{
			alert("Please select some phrases")
			return;
		}
		document.getElementById("loading").style.visibility = "visible";
		document.phrasesform.submit();
	}

<% 
	// This is for testing only
	if (false && user != null && user.getEmail().startsWith("dev.gude@text")) { 
%>
	var j = 5;
	for (var i = 0; i < 15 ; i++ )
	{
		var idStr = "" + (100 + i) + "_" + j;
		var img = document.getElementById(idStr);
		if (img != null)
		{
			rating(img)
			rateIt(img);
		}
		j--;
		if (j < 1) j = 5;
	}
<%	} %>
</script>
  
<%@include file="rq_footer.jsp"%>
</body>
</html>