<%@include file="header.lay"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>

<script type="text/javascript">
	
	
	function  doSubmit() 
	{		
		if (isValid())
		{
			return true;
		}		
		return false;
	}

	function  isValid()
	{				
		if (!document.subscribe.privacy.checked)
		{		
			alert("Please make sure you have read and agree to the terms of service.");
			return false;
		}

		if (document.subscribe.passwordconfirm.value!=document.subscribe.password.value)
		{
			alert("Your passwords do not match!");
			return false;
		}

		if (document.subscribe.name.value=="" )
		{
			alert("Please enter your name.");
			return false;
		}

		if (document.subscribe.email.value=="")
		{
			alert("Please enter your email.");
			return false;
		}

		if (document.subscribe.password.value=="")
		{
			alert("Please enter your password.");
			return false;
		}
		
		return true;
	}
	
</script>


<br>
<br>

<center>
<%

	String error;
	if ((error = (String) session.getAttribute(SESSION_ATTRIBUTE.ERROR.toString())) != null)
	{
		out.println("<FONT style=\"BACKGROUND-COLOR: RED\">" + error + "</FONT>");
		out.println("<br>");
		out.println("<br>");
		session.removeAttribute(SESSION_ATTRIBUTE.ERROR.toString());
	}
	
%> 
<br>
<br>
</center>


<form method="post" action="Subscribe" name="subscribe">
<table border="0" cellpadding="0" cellspacing="0" width="97%">
	<tr>
		<td><span class="orange">Name:</span></td>
		<td><input type="text" size="75" name="name" value=""
			maxlength="200"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Email:</span></td>
		<td><input type="text" size="75" name="email" value=""
			maxlength="150"></td>
	</tr>
	<!--  <tr>
		<td>Login:</td>
		<td><input type="text" size="25" name="login" value=""
			maxlength="20"></td>
	</tr> -->
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Password:</span></td>
		<td><input type="password" size="25" name="password" value=""
			maxlength="20"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Password:</span></td>
		<td><input type="password" size="25" name="passwordconfirm"
			value="" maxlength="20"> <span class="green">
		(Confirm)</span></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><input type="checkbox" size="25" name="privacy"> I have read and agreed to the <b><a
			href="http://www.resumetuner.com/terms.pdf">Terms of Service.</a></b>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<center>

<input type="image" alt="signup" src="https://sandbox.google.com/checkout/buttons/buy.gif?merchant_id=<%=merchantId%>&amp;w=117&amp;h=48&amp;style=white&amp;variant=text&amp;loc=en_US" onclick="return doSubmit()" name="signup" id="signup"/>
</center>
</form>

<%@include file="footer.lay"%>