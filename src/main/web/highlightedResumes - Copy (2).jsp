<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.subscription.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textnomics.data.*"%>
<script language="JavaScript" type="text/JavaScript" src="prototype.js"></script>
<%
	if (user == null)
	{
		response.sendRedirect("Login.jsp");
		return;
	}
	JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
	String postingId = "";
	if (posting != null)
		postingId = posting.getPostingId();
	boolean showPosting = true;
	if ("false".equals(request.getParameter("showPosting"))) showPosting = false;
	List<File> resumeFiles = (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
	List<Double> scores = (List<Double>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
	if (resumeFiles == null)
	{
		System.out.println("No results found");
		return;
	}
	String error = request.getParameter("error");
	if (error == null || error.trim().length() == 0)
		error = (String) session.getAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());
	if (error != null && error.length() > 0)
	{
%> <script>alert("<%=error.replace("\n","\\n")%>");</script> 
	<!--br><font color=red><b><%=error.replace("\n","<br>")%></b></font><br-->
<%
	}
	boolean paidSubscriber = SubscriptionService.isValidSubscription(user);
	if (false && !paidSubscriber)
	{
%>
	<script>window.location="rt_request_payment.jsp?type=0";</script>
<%	
		return;
	}
	//temporary
	 paidSubscriber = true;
	String defValue = "Enter Email Address";
	List<String> sortedFiles = new ArrayList<String>();
	for (File resumeFile: resumeFiles)
		sortedFiles.add(resumeFile.getName());
	List<Double> sortedScores = new ArrayList<Double>();
	if (scores != null) sortedScores.addAll(scores);
	List<Integer> indexes = new ArrayList<Integer>();
	for (int i = 0; i < sortedFiles.size(); i++)
		indexes.add(i);
	for (int i = 0; i < sortedScores.size(); i++)
	{
		for (int j = i; j < sortedScores.size(); j++)
		{
			Double scorej = sortedScores.get(j);
			if (scorej == null) scorej = 0.0;
			Double scorei = sortedScores.get(i);
			if (scorei == null) scorei = 0.0;
			if (scorej > scorei)
			{
				Double score = sortedScores.get(i);
				String resume = sortedFiles.get(i);
				Integer index = indexes.get(i);

				sortedScores.set(i, sortedScores.get(j));
				sortedFiles.set(i, sortedFiles.get(j));
				indexes.set(i, indexes.get(j));

				sortedScores.set(j, score);
				sortedFiles.set(j, resume);
				indexes.set(j, index);
			}
		}
	}
	System.out.println("SortedFiles:" + sortedFiles.size() + " sortedScores:" + sortedScores.size());
	List<Map<Phrase,Integer>> matchedPhraseList = (List<Map<Phrase,Integer>>) session.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
	Set<String> tags = (Set<String>) session.getAttribute(SESSION_ATTRIBUTE.TAGS.toString());
	if (tags == null) tags = new HashSet<String>();
	DecimalFormat decformat = new DecimalFormat("########0.00");
	boolean procError = false;
	int rank = 0;
	int numResumes = sortedFiles.size();
	int numToShow = numResumes;
	if (!paidSubscriber)
	{
		numToShow = (numToShow*2)/5;
		if (numToShow == 0) numToShow = 1;
		if (numToShow > 10) numToShow = 10;
	}
	int index = -1;
	int skip = numResumes/5;
	if (skip <= 1) skip = 2;
	List<String> unRatedResumes = new ArrayList<String>();
%>
<script>
	function showResume(ind, error)
	{
		if (error)
		{
			alert("There was an error processing this resume");
			return;
		}
		var link = "ResumeViewer?viewFileIndex=" + ind;
		var newwin = window.open(link, "HighLightedResume");
		newwin.focus();
	}
	var busy = false;
	function sendResumes()
	{
		if (busy)
		{
			alert("Function in progress, please wait...")
			return;
		}
		var elems = document.resumeListForm.elements;
		var selected = false;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name == "emailResumeIdx" && elems[i].checked)
			{
				selected = true;
			}
		}
		if (!selected)
		{
			alert("Please select some resumes")
			return;
		}
		busy = true;
		var button = document.getElementById("emailResumes");
		button.value = "Your email is being sent";
		var url = "email_resumes.jsp?random=" + new Date();
		var form = document.resumeListForm;
		new Ajax.Request(url, { 
			method:'post',
			parameters : Form.serialize(form),
			onSuccess: function(transport){
				
				var response = transport.responseText;
				// ignore response
				if (response != null)
					alert("Email has been sent");
				else
					alert("Error sending email");
				busy = false;
				button.value = "Email Selected Resumes";
			}
		});
	}
	function selectAll(obj)
	{
		var selcbx = document.getElementById("selall");
		var elems = document.resumeListForm.elements;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			if (name == "emailResumeIdx")
			{
				if (selcbx.checked)
				{
					elems[i].checked = true;
				}
				else
				{
					elems[i].checked = false;
				}
			}
		}
	}

	function clearDefault(inp)
	{
		if (inp.value == '<%=defValue%>')
		{
			inp.value = '';
		}
	}
	var sentEmails = new Array();
	function sharePosting()
	{
		var elems = document.resumeListForm.elements;
		var entered = false;
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			var value = elems[i].value;
			if (name == "shareemail" && value != '<%=defValue%>')
			{
				entered = true;
				if (value == '<%=user.getEmail()%>')
				{
					alert("You can not assign this job posting to yourself");
					elems[i].focus();
					return;
				}
				if (value.indexOf("@") == -1 || value.indexOf(".") == -1)
				{
					alert("Please enter a valid email address");
					elems[i].focus();
					return;
				}
				for (var j = 0; j < sentEmails.length; j++)
				{
					if (value == sentEmails[j])
					{
						alert(value + " has already been notified");
						elems[i].focus();
						return;
					}
				}
			}
		}
		if (!entered)
		{
			alert("Please enter email addresses")
			return;
		}
		for (var i = 0; i < elems.length ; i++)
		{
			var name = elems[i].name;
			var value = elems[i].value;
			if (name == "shareemail" && value != '<%=defValue%>')
			{
				sentEmails.push(value);
			}
		}
		var button = document.getElementById("sendEmail");
		button.value = "Your email is being sent";
		var url = "share_job_posting.jsp?random=" + new Date();
		var form = document.resumeListForm;
		new Ajax.Request(url, { 
			method:'post',
			parameters : Form.serialize(form),
			onSuccess: function(transport){
				
				var response = transport.responseText;
				// ignore response
				if (response != null)
				{
					if (response.indexOf("Error") != -1)
					{
						alert("Error sending emails:" + response);
					}
					else
						alert("Emails have been sent");
				}
				else
					alert("Error sending emails");
				button.value = "Send more emails";
			}
		});
	}
</script>
<link href="css/smart_wizard.css" rel="stylesheet" type="text/css">
<div class="middle">
<div class="maintext_dashboard">
 
 <div class="sky_bar">
 	<h3>Dashboard </h3>
    <p>Welcome back Tassavor !</p>
    <a href="#"  class="selected">START NEW JOBS</a>
    <a href="home.jsp">PREVIOUS JOBS</a>
 </div>
 
 <div class="text_dashboard">
<table align="center" border="0" cellpadding="0" cellspacing="0">
<tr><td> 
<!-- Smart Wizard -->
  		<div id="wizard" class="swMain">
  			<ul class="anchor">
  				<li><a href="upload_resumes.jsp">
                <label class="stepNumber">1</label>
                <span class="stepDesc">
                   Upload Resumes <br/> 
                   <small>Ste 1 description</small>              
                </span>
            </a></li>
  				<li><a href="upload_jobposting.jsp">
                <label class="stepNumber">2</label>
                <span class="stepDesc">
                   Upload Job Posting<br />
                   <small>Upload Job Posting</small>
                </span>
            </a></li>
  				<li><a class="mini" href="#step-3" >
                <label class="stepNumber">3</label>
                <span class="stepDesc">
                   Select Phrases
                </span>                   
             </a></li>
  				<li><a class="mini selected" href="#step-4">                
				 <label class="stepNumber">4</label>
				   <span class="stepDesc">
                   Rank Resumes
					</span>                   
            </a></li>
  			</ul>
  			<div class="stepContainer">	
            <div class="content">            	
			<div style="margin-left:30px; margin-top:30px; margin-bottom:30px;">
				<h2>Resumes for Job Posting: <% if (showPosting) { %><a href="get_jobposting.jsp?download=true&jobpostingName=<%=posting.getPostingFile()%>"><% } %><%=postingId%></a></h2>
				<form name=resumeListForm>
<% if (!paidSubscriber) { %>
				<table class=resumestbl>
				<tr>
				<td>
				<b>Since you are not a paid subscriber, we have not shown all the resumes processed. Please click <a href="rt_request_payment.jsp?type=1"><b><u>here</u></b></a> to purchase a subscription in order to view all processed resumes</b>
				</td>
				</tr>
				</table>
				<br>
				<br>
<% } %>
				<div class="table_01">
<%
	for (String resume : sortedFiles)
	{
		index++;
		rank++;
		if (!paidSubscriber && (numToShow <= 0 || index < (numResumes*2)/5 || rank%skip != 0))
		{
			unRatedResumes.add(resume);
			continue;
		}
		int slash = resume.lastIndexOf("\\");
		if (slash != -1) resume = resume.substring(slash+1);
		slash = resume.lastIndexOf("/");
		if (slash != -1) resume = resume.substring(slash+1);
		String scoreStr = "";
		if (sortedScores.size() > index)
		{
			if(sortedScores.get(index) == null) continue;
			scoreStr = "&nbsp;&nbsp;(Score:" + decformat.format(sortedScores.get(index)) +")";
			if (scoreStr.indexOf("999") != -1)
			{
				scoreStr = "Error";
				procError = true;
			}
			else if (!user.isAdmin())
				scoreStr = ""; 
		}
		scoreStr = "" + rank + " / " + sortedFiles.size() + " " + scoreStr;
		int ind = indexes.get(index);
		String tagMatch = "";
		if (matchedPhraseList != null && matchedPhraseList.size() > ind && matchedPhraseList.get(ind) != null)
		{	
			Set<Phrase> matchedPhrases = matchedPhraseList.get(ind).keySet();
			for (Phrase phrase: matchedPhrases)
			{
				String pstr = phrase.getBuffer().trim();
				boolean tagged = false;
				for (String tag: tags)
					if (tag.trim().equalsIgnoreCase(pstr)) tagged = true;
				if (tagged && tagMatch.indexOf("," + pstr) == -1)
				{	
					tagMatch = tagMatch + "," + pstr;
				}
			}
			if (tagMatch.length() > 1) tagMatch = "("+ tagMatch.substring(1) + ")";
		}
		String fullname = resume;
		if (resume.length() > 50) resume = resume.substring(0,47) + "...";
%>
                	<div class="resumeRow">
                        <div class="resumeCheck"><input name="emailResumeIdx" type="checkbox" value="<%=indexes.get(index)+1%>" /></div>
                        <div class="resumeName"><a href = "#" onclick="showResume(<%=indexes.get(index)%>, <%=procError%>)" title="<%=fullname%>"><%=resume%> <font color=red size=-2><%=tagMatch%></font></div>
                        <div class="resumeScore"><%=scoreStr%></div>
                    </div>
<%
	}
	Collections.sort(unRatedResumes);
	for (String resume : unRatedResumes)
	{
%>
                    <div class="resumeRow">
                        <div class="resumeCheck">&nbsp;</div>
                        <div class="resumeName"><font color=gray><%=resume%></font></div>
                        <div class="resumeScore"></div>
                    </div>
<%	} %>
                    <div style="clear:both;"></div>
                    <br/>
<% if (sortedScores.size() > 0) { %>
					<a class="orangeBtn" href="javascript:sendResumes()">Email Selected Resumes</a>
<% } %>
                    
				</div>
                    <div style="clear:both;"></div>
<br/><br/>
<% //if (paidSubscriber) { %>
<% if (user.isAdmin() || user.isMarketing()) { %>
                <h2>Assign Resume Evaluation</h2>

<table class="resumestbl">
<tbody>
<tr><th width="118" rowspan="2" align="center"></th><th colspan="3" align="center">Permissions</th></tr>
<tr><td width="90">Job Posting</td><td width="92">Resumes</td><td width="93">All Details</td></tr>
<%
	for (int i = 0; i < 5; i++)
	{
%>
	<tr>
	<td align=left><input name=shareemail type=text size=20 value="Enter Email Address" onfocus="clearDefault(this)"></td>
	<td><input name=perm1_<%=i%> type=checkbox size=30 value="1" ></td>
	<td><input name=perm2_<%=i%> type=checkbox size=30 value="1" ></td>
	<td><input name=perm3_<%=i%> type=checkbox size=30 value="1" ></td>
	</tr>
<%
	}
%>

<tr><td colspan="100%"><br><a class="orangeBtn">Email Friends</a>&nbsp;<a class="orangeBtn">Reset</a></td></tr>
</tbody></table>                
<% } %>
                
            </div>                                
			</div>
      		 </div>
  			                      

  		</div>
<!-- End SmartWizard Content -->  		
 		
</td></tr>
</table> 
 <script type="text/javascript">
	$(function() {    // Makes sure the code contained doesn't run until
					  //     all the DOM elements have loaded
	
		$('#colorselector').change(function(){
			$('.colors').hide();
			$('#' + $(this).val()).show();
		});
	
	});
 </script>
<div class="clear"></div>
</div>
</div>
    </div>

</form>
<%@include file="footer.jsp"%>
</body>
</html>