<!DOCTYPE html>
<html lang="en">
<head>
  <title>ResumeSort - the most advanced resume sorting technology on the planet.</title>
  <meta charset="utf-8" />
  <meta http-equiv="cache-control" content="no-store"/>
  <meta http-equiv="pragma" content="no-cache"/>
  <meta http-equiv="expires" content="0"/>
	<script language="JavaScript" type="text/JavaScript" src="prototype.js"></script>
</head>
<body onunload="checkSave()" onload="getMessage();">
<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.datadriven.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%
		String phrase = request.getParameter("phrase");
		String notInWordNetStr = request.getParameter("notInWordNet");
		if (notInWordNetStr == null)
			notInWordNetStr = "false";
		boolean notInWordNet = new Boolean(notInWordNetStr);
		String tester = request.getParameter("tester");
		int offset = getInt(request.getParameter("offset"));
		int maxrows = getInt(request.getParameter("maxrows"));
		if (maxrows == 0) maxrows = 10;
		System.out.println("get_synonyms: phrase=" + phrase);
		if (phrase == null) phrase = "";
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());

		if (user == null) 
		{
%>
		<script>alert("Your session has expired.");</script>
<%
			response.sendRedirect(request.getContextPath() + "/Login.jsp?redirectURL=wikicleanup.jsp");
			return;
		}
		if (!user.isAdmin() && !user.isTester())
		{
%>
		<script>alert("No permissions for:<%=user.getLogin()%>")</script>
<%
			return;
		}
		if (tester == null || tester.trim().length() == 0) tester = user.getLogin();
		String command = request.getParameter("command");
		if ("save".equals(command))
		{
			Map<String, String[]> paramMap = request.getParameterMap();
			for (String param: paramMap.keySet())
			{
				String[] values = paramMap.get(param);
				if (param.startsWith("val"))
				{
					String[] phrases = param.split("\\|");
					boolean invalid = true;
					if (values[0].equals("false")) invalid = false;
					if (invalid)
					{
						String criteria = "code = " + phrases[1].trim()
								+ " and phrase = " + UserDBAccess.toSQL(phrases[2].trim())
								+ " and synonym is null and user_name = " + UserDBAccess.toSQL(tester);
						if (phrases.length > 3)
							criteria = "code = " + phrases[1].trim()
								+ " and phrase = " + UserDBAccess.toSQL(phrases[2].trim())
								+ " and synonym = " + UserDBAccess.toSQL(phrases[3].trim())
								+ " and user_name = " + UserDBAccess.toSQL(tester);
						List<RejectedSynonym> rejected = new RejectedSynonym().getObjects(criteria);
						if (rejected.size() == 0)
						{
							RejectedSynonym rs = new RejectedSynonym();
							rs.setCode(getInt(phrases[1].trim()));
							rs.setPhrase(phrases[2].trim());
							if (phrases.length > 3)
								rs.setSynonym(phrases[3].trim());
							if (values[0].equals("suggest")) rs.setNotExact(true);
							if (values[0].equals("hyponym")) rs.setHyponym(true);
							rs.setUserName(tester);
							rs.insert();
						}
						else
						{
							RejectedSynonym rs = rejected.get(0);
							if (values[0].equals("suggest")) 
								rs.setNotExact(true);
							else
								rs.setNotExact(false);
							if (values[0].equals("hyponym")) 
								rs.setHyponym(true);
							else
								rs.setHyponym(false);
							rs.update();
						}
					}
					else
					{
						String criteria = "code = " + phrases[1].trim()
								+ " and phrase = " + UserDBAccess.toSQL(phrases[2].trim())
								+ " and synonym is null and user_name = " + UserDBAccess.toSQL(tester);
						if (phrases.length > 3)
							criteria = "phrase = " + UserDBAccess.toSQL(phrases[2].trim())
								//+ " and code = " + phrases[1].trim()
								+ " and synonym = " + UserDBAccess.toSQL(phrases[3].trim())
								+ " and user_name = " + UserDBAccess.toSQL(tester);
						new UserDBAccess().deleteData(new RejectedSynonym().getDB_TABLE(), criteria);
					}

				}
				else if (param.startsWith("add"))
				{
					String[] phrases = param.split("\\|");
					if (values[0].trim().length() == 0) continue;
					String[] synonyms = values[0].trim().split(",");
					if (param.startsWith("addc"))
					{
						synonyms = new String[1];
						synonyms[0] = values[0].trim();
					}
					for (String synonym: synonyms)
					{
						if (synonym.length() == 0) continue;
						List<AddedSynonym> exist = new AddedSynonym().getObjects("phrase = " + UserDBAccess.toSQL(phrases[2].trim()) + " and synonym = " + UserDBAccess.toSQL(synonym.trim()) + " and user_name =" + UserDBAccess.toSQL(tester));
						if (exist.size() == 0)
						{
							AddedSynonym rs = new AddedSynonym();
							rs.setPhrase(phrases[2].trim());
							rs.setCode(getInt(phrases[1].trim()));
							rs.setSynonym(synonym.trim());
							if (param.startsWith("adds"))
								rs.setNotExact(true);
							if (param.startsWith("addc"))
								rs.setComment(true);
							rs.setUserName(tester);
							rs.insert();
						}
					}
				}
				else if (param.startsWith("reviewed"))
				{
					String[] phrases = param.split("\\|");
					if (values[0].trim().length() == 0) continue;
					List<CheckedEntry> exist = new CheckedEntry().getObjects("phrase = " + UserDBAccess.toSQL(phrases[2].trim()) + " and code = " + phrases[1].trim() + " and user_name =" + UserDBAccess.toSQL(tester));
					if (exist.size() == 0)
					{
						CheckedEntry rs = new CheckedEntry();
						rs.setPhrase(phrases[2].trim());
						rs.setCode(getInt(phrases[1].trim()));
						rs.setUserName(tester);
						rs.insert();
					}
				}
				else if (param.startsWith("del"))
				{
					String[] phrases = param.split("\\|");
					int count  = new AddedSynonym().deleteObjects("phrase = " + UserDBAccess.toSQL(phrases[2].trim()) + " and synonym = " + UserDBAccess.toSQL(phrases[3].trim()) + " and user_name =" + UserDBAccess.toSQL(tester));
				}

			}
		}
        //List<WikiPhrase> wikiPhrases = new ArrayList();
		String notinwn = "";
		String sql = "mainphrase = true and text like " + WikiPhrase.toSQL(phrase.toLowerCase().trim() + "%");
		if (notInWordNet)
		{
			sql = sql + " and not_in_wn = true order by frequency desc";
			notinwn = "checked";
		}
		else
			sql = sql + "  order by frequency desc";
		
		List<WikiPhrase> wikiPhrases = new DBAccess().getObjects(new WikiPhrase(), sql, offset, maxrows);
		int done = new CheckedEntry().getCount("user_name = " + UserDBAccess.toSQL(tester));
%>
<div id=SYSMESSAGE></div>
<form name=wikiform method=post>
<%
		if (user.isAdmin())
		{
%> <Select name=tester onchange="search()"><%
			UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();
			UserIterator users = provider.getUsers("is_tester = true","name");
			User current;
			int i = 0;
			while (users.hasNext())
			{
				current = users.next();
				String selected = "";
				if (current.getLogin().equals(tester))
					selected = "selected";
%>
	<option value=<%=current.getLogin()%> <%=selected%>><%=current.getName()%></option>
<%
			}
			users.close();
			UserDataProviderPool.getInstance().releaseProvider();
%> </Select>
<%
		}
%>
<table align=center border=1 cellpaddng=2 cellspacing=0 ><TBODY id="ttBody">
<tr>
<td colspan=100% align=center>
Current Page #: <%=(offset/maxrows)+1%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Goto Page:<input name=pageNo2  size=2 value="1"><input type=button value=Go onclick="gotoPage2()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Search:<input name=phrase2  size=20 value="<%=phrase%>" onchange="document.wikiform.phrase.value=document.wikiform.phrase2.value"><input type=button value=Go onclick="search2()"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Reviewed: <%=done%> (since 11/4)
</td>
</tr>
<tr style="font-weight:bold;background-color:lightblue">
<th align=center colspan=2><b>Wiki Title</b> (<input type=checkbox name=notInWordNet value="true" <%=notinwn%> onclick="search()">Only those not In WordNet)</th><th colspan=2>Redirects</th>
</tr>
<tr>
<td colspan=100% align=center>
<input type=button value="Prev Page" onclick="prevPage()">&nbsp;&nbsp;
<input type=button value=Save onclick="save()">&nbsp;&nbsp;
<input type=button value="Next Page" onclick="nextPage()">&nbsp;&nbsp;
</td>
</tr>
<!--tr>
<td align=center>Phrase</td><td>Invalid</td><td align=center >Phrase</td><td>Invalid</td>
</tr-->
<%
		String color="";
		for (int j = 0; j < wikiPhrases.size(); j++)
		{
			WikiPhrase wikiTitle = wikiPhrases.get(j);
			long code = wikiTitle.getCode();
			String wkphrase = wikiTitle.getText();
            List<WikiPhrase> equivalents  = new WikiPhrase().getObjects("code = " + wikiTitle.getCode() + " and id !=" + wikiTitle.getId());
			List<AddedSynonym> addSyns  = new AddedSynonym().getObjects("phrase = " + UserDBAccess.toSQL(wikiTitle.getText()) + " and user_name =" + UserDBAccess.toSQL(tester));
			int checkedcnt  = new CheckedEntry().getCount("phrase = " + UserDBAccess.toSQL(wikiTitle.getText()) + " and user_name =" + UserDBAccess.toSQL(tester));
			String reviewed = "";
			if (checkedcnt > 0) reviewed = "checked disabled";
			int invcnt  = new RejectedSynonym().getCount("phrase = " + UserDBAccess.toSQL(wkphrase.trim()) + " and synonym is null and user_name =" + UserDBAccess.toSQL(tester));
			String checked1 = "checked";
			String checked2 = "";
			String style1 ="style='text-decoration:none'";
			if (invcnt > 0)
			{
				checked1 = "";
				checked2 = "checked";
				style1 ="style='text-decoration:line-through,underline'";
			}
			else if (checkedcnt > 0)
			{
				style1 ="style='text-decoration:underline'";
			}

			if (color.length() == 0)
				color="bgcolor='lightgrey'";
			else
				color = "";
			int numrows = equivalents.size() + addSyns.size() + 3;
%>
	<tr <%=color%> id="syntr<%=j%>">
	<td id="td<%=j%>" valign=top rowspan="<%=numrows%>" <%=style1%> nowrap><%=wkphrase%> 
	<input type=checkbox id="cbx<%=j%>" name="reviewed|<%=code%>|<%=wikiTitle.getText()%>" <%=reviewed%> title="Reviewed/Done" onclick="changesMade=true;setReviewed(this, 'td<%=j%>')"></td>
	<td valign=top rowspan="<%=numrows%>" nowrap><input type=radio <%=checked1%> value=false name="val|<%=code%>|<%=wikiTitle.getText()%>" onclick="setChangesMade(<%=j%>);setValid('td<%=j%>')">Valid&nbsp;&nbsp;&nbsp;<input type=radio <%=checked2%> value=true name="val|<%=code%>|<%=wikiTitle.getText()%>" onclick="setChangesMade(<%=j%>);setInValid('td<%=j%>')">Invalid</td>
<%
		for (int i = 0; i < equivalents.size(); i++)
		{
			List<RejectedSynonym> rejected = new RejectedSynonym().getObjects("phrase = " + UserDBAccess.toSQL(wkphrase.trim()) + " and synonym = " + UserDBAccess.toSQL(equivalents.get(i).getText().trim()) + " and user_name =" + UserDBAccess.toSQL(tester));
			checked1 = "checked";
			checked2 = "";
			String checked3 = "";
			String checked4 = "";
			String style2 = style1;
			if (rejected.size() > 0)
			{
				checked1 = "";
				if (rejected.get(0).isNotExact())
				{	
					checked3 = "checked";
					style2 ="style='text-decoration:underline'";
				}
				else if (rejected.get(0).isHyponym())
				{	
					checked4 = "checked";
					style2 ="style='text-decoration:underline'";
				}
				else
				{	
					checked2 = "checked";
					style2 ="style='text-decoration:line-through'";
				}
			}
			if (i != 0)
			{
%>
			<tr <%=color%>>
<%			} %>
	<td  id="td<%=j%>-<%=i%>" <%=style2%>><%=equivalents.get(i).getText()%></td>
	<td valign=top nowrap><input type=radio <%=checked1%> value=false name="val|<%=code%>|<%=wikiTitle.getText()%>|<%=equivalents.get(i).getText()%>" onclick="setChangesMade(<%=j%>);setValid('td<%=j%>-<%=i%>')"> Valid&nbsp;&nbsp;&nbsp;<input type=radio <%=checked2%> value=true name="val|<%=code%>|<%=wikiTitle.getText()%>|<%=equivalents.get(i).getText()%>" onclick="setChangesMade(<%=j%>);setInValid('td<%=j%>-<%=i%>')">Invalid&nbsp;&nbsp;&nbsp;<input type=radio <%=checked3%> value=suggest name="val|<%=code%>|<%=wikiTitle.getText()%>|<%=equivalents.get(i).getText()%>" onclick="setChangesMade(<%=j%>);setSuggest('td<%=j%>-<%=i%>')">Suggestion&nbsp;&nbsp;&nbsp;<input type=radio <%=checked4%> value=hyponym name="val|<%=code%>|<%=wikiTitle.getText()%>|<%=equivalents.get(i).getText()%>" onclick="setChangesMade(<%=j%>);setSuggest('td<%=j%>-<%=i%>')">Hyponym</td>
	</tr>
<%
		} 
		for (int i = 0; i < addSyns.size(); i++)
		{
			String suggest =  "";
			if (addSyns.get(i).isNotExact())
				suggest = "(Suggestion)";
			if (addSyns.get(i).isComment())
				suggest = "(Comment)";
%>
	<tr <%=color%>>
	<td  id="td<%=j%>-<%=i%>" ><%=addSyns.get(i).getSynonym()%></td>
	<td valign=top nowrap><input type=checkbox value=delete name="del|<%=code%>|<%=wikiTitle.getText()%>|<%=addSyns.get(i).getSynonym()%>" onclick="setChangesMade(<%=j%>);"> Delete <%=suggest%></td>
	</tr>
<% } %>
	<tr <%=color%>>
	<td colspan=2>Add equivalents: <input name="add|<%=code%>|<%=wikiTitle.getText()%>" value="" size=60 onchange="setChangesMade(<%=j%>);"></td>
	</tr>
	<tr <%=color%>>
	<td colspan=2>Add suggestions:<input name="adds|<%=code%>|<%=wikiTitle.getText()%>" value="" size=60 onchange="setChangesMade(<%=j%>);"></td>
	</tr>
	<tr <%=color%>>
	<td colspan=2>Add Comment:&nbsp;&nbsp;&nbsp;<input name="addc|<%=code%>|<%=wikiTitle.getText()%>" value="" size=60 onchange="setChangesMade(<%=j%>);"></td>
	</tr>
<%
	}
%>
	<tr>
	<td colspan=100% align=center>
	<input type=button value="Prev Page" onclick="prevPage()">&nbsp;&nbsp;
	<input type=button value=Save onclick="save()">&nbsp;&nbsp;
	<input type=button value="Next Page" onclick="nextPage()">&nbsp;&nbsp;
	</td>
	</tr>
	<tr>
	<td colspan=100% align=center>
	Current Page #: <%=(offset/maxrows)+1%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Goto Page:<input name=pageNo  size=2 value="1"><input type=button value=Go onclick="gotoPage()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Show Per Page:<input name=maxrows  size=2 value="<%=maxrows%>"><input type=button value=Go onclick="save()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Search:<input name=phrase  size=20 value="<%=phrase%>"><input type=button value=Go onclick="search()">
	</td>
	</tr>
	</TBODY></table>
	<input name=offset type=hidden value="<%=offset%>">
	<input name=command type=hidden value="">
	<form>
<script>
	function getMessageX()
	{
		alert('test');
	}
	var changesMade = false;
	function setChangesMade(idx)
	{
		changesMade = true;
		var elem = document.getElementById("cbx" + idx);
		if (elem != null)
			elem.checked = true;
		elem = document.getElementById("td" + idx);
		if (elem != null)
			elem.style.textDecoration = 'underline';
	}
	var busy = false;
	function save()
	{
		if (busy)
		{
			alert("Still processing.... Please wait");
			return;
		}
		busy = true;
		changesMade = false;
		document.wikiform.command.value = 'save';
		document.wikiform.submit();
	}
	function checkSave()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return false;
			}
			changesMade = false;
		}
	}
	function search()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return;
			}
			changesMade = false;
		}
		document.wikiform.offset.value = 0;
		document.wikiform.submit();
	}
	function search2()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return;
			}
			changesMade = false;
		}
		document.wikiform.phrase.value = document.wikiform.phrase2.value;
		document.wikiform.offset.value = 0;
		document.wikiform.submit();
	}
	function nextPage()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return;
			}
			changesMade = false;
		}
		document.wikiform.offset.value = <%=offset%> + <%=maxrows%>;
		document.wikiform.submit();
	}
	function prevPage()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return;
			}
			changesMade = false;
		}
		var newoff = <%=offset%> - <%=maxrows%>;
		if (newoff < 0) newoff = 0;
		document.wikiform.offset.value = newoff;
		document.wikiform.submit();
	}
	function gotoPage()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return;
			}
			changesMade = false;
		}
		var pageNo = parseInt(document.wikiform.pageNo.value);
		var newoff = <%=maxrows%>*(pageNo-1);
		if (newoff < 0) newoff = 0;
		document.wikiform.offset.value = newoff;
		document.wikiform.submit();
	}
	function gotoPage2()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return;
			}
			changesMade = false;
		}
		var pageNo = parseInt(document.wikiform.pageNo2.value);
		var newoff = <%=maxrows%>*(pageNo-1);
		if (newoff < 0) newoff = 0;
		document.wikiform.offset.value = newoff;
		document.wikiform.submit();
	}
	function setValid(id)
	{
		var elem = document.getElementById(id);
		elem.style.textDecoration = 'none';
	}
	function setSuggest(id)
	{
		var elem = document.getElementById(id);
		elem.style.textDecoration = 'underline';
	}
	function setInValid(id)
	{
		var elem = document.getElementById(id);
		elem.style.textDecoration = 'line-through';
	}
	function setReviewed(cbox, id)
	{
		var elem = document.getElementById(id);
		if (cbox.checked)
		{
			elem.style.textDecoration = 'underline';
		}
		else
		{
			elem.style.textDecoration = 'none';
		}
	}
	var timeOutId = null;
	function getMessage()
	{
		var message = document.getElementById("SYSMESSAGE");
		url = "get_system_msg.jsp?param=<%=done%>"
		new Ajax.Request(url, { 
			method:'get',
			parameters : '',
			onSuccess: function(transport){
				
				var response = transport.responseText;
				if(response != null)
				{
					if (message != null)
					{
						message.innerHTML = response;
					}
				}
				else
				{
					if (message != null)
					{
						message.innerHTML = "";
					}
				}
			}
		});

		if (timeOutId != null)
		{
			clearTimeout(timeOutId);
		}
		timeOutId = setTimeout ( getMessage, 60*1000 );
	}
	getMessage();
</script>
  
</body>
</html>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>