<%@include file="rq_header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.checkout.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.subscription.*"%>

<script type="text/javascript">
	
	
	function  doSubmit() 
	{		
		if (isValid())
		{
			return true;
		}		
		return false;
	}

	function  isValid()
	{				

		//if (document.subscribe.name.value=="" )
		//{
		//	alert("Please enter your name.");
		//	return false;
		//}

		//if (document.subscribe.email.value=="")
		//{
		//	alert("Please enter your email.");
		//	return false;
		//}

		if (document.subscribe.password.value=="")
		{
			alert("Please enter your password.");
			return false;
		}
		var selected = false;
		var plans = document.getElementsByName("plan");
		if (plans != null)
		{
			for (var i = 0; i < plans.length ; i++ )
			{
				if (plans[i].checked)
				{
					selected = true;
				}
			}
		}
		if (!selected && (document.subscribe.price == null || document.subscribe.price.value == ""))
		{
			alert("Please select a subscription plan");
			return false;
		}
		if (!document.subscribe.privacy.checked)
		{		
			alert("Please make sure you have read and agree to the terms of service.");
			return false;
		}
		
		return true;
	}
	
</script>


<br>
<br>

<div class="central">

<center>
<%

	String error;
	if ((error = (String) session.getAttribute(SESSION_ATTRIBUTE.ERROR.toString())) != null)
	{
		out.println("<FONT style=\"BACKGROUND-COLOR: RED\">" + error + "</FONT>");
		out.println("<br>");
		out.println("<br>");
		session.removeAttribute(SESSION_ATTRIBUTE.ERROR.toString());
	}
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
	if (user == null)
	{
		response.sendRedirect("Login.jsp");
		return;
	}
	List<SubscriptionPlan> plans = new SubscriptionPlan().getObjects("1 = 1 order by display_order");
	if (plans.size() == 0)
	{
%>
	<script>alert("No subscription plans defined. Please contact system administrator");</script>
<%
	}
%> 
<br>
<br>
</center>
<h1>You have exhausted your number of Free/Paid Processed Resumes. <br><br>Please use Google Checkout below to pay for further use of the service. </h1>
<br>
<br>

<form method="post" action="Subscribe" name="subscribe">
<table border="1" class=subscriptiontbl cellpadding="4" cellspacing="0">
	<tr>
		<td><span class="orange">Name:</span></td>
		<td colspan=3><%=user.getName()%></td>
	</tr>
	<tr>
		<td colspan=4>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Email:</span></td>
		<td colspan=3><%=user.getEmail()%></td>
	</tr>
	<tr>
		<td colspan=4>&nbsp;</td>
	</tr>
	<tr>
		<td><span class="orange">Password:</span></td>
		<td colspan=3><input type="password" size="25" name="password" value=""
			maxlength="20"></td>
	</tr>
	<tr>
		<td colspan=4>&nbsp;</td>
	</tr>
	<tr align=left>
		<td colspan=4>
<!--form action="https://sandbox.google.com/checkout/api/checkout/v2/checkoutForm/Merchant/828317989500506" id="BB_BuyButtonForm" method="post" name="BB_BuyButtonForm" target="_top">
    <table cellpadding="5" cellspacing="0" width="1%">
        <tr>
            <td align="right" width="1%">
                <select name="item_selection_1">
                    <option value="1">$1.95 - Single Resume Package</option>
                    <option value="2">$19.95 - 30 Resumes or 30 days</option>
                    <option value="3">$34.95 - 60 Resumes or 60 days</option>
                </select>
                <input name="item_option_name_1" type="hidden" value="Single Resume Package"/>
                <input name="item_option_price_1" type="hidden" value="1.95"/>
                <input name="item_option_description_1" type="hidden" value=""/>
                <input name="item_option_quantity_1" type="hidden" value="1"/>
                <input name="item_option_currency_1" type="hidden" value="USD"/>
                <input name="shopping-cart.item-options.items.item-1.digital-content.url" type="hidden" value="http://www.resumetuner.com"/>
                <input name="item_option_name_2" type="hidden" value="30 Resumes or 30 days"/>
                <input name="item_option_price_2" type="hidden" value="19.95"/>
                <input name="item_option_description_2" type="hidden" value=""/>
                <input name="item_option_quantity_2" type="hidden" value="1"/>
                <input name="item_option_currency_2" type="hidden" value="USD"/>
                <input name="shopping-cart.item-options.items.item-2.digital-content.url" type="hidden" value="http://www.resumetuner.com"/>
                <input name="item_option_name_3" type="hidden" value="60 Resumes or 60 days"/>
                <input name="item_option_price_3" type="hidden" value="34.95"/>
                <input name="item_option_description_3" type="hidden" value=""/>
                <input name="item_option_quantity_3" type="hidden" value="1"/>
                <input name="item_option_currency_3" type="hidden" value="USD"/>
                <input name="shopping-cart.item-options.items.item-3.digital-content.url" type="hidden" value="http://www.resumetuner.com"/>
            </td>
            <td align="left" width="1%">
                <input alt="" src="https://sandbox.google.com/checkout/buttons/buy.gif?merchant_id=828317989500506&amp;w=117&amp;h=48&amp;style=white&amp;variant=text&amp;loc=en_US" type="image"/>
            </td>
        </tr>
    </table>
</form -->
		</td>
	</tr>
<%
	Date today = new Date();
	String type = request.getParameter("type");
	if (type == null || type.equals("0"))
	{
		for (SubscriptionPlan plan: plans)
		{
			String plantype = "";
			if (plan.getValidFrom().after(today)) continue;
			if (plan.getValidTo().before(today)) continue;
			if (plan.getDays() > 0)
				plantype = plantype + plan.getDays() + " days";
			if (plan.getNumresumes() > 0)
			{
				if (plantype.length() > 0) plantype = plantype + " or ";
				if (plan.getNumresumes() == 1)
					plantype = plantype + plan.getNumresumes() + " Resume";
				else
					plantype = plantype + plan.getNumresumes() + " Resumes";
			}
%>
	<tr valign=middle>
		<td colspan=2><input type=radio name=plan value=<%=plan.getId()%>><%=plantype%></td>
		<td align=left colspan=2>$<%=plan.getPrice()%></td>
	</tr>
<% 
		}
	}
	else
	{
		List<File> resumeFiles = (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		double price = resumeFiles.size()*SubscriptionService.PRICE_PER_RESUME;
%>
	<tr valign=middle>
		<td colspan=2><input type=text readonly name="description" value='<%="" + resumeFiles.size() + " Resumes"%>'></td>
		<td align=left colspan=2>$<input type=text readonly name="price" value='<%=new DecimalFormat("##.00").format(price)%>'></td>
	</tr>
<%
	}
%>
	<tr>
		<td colspan=4>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan=4><input type="checkbox" size="25" name="privacy"> I have read and agreed to the <b><a
			href="Textnomics_Terms_of_Service.pdf">Terms of Service.</a></b>
		</td>
	</tr>
	<tr>
		<td colspan=4>&nbsp;</td>
	</tr>
</table>

<center>

<input type="image" alt="signup" src="https://sandbox.google.com/checkout/buttons/buy.gif?merchant_id=828317989500506&amp;w=117&amp;h=48&amp;style=white&amp;variant=text&amp;loc=en_US" onclick="return doSubmit()" name="signup" id="signup"/>
</center>
</form>
</div>
<%@include file="rq_footer.jsp"%>
  
</body>
</html>