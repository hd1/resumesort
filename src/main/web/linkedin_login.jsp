<%@ page import="java.util.*"%><%@ page import="com.textonomics.*"%><%@ page import="com.textonomics.web.*"%><%@ page import="com.textonomics.user.*"%><%
		System.out.println("Entered linkedin login");
		String id = request.getParameter("id").trim();
		String name = request.getParameter("name").trim();
		String email = request.getParameter("email");
		if (email == null) 
			email = id;
		else if (email.indexOf("@") == -1)
			throw new Exception("Invalid email:" + email);
		String code = request.getParameter("code");
		if (code == null) code = "";
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());	
		System.out.println("Session user:" + user);
		if (user == null)
		{
			UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();//Get a connection for this thread
			user = userDataProvider.getUserByLinkedinId(id);
			if (user == null) user = userDataProvider.getUser(email);
			if (user == null)
			{
				System.out.println("Registering:" + name + " id:" + id);
				user = new User();
				user.setEmail(email);
				user.setLogin(email);
				user.setName(name);
				user.setParticipationCode(code);
				user.setLinkedinId(id);
				if (user.getId() <= 0)
					user.setPassword("linkedin" + SendPassword.generatePassword());
				user.setState(1);
				user.setType("Linkedin");
				if (!email.equals(id))
 					user.setLastLogin(new Date());
				try
				{
					userDataProvider.insertUser(user);
					userDataProvider.commit();
				}
				catch (Exception x)
				{
					x.printStackTrace();
					out.write("Error creating user:" + x.getMessage());
					return;
				}
				if (email.equals(id))
				{
					out.write("newuser");
				}
				else
				{					
					out.write("");
					session.setAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString(), user);
				}
				return;
			}
			else if (user.getEmail().equals(id))
			{
				if (email.equals(id))
				{
					System.out.println("email:" + id);
					out.write("newuser");
					return;
				}
				else
				{
					System.out.println("email:" + email);
					user.setEmail(email);
					user.setLogin(email);
					userDataProvider.updateUser(user);
 					user.setLastLogin(new Date());
					userDataProvider.updateUserField(user, "lastLogin");
 					userDataProvider.commit();
					System.out.println("Logging in:" + name + " email:" + user.getEmail());
					out.write("");
					session.setAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString(), user);
				}
			}
			else
			{
				user.setLastLogin(new Date());
				userDataProvider.updateUserField(user, "lastLogin");
				userDataProvider.commit();
				System.out.println("Logging in:" + name + " email:" + user.getEmail());
				out.write("");
				session.setAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString(), user);
			}
			UserDataProviderPool.getInstance().releaseProvider();
		}
%>