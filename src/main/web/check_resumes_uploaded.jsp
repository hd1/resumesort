<%@ page import="java.util.*"%><%@ page import="java.io.*"%><%@ page import="com.textonomics.*"%><%@ page import="com.textonomics.web.*"%><%@ page import="com.textonomics.user.*"%><%@ page import="com.textonomics.wordnet.model.*"%><%
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (user == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		List<File> resumeFiles = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		if (resumeFiles != null) 
			out.print(resumeFiles.size());
		else
			out.print("");
		return;
%>