<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%
		String phrase = request.getParameter("phrase");
		String command = request.getParameter("command");
		System.out.println("reject_synonyms: command=" + command + " phrase=" + phrase + " idx=" + request.getParameter("idx"));
		if (phrase == null) return;
		int idx = getInt(request.getParameter("idx"));
		if (idx == 0) return;
		Map<String,String> phrasesSynonyms = (Map<String,String>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());
		Map<String,Set<String>> deletedSynonyms = (Map<String,Set<String>>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_DELETED_SYNONYMS.toString());
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (phrasesSynonyms == null || user == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		Set<String> deleted = null;
		if (deletedSynonyms == null)
		{
			deletedSynonyms = new HashMap<String,Set<String>>();
			session.setAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_DELETED_SYNONYMS.toString(),deletedSynonyms);
		}
		else
			deleted = deletedSynonyms.get(phrase.trim());
		if (deleted == null)
			deleted = new HashSet<String>();
		deletedSynonyms.put(phrase.trim(), deleted);
		String[] synonyms = new String[0];
		if (phrasesSynonyms.get(phrase.trim()) != null)
		{
			String synonymsStr = phrasesSynonyms.get(phrase.trim()).trim();
			if (synonymsStr.length() > 1)
			{
				synonyms = synonymsStr.split("\\|");
			}
		}
		String synonymsStr = "";
		String rejectedSyn = "";
		for (int i = 0; i < synonyms.length; i++)
		{
			if (i+1 == idx)
			{
				rejectedSyn = synonyms[i];
				continue;
			}
			synonymsStr = synonymsStr + "|" + synonyms[i];
		}
		System.out.println("synonymsStr:" +synonymsStr);
		if (synonymsStr.length() > 1) synonymsStr = synonymsStr.substring(1);
		//phrasesSynonyms.put(phrase.trim(), synonymsStr);
		if ("delete".equals(command))
		{
			deleted.add(rejectedSyn);
			RejectedSynonym rs = new RejectedSynonym();
			rs.setPhrase(phrase.trim());
			rs.setSynonym(rejectedSyn);
			rs.setUserName(user.getLogin());
			rs.insert();
		}
		else
		{
			deleted.remove(rejectedSyn);
			RejectedSynonym rs = new RejectedSynonym();
			rs.setPhrase(phrase.trim());
			rs.setSynonym(rejectedSyn);
			rs.setUserName(user.getLogin());
			rs.removeFromCache();
			String criteria = "phrase = " + UserDBAccess.toSQL(phrase.trim())
					+ " and synonym = " + UserDBAccess.toSQL(rejectedSyn)
					+ " and user_name = " + UserDBAccess.toSQL(user.getLogin());
			new UserDBAccess().deleteData(new RejectedSynonym().getDB_TABLE(), criteria);
			//AddedSynonym as = new AddedSynonym();
			//as.setPhrase(phrase.trim());
			//as.setSynonym(rejectedSyn);
			//as.setUserName(user.getLogin());
			//new UserDBAccess().deleteData(new AddedSynonym().getDB_TABLE(), criteria);
		}
%>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>