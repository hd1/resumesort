<%@ page import="sun.misc.BASE64Decoder"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%
	String code = request.getParameter("code");
	if (code == null) code = "free_trial";
	String COOKIENAME = "rcwCookie";
	if ("true".equals(request.getParameter("logout")) && session != null)
	{
			session.removeAttribute(SESSION_ATTRIBUTE.SUGGESTER.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.TARGET_FILE.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.REQUEST_STAMP.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.FILTER_TYPE.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.OPTIONS_LIST.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.HELP_LIST.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.EXTRAS_INDEX.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.LAST_SUGGESTION_INDEX.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.RANK_MAP.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.KEY_PHRASE_IDENTIFIER.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			session.invalidate();
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	<meta name="google-site-verification" content="gWafhLWoBQdACp7n1uxuan9YzL0ZOkT8R0cYWd51aZM" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Recruitment Software, Online Recruiting Software, Applicant Tracking System, Resume Screening, Resume Parsing, Applicant Management, Hiring and Staffing, semantic search" /> 
	<meta name="description" content="ResumeSort is the most advanced resume screening and recruitment software in the world.  Our SaaS allows you to find the most qualified applicant screening not just for keywords but for the meaning associated with those key phrases in their resumes.  Never lose the most qualified candidate again.  Try our software for free." /> 
	<title>ResumeSort - the most advanced resume sorting technology on the planet.</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
    <script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="js/jquery.tooltip.js"></script>
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>

	<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

		
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="js/jquery.lavalamp.min.js"></script>
    
    <script type="text/javascript">
        $(function() {
            $("#1, #2, #3").lavaLamp({
                fx: "backout",
                speed: 700,
                click: function(event, menuItem) {
                    return false;
                }
            });
			
	        //$("a[rel^='prettyPhoto']").prettyPhoto();
			$("a[rel^='prettyPhoto']").prettyPhoto({
				default_width: 600,
				default_height: 430
			});
        });
    </script>
    </head>

<body>
<div class="topbg">
      <div class="middle">
    <div class="logo"></div>
    <div class="logotext">We read resumes, so you don't have to!</div>
    <div class="logg">
          <div class="register"><a href="signup.jsp?code=<%=code%>">REGISTER</a></div>
          <div class="login"><a href="signin.jsp?code=<%=code%>">LOGIN</a></div>
        </div>
    <ul class="lavaLampNoImage" id="2">
          <li class="current"><a href="#">Home</a></li>
          <li><a href="#">SUPPORT</a></li>
          <li><a href="http://blog.resumesort.com">BLOG</a></li>
          <li><a href="#">PRICING & SIGNUP</a></li>
        </ul>
    <div class="clear"></div>
    <hr class="hr" />
    <div class="themost"></div>
    <div class="indexpart"> 
    		<a href="http://www.youtube.com/watch?v=Qb2jAh5RBmU" rel="prettyPhoto" title=""><img src="images/badplay2.png" width="320"  /></a>
          <div class="try"><a href="signup.jsp?code=<%=code%>">&nbsp;&nbsp;Try it, it is free!</a></div>
        </div>
    <div class="indexpart">
          <div class="threeeindex">
        <div class="copy"></div>
        <h3>Upload Job Posting</h3>
        <div class="clear"></div>
        <div class="upload"></div>
	    <h3>Upload/Email Resumes</h3>
        <div class="clear"></div>
        <div class="Analyze"></div>
        <h3>Analyze Resumes</h3>
        <div class="clear"></div>
        <div class="sharing"></div>
        <h3>Ask friends for help</h3>
      </div>
	   </div>
	 <div style="width:120px;float:left;margin-top:90px">
		<!--a href="http://youtu.be/dXtPGmmyITc" rel="prettyPhoto" title=""><img src="images/demo_icon.png" width=120px  /></a-->
	</div>
  </div>
    </div>
<div id="phrasespop" style="display:none;" ><p><b>Thank you for coming to ResumeSort.com. 
<br><br>
The registration page already includes your <i><u>free</u></i> participation code.<br><br>You may share this code with any friends/colleagues whom you'd like to help you with candidate selection or invite to the site.
<br><br>
Enjoy using ResumeSort; we look forward to your feedback.
<br><br>- Your ResumeSort.com team</b>
</div>    
<div class="middle">
      <div class="maintext">
    <h2>What others are saying about us:</h2>
    <img src="images/examiner.png" />
    <h4>The Examiner. </h4>
    "Give your resume a tune up" 

    <div class="clear"></div>
    <img src="images/businessj.png" />
    <h4>The SJ Business Journal.</h4>
"Textnomics Inc. helps both job seekers and employers" 

    <div class="clear"></div>
  </div>
   
    </div>
<%@include file="footer.jsp"%>

<%// Cookies
Cookie cookies[] = request.getCookies();
Cookie myCookie = null;
boolean existsCookie = false;
if (cookies != null)
{
	for (int i = 0; i < cookies.length; i++)
	{
		if (cookies[i].getName().equals(COOKIENAME))
		{
			myCookie = cookies[i];
			existsCookie = true;
			break;
		}
	}
}

int cookieCount;
// Cookie exists
if (existsCookie)
{
	try
	{
            BASE64Decoder decoder = new BASE64Decoder();
		String value = new String(decoder.decodeBuffer(myCookie.getValue()));
		String[] split = value.split(":");
		String login = split[0];
		myCookie.setMaxAge(Integer.MAX_VALUE);
		response.addCookie(myCookie);
		
		//TO DO REDIRECT SENDING LOGIN
		%>
<input type="hidden" name="login" value='<%out.write(login); %>'
	maxlength="50" />
<%
		request.getSession().setAttribute("login",login);
		//response.sendRedirect("signin.jsp");
	}
	catch (Exception ex)
	{
		myCookie.setMaxAge(0);
		response.addCookie(myCookie);
		ex.printStackTrace();
	}
}
// Create cookie
else
{
//Do nothing
} 
if (false && code.trim().length() > 0)
{
%>
<script>
		$('#phrasespop').dialog({
			autoOpen: false,
			width: 460
		});
	$('#phrasespop').dialog('open');
</script>
<!-- Start of StatCounter Code for Default Guide --> 
<script type="text/javascript"> var sc_project=8237804; var sc_invisible=1; var sc_security="b718fdb0"; </script> 
<script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script>
<noscript><div class="statcounter"><a title="drupal stats"
href="http://statcounter.com/drupal/" target="_blank"><img class="statcounter" src="http://c.statcounter.com/8237804/0/b718fdb0/1/"
alt="drupal stats"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
<% } %>
</body>
</html>
