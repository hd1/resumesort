<%@ page import="java.util.*"%><%@ page import="java.io.*"%><%@ page import="com.textonomics.*"%><%@ page import="com.textonomics.web.*"%><%@ page import="com.textonomics.user.*"%><%@ page import="com.textonomics.wordnet.model.*"%><%
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		System.out.println("Entered get resume phrases");
		if (user == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		if (!user.isAdmin())
		{
%>
<script>alert("You have no permissions")</script>
<%
			return;
		}
		String sysmsg = request.getParameter("SYSTEM_MESSAGE");
		if (sysmsg != null)
		{
			ResumeCakeWalkConfigurator.systemMessage = sysmsg;
		}
		else
			sysmsg = ResumeCakeWalkConfigurator.systemMessage;
		if (sysmsg == null) sysmsg = "";
%>
<!DOCTYPE html>
<html lang="en">
<body>
<br>
<br>
<form name=msg method=post>
<center>Enter System Message:<input name=SYSTEM_MESSAGE value="<%=sysmsg%>" size=60></center>
<br>
<br>
<center><input type=submit value=Submit>&nbsp;<input type=reset value=Reset></center>
</form>
</body>
</html>