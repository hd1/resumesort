<%@ page import="java.util.*"%><%@ page import="com.textonomics.*"%><%@ page import="com.textonomics.web.*"%><%@ page import="com.textonomics.user.*"%><%
		System.out.println("Entered google login");
		String error = request.getParameter("error");
		if (error != null && error.equals("access_denied"))
		{
			System.out.println("Access Denied");
			return;
		}
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		if (email != null)
		{
			UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();//Get a connection for this thread
			User user = userDataProvider.getUser(email);
			if (user == null)
			{
				System.out.println("Registering:" + name + " email:" + email);
				user = new User();
				user.setEmail(email);
				user.setLogin(email);
				user.setName(name);
				user.setPassword("google" + SendPassword.generatePassword());
				user.setState(1);
				user.setType("Google");
				userDataProvider.insertUser(user);
				out.write("newuser");
			}
			else
			{
				System.out.println("Logging in:" + name + " email:" + email);
				out.write("");
			}
			user.setLastLogin(new Date());
			userDataProvider.updateUserField(user, "lastLogin");
			userDataProvider.commit();
			session.setAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString(), user);
			UserDataProviderPool.getInstance().releaseProvider();
			response.sendRedirect("home.jsp");
		}
		// Else this is reponse from google auth call
		// then Parse the queryString in javascript and call google to get email and name
%>
<script>
//String:  state=/profile&access_token=ya29.AHES6ZQAG9IMI0id-EpZfqn4Uq4UyvdoSWJRT_mQUsjIcLLsyH0Wlg&token_type=Bearer&expires_in=3600
// First, parse the query string
	var params = {}, queryString = location.hash.substring(1),
		regex = /([^&=]+)=([^&]*)/g, m;
	while (m = regex.exec(queryString)) {
	  params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
	}
// Response from google will be:
//{
// "id": "102792185801765262906",
// "email": "services@resumecakewalk.com",
// "verified_email": true,
// "name": "services services",
// "given_name": "services",
// "family_name": "services"
//}
	if (typeof(params.access_token) != "undefined")
	{
		var xmlhttp;
		if (xmlhttp == null)
			xmlhttp = GetXmlHttpObject();
		if (xmlhttp != null) {
			var url = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + params.access_token;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var resp = xmlhttp.responseText;
			var respObj = JSON.parse(resp);
			window.location="google_login.jsp?email=" + respObj.email + "&name=" + respObj.name;
		}
	}
	function GetXmlHttpObject() {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			return new XMLHttpRequest();
		}
		if (window.ActiveXObject) {
			// code for IE6, IE5
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
		return null;
	}     
</script>