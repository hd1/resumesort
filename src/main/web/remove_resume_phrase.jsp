<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%@ page import="com.textnomics.data.*"%>
<%
		System.out.println("Entered Remove resume phrase");
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		Map<String,Integer> resumePhrases = (Map<String,Integer>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
		if (resumePhrases == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		String phrase = request.getParameter("phrase");
		if (phrase == null) return;
		System.out.println("Removing resume phrase:" + phrase);
		resumePhrases.remove(phrase);
		if ("true".equals(request.getParameter("permanent")))
		{
			if (user.isAdmin())
			{
				resumePhrases.remove(phrase);
				DeletedPhrase deleted = new DeletedPhrase();
				deleted.setPhrase(phrase.trim());
				deleted.setUserName(user.getLogin());
				deleted.insert();
			}
			else
			{
				resumePhrases.remove(phrase);
				UserDeletedPhrase deleted = new UserDeletedPhrase();
				deleted.setPhrase(phrase.trim());
				deleted.setUserName(user.getLogin());
				deleted.insert();
			}
		}
		return;
%>