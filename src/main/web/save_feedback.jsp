<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%
		System.out.println("Entered save feedback");
		String phraseIdx = request.getParameter("idx");
		String feedback = request.getParameter("feedback");
		String command = request.getParameter("command");
		if (phraseIdx == null) return;
		List<String> optionsList = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.OPTIONS_LIST.toString());
		if (optionsList == null) return;
		List<String> extrasIndex = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.EXTRAS_INDEX.toString());
		List<String> deletedList = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.DELETED_LIST.toString());
		String options = optionsList.get(getInt(phraseIdx));
		String[] list = optionsList.get(getInt(phraseIdx)).split(" _LIMIT_ ");
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		try
		{
			PhraseSuggester<? extends SuggestionCluster> suggester = 
				(PhraseSuggester<? extends SuggestionCluster>) session.getAttribute(SESSION_ATTRIBUTE.SUGGESTER.toString());
			Map<Phrase, SuggestionSet> suggestionMap = suggester.getSuggestionMap();
			Set<Phrase> targetPhrases = suggestionMap.keySet();
			int i = 0;
			String targetPhrase = "";
			String suggestedPhrase = "";
			String targetSentence = "";
			String sourceSentence = "";
			for (Phrase phrase : targetPhrases)
			{
				if (i == getInt(phraseIdx))
				{
					SuggestionSet suggSet = suggestionMap.get(phrase);
					targetSentence = suggester.getTarget().getSentences().get(phrase.getSentence()).toString();
					targetPhrase = phrase.getBuffer();
					if ("save".equals(command) && targetPhrase.length() > 0)
					{
						System.out.println("---------------- Calling wordnet provider");
						UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();
						provider.insertMismatchedPhrase(targetPhrase, "", targetSentence, "", user.getLogin(), feedback);
						provider.commit();
					}
				}
				i++;
			}
		}
		catch (UserDataProviderException e)
		{
			e.printStackTrace();
		}
		finally
		{
			UserDataProviderPool.getInstance().releaseProvider();//Always release the provider
		}
%>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>