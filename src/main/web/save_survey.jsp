<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.user.*"%>
<%
		System.out.println("Entered save survey");
		String userName = request.getParameter("userName");
		String uploadDir = request.getParameter("uploadDir");
		String[] questions = request.getParameterValues("question");
		if (questions == null) return;
		String[] ratings = request.getParameterValues("rating");
		String comments = request.getParameter("comments");
		Date now = new Date();
		SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
		File surveyFile = PlatformProperties.getInstance().getResourceFile(uploadDir + "/" + timestamp.format(now) + "_ surveyresults.txt");
		PrintWriter writer = new PrintWriter(new FileWriter(surveyFile));
		for (int i = 0; i < questions.length; i++)
		{
			SurveyResult surveyResult = new SurveyResult();
			surveyResult.setQuestion(questions[i]);
			int rating = getInt(ratings[i].trim());
			if (rating == 0) continue;
			surveyResult.setRating(rating);
			surveyResult.setCreationDate(now);
			surveyResult.setUserName(userName);
			surveyResult.insert();
			writer.println("question:" + questions[i] + "\t\t" + "rating:" + rating);
		}
		if (comments != null && comments.length() > 0)
			writer.println("comments:" + comments);
		writer.close();
%>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>