<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.wordnet.model.WordNetDataProviderPool"%>
<%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
	if (user == null)
	{
		return;
	}
	System.out.println("Entered resume_phrases.jsp");
	//
	// Get resume phrases
	//
	int startIndex = getInt(request.getParameter("startIndex"));
	boolean usestars = true;
	if ("false".equals(request.getParameter("usestars")))
		usestars = false;
	Map<String,Integer> resumePhrasesMap = (Map<String,Integer>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
	System.out.println("Resume phrases :" + resumePhrasesMap.keySet().size());
	Map<String,String> phrasesSynonyms = (Map<String,String>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());
	List<UserDeletedPhrase> udps = new UserDeletedPhrase().getObjects("USER_NAME =" + UserDBAccess.toSQL(user.getLogin()));
	Set<String> userDeletedPhrases = new HashSet<String>();
	for (int i = 0; i < udps.size(); i++)
	{
		userDeletedPhrases.add(udps.get(i).getPhrase().toLowerCase());
	}
	boolean openCompanies = false;
	boolean openUniversities = false;
	String openTag = request.getParameter("openTag");
	if ("University".equals(openTag))
		openUniversities = true;
	else if ("Company".equals(openTag))
		openCompanies = true;
	Set<String> resumePhrases =  new TreeSet<String>();
	List<String> companies =  new ArrayList<String>();
	List<String> universities =  new ArrayList<String>();
	DecimalFormat format = new DecimalFormat("000000000");
	for (int i = 0; i < 5; i++)
	{
		try
		{
			resumePhrases = new TreeSet<String>();
			companies = new ArrayList<String>();
			universities = new ArrayList<String>();
			for (String phrase: resumePhrasesMap.keySet())
			{
				int count = resumePhrasesMap.get(phrase);
				if (TagList.isCompany(phrase.trim()))
					companies.add(phrase);
				else if (TagList.isUniversity(phrase.trim()))
					universities.add(phrase);
				else if (count > 0)
					resumePhrases.add(format.format(100000 - count) + "_" + phrase);
			}
		}
		catch (Exception x)
		{
			x.printStackTrace();
			Thread.sleep(300);
		}
	}
	String error = (String) session.getAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());
	if (error != null && error.trim().length() > 0)
	{
		error = error.replace("\n","<br>");
	}
	Collection<PhraseList> jobPhraseList = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	Collection<PhraseList> lemmaPhraseLists = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString());
	Set<String> jobPhrases = new HashSet<String>();
	for(PhraseList p : jobPhraseList)
	{   
		String phrase = p.iterator().next().getPhrase().getBuffer().toString();
		jobPhrases.add(phrase.toLowerCase().trim());
	}
	for(PhraseList p : lemmaPhraseLists)
	{   
		String phrase = p.iterator().next().getPhrase().getBuffer().toString().toLowerCase().trim();
		if (!jobPhrases.contains(phrase))
			jobPhrases.add(phrase);
	}
	System.out.println("Start sort:" + new Date());
	/*
	for (int i = 0; i < resumePhrases.size() && resumePhrases.size() < 1000; i++)
	{
		for (int j = i; j < resumePhrases.size(); j++)
		{
			try
			{
				if (resumePhrasesMap.get(resumePhrases.get(j)) > resumePhrasesMap.get(resumePhrases.get(i))
					|| (resumePhrasesMap.get(resumePhrases.get(j)) == resumePhrasesMap.get(resumePhrases.get(i)) && resumePhrases.get(j).compareTo(resumePhrases.get(i)) < 0))
				{
					String temp = resumePhrases.get(i);
					resumePhrases.set(i, resumePhrases.get(j));
					resumePhrases.set(j, temp);
				}
			}
			catch (Exception x) {}
		}
	}
	*/
	System.out.println("End sort" + new Date());
	if (resumePhrases.size() == 0)
	{
%>		Still Processing... <%
		return;
	}

%>
	<b><!--input type=button onclick="clearPostingPhrases()" value="Clear selection"--></b>
<%
	int i = startIndex;
	Set<String> uniqPhrases = new HashSet<String>();
	for (int type = 0; type < 3; type++)
	{
		System.out.println("Processing type:" + type);
		Collection<String> resphrases = new ArrayList<String>();
		if (type == 0 && companies.size() > 0)
		{
			resphrases = companies;
%>
	<br><a href="javascript:openCloseCompanies()"><b>Companies</b></a>
	<div id="companies_div">
	<div height=1><div colspan=3 class="kwdText"></div>
<% if (usestars) { %>
	<div colspan=3 class="kwdText"></div>
<% } else { %>
	<div colspan=3 class="kwdText"></div>
	<div class="kwdClear">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
<% } %>
	</div>
<%
		}
		if (type == 1 && universities.size() > 0)
		{
			resphrases = universities;
%>
	<br><a href="javascript:openCloseUniversities()"><b>Universities</b></a>
	<div id="universities_div">
	<div><div colspan=3 class="kwdText"></div>
<% if (usestars) { %>
	<div colspan=3 class="kwdText"></div>
<% } else { %>
	<div colspan=3 class="kwdText"></div>
	<div class="kwdClear">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
<% } %>
	</div>
<%
		}
		if (type == 2 && resumePhrases.size() > 0)
		{
			resphrases = resumePhrases;
%>
	<br><b>Other Phrases</b>
	<div id="otherphrases_div">
	<div><div colspan=3 width=100% height=1 class="kwdText"></div>
<% if (usestars) { %>
	<div colspan=3 height=1 class="kwdText"></div>
<% } else { %>
	<div colspan=3 height=1 class="kwdText"></div>
	<div class="kwdClear">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
<% } %>
	</div>
<%
		}
	for (int jobphrase0 = 0; jobphrase0 < 2; jobphrase0++)
	{
	int num = 0;
	for(String rphrase : resphrases)
	{
		num++;
		if (num > 100) break;
		if (rphrase.startsWith("0"))
		{
			int dash = rphrase.indexOf("_");
			if (dash != -1) rphrase = rphrase.substring(dash+1);
		}
		if (!user.isAdmin() && userDeletedPhrases.contains(rphrase.trim().toLowerCase()))
		    continue;
		if (rphrase.toUpperCase().equals(rphrase) && rphrase.length() > 5) continue;
		if (uniqPhrases.contains(rphrase.toLowerCase())) continue;
		if (jobphrase0 == 0 && !jobPhrases.contains(rphrase.toLowerCase())) continue;
		if (jobphrase0 == 1 && jobPhrases.contains(rphrase.toLowerCase())) continue;
		uniqPhrases.add(rphrase.toLowerCase());
		if (rphrase.indexOf(" ") > 3)
			uniqPhrases.add(rphrase.toLowerCase().substring(0,rphrase.indexOf(" "))); 
		boolean bold = false;
		if (jobphrase0 == 0)
			bold = true;
		int count = resumePhrasesMap.get(rphrase);
		int imgcnt = count;
		if (imgcnt > 9) imgcnt = 10;
%>
	<div class="kwdRecord" id="kwd_<%=100+i%>" onmouseover="helperActions(this, 'show')" onmouseout="helperActions(this, 'hide')">
	<div class="kwdTools" nowrap>
		<input type="hidden" value="<%=rphrase%>" id="resumephrase_<%=100+i%>" name="dummyresumephrase_<%=100+i%>"/>
		<img style="vertical-align:middle" src="tinyInfo<%=imgcnt%>.gif" title="Number of Occurrences: <%=count%>">
<%
		if (phrasesSynonyms.get(rphrase.trim()) != null)
		{
			String synonymsStr = phrasesSynonyms.get(rphrase.trim()).trim();
			if (synonymsStr.length() > 1)
			{
				String[] synonymns = synonymsStr.split("\\|");
				synonymsStr = synonymsStr.replace("|","_LIMIT_");
				int syncnt = synonymns.length;
				if (syncnt > 9) syncnt = 10;
%>
	<span class="iconSynonyms" id="syn_<%=100+i%>" onclick='getSynonymsPop(event, "<%=rphrase%>", this, "rtt");synPhrase="<%=rphrase%>"' title="Equivalent Phrases"></span>
<%			} else { %>
	<span class="iconGSynonyms" id="syn_<%=100+i%>" onclick='getSynonymsPop(event, "<%=rphrase%>", this, "rtt");synPhrase="<%=rphrase%>"' title="Equivalent Phrases"></span>
<%			} } else { %>
	<span class="iconGSynonyms" id="syn_<%=100+i%>" onclick='getSynonymsPop(event, "<%=rphrase%>", this, "rtt");synPhrase="<%=rphrase%>"' title="Equivalent Phrases"></span>
<%		} %>
<%	if (user.isAdmin() || user.isTester()) { %>
	<span class="iconDelete" onclick='deletePhrase(this, "<%=rphrase%>", <%=100+i%>);' title="Remove Phrase"></span>
<%		} %>
	</div>
	<div class="kwdText" id="phrase<%=100+i%>" align=left>
<%	if (bold) { %>
		<b><%=rphrase%></b>
<%	} else { %>
		<%=rphrase%>
<%	} %>
	</div>
<% if (usestars) { %>
<div class="kwdRating">
	<div class="rateActions">
		<div id="rateMe" style="width:auto;float:left;" onmouseout="starRater(<%=100+i%>)">
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Optionally nice" id="<%=100+i%>_1" onclick="starRater(this, 'save');setPhraseRating('resumephrase_', <%=100+i%>, 1)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Nice to have" id="<%=100+i%>_2" onclick="starRater(this, 'save');setPhraseRating('resumephrase_', <%=100+i%>, 2)" class=""></a>

		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Good to have" id="<%=100+i%>_3" onclick="starRater(this, 'save');setPhraseRating('resumephrase_', <%=100+i%>, 3)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Should have" id="<%=100+i%>_4" onclick="starRater(this, 'save');setPhraseRating('resumephrase_', <%=100+i%>, 4)" class=""></a>
		<a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Important" id="<%=100+i%>_5" onclick="starRater(this, 'save');setPhraseRating('resumephrase_', <%=100+i%>, 5)" class=""></a>
		<!--a onmouseout="starRater(this, 'off')" onmouseover="starRater(this, 'on')" alt="Must have" id="100_6" onclick="starRater(this, 'save')" class=""></a-->
		</div>
		<span style="display:none;" class="actionStatus" id="status<%=100+i%>"></span>
		<span style="float:right;" id="actions<%=100+i%>">
		<a onclick="clearStarRating(<%=100+i%>);setPhraseRating('resumephrase_', <%=100+i%>, 0);return false;" href="#" style="display:none;" id="clear<%=100+i%>">Clear</a>

		<!-- a onclick="removeKeyphrase(100);return false;" href="#" style="display:none;" id="remove<%=100+i%>">Remove</a -->
		</span>
	</div>
	<input type="hidden" value="0" id="rating_<%=100+i%>" name="rating_<%=100+i%>"/>
	<!--input type="hidden" value="false" id="required_<%=100+i%>" name="required_<%=100+i%>" /-->
</div>
<div align=center class="kwdCbx"><input type="checkbox" value="true" id="required_<%=100+i%>" name="required_<%=100+i%>" onclick="checkRequired(this,<%=100+i%>,'resumephrase_')"/><!--img id="reqimg_<%=100+i%>" src="star_off.gif" onclick="setRequired(this, <%=100+i%>)"/ --></div>
<div align=center class="kwdCbx"><input type="checkbox" value="tag" id="tag_<%=100+i%>" name="tag_<%=100+i%>" /></div>
<% } else { %>
<div align=center class="kwdCbx"><input type="radio" value="maybe1" id="maybe1_<%=100+i%>" name="required_<%=100+i%>" onclick="setPhraseRating('resumephrase_', <%=100+i%>, 1)"/></div>
<div align=center  class="kwdCbx"><input type="radio" value="maybe3" id="maybe3_<%=100+i%>" name="required_<%=100+i%>" onclick="setPhraseRating('resumephrase_', <%=100+i%>, 3)"/></div>
<div align=center  class="kwdCbx" onclick="setPhraseRating('resumephrase_', <%=i%>, 6)"><input type="radio" value="true" id="required_<%=100+i%>" name="required_<%=100+i%>"/></div>
<div align=center  class="kwdCbx"><input type="checkbox" value="tag" id="tag_<%=100+i%>" name="tag_<%=100+i%>" /></div>
<div align=center class="kwdClear"><a href="javascript:deselect(<%=100+i%>)" id="clear_<%=100+i%>" style="display:none">Clear</a></div>
<% } %>
</div>
<%  
	i++;
	}
}
%> 
<br><br>&nbsp;
</div><br/>
<% 
		System.out.println("Done type:" + type);
} %>



<%!
	int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>