<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Resume Sort - Multiple File Upload</title>
</head>
<body> 
<script language="javascript" type="text/javascript"src="common.js" />
<script language="javascript" type="text/javascript">
	documentWriteEmptyApplet();
</script>
<%
	User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
	if (user == null)
	{
%>
	<script>alert("Your session has expired");</script>
<%
		return;
	}
%>
<center>
<table width="640" cellpadding="3" cellspacing="0" border="0">
    <tr>
        <td colspan="2" align="center">
<!-- --------------------------------------------------------------------------------------------------- -->
<!-- --------     A DUMMY APPLET, THAT ALLOWS THE NAVIGATOR TO CHECK THAT JAVA IS INSTALLED   ---------- -->
<!-- --------               If no Java: Java installation is prompted to the user.            ---------- -->
<!-- --------                                                                                 ---------- -->
<!-- --------               THIS IS NOT THE JUpload APPLET TAG !   See below                  ---------- -->
<!-- --------------------------------------------------------------------------------------------------- -->
<!--"CONVERTED_APPLET"-->
<!-- HTML CONVERTER -->
<script language="JavaScript" type="text/javascript"><!--
    var _info = navigator.userAgent;
    var _ns = false;
    var _ns6 = false;
    var _ie = (_info.indexOf("MSIE") > 0 && _info.indexOf("Win") > 0 && _info.indexOf("Windows 3.1") < 0);
//--></script>
    <comment>
        <script language="JavaScript" type="text/javascript"><!--
        var _ns = (navigator.appName.indexOf("Netscape") >= 0 && ((_info.indexOf("Win") > 0 && _info.indexOf("Win16") < 0 && java.lang.System.getProperty("os.version").indexOf("3.5") < 0) || (_info.indexOf("Sun") > 0) || (_info.indexOf("Linux") > 0) || (_info.indexOf("AIX") > 0) || (_info.indexOf("OS/2") > 0) || (_info.indexOf("IRIX") > 0)));
        var _ns6 = ((_ns == true) && (_info.indexOf("Mozilla/5") >= 0));
//--></script>
    </comment>

<script language="JavaScript" type="text/javascript"><!--
    if (_ie == true) document.writeln('<object classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" WIDTH = "0" HEIGHT = "0" NAME = "JUploadApplet"  codebase="http://java.sun.com/update/1.5.0/jinstall-1_5-windows-i586.cab#Version=5,0,0,3"><noembed><xmp>');
    else if (_ns == true && _ns6 == false) document.writeln('<embed ' +
	    'type="application/x-java-applet;version=1.5" \
            CODE = "wjhk.jupload2.EmptyApplet" \
            ARCHIVE = "jupload-5.0.7.jar" \
            NAME = "JUploadApplet" \
            WIDTH = "0" \
            HEIGHT = "0" \
            type ="application/x-java-applet;version=1.6" \
            scriptable ="false" ' +
	    'scriptable=false ' +
	    'pluginspage="http://java.sun.com/products/plugin/index.html#download"><noembed><xmp>');
//--></script>
<applet  CODE = "wjhk.jupload2.EmptyApplet" ARCHIVE = "jupload-5.0.7.jar" WIDTH = "0" HEIGHT = "0" NAME = "JUploadApplet"></xmp>
    <PARAM NAME = CODE VALUE = "wjhk.jupload2.EmptyApplet" >
    <PARAM NAME = ARCHIVE VALUE = "jupload-5.0.7.jar" >
    <PARAM NAME = NAME VALUE = "JUploadApplet" >
    <param name="type" value="application/x-java-applet;version=1.5">
    <param name="scriptable" value="false">
    <PARAM NAME = "type" VALUE="application/x-java-applet;version=1.6">
    <PARAM NAME = "scriptable" VALUE="false">

</xmp>
    
Java 1.5 or higher plugin required.
</applet>
</noembed>
</embed>
</object>

<!--
<APPLET CODE = "wjhk.jupload2.EmptyApplet" ARCHIVE = "jupload-5.0.7.jar" WIDTH = "0" HEIGHT = "0" NAME = "JUploadApplet">
	<PARAM NAME = "type" VALUE="application/x-java-applet;version=1.6">
	<PARAM NAME = "scriptable" VALUE="false">
	</xmp>
Java 1.5 or higher plugin required.
</APPLET>
-->
<!-- "END_CONVERTED_APPLET" -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- --------------------------     END OF THE DUMMY APPLET TAG            ------------------------------ -->
<!-- ---------------------------------------------------------------------------------------------------- -->


<!---------------------------------------------------------------------------------------------------------
-------------------     A SIMPLE AND STANDARD APPLET TAG, to call the JUpload applet  --------------------- 
----------------------------------------------------------------------------------------------------------->
        <applet
	            code="wjhk.jupload2.JUploadApplet"
	            name="JUpload"
	            archive="jupload-5.0.7.jar"
	            width="640"
	            height="300"
	            mayscript="true"
	            alt="The java plugin must be installed.">
            <!-- param name="CODE"    value="wjhk.jupload2.JUploadApplet" / -->
            <!-- param name="ARCHIVE" value="jupload-5.0.7.jar" / -->
            <!-- param name="type"    value="application/x-java-applet;version=1.5" /  -->
            <param name="postURL" value="<%=request.getContextPath()%>/MultiUpload" />
            <!-- Optionnal, see code comments -->
            <param name="showLogWindow" value="false" />
            <param name="allowedFileExtensions" value="doc/docx/rtf/txt/odt/pdf/zip" />
			<param name="showStatusBar" value="false" />
			<param name="formdata" value="uploadform" />
			<param name="uploadPolicy" value="TextnomicsUploadPolicy" />

            Java 1.5 or higher plugin required. 
        </applet>
<!-- --------------------------------------------------------------------------------------------------------
----------------------------------     END OF THE APPLET TAG    ---------------------------------------------
---------------------------------------------------------------------------------------------------------- -->
        </td>
    </tr>
<form name=uploadform>
    <tr>
        <td align="right">Folder Name To Save Uploaded Resumes : </td>
		<td align=left><input type=text name=ResumeFolder size=30 value="RESUMES-<%=new SimpleDateFormat("yyyy-MM-dd").format(new Date())%>"></td>
	</tr>
<%
	String uploadDir="uploads" + File.separator + user.getLogin() + File.separator;
	File userFolder = PlatformProperties.getInstance().getResourceFile(
							uploadDir + File.separator + "MyResumes" + File.separator);
	File []files=null;
	if(userFolder.exists())
		files=userFolder.listFiles();
	
	if(files !=null && files.length!=0){
%>
	<tr>
        <td align="right">OR Add To Previous Folder : </td>
		<td align=left><select name=PrevFolder onchange="copyFolderName()">
				<option value ='' >-- Select Folder -- </option>
<%
		for(int i=0;i<files.length;i++){
			if (files[i].isDirectory())
				out.write("<option>"+files[i].getName()+"</option>");
		}
%>
		</select>
		</td></tr>
<%	} %>    
    <tr>
        <td colspan="2" align="center"><input type=button onclick="window.opener.multiUploadDone();window.close()" value="Close"></td>
	</tr>
</form>
<script>
	function copyFolderName()
	{
		var name = document.uploadform.PrevFolder.options[document.uploadform.PrevFolder.selectedIndex].value;
		if (name != '')
		{
			document.uploadform.ResumeFolder.value = name;
		}
	}
</script>
</table>
</center>
</body>
</html>
