<%@include file="header.jsp"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.mail.*"%>
<%
	String feedback = request.getParameter("feedback");
	String email = request.getParameter("email");
	if (email == null) email = "";
	String name = request.getParameter("name");
	if (feedback != null && feedback.trim().length() > 0)
	{
		String subj = "Feedback from Site Visitor";
		subj = subj + " : " + email + "(" + name + ")";
		GenericMailer.getInstance().sendMail("info@textnomics.com", subj, feedback);
%>
		<script>alert("Your feedback has been sent");</script>
<%
	}
	String deftext = "Please enter your feedback here or send an email to contact@resumetuner.com";
%>
<div class="central">
<div class="about">
Your feedback is greatly appreciated and valued, thank you for your time.
<br>
<br>
<script>
function clearDefaultText(obj)
{
	if (obj.value == '<%=deftext%>')
		obj.value = "";
}
function submitForm()
{
	var feedback = document.fbform.feedback.value;
	var name = document.fbform.name.value;
	if (feedback.length < 3)
	{
		return;
	}
	if (name.length < 3)
	{
		alert("Please enter your full name");
		return;
	}
	document.fbform.submit();
}
</script>
<form name="fbform" method=post>
<table cellpadding=5 cellspacing=0>
<tr>
<td>
<textarea name="feedback" cols=60 rows=20  onfocus="clearDefaultText(this)"><%=deftext%></textarea><br>
</td>
</tr>
<tr>
<td nowrap><br>Your Name: <input name=name size=20> (required)</td>
</tr>
<tr>
<td nowrap><br>Your email address: <input name=email size=20> (optional)</td>
</tr>
<tr>
<td align=center>
<br>
<input type=button value="Send Feedback" onclick="submitForm()">
</td>
</tr>
</table>
</form>
<br>
<br>
If you enjoyed your free ResumeSort<span class="superscript">TM</span> Alpta<span class="superscript">TM</span> test, please visit our <a href="http://www.twitter.com/#!/resumesort" target=_blank>Twitter</a> and <a href="http://www.facebook.com/ResumeSort" target=_blank>Facebook</a> pages and 'like' us!
</div>

</div>
<%@include file="footer.jsp"%>
  
</body>
</html>