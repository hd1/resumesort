<%@include file="header.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%
	if (user == null)
	{
		response.sendRedirect("");
		return;
	}
	String userdir = ResumeSorter.getUploadDir(user);
    session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString(), userdir);
	String[] resumeFolderName = request.getParameterValues("resumeFolderName");
	if ("deleteFolder".equals(request.getParameter("command")) && resumeFolderName != null)
	{
		for (int i = 0; i < resumeFolderName.length; i++)
		{
			List<JobPosting> postings = new JobPosting().getObjects("resume_folder=" + UserDBAccess.toSQL(resumeFolderName[i]));
			System.out.println("Number of jobpostings =" + postings.size());
			int count = new ResumeScore().getCount("resume_folder=" + UserDBAccess.toSQL(resumeFolderName[i]));
			if (postings.size() > 0)
			{
				String postingIds = "";
				for (JobPosting posting: postings)
				{
					postingIds = postingIds + "\t" + posting.getPostingId() + "\\n";
				}
%>
			<script>alert("The folder <%=resumeFolderName[i]%> is being used by the following Job Postings:\n<%=postingIds%> Please delete them first");</script>
<%
			}
			else if (false && count > 0)
			{
%>
			<script>alert("The folder <%=resumeFolderName[i]%> is being used by the <%=count%> Resumes and so cannot be deleted");</script>
<%
			}
			else
			{
				File folder = PlatformProperties.getInstance().getResourceFile(userdir + File.separator + ResumeSorter.RESUME_FOLDER + File.separator + resumeFolderName[i]);
				File[] files = folder.listFiles();
				if (files != null)
				{
					for (File file: files)
					{
						file.delete();
					}
					folder.delete();
				}
			}
		}
	}
	File rootFolder = PlatformProperties.getInstance().getResourceFile(userdir + File.separator + ResumeSorter.RESUME_FOLDER);
	File[] resumeFolders = rootFolder.listFiles();
%>
<script>
	function showFolder(obj)
	{
		window.location = "resume_folder.jsp?resumeFolderName=" + obj.options[obj.selectedIndex].value;
	}
	function deleteFolder(name)
	{
		if (!confirm("Are you sure you wish delete folder: " + name + "?"))
		{
			return;
		}
		window.location = "resume_folder_list.jsp?command=deleteFolder&resumeFolderName=" + escape(name);
	}
	function deleteUnused()
	{
		if (!confirm("Are you sure you wish delete all unused Resume Folders and their contents?"))
		{
			return;
		}
		document.unusedForm.submit();
	}
</script>
	<style type="text/css" title="currentStyle">
			@import "js/table/css/demo_page.css";
			@import "js/table/css/demo_table_jui.css";
			@import "js/table/ui-lightness/jquery-ui-1.8.4.custom.css";
		</style>
		<script type="text/javascript" language="javascript" src="js/table/js/jquery.dataTables.js"></script>
<link type="text/css" href="../css/ui/jquery-blue.css" rel="stylesheet" />	
<div class="middle">
<div class="maintext_dashboard">
<%
	String name = user.getName();// What kind of stupid css is this? It screws up without the spaces.
	if (name.length() < 6) name = name + "&nbsp;&nbsp;&nbsp;";
%>
 
 <div class="sky_bar">
 	<h3>Dashboard </h3>
    <p>Welcome back <%=name%> !</p>
    <a href="#" class="selected">RESUME FOLDERS</a>
    <a href="upload_jobposting.jsp">START NEW JOBS</a>
    <a href="home.jsp">PREVIOUS JOBS</a>
 </div>
 
 <div class="text_dashboard">
<%
	String error = request.getParameter("error");
	if (error != null && error.length() > 0)
	{
%> <span class="error"><%=error%></span><%
	}
%>
<br/><br/>
 
 <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"aaSorting": [[ 3, "desc" ]],
					"aoColumns": [
								null,
								null,
								null,
								null,
								{ "bSortable": false }
								]
				});
			} );
			
	</script>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>Resume Folder</th>
			<th>Resumes</th>
			<th>Jobs</th>
			<th>Date </th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<%
	List<String> unused = new ArrayList<String>();
	for (int i = 0; i < resumeFolders.length; i++)
	{
		if (!resumeFolders[i].isDirectory()) continue;
		File[] resumes = resumeFolders[i].listFiles();
		int count = 0;
		for (File resume: resumes)
		{
			if (resume.isDirectory() || resume.getName().endsWith("Object") || resume.getName().startsWith(".")) continue;
			count++;
		}
		String jobcount = "" + new JobPosting().getCount("resume_folder=" + UserDBAccess.toSQL(resumeFolders[i].getName()));
		if (jobcount.equals("0"))
		{
			jobcount = "Unused";
			unused.add(resumeFolders[i].getName());
		}
%>
  <tr style="font-weight:bold"  class="gradeC">
    <td><a href="resume_folder.jsp?resumeFolderName=<%=resumeFolders[i].getName()%>"><%=resumeFolders[i].getName()%></a></td>
	<td class="center"><%=count%></td>
	<td class="center"><%=jobcount%></td>
    <td class="center"><%=checkNull(new Date(resumeFolders[i].lastModified()),"")%></td>
	<td class="center">  <img src="x.png" onclick='deleteFolder("<%=resumeFolders[i].getName()%>")'/></td>
  </tr>
<%	} %>
	</tbody>
</table>
<br>
<br>
<center>
<form name=unusedForm method=post>
<%
	for (String folder: unused)
	{
%>
<input type=hidden name=resumeFolderName value="<%=folder%>">
<%	} %>
<input type=hidden name=command value="deleteFolder">
		&nbsp;&nbsp;<a onclick="javascript:deleteUnused()" class="orangeBtn">Delete Unused Folders</a>
</form>
</center>
<div class="clear"></div>
</div>
</div>
</div>


<%@include file="footer.jsp"%>
</body>
</html>
<%!
	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
	String checkNull(Object value, String defval)
	{
		if (value == null)
			return defval;
		else if (value instanceof Date)
			return dateformat.format(value);
		return value.toString();
	}
    int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>