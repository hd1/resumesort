<!DOCTYPE html>
<html lang="en">
<head>
  <title>ResumeSort - the most advanced resume sorting technology on the planet.</title>
  <meta charset="utf-8" />
  <meta http-equiv="cache-control" content="no-store"/>
  <meta http-equiv="pragma" content="no-cache"/>
  <meta http-equiv="expires" content="0"/>
</head>
<body onunload="checkSave()">
<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.datadriven.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%
		String phrase = request.getParameter("phrase");
		String tester = request.getParameter("tester");
		int offset = getInt(request.getParameter("offset"));
		int maxrows = getInt(request.getParameter("maxrows"));
		if (maxrows == 0) maxrows = 20;
		System.out.println("get_synonyms: phrase=" + phrase);
		if (phrase == null) phrase = "";
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());

		if (user == null) 
		{
%>
		<script>alert("Your session has expired.");</script>
<%
			response.sendRedirect(request.getContextPath() + "/Login.jsp?redirectURL=oxfordcleanup.jsp");
			return;
		}
		if (!user.isAdmin() && !user.isTester())
		{
%>
		<script>alert("No permissions for:<%=user.getLogin()%>")</script>
<%
			return;
		}
		String command = request.getParameter("command");
		if ("save".equals(command))
		{
			Map<String, String[]> paramMap = request.getParameterMap();
			for (String param: paramMap.keySet())
			{
				String[] values = paramMap.get(param);
				if (param.startsWith("val"))
				{
					String[] phrases = param.split("\\|");
					boolean invalid = true;
					if (values[0].equals("false")) invalid = false;
					if (invalid)
					{
						String criteria = "synset = " + UserDBAccess.toSQL(phrases[1].trim())
								+ " and phrase = " + UserDBAccess.toSQL(phrases[2].trim())
								+ " and synonym is null and user_name = " + UserDBAccess.toSQL(tester);
						if (phrases.length > 3)
							criteria = "synset = " + UserDBAccess.toSQL(phrases[1].trim())
								+ " and phrase = " + UserDBAccess.toSQL(phrases[2].trim())
								+ " and synonym = " + UserDBAccess.toSQL(phrases[3].trim())
								+ " and user_name = " + UserDBAccess.toSQL(tester);
						int count = new RejectedSynonym().getCount(criteria);
						if (count == 0)
						{
							RejectedSynonym rs = new RejectedSynonym();
							rs.setSynset(phrases[1].trim());
							rs.setPhrase(phrases[2].trim());
							if (phrases.length > 3)
								rs.setSynonym(phrases[3].trim());
							rs.setUserName(tester);
							rs.insert();
						}
					}
					else
					{
						String criteria = "synset = " + UserDBAccess.toSQL(phrases[1].trim())
								+ " and phrase = " + UserDBAccess.toSQL(phrases[2].trim())
								+ " and synonym is null and user_name = " + UserDBAccess.toSQL(tester);
						if (phrases.length > 3)
							criteria = "phrase = " + UserDBAccess.toSQL(phrases[2].trim())
								//+ " and code = " + phrases[1].trim()
								+ " and synonym = " + UserDBAccess.toSQL(phrases[3].trim())
								+ " and user_name = " + UserDBAccess.toSQL(tester);
						new UserDBAccess().deleteData(new RejectedSynonym().getDB_TABLE(), criteria);
					}

				}
			}
		}
        //List<WikiPhrase> wikiPhrases = new ArrayList();       
		List<OxfordSynonym> osynonyms = new DBAccess().getObjects(new OxfordSynonym(),"inwordnet = true and phrase like " + WikiPhrase.toSQL(phrase.toLowerCase().trim() + "%") + " order by synset", offset, maxrows);
%>
<form name=oxfordform method=post>
<%
		if (user.isAdmin())
		{
%> <Select name=tester onchange="search()"><%
			UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();
			UserIterator users = provider.getUsers("is_tester = true","name");
			User current;
			int i = 0;
			while (users.hasNext())
			{
				current = users.next();
				String selected = "";
				if (current.getLogin().equals(tester))
					selected = "selected";
%>
	<option value=<%=current.getLogin()%> <%=selected%>><%=current.getName()%></option>
<%
			}
			users.close();
			UserDataProviderPool.getInstance().releaseProvider();
%> </Select>
<%
		}
%>
<table align=center border=1 cellpaddng=2 cellspacing=0 ><TBODY id="ttBody">
<tr style="font-weight:bold;background-color:lightblue">
<th align=center colspan=3><b>Wordnet Synset Phrases</b></th><th colspan=2>Oxford Synonym</th>
</tr>
<!--tr>
<td align=center>Phrase</td><td>Invalid</td><td align=center >Phrase</td><td>Invalid</td>
</tr-->
<%
		String color="";
		String prevSynset = "";
		for (int j = 0; j < osynonyms.size(); j++)
		{
			OxfordSynonym wos = osynonyms.get(j);
			String synset = wos.getSynset();
			String wkphrase = wos.getWordnetPhrases();
			if (synset.equals(prevSynset)) continue;
			prevSynset = synset;
            List<OxfordSynonym> equivalents  = new OxfordSynonym().getObjects("inwordnet = false and synset = " + synset + " and id !=" + wos.getId());
			int invcnt  = new RejectedSynonym().getCount("phrase = " + UserDBAccess.toSQL(wkphrase.trim()) + " and synonym is null and user_name =" + UserDBAccess.toSQL(tester));
			String checked1 = "checked";
			String checked2 = "";
			String style1 ="style='text-decoration:none'";
			if (invcnt > 0)
			{
				checked1 = "";
				checked2 = "checked";
				style1 ="style='text-decoration:line-through'";
			}
			if (color.length() == 0)
				color="bgcolor='lightgrey'";
			else
				color = "";
			int numrows = equivalents.size();
			if (numrows == 0) numrows = 1;
%>
	<tr <%=color%> id="syntr<%=j%>">
	<td valign=top rowspan="<%=numrows%>"><%=wos.getSynset()%></td>
	<td id="td<%=j%>" valign=top rowspan="<%=numrows%>" <%=style1%>><%=wkphrase%></td>
	<td valign=top rowspan="<%=numrows%>" nowrap><input type=radio <%=checked1%> value=false name="val|<%=wos.getSynset()%>|<%=wkphrase%>" onclick="changesMade=true;setValid('td<%=j%>')">Valid&nbsp;&nbsp;&nbsp;<input type=radio <%=checked2%> value=true name="val|<%=wos.getSynset()%>|<%=wkphrase%>" onclick="changesMade=true;setInValid('td<%=j%>')">Invalid</td>
<%
		for (int i = 0; i < equivalents.size(); i++)
		{
			invcnt  = new RejectedSynonym().getCount("phrase = " + UserDBAccess.toSQL(wkphrase.trim()) + " and synonym = " + UserDBAccess.toSQL(equivalents.get(i).getPhrase().trim()) + " and user_name =" + UserDBAccess.toSQL(tester));
			checked1 = "checked";
			checked2 = "";
			String style2 = style1;
			if (invcnt > 0)
			{
				checked1 = "";
				checked2 = "checked";
				style2 ="style='text-decoration:line-through'";
			}
			if (i != 0)
			{
%>
			<tr <%=color%>>
<%			} %>
	<td  id="td<%=j%>-<%=i%>" <%=style2%>><%=equivalents.get(i).getPhrase()%></td>
	<td valign=top nowrap><input type=radio <%=checked1%> value=false name="val|<%=synset%>|<%=wos.getWordnetPhrases()%>|<%=equivalents.get(i).getPhrase()%>" onclick="changesMade=true;setValid('td<%=j%>-<%=i%>')"> Valid&nbsp;&nbsp;&nbsp;<input type=radio <%=checked2%> value=true name="val|<%=synset%>|<%=wos.getWordnetPhrases()%>|<%=equivalents.get(i).getPhrase()%>" onclick="changesMade=true;setInValid('td<%=j%>-<%=i%>')">Invalid</td>
	</tr>
<%
		} %>
	</tr>
<%
	}
%>
	<tr>
	<td colspan=100% align=center>
	<input type=button value="Prev Page" onclick="prevPage()">&nbsp;&nbsp;
	<input type=button value=Save onclick="save()">&nbsp;&nbsp;
	<input type=button value="Next Page" onclick="nextPage()">&nbsp;&nbsp;
	</td>
	</tr>
	<tr>
	<td colspan=100% align=center>
	Current Page #: <%=(offset/maxrows)+1%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Goto Page:<input name=pageNo  size=2 value="1"><input type=button value=Go onclick="gotoPage()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Show Per Page:<input name=maxrows  size=2 value="<%=maxrows%>"><input type=button value=Go onclick="save()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Search:<input name=phrase  size=20 value="<%=phrase%>"><input type=button value=Go onclick="search()">
	</td>
	</tr>
	</TBODY></table>
	<input name=offset type=hidden value="<%=offset%>">
	<input name=command type=hidden value="">
	<form>
<script>
	var changesMade = false;
	var busy = false;
	function save()
	{
		if (busy)
		{
			alert("Still processing.... Please wait");
			return;
		}
		busy = true;
		changesMade = false;
		document.oxfordform.command.value = 'save';
		document.oxfordform.submit();
	}
	function checkSave()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return false;
			}
			changesMade = false;
		}
	}
	function search()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return;
			}
			changesMade = false;
		}
		document.oxfordform.offset.value = 0;
		document.oxfordform.submit();
	}
	function nextPage()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return;
			}
			changesMade = false;
		}
		document.oxfordform.offset.value = <%=offset%> + <%=maxrows%>;
		document.oxfordform.submit();
	}
	function prevPage()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return;
			}
			changesMade = false;
		}
		var newoff = <%=offset%> - <%=maxrows%>;
		if (newoff < 0) newoff = 0;
		document.oxfordform.offset.value = newoff;
		document.oxfordform.submit();
	}
	function gotoPage()
	{
		if (changesMade)
		{
			if(confirm("Do you want to save your changes first?"))
			{
				save();
				return;
			}
			changesMade = false;
		}
		var pageNo = parseInt(document.oxfordform.pageNo.value);
		var newoff = <%=maxrows%>*(pageNo-1);
		if (newoff < 0) newoff = 0;
		document.oxfordform.offset.value = newoff;
		document.oxfordform.submit();
	}
	function setValid(id)
	{
		var elem = document.getElementById(id);
		elem.style.textDecoration = 'none';
	}
	function setInValid(id)
	{
		var elem = document.getElementById(id);
		elem.style.textDecoration = 'line-through';
	}
</script>
  
</body>
</html>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>