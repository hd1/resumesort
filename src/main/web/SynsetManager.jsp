<%@include file="header.lay"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>

<div id="synsetlist">
<form method="post" action="SynsetManager"><input
	style="width: 372px" name="lookupsynset"> <input type="hidden"
	name="op" value="lookupsynset"> <input type="submit"
	value="Lookup Synset"></form>

<%
	Synset bean = (Synset) session.getAttribute("lookupsynset");
	if (bean != null)
	{
		out.print("<h1>");
		out.print("Results for Synset: ");
		out.println("<u>");
		out.println("<font color=\"red\">");
		out.print(bean.getId());
		out.println("</font>");
		out.println("</u>");
		out.print("</h1>");
		out.print("<h2>");
		out.print("Info:");
		out.print("</h2>");
		out.print("<b>Gloss: </b>" + bean.getGloss());
		out.print("<br>");
		out.print("<b>Hyponyms: </b>" + bean.getIIC());

		out.print("<h3>");
		out.print("HYPERNYMS:");
		out.print("</h3>");

		SynsetIterator its = Synset.getHypernyms(bean);
		Synset hyp;
		while (its.hasNext())
		{
			hyp = its.next();
			out.println("<font color=\"red\">");
			out.println("<b>");
			out.println(hyp.getId());
			out.println("</b>");
			out.println("</font>");
			out.println(":");
			out.println(hyp.getGloss());
			out.println("<br>");
		}
		its.close();

		out.print("<h3>");
		out.print("WORDS:");
		out.print("</h3>");

		WordIterator itw = Synset.getWords(bean);
		Word word;
		while (itw.hasNext())
		{
			word = itw.next();
			out.print("<b>");
			out.print(word.toString());
			out.print("</b>");
			out.print("<br>");
			out.print("<form method=\"post\" action=\"WordManager\">");
			out.print("<input type=\"hidden\" name=\"op\" value=\"lookupword\">");
			out.print("<input type=\"hidden\" name=\"lookupword\" value=\"");
			out.print(word.toString());
			out.print("\">");
			out.print("<input type=\"submit\" value=\"Lookup Word\">");
			out.print("</form>");
			out.print("<form method=\"post\" action=\"SynsetManager\">");
			out.print("<input type=\"hidden\" name=\"op\" value=\"removeword\">");
			out.print("<input type=\"hidden\" name=\"removeword\" value=\"");
			out.print(word.getId());
			out.print("\">");
			out.print("<input type=\"submit\" value=\"Remove from Synset\">");
			out.print("</form>");
			out.print("<br>");
		}

		itw.close();
		out.println("<form method=\"post\" action=\"SynsetManager\">");
		out.println("<input type=\"hidden\" name=\"op\" value=\"addtoword\">");
		out.println("<input style=\"width: 372px\" name=\"addtoword\">");
		out.println("<input type=\"submit\" value=\"Add Another Word\">");
		out.println("</form>");
		out.println("<br>");

		out.println("<form method=\"post\" action=\"SynsetManager\">");
		out.println("<input type=\"hidden\" name=\"op\" value=\"deletesynset\">");
		out.println("<input type=\"submit\" value=\"Delete Synset\">");
		out.println("</form>");
		out.println("<br>");
	}
%>
</div>
<hr>
<div>
<form method="post" action="SynsetManager"><input
	style="width: 723px; height: 95px" name="addsynsetgloss">
(GLOSS)<br>
<select style="width: 162px; height: 24px" name="addsynsetpos">
	<option>NOUN</option>
	<option>VERB</option>
</select> (SYNSET TYPE)<br>
<input style="width: 162px" name="addsynsethypernym"> (IMMEDIATE
HYPERNYM ID)<br>
<input type="hidden" name="op" value="addsynset"> <input
	type="submit" value="Add New Synset"></form>
</div>
<%@include file="footer.lay"%>