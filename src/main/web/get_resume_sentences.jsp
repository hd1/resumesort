<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%@ page import="com.textonomics.nlp.*"%>
<%@ page import="com.textnomics.data.*"%>
<%
		String phrase = request.getParameter("phrase");
		int resumeIndex = getInt(request.getParameter("resumeIdx"));
		System.out.println("get_resume_sentences : phrase=" + phrase + " idx=" + resumeIndex);
		if (phrase == null) return;
		JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
		List<Map<Phrase,Integer>> matchedPhraseList = (List<Map<Phrase,Integer>>) session.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
		if (matchedPhraseList == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		Map<String,String> phrasesSynonyms = (Map<String,String>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());
		String[] synonyms = new String[0];
		String synonymsStr = "";
		if (phrasesSynonyms.get(phrase.trim()) == null)
		{
			for (String p: phrasesSynonyms.keySet())
			{
				if (p.equalsIgnoreCase(phrase.trim()))
				{
					synonymsStr = phrasesSynonyms.get(p).trim();
					if (synonymsStr.length() > 1)
					{
						synonyms = synonymsStr.split("\\|");
					}
					break;
				}
			}
		}
		else if (phrasesSynonyms.get(phrase.trim()) != null)
		{
			synonymsStr = phrasesSynonyms.get(phrase.trim()).trim();
			if (synonymsStr.length() > 1)
			{
				synonyms = synonymsStr.split("\\|");
			}
		}
        List<File> resumeFiles = (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		File resumeFile = resumeFiles.get(resumeIndex);
        DocumentRS resumeDoc =  (DocumentRS) FileUtils.deSerializeOject(new File(resumeFile.getAbsolutePath()+".Object"));
		List<Sentence>  nonLemmaSentences = new ArrayList<Sentence>(resumeDoc.getSentences());
		Set<Phrase> phrases = matchedPhraseList.get(resumeIndex).keySet();
		HashMap<String, DatedPhrase> allDates = (HashMap<String, DatedPhrase>) session.getAttribute("DATES_" + posting.getId() + "_" + resumeIndex);
		if (allDates == null)
		{
			long start = System.currentTimeMillis();
			try
			{
				allDates = DateExtractor.extractDatesandExp(nonLemmaSentences, matchedPhraseList.get(resumeIndex));
				session.setAttribute("DATES_" + posting.getId() + "_" + resumeIndex, allDates);
				System.out.println("allDates:" + allDates);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			long end = System.currentTimeMillis();
			System.out.println("Time for Date Extraction:" + (end - start)/1000.0 + " secs.");
		}
		DatedPhrase dates = null;
		if (allDates != null) dates = allDates.get(phrase);
		if (allDates != null && dates == null) dates = allDates.get(phrase + " ");
		System.out.println("Phrase:" + phrase + " Dates:" + dates);
		String totalTime = "";
		if (dates != null) totalTime = phrase + " (" + getTime(dates.getTotalTime()) + ")";
		HashMap<Phrase, DateObject> dateMap = null;
		if (dates != null) dateMap = dates.getMapOfPhrase();
		System.out.println("Map:" + dateMap);
%>
<table border=1 width="300" id="tttable" bgcolor="#FFFFBB" cellpaddng=0 cellspacing=0 class="ttipBody"><TBODY id="ttBody">
<tr>
<td colspan=100% align=center style="font-weight:bold"><b>Text from the Resume</b><br><%=totalTime%><hr></td>
</tr>
<%
	try
	{
		int idx = 0;
		int pidx = -1;
		for (Phrase resumePhrase : phrases)
		{
			String resumePhraseText = resumePhrase.getBuffer().trim();
			pidx++;
			//System.out.println("input=" + phrase + " resumePhrase=" + resumePhraseText);
			boolean found = false;
			if (resumePhraseText.equalsIgnoreCase(phrase)) found = true;
			for (int i = 0; found == false && i < synonyms.length; i++)
			{
				//System.out.println("input=" + phrase + " resumePhrase=" + resumePhraseText + " synonym=" + synonyms[i]);
				if (synonyms[i].equalsIgnoreCase(resumePhraseText))
					found = true;
			}
			if (!found) continue;
			int sentenceIdx = resumePhrase.getSentence();
			String sentence = resumeDoc.getSentence(sentenceIdx);
			//System.out.println("phrase=" + phrase + " sentenceIdx=" + sentenceIdx + " sentence=" + sentence);
			if (sentence.indexOf(resumePhrase.getBuffer().trim()) != -1)
			{
				sentence = sentence.replace(resumePhrase.getBuffer().trim(), "<font color=red>" + resumePhrase.getBuffer().trim() + "</font>").replace('\r',' ').replace('\n', ' ');
			}
			else if (sentence.indexOf(resumePhrase.getBuffer().toLowerCase().trim()) != -1)
			{	
				sentence = sentence.replace(resumePhrase.getBuffer().toLowerCase().trim(), "<font color=red>" + resumePhrase.getBuffer().toLowerCase().trim() + "</font>").replace('\r',' ').replace('\n', ' ');
			}
			else
			{
				sentence = sentence.toLowerCase().replace(resumePhrase.getBuffer().toLowerCase().trim(), "<font color=red>" + resumePhrase.getBuffer().toLowerCase().trim() + "</font>").replace('\r',' ').replace('\n', ' ');
			}
			String fromTo = "";
			if (dateMap != null)
			{
				DateObject date = dateMap.get(resumePhrase);
				if (date != null)
					fromTo = "<br>From:" + date.getStartDate() + " To:" + date.getEndDate() + " Period:" +  getTime(date);
			}
%>
	<tr><td><a href="#" onclick="selectHighLight(null, '<%=pidx%>')"><%=sentence%></a><%=fromTo%><br><hr style="border:dashed #000000; border-width:1px 0 0; height:0;">
</td>
</tr>
<%
			idx++;
		}
	}
	catch (Exception x)
	{
		x.printStackTrace();
	}
%>
	</TBODY></table>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}	
	}
	String  getTime(LengthObject value)
	{
		try
		{
			if (value == null) return "";
			return "" + value.getYears() + " yrs " + value.getMonths() + " mths";
		}
		catch (Exception x)
		{
			return "";
		}	
	}
	String  getTime(DateObject value)
	{
		try
		{
			if (value == null) return "";
			return "" + value.getYears() + "yrs " + value.getMonths() + "mths";
		}
		catch (Exception x)
		{
			return "";
		}	
	}
%>