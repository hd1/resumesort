<%@ page import="java.util.*"%><%@ page import="java.text.*"%><%@ page import="com.textonomics.*"%><%@ page import="com.textnomics.data.*"%><%@ page import="com.textonomics.web.*"%><%@ page import="com.textonomics.user.*"%><%@ page import="com.textonomics.wordnet.model.*"%><%
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		System.out.println("Entered download resume phrases");
		Integer numleft = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
		if (numleft == null) numleft = 0;
		if (numleft > 0)
		{
%>
<script>alert("" + numleft + " resumes not yet processed");</script>
<%
			return;
		}
		String postingId = (String)	session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString());
		Map<String,Integer> resumePhrasesMap = (Map<String,Integer>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
		if (resumePhrasesMap == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		Map<String,String> phrasesSynonyms = (Map<String,String>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());

		List<String> resumePhrases =  new ArrayList<String>();
		List<String> companies =  new ArrayList<String>();
		List<String> universities =  new ArrayList<String>();
		for (int i = 0; i < 5; i++)
		{
			try
			{
				resumePhrases = new ArrayList<String>();
				companies = new ArrayList<String>();
				universities = new ArrayList<String>();
				System.out.println("Number of phrases:" + resumePhrasesMap.keySet().size());
				for (String phrase: resumePhrasesMap.keySet())
				{
					int count = resumePhrasesMap.get(phrase);
					//if (TagList.isCompany(phrase.trim()))
					//	companies.add(phrase);
					//else if (TagList.isUniversity(phrase.trim()))
					//	universities.add(phrase);
					//else 
					if (count > 0)
					{
						resumePhrases.add(phrase);
						String synonymList = phrasesSynonyms.get(phrase.trim());
						if (synonymList == null)
						{
							try
							{
								synonymList = ResumeSorter.getWikiSynonyms(user, phrase);
								phrasesSynonyms.put(phrase.trim(), synonymList);
							}
							catch (Exception x)
							{
								x.printStackTrace();
							}
						}
					}
				}
			}
			catch (Exception x)
			{
				x.printStackTrace();
				Thread.sleep(300);
			}
		}
	System.out.println("Downloading");
	SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
	response.setContentType ("text/comma-separated-values") ;
	response.setHeader("Content-disposition",
					"attachment;filename=JobPhrases_" + postingId.replace(' ','_') + "_" + timestamp.format(new java.util.Date()) + ".csv");
	String line = "Phrase,Occurence,Synonyms";
	out.write(line + "\n");
	for (String phrase: resumePhrases)
	{
		line = phrase + "," + resumePhrasesMap.get(phrase) + "," + phrasesSynonyms.get(phrase.trim());
		out.write(line + "\n");
	}
	return;
%>