<%@ page import="sun.misc.BASE64Decoder;"%>
<%!private final static String COOKIENAME = "rcwCookie"; %>
<%@include file="rq_header.jsp"%>
<script type="text/javascript">
	var url = window.location;
	url = new String(url);
	if (url.indexOf("67.202.61.205") != -1)
	{
		window.location="http://www.resumetuner.com";
	}
	function submit(type){
		if(type=="register"){
			window.location="Register.jsp?type=1";
			}
		else if(type=="login"){
			window.location="Login.jsp";
		}
	}

</script>

<div class="central">
<br />
<h1>Intelligent advice for your Resume!</h1>

<table border="0" cellpadding="0" width="97%" cellspacing="0">
	<tr>
		<td><br />
		<span style="font-family:Arial, Helvetica, sans-serif ;font-size:16px; "> 
	    Thank you for coming to ResumeSorter alpha page.  If you are not currently registered  
    	for the Alpha, you will be registered for the <b>Alpta</b>&#0153; release (between alpha and beta).
    	ResumeSorter's patent pending technology allows you to sort and rank resumes submitted for a
    	specific job posting using the relevant keywords and phrases extracted from the job ad.   
		</span>
		</td>
	</tr>
</table>

<!--<table border="0" cellpadding="0" width="97%" cellspacing="0">-->
<!--	<tr>-->
<!--		<td><br />-->
<!--		<span style="font-family:Arial, Helvetica, sans-serif ;font-size:16px; "> -->
<!--		Thank you for coming to ResumeTuner (TM) alpha page.<br /> <br />-->
<!--		ResumeTuner's patent pending technology allows you-->
<!--		to optimize your resume to a specific job posting using the relevant-->
<!--		keywords and phrases extracted from the job ad. This allows you to-->
<!--		optimize your resume for each job posting, highlighting your skills in-->
<!--		the language that of the job Ad, and what is being searched for by the-->
<!--		recruiters, crawlers, and application tracking systems. In effect, you-->
<!--		are helping them find you faster and moving your resumes closer to the-->
<!--		top of the pile :-) It even allows you to enter your own text and-->
<!--		combine it with our suggestions. <br /><br />-->
<!--		If you are not currently registered-->
<!--		for the Alpha, we will register you for the <i><b>Alpta</b></i> (between alpha and-->
<!--		beta), and let you know shortly-->
<!--		</span>-->
<!--		</td>-->
<!--	</tr>-->
<!--</table>-->


<p></p>

<br />
<br />
<br />
<br />
<br />
<br />

<%// Cookies
Cookie cookies[] = request.getCookies();
Cookie myCookie = null;
boolean existsCookie = false;
if (cookies != null)
{
	for (int i = 0; i < cookies.length; i++)
	{
		if (cookies[i].getName().equals(COOKIENAME))
		{
			myCookie = cookies[i];
			existsCookie = true;
			break;
		}
	}
}

int cookieCount;
// Cookie exists
if (existsCookie)
{
	try
	{
            BASE64Decoder decoder = new BASE64Decoder();
		String value = new String(decoder.decodeBuffer(myCookie.getValue()));
		String[] split = value.split(":");
		String login = split[0];
		myCookie.setMaxAge(Integer.MAX_VALUE);
		response.addCookie(myCookie);
		
		//TO DO REDIRECT SENDING LOGIN
		%>
<input type="hidden" name="login" value='<%out.write(login); %>'
	maxlength="50" />
<%
		request.getSession().setAttribute("login",login);
		response.sendRedirect("Login.jsp");
	}
	catch (Exception ex)
	{
		myCookie.setMaxAge(0);
		response.addCookie(myCookie);
		ex.printStackTrace();
	}
}
// Create cookie
else
{
	// Go to Login in either case
		response.sendRedirect("Login.jsp");
} %>
<center>

<div style="padding-left: 15px;">
<table width="30%" border="1" rules="none" cellpadding="0"
	cellspacing="0">

	<tr>
		<td width="40%" align="center"><input type="button" value='Login'
			onclick='submit("login")' /></td>
		<td width="20%">&nbsp;</td>
		<td width="40%" align="center"><input type="button"
			value='Sign Up' onclick='submit("register")' /></td>
	</tr>

</table>

</div>
</center>
</div>
<%@include file="rq_footer.jsp"%>
  
</body>
</html>