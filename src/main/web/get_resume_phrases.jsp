<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		System.out.println("Entered get resume phrases");
		Map<String,Integer> resumePhrasesMap = (Map<String,Integer>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
		if (resumePhrasesMap == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		boolean openCompanies = false;
		boolean openUniversities = false;
		String openTag = request.getParameter("openTag");
		if ("University".equals(openTag))
			openUniversities = true;
		else if ("Company".equals(openTag))
			openCompanies = true;
		List<String> resumePhrases =  new ArrayList<String>();
		List<String> companies =  new ArrayList<String>();
		List<String> universities =  new ArrayList<String>();
		for (int i = 0; i < 5; i++)
		{
			try
			{
				resumePhrases = new ArrayList<String>();
				companies = new ArrayList<String>();
				universities = new ArrayList<String>();
				for (String phrase: resumePhrasesMap.keySet())
				{
					int count = resumePhrasesMap.get(phrase);
					if (TagList.isCompany(phrase.trim()))
						companies.add(phrase);
					else if (TagList.isUniversity(phrase.trim()))
						universities.add(phrase);
					else if (count > 0)
						resumePhrases.add(phrase);
				}
			}
			catch (Exception x)
			{
				x.printStackTrace();
				Thread.sleep(300);
			}
		}
		String error = (String) session.getAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());
		if (error != null && error.trim().length() > 0)
		{
			error = error.replace("\n","<br>");

%>
<table width=100% border=1 cellpadding=3 bordercolor=red>
<tr><td align=center><font color=red><b>There were errors processing some resumes</b></font></td></tr>
</table>
<br>
<%		} %>
<table border=1 width="100%" id="tttable" bgcolor="#FFFFBB" cellpaddng=2 cellspacing=0 class="resumePhrases"><TBODY id="ttBody">
<tr>
<td colspan=2 align=center style="font-weight:bold"><b>Phrases found in Resumes<br>(Select to add to your list)</b><hr></td><td><img src="icon_close.gif" onclick="closeResumePhrases()"></td>
</tr>
<%
	Collection<PhraseList> jobPhraseList = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
	Set<String> jobPhrases = new HashSet<String>();
	for(PhraseList p : jobPhraseList)
	{   
		String phrase = p.iterator().next().getPhrase().getBuffer().toString();
		jobPhrases.add(phrase.toLowerCase().trim());
	}
	Collection<PhraseList> lemmaPhraseLists = (Collection<PhraseList>) session.getAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString());
	for(PhraseList p : lemmaPhraseLists)
	{   
		String phrase = p.iterator().next().getPhrase().getBuffer().toString().toLowerCase().trim();
		if (!jobPhrases.contains(phrase))
			jobPhrases.add(phrase);
	}

	for (int i = 0; i < resumePhrases.size(); i++)
	{
		for (int j = i; j < resumePhrases.size(); j++)
		{
			if (resumePhrasesMap.get(resumePhrases.get(j)) > resumePhrasesMap.get(resumePhrases.get(i))
				|| (resumePhrasesMap.get(resumePhrases.get(j)) == resumePhrasesMap.get(resumePhrases.get(i)) && resumePhrases.get(j).compareTo(resumePhrases.get(i)) < 0))
			{
				String temp = resumePhrases.get(i);
				resumePhrases.set(i, resumePhrases.get(j));
				resumePhrases.set(j, temp);
			}
		}
	}
	int i = 0;
	if (companies.size() > 0)
	{
%>
<tr>
<td colspan=2>
	<img  src='plus-gsquare.png' height=12 onclick='openCompanies(<%=openCompanies%>);'><b>Companies</b>
</td>
</tr>
<%
	if (openCompanies)
	{
		for (String phrase: companies)
		{
	%>
		<tr id="resPhrase<%=i%>" onMouseOver="this.style.backgroundColor='#FFFF00'" onMouseOut="this.style.backgroundColor=''">
		<td nowrap>
		&nbsp;&nbsp;
		</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" title='Select Phrase' onclick="addKeyName('<%=phrase%>', <%=i%>)" ><%=phrase%></a></td>
		<td><%=resumePhrasesMap.get(phrase)%></td>
		</tr>
	<%
			i++;
		}
	}
	}
	if (universities.size() > 0)
	{
%>
<tr>
<td colspan=2>
	<img  src='plus-gsquare.png' height=12 onclick='openUniversities(<%=openUniversities%>);'><b>Universities</b>
</td>
</tr>
<%
	if (openUniversities)
	{
		for (String phrase: universities)
		{
		if (jobPhrases.contains(phrase.toLowerCase())) continue;
%>
	<tr id="resPhrase<%=i%>" onMouseOver="this.style.backgroundColor='#FFFF00'" onMouseOut="this.style.backgroundColor=''">
	<td nowrap>
	&nbsp;&nbsp;
	</td>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" title='Select Phrase' onclick="addKeyName('<%=phrase%>', <%=i%>)"><%=phrase%></a></td>
	<td><%=resumePhrasesMap.get(phrase)%></td>
	</tr>
<%
			i++;
		}
	}
	}
	for (String phrase: resumePhrases)
	{
		if (jobPhrases.contains(phrase.toLowerCase())) continue;
		String style ="";
		if (TagList.isCompany(phrase))
			style = "style='font-weight:bold'";
		else if (TagList.isUniversity(phrase))
			style = "style='font-weight:bold'";
%>
	<tr id="resPhrase<%=i%>" onMouseOver="this.style.backgroundColor='#FFFF00'" onMouseOut="this.style.backgroundColor=''">
	<td nowrap>
<%	if (user.isAdmin() || user.isTester()) { %>
	<img  src='x.png' height=12 onclick='deleteResumePhrase("<%=phrase%>", <%=i%>);'>
<%		} %>
	</td>
	<td><a href="#" title='Select Phrase' onclick="addResumePhrase('<%=phrase%>', <%=i%>)" <%=style%>><%=phrase%></a></td>
	<td><%=resumePhrasesMap.get(phrase)%></td>
	</tr>
<%
		i++;
	}
%>
	<tr>
	<td colspan=100% ><br>&nbsp;</td>
	</tr>	
	<tr>
	<td colspan=100% align=center style="font-weight:bold"><input type=button value=Close onclick="closeResumePhrases()">&nbsp;<input type=button value=Refresh onclick="showResumePhrasesPopup()"></td>
	</tr>
	<tr>
	<td colspan=100% ><br>&nbsp;</td>
	</tr>	
	</TBODY></table>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>