<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%
	String addphrase = request.getParameter("additional_phrase");
	String command = request.getParameter("command");
	System.out.println("Entered delete additional phrase:" + addphrase + " command:" + command);
	if (addphrase == null) return;
	List<String> deletedAddPhrases = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.DELETED_ADD_PHRASE_LIST.toString());
	if (deletedAddPhrases == null) deletedAddPhrases = new ArrayList<String>();
	if ("delete".equals(command))
		deletedAddPhrases.add(addphrase);
	else
		deletedAddPhrases.remove(addphrase);
	session.setAttribute(SESSION_ATTRIBUTE.DELETED_ADD_PHRASE_LIST.toString(), deletedAddPhrases);
%>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>