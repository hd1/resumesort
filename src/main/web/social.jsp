<%-- 
    Document   : index
    Created on : Aug 1, 2012, 10:51:46 PM
    Author     : Vinod
--%>

<%@page import="com.textonomics.social.SocialProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
    String username= request.getParameter("q");
    SocialProfile sp = new SocialProfile();
    String linkedInProfileURL = sp.getLinkedInProfileURL(username);
    %>
    <head>
        <script type="text/javascript">
        
        function loginFacebook(){

            FB.login(function(response) {
                  if (response.authResponse) {
                    console.log("User is connected to the application.");
                    var accessToken = response.authResponse.accessToken;
                    getFacebookProfileId(accessToken,"<%=username%>" );
                  }
            });
		}
        
        function getFacebookProfileId(accessToken, userName)
		{
            var xmlhttp;
            var url = "<%=request.getRequestURL().toString().replace("social.jsp","")%>/getSocialProfiles.jsp?"
            
            url += "accessToken=" + accessToken + "&userName=" + userName;
            
            if (window.XMLHttpRequest)
              {// code for IE7+, Firefox, Chrome, Opera, Safari
              xmlhttp=new XMLHttpRequest();
		}
		else
              {// code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
              }
            
            xmlhttp.onreadystatechange=function()
		{
              if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    id=  parseInt(xmlhttp.responseText);
                    document.getElementById("facebook-profile").setAttribute("uid",id);
                    document.getElementById("facebook-profile").style.display="inline";
                    document.getElementById("facebook-logo").style.display="none";
                    FB.XFBML.parse();
                }
              }
            
            xmlhttp.open("GET",url,true);
            xmlhttp.send();
        }
        </script>
        <script type="text/javascript" src="http://platform.linkedin.com/in.js">
	<%
		if (request.getRequestURL().toString().contains("resumesort.com"))
		{
	%>  api_key: slct30zu57dk <%
		}
		else if (request.getRequestURL().toString().contains("resumetuner.com"))
		{
	%>  api_key: hprez6o7wi7e <%
		}
		else
		{
	%>  api_key: w1b6zwsm36ts
	<%	} %>
		authorize: true
        </script>
        <script src="http://platform.twitter.com/anywhere.js?id=o1UE1QoCUNLHtcChreBRiA&v=1" type="text/javascript"></script>
        <title>Social Plugins</title>
    </head>
    <body>
        <!-- Facebook JavaScript SDK Loading Script -->
        <div id="fb-root"></div>
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '281360288542157', // App ID
              //channelUrl : '//localhost:8084/SocialPlugins/channel.html', // Channel File
              status     : true, // check login status
              cookie     : true, // enable cookies to allow the server to access the session
              xfbml      : true, // parse XFBML
              oauth      : true  // oauth enabled
            });

            // Additional initialization code here
          };

          // Load the SDK Asynchronously
          (function(d){
             var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement('script'); js.id = id; js.async = true;
             js.src = "//connect.facebook.net/en_US/all.js";
             ref.parentNode.insertBefore(js, ref);
           }(document));
        </script>

        <!-- LinkedIn Profile Plugin Script -->
        
        <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
        <script type="IN/MemberProfile" data-id="<%=linkedInProfileURL%>" data-format="hover" data-text="<%=username%>" data-related="false"></script>
            
        <fb:profile-pic id="facebook-profile" style="display: none;" uid="587811180" facebook-logo="true" linked="true" width="40" height="40"></fb:profile-pic>
        <a style="decoration:none;" href="#"><img id="facebook-logo" src="images/f_logo.jpg" style="height:40px; width: 40px; display:inline;" onclick="loginFacebook()" /></a>

        <script>
        FB.getLoginStatus(function(response) {
          if (response.status === 'connected') {
            var uid = response.authResponse.userID;
            var accessToken = response.authResponse.accessToken;
            
            getFacebookProfileId(accessToken, "<%= username %>");
            document.getElementById("facebook-profile").style.display="inline";
            document.getElementById("facebook-logo").style.display="none";
          } else if (response.status === 'not_authorized') {
          } else {
              
          }
        });
        
        
        </script>
        <%
           String userName =sp.getTwitterProfileURL(request.getParameter("q")); 
           if(!userName.equalsIgnoreCase("not_found")){
        %>

        <span id="username">

        <img style="width:40px; height:40px;" src="https://twitter.com/images/resources/twitter-bird-white-on-blue.png" alt="<%= "@" + userName%>"/>
        </span>
        <script type="text/javascript">

        twttr.anywhere(function (T) {
          T("#username>img").hovercards({
              username: function(node) {
                    return node.alt;
              }
          });
        });

        </script>
        <% }
        %>
    </body>
</html>
