<%@ page import="java.util.*"%><%@ page import="java.io.*"%><%@ page import="com.textonomics.*"%><%@ page import="com.textonomics.web.*"%><%@ page import="com.textonomics.user.*"%><%@ page import="com.textonomics.user.dataprovider.*"%><%@ page import="com.textonomics.mail.*"%><%@ page import="com.textonomics.db.*"%><%@ page import="com.textnomics.data.*"%><%@ page import="com.textonomics.subscription.*"%>
<%
		String[] emails = request.getParameterValues("resumeemail");
		String[] resumeIdxs = request.getParameterValues("emailResumeIdx");
		System.out.println("share_resumes: num emails =" + emails.length + " num resumes =" + resumeIdxs.length);
		if (emails == null || emails.length == 0) return;
		if (resumeIdxs == null || resumeIdxs.length == 0) return;
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (user == null) 
		{
%>
Error: Your session has expired.
<%
			return;
		}
		String postingId = (String)	session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString());
		List<File> resumeFiles = (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
		if (posting == null)
		{
%>
Error: Job Posting: <%=postingId%> not found.
<%
	   }

		try
		{
			UserDataProvider userProvider = UserDataProviderPool.getInstance().acquireProvider();
			for (int i = 0; i < emails.length; i++)
			{
				if (emails[i].indexOf("@") == -1) continue;
				String email = emails[i].trim();
				User shareUser = userProvider.getUser(email);
				boolean created = false;
				if (shareUser == null)
				{
					created = true;
					shareUser = new User();
					shareUser.setEmail(email);
					shareUser.setLogin(email);
					shareUser.setName(email);
					shareUser.setCompany(user.getCompany());
					shareUser.setParticipationCode("Invited Temp By " + user.getLogin());
					shareUser.setPassword(SendPassword.generatePassword());
					shareUser.setState(1);
					userProvider.insertUser(shareUser);
					userProvider.updateUserField(shareUser, "company");
					userProvider.commit();
				}
				String subject = "Shared Resume from Resume Sort";
				StringBuffer bodyBuf = new StringBuffer(user.getName() + " has shared resumes and their evaluation with you.<br><br>\n");
				bodyBuf.append("Please log in to Resume Sort using the link below to view the resumes:<br><br>\n");
				for (String resumeIdx: resumeIdxs)
				{
					int idx = new Integer(resumeIdx.trim()).intValue()-1;
					String fileName = resumeFiles.get(idx).getName();
					List<ResumeScore> oldScores = new ResumeScore().getObjects("RESUME_FILE = " + UserDBAccess.toSQL(fileName)
								+ " and JOB_ID =" + posting.getId() + " and user_name =" + UserDBAccess.toSQL(user.getLogin()));
					if (oldScores.size() > 0)
					{
						ResumeScore rs = oldScores.get(0);
						SharedResumeResult srr = new SharedResumeResult();
						List<SharedResumeResult> oldSrrs =  new SharedResumeResult().getObjects("RESUME_SCORE_ID=" + rs.getId() + " and IDUSER = " + shareUser.getId());
						if (oldSrrs.size() > 0)
							srr = oldSrrs.get(0);
						srr.setResumeScoreId(rs.getId());
						srr.setUserId(shareUser.getId());
						srr.setPerm1(false);
						if ("1".equals(request.getParameter("commperm_"+i)))
							srr.setPerm1(true);
						if (srr.getId() > 0)
							srr.update();
						else
							srr.insert();
						bodyBuf.append("<a href='" + request.getRequestURL().toString().replace("share_resumes.jsp","") + "view_shared_resume.jsp?shareId=" + srr.getId() + "&inviter=" + user.getLogin() + "&login=" + shareUser.getLogin() + "'>" + fileName + "</a><br><br>\n");
					}
				}
				if (created)
				{
					bodyBuf.append("Your username for the site: " + shareUser.getLogin() + "<br>\n");
					bodyBuf.append("Your password for the site: " + shareUser.getPassword() + "<br><br>\n");
				}
				bodyBuf.append("Thank you for using Resume Sort<br><br>\n");
				GenericMailer.getInstance().sendMail(email, subject, bodyBuf.toString());
			}
		}
		catch (Exception x)
		{
			x.printStackTrace();
		}
		finally {
			UserDataProviderPool.getInstance().releaseProvider();
		}
%>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>