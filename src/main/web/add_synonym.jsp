<%@ page import="java.util.*"%>
<%@ page import="com.textonomics.*"%>
<%@ page import="com.textonomics.web.*"%>
<%@ page import="com.textonomics.user.*"%>
<%@ page import="com.textnomics.data.*"%>
<%@ page import="com.textonomics.db.*"%>
<%@ page import="com.textonomics.wordnet.model.*"%>
<%
		String phrase = request.getParameter("phrase");
		String synonym = request.getParameter("synonym");
		String command = request.getParameter("command");
		System.out.println("add_synonym: command=" + command + " phrase=" + phrase + " synonym=" + request.getParameter("synonym"));
		if (phrase == null) return;
		Map<String,String> phrasesSynonyms = (Map<String,String>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());
		Map<String,Set<String>> deletedSynonyms = (Map<String,Set<String>>)           session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_DELETED_SYNONYMS.toString());
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (phrasesSynonyms == null || user == null) 
		{
%>
<table><tr><td><b>Your session has expired.</b></td></tr></table>
<%
			return;
		}
		String[] synonyms = new String[0];
		String synonymsStr = "";
		if (phrasesSynonyms.get(phrase.trim()) != null)
		{
			synonymsStr = phrasesSynonyms.get(phrase.trim()).trim();
			if (synonymsStr.length() > 1)
			{
				synonyms = synonymsStr.split("\\|");
			}
		}
		synonymsStr = synonymsStr + "|" + synonym;
		System.out.println("synonymsStr:" +synonymsStr);
		phrasesSynonyms.put(phrase.trim(), synonymsStr);
		AddedSynonym rs = new AddedSynonym();
		rs.setPhrase(phrase.trim());
		rs.setSynonym(synonym);
		rs.setUserName(user.getLogin());
		rs.insert();
%>
<%!
	private int getInt(String value)
	{
		try
		{
			return new Integer(value).intValue();
		}
		catch (Exception x)
		{
			return 0;
		}
	}
%>