package com.textonomics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.textonomics.nlp.Sentence;
import java.io.Serializable;

/**
 * Copyright 2009 Textonomics. Inc. <br>
 * 
 * The class represents an abstraction of a document. It holds all the sentences
 * that were identified and all the tokens that were extracted by a specific
 * {@link DocumentProcessor} that created the current instance. One instance of
 * a {@link DocumentProcessor} should be used to create a single
 * {@link Document}. The identified phrases are stored in a HashMap, the key is
 * a String and value is a list of phrases (@link PhraseList) that holds all the
 * occurrences of the the phrase in the document.
 * 
 * If a phrase occurs two times in a document it will only have one entry in the
 * hashmap, but the list of phrases will have two elements.
 * 
 * 
 * @author Nuno Seco <br>
 * 
 */

public class DocumentRS extends Document implements Serializable {
	private static final long serialVersionUID = 27L;

	public static final String OOFFICE_SENTENCE_SPLITTER = "OOSentenceDetector";
	public static final String TIKA_SENTENCE_SPLITTER = "TikaSentenceDetector";
	public static final String HTML_SENTENCE_SPLITTER = "HtmlSentenceDetector";
	/*
	 * Lemmatized sentences
	 */
	private List<Sentence> lemmatizedSentences;

	/*
	 * Type of sentenceSplitter used
	 */
	private String sentenceSplitterType;

	private String email;
	private String address;
	private String zipcode;
	private String name;

	/**
	 * Protected constructor that should only be called by the corresponding
	 * {@link DocumentProcessor}.
	 */
	protected DocumentRS() {
		phrases = new HashMap<String, PhraseList>();
		sentencePhrases = new HashMap<Integer, PhraseList>();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSentenceSplitterType() {
		return sentenceSplitterType;
	}

	public void setSentenceSplitterType(String sentenceSplitterType) {
		this.sentenceSplitterType = sentenceSplitterType;
	}

	/**
	 * Obtains the noun and verb tokens that are in this instance and not in the
	 * given parameter (set difference).
	 * 
	 * @param another
	 *            The document holding the {@link Phrase} to be removed from
	 *            this instance.
	 * @return Set of string that represent the tokens in this instance that are
	 *         not
	 */
	public Set<String> getDifference(DocumentRS another) {
		HashSet<String> terms = new HashSet<String>();
		PhraseList tokenList;

		for (String term : phrases.keySet()) // EBB get the keySet from the
												// hashTable, which are the
												// strings used. NOT SURE IF
												// THESE ARE LEMMATIZED OR
												// NOT!!!
		{

			if (!another.phrases.containsKey(term) && term.trim().indexOf(" ") != -1) // if
																						// the
																						// "term"
																						// string
																						// is
																						// not
																						// in
																						// the
																						// other
																						// document,
																						// and
																						// is
																						// a
																						// noun
																						// or
																						// verb
																						// phrase
																						// then
																						// add
																						// it
																						// to
																						// the
																						// list
																						// of
																						// terms.
			{
				tokenList = phrases.get(term);
				if (tokenList.getFirst().isNounPhrase() || tokenList.getFirst().isVerbPhrase())
					terms.add(term);
			}
		}

		return terms;
	}

	/**
	 * Accessor method returns all the phrases found in the document clustered
	 * in {@link PhraseList}
	 * 
	 * @return an unmodifiable map.
	 */
	public Collection<PhraseList> getDocumentPhrases() {
		List<PhraseList> tmplist = new ArrayList(); // create temp list, so it
													// will be serializable
		for (PhraseList pl : phrases.values())
			tmplist.add(pl);
		return tmplist;
	}

	/**
	 * Returns all Phrases found in the document clustered into
	 * {@link PhraseList}, but ordered according to some criteria defined in the
	 * comparator
	 * 
	 * @param comparator
	 *            Defines the criteria to do the sorting
	 * @return Ordered set of {@link PhraseList}
	 */
	// TODO EBB why is this called, and we want the ordered list of the
	// phraseLists?
	// EBB look at its usage.
	public Set<PhraseList> getDocumentPhrases(Comparator<PhraseList> comparator) {
		TreeSet<PhraseList> ordered = new TreeSet<PhraseList>(comparator);
		ordered.addAll(phrases.values());
		return Collections.unmodifiableSet(ordered);
	}

	/**
	 * Gets all the phrases that are in a given sentence identified by the index
	 * 
	 * @param sentence
	 *            The index of the sentence to inspect
	 * @return The list of phrases
	 */
	public PhraseList getPhrases(int sentence) {
		return sentencePhrases.get(sentence);
	}

	/**
	 * Gets the sentence identified by the given index. An index of 0 represents
	 * the first phrase.
	 * 
	 * @param index
	 *            The index of the sentence
	 * @return The string representing the sentence.
	 */
	public String getSentence(int index) {
		return sentences.get(index).toString();
	}

	/**
	 * Accessor the sentence attribute
	 * 
	 * @return an unmodifiable list of strings.
	 */
	public List<Sentence> getSentences() {
		return Collections.unmodifiableList(sentences);
	}

	/**
	 * Adds o phrase to the map. Starts by verifying if the underlying string
	 * has already been identified previously. If not then a new
	 * {@link PhraseList} is created and added to the map using the string as
	 * the key and the phraseList as the value.
	 * 
	 * @param phrase
	 *            The {@link Phrase} to be added
	 */
	public void addDocumentPhrase(Phrase phrase) {
		// PhraseList previous = phrases.get(phrase.getBuffer() + ":" +
		// phrase.getNgram().getPosSequence());
		PhraseList previous = phrases.get(phrase.getBuffer());
		if (previous == null) // TODO EB BUG -- FIXED now we see all the
								// sentences -- here he uses one key to get the
								// PhraseList but below he uses another one to
								// add it in.
			previous = new PhraseList(this);

		previous.add(phrase);
		phrases.put(phrase.getBuffer(), previous); // TODO EB POTENTIAL BUG
													// FIXED... the key above
													// is:
													// phrases.get(phrase.getBuffer()
													// + ":" +
													// phrase.getNgram().getPosSequence());
													// But here he is using just
													// phrase.getBuffer!!! and
													// not concatenating with +
													// ":" +
													// phrase.getNgram().getPosSequence()).
													// So when it gets looked up
													// -- for instance in --
													// WNPhraseSuggester.obtainSuggestioClusters:94,
													// then the index has
													// changed and it is never
													// found. Either that, or
													// the concatenation above
													// is not looked up properly
		// TODO EBB HUGE BUG... the HashMap "phrases" overwrites the list of
		// phrases with a new "previous". Seems like previous will always have
		// lenght one!

		previous = sentencePhrases.get(phrase.getSentence());
		if (previous == null)
			previous = new PhraseList(this);

		previous.add(phrase);
		sentencePhrases.put(phrase.getSentence(), previous);

	}

	public List<String> getContexts(String phraseText) {
		PhraseList phraseList = phrases.get(phraseText);
		if (phraseList == null) {
			return null;
		} else {
			// System.out.println("text:" + phraseText + " contexts:" +
			// phraseList.getContexts());
			return phraseList.getContexts();
		}
	}

	/**
	 * Mutator methods for the sentences attribute. Should only be called from
	 * within a {@link DocumentProcessor}.
	 * 
	 * @param sentences
	 *            new array of sentences
	 */
	protected void setSentences(List<Sentence> sentences) {
		this.sentences = sentences;
	}

	public List<Sentence> getLemmatizedSentences() {
		return lemmatizedSentences;
	}

	public void setLemmatizedSentences(List<Sentence> lemmatizedSentences) {
		this.lemmatizedSentences = lemmatizedSentences;
	}
}
