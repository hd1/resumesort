/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics;

import com.textnomics.data.TagList;
import com.textonomics.db.DBAccess;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dev Gude
 */
public class TagListMgr {
	public static void main(String[] args) {
		String fileName = "/textnomics/docs/university_names_top400.txt";
		fileName = "/textnomics/docs/university_names_top400_2.txt";
		fileName = "/textnomics/docs/universityName_UK_India_China_Canada_largest.txt";
		if (args == null || args.length < 1 || args[0].trim().length() == 0) {
			System.out.println("Please pass resume directory or file name as a parameter");
		} else
			fileName = args[0];
		PlatformProperties.resourceDirectory = "/Textnomics/ResumeCakeWalk/web/WEB-INF/Resources";
		try {
			new TagListMgr().process(fileName);
		} catch (Exception ex) {
			Logger.getLogger(TagListMgr.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.exit(0);
	}

	public void process(String fileName) {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			int linenum = 0;
			int rownum = 0;
			String topic = "";
			while ((line = bufferedReader.readLine()) != null) {
				linenum++;
				// if (linenum > 20) break;
				if (linenum % 100 == 0)
					System.out.println("Processing line:" + linenum);
				// if (line.indexOf('\t') == -1)
				// {
				// rownum = 0;
				// continue;
				// }
				rownum++;

				String[] columns = line.split("\t");
				// if (columns.length < 5)
				// {
				// System.out.println("Invalid line " + linenum + ":" + line);
				// continue;
				// }

				List<TagList> tags = new DBAccess().getObjects(new TagList(),
						"tag_name = " + DBAccess.toSQL(columns[0].trim()));
				if (tags.size() > 0)
					continue;
				TagList tag = new TagList();
				String name = columns[0].trim();
				if (name.indexOf("(") != -1) {
					String abbrev = name.substring(name.indexOf("(") + 1);
					abbrev = abbrev.replace(')', ' ').trim();
					name = name.substring(0, name.indexOf("("));
					if (abbrev.length() > name.length()) {
						String temp = abbrev;
						abbrev = name;
						name = abbrev;
					}
					tag.setAbbrev(abbrev);
				}
				if (name.trim().length() == 0)
					continue;
				tag.setTagName(name);
				tag.setType("University");
				tag.setUserName("dev.gude@textnomics.com");
				tag.insert();
				tag.setCode(tag.getId());
				tag.update();
			}
			bufferedReader.close();
		} catch (Exception x) {
			x.printStackTrace();
		} finally {
		}

	}
}
