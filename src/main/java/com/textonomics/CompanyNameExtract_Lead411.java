package com.textonomics;
/**
 *
 * @author Hanh Pham
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Hanh Pham
 * 
 * 
 *         Extract list of companies from
 *         http://www.lead411.com/top-companies-list.taf
 * 
 * 
 *         **JSOUP external package is used for easy html-parsing
 */

public class CompanyNameExtract_Lead411 {

	public static void main(String[] args) {

		String fileName = "company_names/companyNames_from_Lead411_2000_part1.6.txt";

		Elements links = null;

		try {

			FileWriter writer = new FileWriter(fileName, true);

			for (int i = 1; i < 2101; i = i + 100) {
				String linkUrl = "http://www.lead411.com/top-companies-list.taf?&_start=" + i;
				System.out.println(linkUrl);
				Document doc = Jsoup.connect(linkUrl).get();

				links = doc.select("td");

				// System.out.println(doc.toString());
				// System.out.println(links.toString());

				Pattern p = Pattern.compile("(\\<[^\\>]+\\>).*");

				// (jsoup is not correctly selecting all the "td" elements in
				// this case, instead all the info we want is bunched in the 1st
				// result)
				Element link = links.get(1);

				String[] lines = link.toString().split("\\n");
				System.out.println(lines.length);

				for (int j = 0; j < lines.length; j++) {
					// System.out.println(lines[j]+"\n\n");

					if (lines[j].contains("a href") && lines[j].contains("width=\"350\"")) {
						lines[j] = lines[j].trim();

						Matcher matcher = p.matcher(lines[j]);
						while (matcher.matches()) {
							lines[j] = lines[j].replace(matcher.group(1), "").trim();
							matcher = p.matcher(lines[j]);
						}

						lines[j] = lines[j].replaceAll("\\<.+\\>", "");
						System.out.println(lines[j] + "\n");
						writer.append(lines[j]).append("\n");
					}

				}

			} // end outer for loop

			writer.close();

		} catch (IOException e) {
		}

	}

}
