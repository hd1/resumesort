/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.textonomics.social;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

/**
 *
 * @author Vinod
 */
public class SocialProfile {

	OAuthService service;
	Token requestToken;

	public SocialProfile() {

		service = new ServiceBuilder().provider(TwitterApi.class).apiKey("o1UE1QoCUNLHtcChreBRiA")
				.apiSecret("MOJ0MNUUZtOPTggVmWD1yzxXCvHXD2uNLenvzgYAqVE").build();

		// requestToken = service.getRequestToken();
		requestToken = new Token("559926950-ReM9s16VnVzBhr9Y5POgYm1ru6FxntXJuEZ1e3M3",
				"lsefR1cYby6IAM9Zwu59X3G8T3PtOhLprSrxYkzMJU");
		// String authUrl = service.getAuthorizationUrl(requestToken);

		// System.out.println(authUrl);
	}

	public String getFacebookProfileID(String accessToken, String fullName) {

		OAuthService service = new ServiceBuilder().provider(FacebookApi.class).apiKey("281360288542157")
				.apiSecret("b9603f70af3032bd2e8aa5e398e544c2").build();

		// Token requestToken = service.getRequestToken();

		// Token accessToken = new
		// Token("AAADZC5UwNbc0BAOQyLu6cgRVlatmU3qFX1pRUlKcZAJ79eeKjXyiFEJZBvTpgYZB6j7LkZCck1dgkZAmZAgfTiNyvCZClGrf8wVXfOPaNvyITAZDZD","b9603f70af3032bd2e8aa5e398e544c2");

		String url = "https://graph.facebook.com/search?";
		if (fullName != null)
			url += "q=" + URLEncoder.encode(fullName);
		else
			url += "facebook";

		url += "&type=user&access_token=" + accessToken;
		OAuthRequest request = new OAuthRequest(Verb.GET, url);
		// service.signRequest(accessToken, request); // the access token from
		// step 4
		Response response = request.send();

		String jsonString = response.getBody();
		// String jsonString = responseString.substring(1,
		// responseString.length()-1);

		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		JSONArray jsonArray;
		if (jsonString.length() > 0) {
			jsonArray = jsonObject.getJSONArray("data");
		} else {
			return "not_found";
		}

		String userId = JSONObject.fromObject(jsonArray.get(0)).getString("id");

		System.out.println("Facebook User Id: " + userId);
		return userId;

	}

	public String getTwitterProfileURL(String fullName) {

		/*
		 * OAuthService service = new
		 * ServiceBuilder().provider(TwitterApi.class) .apiKey("w1b6zwsm36ts")
		 * .apiSecret("8gpTjDzfj4AJPnLm") .build();
		 * 
		 * Token requestToken = service.getRequestToken(); //Token requestToken
		 * = new Token("559926950-ReM9s16VnVzBhr9Y5POgYm1ru6FxntXJuEZ1e3M3",
		 * "lsefR1cYby6IAM9Zwu59X3G8T3PtOhLprSrxYkzMJU"); String authUrl =
		 * service.getAuthorizationUrl(requestToken);
		 * 
		 * System.out.println(authUrl);
		 */
		// Verifier v = new Verifier("24442");
		// Token accessToken = service.getAccessToken(requestToken, v);

		String url = "http://api.twitter.com/1/users/search.json?q=";
		if (fullName != null)
			url += URLEncoder.encode(fullName);
		else
			url += "twitter";

		OAuthRequest request = new OAuthRequest(Verb.GET, url);
		// OAuthRequest request = new OAuthRequest(Verb.GET,
		// "http://api.linkedin.com/v1/people-search?first-name=vinod%20reddy&last-name=rondla");
		service.signRequest(requestToken, request); // the access token from
													// step 4
		Response response = request.send();

		String responseString = response.getBody();
		String jsonString = responseString.substring(1, responseString.length() - 1);

		JSONObject jsonObject;
		if (jsonString.length() > 0) {
			jsonObject = JSONObject.fromObject(jsonString);
		} else {
			return "not_found";
		}

		String userName = jsonObject.get("screen_name").toString();

		System.out.println("User name: " + userName);
		return userName;

	}

	public String getLinkedInProfileURL(String query) {
		String profileURL = "";

		try {

			query = "linkedin " + query;

			query = URLEncoder.encode(query);
			String url = "https://api.datamarket.azure.com/Bing/Search/Web?Query=%27" + query
					+ "%27&$top=10&$format=JSON";

			String resultsXML = getResults(url);

			JSONObject jsonObject;
			if (resultsXML.length() > 0) {
				jsonObject = JSONObject.fromObject(resultsXML);
			} else {
				return "not_found";
			}

			JSONArray resultArray = jsonObject.getJSONObject("d").getJSONArray("results");

			for (int i = 0; i < resultArray.size(); i++) {

				JSONObject resultObject = (JSONObject) resultArray.get(i);
				String resultURL = resultObject.getString("Url");

				if (resultURL.startsWith("http://www.linkedin.com/in")
						|| (resultURL.startsWith("http://www.linkedin.com/in")
								&& !resultURL.startsWith("http://www.linkedin.com/pub/dir"))) {

					profileURL = resultURL;
					break;
				}
			}

			System.out.println("LinkedIn Profile: " + profileURL);

			/*
			 * DocumentBuilderFactory dbf =
			 * DocumentBuilderFactory.newInstance(); DocumentBuilder db =
			 * dbf.newDocumentBuilder(); ByteArrayInputStream bis = new
			 * ByteArrayInputStream(resultsXML.getBytes()); org.w3c.dom.Document
			 * doc = db.parse(bis);
			 * 
			 * NodeList entries = doc.getElementsByTagName("d:Url"); Node entry;
			 * 
			 */
		} catch (NoSuchElementException ex) {
			Logger.getLogger(SocialProfile.class.getName()).log(Level.SEVERE, null, ex);
		} /*
			 * catch (SAXException ex) {
			 * Logger.getLogger(SocialProfile.class.getName()).log(Level.SEVERE,
			 * null, ex); } catch (IOException ex) {
			 * Logger.getLogger(SocialProfile.class.getName()).log(Level.SEVERE,
			 * null, ex); } catch (ParserConfigurationException ex) {
			 * Logger.getLogger(SocialProfile.class.getName()).log(Level.SEVERE,
			 * null, ex); }
			 */

		return profileURL;
	}

	private String getResults(String bingUrl) {

		StringBuilder docText = new StringBuilder();

		try {
			String accountKey = "OeUCO9Kq1ISL0pERD3z1JjheXV/J4iLxiTFuAzNX/Zk=";
			byte[] accountKeyBytes = Base64.encodeBase64((accountKey + ":" + accountKey).getBytes());
			String accountKeyEnc = new String(accountKeyBytes);

			URL url = new URL(bingUrl);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setRequestProperty("Authorization", "Basic " + accountKeyEnc);

			// urlConnection.setReadTimeout(10000);
			urlConnection.connect();

			BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				docText.append(inputLine);
			}
			in.close();

		} catch (IOException ex) {
			Logger.getLogger(SocialProfile.class.getName()).log(Level.SEVERE, null, ex);
		} catch (Exception ex) {
			Logger.getLogger(SocialProfile.class.getName()).log(Level.SEVERE, null, ex);
		}
		// System.out.println(docText.toString());
		return docText.toString();
	}

	public static void main(String[] args) {
		SocialProfile sp = new SocialProfile();
		sp.getLinkedInProfileURL("esfandiar bandari");
		sp.getTwitterProfileURL("esfandiar");
		sp.getFacebookProfileID(
				"AAADZC5UwNbc0BAFtnXscr751AsbkyIsIt8ufIzBrY3siK5vs2G5hWeDpwvel7r6chvwwC6Bd9ZAxXf2dG0dywm894fqyAdhHnp5HZCRlQZDZD",
				"preethi mudda");
	}

}
