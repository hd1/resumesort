/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.aws;

import com.textonomics.PlatformProperties;

/**
 *
 * @author Preeti Mudda
 */
public class ResumeSortAWSCredentials implements com.amazonaws.auth.AWSCredentials {

	public String getAWSAccessKeyId() {
		return PlatformProperties.getInstance().getProperty("com.textonomics.AWSAcessKeyId");
	}

	public String getAWSSecretKey() {
		return PlatformProperties.getInstance().getProperty("com.textonomics.AWSSecretKey");
	}

}
