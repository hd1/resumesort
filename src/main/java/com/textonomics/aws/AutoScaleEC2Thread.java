/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.textonomics.aws;

import com.textonomics.web.BeanstalkClient;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 * AutoScaleEC2Thread that is running continuosly to watch the EC2Instancs 1.
 * Creates a new EC2 instance 2. Checks if there are any jobs in the beanstalkd
 * tube 3. Terminates the EC2 isntance
 * 
 * @author Preeti
 */
public class AutoScaleEC2Thread extends Thread {
	static Logger logger = Logger.getLogger(AutoScaleEC2Thread.class);
	private static final int maximumInstances = 5;
	private int minInstances = 1; // minimum instances up
	private int maxInstances = 1; // maximum instances up
	private int dynamicDefinedNum = maxInstances;// initially dynamicDefinedNum
													// is equal to maxInstances

	public AutoScaleEC2Thread() {
	}

	/**
	 * 0. Loop infinately 1. Creates a new EC2 instance 2. Terminates the EC2
	 * isntance if no jobs in the beanstalkd
	 * 
	 */
	public void run() {
		logger.info("inside the AutoScaleEC2 thread testing");
		// 0. Loop infinately
		while (true) {
			try {
				// 1.0 if no instances are up
				if (AutoScaleEC2.getInstance().isInstancesUp() == false) {
					logger.info("Non instances are up so checking further" + BeanstalkClient.upFirst);
					// 1.1 if this is the first time up then create new instance
					// and set upFirst = false
					if (BeanstalkClient.upFirst == true) {
						logger.info("creating instances for first time");
						AutoScaleEC2.getInstance().createInstances(minInstances, maxInstances);
						BeanstalkClient.upFirst = false;
					}
					// 1.2 if Beanstalkd got new jobs
					// 1.2.1 if dynamicDefineNum == maxInstance then create
					// maxInstances
					// 1.2.2 else create dynamicDefinedNum instance
					else if (BeanstalkClient.hasJobs() == true) {
						dynamicDefinedNum = BeanstalkClient.getInstance().getNoOfInstances();
						logger.info("Non instances are up and tube has jobs");
						// 1.2.1 if dynamicDefineNum == maxInstance then create
						// maxInstances
						if (dynamicDefinedNum == maxInstances) {
							logger.info("creating instances since no instance are up and there are jobs in the tube"
									+ dynamicDefinedNum);
							AutoScaleEC2.getInstance().createInstances(minInstances, maxInstances);
						}
						// 1.2.2 else create dynamicDefinedNum instance
						else {
							logger.info("changing the max number of instances" + dynamicDefinedNum);
							AutoScaleEC2.getInstance().createInstances(minInstances, dynamicDefinedNum);
						}
					} // 1.3 logs the message that there is no new job in
						// beanstalkd to start new instance
					else {
						logger.info("There is no job in the beanstalkd tube so executing this line");
					}
				}
				// 2. Terminates the EC2 isntance
				else {
					dynamicDefinedNum = BeanstalkClient.getInstance().getNoOfInstances();
					logger.info("Instances are up and checking whether there are any jobs left in the tube");
					// 2.1 if Beanstalkd has no jobs then terminate the
					// instances
					if (BeanstalkClient.hasJobs() == false) {
						logger.info("terminating instances. job is done");
						AutoScaleEC2.getInstance().terminateInstances();
					}
					// 2.2 if dynamicDefinedNum > maxInstances then start
					// (dynamicDefinedNum - maxInstances) instances
					else if (dynamicDefinedNum > maxInstances
							&& AutoScaleEC2.getInstance().getTotalInstances() < maximumInstances) // need
																									// to
																									// add:
																									// check
																									// to
																									// is
																									// if
																									// the
																									// upInstances
																									// =
																									// maximumInstances
																									// (5)
					{
						logger.info(
								"creating instances for second time. since there are more jobs and dynamicDefinedNum > maxInstances");
						AutoScaleEC2.getInstance().createInstances(minInstances, dynamicDefinedNum - maxInstances);// add
																													// the
																													// remaining
																													// instances
					}
					// 3. logs a message informaing that the instances are
					// processing the jobs.
					else {
						logger.info("Instances are still processign the jobs");
					}
				}
				// sleep for 6000 milliseconds before starting the while loop
				sleep(6000);
			} catch (Exception ex) {
				java.util.logging.Logger.getLogger(AutoScaleEC2Thread.class.getName()).log(Level.SEVERE, null, ex);
			}
		} // while(true) ends
	}
}