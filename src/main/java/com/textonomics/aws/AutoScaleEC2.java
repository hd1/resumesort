/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.aws;

import com.amazonaws.services.ec2.AmazonEC2AsyncClient;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.textonomics.PlatformProperties;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * AutoScaleEC2 provides a tool for the main server to manage the EC2 instances:
 * 1. Create only one AutoScaleEC2 object. 2. Create the AmazonAsycnClient
 * object. 3. Start new instances. 4. Track the new instances up time. 5. Delete
 * the new instances when the job is done and it's up for specified amount of
 * time (eg. 1 hour or multiples of hour)
 * 
 * @author Preeti Mudda
 */
public class AutoScaleEC2 {
	static Logger logger = Logger.getLogger(AutoScaleEC2.class);
	private static final int maximumInstances = 5;
	private List<String> listOfInsId = new ArrayList<String>(); // List<String>
																// Id of the
																// current up
																// and running
																// EC2
																// instances.
	private List<Instance> listOfIns = new ArrayList<Instance>();// List<Instance>
																	// of the
																	// current
																	// up and
																	// running
																	// EC@
																	// instances.
	private List<String> dummyValueList = new ArrayList<String>(); // dummy list
																	// for
																	// testing
																	// purpose
	private AmazonEC2AsyncClient awsEC2; // AmazonAsyncClient object
	private ResumeSortAWSCredentials awsCred;// ResumeSortAWSCredentials object
	private static AutoScaleEC2 autoScaleEC2Instance = null;// AutoScaleEC2
															// object
	private String imageID;// The AMI id used to create an EC2 instance.

	/**
	 * 2. Init the ImageId, AWSCred, AWSEC2 2.0 Gets AMIID from the proterites
	 * 2.1 Creates ResumeSortAWSCredential object 2.2 Create
	 * AmazonEC2AsyncClient object
	 */
	private AutoScaleEC2() {
		imageID = PlatformProperties.getInstance().getProperty("com.textonomics.AMIId"); // 2.0
		awsCred = new ResumeSortAWSCredentials(); // 2.1
		awsEC2 = new AmazonEC2AsyncClient(awsCred); // 2.2
	}

	/**
	 * 1. Create a new AutoScaleEC2 object
	 * 
	 * @return autoScaleEC2Instance : returns the AutoScaleEC2 object
	 */
	public static synchronized AutoScaleEC2 getInstance() {
		// 1.0 Checks whether instance is null
		// 1.1 Creates a AutoScaleEC2 object.
		// 1.2 returns the AutoScaleEC2 object
		if (autoScaleEC2Instance == null) // 1.0
		{
			autoScaleEC2Instance = new AutoScaleEC2(); // 1.1 Only if the
														// AutoScaleEC2 object
														// is null.
		}
		return autoScaleEC2Instance;// 1.2
	}

	/**
	 * CreateInstance creates instances. 3.0 Create a RunInstanceRequest object
	 * by passing the imageId,min and max instances 3.1 Makes a call to awsEC2
	 * runInstamces method 3.2 Loops through RunInstancesResult to store the
	 * instance in a list
	 * 
	 * @param min
	 *            - minimum number of instances to start
	 * @param max
	 *            - maximum number of instances to start
	 */
	public void createInstances(int min, int max) {
		logger.info("In the AutoScaleEC2. Creating an Instance");
		// 3.0 Create a RunInstanceRequest runInstance object by passing the
		// imageId,min and max instances
		RunInstancesRequest runInstance = new RunInstancesRequest(imageID, min, max);
		// 3.0.1 set the InstanceType to m1.small
		runInstance.setInstanceType("m1.small");
		// dummy loop for testing purpose
		for (int i = 0; i < max; i++) {
			logger.info("In the AutoScaleEC2. adding " + i + "strings " + imageID);
			dummyValueList.add(imageID);
		} // for(int i = 0; i < max; i++)
			// 3.1 Makes a call to awsEC2 runInstamces method passing the
			// runInstance
		if (getTotalInstances() < getMax()) {
			RunInstancesResult runInsResult = awsEC2.runInstances(runInstance);
			int i = 1;
			// 3.2 Loops through RunInstancesResult to store the instance:
			// List<Instance> listOfIns and List<String> listOfInsId
			for (Instance instance : runInsResult.getReservation().getInstances()) {
				System.out.println("Launched On-Demand Instace: " + i + ": " + instance.getInstanceId());
				listOfInsId.add(instance.getInstanceId());
				listOfIns.add(instance);
				i++;
			} // ends - for (Instance instance:
				// runInsResult.getReservation().getInstances())
		}
	}

	/**
	 * 5. Terminates the instances when the upRunningTime is close to specified
	 * upTime. 5.1 Iterates through the listOfIns. 5.1.1 Checks whether the
	 * upRunningTime is close to specified upTime 5.1.2 logs a message informing
	 * that none instances met the specified upTime criteria to terminate.
	 */
	public void terminateInstances() {
		logger.info("In the AutoScaleEC2. Checking the instances up time");
		// 5.0 Get the ierator for the listOfIns
		Iterator iterOfIns = listOfIns.iterator();
		long instanceLaunchtime; // instanceLaunchTime
		long currentTime = System.currentTimeMillis(); // current time
		long upRunningTime; // upRunningTime = currentTime - instanceLaunchTime
		String instanceId;// instanceID
		List<String> removeInstanceIdList = new ArrayList<String>();// List<String>
																	// instanceIdList
																	// used for
																	// terminating
																	// the
																	// instances
		// dummy loop for testing purpose
		for (int i = 0; i < dummyValueList.size(); i++) {
			logger.info("In the AutoScaleEC2. deleting " + i + "strings " + imageID);
			dummyValueList.remove(i);
		} // dummy loop ends
			// 5.1 Iterates through the listOfIns.
		while (iterOfIns.hasNext()) {
			Instance instance = (Instance) iterOfIns.next();// get the next
															// Instance
			instanceLaunchtime = instance.getLaunchTime().getTime();// get the
																	// launch
																	// time of
																	// the
																	// instance
			upRunningTime = currentTime - instanceLaunchtime;// calculate the
																// total
																// upRunningTime
			// GetMetricStatisticsRequest request = new
			// GetMetricStatisticsRequest();
			// GetMetricStatisticsResult result =
			// cloudWatchClient.getMetricStatistics(request);
			// 5.1.1 Checks whether the upRunningTime is close to specified
			// upTime
			if (isInstanceUpForHour(upRunningTime) == true) {
				instanceId = instance.getInstanceId();// get the instanceID
				logger.info("In the AutoScaleEC2. terminating the instance" + instanceId);
				removeInstanceIdList.add(instanceId);// add the instanceId to
														// removeInstanceIdList
				// 5.1.1.1 Create a TerminateInstanceRequest by passing the
				// removeInstanceIdList
				TerminateInstancesRequest terminateInstancesRequest = new TerminateInstancesRequest(
						removeInstanceIdList);
				// 5.1.1.2 send the request to awsEC2 to terminate instances
				awsEC2.terminateInstances(terminateInstancesRequest);
				// 5.1.1.3 clear the removeInstanceIdList
				removeInstanceIdList.clear();
				// 5.1.1.4 remove that instancId from the listOfInsId
				listOfInsId.remove(instanceId);
			}
			// 5.1.2 logs a message informing that none instances met the
			// specified upTime criteria to terminate.
			else
				logger.info(
						"In the AutoScaleEC2. couldnt terminate the instance becuase the instance up time is not close to 1 hour ");
		} // ends - while(iterOfIns.hasNext())
	}

	public int getTotalInstances() {
		return listOfInsId.size();
	}

	/**
	 * isInstanceUp to check whether any instances are up and running
	 * 
	 * @return ture: if instance is up else false
	 */
	public boolean isInstancesUp() {
		boolean isUp = true;
		// //1.0 if the listOfInsId is empty then isUp will be false
		// if(dummyValueList.isEmpty() == true)
		// {
		// // isUp = false;
		// // logger.info("In the AutoScaleEC2. no instances up");
		// }
		// if listOfInsId is empty then no instances are up
		if (listOfInsId.isEmpty() == true) {
			isUp = false;
			logger.info("In the AutoScaleEC2. no instances up");
		}
		return isUp;
	}

	/**
	 * 4.0 Checks whether the instance uprunningtime is in the specified amount
	 * of time. 4.1 Checks if the upRunningTime is between 50 -55 mins or
	 * multiples of these minutes 4.2 Checks if the upRunningTime is less than
	 * 50 minutes or multiples of 50 minutes 4.3 Sets the minTime = minTime * 2
	 * and maxTime = maxTime * 2.
	 * 
	 * @param startTime
	 *            -instance start time in milliseconds
	 * @param currentTime-instance
	 *            current time in milliseconds
	 * @return boolean: true if 4.1 else false
	 */
	public boolean isInstanceUpForHour(long upRunningTime) {
		long minTime = 480000; // 3000000;//50 mins in milliseconds
		long maxTime = 600000;// 3300000; //55 mins in milliseconds
		logger.info("In the isInstanceUp method. checking whether instance is up for an hour");
		// 4.0
		for (int i = 0; i < 10; i++) {
			// 4.1 Checks if the upRunningTime is between 50 -55 mins or
			// multiples of these minutes
			if (upRunningTime <= maxTime && upRunningTime >= minTime) // Total
																		// time
																		// for
																		// the
																		// instance
																		// up is
																		// between
																		// 50mins(*
																		// i) to
																		// 55mins
																		// (* i)
			{
				logger.info("In the isInstanceUp method. instance is up for an hour");
				return true;
			}
			// 4.2 Checks if the upRunningTime is less than 50 minutes or
			// multiples of 50 minutes
			else if (upRunningTime <= minTime) {
				logger.info("In the isInstanceUp method. instance is up for an " + minTime);
				return false;
			}
			// 4.3 Sets the minTime = minTime * 2 and maxTime = maxTime * 2.
			else {
				maxTime = maxTime * 2;
				minTime = minTime * 2;
			}
			logger.info("In the isInstanceUp method. instance is not up for an hour");
		}
		return false;
	}

	private int getMax() {
		return maximumInstances;
	}
}