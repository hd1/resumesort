/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics;

import com.textonomics.nlp.Chunker;
import com.textonomics.nlp.DefaultNLPSuite;
import com.textonomics.nlp.NLPSuite;
import com.textonomics.nlp.POSKeyPhraseIdentifier;
import com.textonomics.nlp.POSTagger;
import com.textonomics.nlp.Sentence;
import com.textonomics.nlp.SentenceSpliter;
import com.textonomics.nlp.Token;
import com.textonomics.wordnet.CompoundPhraseIdentifier;
import com.textonomics.wordnet.DomainKeyPhraseIdentifier;
import com.textonomics.wordnet.WordNetPhraseFilter;
import com.textonomics.wordnet.model.Domain;
import com.textonomics.wordnet.model.POS;
import com.textonomics.wordnet.model.Word;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import com.textonomics.wordnet.morphy.Morphy;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dev Gude
 */
public class ParseResumes {
	static KeyPhraseIdentifierPipe keyTokenIdentifier = new KeyPhraseIdentifierPipe();
	static PhraseFilterPipe sourceTokenFilterer = new PhraseFilterPipe();
	static DomainKeyPhraseIdentifier defaultKeyTokenIdentifier;
	static HashMap<String, PhraseListFilter> tokenListFilterMap;
	private SentenceSpliter sentenceDetector;
	private NLPSuite suite;
	private POSTagger tagger;
	private Chunker chunker;

	public ParseResumes() throws Exception {
		suite = new DefaultNLPSuite();
		sentenceDetector = suite.getSpliter();
		tagger = suite.getTagger();
		chunker = suite.getChunker();
	}

	static Map<String, Integer> verbMap = new HashMap<String, Integer>();
	private static CompoundPhraseIdentifier compoundTokenIdentifier;

	private static POSKeyPhraseIdentifier posKeyTokenIdentifier;
	private static WordNetPhraseFilter wordnetPhraseFilter;

	public static void main(String[] args) {
		String fileName = "/textnomics/TestResumes/zArthurResume2.doc";
		// fileName = "/textnomics/docs/Resumes";
		fileName = "/textnomics/docs/jobpostings/bfo";
		fileName = "/textnomics/docs/542662367-1633Summary.txt";
		if (args == null || args.length < 1 || args[0].trim().length() == 0) {
			System.out.println("Please pass resume directory or file name as a parameter");
		} else
			fileName = args[0];
		PlatformProperties.resourceDirectory = "/Textnomics/ResumeCakeWalk/web/WEB-INF/Resources";
		try {
			new ParseResumes().process(fileName);
		} catch (Exception ex) {
			Logger.getLogger(ParseResumes.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.exit(0);
	}

	public void process(String fileName) {
		File file = new File(fileName);
		File[] files = new File[1];
		if (!file.exists())
			throw new RuntimeException("File/Folder not found:" + fileName);
		if (file.isDirectory()) {
			files = file.listFiles();
		} else
			files[0] = file;

		try {
			WordNetDataProviderPool.getInstance().acquireProvider();

			defaultKeyTokenIdentifier = new DomainKeyPhraseIdentifier(Domain.getDomainByName("Generic"));
			compoundTokenIdentifier = new CompoundPhraseIdentifier();
			posKeyTokenIdentifier = new POSKeyPhraseIdentifier();
			wordnetPhraseFilter = new WordNetPhraseFilter();

			tokenListFilterMap = new HashMap<String, PhraseListFilter>();
			tokenListFilterMap.put("Default 100",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt"), 100));
			tokenListFilterMap.put("Default 300",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt"), 300));
			tokenListFilterMap.put("Default 600",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt"), 600));
			tokenListFilterMap.put("Default All",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt")));

			processFiles(file.getName(), files);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			WordNetDataProviderPool.getInstance().releaseProvider();// Always
																	// release
																	// the
																	// provider
		}
	}

	void processFiles(String fileName, File[] files) {
		PrintWriter fout = null;
		PrintWriter errout = null;
		try {
			fout = new PrintWriter(new FileWriter("/textnomics/docs/verbPrep_" + fileName + ".txt"));
			errout = new PrintWriter(new FileWriter("/textnomics/docs/errors_" + fileName + ".txt"));
			Map<String, Integer> verbPrepositions = new HashMap<String, Integer>();
			Map<String, TreeMap<String, Integer>> vpNounsBetween = new HashMap<String, TreeMap<String, Integer>>();
			Map<String, TreeMap<String, Integer>> vpNounsAfter = new HashMap<String, TreeMap<String, Integer>>();
			Morphy morphyInstance = Morphy.getInstance();
			int num = 0;
			for (File file : files) {
				System.out.println("Processing file " + num++ + " : " + file.getName());
				// Adding filters
				keyTokenIdentifier.addKeyTermIdentifer(defaultKeyTokenIdentifier);
				keyTokenIdentifier.addKeyTermIdentifer(posKeyTokenIdentifier);
				keyTokenIdentifier.addKeyTermIdentifer(compoundTokenIdentifier);
				sourceTokenFilterer.addTokenFilter(tokenListFilterMap.get("Default 300")); // Checks
																							// if
																							// the
																							// word
																							// is
																							// in
																							// the
																							// top
																							// 300
																							// words
																							// list.
				// sourceTokenFilterer.addTokenFilter(new StyleFilter());
				sourceTokenFilterer.addTokenFilter(new NumericFilter());
				// sourceTokenFilterer.addTokenFilter(wordnetPhraseFilter);
				// DocumentProcessorRS docProc = new DocumentProcessorRS(new
				// DefaultNLPSuite(), keyTokenIdentifier, sourceTokenFilterer,
				// 8);
				// DocumentRS resume = docProc.process(file, false, true);
				ArrayList<Sentence> nonLemmaSentences; // contains
														// non-lemmatized
														// sentences
				try {
					nonLemmaSentences = sentenceDetector.split(file); // get non
																		// lemmatized
																		// sentences
																		// from
																		// the
																		// resume
				} catch (Exception e) {
					e.printStackTrace();
					errout.println("Error processing:" + file.getName() + ":" + e.getMessage());
					continue;
				}
				// docProc.setSentences(nonLemmaSentences);

				ArrayList<Sentence> lemmaSentences = new ArrayList<Sentence>();

				for (Sentence nls : nonLemmaSentences) // lemmatize each
														// sentence
				{
					Sentence lemmaSentence = new Sentence();
					List<Token> tokenList = nls.getTokens(); // List of "Token"s
					String[] tokens = nls.getTokenBuffers(); // Contains token
																// buffers, not
																// tokens itself
					String[] tags = tagger.tag(tokens); // POS Tagging
					List<Token> lemmaTokenList = new ArrayList<Token>();

					for (int tokenIndex = 0; tokenIndex < tokens.length; tokenIndex++) // lemmatize
																						// each
																						// token
																						// of
																						// the
																						// sentence
																						// from
																						// the
																						// resume
					{
						Set<Word> lemmas = new HashSet<Word>();
						if (tags[tokenIndex].startsWith("VB") || tags[tokenIndex].startsWith("NN")) {
							if (tags[tokenIndex].startsWith("VB") && !tags[tokenIndex].equalsIgnoreCase("VB")) {
								lemmas = morphyInstance.getStems(tokens[tokenIndex], POS.VERB);
								if (!lemmas.isEmpty()
										&& !lemmas.iterator().next().toString().trim().equalsIgnoreCase("")) {
									tokens[tokenIndex] = lemmas.iterator().next().toString().toLowerCase(); // Change
																											// the
																											// token
																											// buffer
																											// to
																											// lemmatized
																											// form
								}
							}
							if (tags[tokenIndex].equalsIgnoreCase("NNS") || tags[tokenIndex].equalsIgnoreCase("NNPS")) {
								lemmas = morphyInstance.getStems(tokens[tokenIndex], POS.NOUN);
								if (!lemmas.isEmpty()
										&& !lemmas.iterator().next().toString().trim().equalsIgnoreCase("")) {
									tokens[tokenIndex] = lemmas.iterator().next().toString(); // Change
																								// the
																								// token
																								// buffer
																								// to
																								// lemmatized
																								// form
								}
							}
						}

						Token lemmaToken = new Token(tokens[tokenIndex] + " ", tokenList.get(tokenIndex).getPosition(),
								null);
						lemmaToken.setPos(tags[tokenIndex]);
						lemmaSentence.add(lemmaToken); // Add the token to the
														// lemmatized sentence
						lemmaTokenList.add(lemmaToken);
					}
					lemmaSentences.add(lemmaSentence); // Add the currently
														// lemmatized sentence
														// to the list of
														// lemmatized sentences.

					for (int i = 0; i < lemmaTokenList.size(); i++) {
						Token token = lemmaTokenList.get(i);
						if (token.isVerb() && token.getBuffer().trim().length() > 1) {
							List<Token> verbPrep = new ArrayList<Token>();
							List<Token> nounsBet = new ArrayList<Token>();
							List<Token> nounsAft = new ArrayList<Token>();
							boolean prepfound = false;
							for (int j = i + 1; j < lemmaTokenList.size(); j++) {
								Token token2 = lemmaTokenList.get(j);
								if (token2.isVerb() || !Character.isLetter(token2.getBuffer().charAt(0)))
									break;
								// Skip prepositions
								if (!token2.isPreposition() && !token2.getPos().equals("TO")) {
									// if (token2.isNoun() || token2.isAdVerb())
									// // allow only nouns or adverbs
									// if (!token2.isAdVerb() &&
									// !token2.isAdjective()) // skip adverbs
									// and adjectives
									// if (!token2.isAdjective()) // Allow
									// everything except adjectives
									if (token2.isNoun()) // Allow only nouns
									{
										if (verbPrep.isEmpty())
											nounsBet.add(token2);
										else
											nounsAft.add(token2);
									}
								} else if (token2.isPreposition() && !prepfound) {
									prepfound = true;
									verbPrep.add(token);
									verbPrep.add(token2);
								} else if ((token2.isPreposition() || token2.getPos().equals("TO")) && prepfound)
									break;
							}
							if (nounsBet.size() > 0 && nounsBet.get(nounsBet.size() - 1).isConjunction())
								nounsBet.remove(nounsBet.size() - 1);
							if (nounsAft.size() > 0 && nounsAft.get(nounsAft.size() - 1).isConjunction())
								nounsAft.remove(nounsAft.size() - 1);
							if (verbPrep.size() > 1 && (nounsBet.size() > 0 || nounsAft.size() > 0)) {
								String vp = verbPrep.get(0).getBuffer() + verbPrep.get(1).getBuffer();
								vp = vp.trim();
								Integer count = verbPrepositions.get(vp.trim());
								if (count == null)
									count = 1;
								else
									count = 1 + count;
								verbPrepositions.put(vp, count);
								if (nounsBet.size() > 0) {
									TreeMap<String, Integer> nounsBetween = vpNounsBetween.get(vp);
									if (nounsBetween == null)
										nounsBetween = new TreeMap<String, Integer>();
									String nounPhrase = "";
									for (Token ntk : nounsBet)
										nounPhrase = nounPhrase + ntk.getBuffer();
									count = nounsBetween.get(nounPhrase.trim());
									if (count == null)
										count = 1;
									else
										count = 1 + count;
									nounsBetween.put(nounPhrase.trim(), count);
									vpNounsBetween.put(vp, nounsBetween);
								}
								if (nounsAft.size() > 0) {
									TreeMap<String, Integer> nounsAfter = vpNounsAfter.get(vp);
									if (nounsAfter == null)
										nounsAfter = new TreeMap<String, Integer>();
									String nounPhrase = "";
									for (Token ntk : nounsAft)
										nounPhrase = nounPhrase + ntk.getBuffer();
									count = nounsAfter.get(nounPhrase.trim());
									if (count == null)
										count = 1;
									else
										count = 1 + count;
									nounsAfter.put(nounPhrase.trim(), count);
									vpNounsAfter.put(vp, nounsAfter);
								}
							}
						}
					}
				}

			}
			com.textonomics.web.FileUtils.serializeOject(verbPrepositions,
					new File("/textnomics/docs/verbPrep_" + fileName + ".Object"));

			// Sort by count
			DecimalFormat df = new DecimalFormat("00000");
			TreeMap<String, Integer> verbPrepositionsSort = new TreeMap<String, Integer>();
			// Map<String,Map<String, Integer>> vpNounsBetweenSort = new
			// HashMap<String, Map<String, Integer>>();
			// Map<String,Map<String, Integer>> vpNounsAfterSort = new
			// HashMap<String, Map<String, Integer>>();

			for (String verbPrep : verbPrepositions.keySet()) {
				Integer count1 = verbPrepositions.get(verbPrep);
				verbPrepositionsSort.put(df.format(count1) + "_" + verbPrep, count1);
				Map<String, Integer> vpNounsBetweenMap = vpNounsBetween.get(verbPrep);
				if (vpNounsBetweenMap != null) {
					TreeMap<String, Integer> vpVounsBetweenMapSort = new TreeMap<String, Integer>();
					for (String nouns : vpNounsBetweenMap.keySet()) {
						Integer count = vpNounsBetweenMap.get(nouns);
						vpVounsBetweenMapSort.put(df.format(count) + "_" + nouns, count);
					}
					vpNounsBetween.remove(verbPrep);
					vpNounsBetween.put(df.format(count1) + "_" + verbPrep, vpVounsBetweenMapSort);
				}
				Map<String, Integer> vpNounsAfterMap = vpNounsAfter.get(verbPrep);
				if (vpNounsAfterMap != null) {
					TreeMap<String, Integer> vpNounsAfterMapSort = new TreeMap<String, Integer>();
					for (String nouns : vpNounsAfterMap.keySet()) {
						Integer count = vpNounsAfterMap.get(nouns);
						vpNounsAfterMapSort.put(df.format(count) + "_" + nouns, count);
					}
					vpNounsAfter.remove(verbPrep);
					vpNounsAfter.put(df.format(count1) + "_" + verbPrep, vpNounsAfterMapSort);
				}
			}

			int k = 0;
			for (String verbPrep : verbPrepositionsSort.descendingKeySet()) {
				k++;
				System.out.println("" + k + ". Verb/Prep:" + verbPrep + " Count:" + verbPrepositionsSort.get(verbPrep));
				if (vpNounsBetween.get(verbPrep) != null)
					System.out.println("\t\tNouns Between:" + vpNounsBetween.get(verbPrep));
				if (vpNounsAfter.get(verbPrep) != null)
					System.out.println("\t\tNouns After:" + vpNounsAfter.get(verbPrep));
				String line = verbPrep.substring(verbPrep.indexOf("_") + 1) + "=>" + verbPrepositionsSort.get(verbPrep)
						+ "\t";
				if (vpNounsBetween.get(verbPrep) != null) {
					String delim = "(";
					for (String noun : vpNounsBetween.get(verbPrep).descendingKeySet()) {
						line = line + delim + noun.substring(verbPrep.indexOf("_") + 1) + "=>"
								+ vpNounsBetween.get(verbPrep).get(noun);
						delim = ",";
					}
					line = line + ")";
				} else
					line = line + "()";
				line = line + "\t";
				if (vpNounsAfter.get(verbPrep) != null) {
					String delim = "(";
					for (String noun : vpNounsAfter.get(verbPrep).descendingKeySet()) {
						line = line + delim + noun.substring(verbPrep.indexOf("_") + 1) + "=>"
								+ vpNounsAfter.get(verbPrep).get(noun);
						delim = ",";
					}
					line = line + ")";
				} else
					line = line + "()";
				fout.println(line);
			}
			fout.close();
			errout.close();
		} catch (Exception ex) {
			Logger.getLogger(ParseResumes.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	static public void lemmatize(String fileName) {
		WordNetDataProviderPool.getInstance().acquireProvider();
		fileName = "/textnomics/docs/NotInWN_wikiRedirects.txt";
		PrintWriter fout = null;
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
			StringBuffer stringBuffer = new StringBuffer();
			fout = new PrintWriter(new FileWriter("/textnomics/docs/lemma_NotInWN_wikiRedirects.txt"));
			String line;
			int code = 10000;

			int linenum = 0;
			int rownum = 0;
			String topic = "";
			while ((line = bufferedReader.readLine()) != null) {
				linenum++;
				// if (linenum > 20) break;
				if (linenum % 100 == 0)
					System.out.println("Processing line:" + linenum);
				if (false && line.indexOf('\t') == -1) {
					rownum = 0;
					continue;
				}
				rownum++;

				String[] columns = line.split("\t");
				String wikititle = columns[0];
				String[] redirects = columns[1].split(",");
				String[] words = wikititle.split(" ");
				String[] lemmaWords = new String[words.length];
				for (int i = 0; i < words.length; i++) {
					lemmaWords[i] = words[i];
					Set<Word> lemmas = Morphy.getInstance().getStems(words[i], POS.NOUN);
					if (!lemmas.isEmpty())
						lemmaWords[i] = lemmas.iterator().next().toString();
					else {

					}
				}
				String lemmaTitle = "";
				for (int i = 0; i < lemmaWords.length; i++)
					lemmaTitle = lemmaTitle + " " + lemmaWords[i];
				lemmaTitle = lemmaTitle.substring(1);

				String[] lemmaRedirects = new String[redirects.length];
				for (int i = 0; i < redirects.length; i++) {
					words = redirects[i].trim().split(" ");
					lemmaWords = new String[words.length];
					for (int j = 0; j < words.length; j++) {
						lemmaWords[j] = words[j];
						Set<Word> lemmas = Morphy.getInstance().getStems(words[j], POS.NOUN);
						if (!lemmas.isEmpty())
							lemmaWords[j] = lemmas.iterator().next().toString();
					}
					lemmaRedirects[i] = "";
					for (int j = 0; j < lemmaWords.length; j++)
						lemmaRedirects[i] = lemmaRedirects[i] + " " + lemmaWords[j];
					lemmaRedirects[i] = lemmaRedirects[i].substring(1);
				}
				StringBuffer outBuf = new StringBuffer(lemmaTitle);
				String delim = "\t";
				for (String lemmaRedirect : lemmaRedirects) {
					outBuf.append(delim + lemmaRedirect);
					delim = ", ";
				}
				fout.println(outBuf.toString());
			}
			fout.close();
		} catch (Exception x) {
			x.printStackTrace();
		}
	}

}
