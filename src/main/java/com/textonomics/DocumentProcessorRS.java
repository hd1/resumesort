package com.textonomics;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.textonomics.nlp.Chunker;
import com.textonomics.nlp.NGram;
import com.textonomics.nlp.NLPSuite;
import com.textonomics.nlp.POSTagger;
import com.textonomics.nlp.Sentence;
import com.textonomics.nlp.SentenceSpliter;
import com.textonomics.nlp.Token;
import com.textonomics.htmlparser.HtmlSentenceDetector;
import com.textonomics.nlp.PatternExtractor;
import com.textonomics.openoffice.OOSentenceDetector;
import com.textonomics.tika.TikaSentenceDetector;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 * Copyright 2011 Textonomics. Inc. <br>
 * <br>
 *
 * A class that is used to process a given character streams and return a
 * {@link Document} instance. The processor can be configured to use a
 * {@link PhraseFilter} or a {@link KeyPhraseIdentifier} in order to guide the
 * tokenization and remove the irrelevant {@link Phrase}. The processor uses
 * another parameter that limits the size of {@link NGram} to be analyzed. This
 * class depends on the OpenNlp package (http://opennlp.sourceforge.net/).
 *
 *
 * @author Vinod <br>
 *
 */
public class DocumentProcessorRS {

	/**
	 * A constant indicating the number of characters to ignore before actually
	 * starting to process the document and extract phrases. This usually only
	 * makes sense for resumes where you want to avoid the address information,
	 * etc.
	 */
	private static final int HEADER_CHARS_TO_IGNORE = 250; // TODO EBB -- I
															// think this
															// applies only to
															// the resume and
															// not the job Ad.
	/**
	 * The maximum size of a ngram to consider. Most sentences won't have more
	 * then 20 tokens so this number should be more then enough.
	 */
	private static final int DEFAULT_NGAM_SIZE = 20; // EBB was 20 but
														// appropraite Wordnet
														// phrases are around 6
														// words long
	/**
	 * An object used to split the document into sentences.
	 */
	private final SentenceSpliter sentenceDetector;
	/**
	 * Part of speech tagger used to tag the words in a sentence
	 */
	private final POSTagger tagger;
	/**
	 * A phrase chunker that splits a sentence (after being POS tagged) into
	 * noun phrases and verb phrases.
	 */
	private final Chunker chunker;
	/**
	 * The maximum size of {@link NGram} to consider
	 */
	private final int maxNgramSize;
	/**
	 * An identifier has mostly the same function as a chunker. But instead of
	 * using statistical models based on the POS tags it simply use direct table
	 * look up or a set of heuristics. Its meant to complement the work of the
	 * chunker
	 *
	 */
	private final KeyPhraseIdentifier keyTermTokenizer;
	/**
	 * A Filter is used to ignore certain phrases or words from being considered
	 * for suggestion purposes.
	 */
	private final PhraseFilter tokenFilter;

	/**
	 * Methods for selecting phrases to processing in document -- Dev
	 */
	public enum PROCESSING_METHOD {

		OPENNLP_NONOVERLAPPING_PHRASES, // Default method
		OPENNLP_OVERLAPPING_PHRASES, // Selection of all combinations of words
		OPENNLP_CHUNKEDPHRASES, // Selecting Beginning to End of phrases as
								// chnked by OpenNLP parser
		STANFORDPARSER_PHRASES, // Selecting phrases as chunked by Stanford
								// parser
	}

	private PROCESSING_METHOD processingMethod;

	// VINOD: The Document object
	DocumentRS document = new DocumentRS();;

	static Logger logger = Logger.getLogger(DocumentProcessorRS.class);

	/**
	 * Constructor that accepts a NLP suite
	 *
	 * @param suite
	 *            The NLP suite containing the default processors (POS taggers,
	 *            Sentence Splitters, tokenizer, chunker etc) to be used in this
	 *            processor.
	 *
	 */
	public DocumentProcessorRS(NLPSuite suite) {
		this.sentenceDetector = suite.getSpliter();
		this.tagger = suite.getTagger();
		this.chunker = suite.getChunker();
		this.keyTermTokenizer = null;
		this.tokenFilter = null;
		this.maxNgramSize = DEFAULT_NGAM_SIZE;
		this.processingMethod = PROCESSING_METHOD.OPENNLP_NONOVERLAPPING_PHRASES;
	}

	/**
	 * Constructor that accepts a NLP Suite as above, but also receives a
	 * Filterer and a Identifier to help extract the relevant phrases from
	 * document.
	 *
	 * @param suite
	 *            Core NLP processing modules
	 * @param keyTermTokenizer
	 *            Tokenizer used extract useful phrases using a look up list or
	 *            heuristics
	 * @param tokenFilter
	 *            Used to ignore unwanted words or phrases
	 * @param ngramSize
	 *            Maximum ngram size to use
	 * @param processingMethod
	 *            processing method opennlp/stanford parser etc
	 */
	public DocumentProcessorRS(NLPSuite suite, KeyPhraseIdentifier keyTermTokenizer, PhraseFilter tokenFilter,
			int ngramSize, PROCESSING_METHOD processingMethod) {
		this.sentenceDetector = suite.getSpliter();
		this.tagger = suite.getTagger();
		this.chunker = suite.getChunker();
		this.keyTermTokenizer = keyTermTokenizer;
		this.tokenFilter = tokenFilter;
		this.maxNgramSize = ngramSize;
		this.processingMethod = processingMethod;
	}

	/*
	 * Constructor without processing method
	 */
	public DocumentProcessorRS(NLPSuite suite, KeyPhraseIdentifier keyTermTokenizer, PhraseFilter tokenFilter,
			int ngramSize) {
		this.sentenceDetector = suite.getSpliter();
		this.tagger = suite.getTagger();
		this.chunker = suite.getChunker();
		this.keyTermTokenizer = keyTermTokenizer;
		this.tokenFilter = tokenFilter;
		this.maxNgramSize = ngramSize;
		this.processingMethod = PROCESSING_METHOD.OPENNLP_NONOVERLAPPING_PHRASES; // Dev
	}

	/**
	 * The core method of this class that analyzes the given file by splitting
	 * it into sentences, tokenizing, POS Tagging and chunking. If a filter
	 * and/or tokenizer are given then these are also used in this method.
	 *
	 * @param textDocument
	 *            The file to process
	 * @param ignoreHeader
	 *            Flag indicating if the header (defined by
	 *            HEADER_CHARS_TO_IGNORE) is to be ignored
	 * @param clean
	 *            Flag indicating if the phrases are to be cleaned; certain
	 *            strings should be trimmed off phrases
	 * @return Document consisting of the extracted phrases
	 */

	public DocumentRS process(File textDocument, boolean ignoreHeader, boolean clean) {
		logger.debug("DocumentProcessor: " + textDocument.getName() + " using " + processingMethod + " for parsing");
		Phrase phrase = null;
		ArrayList<Sentence> sentences = null;
		List<Token> tokenList;
		String[] tokens;
		String[] tags;
		String[] chunks;
		int tokenIndex, sentenceIndex, nGramIndex;
		ArrayList<NGram> nGrams;
		NGram ngram = new NGram();
		Sentence sentence;
		int charCount = 0;

		// 1. Extract sentences... if number of sentences less than 5
		// there may be a table so convert to HTML and then detect sentences
		// 2. Process Each Sentence
		// 2.1 tokenize and pos tag and chunk the sentences
		// 3. create the ArrayList of ngrams (from 1 to token to n-tokens)
		try {
			// EBB for the try catch block...
			// if the number of sentences is less than 10, ACCORDING TO VINOD
			// it assumes that document contains tables, it then converts
			// the document to HTML and then detects the sentences.
			try {
				logger.debug(sentenceDetector.getClass().getSimpleName() + ":splitting sentences for document:"
						+ textDocument.getAbsolutePath());
				if (sentenceDetector instanceof TikaSentenceDetector)
					document.setSentenceSplitterType(document.TIKA_SENTENCE_SPLITTER);
				else if (sentenceDetector instanceof OOSentenceDetector)
					document.setSentenceSplitterType(document.OOFFICE_SENTENCE_SPLITTER);
				if (sentenceDetector instanceof HtmlSentenceDetector)
					document.setSentenceSplitterType(document.HTML_SENTENCE_SPLITTER);
				sentences = sentenceDetector.split(textDocument);// sentences
																	// are split
				// If open office does not find many sentences, it is probably
				// because the doc contains tables
				// TODO EBB the way it is processed is
				// OPENNLP_NONOVERLAPPING_PHRASES that may not be a good thing.
				if (sentences == null || sentences.size() < 2) {
					throw new Exception("Only " + sentences.size() + " found"); // this
																				// error
																				// will
																				// trigger
																				// html
																				// parser
																				// usage
				}
			}
			// EBB this makes no sense. in the if above "if
			// (textDocument.getName().indexOf(".htm") == -1 &&
			// processingMethod... " we know that file does not have a .htm in
			// its name. So why do we check again below to see if it is an html
			// file.
			catch (Exception x) {
				if (textDocument.getName().endsWith(".html")) {
					logger.debug("Using html sentence detector");
					sentences = new HtmlSentenceDetector().split(textDocument); // try
																				// splitting
																				// with
																				// html
																				// parser
				} else {
					logger.debug("Error splitting sentences for document:" + textDocument.getName() + ":" + x);
					throw x;
				}
			}

			document.setSentences(sentences);
			if (true)
				printSentences(textDocument, sentenceDetector, sentences);
			// 2. Process Each Sentence
			// 2.1 tokenize and pos tag and chunk the sentences
			Set<String> patternPhrases = getPhrasesFromPatterns(document);
			logger.debug("Pattern phrases:" + patternPhrases);
			String email = null;
			String address = null;
			String name = null;
			String zipcode = null;
			for (sentenceIndex = 0; sentenceIndex < sentences.size(); sentenceIndex++) {
				sentence = sentences.get(sentenceIndex);
				// System.out.println("Sentence "+sentenceIndex+":"+sentence);
				if (email == null && sentence.containsEmail()) {
					tokens = sentences.get(sentenceIndex).getTokenBuffers();
					for (int i = 0; i < tokens.length; i++) {
						String token = tokens[i];
						if (token.indexOf("@") != -1 && i > 0) {
							email = token;
							if (email.length() == 1) {
								email = tokens[i - 1];
								if (tokens.length > i)
									email = email + "@" + tokens[i + 1];
								if (tokens.length > i + 2 && tokens[i + 2].equals("."))
									email = email + "." + tokens[i + 3];
							}
							document.setEmail(email);
							break;
						}
					}
				} else if (name == null && !sentence.containsEmail()) {
					name = sentences.get(sentenceIndex).toString();
					document.setName(name);
				}
				// try extract zipcode only, instead of address
				if (zipcode == null && sentenceIndex < 10) {
					tokens = sentences.get(sentenceIndex).getTokenBuffers();
					for (String token : tokens) {
						if (token.length() == 5 && getInt(token) != 0) {
							zipcode = token;
							document.setAddress(zipcode); // for now
							document.setZipcode(zipcode);
						}
					}
				}

				if (ignoreHeader) // TODO EB BUG the header is for the resume
									// only and it is 40 characters and then
									// within that 40 characters looks for the
									// email address... It has to be a lot more
									// clever than that. Where it looks for the
									// email address and sees if it is part of
									// the header... may be looking at the
									// formating/name/...
				{
					charCount += sentence.toString().length();

					if (sentence.containsEmail() || charCount > HEADER_CHARS_TO_IGNORE) // TODO
																						// as
																						// soon
																						// as
																						// it
																						// sees
																						// the
																						// email,
																						// ignores
																						// that
																						// sentence
																						// and
																						// then
																						// stops
																						// ignoring
																						// the
																						// rest.
																						// But
																						// the
																						// email
																						// better
																						// appear
																						// within
																						// those
																						// HEADER_CHARS_TO_IGNORE
																						// (i.e.,
																						// 40)
																						// characters.
					{
						ignoreHeader = false;
					}
					continue;
				}

				// 2.1 tokenize and pos tag and chunk the sentences
				tokenList = sentence.getTokens(); // Tokenization
				tokens = sentence.getTokenBuffers();
				tags = tagger.tag(tokens); // POS Tagging
				chunks = chunker.chunk(tokens, tags); // Chunking

				if (processingMethod == null || processingMethod == PROCESSING_METHOD.OPENNLP_NONOVERLAPPING_PHRASES
						|| processingMethod == PROCESSING_METHOD.OPENNLP_OVERLAPPING_PHRASES) {
					for (tokenIndex = 0; tokenIndex < tokens.length; tokenIndex++) {
						// 3. create the ArrayList of ngrams (from 1 to token to
						// n-tokens)
						nGrams = getNGrams(tokenList, tags, chunks, tokenIndex);// generate
																				// all
																				// ngrams
																				// starting
																				// at
																				// tokenindex;
																				// //
																				// EBB
																				// Changed
																				// it
																				// to
																				// reflect
																				// the
																				// ','
																				// and
																				// split
																				// around
																				// commas

						for (nGramIndex = nGrams.size() - 1; nGramIndex >= 0; nGramIndex--) // EB
																							// process
																							// longer
																							// ngrams
																							// first
						{
							ngram = nGrams.get(nGramIndex);

							Token nGramLastToken = ngram.getLast();
							Token nGramFirstToken = ngram.getFirst();
							// System.out.println("First token:" +
							// nGramFirstToken.getBuffer() + " Pos:"
							// +nGramFirstToken.getPos());
							// Ignore ngrams whose first tokens are starting
							// with conjunction/preposition/symbols etc.
							// This is no longer required as we are creating
							// ngrams starting with only "NN" and "VB"
							/*
							 * if ( nGramFirstTokenPOS.equalsIgnoreCase("CC") ||
							 * nGramFirstTokenPOS.equalsIgnoreCase("IN") ||
							 * nGramFirstTokenPOS.startsWith("JJ") ||
							 * nGramFirstTokenPOS.startsWith("RB") ||
							 * nGramFirstTokenPOS.startsWith("SYM")||
							 * nGramFirstTokenPOS.startsWith("PR") ||
							 * nGramFirstTokenPOS.startsWith(".")||
							 * nGramFirstTokenPOS.startsWith("*") ||
							 * nGramFirstTokenPOS.startsWith(":") ||
							 * nGramFirstTokenPOS.startsWith("''") ||
							 * nGramFirstTokenPOS.startsWith("``"))
							 * 
							 * { System.out.
							 * println("***This code never executes***");
							 * continue; }
							 */

							while (tokenFilter != null) {
								if (ngram.size() != 0) {
									nGramLastToken = ngram.getLast();
								} else
									break; // Break if NGram is empty

								// If the NGram ends with an unwanted token,
								// remove it.
								if (tokenFilter.ignore(new Phrase(nGramLastToken.getBuffer(), ngram, sentenceIndex)))
								/*
								 * || nGramLastToken.getPos().equalsIgnoreCase(
								 * "CC") ||
								 * nGramLastToken.getBuffer().startsWith(".") ||
								 * nGramLastToken.getPos().startsWith("SYM") ||
								 * nGramLastToken.getPos().startsWith(":"))
								 */
								{
									ngram.removeLastElement(); // Remove the
																// last token in
																// the NGram
									continue;
								}
								break;
							}

							if (ngram.size() != 0) {
								phrase = new Phrase(getPhraseBuffer(sentence, ngram), ngram, sentenceIndex);
								// Comment out for now, does not give good
								// phrases
								// for (String patternPhrase: patternPhrases)
								// {
								// if
								// (patternPhrase.contains(phrase.getBuffer()))
								// {
								// document.addDocumentPhrase(phrase);
								// break;
								// }
								// }
								if (ngram.size() > 1 || ngram.getFirst().getPos().startsWith("NNP")) // If
																										// the
																										// ngram
																										// has
																										// only
																										// one
																										// token,
																										// then
																										// it
																										// should
																										// be
																										// a
																										// proper
																										// noun
																										// for
																										// it
																										// to
																										// become
																										// a
																										// phrase.
								{
									if (clean) {
										Token firstToken = phrase.getNgram().getFirst();

										// Avoid Phrases containing unnecessary
										// symbols
										if (phrase.getBuffer().trim().equals("")
												|| phrase.getBuffer().trim().length() < 3
												|| phrase.getBuffer().contains(";") || phrase.getBuffer().contains(".")
												|| phrase.getBuffer().contains("”")
												|| phrase.getBuffer().contains("\u00A0")
												|| phrase.getBuffer().contains("’")) {
											break;
										}

										// Ignore the phrase if it is starting
										// with any of the 300 Words in the list
										// and process the next ngram.
										if (tokenFilter != null
												&& tokenFilter.ignore(
														new Phrase(firstToken.getBuffer(), ngram, sentenceIndex))
												|| firstToken.getBuffer().trim().length() < 3) {
											break;
										}
									}
									// if (phrase.getBuffer().contains("search
									// box"))
									// System.out.println("Adding Phrase 0:"
									// +phrase.getBuffer());
									document.addDocumentPhrase(phrase); // TODO
																		// EBB
																		// BUG...
																		// checkout
																		// document.java.
																		// There
																		// may
																		// be a
																		// bug
																		// here!

									break;
								}
							}
							phrase = null;
						}

						if (phrase != null) // EB now that the phrase within the
											// NGram has been identified (may
											// be?!) as the keyphrase, go
											// forwrad int the sentence the
											// lenght of that NGram -- i.e.,
											// nGramIndex
						{

							if (processingMethod == PROCESSING_METHOD.OPENNLP_NONOVERLAPPING_PHRASES) {
								tokenIndex += ngram.size(); // DG - use for
															// target
							} else
								continue;
						}
					}

					// VINOD 1. Check for tokens that are tagged 'NNP' and get
					// the list of ngrams starting from that token.
					// 2. If the ngram contains consecutive NNP's, move forward
					// in the ngram list. If the ngram does not contain
					// consecutive NNP's, go back to the previous ngram (i.e.,
					// index-1)
					// 3. Construct a phrase from the tokens in the last ngram
					// which has consecutive NNP's and add it to the document.
					// System.out.println("Processing sentence:" + sentenceIndex
					// + ": " + sentence.toString());
					for (int i = 0; i < tokens.length; i++) {
						if (tokens[i].startsWith("$"))
							continue;
						// Proper nouns or universities
						if (tags[i].startsWith("NNP") || tokens[i].trim().equalsIgnoreCase("University")
								|| tokens[i].trim().equalsIgnoreCase("Institute")
								|| tokens[i].trim().equalsIgnoreCase("College") || isUSState(tokens[i].trim())
								|| (i == 0 && (tokens.length - i) > 2 && tags[i].startsWith("NN")
										&& tags[i + 1].startsWith("NNP"))) // becuase
																			// first
																			// word
																			// in
																			// a
																			// sentence
																			// always
																			// consided
																			// NN
																			// instead
																			// of
																			// NNP
						{
							// System.out.println("Processing token:" +
							// sentenceIndex + "_" + i + " : " +
							// tokens[i]+",tag:" + tags[i]);
							ArrayList<NGram> ngramsList = getNGrams(tokenList, tags, chunks, i);

							if (!ngramsList.isEmpty()) {
								for (int index = 0; index < ngramsList.size(); index++) // Process
																						// ngrams
																						// starting
																						// from
																						// smaller
																						// to
																						// larger.
								{
									// //Debug
									// String temp =
									// ngramsList.get(index).toString();
									// if
									// (tokens[i].equalsIgnoreCase("University")
									// ||
									// tokens[i].equalsIgnoreCase("Cambridge")
									// || temp.indexOf("Taco") != -1)
									// {
									// System.out.println("----" + index + "
									// total:" + ngramsList.size() + " lastpos:"
									// +
									// ngramsList.get(index).getLast().getPos()
									// + " last:" +
									// ngramsList.get(index).getLast().getBuffer()
									// + "
									// char:"+Integer.valueOf(ngramsList.get(index).getLast().getBuffer().trim().charAt(0)));
									// int k = 0;
									// for (NGram ng: ngramsList)
									// System.out.println("ngram " + k++ + ":" +
									// ng.toString());
									// }
									// if
									// (tokens[i].equalsIgnoreCase("Cambridge"))
									// System.out.println("Last:'" +
									// ngramsList.get(index).getLast().getBuffer().trim()
									// + "'");
									if (index < ngramsList.size() - 2
											&& ngramsList.get(index).getLast().getBuffer().trim().equals("A")
											&& ngramsList.get(index + 1).getLast().getBuffer().trim().equals("&")
											&& ngramsList.get(index + 2).getLast().getBuffer().trim().equals("M")) // for
																													// Texas
																													// A&M
									{
										index = index + 2;
									}
									// If the last token is NNP, check for next
									// largest ngram.
									// Remove last token if it contains 0xA0
									// (Non-breaking space - &nbsp;)
									if ((ngramsList.get(index).getLast().getPos().startsWith("NNP")
											|| ngramsList.get(index).getLast().getBuffer().trim().equals("of")
											|| ngramsList.get(index).getLast().getBuffer().trim()
													.equalsIgnoreCase("University")
											|| ngramsList.get(index).getLast().getBuffer().trim()
													.equalsIgnoreCase("Institute")
											|| ngramsList.get(index).getLast().getBuffer().trim()
													.equalsIgnoreCase("College")
											|| isUSState(ngramsList.get(index).getLast().getBuffer().trim())
											|| (index == 0 && ngramsList.size() > 0)
									// ||
									// ngramsList.get(index).getLast().getBuffer().trim().equals("&")
									// // for Texas A&M
									) && ngramsList.get(index).getLast().getBuffer().trim().charAt(0) != 0xA0
											&& index < ngramsList.size() - 1) // 0xA0
																				// is
																				// a
																				// non-breaking
																				// space
										continue;

									if (index > 0 && !ngramsList.get(index).getLast().getPos().startsWith("NN"))
										ngram = ngramsList.get(index - 1);
									else
										ngram = ngramsList.get(index); // Added
																		// by
																		// Dev
																		// -- is
																		// this
																		// correct???

									StringBuilder phraseBuffer = new StringBuilder();
									List<Token> tokenlist = ngram.getElements();
									// Remove last token if it contains 0xA0
									// (Non-breaking space - &nbsp;)
									int k = 0;
									for (Token t : tokenlist) {
										if (k == tokenlist.size() - 1 && t.getBuffer().trim().charAt(0) == 0xA0) {
											continue;
										}
										phraseBuffer.append(t.getBuffer() + " ");
										k++;
									}
									String phraseStr = phraseBuffer.toString().trim();
									if (phraseStr.length() > 2 && !phraseStr.equalsIgnoreCase("University")
											&& !phraseStr.equalsIgnoreCase("Institute")
											&& !phraseStr.equalsIgnoreCase("College")) {
										Phrase addPhrase = new Phrase(phraseStr, ngram, sentenceIndex);
										// if
										// (phraseBuffer.toString().contains("University"))
										// System.out.println("Adding Phrase 1:"
										// +phraseStr);
										if (!document.getDocumentPhrases().contains(addPhrase))
											document.addDocumentPhrase(addPhrase);
									}
									i += ngram.size();
									break;
								}
							}
						}
					}
				}
			}
			return document;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Utility method used to obtain the exact phrase (exactly as it appears in
	 * the sentence) given the corresponding ngram.
	 *
	 * @param sentence
	 *            The sentence from the document
	 * @param ngram
	 *            The ngram corresponding to the phrase
	 * @return The exact phrase as found in the sentence
	 */
	private String getPhraseBuffer(Sentence sentence, NGram ngram) {
		Token first = ngram.getFirst();
		Token last = ngram.getLast();

		String buffer = null;

		if (last.getPosition() + last.getBuffer().length() < sentence.toString().length()) {
			buffer = sentence.toString().substring(first.getPosition(), last.getPosition() + last.getBuffer().length());
		} else {
			buffer = sentence.toString().substring(first.getPosition());
		}
		return buffer;
	}

	/**
	 * EBB Modified it to take into account leading and trailing commas.
	 *
	 * A utility method that is used to create an array of {@link NGram}. Given
	 * an array of tokens and a starting index this method creates all
	 * {@link NGram} of sizes below the maximum value defined. An corresponding
	 * array of POS and chunks are also passed to this method so that
	 * information can be stored in the {@link NGram}.
	 *
	 * EB:: if the tokenList is: t1 t2 t3 t4 t5 and ngramSize=4 -- which is
	 * actually 3 -- N.B. ngramSize is the maximum size of n-grams (i.e., max
	 * num tokens in ngram) And the size of the highlighted words, and num words
	 * in drop down phrases! this may be too small. Then this method RETURNS an
	 * array list of size ngramSize=4 containging: [ t1 t1 t2 t1 t2 t3 t1 t2 t3
	 * t4 ] N.B. the shortest n-gram is first. But then in document processor he
	 * starts with the longest first (I think!).
	 *
	 * @param tokens
	 *            array of string (EB list of sequential tokens from a sentence)
	 *            from which several {@link NGram} will be created
	 * @param pos
	 *            an array of POS tags (assuming the Penn TreeBank tagset)
	 * @param chunks
	 *            an array of chunk tags (assuming a IOB phrase tagset)
	 * @param start
	 *            the index of the first string of the array of {@link NGram}
	 * @return a list of {@link NGram}
	 * @throws CloneNotSupportedException
	 */
	private ArrayList<NGram> getNGrams(List<Token> tokens, String[] pos, String[] chunks, int start)
			throws CloneNotSupportedException {
		int end = start;
		while (end < tokens.size() && !tokens.get(end).getBuffer().trim().equals(",")
				&& !tokens.get(end).getBuffer().trim().equals("$")
				&& !tokens.get(end).getBuffer().trim().equalsIgnoreCase("or")
				&& !tokens.get(end).getBuffer().trim().equalsIgnoreCase("and")
				&& !tokens.get(end).getBuffer().trim().equalsIgnoreCase("and/or")
				// or and / can cause logical problems for instance:
				// computer software / hardware
				// should be viewed as computer software OR computer hardware
				// TODO EBB add the logic for "NN_0 NN_1 or NN_2" to become two
				// ngrams -- NN_0 NN_1 and NN_0 NN_2. Also "or" can be a /.
				&& !tokens.get(end).getBuffer().trim().equalsIgnoreCase("/")
				&& !tokens.get(end).getBuffer().trim().equalsIgnoreCase("/or") && end < start + maxNgramSize
				&& (pos[end].startsWith("NN") || pos[end].startsWith("VBN")
						|| tokens.get(end).getBuffer().trim().equalsIgnoreCase("of"))) {
			end++;
			if (end < tokens.size() - 2 && tokens.get(end).getBuffer().trim().equals("A")
					&& tokens.get(end + 1).getBuffer().trim().equals("&")
					&& tokens.get(end + 2).getBuffer().trim().equals("M")) // for
																			// Texas
																			// A&M
			{
				end = end + 2;
			}
			// if (false &&
			// tokens.get(start).getBuffer().trim().equalsIgnoreCase("texas") &&
			// end < tokens.size())
			// {
			// System.out.println("Token:" + tokens.get(end).getBuffer() + ":" +
			// chunks[end] + " pos:" + pos[end]);
			// if (end+1 < tokens.size())
			// System.out.println("Token:" + tokens.get(end+1).getBuffer() + ":"
			// + chunks[end+1] + " pos:" + pos[end+1]);
			// if (end+2 < tokens.size())
			// System.out.println("Token:" + tokens.get(end+2).getBuffer() + ":"
			// + chunks[end+2] + " pos:" + pos[end+2]);
			// }
		}

		ArrayList<NGram> ngrams = new ArrayList<NGram>(end - start);
		NGram currentNgram = new NGram();
		Token token;

		for (int i = start; i < end && i < tokens.size(); i++) {
			token = tokens.get(i);

			// TODO EB -- this may not be a good idea. May be we should remove
			// these odd characters from the token. But we have to be careful
			// because it may appear in the middle of the noun or verb phrase.
			if (token.getBuffer().contains("/") || token.getBuffer().contains("\"") || token.getBuffer().contains(":")
					|| token.getBuffer().contains(";") || token.getBuffer().contains("'")
					|| token.getBuffer().contains("."))
				continue;

			token.setPos(pos[i]); // entire tokens in the tokensList of a
									// sentence
			token.setChunk(chunks[i]); // once and not repeat here again and
										// again.
			currentNgram.addElement(token);
			ngrams.add(currentNgram);
			currentNgram = (NGram) currentNgram.clone();
		}
		return ngrams;
	}

	static Set states = new HashSet();
	static {
		states.add("ALABAMA");
		states.add("ALASKA");
		states.add("ARIZONA");
		states.add("ARKANSAS");
		states.add("CALIFORNIA");
		states.add("COLORADO");
		states.add("CONNECTICUT");
		states.add("DELAWARE");
		states.add("FLORIDA");
		states.add("GEORGIA");
		states.add("HAWAII");
		states.add("IDAHO");
		states.add("ILLINOIS");
		states.add("INDIANA");
		states.add("IOWA");
		states.add("KANSAS");
		states.add("KENTUCKY");
		states.add("LOUISIANA");
		states.add("MAINE");
		states.add("MARYLAND");
		states.add("MASSACHUSETTS");
		states.add("MICHIGAN");
		states.add("MINNESOTA");
		states.add("MISSISSIPPI");
		states.add("MISSOURI");
		states.add("MONTANA");
		states.add("NEBRASKA");
		states.add("NEVADA");
		states.add("NEW HAMPSHIRE");
		states.add("NEW JERSEY");
		states.add("NEW MEXICO");
		states.add("NEW YORK");
		states.add("NORTH CAROLINA");
		states.add("NORTH DAKOTA");
		states.add("OHIO");
		states.add("OKLAHOMA");
		states.add("OREGON");
		states.add("PENNSYLVANIA");
		states.add("RHODE ISLAND");
		states.add("SOUTH CAROLINA");
		states.add("SOUTHDAKOTA");
		states.add("TENNESSEE");
		states.add("TEXAS");
		states.add("UTAH");
		states.add("VERMONT");
		states.add("VIRGINIA");
		states.add("WASHINGTON");
		states.add("WEST VIRGINIA");
		states.add("WISCONSIN");
		states.add("WYOMING");
	};

	public static boolean isUSState(String stateName) {
		return states.contains(stateName.toUpperCase());
	}

	public DocumentRS getDocument() {
		return document;
	}

	public void setSentences(ArrayList<Sentence> sentences) {
		document.setSentences(sentences);
	}

	public Set<String> getPhrasesFromPatterns(Document sourceDocument) throws Exception {
		Set<String> set;
		PatternExtractor pe = new PatternExtractor(sourceDocument);
		set = pe.extract();
		return set;
	}

	private void printSentences(File file, SentenceSpliter spliter, List<Sentence> sentences) {
		try {
			File tempDir = PlatformProperties.getInstance().getResourceFile("temp");
			if (!tempDir.exists())
				tempDir.mkdirs();
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
					new File(tempDir, file.getName() + "_" + spliter.getClass().getSimpleName() + ".txt"))));

			out.println(" ------- SpliterClass: " + spliter.getClass().getSimpleName() + "-------");
			for (Sentence sentence : sentences) {
				out.println(sentence.toString());
			}
			out.close();
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
	}

	private int getInt(String value) {
		try {
			return new Integer(value).intValue();
		} catch (Exception x) {
			return 0;
		}
	}
}
