/*
 * Methods related to user paid subscriptions
 */

package com.textonomics.subscription;

import com.textnomics.data.JobPosting;
import com.textnomics.data.ResumeScore;
import com.textnomics.data.SubscriptionPlan;
import com.textonomics.PlatformProperties;
import com.textonomics.db.UserDBAccess;
import com.textonomics.user.Subscription;
import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderPool;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Dev Gude
 */
public class SubscriptionService {

	public static final double PRICE_PER_RESUME = 0.50;
	public static final double PRICE_PER_POSTING = 19.95;
	public static final int MAX_POSTING_RUNS = 5;
	public static final int MAX_INVITEES = 2;

	public static boolean isValidSubscription(User user) throws Exception {
		return isValidSubscription(user, null, 0, false);
	}

	public static boolean isValidSubscription(User user, JobPosting posting, int newResumes, boolean decrement)
			throws Exception {
		if (user.isAdmin() || user.isMarketing())
			return true;
		long resumeProcessed = new ResumeScore()
				.getCount("user_name =" + ResumeScore.toSQL(user.getLogin()) + " and by_invitation = false");
		long oldScores = new ResumeScore()
				.getCount("JOB_ID =" + posting.getId() + " and user_name =" + UserDBAccess.toSQL(user.getLogin()));
		newResumes = newResumes - (int) oldScores;
		boolean newPosting = true;
		if (oldScores > 0)
			newPosting = false;
		long postings = new JobPosting().getCount(
				"user_name =" + ResumeScore.toSQL(user.getLogin()) + "  and id in (select job_id from RESUME_SCORE)");
		int freeResumes = getInt(PlatformProperties.getInstance().getProperty("com.textnomics.free_resumes_processed"));
		if (freeResumes == 0)
			freeResumes = 500;
		int freePostings = getInt(
				PlatformProperties.getInstance().getProperty("com.textnomics.free_postings_processed"));
		if (freePostings == 0)
			freePostings = 2;
		System.out.println("numprocessed:" + user.getNumProcessed() + " newResumes:" + newResumes + " postings:"
				+ postings + " newPosting:" + newPosting);
		// No checking free postings for now
		if (true || user.getNumProcessed() + newResumes >= freeResumes || (postings >= freePostings && newPosting)) {
			UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();
			List<Subscription> subscriptions = userDataProvider.getSubscriptionsByUser(user);
			Subscription validSubscription = null;
			if (subscriptions != null) {
				Calendar today = Calendar.getInstance();
				today.set(Calendar.HOUR, 0);
				today.set(Calendar.MINUTE, 0);
				today.set(Calendar.SECOND, 0);
				today.set(Calendar.MILLISECOND, 0);
				for (Subscription subscription : subscriptions) {
					if (!subscription.isPaid())
						continue;
					if (subscription.getExpirationDate() != null
							&& subscription.getExpirationDate().before(today.getTime()))
						continue;
					// if ((subscription.getUnusedResumeCount() > 0 &&
					// subscription.getUnusedJobPostings() > 0) &&
					// Only check postings for now
					if ((subscription.getUnusedJobPostings() > 0) && (subscription.getExpirationDate() == null
							|| subscription.getExpirationDate().after(today.getTime())))

					{
						validSubscription = subscription;
					}
				}
			}
			if (validSubscription == null) {
				System.out.println("User:" + user.getLogin() + " postings:" + postings + " resumes:" + resumeProcessed
						+ " invalid");
				return false;
			}
			if (decrement) {
				int remaining = validSubscription.getUnusedJobPostings();
				remaining--;
				if (remaining < 0)
					remaining = 0;
				validSubscription.setUnusedJobPostings(remaining);
				remaining = validSubscription.getUnusedResumeCount() - newResumes;
				if (remaining < 0)
					remaining = 0;
				validSubscription.setUnusedResumeCount(remaining);
				userDataProvider.updateSubscription(validSubscription);
				userDataProvider.commit();
			}
			UserDataProviderPool.getInstance().releaseProvider();
		}
		System.out.println(
				"User:" + user.getLogin() + " postings:" + postings + " resumes:" + resumeProcessed + " valid");
		return true;
	}

	public static boolean hasUnexpiredSubscription(User user) throws Exception {
		UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();
		List<Subscription> subscriptions = userDataProvider.getSubscriptionsByUser(user);

		UserDataProviderPool.getInstance().releaseProvider();
		if (subscriptions != null) {
			Calendar today = Calendar.getInstance();
			today.set(Calendar.HOUR, 0);
			today.set(Calendar.MINUTE, 0);
			today.set(Calendar.SECOND, 0);
			today.set(Calendar.MILLISECOND, 0);
			for (Subscription subscription : subscriptions) {
				if (!subscription.isPaid())
					continue;
				if (subscription.getExpirationDate() != null && subscription.getExpirationDate().after(today.getTime()))
					return true;
			}
		}
		return false;
	}

	public static SubscriptionPlan getUserPlan(User user) {
		return null;
	}

	public static List<SubscriptionPlan> getValidSubscriptionPlans() throws Exception {
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		List<SubscriptionPlan> plans = new SubscriptionPlan().getObjects("");
		for (int i = 0; i < plans.size(); i++) {
			if (plans.get(i).getValidFrom().after(today.getTime())
					|| plans.get(i).getValidTo().before(today.getTime())) {
				plans.remove(i);
				i--;
			}
		}
		return plans;
	}

	private static int getInt(String value) {
		try {
			return new Integer(value).intValue();
		} catch (Exception x) {
			return 0;
		}
	}
}
