/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Vinod
 */
public class BeanstalkWorkerThread extends Thread {

	static Logger logger = Logger.getLogger(BeanstalkWorkerThread.class);

	public BeanstalkWorkerThread() {
		try {

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
	}

	public void run() {
		while (true) {
			logger.info("inside the beanstalk worker");
			BeanstalkWorker w;
			try {
				logger.info("Waiting for the job ready ");
				w = new BeanstalkWorker();
				w.readJob();
				sleep(10);
				logger.info("Sleeping ");
			} catch (Exception ex) {
				java.util.logging.Logger.getLogger(BeanstalkWorkerThread.class.getName()).log(Level.SEVERE, null, ex);
			}

		}
	}
}