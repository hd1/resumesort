package com.textonomics.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Class used to clean path names when files are uploaded. Some Internet
 * Explorer clients send the full path of the file on the clients host. This
 * class takes care of those situations
 *
 * @author Nuno Seco
 *
 */

public class FileUtils {
	private FileUtils() {
		;
	}

	/**
	 * Cleans strips all the path info from the given string
	 *
	 * @param file
	 * @return a clean file name
	 */
	public static String removePathInfo(String file) {
		StringBuilder builder = new StringBuilder();

		for (int i = file.length() - 1; i >= 0; i--) {
			if (file.charAt(i) == ':' || file.charAt(i) == '\\' || file.charAt(i) == '/' || file.charAt(i) == '#')
				break;

			builder.append(file.charAt(i));
		}

		return builder.reverse().toString();
	}

	/*
	 * Serializes an object to a file
	 */
	public static void serializeOject(Object object, File file) throws Exception {
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		try {
			fos = new FileOutputStream(file);
			out = new ObjectOutputStream(fos);
			out.writeObject(object);
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/*
	 * Reads a serialized object from a file
	 */
	public static Object deSerializeOject(File file) {
		FileInputStream fis = null;
		ObjectInputStream in = null;
		try {
			fis = new FileInputStream(file);
			in = new ObjectInputStream(fis);
			Object object = in.readObject();
			in.close();
			return object;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * Copies the the src file to dst file
	 *
	 * @param src
	 * @param dst
	 * @throws IOException
	 */
	public static void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	public static List<File> openZipFile(File zipFile, String directory) throws Exception {
		List<File> contents = new ArrayList<File>();
		FileInputStream fis = new FileInputStream(zipFile);
		ZipInputStream zin = new ZipInputStream(new BufferedInputStream(fis));
		ZipEntry entry;
		while ((entry = zin.getNextEntry()) != null) {
			int BUFFER = 2048;
			int count = 0;
			byte data[] = new byte[BUFFER];
			if (entry.isDirectory())
				continue;
			FileOutputStream fos = new FileOutputStream(directory + "/" + entry.getName().replace('/', '-'));
			BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);
			while ((count = zin.read(data, 0, BUFFER)) != -1) {
				// System.out.write(x);
				dest.write(data, 0, count);
			}
			dest.flush();
			dest.close();
			contents.add(new File(directory + "/" + entry.getName()));
		}
		zin.close();
		return contents;
	}

}
