package com.textonomics.web;

import com.textonomics.PlatformProperties;
import com.textonomics.mail.MailReaderThread;
import java.util.Timer;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.PropertyConfigurator;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Application lifecycle listener used to set application properties in the JVM.
 * These properties are used for bootstrapping. They indicate the location of
 * the configuration file and add the need information for the google check out
 * api.
 * 
 * Suggestion: Put the data for the google checkout API in the conf file and
 * keep only ONE hardcoded entry.
 *
 *
 * @author Nuno Seco
 *
 */
public class ResumeCakeWalkConfigurator implements ServletContextListener {
	public static String TEMP_DIR;
	public static String systemMessage = "";
	public static Timer mysqlWatch1 = null;
	public static Timer mysqlWatch2 = null;
	public static int interval = 60; // minutes
	MailReaderThread mailReader = null;

	/**
	 * Default constructor.
	 */
	public ResumeCakeWalkConfigurator() {
		;
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent scEvent) {
		System.out.println("------------------- ResumeSorter - Context Initialized ----------------");
		// For soem strange reason log4j can not find the properties file, so
		// need to do this!
		String classDir = scEvent.getServletContext().getRealPath("WEB-INF/classes");
		PropertyConfigurator.configure(classDir + "/log4j.properties");
		System.setProperty("com.textonomics.resource.dir",
				scEvent.getServletContext().getRealPath("WEB-INF/Resources"));
		PlatformProperties.resourceDirectory = scEvent.getServletContext().getRealPath("WEB-INF/Resources");
		// System.setProperty("com.textonomics.checkout.merchantid",
		// "120939051152649");
		// System.setProperty("com.textonomics.checkout.merchantkey",
		// "TMJU8XYKzJk9YyuXD6cmaA");
		// System.setProperty("com.textonomics.checkout.merchantid",
		// "722289033647715"); // ResumeSort production
		// System.setProperty("com.textonomics.checkout.merchantkey",
		// "c7CgxLmLB1160o72D3H1rg"); // ResumeSort production
		// System.setProperty("com.textonomics.checkout.url",
		// "https://sandbox.google.com/checkout/api/checkout/v2/merchantCheckout/Merchant/722289033647715");
		if (!"true".equals(PlatformProperties.getInstance().getProperty("com.textnomics.production"))) {
			System.setProperty("com.textonomics.checkout.merchantid", "772256547206103"); // ResumeSort
																							// development
			System.setProperty("com.textonomics.checkout.merchantkey", "LhBkTdKCIkZUVHXY0ZI46A"); // ResumeSort
																									// development
			System.setProperty("com.textonomics.checkout.url",
					"https://sandbox.google.com/checkout/api/checkout/v2/merchantCheckout/Merchant/772256547206103");
		} else {
			System.setProperty("com.textonomics.checkout.merchantid", "722289033647715"); // ResumeSort
																							// production
			System.setProperty("com.textonomics.checkout.merchantkey", "c7CgxLmLB1160o72D3H1rg"); // ResumeSort
																									// production
			System.setProperty("com.textonomics.checkout.url",
					"https://checkout.google.com/api/checkout/v2/request/Merchant/722289033647715");
		}
		mysqlWatch1 = new Timer(true);
		mysqlWatch1.schedule(new com.textonomics.user.dataprovider.MySQLWatchDog(), 1000 * 60, 1000 * 60 * interval);
		mysqlWatch2 = new Timer(true);
		mysqlWatch2.schedule(new com.textonomics.wordnet.dataprovider.MySQLWatchDog(), 1000 * 60, 1000 * 60 * interval);
		TEMP_DIR = scEvent.getServletContext().getRealPath("temp");
		try {
			if ("true".equalsIgnoreCase(PlatformProperties.getInstance().getProperty("com.textnomics.read_email"))) {
				mailReader = new MailReaderThread();
				mailReader.start();
			}
		} catch (Exception x) {
			x.printStackTrace();
		}

		String worker = PlatformProperties.getInstance().getProperty("com.textonomics.beanstalkworker");
		if ("true".equals(worker)) {
			BeanstalkWorkerThread bwt = new BeanstalkWorkerThread();
			bwt.start();
		}
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		// Do nothing
		systemMessage = "System Going Down Immediately";
		mysqlWatch1.cancel();
		mysqlWatch2.cancel();
		if (mailReader != null)
			mailReader.stop();
	}

}
