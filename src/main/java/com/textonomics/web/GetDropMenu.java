package com.textonomics.web;

import com.textonomics.Phrase;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 *
 *
 * @author Rodrigo Neves
 *
 */

public class GetDropMenu extends HttpServlet {

	public static final String EMPTY_USER_OPTION_TEXT = "[ Enter Text ]";
	static Logger logger = Logger.getLogger(GetDropMenu.class);

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {
			HttpSession session = request.getSession();

			int fileIndex = Integer.parseInt(request.getParameter("viewFileIndex"));
			int idx = Integer.parseInt(request.getParameter("idx"));
			int type = Integer.parseInt(request.getParameter("type"));
			logger.debug("idx:" + idx + " type:" + type);
			// Drop Menu
			if (type == 1) {
				List<String> optionsList = (List<String>) session
						.getAttribute(SESSION_ATTRIBUTE.OPTIONS_LIST.toString());
				if (optionsList == null) {
					logger.info("No data in session, must have expired");
					return;
				}
				response.setContentType("text/xml");
				returnMenuAsUL(session, idx, out); // Changed method to use
													// <UL><LI> instead of a
													// <TABLE>
				// returnMenu(session, idx, out);
			}

			// Lists size
			else if (type == 5) {
				response.setContentType("text/xml");
				returnOptionsListSize(session, out, fileIndex);
			}

		} finally {
			out.close();
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on
	// the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	public String getManualEntryStr(String index) {

		return "<tr><td colspan='2' class='enterOwnText' ><input type='text' id='manualEntry" + index
				+ "' class='enterOwnText' maxlength='100' value='" + EMPTY_USER_OPTION_TEXT
				+ "' onkeydown='return handleKeyDown(event)' onclick='return handleUserOptionClick(this)'/></td><td>&nbsp;</td><td>&nbsp;</td></tr>";

	}

	public String getBottomLinksStr(String index, boolean hasExtras, String moreOrLessStr) {

		String bottomLinksStr = "<tr height='10px'><td width='100%' colspan='2'><table width='100%'><tr>";
		bottomLinksStr += "<td width='40%' class=feedbackLink><a href='void(0);' onclick='return showFeedback(" + index
				+ ");'>Feedback</a></td>";
		bottomLinksStr += "</td><td width='20%' class=tagLink>";

		// bottomLinksStr += "<a href='void(0);' onclick='return
		// toggleTag(this," + index + ");'>" + "Tag as Error" + "</a>";
		// (tagArray[index]==0? "tag" : "untag")
		bottomLinksStr += "</td><td width='40%' class=moreLink>";
		if (hasExtras) {
			bottomLinksStr += "<a href='void(0);' onclick='return toggleMore(this," + index + ");'>" + moreOrLessStr
					+ "</a>";
		}
		bottomLinksStr += "</td></tr></table></td></tr>";

		return bottomLinksStr;
	}

	public void returnMenu(HttpSession session, int idx, PrintWriter out) {

		StringBuffer strb = new StringBuffer();

		// Add XML header
		strb.append("<?xml version='1.0' encoding='ISO-8859-1'?>");
		strb.append("<content>");
		strb.append("<menu><![CDATA[");

		// Obtain lists in session
		List<String> optionsList = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.OPTIONS_LIST.toString());
		List<String> helpList = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.HELP_LIST.toString());
		List<String> extrasIndex = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.EXTRAS_INDEX.toString());

		// Create HTML
		boolean hasExtras = (Integer.parseInt(extrasIndex.get(idx)) > 0);
		String[] list = optionsList.get(idx).split(" _LIMIT_ ");

		// int nSet1 = hasExtras ? extrasIndex.get(idx) : list.length;
		int nSet1;
		if (hasExtras)
			nSet1 = Integer.parseInt(extrasIndex.get(idx));
		else
			nSet1 = list.length;

		strb.append("<table id='dropdowntable_" + idx
				+ "' border=1 frame='box' rules='rows' class='tune' cellpadding=10 cellspacing=0>");

		for (int k = 0; k < nSet1; k++) {
			strb.append("<tr id='dropdownrow_" + idx + "_" + k + "'><td nowrap ><a class=");
			if (k == 0)
				strb.append("original");
			else
				strb.append("errOption");

			strb.append(" href='javascript:void(0);' onclick='javascript:selectWord(this," + k + ")'>" + list[k]
					+ "</a></td>");
			if (k != 0)
				strb.append("<td width='10' nowrap><a href='javascript:void(0);' onclick='javascript:deleteWord(this,"
						+ k + ")'><img id='delete_" + idx + "_" + k
						+ "' border='0' src='x.png' title='Remove this Suggestion'></a></td>");
			else
				strb.append("<td>&nbsp;</td><td>&nbsp;</td>");
			// -- new feature Help Info
			// -- only include an icon, if help exists for option j,k
			if (idx < helpList.size()) {
				String[] helpItems = helpList.get(idx).split(" _LIMIT_ ");
				if (k < helpItems.length) {
					String helpMsg = helpItems[k];
					if (helpMsg.length() > 0) {
						strb.append("<td align='right' width='10' nowrap ><img id='info" + k
								+ "' src='i.png' onmouseover='handleTooTip(event);'></td>");
					}
				}
			}
			strb.append("</tr>");
		}

		strb.append(getManualEntryStr(Integer.toString(idx)));
		strb.append(getBottomLinksStr(Integer.toString(idx), hasExtras, "More"));
		strb.append("</table>"); // end the 1st set

		if (hasExtras) {
			// -- Create drop-menu set 2 (for extras)
			strb.append("<div class='dropMenu' id='dropMenu" + idx
					+ "e'><table class='menuTable' cellpadding=0 cellspacing=0>");
			for (int k = 0; k < list.length; k++) {
				// -- new feature Help Info
				// -- only include an icon, if help exists for option j,k
				// -- need to know if we have a help msg, for proper column
				// spacing
				String helpMsg = "";
				if (idx < helpList.size()) {
					String[] helpItems = helpList.get(idx).split(" _LIMIT_ ");
					if (k < helpItems.length) {
						helpMsg = helpItems[k];
					}
				}

				if (k == nSet1)
					strb.append("<tbody class=errOptionExtra>");
				strb.append("<tr><td");
				if (helpMsg.length() == 0)
					strb.append(" colspan='2'");
				if (k == nSet1)
					strb.append(" style='border-top: solid 1px black;'");
				strb.append("><a class=errOption");
				if (k == 0)
					strb.append("1");
				strb.append(" href='javascript:void(0);' onclick='javascript:selectWord(this," + k + ")'>" + list[k]
						+ "</a></td>");

				if (helpMsg.length() > 0) {
					strb.append("<td align='right'");
					if (k == nSet1)
						strb.append(" style='border-top: solid 1px black;'");
					if (k < nSet1) {
						strb.append(
								"><img id='info" + k + "' src='tinyInfo.gif' onmouseover='handleTooTip(event);'></td>");
					} else {
						strb.append("></td>");
					}
				}
			}

			strb.append("</tbody>");
			// -- Do bottom links (Feedback & More/Less)
			strb.append(getManualEntryStr("" + idx + "e"));
			strb.append(getBottomLinksStr(Integer.toString(idx), hasExtras, "Less"));
			strb.append("</table></div>"); // end the 2nd set

		}

		strb.append("]]></menu>");

		String helpItem = helpList.get(idx).replaceAll(" _LIMIT_ ", "| ");
		// System.out.println(helpList.get(idx));
		strb.append("<helplist><![CDATA[" + helpItem + "]]></helplist>");
		String option = optionsList.get(idx).replaceAll(" _LIMIT_ ", ",");
		// System.out.println(option);
		strb.append("<optionlist><![CDATA[" + option + "]]></optionlist>");
		String extras = extrasIndex.get(idx);
		strb.append("<extrasindex><![CDATA[" + extras + "]]></extrasindex>");
		List<String> deletedList = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.DELETED_LIST.toString());
		String deletedlist = "";
		if (deletedList != null)
			deletedlist = deletedList.get(idx);
		strb.append("<deletedlist><![CDATA[" + deletedlist + "]]></deletedlist>");
		strb.append("</content>");

		// Static html code for testing
		/*
		 * strb.append("<div id=\"dropMenu"
		 * +idx+"\" class=\"dropMenu\" style=\"left: 350px; top: 71px; visibility: visible;\">"
		 * ); strb.
		 * append("<table class=\"menuTable\" cellpadding=\"0\" cellspacing=\"0\">"
		 * ); strb.append("<tbody><tr>"); strb.
		 * append("<td><a class=\"errOption1\" href=\"javascript:void(0);\" onclick=\"javascript:selectWord(this,0)\">HR</a>"
		 * ); strb.append("</td></tr><tr>"); strb.
		 * append("<td><a class=\"errOption\" href=\"javascript:void(0);\" onclick=\"javascript:selectWord(this,1)\"> SEC</a>"
		 * ); strb.
		 * append("</td><td align=\"right\"><img id=\"info1\" src=\"tinyInfo.gif\" onmouseover=\"handleTooTip(event);\">"
		 * ); strb.append("</td></tr><tr>"); strb.
		 * append("<td><a class=\"errOption\" href=\"javascript:void(0);\" onclick=\"javascript:selectWord(this,2)\"> Business Maturity</a>"
		 * ); strb.append("</td>"); strb.
		 * append("<td align=\"right\"><img id=\"info2\" src=\"tinyInfo.gif\" onmouseover=\"handleTooTip(event);\">"
		 * ); strb.append("</td></tr><tr>");strb.append(
		 * "<td colspan=\"2\"><input id=\"manualEntry8\" class=\"userSuppliedText\" maxlenth=\"100\" value=\"[ Enter Text ]\" onkeydown=\"return handleKeyDown(event)\" onclick=\"return handleUserOptionClick(this)\" type=\"text\">"
		 * ); strb.
		 * append("</td></tr><tr height=\"10\"> <td colspan=\"2\" width=\"100%\">"
		 * ); strb.append("<table width=\"100%\"> <tbody>");
		 * strb.append("<tr><td class=\"feedbackLink\" width=\"40%\">"); strb.
		 * append("<a href=\"void%280%29;\" onclick=\"return showFeedback(8);\">Feedback</a>"
		 * ); strb.append("</td><td class=\"tagLink\" width=\"20%\">"); strb.
		 * append("<a style=\"color: red;\" href=\"void%280%29;\" onclick=\"return toggleTag(this,8);\"> Tag as Error</a>"
		 * ); strb.append("</td> <td class=\"moreLink\" width=\"40%\">"); strb.
		 * append("<a href=\"void%280%29;\" onclick=\"return toggleMore(this,8);\">More</a>"
		 * ); strb.
		 * append("</td> </tr> </tbody> </table> </td></tr></tbody></table>");
		 */
		// strb.append("</div>");
		out.write(strb.toString());
	}

	/*
	 * Returns Dropdown menu as <UL></UL> instead of a table (DG)
	 * 
	 */
	public void returnMenuAsUL(HttpSession session, int idx, PrintWriter out) {

		StringBuffer strb = new StringBuffer();

		// Add XML header
		strb.append("<?xml version='1.0' encoding='ISO-8859-1'?>");
		strb.append("<content>");
		strb.append("<menu><![CDATA[");

		// Obtain lists in session
		List<String> optionsList = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.OPTIONS_LIST.toString());
		List<String> helpList = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.HELP_LIST.toString());
		List<String> extrasIndex = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.EXTRAS_INDEX.toString());
		if (extrasIndex == null)
			return;
		// Create HTML
		boolean hasExtras = false;
		if (idx < extrasIndex.size())
			hasExtras = (Integer.parseInt(extrasIndex.get(idx)) > 0);
		String[] list = optionsList.get(idx).split(" _LIMIT_ ");

		// int nSet1 = hasExtras ? extrasIndex.get(idx) : list.length;
		int nSet1;
		if (hasExtras)
			nSet1 = Integer.parseInt(extrasIndex.get(idx));
		else
			nSet1 = list.length;

		strb.append("<div class='tune'><ul id='dropdowntable_" + idx + "'>");

		for (int k = 0; k < nSet1; k++) {
			strb.append("<li id='dropdownrow_" + idx + "_" + k + "' class='");
			String suggcls = "suggestion";
			if (k == 0) {
				strb.append("original");
				suggcls = "original";
			} else {
				strb.append("");
			}

			strb.append("'><a class='" + suggcls + "' href='javascript:void(0);' onclick='javascript:selectWord(this,"
					+ k + ")'>" + list[k] + "</a>");
			strb.append("<span class='plus' title='Add to text' id='plusspan_" + idx + "_" + k
					+ "'><a href='javascript:void(0);' onclick='javascript:addWord(this," + k + ")'><img id='plus_"
					+ idx + "_" + k + "' border='0' src='plus.png' title='Add to text box'></a></span>");
			if (k != 0)
				strb.append("<span class='delete' title='Mark this suggestion as Invalid and Remove it' id='sugg_" + idx
						+ "_" + k + "'><a href='javascript:void(0);' onclick='javascript:deleteWord(this," + k
						+ ")'><img id='delete_" + idx + "_" + k
						+ "' border='0' src='x.png' title='Mark this suggestion as Invalid and Remove it'></a></span>");
			else
				strb.append("<span class='delete' title='Mark all suggestions as Invalid' id='sugg_" + idx + "_" + k
						+ "'><a href='javascript:void(0);' onclick='javascript:deleteWord(this," + k
						+ ")'><img id='delete_" + idx + "_" + k
						+ "' border='0' src='x.png' title='Mark all suggestions as Invalid'></a></span>");
			// -- new feature Help Info
			// -- only include an icon, if help exists for option j,k
			if (idx < helpList.size()) {
				String[] helpItems = helpList.get(idx).split(" _LIMIT_ ");
				if (k < helpItems.length) {
					String helpMsg = helpItems[k];
					if (helpMsg.length() > 0) {
						strb.append("<span class='info'><img id='info" + k
								+ "' src='tinyInfo.gif' onmouseover='handleTooTip(event);'></span>");
					}
				}
			}
			strb.append("</li>");
		}

		strb.append(getManualEntryLiStr(Integer.toString(idx), list[0]));
		strb.append(getBottomLinksLiStr(Integer.toString(idx), hasExtras, "More"));
		strb.append("</ul></div>"); // end the 1st set

		if (hasExtras) {
			// -- Create drop-menu set 2 (for extras)
			strb.append("<div class='dropMenu' id='dropMenu" + idx
					+ "e'><table class='menuTable' cellpadding=0 cellspacing=0>");
			for (int k = 0; k < list.length; k++) {
				// -- new feature Help Info
				// -- only include an icon, if help exists for option j,k
				// -- need to know if we have a help msg, for proper column
				// spacing
				String helpMsg = "";
				if (idx < helpList.size()) {
					String[] helpItems = helpList.get(idx).split(" _LIMIT_ ");
					if (k < helpItems.length) {
						helpMsg = helpItems[k];
					}
				}

				if (k == nSet1)
					strb.append("<tbody class=errOptionExtra>");
				strb.append("<tr><td");
				if (helpMsg.length() == 0)
					strb.append(" colspan='2'");
				if (k == nSet1)
					strb.append(" style='border-top: solid 1px black;'");
				strb.append("><a class=errOption");
				if (k == 0)
					strb.append("1");
				strb.append(" href='javascript:void(0);' onclick='javascript:selectWord(this," + k + ")'>" + list[k]
						+ "</a></td>");

				if (helpMsg.length() > 0) {
					strb.append("<td align='right'");
					if (k == nSet1)
						strb.append(" style='border-top: solid 1px black;'");
					if (k < nSet1) {
						strb.append(
								"><img id='info" + k + "' src='tinyInfo.gif' onmouseover='handleTooTip(event);'></td>");
					} else {
						strb.append("></td>");
					}
				}
			}

			strb.append("</tbody>");
			// -- Do bottom links (Feedback & More/Less)
			strb.append(getManualEntryStr("" + idx + "e"));
			strb.append(getBottomLinksStr(Integer.toString(idx), hasExtras, "Less"));
			strb.append("</table></div>"); // end the 2nd set

		}

		strb.append("]]></menu>");

		String helpItem = helpList.get(idx).replaceAll(" _LIMIT_ ", "| ");
		// System.out.println(helpList.get(idx));
		strb.append("<helplist><![CDATA[" + helpItem + "]]></helplist>");
		String option = optionsList.get(idx).replaceAll(" _LIMIT_ ", ",");
		// System.out.println(option);
		strb.append("<optionlist><![CDATA[" + option + "]]></optionlist>");
		List<String> deletedList = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.DELETED_LIST.toString());
		String deletedlist = "";
		if (deletedList != null)
			deletedlist = deletedList.get(idx);
		strb.append("<deletedlist><![CDATA[" + deletedlist + "]]></deletedlist>");
		String extras = extrasIndex.get(idx);
		strb.append("<extrasindex><![CDATA[" + extras + "]]></extrasindex>");
		strb.append("</content>");
		out.write(strb.toString());
	}

	public String getManualEntryLiStr(String index, String text) {

		return "<li style='cursor:default'><input type='text' id='manualEntry" + index
				+ "' class='enterOwnText' maxlength='100' value='" + text
				+ "' onkeydown='return handleKeyDown(event)' onclick='return handleUserOptionClick(this)'/></li>";

	}

	public String getBottomLinksLiStr(String index, boolean hasExtras, String moreOrLessStr) {

		String bottomLinksStr = "<li class='feedbackLink'>";
		bottomLinksStr += "<a href='void(0);' class='feedbackLink' onclick='return showFeedback(" + index
				+ ");'>Feedback</a></li>";
		if (hasExtras) {
			bottomLinksStr += "<li><a href='void(0);' onclick='return toggleMore(this," + index + ");'>" + moreOrLessStr
					+ "</a></li>";
		}

		return bottomLinksStr;
	}

	public void returnOptionsListSize(HttpSession session, PrintWriter out, int fileIndex) {

		StringBuffer strb = new StringBuffer();

		List<Map<Phrase, Integer>> matchedPhraseList = (List<Map<Phrase, Integer>>) session
				.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
		Map<Phrase, Integer> matchedPhrases = matchedPhraseList.get(fileIndex);
		strb.append("<?xml version='1.0' encoding='ISO-8859-1'?>");
		strb.append("<content>");
		strb.append("<optionsListSize> " + matchedPhrases.keySet().size() + "</optionsListSize>");
		strb.append("</content>");

		out.write(strb.toString());

	}

}
