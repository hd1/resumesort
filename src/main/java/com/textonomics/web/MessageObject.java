/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

/**
 *
 * @author Preeti Mudda
 */
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * MessageObject that holds three types of Data : HashMap of session attributes,
 * List<file>
 * 
 * @author Preeti Mudda
 */
public class MessageObject extends Object implements Serializable {
	private List<File> listFiles; // list of files for a given job
	private HashMap<String, Object> sessionAttribute = new HashMap<String, Object>(); // Http
																						// session
																						// attributes
																						// put
																						// in
																						// the
																						// HashMap

	/**
	 * Constructs a MessageObject with two arguments
	 * 
	 * @param sessionAttribute:
	 *            HttpSession attributes.
	 * @param listOfFiles
	 *            : list of files per job.
	 */
	public MessageObject(HashMap<String, Object> sessionAttribute, List<File> listOfFiles) {
		this.listFiles = listOfFiles;
		this.sessionAttribute = sessionAttribute;
	}

	/**
	 * 
	 * @return sessionAttribute: returns the sessionAttribute
	 */
	public HashMap<String, Object> getSessionAtt() {
		return sessionAttribute;
	}

	/**
	 *
	 * @return listFiles : list of files per job.
	 */
	public List<File> getListOfFiles() {
		return listFiles;
	}

	/**
	 * Converts an object to a byte array 1. Creates a BteArrayOutputStream
	 * object 2. Passes the ByteArrayOutputStream to ObjectOutputStream 3.
	 * writes the obj to ObjectOutputStream 4. flush the ObjectOutputStream. 5.
	 * returns the byteArray.
	 * 
	 * @param obj
	 *            : object that is coverted to byteArray. here object is
	 *            MessageObject
	 * @return b : returns the serialized MessageObject
	 * @throws IOException
	 */
	public static byte[] serialize(Object obj) throws IOException {
		// 1. creates a BteArrayOutputStream object
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		// 2. Passes the ByteArrayOutputStream to ObjectOutputStream
		ObjectOutputStream o = new ObjectOutputStream(b);
		// 3. writes the obj to ObjectOutputStream
		o.writeObject(obj);
		// 4. flush the ObjectOutputStream.
		o.flush();
		// 5. returns the byteArray.
		return b.toByteArray();
	}

	/**
	 * Converts the byte array to object 1. Creates a BteArrayInputStream object
	 * by passing the bytes 2. Passes the ByteArrayInputStream to
	 * ObjectInputStream 3. return the obj.
	 * 
	 * @param bytes
	 *            : the bytes that need to converted to Object
	 * @return o: returns the deserialzed MessageObject
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		// 1. Creates a BteArrayInputStream object by passing the bytes
		ByteArrayInputStream b = new ByteArrayInputStream(bytes);
		// 2. Passes the ByteArrayInputStream to ObjectInputStream
		ObjectInputStream o = new ObjectInputStream(b);
		// 3. return the obj.
		return o.readObject();
	}
}

// private long jobId;
// private File file;
// private String fileName;

// public MessageObject( HashMap<String,Object> session, String fileName,boolean
// check)
// {
// this.sessionAttribute = session;
// this.fileName = fileName;
// this.check = check;
//
// }
