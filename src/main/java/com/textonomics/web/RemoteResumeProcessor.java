/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.textonomics.web;

/**
 *
 * @author Preeti Mudda
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.textnomics.data.DeletedPhrase;
import com.textonomics.DocumentRS;
import com.textonomics.DocumentProcessorRS;
import com.textonomics.KeyPhraseIdentifierPipe;
import com.textonomics.NumericFilter;
import com.textonomics.PhraseFilterPipe;
import com.textonomics.PhraseList;
import com.textonomics.PhraseListFilter;
import com.textonomics.PlatformProperties;
import com.textonomics.nlp.DefaultNLPSuite;
import com.textonomics.nlp.NLPSuite;
import com.textonomics.nlp.Sentence;
import com.textonomics.nlp.SentenceSpliter;
import com.textonomics.wordnet.DomainKeyPhraseIdentifier;
import com.textonomics.wordnet.model.Domain;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Preeti Mudda
 */
public class RemoteResumeProcessor extends Thread {

	static Logger logger = Logger.getLogger(RemoteResumeProcessor.class);
	private SentenceSpliter sentenceDetector;
	private NLPSuite suite;
	// boolean noPhrases;
	KeyPhraseIdentifierPipe keyTokenIdentifier = new KeyPhraseIdentifierPipe();
	PhraseFilterPipe sourceTokenFilterer = new PhraseFilterPipe();
	// File resumes;
	List<File> resumeFiles = new ArrayList<File>();
	HashMap<String, Object> sessionAttributes = new HashMap<String, Object>();
	private long jobId;
	// public RemoteResumeProcessor(HashMap<String,Object> session, File
	// resumes, boolean noPhrases)
	// {
	// try {
	// this.suite = new DefaultNLPSuite();
	// this.sentenceDetector = suite.getSpliter();
	// this.sessionAttributes = session;
	// this.resumes = resumes;
	// this.noPhrases = noPhrases;
	// } catch (Exception ex) {
	// ex.printStackTrace();
	// logger.error(ex);
	// }
	// }
	//
	// public RemoteResumeProcessor(HashMap<String,Object> session, String
	// resumeFileName, boolean noPhrases)
	// {
	// try {
	// this.suite = new DefaultNLPSuite();
	// this.sentenceDetector = suite.getSpliter();
	// this.sessionAttributes = session;
	// this.resumes = new File(resumeFileName);
	// this.noPhrases = noPhrases;
	// } catch (Exception ex) {
	// ex.printStackTrace();
	// logger.error(ex);
	// }
	// }

	/**
	 * HashMap<String,Object> session, a hashmap where the key is "ID", and the
	 * value is the user session id List<File> resumeFiles, long jobId:: set by
	 * BeanStalkd
	 * 
	 * @param session
	 * @param resumeFiles
	 * @param jobId
	 */
	public RemoteResumeProcessor(HashMap<String, Object> session, List<File> resumeFiles, long jobId) {
		try {
			this.suite = new DefaultNLPSuite();
			this.sentenceDetector = suite.getSpliter();
			this.sessionAttributes = session;
			this.resumeFiles = resumeFiles;
			this.jobId = jobId;
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
	}

	public void run() {

		WordNetDataProviderPool.getInstance().acquireProvider();

		process();

		WordNetDataProviderPool.getInstance().releaseProvider();
	}

	/**
	 * 1. creates a pool of threads 2. creates a
	 */
	private void process() {

		// 0 start timing to see how much each resume takes
		long start = System.currentTimeMillis();
		int numResumes = 0;

		try {
			sessionAttributes.put(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(), new Integer(resumeFiles.size()));
			DomainKeyPhraseIdentifier defaultKeyTokenIdentifier = new DomainKeyPhraseIdentifier(
					Domain.getDomainByName("Generic"));
			keyTokenIdentifier.addKeyTermIdentifer(defaultKeyTokenIdentifier); // ??
			HashMap<String, PhraseListFilter> tokenListFilterMap;
			tokenListFilterMap = new HashMap<String, PhraseListFilter>();
			tokenListFilterMap.put("Default 100",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt"), 100));
			tokenListFilterMap.put("Default 300",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt"), 300));
			tokenListFilterMap.put("Default 600",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt"), 600));
			tokenListFilterMap.put("Default All",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt")));
			sourceTokenFilterer.addTokenFilter(tokenListFilterMap.get("Default 300")); // Checks
																						// if
																						// the
																						// word
																						// is
																						// in
																						// the
																						// top
																						// 300
																						// words
																						// list.
			sourceTokenFilterer.addTokenFilter(new NumericFilter());

			NLPSuite suite = new DefaultNLPSuite();
			SentenceSpliter sentenceDetector = suite.getSpliter();
			Map<String, Integer> allResumePhrases = (Map<String, Integer>) sessionAttributes
					.get(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
			if (allResumePhrases == null) {
				allResumePhrases = new TreeMap<String, Integer>();
				sessionAttributes.put(SESSION_ATTRIBUTE.RESUME_PHRASES.toString(), allResumePhrases);
			}
			sessionAttributes.put(SESSION_ATTRIBUTE.RESUME_FILES.toString(), resumeFiles);

			// ----------------------------------------------------------------
			// Everything above this line seems to be crap
			// it was used to tell the user how many files are remaining -- no
			// longer needed

			// 1. set the number of threads in the pool equl to the number
			// of porcessors available on this machine
			ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

			// 2. put each resume in the pool to be processed
			numResumes = resumeFiles.size();
			System.out.println("Num of Resumes " + numResumes);
			for (File resume : resumeFiles) {
				ResumePhraseThread rpt = new ResumePhraseThread(resume);
				pool.execute(rpt);
			}
			try {
				// Wait till all the threads in the pool are finished
				pool.shutdown();
				pool.awaitTermination(10, TimeUnit.DAYS);
				System.out.println("Threads completed");

			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// 0.3 record the time it took. the timing stuff can be commented out.
		long end = System.currentTimeMillis();
		sessionAttributes.put(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(), 0); // was
																						// used
																						// to
																						// tell
																						// the
																						// user
																						// how
																						// many
																						// files
																						// are
																						// remaining
																						// --
																						// no
																						// longer
																						// needed
		logger.info("Time to process " + numResumes + " resumes :" + (end - start) / numResumes + " msecs/resume");
		try {
			// Create file
			String filename = jobId + ".txt";
			FileWriter fstream = new FileWriter(filename);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write("Time to process\t" + numResumes + "\tresumes:\t" + (end - start) / numResumes + "\t msecs/resume"
					+ "\t Start Time " + start + "\t End Time" + end);
			// Close the output stream
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	public class ResumePhraseThread implements Runnable {

		File resume;

		public ResumePhraseThread(File resume) {
			this.resume = resume;
		}

		public void run() {
			WordNetDataProviderPool.getInstance().acquireProvider();
			try {
				// 1. get the strings in all_resumes and their count and put
				// them in a hash map to be sent back on memcached
				Map<String, Integer> allResumePhrases = (Map<String, Integer>) sessionAttributes
						.get(SESSION_ATTRIBUTE.RESUME_PHRASES.toString()); // ??
				DocumentRS resumeDoc = null;
				File serializedResume = new File(resume.getPath() + ".Object");
				// fetch the resumeDoc from memcached
				/* PJM get the resumeObject from memcached */
				// 1. checks in memcached
				// 2. checks in Amazon S3 else process the resume
				// 3. if (resumeDoc == null) then process the resume
				// key = user's seeesion ID + resume name
				String resumeName = resume.getName();
				resumeName = resumeName.replace('-', 'A');
				resumeName = resumeName.replace(' ', 'A');
				String key = sessionAttributes.get("ID") + resumeName;

				/* PJM get the resumeObject from memcached *///
				resumeDoc = (DocumentRS) MemCacheClient.getInstance().get(key);// (DocumentRS)
																				// memCU.get(key);

				if (resumeDoc != null) {
					logger.info("Successfully! fetched the DocumentRS object from memcached");
				} // Check whether resume object exists in S3
				else if (serializedResume.exists()) {
					resumeDoc = (DocumentRS) FileUtils.deSerializeOject(new File(resume.getPath() + ".Object"));
					logger.info("Successfully! fetched the DocumentRS object from Amazon S3");
				} // Process the resume
				else {
					logger.info("ZipfileProcessingthread Couldnt! fetch the DocumentRS object from memcached");
					// Serialize Resume
					String serializedName = resume.getPath() + ".Object";
					DocumentProcessorRS resumeObject = null;
					// Create a DocumentProcessorRS object with
					// DefaultNLPSuite(), keyTokenIdentifier,
					// sourceTokenFilterer, 8
					try {
						resumeObject = new DocumentProcessorRS(new DefaultNLPSuite(), keyTokenIdentifier,
								sourceTokenFilterer, 8);
					} catch (Exception ex) {
						java.util.logging.Logger.getLogger(RemoteResumeProcessor.class.getName()).log(Level.SEVERE,
								null, ex);
						return;
					}
					// 1. Checks the resume file type
					// 1.0 Converts the PDF to HTML
					if (resume.getName().toLowerCase().endsWith(".pdf")) {
						String fileName = resume.getName();
						if (fileName.contains(" ")) {
							fileName = fileName.replace(' ', '_');
							File newFile = new File(resume.getParent(), fileName);
							resume.renameTo(newFile);
							resume = newFile;
						}
						if (fileName.lastIndexOf(".") != -1) {
							fileName = ResumeSorter.CONVERTED_PREFIX + fileName.substring(0, fileName.lastIndexOf("."));
							fileName = fileName + ".html";
							System.out.println("New file name:" + fileName);
						}
						try {
							resume = new PDFtoHTML().convert(resume, new File(resume.getParent(), fileName));
						} catch (Exception x) {
							logger.error("Error converting resume to pdf file:", x);
							String error = (String) sessionAttributes
									.get(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());
							if (error == null) {
								error = "";
							}
							error = error + "\nError converting pdf to html, resume : " + resume.getName();
							sessionAttributes.put(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString(), error);
							int numleft = (Integer) sessionAttributes
									.get(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
							sessionAttributes.put(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(),
									new Integer(--numleft));
							return;
							// throw new Exception("There was was an error
							// processing your .docx file. Please convert it to
							// .doc and retry.");
						}
					}
					// 1 Process the resume
					// 1.0 process method takes File resume, boolean
					// ignoreHeader = false, boolean clean = false
					// 2 Serialize the resume object and save it on S3
					// 3 Put the resume object in Memcached
					try {
						// ArrayList<Sentence> nonLemmaSentences =
						// sentenceDetector.split(resume); // get non lemmatized
						// sentences from the resume
						logger.debug("Processing Resume file calling sentecnceDetector's split method twice");
						resumeObject.process(resume, false, true);
						// resumeObject.setSentences(nonLemmaSentences);
						resumeDoc = resumeObject.getDocument();
						FileUtils.serializeOject(resumeDoc, new File(serializedName));
						/* PJM put the resumeObject into memcached */
						// The key should contain only string. Format the
						// resumeName so that it contains only String
						// Key = sessionId + resume name
						resumeName = resume.getName();
						resumeName = resumeName.replace('-', 'A');
						resumeName = resumeName.replace(' ', 'A');
						key = (String) sessionAttributes.get("ID");
						key = key + resumeName;
						logger.info("Adding the doc to memcached Key of the doc " + key);
						MemCacheClient.getInstance().set(key, 3600, resumeDoc);
						// PJM
					} catch (Exception x) {
						x.printStackTrace();
						logger.error("Error processing resume:" + resume.getName(), x);
						String error = (String) sessionAttributes
								.get(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());
						if (error == null) {
							error = "";
						}
						error = error + "\nError processing resume : " + resume.getName();
						sessionAttributes.put(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString(), error);
						int numleft = (Integer) sessionAttributes
								.get(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
						sessionAttributes.put(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(),
								new Integer(--numleft));
						return;
					}
				}
				// 1. Add the resume phrases to allResumePhrases
				// 1.1 Get the resume phrases from resumeDoc object
				// 1.2 Get the Collection<PhraseList> lemmatized phrases of the
				// resumePhrases
				// 1.3 Loop through collections of lemmaPhrases list
				// 1.3.1 Get the first phrase from the lemmaPhrases list
				// 1.3.2 Ignore the phrase that 1. starts with Digit 2 non
				// letter character 3. phrase exists in DeletedPhrases
				// 1.3.3 if the phrase contains in allResumePhrase then
				// increment the count and put the phrase in allResumePhrase
				// else put the phrase in allResumePhrase
				// 2. Delete the duplicate phrases from allResumePhrases
				// 2.1 loop through allResumePhrase and check for duplicates
				// 2.2 if duplicate found then put that phrase in removeList
				logger.debug("Sorting Resume Phrases");
				synchronized (allResumePhrases) {
					Collection<PhraseList> resumePhrases = resumeDoc.getDocumentPhrases();
					Collection<PhraseList> lemmaPhrases = ResumeSorter.getLemmatizedPhrases(resumePhrases, resumeDoc); // Get
																														// lemmatized
																														// phrases
					for (PhraseList pl : lemmaPhrases) {
						String phrase = pl.getFirst().getBuffer().trim();
						if (Character.isDigit(phrase.charAt(0))) {
							continue;
						}
						if (!Character.isLetter(phrase.charAt(0))) {
							continue;
						}
						try {
							if (DeletedPhrase.isDeleted(phrase)) {
								continue;
							}
						} catch (Exception ex) {
							java.util.logging.Logger.getLogger(RemoteResumeProcessor.class.getName()).log(Level.SEVERE,
									null, ex);
						}
						phrase = phrase.replace('\n', ' ').replace('\r', ' ');
						if (allResumePhrases.containsKey(phrase)) {
							Integer count = allResumePhrases.get(phrase);
							allResumePhrases.put(phrase, count + pl.size());
						} else {
							allResumePhrases.put(phrase, pl.size());
						}
					}

					// removed any capitalized word that is in there already as
					// a lower case version
					List<String> removeList = new ArrayList<String>();
					for (String phrase : allResumePhrases.keySet()) {
						if (Character.isUpperCase(phrase.charAt(0))) {
							String lower = phrase.toLowerCase();
							if (allResumePhrases.containsKey(lower)) {
								int count = allResumePhrases.get(phrase);
								int countl = allResumePhrases.get(lower);
								allResumePhrases.put(lower, count + countl);
								removeList.add(phrase);
							}
						}
					}
					for (String phrase : removeList) {
						allResumePhrases.remove(phrase);
					}

				} // synchronized (allResumePhrases) {

				int numleft = (Integer) sessionAttributes.get(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
				sessionAttributes.put(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(), new Integer(--numleft));
				/* PJM put the allResumePhrases into memcached */
				key = (String) sessionAttributes.get("ID");
				key = key + "Phrases" + jobId;
				logger.info("The phrases key: " + key);
				MemCacheClient.getInstance().set(key, 3600, allResumePhrases);
			} finally {
				WordNetDataProviderPool.getInstance().releaseProvider();
			}
		}
	}
}
