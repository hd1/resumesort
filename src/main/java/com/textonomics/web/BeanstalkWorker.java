/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

/**
 *
 * @author Preeti Mudda
 */
import com.textonomics.PlatformProperties;
import dk.safl.beanstemc.Beanstemc;
import dk.safl.beanstemc.BeanstemcException;
import dk.safl.beanstemc.Job;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BeanstalkWorker {
	private Beanstemc beanStem;
	private Job myJob;
	private String msg_w = "rwork";
	private int mainServer_port;// setting the port in properties = 11300;
	private String mainServerID;// setting the port in properties =
								// "10.124.215.66";

	public BeanstalkWorker() throws UnknownHostException, IOException {
		mainServerID = PlatformProperties.getInstance().getProperty("com.textonomics.mainServer_IP");
		mainServer_port = Integer
				.parseInt(PlatformProperties.getInstance().getProperty("com.textonomics.mainServer_BeanstalkdPort"));
		beanStem = new Beanstemc(mainServerID, mainServer_port);// main server
																// ip, and its
																// port
	}

	/**
	 * reads the job from given a BeanStockD tube -- called rworker. It also
	 * sends a message back to the client
	 * 
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws BeanstemcException
	 */
	public void readJob() throws IOException {
		System.out.println("In the read Job");
		try {
			beanStem.watch(msg_w); // 1. watch the beanStalkd tube
		} catch (BeanstemcException ex) {
			Logger.getLogger(BeanstalkWorker.class.getName()).log(Level.SEVERE, null, ex);
		}
		while (true) {
			System.out.println("In the read Job");
			try {
				System.out.println(beanStem.statsTube(msg_w));
				myJob = beanStem.reserve(); // 2. if job is ready, reserve it so
											// no one else gets it, otherwise
											// wait

				// 3.0 get the resumes -- MessageObject -- from BeanStalkd
				long jobId = myJob.getId();
				System.out.println("In the read Job ID " + beanStem.statsJob(jobId));
				MessageObject zipObj = (MessageObject) MessageObject.deserialize(myJob.getData());

				// 4. start a RemoteResumeProcessor thread and then block till
				// the resumes are done
				if (zipObj.getListOfFiles() != null) {
					System.out.println("The Session Id :" + zipObj.getSessionAtt().get("ID"));
					System.out.println("Calling RemoteResumeProcessor now");

					// currently zipObj.getSessionAtt() is a hashmap, whose key
					// is "ID" and its value is the user's session ID
					RemoteResumeProcessorThread resumeProcess = new RemoteResumeProcessorThread(zipObj.getSessionAtt(),
							zipObj.getListOfFiles(), jobId);
					resumeProcess.run(); // 4.1 blocks till all the resumes are
											// processed -- DOES THIS HAVE TO BE
											// A THREAD
					System.out.println("Processed a job now " + beanStem.statsTube(msg_w));
				} else {
					System.out.println("Not able to deserialize the object ");
				}
				beanStem.delete(myJob); // 5. delte the job from BeanStalkd

				System.out.println("In the read Job 3");
			} catch (IOException ex) {
				Logger.getLogger(BeanstalkWorker.class.getName()).log(Level.SEVERE, null, ex);
				continue;
			} catch (ClassNotFoundException ex) {
				Logger.getLogger(BeanstalkWorker.class.getName()).log(Level.SEVERE, null, ex);
				continue;
			} catch (BeanstemcException ex) {
				Logger.getLogger(BeanstalkWorker.class.getName()).log(Level.SEVERE, null, ex);
				continue;
			}
			System.out.println("In the read Job end");
		} // while
	}
}
