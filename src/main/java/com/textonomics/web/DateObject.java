/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import org.joda.time.LocalDate;

/**
 *
 * @author Preeti Mudda
 */
public class DateObject {
	private LocalDate startDate;
	private LocalDate endDate;
	private int lineNum;
	private int endOfParagrapgh;
	private boolean current;
	private String originalLine;
	private String lengthOfTime;
	private LengthObject lenOfTime;
	private LengthObject gapBetweenPeriods;

	public DateObject(LocalDate d1, LocalDate d2, int lnum, String oLine, String yrs, boolean curr, int years,
			int months) {
		startDate = d1;
		endDate = d2;
		lineNum = lnum;
		originalLine = oLine;
		current = curr;
		lengthOfTime = yrs;
		lenOfTime = new LengthObject(years, months);
	}

	/**
	 *
	 * @return
	 */
	public LocalDate getStartDate() {
		return startDate;
	}

	/**
	 *
	 * @return
	 */
	public LocalDate getEndDate() {
		return endDate;
	}

	/**
	 *
	 * @return
	 */
	public int getLineNum() {
		return lineNum;
	}

	/**
	 *
	 * @return
	 */
	public boolean isCurrent() {
		return current;
	}

	/**
	 *
	 * @return
	 */
	public String getOriginalLine() {
		return originalLine;
	}

	/**
	 *
	 * @return
	 */
	public String getLenOfTime() {
		return lengthOfTime;
	}

	/**
	 *
	 * @param lNum
	 */
	public void setEndLine(int lNum) {
		this.endOfParagrapgh = lNum;
	}

	/**
	 *
	 * @return
	 */
	public int getEndLinNum() {
		return endOfParagrapgh;
	}

	/**
	 *
	 * @param pLineNum
	 * @return
	 */
	public boolean phraseExists(int pLineNum) {
		if (pLineNum >= lineNum && pLineNum < endOfParagrapgh) {
			return true;
		}
		return false;
	}

	/**
	 *
	 * @return
	 */
	public int getYears() {
		return lenOfTime.getYears();

	}

	/**
	 *
	 * @return
	 */
	public int getEndPharagraph() {
		return endOfParagrapgh;
	}

	public int getMonths() {
		return lenOfTime.getMonths();
	}

	public void setGapPeriods(int yrs, int mts) {
		// System.out.println("Yrs"+ yrs);
		gapBetweenPeriods = new LengthObject(yrs, mts);
	}

	public int getGapYears() {
		return gapBetweenPeriods.getYears();
	}

	public int getGapMonth() {
		return gapBetweenPeriods.getMonths();
	}
}
