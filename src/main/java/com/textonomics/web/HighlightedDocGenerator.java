/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textnomics.data.ResumeComments;
import com.textonomics.DocumentRS;
import com.textonomics.Phrase;
import com.textonomics.PhraseList;
import com.textonomics.PhrasePositionComparator;
import com.textonomics.PlatformProperties;
import com.textonomics.db.DBAccess;
import com.textonomics.openoffice.OODocumentConverter;
import com.textonomics.openoffice.OODocumentHighlighter;
import com.textonomics.openoffice.OODocumentInsertHtml;
import com.textonomics.openoffice.OOFILTER_TYPE;
import com.textonomics.user.User;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Vinod
 */
public class HighlightedDocGenerator {

	HttpSession session;

	File resumeFile;
	String originalResumeName;
	String uploadDir;

	Map<Phrase, Integer> matchedPhrases = new TreeMap<Phrase, Integer>(new PhrasePositionComparator());
	List<String> missingPhrases = new ArrayList<String>();

	List<File> highlightedFiles = new ArrayList<File>();
	List<File> highlightedHtmls = new ArrayList<File>();
	List<Map<Phrase, Integer>> matchedPhraseList = new ArrayList<Map<Phrase, Integer>>();
	List<List<String>> unmatchedPhraseList = new ArrayList<List<String>>();
	List<Integer> phrasesToBeMatchedImp = new ArrayList<Integer>();
	Map<String, String> phrasesSynonyms;
	Map<String, Set<String>> deletedSynonyms;
	int fileIndex;

	//
	// TODO: Neeed to get rid of this constructor - change wherever it is called
	// (jsps)
	//
	public HighlightedDocGenerator(HttpServletRequest request, int fileIndex) {
		try {
			this.session = request.getSession();
			uploadDir = (String) session.getAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString()); // the
																								// upload
																								// dir
																								// (for
																								// resumes)
																								// of
																								// the
																								// user
			matchedPhraseList = (List<Map<Phrase, Integer>>) session
					.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
			unmatchedPhraseList = (List<List<String>>) session
					.getAttribute(SESSION_ATTRIBUTE.UNMATCHED_PHRASES.toString());
			highlightedHtmls = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_HTMLRESUMES.toString());
			phrasesToBeMatchedImp = (List<Integer>) session
					.getAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED_IMPORTANCE.toString());
			phrasesSynonyms = (Map<String, String>) session
					.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());
			deletedSynonyms = (Map<String, Set<String>>) session
					.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_DELETED_SYNONYMS.toString());
			List<File> resumeFiles = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
			resumeFile = resumeFiles.get(fileIndex);
			System.out.println("Resume " + resumeFile.getName() + " fileIndex:" + fileIndex);
			this.fileIndex = fileIndex;
		} catch (Exception ex) {
			ex.printStackTrace();
			Logger.getLogger(DocumentAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public HighlightedDocGenerator(HttpSession session, int fileIndex) {
		try {
			this.session = session;
			uploadDir = (String) session.getAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString()); // the
																								// upload
																								// dir
																								// (for
																								// resumes)
																								// of
																								// the
																								// user
			matchedPhraseList = (List<Map<Phrase, Integer>>) session
					.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
			unmatchedPhraseList = (List<List<String>>) session
					.getAttribute(SESSION_ATTRIBUTE.UNMATCHED_PHRASES.toString());
			highlightedHtmls = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_HTMLRESUMES.toString());
			phrasesToBeMatchedImp = (List<Integer>) session
					.getAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED_IMPORTANCE.toString());
			phrasesSynonyms = (Map<String, String>) session
					.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());
			deletedSynonyms = (Map<String, Set<String>>) session
					.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_DELETED_SYNONYMS.toString());
			List<File> resumeFiles = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
			resumeFile = resumeFiles.get(fileIndex);
			System.out.println("Resume " + resumeFile.getName() + " fileIndex:" + fileIndex);
			this.fileIndex = fileIndex;
		} catch (Exception ex) {
			ex.printStackTrace();
			Logger.getLogger(DocumentAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public File processResume() {
		try {
			WordNetDataProviderPool.getInstance().acquireProvider();

			// Highlighting
			try {
				originalResumeName = resumeFile.getName();
				if (resumeFile.getName().toLowerCase().endsWith(".docx")) {
					String newName = ResumeSorter.CONVERTED_PREFIX
							+ resumeFile.getName().substring(0, resumeFile.getName().length() - 1);
					resumeFile = new OODocumentConverter().convertToDoc(resumeFile,
							new File(resumeFile.getParent(), newName));
				}
				if (resumeFile.getName().toLowerCase().endsWith(".pdf")) {
					String fileName = resumeFile.getName();
					if (fileName.lastIndexOf(".") != -1) {
						fileName = ResumeSorter.CONVERTED_PREFIX + fileName.substring(0, fileName.lastIndexOf("."));
						fileName = fileName + ".html";
						System.out.println("New file name:" + fileName);
					}
					resumeFile = new File(resumeFile.getParent(), fileName);
				}
				matchedPhrases = matchedPhraseList.get(fileIndex);
				missingPhrases = unmatchedPhraseList.get(fileIndex);
				Boolean useStars = (Boolean) session.getAttribute(SESSION_ATTRIBUTE.USE_STARS.toString());
				if (useStars == null)
					useStars = true;
				OODocumentHighlighter highlighter = new OODocumentHighlighter(resumeFile, matchedPhrases, useStars);
				// Change plain text to doc output
				String outName = resumeFile.getName();
				if (outName.toLowerCase().endsWith(".txt"))
					outName = outName.substring(0, outName.lastIndexOf(".")) + ".doc";
				File highLightedDocFile = PlatformProperties.getInstance().getResourceFile(uploadDir + File.separator
						+ "HIGHLIGHTED_" + outName.replace(ResumeSorter.CONVERTED_PREFIX, ""));
				File highlightedHtml = new File(
						ResumeCakeWalkConfigurator.TEMP_DIR + "/" + highLightedDocFile.getName() + ".html");
				// logger.info("Highlighting resume:" +
				// highLightedDocFile.getAbsolutePath());

				highlighter.process(highLightedDocFile, getExtension(highLightedDocFile.getName()));
				List<String> userSelectedPhrases = (List<String>) session
						.getAttribute(SESSION_ATTRIBUTE.SELECTED_PHRASES.toString());
				List<String> userEnteredPhrases = (List<String>) session
						.getAttribute(SESSION_ATTRIBUTE.ENTERED_PHRASES.toString());
				List<Boolean> requiredPhrase = (List<Boolean>) session
						.getAttribute(SESSION_ATTRIBUTE.REQUIRED_PHRASES.toString());
				Set<String> tags = (Set<String>) session.getAttribute(SESSION_ATTRIBUTE.TAGS.toString());
				Map<String, Integer> phraseImportance = (Map<String, Integer>) session
						.getAttribute(SESSION_ATTRIBUTE.PHRASE_IMPORTANCE.toString());
				List<Double> scores = (List<Double>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());

				DocumentRS sourceDocument = (DocumentRS) session
						.getAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString());
				Collection<PhraseList> unlemmaPhrases = (Collection<PhraseList>) session
						.getAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString());
				Double score = null;
				// if (scores != null && scores.size() > fileIndex)
				// score = scores.get(fileIndex);
				String resumeReport = new ResumeReport(sourceDocument, unlemmaPhrases, matchedPhrases.keySet(),
						missingPhrases, userSelectedPhrases, phraseImportance, requiredPhrase, userEnteredPhrases, tags,
						useStars).generateReport(true, score);
				String resumeName = resumeFile.getName();
				if (resumeName.startsWith(ResumeSorter.CONVERTED_PREFIX))
					resumeName = resumeName.substring(ResumeSorter.CONVERTED_PREFIX.length());
				if (resumeName.indexOf("_") != -1)
					resumeName = resumeName.substring(resumeName.indexOf("_") + 1);
				User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
				String sql = "resume_file = " + DBAccess.toSQL(originalResumeName) + " and user_name = "
						+ DBAccess.toSQL(user.getLogin());
				File resumeFolder = (File) session.getAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString());
				String resumeFolderName = null;
				if (resumeFolder != null) {
					resumeFolderName = resumeFolder.getName();
					sql = sql + " and (resume_folder = " + DBAccess.toSQL(resumeFolderName)
							+ " or resume_folder is null)";
				}
				List<ResumeComments> history = new ResumeComments().getObjects(sql);
				String comments = "No Comments Entered";
				if (history.size() > 0)
					comments = history.get(0).getComments();
				comments = comments.replace("\n", "<br>");
				resumeReport = resumeReport.replace(ResumeReport.RESUME_COMMENTS, comments);

				File htmlReport = new File(highlightedHtml.getParent(), highLightedDocFile.getName() + ".report.html");
				Writer reportWriter = new BufferedWriter(new FileWriter(htmlReport));
				reportWriter.write(resumeReport + "\n");
				String coverLetter = ResultHTMLGenerator.getCoverLetterHtml(resumeFile);
				if (coverLetter.length() > 0) {
					reportWriter.write("<p style='page-break-before: always'>.</p>\n");
					reportWriter.write(coverLetter);
					reportWriter.write("\n<p style='page-break-before: always'>.</p>\n");
				}
				reportWriter.close();
				String fileName = highLightedDocFile.getName();
				int dot = fileName.lastIndexOf(".");
				fileName = fileName.substring(0, dot) + "_Report" + fileName.substring(dot);
				File resultDoc = new File(highLightedDocFile.getParent(), fileName);
				resultDoc = new OODocumentInsertHtml(highLightedDocFile, htmlReport, resultDoc)
						.process(getExtension(highLightedDocFile.getName()));
				if (resultDoc.getName().endsWith(".html")) {
					resultDoc = new OODocumentConverter().convertToDoc(resultDoc);
				}
				highLightedDocFile.delete();
				return resultDoc;
			} catch (Exception x) {
				System.out.println("Error in open office highlighter:" + x);
				x.printStackTrace();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			WordNetDataProviderPool.getInstance().releaseProvider();
		}
		return null;
	}

	/**
	 *
	 * @param name
	 *            the name of the file
	 * @return an enumeration type that represents the type of the file
	 */
	private OOFILTER_TYPE getExtension(String name) {
		String extension;
		StringBuilder builder = new StringBuilder();
		for (int i = name.length() - 1; i >= 0; i--) {
			if (name.charAt(i) == '.') {
				break;
			}
			builder.append(name.charAt(i));
		}

		extension = builder.reverse().toString().toLowerCase().trim();

		if (extension.equals("doc"))
			return OOFILTER_TYPE.MS_WORD_97;

		if (extension.equals("docx"))
			return OOFILTER_TYPE.MS_Word_2003_XML;

		if (extension.equals("odt"))
			return OOFILTER_TYPE.ODT;

		if (extension.equals("rtf"))
			return OOFILTER_TYPE.Rich_Text_Format;

		if (extension.equals("html"))
			return OOFILTER_TYPE.HTML;

		if (extension.equals("txt"))
			return OOFILTER_TYPE.Plain_Text;

		return null;

	}
}
