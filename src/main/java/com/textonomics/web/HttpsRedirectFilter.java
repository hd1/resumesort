/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textonomics.PlatformProperties;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dev Gude
 */
public class HttpsRedirectFilter implements Filter {
	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String useHttps = PlatformProperties.getInstance().getProperty("com.textnomics.use_https");
		if ((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse)) {
			String requestUrl = ((HttpServletRequest) request).getRequestURL().toString();
			String queryString = ((HttpServletRequest) request).getQueryString();
			if (queryString == null)
				queryString = "code=free_registration";
			if ("true".equals(useHttps) && !request.isSecure()) {
				String redirectTarget = requestUrl.replaceFirst("http:", "https:");
				if (redirectTarget.contains("69.50.255.101"))
					redirectTarget = "https://www.resumesort.com?" + queryString;
				else if (requestUrl.indexOf("www") == -1)
					redirectTarget = requestUrl.replaceFirst("http://", "https://www.");
				((HttpServletResponse) response).sendRedirect(redirectTarget);
			} else {
				chain.doFilter(request, response);
			}
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
}
