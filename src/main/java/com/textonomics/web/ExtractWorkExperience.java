/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.textonomics.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.star.util.DateTime;

/**
 *
 * @author Preeti Mudda
 */
public class ExtractWorkExperience {
	// date pattern 1: ex. 2000-2003 or 2000 - 2003 or 2010-Present
	// private static String pattern1 =
	// "\\d{4}\\p{Space}*+\\p{Punct}\\p{Space}*+\\d{4}|w{7}?";
	private static String pattern5 = "\\d{4}\\p{Space}\\p{Punct}\\p{Alpha}{7}";
	private static String pattern0 = "\\d{4}\\p{Space}\\p{Punct}\\d{4}";
	// pattern 2: ex. June, 2008 – Dec, 2010 or March 2008 - Feb 2009 or March
	// 2010 - current
	private static String pattern2 = "\\w{3,}?\\p{Punct}*\\p{Space}*\\d{4}\\p{Space}*\\p{Punct}\\p{Space}*\\w{3,}?\\p{Punct}*\\p{Space}*\\d{0,}?";
	private static String pattern2a = "\\[a-zA-Z]{3,8}\\p{Space}\\d{4}\\p{Space}\\[a-zA-Z]{3,8}\\p{Space}\\d{0,4}";
	// pattern 3: ex.08/09-12/11 or 08/2000-09/2002
	private static String pattern3 = "\\d{2}\\p{Punct}\\d{2,4}?\\p{Space}*\\p{Punct}\\d{2}\\p{Punct}\\d{2,4}?";
	private static String pattern3a = "\\d{2}\\p{Space}\\d{2,4}\\p{Space}\\d{2}\\p{Space}\\d{2,4}";
	// pattern 4: ex. 2000 to Present or 2000 to 2001
	private static String pattern4 = "\\d{4}\\p{Space}\\p{Alpha}{2}\\p{Space}\\d{4}";
	private static String pattern4a = "\\d{4}\\p{Space}\\p{Alpha}{2}\\p{Space}\\p{Alpha}{7}";
	private static String[] patterns = { pattern2a, pattern2, pattern3, pattern3a, pattern4, pattern0, pattern5,
			pattern4a };
	static int[] months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
	private HashMap<String, ArrayList<String>> hashMapOfExperience = new HashMap<String, ArrayList<String>>();
	private HashMap<Integer, String> hashMapOfDates = new HashMap<Integer, String>();
	private HashMap<String, PhraseCountObject> hashMapOfPhrasesExp = new HashMap<String, PhraseCountObject>();

	public ExtractWorkExperience() {
	}

	public ArrayList<String> getLines(String line) {
		// System.out.println(line);
		// System.out.println("--------------------------------------->");
		ArrayList<String> result = new ArrayList<String>();
		String[] lineTerminators = { "\r", "\n", ";" };
		int index = 0, temp = -1;
		int length = line.length();
		// int prevIndex =0;
		while (length > 0) {
			for (String del : lineTerminators) {
				temp = line.indexOf(del);
				if (index == 0 && temp > 0)
					index = temp;
				if (index > temp && temp > 0 && temp <= line.length())
					index = temp;
			}
			result.add(line.substring(0, index));
			System.out.println(line.substring(0, index));
			System.out.println("--------------------------------");
			index++;
			if (index >= length)
				break;
			line = line.substring(index);
			length = line.length();
			index = 0;
		}
		// if(listOfLines.length == 0){
		// result.add(line);
		// System.out.println(line);
		// System.out.println("--------------------------------");
		//
		// }
		// for(String l : listOfLines)
		// {
		// System.out.println(l);
		// System.out.println("--------------------------------");
		//
		//
		// result.add(l);
		// }
		return result;
	}

	/**
	 * Extracts the dates from the file. 1. loop through all the lines and check
	 * against all the patterns. if match found then extract that date.
	 * 
	 * @param pText
	 */
	public void extractDates(ArrayList<String> pText) {
		String b = null;
		String line = null;
		// 1. loop through all the lines
		for (int i = 0; i < pText.size(); i++) {
			// 1.0 check against all the patterns
			line = pText.get(i).trim().replaceAll("\\p{Punct}", " ").replaceAll("\t", " ").replaceFirst("-", " ");
			// System.out.println(i+"<<<<"+ line +">>>>>");
			b = matchAllPatterns(line);
			// b = matchAllPatterns(pText[i].trim().replaceAll("\\W\\S\\D", "
			// ").replaceAll("\t", " ").trim());
			// System.out.println("para " + i+":" + pText[i].trim());
			// 1.1 if match found then extract that date
			if (b != null) {
				// System.out.println(pText[i].trim().replaceAll("\\p{Punct}", "
				// ").replaceAll("\t", " ").trim()+"->");
				// 1.1.1 extract that date and store it into HashMap
				hashMapOfDates.put(i, b);
			}
		}
	}

	/**
	 * Extracts the dates from the file. 1. loop through all the lines and check
	 * against all the patterns. if match found then extract that date.
	 * 
	 * @param pText
	 */
	public void extractDatesOld(String[] pText) {
		String b = null;
		// 1. loop through all the lines
		for (int i = 0; i < pText.length; i++) {
			// 1.0 check against all the patterns
			// b = matchAllPatterns(pText[i].trim().replaceAll("\\p{Punct}", "
			// ").replaceAll("\t", " ").replaceFirst("-"," "));
			// b = matchAllPatterns(pText[i].trim().replaceAll("\\W\\S\\D", "
			// ").replaceAll("\t", " ").trim());
			// System.out.println("para " + i+":" + pText[i].trim());
			String[] lines = pText[i].split(".\n\r");
			System.out.println("para " + i + ":" + lines[0].trim());
			// 1.1 if match found then extract that date
			if (b != null) {
				// System.out.println(pText[i].trim().replaceAll("\\p{Punct}", "
				// ").replaceAll("\t", " ").trim()+"->");
				// 1.1.1 extract that date and store it into HashMap
				hashMapOfDates.put(i, b);
			}
		}
	}

	/**
	 * 1. Loop through the dates extract the lines found in-between two dates.
	 * Add the lines to an arraylist extract the keyphrases for each line and
	 * add that phrase and no. of years experiences to an hashmap. Add the lines
	 * to a HashMap <String, ArrayList<String>> where key = date and value =
	 * lines found from the date till next date.
	 * 
	 * @param pText
	 * @param phrases
	 */
	public void addExpData(ArrayList<String> pText, String[] phrases) {
		ArrayList<String> tempArryListLines = null;
		Set<Integer> lineNumSet = hashMapOfDates.keySet();
		// 1.0 Loop through the dates and extract the lines in between dates
		for (int num : lineNumSet) { // loop 1
			// System.out.println("Line "+num+": "+hashMapOfDates.get(num));
			// 1.1 create a tempArryListLines to store the extracted lines.
			tempArryListLines = new ArrayList<String>();
			// 1.2 if the line is too long or null then ignore
			if (hashMapOfDates.get(num).length() > 50 || hashMapOfDates.get(num) == null)
				continue;
			// 1.3 extract the lines from strating the num+1 till the next date.
			for (int i = (num + 1); i < pText.size(); i++) {// loop 2
				// 1.3.1 Check to see if the line number exists in linNumSet. if
				// exists then break
				if (lineNumSet.contains(i))
					break;
				// 1.3.2 Add the lines which contains text and call
				// addExpFoundPhrases to extract the phrases and for the given
				// date.
				else if (pText.get(i).length() > 1) {
					// 1.3.2.1 add the line
					tempArryListLines.add(pText.get(i));
					// System.out.println("The line number is "+ num+":
					// "+hashMapOfDates.get(num));
					// 1.3.2.2 call addExpFoundPhrases to extract the phrases
					// and for the given date.
					addExpFoundPhrases(pText.get(i), phrases, hashMapOfDates.get(num));
				}
			} // end loop2
				// 1.4 put the date and correspomding line in a
				// hashmapOfExperience
			hashMapOfExperience.put(hashMapOfDates.get(num), tempArryListLines);
		} // end loop1
	}

	/**
	 * 1. Loop through the dates extract the lines found in-between two dates.
	 * Add the lines to an arraylist extract the keyphrases for each line and
	 * add that phrase and no. of years experiences to an hashmap. Add the lines
	 * to a HashMap <String, ArrayList<String>> where key = date and value =
	 * lines found from the date till next date.
	 * 
	 * @param pText
	 * @param phrases
	 */
	public void addExpDataOld(String[] pText, String[] phrases) {
		ArrayList<String> tempArryListLines = null;
		Set<Integer> lineNumSet = hashMapOfDates.keySet();
		// 1.0 Loop through the dates and extract the lines in between dates
		for (int num : lineNumSet) { // loop 1
			// System.out.println("Line "+num+": "+hashMapOfDates.get(num));
			// 1.1 create a tempArryListLines to store the extracted lines.
			tempArryListLines = new ArrayList<String>();
			// 1.2 if the line is too long or null then ignore
			if (hashMapOfDates.get(num).length() > 50 || hashMapOfDates.get(num) == null)
				continue;
			// 1.3 extract the lines from strating the num+1 till the next date.
			for (int i = (num + 1); i < pText.length; i++) {// loop 2
				// 1.3.1 Check to see if the line number exists in linNumSet. if
				// exists then break
				if (lineNumSet.contains(i))
					break;
				// 1.3.2 Add the lines which contains text and call
				// addExpFoundPhrases to extract the phrases and for the given
				// date.
				else if (pText[i].length() > 1) {
					// 1.3.2.1 add the line
					tempArryListLines.add(pText[i]);
					// System.out.println("The line number is "+ num+":
					// "+hashMapOfDates.get(num));
					// 1.3.2.2 call addExpFoundPhrases to extract the phrases
					// and for the given date.
					addExpFoundPhrases(pText[i], phrases, hashMapOfDates.get(num));
				}
			} // end loop2
				// 1.4 put the date and correspomding line in a
				// hashmapOfExperience
			hashMapOfExperience.put(hashMapOfDates.get(num), tempArryListLines);
		} // end loop1
	}

	/**
	 * Extract the the years between the date and add the years to found
	 * phrases.
	 * 
	 * @param line
	 * @param phrases
	 * @param date
	 */
	public void addExpFoundPhrases(String line, String[] phrases, String date) {
		PhraseCountObject pco = null;
		boolean isRecent = false;
		boolean check = true;
		// 1.0 check if the date is recent else check if the date ends with a
		// digit
		if (date.toLowerCase().contains("current") || date.toLowerCase().contains("present")) {
			isRecent = true;
		} else if (!Character.isDigit(date.toLowerCase().charAt(date.length() - 1))) { // check
																						// if
																						// the
																						// date
																						// ends
																						// with
																						// a
																						// digit
			// System.out.println(date.toLowerCase());
			check = false;
		}
		// 1.1 if true then proceed to match phrases against the line.
		if (check) {
			// 1.2 get the years between the date.
			int yrs = getYearsExp(date, isRecent);
			if (yrs < 0)
				yrs = yrs * -1;
			System.out.println("The date is : " + date + " yrs:" + yrs);
			// 1.3 loop through the phrases and check agianst the line.
			for (String p : phrases) {
				// 1.3.1 if the line contains the phrases then create
				// phrasecountobject and put it into hashmapPfPhrasesExp.
				if (line.toLowerCase().contains(p.toLowerCase())) {
					// System.out.println(line + "->" + p);
					// 1.3.1.1 get the object from hashMapOfPhraseExp
					pco = hashMapOfPhrasesExp.get(p.toLowerCase());
					// 1.3.1.2 if pco is null then create the object and put the
					// objec back to hashmap
					if (pco == null) {
						pco = new PhraseCountObject(yrs, isRecent);
						pco.setYear(date);
						hashMapOfPhrasesExp.put(p.toLowerCase(), pco);
					}
					// 1.3.1.3 if pco exists.
					// the date is different then add the years to previous
					// years
					// set the status of pco if isRecent and the pco object
					// status is fasle
					else if (!pco.getYear().equalsIgnoreCase(date)) {
						pco.addCount(yrs);
						if (isRecent == true && !pco.isRecentExp()) {
							pco.setStatus(isRecent);
						}
						pco.setYear(date);
						hashMapOfPhrasesExp.put(p.toLowerCase(), pco);
					}
					System.out.println(
							"The phrase: " + p + " : " + pco.getCount() + " Recent: " + isRecent + " year" + date);
				}
			}
		}
	}

	/**
	 * returns the HashMap<String, PhraseCountObject>
	 * 
	 * @return
	 */
	public HashMap<String, PhraseCountObject> getExp() {
		return hashMapOfPhrasesExp;
	}

	/**
	 * for testing purpose only.
	 */
	public void display(String f) {
		System.out.println("The file is <<<<<<<" + f + ">>>>>>");
		PhraseCountObject tempObj = null;
		Set<String> phrases = hashMapOfPhrasesExp.keySet();
		for (String p : phrases) {
			tempObj = hashMapOfPhrasesExp.get(p);
			System.out.println(
					"phrase: " + p + "\tNo. Of Yrs exp: " + tempObj.getCount() + "\tRecent: " + tempObj.isRecentExp());
		}
	}

	// public void addDates(int lineNum, String line)
	// {
	// hashMapOfDates.put(lineNum, line);
	//// System.out.println("The line number is "+ lineNum+": "+line);
	// }
	/**
	 * Match the line against all the patterns if found extratc the date and
	 * return.
	 * 
	 * @param line
	 * @return
	 */
	public String matchAllPatterns(String line) {
		Pattern p = null;
		Matcher m = null;
		String matched = null;

		for (String pattern : patterns) {
			// System.out.println("The pattern is : "+ pattern);
			p = Pattern.compile(pattern);
			m = p.matcher(line.replaceFirst("-", " "));
			if (m.find()) {
				matched = m.group(0);
				System.out.println(matched + "<<<<" + pattern + ">>>>>" + line);
				// break;
			}
		}
		return matched;
	}

	/**
	 * Extract the years between two dates
	 * 
	 * @param date
	 * @param isRecent
	 * @return
	 */
	public int getYearsExp(String date, boolean isRecent) {
		// System.out.println("The date is " + date);
		int years = 0;
		SimpleDateFormat temp;
		Date parseDate1 = null, parseDate2 = null;
		// 2 words: 2010 - Present/Current.
		// 3 words: 2010 to 2011
		// 4 words: 10/09-11/10 or Mar 2001 - Jan 2005
		String[] tempDate = date.split(" ");
		int currentYear;
		if (tempDate.length == 2) {
			currentYear = getCurrentDate(tempDate[1], isRecent);
			years = currentYear - Integer.parseInt(tempDate[0]);
		} else if (tempDate.length == 3) {
			currentYear = getCurrentDate(tempDate[2], isRecent);
			years = currentYear - Integer.parseInt(tempDate[0]);
		} else if (tempDate.length == 4) {
			if (tempDate[1].length() == 2) {
				temp = new SimpleDateFormat("mm yy");
				try {
					parseDate1 = temp.parse(tempDate[0] + " " + tempDate[1]);
					parseDate2 = temp.parse(tempDate[2] + " " + tempDate[3]);
				} catch (ParseException ex) {
					Logger.getLogger(ExtractWorkExperience.class.getName()).log(Level.SEVERE, null, ex);
				}
				currentYear = parseDate2.getYear();
				years = currentYear - parseDate1.getYear();
			} else if (tempDate[1].length() == 4) {
				temp = new SimpleDateFormat("mm yyyy");
				try {
					parseDate1 = temp.parse(tempDate[0] + " " + tempDate[1]);
					parseDate2 = temp.parse(tempDate[2] + " " + tempDate[3]);
				} catch (ParseException ex) {
					Logger.getLogger(ExtractWorkExperience.class.getName()).log(Level.SEVERE, null, ex);
				}
				currentYear = parseDate2.getYear();
				years = currentYear - parseDate1.getYear();
			} else {
				currentYear = getCurrentDate(tempDate[3], isRecent);
				years = currentYear - Integer.parseInt(tempDate[2]);
			}
		}
		// System.out.println("The date : "+ date +" -- "+ years);
		return years;
	}

	/**
	 * return the current year or return the date
	 * 
	 * @param date
	 * @param isRecent
	 * @return
	 */
	public int getCurrentDate(String date, boolean isRecent) {
		int result;
		DateTime currentDate;
		short m = 1, d = 1, h = 0, s = 0;
		// System.out.println("The date is : "+ date);
		if (isRecent) {
			// currentDate = new DateTime();
			// System.out.println("The current year "+ currentDate.Year);
			result = Calendar.getInstance().get(Calendar.YEAR);
		} else {
			result = Integer.parseInt(date);
		}
		return result;
	}
}

class PhraseCountObject {
	private int count;
	private boolean isRecent;
	private String date;

	public PhraseCountObject(int c, boolean check) {
		count = c;
		isRecent = check;
	}

	public void setYear(String d) {
		date = d;
	}

	public String getYear() {
		return date;
	}

	public int getCount() {
		return count;
	}

	public boolean isRecentExp() {
		return isRecent;
	}

	public void addCount(int c) {
		count = count + c;
	}

	public void setStatus(boolean b) {
		isRecent = b;
	}
}