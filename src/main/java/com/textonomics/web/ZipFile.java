package com.textonomics.web;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 *
 * Used to create a zip file containing all the given files in the
 * ZipAndSendFiles method and then sends the zip file back to the client
 *
 * @author Rodrigo Neves
 *
 */

public class ZipFile {

	/**
	 * The compression level for zip file creation (0-9) 0 = No compression 1 =
	 * Standard compression (Very fast) ... 9 = Best compression (Very slow)
	 */
	private int compressionLevel;
	private HttpServletResponse response;

	/**
	 * constructor
	 * 
	 * @param compressionLevel
	 * @param response
	 */
	public ZipFile(int compressionLevel, HttpServletResponse response) {
		this.compressionLevel = compressionLevel;
		this.response = response;
	}

	public boolean ZipAndSendFiles(String[] fileNames, String dirPath, String zipName) {

		File dir_file = new File(dirPath);
		int dir_l = dir_file.getAbsolutePath().length();
		response.setContentType("application/zip");
		response.setHeader("Content-Disposition", "attachment;filename=\"" + zipName + ".zip\"");

		// ServletOutputStream out = response.getOutputStream();
		ServletOutputStream out;
		try {
			out = response.getOutputStream();
		} catch (IOException e1) {
			e1.printStackTrace();
			return false;
		}
		ZipOutputStream zipout = new ZipOutputStream(out);
		zipout.setLevel(this.compressionLevel);
		for (int i = 0; i < fileNames.length; i++) {
			File f = (File) new File(dirPath + fileNames[i]);
			if (f.canRead()) {
				// System.out.println("Can read:" + f.getAbsolutePath());
				try {
					zipout.putNextEntry(new ZipEntry(f.getAbsolutePath().substring(dir_l + 1)));
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
				BufferedInputStream fr;
				try {
					fr = new BufferedInputStream(new FileInputStream(f));

					byte buffer[] = new byte[0xffff];
					// copyStreamsWithoutClose(fr, zipout, buffer);
					int b;
					while ((b = fr.read(buffer)) != -1)
						zipout.write(buffer, 0, b);

					fr.close();
					zipout.closeEntry();

				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return false;
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
		}

		try {
			zipout.finish();
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;

	}

}
