package com.textonomics.web;

import com.textonomics.mail.PasswordMailer;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderException;
import com.textonomics.user.UserDataProviderPool;
import org.apache.log4j.Logger;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This servlet is used to send a new password to the user.
 *
 */

public class SendPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(SendPassword.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SendPassword() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User user;
		try {
			UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();
			logger.info("Resetting password for " + request.getParameter("login"));
			if (request.getParameter("login") != null && !request.getParameter("login").equals("")) {
				user = provider.getUser(request.getParameter("login").trim());
				if (user == null) {
					request.getRequestDispatcher("/signin.jsp?error=Invalid email address");
					return;
				}
				String newPassword = generatePassword();
				user.setPassword(newPassword);
				provider.updateUser(user);
				provider.commit();
				logger.info("Sending new password to " + user.getLogin());
				PasswordMailer.getInstance().sendMail(user);
				request.getRequestDispatcher("/SentPassword.jsp?email=" + user.getEmail()).forward(request, response);
			}
		} catch (UserDataProviderException ex) {
			throw new ServletException(ex);
		} finally {
			UserDataProviderPool.getInstance().releaseProvider();// Release
																	// it!!!!!!!!
		}
	}

	/** Minimum length for a decent password */
	public static final int MIN_LENGTH = 10;

	/** The random number generator. */
	protected static java.util.Random r = new java.util.Random();

	/*
	 * Set of characters that is valid. Must be printable, memorable, and "won't
	 * break HTML" (i.e., not ' <', '>', '&', '=', ...). or break shell commands
	 * (i.e., not ' <', '>', '$', '!', ...). I, L and O are good to leave out,
	 * as are numeric zero and one.
	 */
	protected static char[] goodChar = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's',
			't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'Q',
			'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '8', '9', '+', '-', '@', };

	/* Generate a Password object with a random password. */
	public static String generatePassword() {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < MIN_LENGTH; i++) {
			sb.append(goodChar[r.nextInt(goodChar.length)]);
		}
		return sb.toString();
	}

}
