package com.textonomics.web;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Servlet that is used to create a file indicating that a certain submission
 * has been verified in the analyser interface (see Analyser.jsp). The Analyser
 * interface was created to be used by Erin
 * 
 * @author Rodrigo Neves
 */
public class CheckSubmission extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CheckSubmission() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String resume = request.getParameter("resume");
		String name = request.getParameter("name");
		String path = request.getParameter("path");
		HttpSession session = request.getSession();
		Hashtable<String, String> checked = (Hashtable<String, String>) session.getAttribute("checked");
		File file = new File(path + "IS_CHECKED.txt");

		BufferedWriter buffer = new BufferedWriter(new FileWriter(file, true));

		buffer.write(resume + "\n");
		buffer.close();
		checked.put(resume, "");
		session.setAttribute("checked", checked);

		response.sendRedirect("Analyser.jsp?name=" + name + "&resume=" + resume);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
