/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

/**
 *
 * @author Preeti Mudda
 */
public class LengthObject {
	private int years;
	private int months;

	public LengthObject(int y, int m) {
		years = y;
		months = m;
	}

	public void addYears(int yrs) {
		years += yrs;
	}

	public void addMonths(int mths) {
		months += mths;
	}

	public int getYears() {
		return years;
	}

	public int getMonths() {
		return months;
	}
}