/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textonomics.PlatformProperties;
import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderPool;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet for files form JUpload Applet
 */
public class MultiFileUpload extends HttpServlet {
	static Logger logger = Logger.getLogger(MultiFileUpload.class);

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", -1);
		PrintWriter out = response.getWriter();
		File resumeDirectory = null;
		HttpSession session = request.getSession();
		try {
			WordNetDataProviderPool.getInstance().acquireProvider();
			UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();

			User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			PlatformProperties.getInstance().loggedInUser.set(user); // Set
																		// currrent
																		// user
			String uploadDir = PlatformProperties.getInstance().getResourceFile(getUploadDir(user)).getAbsolutePath()
					+ "/";
			// Initialization for chunk management.
			boolean bLastChunk = false;
			int numChunk = 0;

			// CAN BE OVERRIDEN BY THE postURL PARAMETER: if error=true is
			// passed as a parameter on the URL
			boolean generateError = false;
			boolean generateWarning = false;
			boolean sendRequest = false;
			// Get URL Parameters.
			Enumeration paraNames = request.getParameterNames();
			String pname;
			String pvalue;
			boolean maxFilesExceeded = false;
			while (paraNames.hasMoreElements()) {
				pname = (String) paraNames.nextElement();
				pvalue = request.getParameter(pname);
				if (pname.equals("jufinal")) {
					bLastChunk = pvalue.equals("1");
				} else if (pname.equals("jupart")) {
					numChunk = Integer.parseInt(pvalue);
				}

				// For debug convenience, putting error=true as a URL parameter,
				// will generate an error
				// in this response.
				if (pname.equals("error") && pvalue.equals("true")) {
					generateError = true;
				}

				// For debug convenience, putting warning=true as a URL
				// parameter, will generate a warning
				// in this response.
				if (pname.equals("warning") && pvalue.equals("true")) {
					generateWarning = true;
				}

				// For debug convenience, putting readRequest=true as a URL
				// parameter, will send back the request content
				// into the response of this page.
				if (pname.equals("sendRequest") && pvalue.equals("true")) {
					sendRequest = true;
				}
			}

			int ourMaxMemorySize = 10000000;
			int ourMaxRequestSize = 20000000;
			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();

			// Set factory constraints
			factory.setSizeThreshold(ourMaxMemorySize);
			factory.setRepository(new File(uploadDir));

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);

			// Set overall request size constraint
			upload.setSizeMax(ourMaxRequestSize);
			logger.debug("request content type:" + request.getContentType());

			String folderName = "";
			// Parse the request
			if (sendRequest) {
				// For debug only. Should be removed for production systems.
				// logger.debug("sendRequest:
				// ===========================================================================");
				// logger.debug("Sending the received request content: ");
				InputStream is = request.getInputStream();
				int c;
				while ((c = is.read()) >= 0) {
					logger.debug(c);
				} // while
				is.close();
				// logger.debug("===========================================================================");
			} else if (request.getContentType() == null
					|| !request.getContentType().startsWith("multipart/form-data")) {
				// logger.debug("form/data: No parsing of uploaded file: content
				// type is " + request.getContentType());
			} else if (request.getContentType() != null && request.getContentType().startsWith("multipart/form-data")) {
				List /* FileItem */ items = upload.parseRequest(request);
				// Process the uploaded items
				Iterator iter = items.iterator();
				FileItem fileItem;
				File fout;
				while (iter.hasNext()) {
					fileItem = (FileItem) iter.next();

					if (fileItem.isFormField()) {
						logger.debug("(form field) " + fileItem.getFieldName() + " = " + fileItem.getString());

						// If we receive the md5sum parameter, we've read
						// finished to read the current file. It's not
						// a very good (end of file) signal. Will be better in
						// the future ... probably !
						// Let's put a separator, to make output easier to read.
						if (fileItem.getFieldName().equals("md5sum[]")) {
							// logger.debug("------------------------------ ");
						}
						if (fileItem.getFieldName().equals("ResumeFolder")) {
							logger.info("Resume Folder:" + fileItem.getString());
							folderName = fileItem.getString();
							File dir = new File(uploadDir, ResumeSorter.RESUME_FOLDER + "/" + folderName);
							if (!dir.exists())
								dir.mkdirs();
							session.setAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString(), dir);
							resumeDirectory = dir;
						}
					} else {
						// Ok, we've got a file. Let's process it.
						// Again, for all informations of what is exactly a
						// FileItem, please
						// have a look to
						// http://jakarta.apache.org/commons/fileupload/
						//
						logger.debug("FieldName: " + fileItem.getFieldName());
						logger.debug("File Name: " + fileItem.getName());
						logger.debug("ContentType: " + fileItem.getContentType());
						logger.debug("Size (Bytes): " + fileItem.getSize());
						// If we are in chunk mode, we add ".partN" at the end
						// of the file, where N is the chunk number.
						String uploadedFilename = fileItem.getName() + (numChunk > 0 ? ".part" + numChunk : "");
						fout = new File(uploadDir + ResumeSorter.RESUME_FOLDER + "/" + folderName + "/"
								+ (new File(uploadedFilename)).getName());
						logger.debug("File Out: " + fout.toString());
						long oldsize = -1;
						if (fout.exists()) {
							oldsize = fout.length();
						}
						// write the file
						fileItem.write(fout);
						// If new file size different from old file size delete
						// the parsed file
						if (oldsize != -1 && oldsize != fout.length()) {
							File parsedFile = new File(fout.getParent(), fout.getName() + ".Object");
							if (parsedFile.exists())
								parsedFile.delete();
							parsedFile = new File(fout.getParent(), fout.getName() + ".tikaObject");
							if (parsedFile.exists())
								parsedFile.delete();

						}
						List<File> resumeFiles = (List<File>) session
								.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
						if (resumeFiles == null) {
							resumeFiles = ProcessResumes.getResumeFiles(resumeDirectory);
						}
						if (fout.getName().toLowerCase().endsWith(".zip")) {
							File outdir = (File) session.getAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString());
							FileUtils.openZipFile(fout, outdir.getAbsolutePath());
							resumeFiles = ProcessResumes.getResumeFiles(resumeDirectory);
							fout.delete();
						} else if (!resumeFiles.contains(fout))
							resumeFiles.add(fout);
						session.setAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString(), resumeFiles);
						//////////////////////////////////////////////////////////////////////////////////////
						// Chunk management: if it was the last chunk, let's
						////////////////////////////////////////////////////////////////////////////////////// recover
						////////////////////////////////////////////////////////////////////////////////////// the
						////////////////////////////////////////////////////////////////////////////////////// complete
						////////////////////////////////////////////////////////////////////////////////////// file
						// by concatenating all chunk parts.
						//
						if (bLastChunk) {
							logger.debug(" Last chunk received: let's rebuild the complete file (" + fileItem.getName()
									+ ")");
							// First: construct the final filename.
							FileInputStream fis;
							FileOutputStream fos = new FileOutputStream(uploadDir + fileItem.getName());
							int nbBytes;
							byte[] byteBuff = new byte[1024];
							String filename;
							for (int i = 1; i <= numChunk; i += 1) {
								filename = fileItem.getName() + ".part" + i;
								logger.debug("" + "  Concatenating " + filename);
								fis = new FileInputStream(uploadDir + filename);
								while ((nbBytes = fis.read(byteBuff)) >= 0) {
									// logger.debug("" + " Nb bytes read: " +
									// nbBytes);
									fos.write(byteBuff, 0, nbBytes);
								}
								fis.close();
							}
							fos.close();
						}

						// End of chunk management
						//////////////////////////////////////////////////////////////////////////////////////

						fileItem.delete();
						if (resumeFiles.size() > ProcessResumes.MAX_RESUMES_PER_FOLDER) {
							maxFilesExceeded = true;
							for (int i = ProcessResumes.MAX_RESUMES_PER_FOLDER; i < resumeFiles.size(); i++) {
								System.out.println("Removing resume file:" + resumeFiles.get(i).getName());
								resumeFiles.get(i).delete();
								resumeFiles.remove(i);
								i--;
							}
						}
					}
				} // while
			}
			if (generateWarning) {
				logger.info("WARNING: just a warning message.\\nOn two lines!");
			}
			out.println("SUCCESS");

			logger.debug("" + "Let's write a status, to finish the server response :");

			// Do you want to test a successful upload, or the way the applet
			// reacts to an error ?
			if (generateError) {
				logger.debug(
						"ERROR: this is a test error (forced in /wwwroot/pages/parseRequest.jsp).\\nHere is a second line!");
			} else {
				logger.debug("SUCCESS");
				// logger.debug(" <span class=\"cpg_user_message\">Il y eu une
				// erreur lors de l'exécution de la requête</span>");
				if (maxFilesExceeded)
					session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_ERROR.toString(),
							"Maximum number of files exceeded, only the first " + ProcessResumes.MAX_RESUMES_PER_FOLDER
									+ " will be processed");
				else
					session.removeAttribute(SESSION_ATTRIBUTE.UPLOAD_ERROR.toString());
			}
			logger.debug("" + "End of server treatment ");
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		} finally {
			out.close();
			WordNetDataProviderPool.getInstance().releaseProvider();
			UserDataProviderPool.getInstance().releaseProvider();
		}
		// if (resumeDirectory != null)
		// {
		// ZipfileProcessingThread zipThread = new
		// ZipfileProcessingThread(session, resumeDirectory, false);
		// zipThread.start();
		// }
	}

	// Method to get the Upload Directory of the user

	public static String getUploadDir(User user) throws IOException {
		String path = "uploads" + File.separator + user.getLogin() + File.separator;
		logger.debug("User Login: " + user.getLogin());
		File dir = PlatformProperties.getInstance().getResourceFile(path);

		if (!dir.exists())
			dir.mkdirs();

		return path;
	}

	/**
	 * Copies the the src file to dst file
	 *
	 * @param src
	 * @param dst
	 * @throws IOException
	 */
	protected void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	} // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click
		// on the + sign on the left to edit the code.">

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}
