/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.textonomics.web;

/**
 *
 * @author Preeti Mudda
 */
import com.textonomics.PlatformProperties;
import net.spy.memcached.AddrUtil;
import net.spy.memcached.BinaryConnectionFactory;
import net.spy.memcached.MemcachedClient;

/**
 * MemCacheClient that connects to Memcached. It puts and grabs the objects from
 * Memcached
 * 
 * @author Preeti Mudda
 */
public class MemCacheClient {
	private static MemCacheClient instance = null;// only one instance is
													// created
	private static MemcachedClient[] m = null; // an array of MemcachedClient
	private int mainServer_port;// setting the port in properties = 11211
	private String mainServerID;// setting the port in properties =
								// "10.124.215.66"

	/**
	 * Connects to Memcached
	 */
	private MemCacheClient() {
		mainServerID = PlatformProperties.getInstance().getProperty("com.textonomics.mainServer_IP"); // get
																										// the
																										// mainServer
																										// IP
																										// from
																										// PlatformProperties
		mainServer_port = Integer
				.parseInt(PlatformProperties.getInstance().getProperty("com.textonomics.mainServer_MemcachedPort")); // get
																														// the
																														// port
																														// from
																														// PlatformProperties
		try {
			m = new MemcachedClient[21];
			// for (int i = 0; i <= 20; i ++)
			// {
			MemcachedClient c = new MemcachedClient(new BinaryConnectionFactory(),
					AddrUtil.getAddresses(mainServerID + ":" + mainServer_port));
			m[0] = c;
			// }
		} catch (Exception e) {
		}

	}

	/**
	 * Creates only one MemCacheClient.
	 * 
	 * @return instance: returns the MemCacheClient object
	 */
	public static synchronized MemCacheClient getInstance() {
		// Checks whether instance is null than creates a new MemCacheClient
		// object
		if (instance == null) {
			instance = new MemCacheClient();
		}
		return instance;
	}

	/**
	 * Sets the objects in Memcached
	 * 
	 * @param key
	 *            : key used to put in Memcached
	 * @param ttl
	 *            : time the object lives in Memcached in seconds
	 * @param o
	 *            : object that is put in Memcached
	 */
	public void set(String key, int ttl, final Object o) {
		getCache().set(key, ttl, o);
	}

	/**
	 * Get the object from Memcached
	 * 
	 * @param key
	 *            : key that is used to grab the object
	 * @return messageObject : returns the object if exists else returns null
	 */
	public Object get(String key) {
		Object messageObject = getCache().get(key);// Object that is stored in
													// Memcached
		// 1.0 Checks if the object is null then dumps a message Cache Miss for
		// debugging purpose
		if (messageObject == null) {
			System.out.println("Cache MISS for KEY: " + key);
		}
		// 1.1 dumps a message Cache Hit for debugging purpose
		else {
			System.out.println("Cache HIT for KEY: " + key);
		}
		return messageObject;
	}

	/**
	 * Deletes the object from Memcached
	 * 
	 * @param key
	 *            : the key of the object
	 * @return object: returns the object that is deleted from Memcached
	 */
	public Object delete(String key) {
		return getCache().delete(key);// returns the object that is deleted from
										// Memcached
	}

	/**
	 * getCache returns a MemcachedClient that is used to connect Memcached
	 * 
	 * @return client: returns the client
	 */
	public MemcachedClient getCache() {
		MemcachedClient client = null;
		try {
			client = m[0];
		} catch (Exception e) {
		}
		return client;
	}

	/**
	 * Flush the Memcached.
	 */
	public void flush() {
		getCache().flush(); // cleans the Memcached
	}
}