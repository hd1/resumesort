package com.textonomics.web;

import com.textnomics.data.JobPosting;
import org.apache.log4j.*;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.textonomics.PlatformProperties;
import com.textonomics.user.User;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * 
 * Copyright 2012 Textonomics. Inc. <br>
 * <br>
 *
 * This servlet is used to process uploaded resumes. It receives a resume folder
 * name, it calls a processing thread to parse resumes and extract phrases and
 * the redirects to select_phrases.jsp.
 *
 */

public class ProcessResumes extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(ProcessResumes.class);
	public static final int MAX_RESUMES_PER_FOLDER = 200;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProcessResumes() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			String resumeFolder = request.getParameter("resumeFolder");
			File resumeDirectory = null;
			String assignResumes = request.getParameter("Assign");
			String uploadDir = "uploads" + File.separator + user.getLogin() + File.separator;
			logger.info("resumeFolder:" + resumeFolder);
			if (resumeFolder != null)
				resumeDirectory = new File(PlatformProperties.getInstance().getResourceFile(uploadDir),
						ResumeSorter.RESUME_FOLDER + "/" + resumeFolder);
			else
				resumeDirectory = (File) session.getAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString());
			if (!resumeDirectory.exists())
				resumeDirectory.mkdirs();
			logger.info("resumeDirectory:" + resumeDirectory);
			JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
			if (posting == null) {
				response.sendRedirect("upload_jobposting.jsp");
				return;
			}
			String postingId = posting.getPostingId();
			posting.setResumeFolder(resumeFolder);
			String createEmail = request.getParameter("mailbox");
			if ("true".equalsIgnoreCase(createEmail))
				posting.setEmailAddress(postingId.toLowerCase().replace(' ', '_'));
			posting.update();
			String bserver = PlatformProperties.getInstance().getProperty("com.textonomics.beanstalkserver");
			if ("true".equals(bserver)) {
				// PJM create an BeanstalkClient instant and pass the job
				BeanstalkClient bClient = BeanstalkClient.getInstance();
				bClient.connect(session, resumeDirectory);
			} else {
				// Check if some previous job is still in progress
				ExecutorService threadPool = (ExecutorService) session
						.getAttribute(ZipfileProcessingThread.RESUME_THREAD_POOL);
				if (threadPool != null) {
					try {
						threadPool.shutdownNow();
					} catch (Exception x) {
						x.printStackTrace();
					}
					int inprocess = 1;
					int retry = 10;
					while (inprocess > 0 && retry > 0) {
						Integer numleft = (Integer) session
								.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
						if (numleft == null)
							numleft = 0;
						if (numleft > 0) {
							Thread.sleep(10000);
						}
						retry--;
						inprocess = numleft;
					}
				}
				if (resumeDirectory.exists()) {
					List<File> resumeFiles = getResumeFiles(resumeDirectory);
					// This will get overwritten by ZipfileProcessingThread
					logger.info("ResumeFiles:" + resumeFiles.size());
					session.setAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString(), resumeFiles);
				}
				ZipfileProcessingThread zipThread = new ZipfileProcessingThread(session, resumeDirectory, false);
				zipThread.start();
			}

			if ("true".equals(assignResumes))
				request.getRequestDispatcher("/ranked_resumes.jsp").forward(request, response);
			else
				request.getRequestDispatcher("/select_phrases.jsp").forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	public static List<File> getResumeFiles(File resumeDirectory) {
		List<File> fileList = new ArrayList<File>();
		File[] files = resumeDirectory.listFiles();
		boolean zip = false;
		for (File file : files) {
			if (file.getName().toLowerCase().endsWith("zip"))
				zip = true;
			if (!file.getName().endsWith("Object") && !file.getName().startsWith(ResumeSorter.CONVERTED_PREFIX)
					&& !file.getName().startsWith(ResumeSorter.COVERLETTER_PREFIX) && !file.getName().endsWith("zip")
					&& !file.getName().startsWith("."))
				fileList.add(file);
		}
		if (zip && fileList.size() == 0) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException ex) {
			}
			files = resumeDirectory.listFiles();
			for (File file : files) {
				if (!file.getName().endsWith("Object") && !file.getName().startsWith(ResumeSorter.CONVERTED_PREFIX)
						&& !file.getName().startsWith(ResumeSorter.COVERLETTER_PREFIX)
						&& !file.getName().endsWith("zip") && !file.getName().startsWith("."))
					fileList.add(file);
			}
		}
		return fileList;
	}
}
