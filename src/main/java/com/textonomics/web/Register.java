package com.textonomics.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.tanesha.recaptcha.*;

import com.textonomics.PlatformProperties;
import com.textonomics.mail.RegistrationMailer;
import com.textonomics.mail.RegistrationMailerThread;
import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderException;
import com.textonomics.user.UserDataProviderException.USER_EXCEPTION_TYPES;
import com.textonomics.user.UserDataProviderPool;
import java.security.MessageDigest;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 * 
 * This servlet allows users to register for the alpha and beta releases. The
 * doPost method loads a file of emails of people that will be alpha users. If
 * the the email sent by the client matches one of these emails then the user is
 * signed up for the alpha, otherwise they are added to alpt users table.
 * 
 *
 *
 * @author Nuno Seco and Rodrigo Neves
 *
 */
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final String CAPTHA_KEY = "6Lfp3tUSAAAAALcyNKpPaTmDkosS-8yeJjwg7OEI";

	/**
	 * Executor used to spawn threads for sending emails
	 */
	public final Executor exec;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Register() {
		super();
		exec = new ThreadPoolExecutor(10, 50, 10, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(50));
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User user = null;
		UserDataProvider userProvider = null;

		// Loads a text file containing the alpha users
		HashSet<String> set = loadUsersList();
		String password = request.getParameter("password").trim();
		String encryptedPW = password;
		sun.misc.BASE64Encoder baseEncoder = new sun.misc.BASE64Encoder();
		String code = request.getParameter("betacode");
		if (code != null)
			code = code.trim().toLowerCase();
		// if (code == null || code.length() == 0)
		// code = "craigslist";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.reset();
			md.update((password + request.getParameter("email")).getBytes());
			byte[] b = md.digest();
			encryptedPW = new String(baseEncoder.encode(b));
		} catch (Exception x) {
			x.printStackTrace(); // should never happen
		}
		try {
			String remoteAddr = request.getRemoteAddr();
			ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
			reCaptcha.setPrivateKey(CAPTHA_KEY);

			String challenge = request.getParameter("recaptcha_challenge_field");
			String uresponse = request.getParameter("recaptcha_response_field");
			if (challenge != null) {
				ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, uresponse);

				if (!reCaptchaResponse.isValid()) {
					throw new Exception("Invalid Captcha Response. Please try again");
				}
			}

			// If email exists register user (DG - Stop using registration table
			// for now).
			System.out.println("New registration :" + request.getParameter("email") + " code:" + code);
			int state = -1;
			int type = 2;
			if (set.contains(request.getParameter("email").trim()) || set.contains(code)) {
				state = 1;
				type = 1;
			}
			if (code != null && code.length() > 0 && state == -1) {
				request.getSession().setAttribute(SESSION_ATTRIBUTE.ERROR.toString(),
						"Invalid Participation Code:" + code);
				response.sendRedirect(
						response.encodeRedirectURL("signup.jsp?error=Invalid Participation Code:" + code));
				return;
			}

			userProvider = UserDataProviderPool.getInstance().acquireProvider();
			int freeResumes = getInt(
					PlatformProperties.getInstance().getProperty("com.textnomics.free_resumes_processed"));
			if (freeResumes == 0)
				freeResumes = 200;
			int freePostings = getInt(
					PlatformProperties.getInstance().getProperty("com.textnomics.free_postings_processed"));
			if (freePostings == 0)
				freePostings = 3;

			user = new User();
			user.setEmail(request.getParameter("email").trim());
			user.setLogin(request.getParameter("email").trim());
			user.setName(request.getParameter("name").trim());
			user.setCompany(request.getParameter("company").trim());
			user.setParticipationCode(code);
			user.setPassword(encryptedPW); // save encrypted password
			user.setState(state);
			user.setSimpleRating(true);
			user.setFreePostings(freePostings);
			user.setFreeResumes(freeResumes);
			userProvider.insertUser(user);
			userProvider.updateUserField(user, "company");
			userProvider.updateUserField(user, "simpleRating");
			userProvider.updateUserField(user, "freePostings");
			userProvider.updateUserField(user, "freeResumes");
			userProvider.commit();
			exec.execute(new RegistrationMailerThread(user.getEmail(), RegistrationMailer.REGISTRATION.ALPHA,
					user.getEmail(), password));
			request.getRequestDispatcher("/RegistrationComplete.jsp?type=" + type).forward(request, response);
			return;

		} catch (UserDataProviderException ex) {
			ex.printStackTrace();
			if (ex.getType() == USER_EXCEPTION_TYPES.USER_EXISTS) {
				response.sendRedirect(
						response.encodeRedirectURL("signup.jsp?error=" + "Email or Login is already signed up."));
				return;
			} else {
				request.getSession().setAttribute(SESSION_ATTRIBUTE.ERROR.toString(), ex.getMessage());
				response.sendRedirect(response.encodeRedirectURL("signup.jsp?error=" + ex.getMessage()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("email", request.getParameter("email").trim());
			request.setAttribute("name", request.getParameter("name").trim());
			request.setAttribute("company", request.getParameter("company").trim());
			request.setAttribute("betacode", request.getParameter("betacode").trim());
			request.getSession().setAttribute(SESSION_ATTRIBUTE.ERROR.toString(), e.getMessage());
			request.getRequestDispatcher("signup.jsp?error=" + e.getMessage()).forward(request, response);
		} finally {
			if (userProvider != null)
				UserDataProviderPool.getInstance().releaseProvider();
		}
	}

	/**
	 * 
	 * @return a set containing the emails of the alpha users
	 */
	public HashSet<String> loadUsersList() {
		File usersList = PlatformProperties.getInstance().getResourceFile("userList.txt");
		HashSet<String> set = new HashSet<String>();
		try {

			BufferedReader br = new BufferedReader(new FileReader(usersList));
			String text = "";
			while ((text = br.readLine()) != null) {
				if (!text.equals(""))
					set.add(text.substring(1, text.indexOf(">")));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return set;
	}

	private static int getInt(String value) {
		try {
			return new Integer(value).intValue();
		} catch (Exception x) {
			return 0;
		}
	}
}
