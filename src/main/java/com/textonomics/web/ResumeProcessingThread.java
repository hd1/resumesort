/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textnomics.data.RejectedSynonym;
import com.textnomics.data.UserTemplatePhrase;
import com.textonomics.DocumentRS;
import com.textonomics.DocumentProcessorRS;
import com.textonomics.Phrase;
import com.textonomics.PhrasePositionComparator;
import com.textonomics.PlatformProperties;
import com.textonomics.SynonymPhrase;
import com.textonomics.nlp.Chunker;
import com.textonomics.nlp.DefaultNLPSuite;
import com.textonomics.nlp.NGram;
import com.textonomics.nlp.NLPSuite;
import com.textonomics.nlp.POSTagger;
import com.textonomics.nlp.Sentence;
import com.textonomics.nlp.SentenceSpliter;
import com.textonomics.nlp.Token;
import com.textonomics.openoffice.OOFILTER_TYPE;
import com.textonomics.user.User;
import com.textonomics.wordnet.model.POS;
import com.textonomics.wordnet.model.Word;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import com.textonomics.wordnet.morphy.Morphy;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import org.hamcrest.core.IsInstanceOf;

/**
 *
 * @author Vinod
 */
public class ResumeProcessingThread implements Runnable {
	static Logger logger = Logger.getLogger(ResumeProcessingThread.class);
	private SentenceSpliter sentenceDetector;
	private NLPSuite suite;
	private POSTagger tagger;
	private Chunker chunker;
	boolean useTika;

	double maxSqrtCount = 2.65; // past 7 counts, it counts it as 8.
	// this is a modified Hamming distance! If the score is above or equal to
	// the HammingDistScoreThreshold we use 1.0 or hammingDistIfAboveScoreThresh
	// if it is below we use hammingDistIfBelowScoreThresh (or .8)
	// tags do not count!
	double HammingDistScoreThreshold = 3;
	double hammingDistIfAboveScoreThresh = 1.0;
	double hammingDistIfBelowScoreThresh = 0.8;

	HttpSession session;

	File resume;
	ArrayList<Phrase> phrasesToBeMatched;

	String uploadDir;

	List<Sentence> nonLemmaSentences; // contains non-lemmatized sentences
	List<Sentence> lemmaSentences; // contains lemmatized sentences

	Map<Phrase, Integer> matchedPhrases = new TreeMap<Phrase, Integer>(new PhrasePositionComparator());
	List<String> missingPhrases = new ArrayList<String>();

	List<File> highlightedFiles = new ArrayList<File>();
	List<File> highlightedHtmls = new ArrayList<File>();
	List<Map<Phrase, Integer>> matchedPhraseList = new ArrayList<Map<Phrase, Integer>>();
	List<List<String>> unmatchedPhraseList = new ArrayList<List<String>>();
	List<Integer> phrasesToBeMatchedImp = new ArrayList<Integer>();
	List<Boolean> processedByTika;
	Map<String, String> phrasesSynonyms;
	Map<String, Set<String>> deletedSynonyms;
	Map<String, Integer> resumePhrasesMap;
	List<String> candidateNames;
	List<String> candidateEmails;
	List<String> candidateAddresses;
	Set<String> templatePhraseSet = new HashSet<String>();

	User user;
	int fileIndex;

	public ResumeProcessingThread(File resume, HttpSession session, int fileIndex, boolean useOpenOffice) {
		this(resume, session, fileIndex);
		useTika = !useOpenOffice;
		// logger.debug("USE TIKA FLAG2:" + useTika);
	}

	public ResumeProcessingThread(File resume, HttpSession session, int fileIndex) {
		try {
			useTika = "true".equalsIgnoreCase(
					PlatformProperties.getInstance().getUserProperty("com.textnomics.tika_sentence_detector"));
			// logger.debug("USE TIKA FLAG:" + useTika);
			suite = new DefaultNLPSuite(false, useTika);
			sentenceDetector = suite.getSpliter();
			tagger = suite.getTagger();
			chunker = suite.getChunker();

			this.resume = resume;
			this.session = session;
			long lastAccess = session.getLastAccessedTime();
			long idle = System.currentTimeMillis() - lastAccess;
			int maxInactive = session.getMaxInactiveInterval();
			// If remaining idle time is less than 2 hours, add 2 hours to it
			// (give user time to come back)
			if ((maxInactive - idle / 1000) < 60 * 60 * 2)
				session.setMaxInactiveInterval(maxInactive + 60 * 60 * 2);
			this.highlightedFiles = (List<File>) session
					.getAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString());
			uploadDir = (String) session.getAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString()); // the
																								// upload
																								// dir
																								// (for
																								// resumes)
																								// of
																								// the
																								// user
			phrasesToBeMatched = (ArrayList<Phrase>) (Collection<Phrase>) session
					.getAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED.toString());
			matchedPhraseList = (List<Map<Phrase, Integer>>) session
					.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
			unmatchedPhraseList = (List<List<String>>) session
					.getAttribute(SESSION_ATTRIBUTE.UNMATCHED_PHRASES.toString());
			highlightedHtmls = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_HTMLRESUMES.toString());
			phrasesToBeMatchedImp = (List<Integer>) session
					.getAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED_IMPORTANCE.toString());
			phrasesSynonyms = (Map<String, String>) session
					.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());
			deletedSynonyms = (Map<String, Set<String>>) session
					.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_DELETED_SYNONYMS.toString());
			processedByTika = (List<Boolean>) session.getAttribute(SESSION_ATTRIBUTE.PROCESSED_BY_TIKA.toString());
			resumePhrasesMap = (Map<String, Integer>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
			candidateNames = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_NAMES.toString());
			candidateEmails = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_EMAILS.toString());
			candidateAddresses = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_ADDRESSES.toString());
			List<UserTemplatePhrase> templatePhrases = (List<UserTemplatePhrase>) session
					.getAttribute(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString());
			if (templatePhrases == null)
				templatePhrases = new ArrayList<UserTemplatePhrase>();
			for (UserTemplatePhrase templatePhrase : templatePhrases)
				templatePhraseSet.add(templatePhrase.getPhrase());
			user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			System.out.println("Resume:" + resume + " fileIndex:" + fileIndex);
			this.fileIndex = fileIndex;
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
	}

	public void run() {

		WordNetDataProviderPool.getInstance().acquireProvider();

		processResume();

		WordNetDataProviderPool.getInstance().releaseProvider();
	}

	boolean errorInProcessing = false;

	public boolean isErrorInProcessing() {
		return errorInProcessing;
	}

	private void processResume() {
		errorInProcessing = false;
		try {

			System.out.println("Matching phrases for " + resume.toString());

			DocumentRS resumeDoc = null;
			File serializedResume = new File(resume.getPath() + ".Object");
			processedByTika.set(fileIndex, false);

			File serializedTikaResume = new File(resume.getPath() + ".tikaObject");
			if (resumeDoc == null && useTika && serializedTikaResume.exists()) {
				resumeDoc = (DocumentRS) FileUtils.deSerializeOject(serializedTikaResume);
				processedByTika.set(fileIndex, true);
			}

			if (resumeDoc == null && serializedResume.exists()) {
				resumeDoc = (DocumentRS) FileUtils.deSerializeOject(serializedResume);
				processedByTika.set(fileIndex, false);
			}

			if (resumeDoc != null) {
				nonLemmaSentences = new ArrayList<Sentence>(resumeDoc.getSentences());
				lemmaSentences = resumeDoc.getLemmatizedSentences();
			}

			if (resumeDoc == null) {
				NLPSuite nlpSuite = new DefaultNLPSuite();
				String serializedObjName = resume.getPath() + ".Object";
				if (useTika) {
					serializedObjName = resume.getPath() + ".tikaObject";
					nlpSuite = new DefaultNLPSuite(false, true);
					processedByTika.set(fileIndex, true);
				} else {
					if (resume.getName().toLowerCase().endsWith(".pdf")) {
						String fileName = resume.getName();
						if (fileName.contains(" ")) {
							fileName = fileName.replace(' ', '_');
							File newFile = new File(resume.getParent(), fileName);
							resume.renameTo(newFile);
							resume = newFile;
						}
						if (fileName.lastIndexOf(".") != -1) {
							fileName = ResumeSorter.CONVERTED_PREFIX + fileName.substring(0, fileName.lastIndexOf("."));
							fileName = fileName.replace(' ', '_') + ".html";
							System.out.println("New file name:" + fileName);
						}
						try {
							// File uploadDir =
							// PlatformProperties.getInstance().getResourceFile(ResumeSorter.getUploadDir(user));
							resume = new PDFtoHTML().convert(resume, new File(resume.getParent(), fileName));
						} catch (Exception x) {
							logger.error("There was was an error processing your .pdf file:", x);
							throw new Exception(
									"There was was an error processing your .pdf file. Please convert it to .doc and retry.");
						}
					}
				}
				// Serialize Resume
				DocumentProcessorRS resumeObject = new DocumentProcessorRS(nlpSuite);
				resumeObject.process(resume, false, true);
				if (!useTika)
					processedByTika.set(fileIndex, false);
				// nonLemmaSentences = sentenceDetector.split(resume); // get
				// non lemmatized sentences from the resume
				// resumeObject.setSentences(nonLemmaSentences);

				resumeDoc = resumeObject.getDocument();
				nonLemmaSentences = resumeDoc.getSentences();
				// Lemmatize sentences
				lemmatizeSentences();
				resumeDoc.setLemmatizedSentences(lemmaSentences);

				FileUtils.serializeOject(resumeDoc, new File(serializedObjName));
			}
			if (candidateEmails != null && candidateEmails.size() > fileIndex && candidateEmails.get(fileIndex) == null
					|| useTika)
				candidateEmails.set(fileIndex, resumeDoc.getEmail());
			if (candidateNames != null && candidateNames.size() > fileIndex && candidateNames.get(fileIndex) == null
					|| useTika)
				candidateNames.set(fileIndex, resumeDoc.getName());
			if (candidateAddresses != null && candidateAddresses.size() > fileIndex
					&& candidateAddresses.get(fileIndex) == null || useTika)
				candidateAddresses.set(fileIndex, resumeDoc.getZipcode());
			// Lemmatize sentences
			if (lemmaSentences == null || lemmaSentences.size() == 0)
				lemmatizeSentences();

			// Match the phrases
			matchPhrases();

			matchedPhraseList.set(fileIndex, matchedPhrases);
			unmatchedPhraseList.set(fileIndex, missingPhrases);

		} catch (Exception ex) {
			// Indicate error
			List<Double> scores = (List<Double>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
			if (scores != null)
				scores.set(fileIndex, -999.0);
			ex.printStackTrace();
			String error = (String) session.getAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());
			if (error == null)
				error = "";
			error = error + "\nError processing resume:" + resume.getName();
			session.setAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString(), error);
			logger.error("Error processing resume:" + resume.getAbsolutePath(), ex);
			errorInProcessing = true;
			processedByTika.set(fileIndex, true);
		}
	}

	/**
	 *
	 * @param name
	 *            the name of the file
	 * @return an enumeration type that represents the type of the file
	 */
	private OOFILTER_TYPE getExtension(String name) {
		String extension;
		StringBuilder builder = new StringBuilder();
		for (int i = name.length() - 1; i >= 0; i--) {
			if (name.charAt(i) == '.') {
				break;
			}
			builder.append(name.charAt(i));
		}

		extension = builder.reverse().toString().toLowerCase().trim();

		if (extension.equals("doc"))
			return OOFILTER_TYPE.MS_WORD_97;

		if (extension.equals("docx"))
			return OOFILTER_TYPE.MS_Word_2003_XML;

		if (extension.equals("odt"))
			return OOFILTER_TYPE.ODT;

		if (extension.equals("rtf"))
			return OOFILTER_TYPE.Rich_Text_Format;

		if (extension.equals("html"))
			return OOFILTER_TYPE.HTML;

		if (extension.equals("txt"))
			return OOFILTER_TYPE.Plain_Text;

		return null;

	}

	// 1. Tokenize the phrase, lemmatized and non-lemma sentences.
	// 2. Compare the first token of the phrase with the tokens of the
	// sentences. If it matches, check if the consecutive tokens match too.
	// 3.
	private void matchPhrases() {
		// Searching for the user selected phrases

		int ind = 0;
		Map<Phrase, Integer> synonymImp = new HashMap<Phrase, Integer>();
		Set<String> foundPhrases = new HashSet<String>();
		Set<String> missing = new HashSet<String>();
		for (Phrase phrase : phrasesToBeMatched) // For each phrase
		{
			String phraseBuffer = phrase.getBuffer(); // Get the phrase buffer
			// System.out.println("Matching phrase:" + phraseBuffer);
			boolean match = false;
			String synonymStr = phrasesSynonyms.get(phraseBuffer.trim());
			Set<String> deleted = new HashSet<String>();
			if (deletedSynonyms != null) {
				deleted = deletedSynonyms.get(phraseBuffer.trim());
				// if (deleted != null && deleted.size() > 0)
				// System.out.println("Phrase:" + phraseBuffer.trim() + "
				// deleted synonyms:" + deleted);
			}
			// System.out.println("Phrase:" + phraseBuffer + " Synonym:" +
			// synonymStr + ":" + phrasesToBeMatched.size());
			String[] synonyms = new String[0];
			if (synonymStr != null)
				synonyms = synonymStr.split("\\|");
			List<Token> selectedPhraseTokens = phrase.getNgram().getElements(); // Get
																				// the
																				// selected
																				// phrase
																				// tokens
			List<String> synonymList = new ArrayList<String>();
			List<String[]> synonymTokensList = new ArrayList<String[]>();
			for (String synonym : synonyms) {
				if (synonym.trim().length() == 0)
					continue;
				try {
					if (deleted != null && deleted.contains(synonym)
							|| RejectedSynonym.isRejected(user.getLogin(), phrase.getBuffer().trim(), synonym))
						continue;
				} catch (Exception ex) {
					java.util.logging.Logger.getLogger(ResumeProcessingThread.class.getName()).log(Level.SEVERE, null,
							ex);
				}
				boolean exists = false;
				for (Phrase phrase2 : phrasesToBeMatched) {
					if (phrase2.getBuffer().trim().equalsIgnoreCase(synonym)) {
						exists = true;
						break;
					}
				}
				if (exists)
					continue;
				synonymList.add(synonym);
				String[] tokens = synonym.split(" ");
				synonymTokensList.add(tokens);
			}
			int firstToken = -1;

			boolean found = false;

			for (int sentenceIndex = 0; sentenceIndex < lemmaSentences.size(); sentenceIndex++) {
				Sentence lemmaSentence = lemmaSentences.get(sentenceIndex); // Get
																			// the
																			// lemma
																			// sentence
				// System.out.println("lemma sentence: " +
				// lemmaSentence.toString());

				Sentence nonLemmaSentence = nonLemmaSentences.get(sentenceIndex); // Get
																					// the
																					// non-lemma
																					// sentence
				// System.out.println("non-lemma sentence: " +
				// nonLemmaSentence.toString());

				List<Token> lemmaSentenceTokens = lemmaSentence.getTokens(); // Get
																				// the
																				// lemmatized
																				// tokens
																				// from
																				// the
																				// sentence
				List<Token> nonLemmaSentenceTokens = nonLemmaSentence.getTokens(); // Get
																					// the
																					// non-lemmatized
																					// tokens
																					// from
																					// the
																					// sentence

				for (int i = 0; i < lemmaSentenceTokens.size(); i++) {
					int x = i;
					for (int j = 0; j < selectedPhraseTokens.size(); j++) {
						if (x >= lemmaSentenceTokens.size()) {
							match = false;
							break;
						}

						boolean testPassed = false;

						Token selectedPhraseToken = selectedPhraseTokens.get(j);

						String nonLemmaSentenceTokenText = nonLemmaSentenceTokens.get(x).getBuffer().toString().trim();
						String selectedPhraseTokenText = selectedPhraseToken.getBuffer().trim();
						String lemmaSentenceTokenText = lemmaSentenceTokens.get(x).getBuffer().toString().trim();
						// If it is an user "entered" phrase. try exact match
						// (nonlemma)
						if (selectedPhraseToken.getPos().equalsIgnoreCase("No POS")
								&& !resumePhrasesMap.containsKey(phraseBuffer.trim())
								&& !templatePhraseSet.contains(phraseBuffer.trim())) {
							testPassed = selectedPhraseTokenText.equalsIgnoreCase(nonLemmaSentenceTokenText);
							// if
							// (nonLemmaSentenceTokenText.toLowerCase().startsWith("script")
							// ||
							// nonLemmaSentenceTokenText.toLowerCase().startsWith("budget"))
							// System.out.println("nonLemmaSentenceTokenText:" +
							// nonLemmaSentenceTokenText + " selected phrase:" +
							// selectedPhraseTokenText);
						} else // If it is an user "selected" or "checked"
								// phrase
						{
							testPassed = selectedPhraseTokenText.equalsIgnoreCase(lemmaSentenceTokenText)
									|| selectedPhraseTokenText.equalsIgnoreCase(nonLemmaSentenceTokenText);
							// if
							// (lemmaSentenceTokenText.toLowerCase().startsWith("script")
							// ||
							// nonLemmaSentenceTokenText.toLowerCase().startsWith("budget"))
							// System.out.println("lemmaSentenceToken:" +
							// lemmaSentenceTokenText + "
							// nonLemmaSentenceTokenText:" +
							// nonLemmaSentenceTokenText + " selected phrase:" +
							// selectedPhraseTokenText);
						}

						if (testPassed) {
							if (firstToken == -1)
								firstToken = x;
							match = true;
							x++;
						} else {
							firstToken = -1;
							match = false;
							break;
						}
					}
					if (match) {
						try {
							NGram ngram = new NGram();
							Sentence nls = nonLemmaSentences.get(sentenceIndex);
							List<Token> tokenList = nls.getTokens(); // Tokenization
							String[] tokens = nls.getTokenBuffers();
							String[] pos = tagger.tag(tokens); // POS Tagging
							// String[] chunks = chunker.chunk(tokens, pos);
							// //Chunking
							for (int k = 0; k < selectedPhraseTokens.size(); k++) {
								Token token = tokenList.get(k + firstToken);
								token.setPos(pos[k + firstToken]);
								// token.setChunk(chunks[k + firstToken]);
								ngram.addElement(token);
							}
							Phrase matchedPhrase = new Phrase(phraseBuffer, ngram, sentenceIndex);

							matchedPhrases.put(matchedPhrase, phrasesToBeMatchedImp.get(ind));
							foundPhrases.add(phraseBuffer.toLowerCase());
							// logger.debug("Matched Phrase:" +
							// matchedPhrase.toString() + " Imp:" +
							// phrasesToBeMatchedImp.get(ind));
							found = true;
						} catch (Exception ex) {
							ex.printStackTrace();
							logger.error(ex);
						}
					}

					// Now check all synonyms
					for (int l = 0; l < synonymTokensList.size(); l++) {
						firstToken = -1;
						x = i;
						String[] synonymTokens = synonymTokensList.get(l);
						String synonymText = synonymList.get(l);
						// System.out.println("synonymText:" + synonymText + "
						// sentence:" + nonLemmaSentence.toString());

						// If the synonym is also another selected phrase, then
						// use the rating of that phrase
						int synImp = phrasesToBeMatchedImp.get(ind);
						for (int k = 0; k < phrasesToBeMatched.size(); k++) {
							Phrase otherPhrase = phrasesToBeMatched.get(k);
							if (otherPhrase.getBuffer().trim().equalsIgnoreCase(synonymText)) {
								synImp = phrasesToBeMatchedImp.get(k);
								break;
							}
						}
						for (int j = 0; j < synonymTokens.length; j++) {
							if (x >= lemmaSentenceTokens.size()) {
								match = false;
								break;
							}

							boolean testPassed = false;

							String nonLemmaSentenceTokenText = nonLemmaSentenceTokens.get(x).getBuffer().toString()
									.trim();
							String selectedPhraseTokenText = synonymTokens[j].trim();
							String lemmaSentenceTokenText = lemmaSentenceTokens.get(x).getBuffer().toString().trim();
							testPassed = selectedPhraseTokenText.equalsIgnoreCase(lemmaSentenceTokenText)
									|| selectedPhraseTokenText.equalsIgnoreCase(nonLemmaSentenceTokenText);

							if (testPassed) {
								if (firstToken == -1)
									firstToken = x;
								match = true;
								x++;
							} else {
								firstToken = -1;
								match = false;
								break;
							}
						}
						if (match) {
							try {
								NGram ngram = new NGram();
								Sentence nls = nonLemmaSentences.get(sentenceIndex);
								List<Token> tokenList = nls.getTokens(); // Tokenization
								String[] tokens = nls.getTokenBuffers();
								String[] pos = tagger.tag(tokens); // POS
																	// Tagging
								// String[] chunks = chunker.chunk(tokens, pos);
								// //Chunking
								// System.out.println("Phrase:" + synonymText +
								// " sent:" + nls.toString() + " firstToken:" +
								// firstToken + ":" + tokenList.get(firstToken)
								// + " selectedPhraseTokens:" +
								// selectedPhraseTokens.size());
								for (int k = 0; k < selectedPhraseTokens.size(); k++) {
									if (k + firstToken >= tokenList.size())
										continue;
									Token token = tokenList.get(k + firstToken);
									token.setPos(pos[k + firstToken]);
									// token.setChunk(chunks[k + firstToken]);
									ngram.addElement(token);
								}
								Phrase matchedPhrase = new SynonymPhrase(synonymText, ngram, sentenceIndex,
										phraseBuffer);
								synonymImp.put(matchedPhrase, synImp);
								// matchedPhrases.put(matchedPhrase, 7);
								matchedPhrases.put(matchedPhrase, synImp);
								foundPhrases.add(synonymText.toLowerCase());
								found = true;
							} catch (Exception ex) {
								ex.printStackTrace();
								logger.error(ex);
							}
							break;
						}
					}
				}
			}

			if (!found && !missing.contains(phraseBuffer.toLowerCase())
					&& !foundPhrases.contains(phraseBuffer.toLowerCase())) {
				// System.out.println("Adding to missing:" + phraseBuffer);
				missingPhrases.add(phraseBuffer);
				missing.add(phraseBuffer.toLowerCase());
			} else {
				// System.out.println("Phrase found:" + phraseBuffer);
			}
			ind++;
		}
		double score = 0.0;
		Double maxScore = (Double) session.getAttribute(SESSION_ATTRIBUTE.MAX_SCORE.toString());
		Map<String, Integer> phraseImportance = (Map<String, Integer>) session
				.getAttribute(SESSION_ATTRIBUTE.PHRASE_IMPORTANCE.toString());
		List<String> userEnteredPhrases = (List<String>) session
				.getAttribute(SESSION_ATTRIBUTE.ENTERED_PHRASES.toString());
		score = getScore(matchedPhrases, missingPhrases, phraseImportance);
		List<Double> scores = (List<Double>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
		scores.set(fileIndex, score);
		//
		// Keep old code for now
		// Set<String> uniquePhrase = new HashSet<String>();
		// int numfound = 0; // Number of unique Matched Phrases
		// for (Phrase matchedPhrase: matchedPhrases.keySet())
		// {
		// Integer temp = matchedPhrases.get(matchedPhrase);
		// if (temp == null)
		// temp = 0;
		// double imp = temp;
		// if (imp == 7) // Synonym match
		// {
		// Integer synimp = synonymImp.get(matchedPhrase);
		// if (synimp == null)
		// imp = 2;
		// else
		// imp = (3.0*synimp)/4;
		// }
		// if (imp == 6) imp = 7; // Must have match
		// score = score + imp;
		// if (uniquePhrase.contains(matchedPhrase.getBuffer())) continue;
		// numfound++;
		// uniquePhrase.add(matchedPhrase.getBuffer());
		// }
		// System.out.println("Resume:" + resume.getName() + " score:" + score +
		// " matched:" + matchedPhrases.keySet().size() + " Numfound:" +
		// numfound + " maxScore:"+ maxScore);
		// if (matchedPhrases.keySet().size() > 0)
		// score = score*numfound/matchedPhrases.keySet().size();
		// boolean missingMustHave = false;
		// for (String missing: missingPhrases)
		// {
		// Integer missingImp =
		// phraseImportance.get(missing.toLowerCase().trim());
		// if (missingImp != null && missingImp == 6) // Must have phrases
		// missingMustHave = true;
		// if ((missingImp == null || missingImp == 0) &&
		// userEnteredPhrases.contains(missing.trim())) // Must have phrases
		// missingMustHave = true;
		// }
		// if (maxScore != 0.0)
		// score = score/maxScore;
		// score = score*5.0;
		// if (missingMustHave) score = score - 5;

	}

	private void lemmatizeSentences() {
		lemmaSentences = new ArrayList<Sentence>();

		// lemmatize each sentence
		for (Sentence nls : nonLemmaSentences) // lemmatize each sentence
		{

			try // lemmatize each sentence
			{
				Sentence lemmaSentence = new Sentence();
				List<Token> tokenList = nls.getTokens(); // List of "Token"s
				String[] tokens = nls.getTokenBuffers(); // Contains token
															// buffers, not
															// tokens itself
				String[] tags = tagger.tag(tokens); // POS Tagging

				Morphy morphyInstance = Morphy.getInstance();

				for (int tokenIndex = 0; tokenIndex < tokens.length; tokenIndex++) // lemmatize
																					// each
																					// token
																					// of
																					// the
																					// sentence
																					// from
																					// the
																					// resume
				{
					Set<Word> lemmas = new HashSet<Word>();
					if (tags[tokenIndex].startsWith("VB") || tags[tokenIndex].startsWith("NN")) {
						if (tags[tokenIndex].startsWith("VB") && !tags[tokenIndex].equalsIgnoreCase("VB")) {
							lemmas = morphyInstance.getStems(tokens[tokenIndex], POS.VERB);
						}
						if (tags[tokenIndex].equalsIgnoreCase("NNS") || tags[tokenIndex].equalsIgnoreCase("NNPS")) {
							lemmas = morphyInstance.getStems(tokens[tokenIndex], POS.NOUN);
						}
					}
					if (!lemmas.isEmpty() && !lemmas.iterator().next().toString().trim().equalsIgnoreCase("")) {
						tokens[tokenIndex] = lemmas.iterator().next().toString(); // Change
																					// the
																					// token
																					// buffer
																					// to
																					// lemmatized
																					// form
					}
					lemmaSentence
							.add(new Token(tokens[tokenIndex] + " ", tokenList.get(tokenIndex).getPosition(), null)); // Add
																														// the
																														// token
																														// to
																														// the
																														// lemmatized
																														// sentence
				}
				lemmaSentences.add(lemmaSentence); // Add the currently
													// lemmatized sentence to
													// the list of lemmatized
													// sentences.
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	/*
	 * Get Resume score
	 * 
	 * @param - matchedPhrases - Phrase, # of Stars (1-5, must have = 6)
	 * 
	 * @param - missingPhrases - String, # of Stars (1-5, must have = 6) Nice to
	 * have = 1 star Should have = 3 stars
	 *******
	 * There are 3 factors that matter: A) How many phrases are matched and how
	 * what is their score... see the the research notes on the scoring
	 * mechanism here. B) How many phrases are missing and how does the table on
	 * top look. If It is heavily skewed to the side of the missing phrases, it
	 * will look bad, so... we want to use a Hamming distace mechanism TYPE to
	 * handle this too. C) How many highlighting we actually have. This is a
	 * function of the lenght of the document as well. But we want a document
	 * that glows with highlights.
	 *
	 * 1. If nothing matches, return the negative score due to must have's.
	 * Beacuse our new score is not between 0 and 5, subtract -50 for each
	 * missing Must Have phrase. 2. Count the number of matched phrases for each
	 * phrase. It's a bitch! 3. Count the number of Missing Must Have phrases.
	 * 4. Initialize the sytem for score with Feedback, combination of LK
	 * divergence and OKapi method... Just initialize for now 5. Calculate the
	 * score based on the Term Frequency Count and num stars. 6. add in
	 * contribution due to Ratio of matched to total phrases 7. add in
	 * contribution due to Number of highlights and missing Must Haves and
	 * return the score.
	 */
	double getScore(Map<Phrase, Integer> matchedPhrases, List<String> missingPhrases,
			Map<String, Integer> phraseImportance) {
		////// added later EB Nov 25, 2013 and changed March 19th
		double negScoreForMissingPhrases = -15.0;
		// 1.
		// if nothing matches, return the negative score
		if (matchedPhrases.isEmpty()) {
			double negScore = 0.0;
			for (Integer s : phraseImportance.values()) {
				if (s == 6) {
					negScore += negScoreForMissingPhrases;
				}
			}
			return negScore;
		}

		/// if there are no missing phrases return 5.0
		// if (missingPhrasesNotUsed.isEmpty()) {
		// return 5.0;
		// }
		// 2.
		Map<String, Integer> stringCounts = getCountsForPositiveStars(matchedPhrases);

		// 3.
		int numMissingMustHavePhrases = 0;
		for (String s : missingPhrases) {
			if (phraseImportance.containsKey(s)) {
				if (phraseImportance.get(s) == 6) {
					numMissingMustHavePhrases++;
				}
			} else if (phraseImportance.containsKey(s.trim().toLowerCase())) {
				if (phraseImportance.get(s.trim().toLowerCase()) == 6) {
					numMissingMustHavePhrases++;
				}
			}
		}

		// 4. initialization See research notes.
		// score for the number of stars from 0 to 6
		double Q_STAR_WIEGHTS_6_LOG[] = { 0.0, 0.916290731874155, 1.098612288668110, 1.386294361119891,
				1.609437912434100, 1.791759469228055, 2.5 };
		// score for the term frequency count it is "shomareh taghsim beh Seh
		// beh alaaveh" natural logrithm "shomareh beh alaaveh yek"
		double tfCountScore[] = { 0.0, 1.026480513893279, 1.765278955334777, 2.386294361119891, 2.942771245767434,
				3.458426135894721, 3.945910149055313, 4.412774875013169, 4.863891244002886, 5.302585092994046,
				5.731228606131705 };

		double tmpScoreForTF = 0.0;
		int maxNumRepeatPhrases = 9;
		double addedTFcontributionAboveMaxNumRepeatPhrases = 0.0175;
		int numHighlights = 0;
		int numZeroStarsMatched = 0; // this is passed as part of the matched
										// phrases -- sucks but counts toward
										// numHighlights
		// 5.
		for (String s : stringCounts.keySet()) {

			if (s.equalsIgnoreCase("bakhtegan")) {
				System.out.println("*");
			}

			int sCount = stringCounts.get(s);
			double tfContribution = 0;
			if (sCount > maxNumRepeatPhrases) { // if it is above 10 -- increase
												// past 10 slowly
				tfContribution = tfCountScore[maxNumRepeatPhrases]
						+ (sCount - maxNumRepeatPhrases) * addedTFcontributionAboveMaxNumRepeatPhrases;
			} else {
				tfContribution = tfCountScore[sCount];
			}

			double starsContribution = 0;
			int numStars = 0;
			if (phraseImportance.containsKey(s)) {
				numStars = phraseImportance.get(s);
			} else {
				numStars = phraseImportance.get(s.trim().toLowerCase());
			}
			starsContribution = Q_STAR_WIEGHTS_6_LOG[numStars];

			tmpScoreForTF += tfContribution * starsContribution;
			numHighlights += sCount;
			if (numStars == 0) {
				numZeroStarsMatched++;
			}
		} // for (String s : counts.keySet())

		// 7. and 8.
		double numHighlightMultiple = 0.2;
		double ratioMatchMultiple = 10.0;
		double tmp = stringCounts.size() - numZeroStarsMatched;
		double totalNumPhrases = 0; // only phrases that have a score.
		for (String s : phraseImportance.keySet()) {
			if (phraseImportance.get(s) != 0) {
				totalNumPhrases++;
			}
		}
		double ratioMatched = tmp / totalNumPhrases;

		double tmpScore = tmpScoreForTF + ratioMatched * ratioMatchMultiple;
		double score = tmpScore / (totalNumPhrases * 6.0) * 5 + numMissingMustHavePhrases * negScoreForMissingPhrases;

		return score;
	} // double getScore2 (...) {

	/*
	 * GetCounts2
	 * 
	 * @param - matchedPhrases - Phrase, # of Stars (1-5, must have = 6)
	 * 
	 * @param - counts initialized to 0 - String, # of Stars (1-5, must have =
	 * 6)
	 * 
	 * checks and removes duplicates in matched phrases. It does this by
	 * checking to see if two phrase have the same location and the same score.
	 * 1. Initialize the retunred "results" and FOR Phrases with importance > 0
	 * put phrases, their importance, and their locations into arrays. Skipping
	 * over the ones that have importance of 0 (i.e., tags) means that when it
	 * comes to highlighting they will not be counted in the scores either :-( I
	 * guess I can kill that optimization. 2. then loop 3. if it is not the last
	 * element compare every one with the next one. 4. if the locations are the
	 * same && 5. scores are the same and if 6. buffers or source buffers are
	 * the same 7. then continue -- i.e., skip over this entry 8. else PROCESS
	 * and add to the counts
	 */
	Map<String, Integer> getCountsForPositiveStars(Map<Phrase, Integer> matchedPhrases) {
		// 1. put the phrases, their scores, and their location into arrays...
		HashMap<String, Integer> res = new HashMap<String, Integer>();
		ArrayList<Phrase> phraseArray = new ArrayList<Phrase>();
		ArrayList<Integer> impArray = new ArrayList<Integer>();
		ArrayList<String> phraseLocation = new ArrayList<String>();

		for (Phrase p : matchedPhrases.keySet()) {// can use addAll for
													// ArrayList
			if (matchedPhrases.get(p) != 0) { // skip over tags
				phraseArray.add(p);
				impArray.add(matchedPhrases.get(p));
				String phraseString = p.toString();
				int indx = phraseString.indexOf("@");
				phraseLocation.add(phraseString.substring(indx));
			}
		}

		// 2.
		int numPhrasesMatched = phraseArray.size();
		for (int i = 0; i < numPhrasesMatched; i++) {
			// 3.
			if (i != numPhrasesMatched - 1) {
				// 4.
				if (phraseLocation.get(i).compareTo(phraseLocation.get(i + 1)) == 0
						&& impArray.get(i) == impArray.get(i + 1)) {// 5.
					// Get the buffer or the sourceBuffer (for SynonymPhrase)
					// for 6.
					String s, sn;
					// get the buffers or the sourceBuffers
					Phrase ptmp = phraseArray.get(i);
					if (ptmp instanceof SynonymPhrase) {
						SynonymPhrase sp = (SynonymPhrase) ptmp;
						s = sp.getSourcePhrase();
					} else {
						s = ptmp.getBuffer();
					}
					ptmp = phraseArray.get(i + 1);
					if (ptmp instanceof SynonymPhrase) {
						SynonymPhrase sp = (SynonymPhrase) ptmp;
						sn = sp.getSourcePhrase();
					} else {
						sn = ptmp.getBuffer();
					}
					if (s.compareToIgnoreCase(sn) == 0) { // 6.
						continue; // 7. skips this phrase and move on to next
					}
				} // if (phraseLocation.get(i).compareTo(phraseLocation.get(i +
					// 1))
			} // if (i != numPhrasesMatched - 1)
				// 8. increment the count
			String s;
			Phrase p = phraseArray.get(i);
			if (p instanceof SynonymPhrase) {
				SynonymPhrase p2 = (SynonymPhrase) p;
				s = p2.getSourcePhrase(); // .trim().toLowerCase();
			} else {
				s = p.getBuffer(); // .trim().toLowerCase();
			}
			if (res.containsKey(s)) { // add the count of the source phrase
				res.put(s, res.get(s) + 1);
			} else {
				res.put(s, 1);
			}
		} // for (int i = 0; i < numPhrasesMatched; i++)
		return res;
	} // void getCounts2 (...) {

	/*
	 * Get Resume score
	 * 
	 * @param - matchedPhrases - Phrase, # of Stars (1-5, must have = 6)
	 * 
	 * @param - missingPhrases - String, # of Stars (1-5, must have = 6) Nice to
	 * have = 1 star Should have = 3 stars
	 */
	double getScoreOld(Map<Phrase, Integer> matchedPhrases, List<String> missingPhrases,
			Map<String, Integer> phraseImportance) {

		Set<String> uniquePhrase = new HashSet<String>();
		Map<String, Integer> phraseOccurence = new HashMap<String, Integer>();
		Map<String, Double> phraseScore = new HashMap<String, Double>();
		int numfound = 0; // Number of unique Matched Phrases
		// Maxscore = SUM(#STARS of all phrases)
		Double maxScore = (Double) session.getAttribute(SESSION_ATTRIBUTE.MAX_SCORE.toString());
		for (Phrase matchedPhrase : matchedPhrases.keySet()) {
			Integer temp = matchedPhrases.get(matchedPhrase);
			if (temp == null)
				temp = 0;
			double imp = temp;
			Integer occurence = phraseOccurence.get(matchedPhrase.getBuffer().trim());
			if (occurence == null)
				occurence = 0;
			occurence++;
			phraseOccurence.put(matchedPhrase.getBuffer().trim(), occurence);
			phraseScore.put(matchedPhrase.getBuffer().trim(), imp);
			if (uniquePhrase.contains(matchedPhrase.getBuffer()))
				continue;
			numfound++; // Only find number of unique phrases
			uniquePhrase.add(matchedPhrase.getBuffer());
		}
		double score = 0.0;
		if (matchedPhrases.keySet().size() > 0) {
			// Use square root instead, for both numerator and denomenator per
			// Esfandiar
			for (String phraseStr : phraseOccurence.keySet()) {
				int occurenceForPhrase = phraseOccurence.get(phraseStr);
				Double scoreForPhrase = phraseScore.get(phraseStr);
				score = score = scoreForPhrase * Math.sqrt(occurenceForPhrase);
			}
			// score = score * numfound / matchedPhrases.keySet().size();
			score = score * numfound / Math.sqrt(matchedPhrases.keySet().size());
		}
		boolean missingMustHave = false;
		for (String missing : missingPhrases) {
			Integer missingImp = phraseImportance.get(missing.toLowerCase().trim());
			if (missingImp != null && missingImp == 6) // Must have phrases
				missingMustHave = true;
		}
		if (maxScore != 0.0)
			score = score / maxScore;
		score = score * 5.0;
		if (missingMustHave)
			score = score - 5;
		return score;
	}

	/*
	 * Get Resume score
	 * 
	 * @param - matchedPhrases - Phrase, # of Stars (1-5, must have = 6)
	 * 
	 * @param - missingPhrases - String, # of Stars (1-5, must have = 6) Nice to
	 * have = 1 star Should have = 3 stars
	 */
	double getScore2(Map<Phrase, Integer> matchedPhrases, List<String> missingPhrasesNotUsed,
			Map<String, Integer> phraseImportance) {
		////// added later EB Nov 25, 2013
		if (matchedPhrases.size() == 0) {
			return 0.0;
		}
		// initialize all the counts for all phrases to 0
		Map<String, Integer> counts = new HashMap<String, Integer>();
		for (String s : phraseImportance.keySet()) {

			counts.put(s.trim().toLowerCase(), 0);
		}

		// now find the phrases -- original and equivalent phrases and count
		// those from the matchedPhrases. Here is the email from DEV:
		// Q: What if it is an equivalent phrase? Do I know what phrase it goes
		// with?
		// Ans: If it is equivalent phrase, the object is an instance of
		// SynonymPhrase and the original phrase is in the variable
		// sourcePhrase.
		//// count the number of the phrases. If it is an equivalent phrase,
		// count them main phrase.
		for (Phrase p : matchedPhrases.keySet()) {
			String s;

			// check to see if it is an equivalent phrase
			if (p instanceof SynonymPhrase) { // could have used getClass()
				SynonymPhrase p2 = (SynonymPhrase) p;
				s = p2.getSourcePhrase().trim().toLowerCase();
			} else { // it is the original phrase
				s = p.getBuffer().trim().toLowerCase();
			}
			Integer c = counts.get(s);
			if (c == null) {
				c = 1;
				counts.put(s, c);
			} else {
				c++;
				counts.put(s, c);
			}
		} // for (Phrase p : matchedPhrases.keySet())

		// Testing purpose dumping the missing phrase and importance
		// for(String missing: missingPhrases)
		// {
		// int score = phraseImportance.get(missing.toLowerCase().trim());
		// try {
		// fstream.write("Missing Phrase: " + missing + "\t score: " + score +
		// "\n");
		// } catch (IOException ex) {
		// java.util.logging.Logger.getLogger(ResumeProcessingThread.class.getName()).log(Level.SEVERE,
		// null, ex);
		// }
		// }

		/////////////////////////////////////
		// Now we calculate the scores for various formulas.
		// The new formulat is the following:
		// 1. Count_i -> square root Count_i
		// if Count_i > maxSqrtCount
		// then the Count_i = maxSqrtCount
		// (right now the if the count > 7 it makes the count sqrt 7)
		// 2. socre is 2.1 weighted average score of the words divided by the
		// 2.2 average score of the words multiplied by the 2.3 Hamming
		// distance times 5 and rounded off to 5.
		// If we have a must have phrase we subtract 6 from the score,
		// and for each additional must haves we subtrast another 1.
		// Now 2. is broken down to the following.

		// first lowercase the phraseImportance.
		Map<String, Integer> phImpLower = new HashMap<String, Integer>();
		for (String s : phraseImportance.keySet()) {
			int score = phraseImportance.get(s);
			// try {
			// fstream.write("Phrase: " + s + "\t" + "Importance : " +
			// phraseImportance.get(s) +"-> "+ tmp);
			// fstream.write("\n");
			// } catch (IOException ex) {
			// java.util.logging.Logger.getLogger(ResumeProcessingThread.class.getName()).log(Level.SEVERE,
			// null, ex);
			// }
			phImpLower.put(s.trim().toLowerCase(), score);
		}

		// 2.1. Weighted average
		double sumNum = 0;
		double sumScores = 0;
		for (String s : counts.keySet()) {
			if (phImpLower.get(s) == 0) { // if it is just a tag do not count
				continue;
			} else {
				double count = counts.get(s);
				double sqrtCount = Math.sqrt(count);
				sqrtCount = sqrtCount > maxSqrtCount ? maxSqrtCount : sqrtCount;
				sumNum += sqrtCount;
				sumScores += sqrtCount * phImpLower.get(s);
			}
			// try {
			// fstream.write("Matched Phrase: " + s + "\t" + "Occurence: " +
			// num);
			// } catch (IOException ex) {
			// java.util.logging.Logger.getLogger(ResumeProcessingThread.class.getName()).log(Level.SEVERE,
			// null, ex);
			// }
		}
		double weightedAverage = sumScores / sumNum;

		// 2.2 average score of the phrases and number of missing must haves
		int numMissingMustHaves = 0;
		int numOfTags = 0;
		sumScores = 0;
		for (String s : phImpLower.keySet()) {
			int score = phImpLower.get(s);
			sumScores += score;
			if (score >= 6 && counts.get(s) == 0) {
				numMissingMustHaves++;
			}
			if (score == 0)
				numOfTags++;
		}
		double averagePhraseScore = sumScores / (double) (phImpLower.size() - numOfTags);

		// 2.3 the Hamming distance
		// double hammingDistance = counts.size() / (double) phImpLower.size();
		//
		// 2.3 Modified Square ROOT Hamming Distance
		// If the score is above or equal to the
		// HammingDistScoreThreshold we use 1.0 or hammingDistIfAboveScoreThresh
		// if it is below we use hammingDistIfBelowScoreThresh (or .8)
		// tags do not count!
		double hammingNumerator = 0.0;
		double hammingDenuminator = 0.0;
		for (String s : phImpLower.keySet()) {
			if (phImpLower.get(s) > 0) { // if the importance is greater than
											// zero then add it to Hamming
											// distance
				double c = phImpLower.get(s) >= HammingDistScoreThreshold ? hammingDistIfAboveScoreThresh
						: hammingDistIfBelowScoreThresh;
				hammingDenuminator += c;
				if (counts.get(s) != 0) {
					hammingNumerator += c;
				}
			}
		}
		double hammingDistance;
		if (hammingDenuminator == 0) { // it better not be otherwise everything
										// is just tags
			hammingDistance = 0;
		} else {
			hammingDistance = Math.sqrt(hammingNumerator / hammingDenuminator); // sqrt
																				// of
																				// Hamming
																				// distance
		}

		// Now measuring the values without the must have
		double score = 0;
		if (averagePhraseScore == 0) { // then the weighted average should be
										// zoro too (and same with Hamming) in
										// which case we have 0/0
			score = 0;
		} else {
			score = weightedAverage / averagePhraseScore * hammingDistance * 5.0;
		}
		score = score > 5.0 ? 5.0 : score;

		// now account for the Must Haves
		// we could have: score = score - 5.0 * numMissingMustHaves
		// but multiplications would make the score too negative
		// so instead we use score = score - (5.0 + numMissingMustHaves)
		// so for each missing phrase we subtract another negative 1 and the
		// score will get more and more negative.
		if (numMissingMustHaves > 0) {
			score = score - (5.0 + numMissingMustHaves);
		}
		// try {
		// fstream.write("Resume name: "+resume.getName() +" weightedaverage: "
		// + weightedAverage + "\t" + "average: " + averagePhraseScore + "\t" +
		// "hamming distance: " + hammingDistance + "\t" + "score: " + score);
		// fstream.close();
		// } catch (IOException ex) {
		// java.util.logging.Logger.getLogger(ResumeProcessingThread.class.getName()).log(Level.SEVERE,
		// null, ex);
		// }
		return score;
	}
}