package com.textonomics.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.textonomics.user.User;
import com.textonomics.wordnet.model.Synset;
import com.textonomics.wordnet.model.SynsetWord;
import com.textonomics.wordnet.model.Word;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import com.textonomics.wordnet.model.WordnetDataProviderException;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This servlet allows the user to add, delete, update words from the underlying
 * database. This servlet (and corresponding interface - WordManager.jsp) was
 * meant for Margaret/Erin but never got used, nor continued to evolve....
 *
 * @author Nuno Seco
 *
 */
public class WordManager extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {
		super.init();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (user == null) {
			response.sendRedirect("");
			return;
		}

		try {
			WordNetDataProviderPool.getInstance().acquireProvider(); // Per
																		// request
																		// connection;
																		// should
																		// be
																		// changed
																		// to
																		// seesion
																		// based
																		// connection
																		// in
																		// alpha
			// system
			String op = (String) request.getParameter("op");

			if (op == null || op.equals("lookupword")) {
				String buffer = request.getParameter("lookupword");
				Word word = Word.getWordByString(buffer);
				session.setAttribute("lookupword", word);
			} else if (op.equals("addanothersynset")) {
				int id = Integer.parseInt(request.getParameter("addanothersynset"));
				Synset synset = Synset.getSynsetById(id);
				Word word = (Word) session.getAttribute("lookupword");
				SynsetWord sw = new SynsetWord();
				sw.setIdSynset(synset.getId());
				sw.setIdWord(word.getId());
				WordNetDataProviderPool.getInstance().getDataProvider().insertSynsetWord(sw);
				WordNetDataProviderPool.getInstance().getDataProvider().commit();
			} else if (op.equals("removesynset")) {
				int synsetId = Integer.parseInt(request.getParameter("removesynset"));
				Word word = (Word) session.getAttribute("lookupword");
				WordNetDataProviderPool.getInstance().getDataProvider().deleteSynsetWord(synsetId, word.getId());
				WordNetDataProviderPool.getInstance().getDataProvider().commit();
			} else if (op.equals("addword")) {
				String buffer = request.getParameter("addword");
				Word word = new Word();
				word.setBuffer(buffer);
				WordNetDataProviderPool.getInstance().getDataProvider().insertWord(word);
				WordNetDataProviderPool.getInstance().getDataProvider().commit();
				session.setAttribute("lookupword", word);
			} else if (op.equals("deleteword")) {
				Word word = (Word) session.getAttribute("lookupword");
				WordNetDataProviderPool.getInstance().getDataProvider().deleteWord(word);
				WordNetDataProviderPool.getInstance().getDataProvider().commit();
				session.removeAttribute("lookupword");
			}
			request.getRequestDispatcher("WordManager.jsp").forward(request, response);

		} catch (WordnetDataProviderException ex) {
			throw new ServletException(ex);
		} finally {
			WordNetDataProviderPool.getInstance().releaseProvider();
		}
	}

}
