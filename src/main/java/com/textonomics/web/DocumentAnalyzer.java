/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textnomics.data.JobPosting;
import com.textnomics.data.NotSelectedPhrase;
import com.textnomics.data.ResumeScore;
import com.textnomics.data.SharedJobPosting;
import com.textnomics.data.SortPhrase;
import com.textnomics.data.UserTemplatePhrase;
import com.textonomics.Phrase;
import com.textonomics.PhraseList;
import com.textonomics.PlatformProperties;
import com.textonomics.db.UserDBAccess;
import com.textonomics.nlp.NGram;
import com.textonomics.nlp.Token;
import com.textonomics.openoffice.OOFILTER_TYPE;
import com.textonomics.subscription.SubscriptionService;
import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderPool;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Vinod
 */
public class DocumentAnalyzer extends HttpServlet {

	static Logger logger = Logger.getLogger(DocumentAnalyzer.class);
	/*
	 * //private SentenceSpliter sentenceDetector; private NLPSuite suite;
	 * //private POSTagger tagger; private Chunker chunker;
	 */

	/*
	 * @Override public void init() throws ServletException { try { suite = new
	 * DefaultNLPSuite(); // sentenceDetector = suite.getSpliter(); // tagger =
	 * suite.getTagger(); //chunker = suite.getChunker(); } catch (IOException
	 * ex) { ex.printStackTrace();
	 * Logger.getLogger(DocumentAnalyzer.class.getName()).log(Level.SEVERE,
	 * null, ex); } }
	 */

	public static int DEFAULT_TAG_IMPORTANCE = 0;

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		boolean forwarded = false;
		try {

			WordNetDataProviderPool.getInstance().acquireProvider();

			session.setAttribute(SESSION_ATTRIBUTE.RANKING_INPROGRESS.toString(), true);
			User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			String uploadDir = (String) session.getAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString()); // the
																										// upload
																										// dir
																										// (for
																										// resumes)
																										// of
																										// the
																										// user
			session.removeAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());
			List<String> userSelectedPhrases = new ArrayList<String>(); // Stores
																		// the
																		// list
																		// of
																		// the
																		// user
																		// "selected"
																		// phrases
																		// index.
																		// Here
																		// the
																		// index
																		// is
																		// the
																		// phrase
																		// buffer
																		// itself.
			List<String> userEnteredPhrases = new ArrayList<String>(); // Stores
																		// the
																		// list
																		// of
																		// the
																		// user
																		// "entered"
																		// phrases
																		// text
			List<Boolean> requiredPhrase = new ArrayList<Boolean>();
			List<Integer> stars = new ArrayList<Integer>();
			Map<String, Integer> phraseImportance = new HashMap<String, Integer>();
			Set<String> tags = new HashSet<String>();
			session.setAttribute(SESSION_ATTRIBUTE.SELECTED_PHRASES.toString(), userSelectedPhrases);
			session.setAttribute(SESSION_ATTRIBUTE.ENTERED_PHRASES.toString(), userEnteredPhrases);
			session.setAttribute(SESSION_ATTRIBUTE.TAGS.toString(), tags);
			session.setAttribute(SESSION_ATTRIBUTE.REQUIRED_PHRASES.toString(), requiredPhrase);
			session.setAttribute(SESSION_ATTRIBUTE.PHRASE_IMPORTANCE.toString(), phraseImportance);
			JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());

			Collection<Phrase> phrasesToBeMatched = new ArrayList<Phrase>(); // Stores
																				// the
																				// original
																				// set
																				// of
																				// Phrases
																				// selected
																				// by
																				// the
																				// user
			List<Integer> phrasesToBeMatchedImp = new ArrayList<Integer>();
			// Collection<PhraseList> selectedNonLemmaPhrases = new
			// ArrayList<PhraseList>();

			boolean simpleRating = user.isSimpleRating();
			if ("false".equals(request.getParameter("usestars")))
				simpleRating = true;
			else if ("true".equals(request.getParameter("usestars")))
				simpleRating = false;
			// VINOD: Get the indexes of the phrases from the phrase selection
			// page
			Map<String, String[]> paramMap = request.getParameterMap();
			for (String param : paramMap.keySet()) {
				if (param.startsWith("suggphrases")) {
					String[] values = paramMap.get(param);
					String ind = param.substring(param.indexOf("_") + 1);

					if (request.getParameter("required_" + ind) == null
							&& getInt(request.getParameter("rating_" + ind)) == 0
							&& (request.getParameter("tag_" + ind) == null
									|| request.getParameter("tag_" + ind).length() == 0))
						continue;
					String value = values[0].trim();
					userSelectedPhrases.add(values[0]);

					stars.add(getInt(request.getParameter("rating_" + ind)));
					phraseImportance.put(values[0].toLowerCase().trim(), getInt(request.getParameter("rating_" + ind)));
					if (request.getParameter("required_" + ind) != null
							&& "true".equals(request.getParameter("required_" + ind))) {
						requiredPhrase.add(true);
						phraseImportance.put(values[0].toLowerCase().trim(), 6);
					} else {
						if ("maybe1".equals(request.getParameter("required_" + ind))) {
							stars.set(stars.size() - 1, 1);
							phraseImportance.put(values[0].toLowerCase().trim(), 1);
						} else if ("maybe3".equals(request.getParameter("required_" + ind))) {
							stars.set(stars.size() - 1, 3);
							phraseImportance.put(values[0].toLowerCase().trim(), 3);
						}
						requiredPhrase.add(false);
					}

					if (request.getParameter("tag_" + ind) != null && request.getParameter("tag_" + ind).length() > 0) {
						tags.add(values[0]);
						if (phraseImportance.get(values[0].toLowerCase().trim()) == 0)
							phraseImportance.put(values[0].toLowerCase().trim(), DEFAULT_TAG_IMPORTANCE);
						if (phraseImportance.get(values[0].toLowerCase()) == null
								|| phraseImportance.get(values[0].toLowerCase()) == 0)
							phraseImportance.put(values[0].toLowerCase(), DEFAULT_TAG_IMPORTANCE);
						// System.out.println("Tagged phrase:" + values[0] + "
						// stars:" +
						// phraseImportance.get(values[0].toLowerCase()));
					}
					// System.out.println("Selected phrase:" + values[0] + "
					// rating:" + request.getParameter("rating_" + ind) + "
					// required:" + request.getParameter("required_" + ind));
				} else if (param.startsWith("userphrase")) {
					String[] values = paramMap.get(param);
					String ind = param.substring(param.indexOf("_") + 1);
					String required = request.getParameter("required_" + ind);
					String tag = request.getParameter("tag_" + ind);
					if (param.startsWith("userphraser") && required == null)
						continue;
					for (String value : values) {
						value = value.trim();
						if (value.length() > 0) {
							userEnteredPhrases.add(value);
							phraseImportance.put(value.toLowerCase(), getInt(request.getParameter("rating_" + ind)));
							if ("maybe1".equals(required)) {
								phraseImportance.put(value.toLowerCase(), 1);
							} else if ("maybe3".equals(required)) {
								phraseImportance.put(value.toLowerCase(), 3);
							} else if ("true".equals(required)) {
								phraseImportance.put(value.toLowerCase(), 6);
							}
							if (tag != null) {
								tags.add(value);
								if (phraseImportance.get(value.toLowerCase()) == 0)
									phraseImportance.put(value.toLowerCase(), DEFAULT_TAG_IMPORTANCE);
							}
							System.out.println("user Phrase:" + value.trim() + " Importance:"
									+ request.getParameter("rating_" + ind));
						}
					}
				} else if (param.startsWith("tmplphrases")) {
					String[] values = paramMap.get(param);
					String ind = param.substring(param.indexOf("_") + 1);
					String required = request.getParameter("required_" + ind);
					String tag = request.getParameter("tag_" + ind);
					if (request.getParameter("required_" + ind) == null
							&& getInt(request.getParameter("rating_" + ind)) == 0
							&& request.getParameter("tag_" + ind) == null)
						continue;
					for (String value : values) {
						value = value.trim();
						// System.out.println("Resume phrase:" + value + "
						// rating:" + request.getParameter("rating_" + ind) + "
						// required:" + required + " tag:" + tag);
						if (value.length() > 0) {
							userEnteredPhrases.add(value);
							phraseImportance.put(value.toLowerCase(), getInt(request.getParameter("rating_" + ind)));
							if ("maybe1".equals(required)) {
								phraseImportance.put(value.toLowerCase(), 1);
							} else if ("maybe3".equals(required)) {
								phraseImportance.put(value.toLowerCase(), 3);
							} else if ("true".equals(required)) {
								phraseImportance.put(value.toLowerCase(), 6);
							}
							if (tag != null && tag.equals("tag")) {
								tags.add(value);
								if (phraseImportance.get(value.toLowerCase()) <= 0)
									phraseImportance.put(value.toLowerCase(), DEFAULT_TAG_IMPORTANCE);
							}
							// System.out.println("Resume Phrase:" +
							// value.trim() + " Importance:" +
							// request.getParameter("rating_" + ind));
						}
					}
				} else if (param.startsWith("resumephrase")) {
					String[] values = paramMap.get(param);
					String ind = param.substring(param.indexOf("_") + 1);
					String required = request.getParameter("required_" + ind);
					String tag = request.getParameter("tag_" + ind);
					if (request.getParameter("required_" + ind) == null
							&& getInt(request.getParameter("rating_" + ind)) == 0
							&& request.getParameter("tag_" + ind) == null)
						continue;
					for (String value : values) {
						value = value.trim();
						// System.out.println("Resume phrase:" + value + "
						// rating:" + request.getParameter("rating_" + ind) + "
						// required:" + required + " tag:" + tag);
						if (value.length() > 0) {
							userEnteredPhrases.add(value);
							phraseImportance.put(value.toLowerCase(), getInt(request.getParameter("rating_" + ind)));
							if ("maybe1".equals(required)) {
								phraseImportance.put(value.toLowerCase(), 1);
							} else if ("maybe3".equals(required)) {
								phraseImportance.put(value.toLowerCase(), 3);
							} else if ("true".equals(required)) {
								phraseImportance.put(value.toLowerCase(), 6);
							}
							if (tag != null && tag.equals("tag")) {
								tags.add(value);
								if (phraseImportance.get(value.toLowerCase()) <= 0)
									phraseImportance.put(value.toLowerCase(), DEFAULT_TAG_IMPORTANCE);
							}
							// System.out.println("Resume Phrase:" +
							// value.trim() + " Importance:" +
							// request.getParameter("rating_" + ind));
						}
					}
				}
			}
			Date currDate = new Date();
			long timestamp = currDate.getTime();
			Collection<PhraseList> lemmaPhraseLists = (Collection<PhraseList>) session
					.getAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString());
			Collection<PhraseList> nonLemmaPhraseLists = (Collection<PhraseList>) session
					.getAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString());
			Iterator<PhraseList> iterator = nonLemmaPhraseLists.iterator();

			// // Delete any previously saved phrases
			// new SortPhrase().deleteObjects("JOB_ID = " + posting.getId() + "
			// and USER_NAME = " + DBAccess.toSQL(user.getLogin()));

			// VINOD: Get the user selected phrases from the original lemmatized
			// phrases list
			for (PhraseList lemmaPhraseList : lemmaPhraseLists) {
				PhraseList unlemma = iterator.next();
				SortPhrase sp = new SortPhrase();
				sp.setJobId(posting.getId());
				sp.setLemmatizedPhrase(lemmaPhraseList.iterator().next().getPhrase().getPhrase().getBuffer());
				String unlemmaText = unlemma.iterator().next().getPhrase().getBuffer();
				sp.setUserName(user.getEmail());
				sp.setSelected(false);
				sp.setSavedTime(timestamp);
				sp.setOriginalPhrase(unlemmaText);
				sp.setIndustry(posting.getIndustry());
				NotSelectedPhrase nsp = new NotSelectedPhrase();
				nsp.setJobId(posting.getId());
				nsp.setPhrase(unlemmaText);
				nsp.setSessionDate(currDate);
				nsp.setIndustry(posting.getIndustry());
				nsp.setUserName(user.getEmail());
				int i = 0;
				boolean required = false;
				int starRating = 0;
				boolean tagged = false;
				boolean selected = false;
				for (String selectedPhrase : userSelectedPhrases) // find out
																	// which one
																	// is
																	// suggested
				{
					Phrase phraseToBeCheckedForSelection = lemmaPhraseList.iterator().next().getPhrase();

					if (phraseToBeCheckedForSelection.getBuffer().equalsIgnoreCase(selectedPhrase)) { // if
																										// the
																										// phrase
																										// was
																										// checkmarked
																										// by
																										// the
																										// user

						phrasesToBeMatched.add(lemmaPhraseList.getFirst()); // Add
																			// it
																			// to
																			// the
																			// list
																			// of
																			// selected
																			// lemmatized
																			// phrases
						selected = true;
						required = requiredPhrase.get(i);
						starRating = stars.get(i);
						if (tags.contains(selectedPhrase)) {
							tagged = true;
							System.out.println("Tag:" + selectedPhrase + " Rating:" + starRating);
							// if (starRating == 0) starRating = 3;
						}
						if (required)
							phrasesToBeMatchedImp.add(6);
						else
							phrasesToBeMatchedImp.add(starRating);
					}
					i++;
				}

				// Save phrase to database
				try {
					// List<SortPhrase> existPhrases = sp.getObjects("JOB_ID = "
					// + posting.getId() + " and LEMMATIZED_PHRASE = " +
					// DBAccess.toSQL(sp.getLemmatizedPhrase()) + " and
					// USER_NAME = " + DBAccess.toSQL(sp.getUserName()));
					// if (existPhrases.size() > 0)
					// {
					// sp = existPhrases.get(0);
					// if (sp.getOriginalPhrase().indexOf(unlemmaText) == -1)
					// sp.setOriginalPhrase(sp.getOriginalPhrase() + "," +
					// unlemmaText);
					// }
					// else
					// {
					// sp.setOriginalPhrase(unlemmaText);
					// }
					if (selected) {
						sp.setSelected(true);
						sp.setStars(new Double(starRating));
						sp.setRequired(required);
						sp.setScore(starRating);
						if (required)
							sp.setScore(6);
						if (simpleRating) {
							if (starRating == 1)
								sp.setScore(-1);
							else if (starRating == 3)
								sp.setScore(-2);
							else if (required)
								sp.setScore(-3);
						}
						sp.setTag(tagged);
						if (sp.getId() > 0)
							sp.update();
						else
							sp.insert();
					} else {
						nsp.insert();
					}
				} catch (Exception x) {
					x.printStackTrace();
				}
			}

			session.setAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED_IMPORTANCE.toString(), phrasesToBeMatchedImp);

			// Construct phrase objects for user entered phrases and add them to
			// the set of selected phrases
			int phraseIndex = 0;
			Map<String, Integer> resumePhrasesMap = (Map<String, Integer>) session
					.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
			List<UserTemplatePhrase> templatePhrases = (List<UserTemplatePhrase>) session
					.getAttribute(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString());
			if (templatePhrases == null)
				templatePhrases = new ArrayList<UserTemplatePhrase>();
			Set<String> templatePhraseSet = new HashSet<String>();
			for (UserTemplatePhrase templatePhrase : templatePhrases)
				templatePhraseSet.add(templatePhrase.getPhrase());
			for (String userEnteredPhrase : userEnteredPhrases) {
				phraseIndex++;

				int tokenPosition = 0;
				NGram ngram = new NGram();
				String[] tokenBuffers = userEnteredPhrase.split(" ");
				StringBuilder phraseBuffer = new StringBuilder();

				// System.out.println("user entered phrase: " +
				// userEnteredPhrase);
				for (String tokenBuffer : tokenBuffers) {
					Token token = new Token(tokenBuffer, tokenPosition, null);
					tokenPosition += tokenBuffer.length() + 1;

					token.setChunk("No Chunk");
					token.setPos("No POS");
					ngram.addElement(token);

					phraseBuffer.append(tokenBuffer + " ");
				}

				// System.out.println("ngram: " + ngram);
				Phrase phrase = new Phrase(phraseBuffer.toString(), ngram, phraseIndex);

				// System.out.println("Adding user entered phrase: " +
				// phraseBuffer.toString());
				phrasesToBeMatched.add(phrase);
				if (phraseImportance.containsKey(phraseBuffer.toString().toLowerCase().trim())) {
					phrasesToBeMatchedImp.add(phraseImportance.get(phraseBuffer.toString().toLowerCase().trim()));
					System.out.println("Phrase:" + phraseBuffer.toString() + " Importance:"
							+ phraseImportance.get(phraseBuffer.toString().toLowerCase().trim()));
				} else
					phrasesToBeMatchedImp.add(0);
				SortPhrase sp = new SortPhrase();
				sp.setJobId(posting.getId());
				sp.setLemmatizedPhrase(userEnteredPhrase);
				sp.setOriginalPhrase(userEnteredPhrase);
				sp.setUserName(user.getEmail());
				sp.setSavedTime(timestamp);
				sp.setIndustry(posting.getIndustry());
				if (templatePhraseSet != null && templatePhraseSet.contains(userEnteredPhrase.trim()))
					sp.setTemplatePhrase(true);
				else if (resumePhrasesMap != null && resumePhrasesMap.containsKey(userEnteredPhrase.trim()))
					sp.setResumePhrase(true);
				else
					sp.setUserentered(true);
				// Save phrase to database
				try {
					// List<SortPhrase> existPhrases = sp.getObjects("JOB_ID = "
					// + posting.getId() + " and LEMMATIZED_PHRASE = " +
					// DBAccess.toSQL(sp.getLemmatizedPhrase()) + " and
					// USER_NAME = " + DBAccess.toSQL(sp.getUserName()));
					// if (existPhrases.size() > 0)
					// {
					// sp = existPhrases.get(0);
					// }

					sp.setSelected(true);
					Integer imp = phraseImportance.get(userEnteredPhrase.toLowerCase());
					if (imp != null && imp != 0)
						sp.setStars(imp.doubleValue());
					if (imp != null && imp == 6)
						sp.setRequired(true);
					if (simpleRating) {
						if (sp.getStars() == null || sp.getStars() == 0)
							sp.setScore(0);
						else if (sp.getStars() == 1)
							sp.setScore(-1);
						else if (sp.getStars() == 3)
							sp.setScore(-2);
						else if (sp.getStars() == 6)
							sp.setScore(-3);
					}
					if (tags.contains(userEnteredPhrase))
						sp.setTag(true);
					if (sp.getId() > 0)
						sp.update();
					else
						sp.insert();
				} catch (Exception x) {
					x.printStackTrace();
				}
			}

			session.setAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED.toString(), phrasesToBeMatched);
			double maxScore = 0.0;
			for (Integer imp : phrasesToBeMatchedImp) {
				maxScore = maxScore + imp;
			}
			session.setAttribute(SESSION_ATTRIBUTE.MAX_SCORE.toString(), maxScore);
			// Processing resumes starts here

			File resumes = (File) session.getAttribute(SESSION_ATTRIBUTE.TARGET_FILE.toString());
			List<File> resumeFiles = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
			if (resumeFiles == null) {
				Thread.sleep(2000);
				resumeFiles = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
			}
			if (resumeFiles == null)
				throw new Exception("Processed resumeFiles not found");

			Integer freePostings = user.getFreePostings();
			if (freePostings == null)
				freePostings = 0;
			boolean validSubscription = false;
			if (freePostings > 0) {
				validSubscription = true;
				freePostings--;
				user.setFreePostings(freePostings);
				UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();
				userDataProvider.updateUserField(user, "freePostings");
				userDataProvider.commit();
				UserDataProviderPool.getInstance().releaseProvider();
			}
			if (!validSubscription)
				validSubscription = SubscriptionService.isValidSubscription(user, posting, resumeFiles.size(), true);
			if (validSubscription) {
				posting.setPaid(true);
				int runs = posting.getNumRuns();
				posting.setNumRuns(runs + 1);
				if (posting.getNumRuns() > SubscriptionService.MAX_POSTING_RUNS) {
					posting.setPaid(false);
					posting.setNumRuns(0);
				}
				posting.update();
			}
			if (user.getLogin().equals("dev.gude@gmailx.com") && !validSubscription) {
				request.getRequestDispatcher("request_payment.jsp").forward(request, response);
				return;
			}

			// Document document = new Document();
			boolean htmlDone = false;
			List<File> highlightedFiles = new ArrayList<File>();
			List<File> highlightedHtmls = new ArrayList<File>();
			List<Map<Phrase, Integer>> matchedPhraseList = new ArrayList<Map<Phrase, Integer>>();
			List<List<String>> unmatchedPhraseList = new ArrayList<List<String>>();
			List<Boolean> processedByTika = new ArrayList<Boolean>();
			List<Double> scores = new ArrayList<Double>();
			List<Integer> userScores = new ArrayList<Integer>();
			List<String> candidateNames = new ArrayList<String>();
			List<String> candidateEmails = new ArrayList<String>();
			List<String> candidateAddresses = new ArrayList<String>();
			List<Double> candidateDistances = new ArrayList<Double>();
			for (int i = 0; i < resumeFiles.size(); i++) {
				highlightedFiles.add(null);
				highlightedHtmls.add(null);
				matchedPhraseList.add(null);
				unmatchedPhraseList.add(null);
				processedByTika.add(false);
				scores.add(null);
				userScores.add(null);
				candidateNames.add(null);
				candidateEmails.add(null);
				candidateAddresses.add(null);
				candidateDistances.add(null);
			}
			session.setAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString(), highlightedFiles);
			session.setAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_HTMLRESUMES.toString(), highlightedHtmls);
			session.setAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString(), matchedPhraseList);
			session.setAttribute(SESSION_ATTRIBUTE.UNMATCHED_PHRASES.toString(), unmatchedPhraseList);
			session.setAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString(), scores);
			session.setAttribute(SESSION_ATTRIBUTE.TAGGED_HTMLRESUMES.toString(), highlightedHtmls);
			session.setAttribute(SESSION_ATTRIBUTE.PROCESSED_BY_TIKA.toString(), processedByTika);
			session.setAttribute(SESSION_ATTRIBUTE.USER_SCORES.toString(), userScores);
			session.setAttribute(SESSION_ATTRIBUTE.CANDIDATE_NAMES.toString(), candidateNames);
			session.setAttribute(SESSION_ATTRIBUTE.CANDIDATE_EMAILS.toString(), candidateEmails);
			session.setAttribute(SESSION_ATTRIBUTE.CANDIDATE_ADDRESSES.toString(), candidateAddresses);
			session.setAttribute(SESSION_ATTRIBUTE.CANDIDATE_DISTANCES.toString(), candidateDistances);
			boolean showResumes = true;
			boolean showPosting = true;
			if (!posting.getUserName().equals(user.getLogin())) {
				showResumes = false;
				showPosting = false;
				SharedJobPosting sjp = (SharedJobPosting) session
						.getAttribute(SESSION_ATTRIBUTE.SHARED_POSTING.toString());
				if (sjp != null) {
					if (sjp.isPerm2() || sjp.isPerm3())
						showResumes = true;
					if (sjp.isPerm1())
						showPosting = false;
				}
			}

			boolean useStars = true;
			if ("false".equals(request.getParameter("usestars")))
				useStars = false;
			session.setAttribute(SESSION_ATTRIBUTE.USE_STARS.toString(), useStars);
			// session.setAttribute(SESSION_ATTRIBUTE.PHRASE_STARS.toString(),
			// stars);
			session.setAttribute(SESSION_ATTRIBUTE.PHRASE_REQUIRED.toString(), requiredPhrase);
			Integer numleft = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
			boolean useTika = "true".equalsIgnoreCase(
					PlatformProperties.getInstance().getUserProperty("com.textnomics.tika_sentence_detector"));
			if (numleft == null)
				numleft = 0;
			if ((numleft != null && numleft > 5) || resumeFiles.size() > 100) {
				logger.info("Forwarding request, left to process:" + numleft);
				if (showResumes)
					request.getRequestDispatcher("/stillProcessing.jsp").forward(request, response);
				else
					request.getRequestDispatcher("/Finished.jsp").forward(request, response);

				AnalyzeThread athread = new AnalyzeThread(user, session, posting, resumeFiles, useTika,
						validSubscription);
				athread.start();
				forwarded = true;
			} else {
				analyzeResumes(user, session, posting, resumeFiles, useTika, validSubscription);
			}

			if (!forwarded) {
				if (showResumes && resumeFiles.size() > 0)
					request.getRequestDispatcher("/ranked_resumes.jsp?showPosting=" + showPosting).forward(request,
							response);
				else
					request.getRequestDispatcher("/Finished.jsp").forward(request, response);
			}
			forwarded = true;
			UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();
			if (request.getParameter("usestars") != null) {
				if ("false".equals(request.getParameter("usestars")))
					user.setSimpleRating(true);
				else
					user.setSimpleRating(false);
				userDataProvider.updateUserField(user, "SimpleRating");
			}
			userDataProvider.commit();

		} catch (Exception e) {
			e.printStackTrace();
			session.removeAttribute(SESSION_ATTRIBUTE.RANKING_INPROGRESS.toString());
			if (!forwarded)
				request.getRequestDispatcher("/home.jsp?error=" + e.getMessage()).forward(request, response);
		} finally {
			WordNetDataProviderPool.getInstance().releaseProvider();
			UserDataProviderPool.getInstance().releaseProvider();
		}
	}

	public static void analyzeResumes(User user, HttpSession session, JobPosting posting, List<File> resumeFiles,
			boolean useTika, boolean validSubscription) throws Exception {
		analyzeResumes(user, session, posting, resumeFiles, useTika, null, validSubscription);
	}

	public static void analyzeResumes(User user, HttpSession session, JobPosting posting, List<File> resumeFiles,
			boolean useTika, Integer onlyIndex, boolean validSubscription) throws Exception {
		System.out.println("ResumeFiles:" + resumeFiles.size() + " index:" + onlyIndex);
		try {
			session.setAttribute(SESSION_ATTRIBUTE.RANKING_INPROGRESS.toString(), true);
			Integer numleft = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
			if (numleft == null)
				numleft = 0;
			int timeout = numleft * 12; // 5 seconds
			while (numleft > 0) {
				session.setAttribute(SESSION_ATTRIBUTE.ANALYZER_WAITING.toString(), true);
				logger.info("Waiting for resume processing, left to process:" + numleft);
				Thread.sleep(5000);
				if (timeout-- < 0)
					throw new Exception("Processed Resumes not completed in time");
				numleft = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
				if (numleft == null)
					numleft = 0;
			}
			ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
			for (int fileIndex = 0; fileIndex < resumeFiles.size(); fileIndex++) {
				if (onlyIndex != null && fileIndex != onlyIndex)
					continue;
				File resumeFile = resumeFiles.get(fileIndex);
				ResumeProcessingThread rpt = new ResumeProcessingThread(resumeFile, session, fileIndex, !useTika);

				pool.execute(rpt);
			}

			try {
				// Wait till all the threads in the pool are finished

				pool.shutdown();
				pool.awaitTermination(10, TimeUnit.DAYS);
				System.out.println("Threads completed");

			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}

			// Increment num of resumes processed
			try {
				Integer num = user.getNumProcessed();
				if (num == null)
					num = 0;
				user.setNumProcessed(num + resumeFiles.size());
				UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();
				userDataProvider.updateUserField(user, "numProcessed");
				userDataProvider.commit();
				List<Integer> ranks = new ArrayList<Integer>();
				for (int i = 0; i < resumeFiles.size(); i++)
					ranks.add(i);
				List<Double> scores = (List<Double>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
				List<Integer> userScores = (List<Integer>) session
						.getAttribute(SESSION_ATTRIBUTE.USER_SCORES.toString());
				List<String> addresses = (List<String>) session
						.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_ADDRESSES.toString());
				List<Double> distances = (List<Double>) session
						.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_DISTANCES.toString());
				List<String> emails = (List<String>) session
						.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_EMAILS.toString());
				List<String> names = (List<String>) session.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_NAMES.toString());

				List<Double> sortedScores = new ArrayList<Double>();
				sortedScores.addAll(scores);
				for (int i = 0; i < sortedScores.size(); i++) {
					for (int j = i; j < sortedScores.size(); j++) {
						Double scorej = sortedScores.get(j);
						if (scorej == null)
							scorej = 0.;
						Double scorei = sortedScores.get(i);
						if (scorei == null)
							scorei = 0.;
						if (scorej > scorei) {
							double score = scorei;
							Integer rank = ranks.get(i);

							sortedScores.set(i, scorej);
							ranks.set(i, ranks.get(j));

							sortedScores.set(j, score);
							ranks.set(j, rank);
						}
					}
				}
				String zipcode = posting.getLocation();

				Date now = new Date();
				for (int i = 0; i < resumeFiles.size(); i++) {
					Integer userScore = null;
					ResumeScore rs = new ResumeScore();
					rs.setJobId(posting.getId());
					String fileName = resumeFiles.get(i).getName();
					fileName = fileName.replace('\\', '/');
					int ind = fileName.lastIndexOf("/");
					if (ind != -1)
						fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
					if (fileName.indexOf("_") != -1)
						fileName = fileName.substring(fileName.indexOf("_") + 1);
					String folderName = resumeFiles.get(i).getParent();
					folderName = folderName.replace('\\', '/');
					ind = folderName.lastIndexOf("/");
					if (ind != -1)
						folderName = folderName.substring(folderName.lastIndexOf("/") + 1);
					List<ResumeScore> oldScores = new ResumeScore()
							.getObjects("RESUME_FILE = " + UserDBAccess.toSQL(fileName) + " and JOB_ID ="
									+ rs.getJobId() + " and user_name =" + UserDBAccess.toSQL(user.getLogin()));
					if (oldScores.size() > 0) {
						rs = oldScores.get(0);
					}
					if (emails.get(i) != null)
						rs.setResumeEmail(emails.get(i));
					rs.setResumeFile(fileName);
					rs.setResumeFolder(folderName);
					if (scores.get(i) != null)
						rs.setScore(scores.get(i));
					userScores.set(i, rs.getUserScore());
					String tagMatch = "";
					try {
						tagMatch = getMatchedTags(i, session);
					} catch (Exception x) {
						x.printStackTrace();
					}
					rs.setTagsFound(tagMatch);
					rs.setUserName(user.getLogin());
					rs.setTotal(resumeFiles.size());
					rs.setModificationTime(now);
					rs.setSimpleRating(user.isSimpleRating());
					if (validSubscription)
						rs.setPaid(true);
					if (!posting.getUserName().equals(user.getLogin()))
						rs.setByInvitation(true);
					else
						rs.setByInvitation(false);
					for (int j = 0; j < ranks.size(); j++)
						if (ranks.get(j) == i)
							rs.setRank(j + 1);
					if (rs.getId() > 0) {
						if (useTika)
							rs.update();
					} else {
						rs.setCreationTime(now);
						rs.insert();
					}
				}
			} catch (Exception x) {
				UserDataProviderPool.getInstance().releaseProvider();
				x.printStackTrace();
			}
			JobPostingSession jps = new JobPostingSession(user.getLogin(), posting.getId());
			jps.save(session);
		} finally {
			session.removeAttribute(SESSION_ATTRIBUTE.RANKING_INPROGRESS.toString());
		}

	}

	public static String getMatchedTags(int ind, HttpSession session) {
		String tagMatch = "";
		List<Map<Phrase, Integer>> matchedPhraseList = (List<Map<Phrase, Integer>>) session
				.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
		Set<String> tags = (Set<String>) session.getAttribute(SESSION_ATTRIBUTE.TAGS.toString());
		Set<Phrase> matchedPhrases = matchedPhraseList.get(ind).keySet();
		for (Phrase phrase : matchedPhrases) {
			String pstr = phrase.getBuffer().trim();
			boolean tagged = false;
			for (String tag : tags)
				if (tag.trim().equalsIgnoreCase(pstr))
					tagged = true;
			if (tagged && tagMatch.indexOf("," + pstr) == -1) {
				tagMatch = tagMatch + "," + pstr;
			}
		}
		if (tagMatch.length() > 1)
			tagMatch = tagMatch.substring(1);
		return tagMatch;
	}

	public class AnalyzeThread extends Thread {
		User user;
		HttpSession session;
		JobPosting posting;
		List<File> resumeFiles;
		boolean useTika;
		boolean validSubscription;

		public AnalyzeThread(User user, HttpSession session, JobPosting posting, List<File> resumeFiles,
				boolean useTika, boolean validSubscription) {
			this.user = user;
			this.session = session;
			this.posting = posting;
			this.resumeFiles = resumeFiles;
			this.useTika = useTika;
			this.validSubscription = validSubscription;
		}

		public void run() {
			try {
				analyzeResumes(user, session, posting, resumeFiles, useTika, validSubscription);
			} catch (Exception ex) {
				ex.printStackTrace();
				java.util.logging.Logger.getLogger(DocumentAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	/**
	 *
	 * @param name
	 *            the name of the file
	 * @return an enumeration type that represents the type of the file
	 */
	private OOFILTER_TYPE getExtension(String name) {
		String extension;
		StringBuilder builder = new StringBuilder();
		for (int i = name.length() - 1; i >= 0; i--) {
			if (name.charAt(i) == '.') {
				break;
			}
			builder.append(name.charAt(i));
		}

		extension = builder.reverse().toString().toLowerCase().trim();

		if (extension.equals("doc"))
			return OOFILTER_TYPE.MS_WORD_97;

		if (extension.equals("docx"))
			return OOFILTER_TYPE.MS_Word_2003_XML;

		if (extension.equals("odt"))
			return OOFILTER_TYPE.ODT;

		if (extension.equals("rtf"))
			return OOFILTER_TYPE.Rich_Text_Format;

		if (extension.equals("html"))
			return OOFILTER_TYPE.HTML;

		if (extension.equals("txt"))
			return OOFILTER_TYPE.Plain_Text;

		return null;

	}

	/**
	 * Sends the content of the data in the file to the given stream
	 *
	 * @param file
	 *            the file to output
	 * @param writer
	 *            the stream to use
	 * @throws IOException
	 */
	private void include(File file, PrintWriter writer) throws IOException {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			char buffer[] = new char[4096];
			int read;
			while ((read = reader.read(buffer)) != -1)
				writer.write(buffer, 0, read);

			reader.close();
		} catch (IOException x) {
			x.printStackTrace();
			throw x;
		}
	} // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click
		// on the + sign on the left to edit the code.">

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);

	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);

	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	/*
	 * Distance between two locations see
	 * http://www.androidsnippets.com/distance-between-two-gps-coordinates-in-
	 * meter
	 *
	 */
	public static double gps2m(double lat_a, double lng_a, double lat_b, double lng_b) {
		float pk = (float) (180 / 3.14169);

		double a1 = lat_a / pk;
		double a2 = lng_a / pk;
		double b1 = lat_b / pk;
		double b2 = lng_b / pk;

		double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
		double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
		double t3 = Math.sin(a1) * Math.sin(b1);
		double tt = Math.acos(t1 + t2 + t3);

		return 6366000 * tt;
	}

	/**
	 * Zips list of file into an outputfile DG
	 *
	 * @param src
	 * @param dst
	 * @throws IOException
	 */
	public static void zipFiles(List<File> inputFiles, File outputZipFile) {
		try {
			byte[] buffer = new byte[4096]; // Create a buffer for copying
			int bytesRead;

			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outputZipFile));

			for (int i = 0; i < inputFiles.size(); i++) {
				File f = inputFiles.get(i);
				if (f.isDirectory())
					continue;// Ignore directory
				FileInputStream in = new FileInputStream(f); // Stream to read
																// file
				ZipEntry entry = new ZipEntry(f.getName()); // Make a ZipEntry
				out.putNextEntry(entry); // Store entry
				while ((bytesRead = in.read(buffer)) != -1)
					out.write(buffer, 0, bytesRead);
				in.close();
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int getInt(String value) {
		try {
			return new Integer(value).intValue();
		} catch (Exception x) {
			return 0;
		}
	}

}
