package com.textonomics.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.textonomics.user.User;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * A filter that is called before any request and makes sure that the current
 * user has been validated. See the web.xml file to verify which servlets
 * request the execution of this filter.
 *
 * @author Nuno Seco
 *
 */
public class SessionValidator implements Filter {

	/**
	 * Default constructor.
	 */
	public SessionValidator() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String redirectURL = httpRequest.getRequestURI() + "?" + httpRequest.getQueryString();
		HttpSession session = ((HttpServletRequest) request).getSession(false);
		if (session == null) {
			request.getRequestDispatcher("/signin.jsp?redirectURL=" + java.net.URLEncoder.encode(redirectURL))
					.forward(request, response);
			return;
		}

		User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
		if (user == null) {
			request.getRequestDispatcher("/signin.jsp?redirectURL=" + java.net.URLEncoder.encode(redirectURL))
					.forward(request, response);
			return;
		}

		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
