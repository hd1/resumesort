/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textonomics.Phrase;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Preeti Mudda
 */
public class DatedPhrase {
	private HashMap<Phrase, DateObject> mapOfPhraseAndDateObjects;
	private ArrayList<DateObject> arryDateObjs;
	private LengthObject totalLengthOfTime;

	/**
	 * 
	 */
	public DatedPhrase() {
		totalLengthOfTime = new LengthObject(0, 0);
		mapOfPhraseAndDateObjects = new HashMap<Phrase, DateObject>();
		arryDateObjs = new ArrayList<DateObject>();
	}

	public void addDateObj(DateObject obj) {
		arryDateObjs.add(obj);
	}

	public ArrayList<DateObject> getDateObjects() {
		return arryDateObjs;
	}

	public void addPhraseAndDateObj(Phrase p, DateObject obj) {
		mapOfPhraseAndDateObjects.put(p, obj);
	}

	public HashMap<Phrase, DateObject> getMapOfPhrase() {
		return mapOfPhraseAndDateObjects;
	}

	public void setLengthOfTime(int year, int month) {
		totalLengthOfTime.addYears(year);
		totalLengthOfTime.addMonths(month);
	}

	public LengthObject getTotalTime() {
		return totalLengthOfTime;
	}
}