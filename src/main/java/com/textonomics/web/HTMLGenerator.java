package com.textonomics.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.textonomics.CAKE_WALK_TAG_TYPE;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * An abstract class used to generate dynamic HTML content to the client.
 * Classes extending this class should provide: the format method the
 * writeHeader method the writeFooter method
 *
 * @author Nuno Seco
 *
 */

public abstract class HTMLGenerator {
	/**
	 * Newline of the system
	 */
	protected final static String newline = System.getProperty("line.separator");

	/**
	 * Stream to read htm file
	 */
	protected final BufferedReader htmlReader;

	/**
	 * HTML statements to be inserted
	 */
	protected final HashMap<STATEMENT, List<String>> statments;

	/**
	 * constructor
	 * 
	 * @param htmlFile
	 * @throws FileNotFoundException
	 */
	public HTMLGenerator(File htmlFile) throws FileNotFoundException {
		this.htmlReader = new BufferedReader(new FileReader(htmlFile));
		this.statments = new HashMap<STATEMENT, List<String>>();
	}

	/**
	 * Clients make request for a certain statement to be added to the HTML when
	 * its being generated
	 * 
	 * @param type
	 *            the type statement
	 * @param value
	 *            the string to insert
	 */
	public void addStatement(STATEMENT type, String value) {
		List<String> codeList = statments.get(type);

		if (codeList == null) {
			codeList = new LinkedList<String>();
		}

		codeList.add(value);
		statments.put(type, codeList);
	}

	/**
	 * Main method used to generate a modified version of the given html file.
	 * The method accepts the output file as its argument.
	 * 
	 * @param resultFile
	 *            the new file
	 * @throws IOException
	 */
	public void generateHTML(File resultFile) throws IOException {
		String html = format(loadData());
		// To show images
		// html = html.replace("IMG SRC=\"HIGHLIGHTED", "IMG
		// SRC=\"temp/HIGHLIGHTED");
		FileWriter writer = new FileWriter(resultFile);
		writeHeader(writer);
		writer.write(html);
		writeFooter(writer);
		writer.close();
	}

	/**
	 * Returns the number of characters in the HTML string that corresponds to
	 * the opening body tag.
	 * 
	 * @param i
	 *            the index where the tag '<body>' starts
	 * @param html
	 *            the string containing the tag
	 * @return the size of the opening tag including attributes
	 */
	protected int getBodyTagLength(int i, String html) {
		int length = 1;
		for (; i < html.length() && html.charAt(i) != '>'; i++, length++)
			;
		return length;
	}

	/**
	 * Verifies if an end body tag starts at the current index in the given
	 * string
	 * 
	 * @param i
	 *            the starting index
	 * @param buffer
	 *            the buffer
	 * @return true if the index marks the start of the end body tag, false
	 *         otherwise
	 */
	protected boolean isHtmlBodyEnd(int i, String buffer) {
		if (buffer.charAt(i) != '<')
			return false;

		String comp = buffer.substring(i, i + 6);

		return comp.toLowerCase().equals("</body");
	}

	/**
	 * Verifies if a start body tag starts at the current index in the given
	 * string
	 * 
	 * @param i
	 *            the starting index
	 * @param buffer
	 * @return true if the index marks the start of the start body tag, false
	 *         otherwise
	 */
	protected boolean isHtmlBodyStart(int i, String buffer) {
		if (buffer.charAt(i) != '<')
			return false;

		String comp = buffer.substring(i, i + 5);

		return comp.toLowerCase().equals("<body");
	}

	/**
	 * Reads the file into a buffer
	 * 
	 * @return the string containing the HTML
	 * @throws IOException
	 */
	private String loadData() throws IOException {
		StringBuilder builder = new StringBuilder();
		char[] buffer = new char[2048];
		int read;

		while ((read = htmlReader.read(buffer)) != -1) {
			builder.append(buffer, 0, read);
		}

		htmlReader.close();
		return builder.toString();
	}

	/**
	 * Verifies if the given tag starts at the given position in the given
	 * string
	 * 
	 * @param i
	 *            the start position in the string
	 * @param buffer
	 *            the buffer to inspect
	 * @param tag
	 *            the tag to look for
	 * @return true if the tag is found at the given pos, false otherwise
	 */
	protected boolean isTag(int i, String buffer, CAKE_WALK_TAG_TYPE tag) {
		if (buffer.charAt(i) != CAKE_WALK_TAG_TYPE.TAG_STARTER.getTag().charAt(0))
			return false;

		String compare = buffer.substring(i, i + tag.getTag().length());
		return compare.equals(tag.getTag());

	}

	/**
	 * Method used to change to the HTML by subclasses
	 * 
	 * @param html
	 *            the html to change
	 * @return the new html
	 */
	protected abstract String format(String html);

	/**
	 * Method used by subclasses to print any info regarding the start of the
	 * html
	 * 
	 * @param writer
	 * @throws IOException
	 */
	protected abstract void writeFooter(FileWriter writer) throws IOException;

	/**
	 * 
	 * Method used by subclasses to print any info regarding the end of the html
	 * 
	 * @param writer
	 * @throws IOException
	 */
	protected abstract void writeHeader(FileWriter writer) throws IOException;

	public void setHeaderHTML(String header) {

	}

	public void setFooterHTML(String header) {

	}

}
