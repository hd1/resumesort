package com.textonomics.web;

import com.textnomics.data.UserTemplatePhrase;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.textonomics.DocumentRS;
import com.textonomics.Phrase;
import com.textonomics.PhraseList;
import com.textonomics.PlatformProperties;
import com.textonomics.openoffice.OODocumentConverter;
import com.textonomics.openoffice.OODocumentRSTagger;
import com.textonomics.openoffice.OOFILTER_TYPE;
import com.textonomics.user.User;
import com.textonomics.user.UserDataProviderPool;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.log4j.Logger;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This servlet receives the resume file from the user,the relevant part of the
 * text from the job ad and all the configuration parameters that are to be used
 * in the suggestion process.
 * 
 * The servlet extracts all the information from the request, configures the
 * suggester accordingly and executes the suggestion algorithm which produces
 * the suggestions. Finally, using the obtained suggestions an HTML file of the
 * resume is created and all the HTML/JavaScript markup is for the dropdowns is
 * added. Finally the data in the new HTML file is sent back to the user where
 * he can then use the dropdowns to choose the suggestions that are most
 * relevant to him.
 * 
 *
 * @author Nuno Seco and Rodrigo Neves
 *
 */

public class ResumeViewer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(ResumeViewer.class);
	/**
	 * Tmp directory to store tmporary files
	 */
	private String tempDir;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ResumeViewer() {
		super();
	}

	@Override
	public void init() throws ServletException {
		super.init();
		tempDir = System.getProperty("java.io.tmpdir");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	// EBB this is after the uploader.jsp and the submit button being pushed.
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", -1);
		response.setContentType("text/html; charset=utf-8");

		int htmlIndex = getInt(request.getParameter("viewFileIndex"));
		User user = null;
		File resume = null;
		List<Map<Phrase, Integer>> matchedPhraseList = null;
		try {
			WordNetDataProviderPool.getInstance().acquireProvider();
			HttpSession session = request.getSession();
			List<Boolean> blockedResumes = (List<Boolean>) session
					.getAttribute(SESSION_ATTRIBUTE.BLOCKED_RESUMES.toString());
			if (blockedResumes != null && blockedResumes.get(htmlIndex)) {
				OutputStream out = response.getOutputStream();
				out.write("<br><br><b><font color=red size=+1><b>Resume has been blocked</b></font>".getBytes());
				return;
			}
			user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			List<File> resumeHTMLs = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.TAGGED_HTMLRESUMES.toString());
			List<File> resumeDocs = (List<File>) session
					.getAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString());
			List<File> resumeFiles = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
			List<Double> scores = (List<Double>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
			String uploadDir = (String) session.getAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString()); // the
																										// upload
																										// dir
																										// (for
																										// resumes)
																										// of
																										// the
																										// user
			StringWriter buffer = new StringWriter();
			matchedPhraseList = (List<Map<Phrase, Integer>>) session
					.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
			List<List<String>> unmatchedPhraseList = (List<List<String>>) session
					.getAttribute(SESSION_ATTRIBUTE.UNMATCHED_PHRASES.toString());
			Collection<PhraseList> nonLemmaPhrases = (Collection<PhraseList>) session
					.getAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString());
			Collection<PhraseList> lemmaPhrases = (Collection<PhraseList>) session
					.getAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString());
			List<String> userSelectedPhrases = (List<String>) session
					.getAttribute(SESSION_ATTRIBUTE.SELECTED_PHRASES.toString());
			List<String> userEnteredPhrases = (List<String>) session
					.getAttribute(SESSION_ATTRIBUTE.ENTERED_PHRASES.toString());
			Set<String> tags = (Set<String>) session.getAttribute(SESSION_ATTRIBUTE.TAGS.toString());
			Map<String, Integer> phraseImportance = (Map<String, Integer>) session
					.getAttribute(SESSION_ATTRIBUTE.PHRASE_IMPORTANCE.toString());
			List<Boolean> required = (List<Boolean>) session.getAttribute(SESSION_ATTRIBUTE.PHRASE_REQUIRED.toString());
			List<Boolean> processedByTika = (List<Boolean>) session
					.getAttribute(SESSION_ATTRIBUTE.PROCESSED_BY_TIKA.toString());
			List<String> candidateNames = (List<String>) session
					.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_NAMES.toString());
			// highLightedDocFile = resumeDocs.get(htmlIndex);
			resume = resumeFiles.get(htmlIndex);
			File highLightedDocFile = PlatformProperties.getInstance()
					.getResourceFile(uploadDir + File.separator + "HIGHLIGHTED_" + resume.getName());

			File highlightedHtml = new File(request.getRealPath("temp") + "/" + highLightedDocFile.getName() + ".html");
			Boolean useStars = (Boolean) session.getAttribute(SESSION_ATTRIBUTE.USE_STARS.toString());
			if (useStars == null)
				useStars = true;
			// If no matching/analysis has been done, the phrase list may be
			// null
			if (matchedPhraseList != null && matchedPhraseList.get(htmlIndex) != null) {
				System.out.println("Processed by tika:" + processedByTika.get(htmlIndex));
				if (processedByTika.get(htmlIndex)) // If processed by tika, we
													// need to redo using open
													// office
				{
					ResumeProcessingThread rthread = new ResumeProcessingThread(resumeFiles.get(htmlIndex), session,
							htmlIndex, true);
					rthread.run();
					WordNetDataProviderPool.getInstance().acquireProvider(); // It
																				// may
																				// have
																				// been
																				// released
																				// by
																				// the
																				// thing
																				// above
				}
				System.out.println("Phrases to Tagger:" + matchedPhraseList.get(htmlIndex).keySet().size() + ":"
						+ matchedPhraseList.get(htmlIndex).keySet());
				if (resume.getName().toLowerCase().endsWith(".pdf")) {
					resume = new File(resume.getParent(), ResumeSorter.CONVERTED_PREFIX
							+ resume.getName().substring(0, resume.getName().length() - 4).replace(' ', '_') + ".html");
				}
				System.out.println("Tagging:" + resume.getAbsolutePath());
				OODocumentRSTagger tagger = new OODocumentRSTagger(resume, matchedPhraseList.get(htmlIndex).keySet());
				tagger.process(highlightedHtml, OOFILTER_TYPE.HTML);
			} else {
				if (resume.getName().toLowerCase().endsWith(".pdf")) {
					highlightedHtml = new File(resume.getParent(), ResumeSorter.CONVERTED_PREFIX
							+ resume.getName().substring(0, resume.getName().length() - 4) + ".html");
				} else
					highlightedHtml = new OODocumentConverter().convertToHtml(resume, highlightedHtml);
				System.out.println("Output file:" + highlightedHtml.getAbsolutePath());
			}
			DocumentRS sourceDocument = (DocumentRS) session.getAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString());
			Map<String, Integer> resumePhrasesMap = (Map<String, Integer>) session
					.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
			List<UserTemplatePhrase> templatePhrases = (List<UserTemplatePhrase>) session
					.getAttribute(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString());
			if (templatePhrases == null)
				templatePhrases = new ArrayList<UserTemplatePhrase>();
			Set<String> templatePhraseSet = new HashSet<String>();
			for (UserTemplatePhrase templatePhrase : templatePhrases)
				templatePhraseSet.add(templatePhrase.getPhrase());
			ResultHTMLGenerator generator = new ResultHTMLGenerator(highlightedHtml, null, sourceDocument);
			generator.setLoggedInUser(user);
			generator.setResumePhrasesMap(resumePhrasesMap);
			generator.setTemplatePhrases(templatePhraseSet);
			generator.setUseStars(useStars);
			generator.setResumeFile(resume);
			include(new File(getServletContext().getRealPath("rt_tuner_header.html")), new PrintWriter(buffer));
			generator.setHeaderHTML(buffer.getBuffer().toString());
			buffer = new StringWriter();
			include(new File(getServletContext().getRealPath("rt_tuner_footer.html")), new PrintWriter(buffer));
			generator.setFooterHTML(buffer.getBuffer().toString());
			generator.setResumeFileIndex(htmlIndex);
			if (matchedPhraseList != null) {
				generator.setMatchedPhrases(matchedPhraseList.get(htmlIndex));
				generator.setUnmatchedPhrases(unmatchedPhraseList.get(htmlIndex));
				generator.setUnlemmaPhrases(nonLemmaPhrases);
				generator.setLemmaPhrases(lemmaPhrases);
				generator.setUserSelectedPhrases(userSelectedPhrases);
				generator.setUserEnteredPhrases(userEnteredPhrases);
				generator.setPhraseImportance(phraseImportance);
				generator.setRequired(required);
				generator.setTags(tags);
				if (candidateNames != null)
					generator.setCandidateName(candidateNames.get(htmlIndex));
				if (generator.getCandidateName() == null)
					generator.setCandidateName("");
				if (scores != null && scores.size() > htmlIndex)
					generator.setScore(scores.get(htmlIndex));
			}
			// generator.setDebugFileName(zipFileName);

			File html = PlatformProperties.getInstance()
					.getResourceFile(uploadDir + File.separator + highlightedHtml.getName()); // EBB
																								// just
																								// the
																								// file
																								// name
			generator.generateHTML(html); // EBB this is where the { } tags are
											// converted to <span...> in the
											// outputed HTML.
			log(html + " generated");

			// EBB this is where the HTML is displayed to the user -- output.
			include(html, response.getWriter()); // EBB HttpServletResponse
													// response.get... is what
													// is being outputed to the
													// user.
			response.getWriter().close();
			long endTime = System.currentTimeMillis();
			html.delete();
			// logger.info("Time to process resume:" + targetFile.getName() +
			// ":" + new Double(endTime-startTime).doubleValue()/1000 + "
			// secs");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Resume file:" + resume.getAbsolutePath());
			if (matchedPhraseList != null && matchedPhraseList.size() > htmlIndex
					&& matchedPhraseList.get(htmlIndex) != null)
				response.sendRedirect("ranked_resumes.jsp?error=" + e);
			else
				response.sendRedirect("home.jsp?error=" + e);
		} finally {
			// Release all providers!!!!!!!!!!!
			WordNetDataProviderPool.getInstance().releaseProvider();
			UserDataProviderPool.getInstance().releaseProvider();
		}
	}

	/**
	 * Sends the content of the data in the file to the given stream
	 * 
	 * @param file
	 *            the file to output
	 * @param writer
	 *            the stream to use
	 * @throws IOException
	 */
	private void include(File file, PrintWriter writer) throws IOException {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			char buffer[] = new char[4096];
			int read;
			while ((read = reader.read(buffer)) != -1)
				writer.write(buffer, 0, read);

			reader.close();
		} catch (IOException x) {
			x.printStackTrace();
			throw x;
		}
	}

	/**
	 * Sends the data to the given stream
	 *
	 * @param file
	 *            the file to output
	 * @param writer
	 *            the stream to use
	 * @throws IOException
	 */
	private void writeString(File file, String data) throws IOException {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(data);
			writer.close();
		} catch (IOException x) {
			x.printStackTrace();
			throw x;
		}
	}

	/**
	 * Copies the the src file to dst file
	 * 
	 * @param src
	 * @param dst
	 * @throws IOException
	 */
	private void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	/**
	 * Zips list of file into an outputfile DG
	 *
	 * @param src
	 * @param dst
	 * @throws IOException
	 */
	public static void zipFiles(List<File> inputFiles, String outputZipFile) {
		try {
			byte[] buffer = new byte[4096]; // Create a buffer for copying
			int bytesRead;

			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outputZipFile));

			for (int i = 0; i < inputFiles.size(); i++) {
				File f = inputFiles.get(i);
				if (f.isDirectory())
					continue;// Ignore directory
				FileInputStream in = new FileInputStream(f); // Stream to read
																// file
				ZipEntry entry = new ZipEntry(f.getName()); // Make a ZipEntry
				out.putNextEntry(entry); // Store entry
				while ((bytesRead = in.read(buffer)) != -1)
					out.write(buffer, 0, bytesRead);
				in.close();
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int getInt(String value) {
		try {
			return new Integer(value).intValue();
		} catch (Exception x) {
			return 0;
		}
	}
}
