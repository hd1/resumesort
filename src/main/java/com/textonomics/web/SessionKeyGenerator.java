/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

/**
 * Generates keys for resume object and allResumePhrases that will be used while
 * putting those objects in Memcached
 * 
 * @author Preeti Mudda
 */
public class SessionKeyGenerator {
	public SessionKeyGenerator() {
	}

	/**
	 * Generates the key using the http session id, job id and resumeName
	 * 
	 * @param id
	 *            : User's Http Session ID
	 * @param resumeName
	 *            : resume file name
	 * @param jobId
	 *            : jobId for each job put in Beanstalkd
	 * @return key : returns the generated key
	 */
	public static String getKey(String id, String resumeName, long jobId) {
		resumeName = resumeName.replace('-', 'A'); // replace non-charcter with
													// A
		resumeName = resumeName.replace(' ', 'A'); // replace space with A
		String key = id + jobId + resumeName;
		return key;
	}

	/**
	 * Generate the key using http session id, jobId and Phrases
	 * 
	 * @param id:
	 *            HttpSession ID
	 * @param jobId
	 *            : jobId for each job put in Beanstalkd
	 * @return sKey : returns the key generated.
	 */
	public static String getKeyForPhrases(String id, long jobId) {
		String sKey = id + "Phrases" + jobId;
		return sKey;
	}
}