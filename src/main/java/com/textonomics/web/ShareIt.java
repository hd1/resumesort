package com.textonomics.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 * 
 * This servlet is used as a call back servlet. When the user receives an email
 * with his resume he can choose to share his experience on some the social web
 * sites.
 * 
 * The email contains the links at the bottom that forward the user to this
 * servlet. When a request is received google analytics is updated on the page
 * automatically refreshes sending the user to the respective site.
 * 
 * 
 * @author Rodrigo Neves
 * 
 */

public class ShareIt extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String SHARE_TWITTER = "Using www.ResumeTuner.com to improve my resume and getting key phrases from a job ad.";
	private static final String SHARE_TITLE = "ResumeTuner.com";
	private static final String SHARE_NOTES = "I am using ResumeTuner.com to improve my resume "
			+ "by synchronizing it to specific job Ad.  The program suggests keywords and phrases "
			+ "from a Job Ad to replace the alternative phrases that I have in my resume.  This helps "
			+ "the HR managers, search engines, and Application Tracking Systems better identify my "
			+ "relevant experience when they look for key phrases and qualification. It's pretty impressive."
			+ "  You can sign up for their Alpta -- between alpha and beta -- by going to their website "
			+ "www.ResumeTuner.com. Cheers,";
	private static final String SHARE_TAGS = "jobs resume keywords replacement";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShareIt() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String type = (String) request.getParameter("type");

		PrintWriter out = response.getWriter();

		out.write("<html>");
		out.write("<head>");
		out.write("<meta HTTP-EQUIV=\"REFRESH\" content=\"0; url=");
		if (type.equals("addtoany")) {
			out.write("http://www.addtoany.com/share_save?linkname=ResumeTuner&linkurl=http://www.resumetuner.com");
		} else if (type.equals("digg")) {
			out.write("http://digg.com/submit?phase=2&url=http://www.resumetuner.com");
		} else if (type.equals("twitter")) {
			out.write("http://twitter.com/home?status=" + SHARE_TWITTER);
		} else if (type.equals("stumbleupon")) {
			out.write("http://www.stumbleupon.com/submit?url=http://www.resumetuner.com");
		} else if (type.equals("delicious")) {
			out.write("http://del.icio.us/post?url=http://www.resumetuner.com&title=" + SHARE_TITLE + "&notes="
					+ SHARE_NOTES + "&tags=" + SHARE_TAGS);
		} else if (type.equals("facebook")) {
			out.write("http://www.facebook.com/share.php?u=http://www.resumetuner.com");
		} else if (type.equals("myspace")) {
			out.write("http://www.myspace.com/Modules/PostTo/Pages/?l=3&u=http://www.resumetuner.com&t=" + SHARE_TITLE);
		} else if (type.equals("googlebookmarks")) {
			out.write("http://www.google.com/bookmarks/mark?op=edit&bkmk=http://www.resumetuner.com&annotation="
					+ SHARE_NOTES + "&title=" + SHARE_TITLE + "&labels=" + SHARE_TAGS);
		} else if (type.equals("reddit")) {
			out.write("http://reddit.com/submit?url=http://www.resumetuner.com&title=" + SHARE_TITLE);
		}
		out.write("\">");

		// out.write("<meta http-equiv=\"Content-Type\" content=\"text/html;
		// charset=ISO-8859-1\">");
		out.write("<title>Share it!</title>");
		out.write("</head>");
		out.write("<body>");
		request.getRequestDispatcher("/AnalyticsTracking.jsp").include(request, response);

		out.write("</body>");
		out.write("</html>");

		/*
		 * if(type.equals("addtoany")){ response.sendRedirect(
		 * "http://www.addtoany.com/share_save?linkname=ResumeTuner&linkurl=http://www.resumetuner.com"
		 * ); } else if(type.equals("digg")){ response.sendRedirect(
		 * "http://digg.com/submit?phase=2&url=http://www.resumetuner.com"); }
		 * else if(type.equals("twitter")){ response.sendRedirect(
		 * "http://twitter.com/home?status=http://www.resumetuner.com"); } else
		 * if(type.equals("stumbleupon")){ response.sendRedirect(
		 * "http://www.stumbleupon.com/submit?url=http://www.resumetuner.com");
		 * } else if(type.equals("delicious")){ response.sendRedirect(
		 * "http://del.icio.us/post?url=http://www.resumetuner.com"); } else
		 * if(type.equals("facebook")){ response.sendRedirect(
		 * "http://www.facebook.com/share.php?u=http://www.resumetuner.com"); }
		 * else if(type.equals("myspace")){ response.sendRedirect(
		 * "http://www.myspace.com/Modules/PostTo/Pages/?l=3&u=http://www.resumetuner.com"
		 * ); } else if(type.equals("googlebookmarks")){ response.sendRedirect(
		 * "http://www.google.com/bookmarks/mark?op=edit&bkmk=http://www.resumetuner.com"
		 * ); } else if(type.equals("reddit")){ response.sendRedirect(
		 * "http://reddit.com/submit?url=http://www.resumetuner.com"); }
		 */
		/*
		 * sb.
		 * append("<a target=\"_blank\" href=\"http://www.addtoany.com/share_save?linkname=ResumeTuner&linkurl=http://www.resumetuner.com\"/><img src=\"http://www.addtoany.com/favicon.ico\"/></a>"
		 * );sb.append(
		 * "<a target=\"_blank\" href=\"http://digg.com/submit?phase=2&url=http://www.resumetuner.com\"/><img src=\"http://digg.com/favicon.ico\" alt=\"http://digg.com/favicon.ico\"/></a>"
		 * );sb.append(
		 * "<a target=\"_blank\" href=\"http://twitter.com/home?status=http://www.resumetuner.com\"><img src=\"http://assets1.twitter.com/images/favicon.ico\" alt=\"http://assets1.twitter.com/images/favicon.ico\"/></a>"
		 * );sb.append(
		 * "<a target=\"_blank\" href=\"http://www.stumbleupon.com/submit?url=http://www.resumetuner.com\"/><img src=\"http://www.stumbleupon.com/favicon.ico\" alt=\"http://www.stumbleupon.com/favicon.ico\"/></a>"
		 * );sb.append(
		 * "<a target=\"_blank\" href=\"http://del.icio.us/post?url=http://www.resumetuner.com\"/><img src=\"http://delicious.com/favicon.ico\" alt=\"http://delicious.com/favicon.ico\"/></a>"
		 * );sb.append(
		 * "<a target=\"_blank\" href=\"http://www.facebook.com/share.php?u=http://www.resumetuner.com\"/><img src=\"http://www.facebook.com/favicon.ico\" alt=\"http://www.facebook.com/favicon.ico\"/></a>"
		 * );sb.append(
		 * "<a target=\"_blank\" href=\"http://www.myspace.com/Modules/PostTo/Pages/?l=3&u=http://www.resumetuner.com\"/><img src=\"http://www.myspace.com/favicon.ico\" alt=\"http://www.myspace.com/favicon.ico\"/></a>"
		 * );sb.append(
		 * "<a target=\"_blank\" href=\"http://www.google.com/bookmarks/mark?op=edit&bkmk=http://www.resumetuner.com\"/><img src=\"http://www.google.com/favicon.ico\" alt=\"http://www.google.com/favicon.ico\"/></a>"
		 * );sb.append(
		 * "<a target=\"_blank\" href=\"http://reddit.com/submit?url=http://www.resumetuner.com\"/><img src=\"http://www.reddit.com/favicon.ico\"/></a>"
		 * );
		 */
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
