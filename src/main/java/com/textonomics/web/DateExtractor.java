/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.textonomics.web;

/**
 *
 * @author Preeti Mudda
 */
import com.textonomics.Phrase;
import com.textonomics.SynonymPhrase;
import com.textonomics.nlp.Sentence;
import com.textonomics.tika.DocumentParserException;
import com.textonomics.tika.TikaSentenceDetector;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.tika.detect.Detector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateExtractor {

	private String spacepattern = "\\p{Space}*";
	private String orpattern = "|";
	private ArrayList<DateObject> sortedDates = new ArrayList<DateObject>();
	// date pattern
	// 1: ex. 2000-2003 or 2000 - 2003 or 2010-Present or 2000 - Current
	// 2. ex: Jan 2000 - Feb 2002 or Jan, 2000 - Feb, 2002 or January 2000 -
	// February 2002
	// 2. ex: Jan 1, 2000 - Feb 1, 2002 or Jan 1, 2000 - Feb 1, 2002 or January
	// 1, 2000 - February 2, 2002
	// 3. ex: 02/09-12/11 or 02/09 - 12/11 02/09 to 12/11 or 02/2009-12/2011 or
	// 02/2009 to 12/2011
	private String pattern1 = "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sept|Sep|Oct|Nov|Dec|"
			+ "January|February|March|April|May|June|July|August|September|October|November|December)" + spacepattern;
	// private String pattern1 =
	// "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sept|Oct|Nov|Dec\\p{Alpha}*)"+spacepattern;
	private String pattern2 = "(Current|Present|Now|Currently|Presently|Till Date|Todate)";
	private String pattern3 = "(to|-|--|~)??";
	private String pattern4 = "((19|20)\\d{2})";// "\\d{4}";
	private String pattern5 = "\\d{1,2}";
	private String pattern5a = "\\d{2}";
	private String pattern6 = "(0?[0-9]|[1-2][0-9]|3[0-1])"; // day 1- 31
	private String monthDigitPattern = "(0?[0-9] | 1[0-2])"; // one digit or two
																// digit month
																// pattern
	private String pattern7 = pattern4 + "\\." + pattern5;
	private String pattern8 = pattern1 + "(((" + pattern5 + "," + spacepattern + ")|(,|-)" + spacepattern + ")??)" + "("
			+ pattern4 + orpattern + pattern5a + ")";
	// private String pattern8 = pattern1 + "(.*) (" + pattern4 + orpattern +
	// pattern5a + ")";
	private String pattern8a = pattern1 + "(.*) (" + pattern4 + orpattern + pattern5a + ")";
	private ArrayList<MatchedDateObject> arryLstOfDates = new ArrayList<MatchedDateObject>();
	private double totalTime = 0.0;
	private String[] patternsNew = {
			// 1.0 Jan 12, 2000 or Jan, 2000 or Jan 2000 or Jan 12, 00 or Jan,
			// 00 or Jan 00
			// 1.1 January 12, 2000 or January, 2000 or January 2000 or January
			// 12, 00 or January, 00 or January 00
			pattern8 + "(.*)(" + pattern8 + orpattern + pattern2 + ")",
			// pattern8+spacepattern+pattern3+spacepattern+pattern2,
			// pattern8+spacepattern+pattern3+spacepattern+"("+pattern8+"|"+pattern2+")",
			// 2.0 2000 to 2000 or 2000 Current
			pattern4 + "(.*) (" + pattern4 + "|" + pattern2 + ")",
			// 3.0 02/2005 or 02/05
			"\\b(" + pattern5 + "/(" + pattern4 + "|" + pattern5a + "))" + "(.*) ((" + pattern5 + "/(" + pattern4 + "|"
					+ pattern5a + "))|" + pattern2 + ")",
			// 4.0 2004.03
			pattern7 + "(.*) (" + pattern7 + "|" + pattern2 + ")",
			pattern1 + spacepattern + pattern4 + "(.*)Till Date$" };
	// private String[] patternsNew = {
	// //1.0 Jan 12, 2000 or Jan, 2000 or Jan 2000 or Jan 12, 00 or Jan, 00 or
	// Jan 00
	// //1.1 January 12, 2000 or January, 2000 or January 2000 or January 12, 00
	// or January, 00 or January 00
	// pattern8 + spacepattern + pattern3 + spacepattern + "(" + pattern8 +
	// orpattern + pattern2 + ")",
	// // pattern8+spacepattern+pattern3+spacepattern+pattern2,
	// //
	// pattern8+spacepattern+pattern3+spacepattern+"("+pattern8+"|"+pattern2+")",
	// //2.0 2000 to 2000 or 2000 Current
	// pattern4 + spacepattern + pattern3 + spacepattern + "(" + pattern4 + "|"
	// + pattern2 + ")",
	// //3.0 02/2005 or 02/05
	// "\\b(" + pattern5 + "/(" + pattern4 + "|" + pattern5a + "))" +
	// spacepattern + pattern3 + spacepattern + "((" + pattern5 + "/(" +
	// pattern4 + "|" + pattern5a + "))|" + pattern2 + ")",
	// //4.0 2004.03
	// pattern7 + spacepattern + pattern3 + spacepattern + "(" + pattern7 + "|"
	// + pattern2 + ")",
	// pattern1+spacepattern+pattern4+"(.*)Till Date$" };
	private static String[] patterns = {
			// 2004 - Present or 2004 to Present
			"\\d{4}\\p{Space}*(-|to)??\\p{Space}*(Current|Present|Now|Currently|Presently|Till Date)",
			// 2004 to 2005 or 2004 - 2005
			"\\d{4}\\p{Space}*(to|-)??\\p{Space}*\\d{4}",
			// Jan 12, 2004 to Mar 14, 2005 or Jan, 2004 to Mar, 2005 or Jan 12,
			// 2004 - Mar 14, 2005 or Jan, 2004 - Mar, 2005
			"(Jan|Feb|Mar|Apr|April|May|Jun|June|Jul|July|Aug|Sept|Oct|Nov|Dec)\\p{Space}*(\\d{1,2},\\p{Space}*|,\\p{Space}*)??\\d{4}\\p{Space}*(to\\p{Space}*|-\\p{Space}*)??(Jan|Feb|Mar|Apr|April|May|Jun|June|Jul|July|Aug|Sept|Oct|Nov|Dec)\\p{Space}*(\\d{1,2},\\p{Space}*| ,\\p{Space}*)??\\d{4}",
			// January 12, 2004 to March 12, 2005 or January, 2005 to March,
			// 2005 or January 12, 2004 - March 12, 2005 or January, 2005 -
			// March, 2005
			"(January|February|March|April|May|June|July|August|September|October|November|December)\\p{Space}*(\\d{1,2},\\p{Space}*|,\\p{Space}*)??\\d{4},??\\p{Space}*(to\\p{Space}*|-\\p{Space}*)??(January|February|March|April|May|June|July|August|September|October|November|December)\\p{Space}*(\\d{1,2},|,)??\\p{Space}*\\d{4}",
			// January 12, 2005 to Present or January, 2005 to Present or
			// January 12, 2005 - Present or January, 2005 - Present
			"(January|February|March|April|May|June|July|August|September|October|November|December)\\p{Space}*(\\d{1,2},\\p{Space}*|,\\p{Space}*)??\\d{4},??\\p{Space}*(to\\p{Space}*|-\\p{Space}*)??(Present|Current|Now|Currently|Presently|Till Date)",
			// Jan 12, 2005 to Present or Jan, 2002 to Present or Jan 12, 2005 -
			// Present or Jan, 2002 - Present
			"(Jan|Feb|Mar|Apr|April|May|Jun|June|Jul|July|Aug|Sept|Oct|Nov|Dec)\\p{Space}*(\\d{1,2},|,)??\\p{Space}*\\d{4}\\p{Space}*(to|-|,)??\\p{Space}*(Present|Current|Now|Currently|Presently|Till\\p{Space}*Date)",
			// Nov 07 to Dec 08 or Nov 07 - Dec 08
			"(Jan|Feb|Mar|Apr|April|May|Jun|June|Jul|July|Aug|Sept|Oct|Nov|Dec)\\p{Space}*\\d{2}\\p{Space}*(to\\p{Space}*|-\\p{Space}*)??(Jan|Feb|Mar|Apr|April|May|Jun|June|Jul|July|Aug|Sept|Oct|Nov|Dec)\\p{Space}*\\d{4}",
			// January 07 to March 08 or January 07 - March 08
			"(January|February|March|April|May|June|July|August|September|October|November|December)\\p{Space}*\\d{2},??\\p{Space}*(to\\p{Space}*|-\\p{Space}*)??(January|February|March|April|May|June|July|August|September|October|November|December)\\p{Space}*\\d{2}",
			// January 07 to Present or January 07 - Present
			"(January|February|March|April|May|June|July|August|September|October|November|December)\\p{Space}*\\d{2},??\\p{Space}*(to\\p{Space}*|-\\p{Space}*)??(Present|Current|Now|Currently|Presently|Till Date)",
			// Jan 07 to Present or Jan 09 - Present
			"(Jan|Feb|Mar|Apr|April|May|Jun|June|Jul|July|Aug|Sept|Oct|Nov|Dec)\\p{Space}*(\\d{4}|\\d{2})\\p{Space}*(to|-|,)??\\p{Space}*(Present|Current|Now|Currently|Presently|Till\\p{Space}*Date)",
			// 04/2005 to 05/2006 or 04/2005 - 05/2006
			"\\d{1,2}/\\d{4}\\p{Space}*(to\\p{Space}*|-\\p{Space}*)??\\d{1,2}/\\d{4}",
			// "\\d{1,2}/\\d{4}\\p{Space}*\\p{Space}*\\d{1,2}/\\d{4}",
			// 04/05 to 05/06 or 04/05 - 05/06
			"\\d{1,2}/\\d{2}\\p{Space}*(to|-)??\\p{Space}*\\d{1,2}/\\d{2}",
			// 04/05 to Present or 04/05 - Present
			"\\d{1,2}/\\d{2}\\p{Space}*(to|-)??\\p{Space}*(Present|Current|Now|Currently|Presently|Till Date)",
			// 04/2005 to Present or 04/05 - Present
			"\\d{1,2}/\\d{4}\\p{Space}*(to|-)??\\p{Space}*(Present|Current|Now|Currently|Presently|Till Date)",
			// 2005.03 to 2006.04 or 2005.03 - 2006.04 or 2005.03 to Present or
			// 2005.03 - Present
			"\\d{4}\\.\\d{1,2}\\p{Space}*(,|-|to)??\\p{Space}*(\\d{4}\\.\\d{1,2}|Present|Current|Now|Currently|Presently|Till Date)" };
	private int endOfDocument;
	private int lineNumOfLastExp = -1;

	public DateExtractor() {
	}

	/**
	 * 1. Extracts dates from the resume 2. For each matched phrases 2.1 Check
	 * whether the it falls between two period 2.2 Extract the line, lineNum
	 * from Phrase object 2.3 Extract the lengthOfTime from DateObject 2.4
	 * Create DataedPhrase Object and store the above two values
	 * 
	 * @param nonLemmaSentences
	 * @param matchedPhrases
	 * @return
	 */
	public static HashMap<String, DatedPhrase> extractDatesandExp(List<Sentence> nonLemmaSentences,
			Map<Phrase, Integer> matchedPhrases) {
		HashMap<String, DatedPhrase> hashmapOfDatedPhrases = new HashMap<String, DatedPhrase>();
		int sentenceIndex = 0;
		String originalPhrase = "";
		DatedPhrase tempdateObj;
		DateExtractor dateExtractor = new DateExtractor();
		dateExtractor.extractDates(nonLemmaSentences);
		ArrayList<DateObject> tempArryDateObject = dateExtractor.calculateYrs();

		for (DateObject dateObj : tempArryDateObject) {
			for (Phrase p : matchedPhrases.keySet()) {

				if (p instanceof SynonymPhrase) {
					SynonymPhrase p2 = (SynonymPhrase) p;
					sentenceIndex = p2.getSentence();
					originalPhrase = p2.getSourcePhrase();
					if (hashmapOfDatedPhrases.containsKey(originalPhrase)) {
						tempdateObj = hashmapOfDatedPhrases.get(originalPhrase);
						if (dateObj.phraseExists(sentenceIndex)) {
							if (tempdateObj.getDateObjects().contains(dateObj)) {
								tempdateObj.setLengthOfTime(0, 0); // dont count
																	// the yrs
																	// if the
																	// phrases
																	// appeared
																	// more than
																	// once
																	// within
																	// the same
																	// period.
							} else {
								tempdateObj.setLengthOfTime(dateObj.getYears(), dateObj.getMonths());
								tempdateObj.addDateObj(dateObj);
							}
							tempdateObj.addPhraseAndDateObj(p, dateObj);
							hashmapOfDatedPhrases.put(originalPhrase, tempdateObj);
						} else {
							tempdateObj.addPhraseAndDateObj(p, null);
							hashmapOfDatedPhrases.put(originalPhrase, tempdateObj);
						}
					} else {
						tempdateObj = new DatedPhrase();
						if (dateObj.phraseExists(sentenceIndex)) {
							if (tempdateObj.getDateObjects().contains(dateObj)) {
								tempdateObj.setLengthOfTime(0, 0); // dont count
																	// the yrs
																	// if the
																	// phrases
																	// appeared
																	// more than
																	// once
																	// within
																	// the same
																	// period.
							} else {
								tempdateObj.setLengthOfTime(dateObj.getYears(), dateObj.getMonths());
								tempdateObj.addDateObj(dateObj);
							}
							tempdateObj.addPhraseAndDateObj(p, dateObj);
							hashmapOfDatedPhrases.put(originalPhrase, tempdateObj);
						} else {
							tempdateObj.addPhraseAndDateObj(p, null);
							hashmapOfDatedPhrases.put(originalPhrase, tempdateObj);
						}
					}
				} else {
					sentenceIndex = p.getSentence();
					originalPhrase = p.getBuffer();
					if (hashmapOfDatedPhrases.containsKey(originalPhrase)) {
						tempdateObj = hashmapOfDatedPhrases.get(originalPhrase);
						if (dateObj.phraseExists(sentenceIndex)) {
							if (tempdateObj.getDateObjects().contains(dateObj)) {
								tempdateObj.setLengthOfTime(0, 0); // dont count
																	// the yrs
																	// if the
																	// phrases
																	// appeared
																	// more than
																	// once
																	// within
																	// the same
																	// period.
							} else {
								tempdateObj.setLengthOfTime(dateObj.getYears(), dateObj.getMonths());
								tempdateObj.addDateObj(dateObj);
							}
							tempdateObj.addPhraseAndDateObj(p, dateObj);
							hashmapOfDatedPhrases.put(originalPhrase, tempdateObj);
						} else {
							tempdateObj.addPhraseAndDateObj(p, null);
							hashmapOfDatedPhrases.put(originalPhrase, tempdateObj);
						}
					} else {
						tempdateObj = new DatedPhrase();
						if (dateObj.phraseExists(sentenceIndex)) {
							if (tempdateObj.getDateObjects().contains(dateObj)) {
								tempdateObj.setLengthOfTime(0, 0); // dont count
																	// the yrs
																	// if the
																	// phrases
																	// appeared
																	// more than
																	// once
																	// within
																	// the same
																	// period.
							} else {
								tempdateObj.setLengthOfTime(dateObj.getYears(), dateObj.getMonths());
								tempdateObj.addDateObj(dateObj);
							}
							tempdateObj.addPhraseAndDateObj(p, dateObj);
							hashmapOfDatedPhrases.put(originalPhrase, tempdateObj);
						} else {
							tempdateObj.addPhraseAndDateObj(p, null);
							hashmapOfDatedPhrases.put(originalPhrase, tempdateObj);
						}
					}
				}
				tempdateObj = null;
			}
		}
		return hashmapOfDatedPhrases;
	}

	public void extractDates(ArrayList<String> results) {
		String temp;
		int lineNum = 0;
		for (String s : results) {
			temp = trimString(s);
			matchAllPatterns(temp, lineNum);
			lineNum++;
		}
		if (endOfDocument < 0) {
			endOfDocument = lineNum;
		}
	}

	private String trimString(String s) {
		StringBuilder results = new StringBuilder();
		char prevChar;
		for (int i = 0; i < s.length(); i++) {
			if (Character.isDigit(s.charAt(i)) || Character.isLetter(s.charAt(i)) || Character.isSpaceChar(s.charAt(i))
					|| s.charAt(i) == '/' || s.charAt(i) == ',' || s.charAt(i) == '.' || s.charAt(i) == '\t'
					|| s.charAt(i) == '&') {
				results.append(s.charAt(i));
			} else if (s.charAt(i) == '-' && (i + 1) < s.length() && Character.isLetter(s.charAt(i + 1))) { // had
																											// prblem
																											// in
																											// this
																											// QA-Auto-Hyder
																											// Shaafi.pdf
																											// due
																											// to
																											// 02/2005
																											// -03/2009
				results.append(s.charAt(i) + " ");
			} else {
				results.append(" ");
			}
		}
		return results.toString();
	}

	/**
	 * 1Match the line against all the patterns 2.0 if found 2.1 extract the
	 * date 2.2 dump the date \t index of pattern \t lineNum.
	 * 
	 * @param line
	 * @return
	 */
	public String matchAllPatterns(String line, int lineNum) {
		Pattern p = null;
		Matcher m = null;
		String matched = "";
		String[] arryOFDel = { "\\bto", "-", "--", "~" };
		String[] tempDates = null;
		int length = 0;
		int i = 0;
		String sTemp = "";
		String endOfPara = "Education";
		MatchedDateObject mDateObj = null;
		for (String pattern : patternsNew) {
			p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
			m = p.matcher(line);
			if (m.find()) {
				matched = m.group(0);
				mDateObj = new MatchedDateObject(matched, i, lineNum, line);
				arryLstOfDates.add(mDateObj);
				lineNumOfLastExp = lineNum;
				break;
			}

			if (lineNum > lineNumOfLastExp) {
				p = Pattern.compile(endOfPara, Pattern.CASE_INSENSITIVE);
				m = p.matcher(line);
				if (m.find()) {
					endOfDocument = lineNum;
				}
			}
			i++;
		}
		return matched;
	}

	/**
	 * Trim the string to have only letters and digits. discard any punctuation.
	 * 
	 * @param s
	 * @return - return the trimmed string
	 */
	private String trimStr(String s) {
		StringBuilder stl = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			if (Character.isLetterOrDigit(s.charAt(i))) {
				stl.append(s.charAt(i));
			} else {
				stl.append(" ");
			}
		}
		return stl.toString();
	}

	public void clearYrs() {
		arryLstOfDates.clear();
		sortedDates.clear();
	}

	public ArrayList<DateObject> calculateYrs() {
		// if date contains Till Date
		// Jan 12, 2000 to Till Date len 6
		// Jan, 2000 to Till Date len 5
		// Jan 12, 2000 Till Date len 5
		// Jan 2000 Till Date len 4
		// else if date contains Current|PResent|Todate|Now|Currently
		// Jan 12, 2000 to Current 5
		// Jan 12, 2000 Current 4
		// Jan, 2000 to Current 4
		// Jan 2000 Current 3
		// else if date is
		// Jan 12, 2000 to Jan 13, 2001 7
		// Jan 12, 2000 Jan 13, 2001 6
		// Jan 2000 to Jan 2001 5
		// Jan 2000 Jan 2001 4
		// if date contains Till Date
		// 2000 to Till Date len 4
		// 2000 Till Date len 3
		// else if date contains Current|PResent|Todate|Now|Currently
		// 2000 to Current len 3
		// 2000 Current len 2
		// else if date is
		// 2000 to 2001
		// 2000 2001
		// 4.0
		// if 2004 03 to 2004 06 5
		// 2004 03 2004 06 4
		// 2005 05 to Current 4 or 3
		// get month and year to create SimpleDateFormat object
		String[] tempDates;

		DateObject tempDateOutput = null;
		String sTemp, tillDate = "till date", to = " to";
		int length, patternNum = 0, i = 0;
		for (MatchedDateObject s : arryLstOfDates) {

			sTemp = trimStr(s.getDate()).toLowerCase().replaceAll("  ", " ").trim();
			tempDates = sTemp.replaceFirst("  ", " ").split(" ");
			length = tempDates.length;
			patternNum = s.getPatternID();
			if (patternNum == 0) {
				// if date contains Till Date
				// Jan 12, 2000 to Till Date len 6
				// Jan, 2000 to Till Date len 5
				// Jan 12, 2000 Till Date len 5
				// Jan 2000 Till Date len 4
				// else if date contains Current|PResent|Todate|Now|Currently
				// Jan 12, 2000 to Current 5
				// Jan 12, 2000 Current 4
				// Jan, 2000 to Current 4
				// Jan 2000 Current 3
				// else if date is
				// Jan 12, 2000 to Jan 13, 2001 7
				// Jan 12, 2000 Jan 13, 2001 6
				// Jan 2000 to Jan 2001 5
				// Jan 2000 Jan 2001 4

				if (sTemp.endsWith(tillDate)) {
					if (sTemp.contains(to)) {
						if (length == 6) { // Jan 12, 2000 to Till Date len 6
							tempDateOutput = getYears(tempDates[0] + " " + tempDates[2],
									tempDates[4] + " " + tempDates[5], s.getLineNum() + "", s.getLine());
						} else { // Jan, 2000 to Till Date len 5
							tempDateOutput = getYears(tempDates[0] + " " + tempDates[1],
									tempDates[3] + " " + tempDates[4], s.getLineNum() + "", s.getLine());
						}
					} else {
						// Jan 12, 2000 Till Date len 5
						// Jan 2000 Till Date len 4
						if (length == 5) {
							tempDateOutput = getYears(tempDates[0] + " " + tempDates[2],
									tempDates[3] + " " + tempDates[4], s.getLineNum() + "", s.getLine());
						} else {
							tempDateOutput = getYears(tempDates[0] + " " + tempDates[1],
									tempDates[2] + " " + tempDates[3], s.getLineNum() + "", s.getLine());
						}
					}

				} else if (endWith(sTemp)) {
					// else if date contains
					// Current|PResent|Todate|Now|Currently
					// Jan 12, 2000 to Current 5
					// Jan 12, 2000 Current 4
					// Jan, 2000 to Current 4
					// Jan 2000 Current 3

					if (sTemp.contains(to)) {
						if (length == 5) {
							tempDateOutput = getYears(tempDates[0] + " " + tempDates[2], tempDates[4],
									s.getLineNum() + "", s.getLine());
						} else {
							tempDateOutput = getYears(tempDates[0] + " " + tempDates[1], tempDates[3],
									s.getLineNum() + "", s.getLine());
						}
					} else {
						if (length == 4) {
							if (tempDates[2].equalsIgnoreCase(to)) {
								tempDateOutput = getYears(tempDates[0] + " " + tempDates[1], tempDates[3],
										s.getLineNum() + "", s.getLine());
							} // System.out.println("Years: "+
								// getYears(tempDates[0] + " " + tempDates[1]
								// ,tempDates[3]));
							else {
								tempDateOutput = getYears(tempDates[0] + " " + tempDates[2], tempDates[3],
										s.getLineNum() + "", s.getLine());
							}
						} else if (length == 2) // nov2009 present
						{
							tempDateOutput = getYears((splitDates(tempDates[0])), tempDates[1], s.getLineNum() + "",
									s.getLine());
						} else { // Mar 2000 Current
							tempDateOutput = getYears(tempDates[0] + " " + tempDates[1], tempDates[2],
									s.getLineNum() + "", s.getLine());
						}
					}
				} else {
					// else if date is
					// Jan 12, 2000 to Jan 13, 2001 7
					// Jan 12, 2000 Jan 13, 2001 6
					// Jan 2000 to Jan 2001 5
					// Jan 2000 Jan 2001 4
					if (sTemp.contains(to)) {
						if (length == 7) {
							tempDateOutput = getYears(tempDates[0] + " " + tempDates[2],
									tempDates[4] + " " + tempDates[6], s.getLineNum() + "", s.getLine());
						} else if (length == 5) {
							tempDateOutput = getYears(tempDates[0] + " " + tempDates[1],
									tempDates[3] + " " + tempDates[4], s.getLineNum() + "", s.getLine());
						} else if (length == 3) // aug99 to may00
						{
							tempDateOutput = getYears((splitDates(tempDates[0])), (splitDates(tempDates[2])),
									s.getLineNum() + "", s.getLine());
						}
					} else {
						if (length == 6) {
							tempDateOutput = getYears(tempDates[0] + " " + tempDates[2],
									tempDates[3] + " " + tempDates[5], s.getLineNum() + "", s.getLine());
						} else if (length == 4) {
							tempDateOutput = getYears(tempDates[0] + " " + tempDates[1],
									tempDates[2] + " " + tempDates[3], s.getLineNum() + "", s.getLine());
						} else if (length == 2) // nov2009 dec2010
						{
							tempDateOutput = getYears(splitDates(tempDates[0]), splitDates(tempDates[1]),
									s.getLineNum() + "", s.getLine());
						}
					}
				}
			} // if date contains Till Date
				// 2000 to Till Date len 4
				// 2000 Till Date len 3
				// else if date is
				// 2000 to Current len 3
				// 2000 Current len 2
				// 2000 to 2001 len 3
				// 2000 2001 len 2
			else if (patternNum == 1) {
				if (sTemp.endsWith(tillDate)) {
					if (length == 4) {
						// if there is no month specified then by default it
						// will be january month
						tempDateOutput = getYears("Jan " + tempDates[0], tempDates[2] + " " + tempDates[3],
								s.getLineNum() + "", s.getLine());
					} else {
						tempDateOutput = getYears("Jan " + tempDates[0], tempDates[1] + " " + tempDates[2],
								s.getLineNum() + "", s.getLine());
					}
				} else {
					if (length == 3) {
						if (endWith(tempDates[2])) {
							tempDateOutput = getYears("Jan " + tempDates[0], tempDates[2], s.getLineNum() + "",
									s.getLine());
						} else {
							tempDateOutput = getYears("Jan " + tempDates[0], "Jan " + tempDates[2], s.getLineNum() + "",
									s.getLine());
						}
					} else if (length == 2) {
						if (endWith(tempDates[1])) {
							tempDateOutput = getYears("Jan " + tempDates[0], tempDates[1], s.getLineNum() + "",
									s.getLine());
						} else {
							tempDateOutput = getYears("Jan " + tempDates[0], "Jan " + tempDates[1], s.getLineNum() + "",
									s.getLine());
						}
					} else if (length == 1) {
						tempDateOutput = getYears("Jan " + tempDates[0].substring(0, 3), tempDates[0].substring(4),
								s.getLineNum() + "", s.getLine());
					}
				}
			} // 04 2000 to Till date 5
				// 04 2000 to 04 2000 5
				// 04 2000 Till date 4
				// 04 2000 to Current 4
				// 04 2000 04 2000 4
				// 04 2000 Current 3
			else if (patternNum == 2) {
				if (length == 5) {
					tempDateOutput = getYears(tempDates[0] + " " + tempDates[1], tempDates[3] + " " + tempDates[4],
							s.getLineNum() + "", s.getLine());
				} else if (length == 4) {
					tempDateOutput = getYears(tempDates[0] + " " + tempDates[1], tempDates[2] + " " + tempDates[3],
							s.getLineNum() + "", s.getLine());
				} else if (length == 3) {
					tempDateOutput = getYears(tempDates[0] + " " + tempDates[1], tempDates[2], s.getLineNum() + "",
							s.getLine());
				}
			} // 2004 04 to Till Date len 5
				// 2004 05 to 2004 06 len 5
				// 2004 05 2004 06 len 4
				// 2004 05 to Current len 4
				// 2004 05 Current len 3
			else if (patternNum == 3) {
				if (length == 5) {
					if (tempDates[4].equalsIgnoreCase(tillDate)) {
						tempDateOutput = getYears(tempDates[1] + " " + tempDates[0], tempDates[3] + " " + tempDates[4],
								s.getLineNum() + "", s.getLine());
					} else {
						tempDateOutput = getYears(tempDates[1] + " " + tempDates[0], tempDates[4] + " " + tempDates[3],
								s.getLineNum() + "", s.getLine());
					}
				} else if (length == 4) {
					if (tempDates[2].equalsIgnoreCase(to)) {
						tempDateOutput = getYears(tempDates[1] + " " + tempDates[0], tempDates[3], s.getLineNum() + "",
								s.getLine());
					} else {
						tempDateOutput = getYears(tempDates[1] + " " + tempDates[0], tempDates[3] + " " + tempDates[2],
								s.getLineNum() + "", s.getLine());
					}
				}
			} else if (patternNum == 4) {
				tempDateOutput = getYears(tempDates[0] + " " + tempDates[1], tempDates[2] + " " + tempDates[3],
						s.getLineNum() + "", s.getLine());
			}
			if (arryLstOfDates.size() > (i + 1)) {
				tempDateOutput.setEndLine(arryLstOfDates.get((i + 1)).getLineNum());

			} else {
				tempDateOutput.setEndLine(endOfDocument); // the last period of
															// the doc. So the
															// end of the
															// paragrapgh will
															// be the end of the
															// document
			}
			// if(i > 0){
			// System.out.println(i+": "+ arryLstOfDates.size()+" line:"+
			// tempDateOutput.getLineNum());
			// DateObject tempDate = sortedDates.get((i-1));
			// System.out.println(tempDate.getStartDate().toString());
			// Period p =
			// getPeriod(tempDateOutput.getEndDate(),tempDate.getStartDate());
			// tempDate.setGapPeriods(p.getYears(), p.getMonths());
			// sortedDates.add((i-1), tempDate);
			// }
			// if((i+1) == arryLstOfDates.size()){
			// System.out.println(i+"here : "+ arryLstOfDates.size()+" line
			// num"+tempDateOutput.getLineNum());
			// tempDateOutput.setGapPeriods(0, 0);
			// }
			sortedDates.add(tempDateOutput);
			// sortedDates.put(Integer.parseInt(sTempArry[2]), tempDateOutput);
			// arryLstOfDatesfinal.add(tempDateOutput+","+sTempArry[2]);
			// tempDateOutput = null;
			// }
			i++;
		}
		return sortedDates;
	}

	/**
	 *
	 * @param sDate1
	 * @param sDate2
	 * @return
	 */
	public DateObject getYears(String sDate1, String sDate2, String lineNum, String OriginalString) {
		String yearsExp = "";
		boolean recent = false;
		DateTimeFormatter fmt, fmt2;
		LocalDate now = null, date = null;
		String[] temp;
		Period period = null;
		int startindex = 0, endindex = 3, year, month;
		if (Character.isLetter(sDate1.charAt(startindex))) {
			temp = sDate1.split(" ");
			sDate1 = temp[startindex].substring(startindex, endindex) + " " + temp[1];
		}
		try {
			if (endWith(sDate2)) {
				now = new LocalDate();
				recent = true;
			} else {
				if (Character.isLetter(sDate2.charAt(startindex))) {
					temp = sDate2.split(" ");
					sDate2 = temp[startindex].substring(startindex, endindex) + " " + temp[1];
				}
				now = new LocalDate();
				fmt = DateTimeFormat.forPattern(getFormat(sDate2));
				now = now.parse(sDate2, fmt);
			}
			date = new LocalDate();
			fmt2 = DateTimeFormat.forPattern(getFormat(sDate1));
			date = date.parse(sDate1, fmt2);
			period = getPeriod(date, now);
			year = period.getYears();
			month = period.getMonths();
			yearsExp = year + "," + month;
			if (year == 0 && month == 0) {
				month = 6;

			}
		} catch (IllegalFieldValueException i) {
			yearsExp = "0,0"; // if the month is not between 1-12 then yearsExp
								// = 0;
			year = 0;
			month = 0;
		} catch (IllegalArgumentException m) {
			yearsExp = "0,0"; // if the format is not correct
			year = 0;
			month = 0;
		}
		DateObject dateObject = new DateObject(date, now, Integer.parseInt(lineNum), OriginalString, yearsExp, recent,
				year, month);
		return dateObject;
	}

	public LocalDate getDate(String sDate) {
		DateTimeFormatter fmt;
		LocalDate now = null;
		String[] temp;
		int startindex = 0, endindex = 3;
		if (Character.isLetter(sDate.charAt(0))) {
			temp = sDate.split(" ");
			sDate = temp[startindex].substring(startindex, endindex) + " " + temp[1];
		}
		try {
			if (endWith(sDate)) {
				now = new LocalDate();
			} else {
				if (Character.isLetter(sDate.charAt(startindex))) {
					temp = sDate.split(" ");
					sDate = temp[startindex].substring(startindex, endindex) + " " + temp[1];
				}
				now = new LocalDate();
				fmt = DateTimeFormat.forPattern(getFormat(sDate));
				now = now.parse(sDate, fmt);
			}
		} catch (IllegalFieldValueException i) {
			// System.out.println(i.getMessage());
		} catch (IllegalArgumentException m) {
			// System.out.println("M: "+sDate1+"; "+m.getMessage());
		}
		return now;
	}

	public Period getPeriod(LocalDate date, LocalDate now) {
		Period period = new Period(date, now, PeriodType.yearMonthDay());
		return period;
	}

	public String getFormat(String sDate) {
		StringBuilder sBuilder = new StringBuilder();
		String temp = "M";
		for (int i = 0; i < sDate.length(); i++) {
			if (!Character.isSpaceChar(sDate.charAt(i))) {
				sBuilder.append(temp);
			} else {
				sBuilder.append(" ");
				temp = "y";
			}
		}
		// System.out.println(sDate+":"+ sBuilder.toString());
		return sBuilder.toString();
	}

	public static void main(String args[]) throws Exception {

		DateExtractor textExtractor = new DateExtractor();
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA- Sharma
		// Roshan.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Abbas
		// Akram.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Trivedi
		// Chetankumar.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Scherle
		// Jordan.docx");
		// File file = new File("C:\\Preeti\\QA
		// Resume\\QAResumes\\QA-Rubinshtyen Ilya.rtf");
		// File file = new File("C:\\Preeti\\QA
		// Resume\\QAResumes\\QA-Balasubramanian Saranya.doc"); //done testing
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Bansal
		// Namita.pdf");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA- Verma
		// Neha.docx");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Patel
		// Bhavin.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Park
		// Heesung.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Birdi
		// Harman.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-MGR-Mughal
		// Imran.doc");
		// File file = new File("C:\\Preeti\\QA
		// Resume\\QAResumes\\QA-Dir-Ramaswami Seshadri.pdf");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Jain
		// Nitin.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-MGR Mak
		// Mary.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Nguyen
		// Brandon.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Nguyen
		// Brandon.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Trivedi
		// Chetankumar.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Le
		// Ann.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Srivasta
		// Richa.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA Macasiljig
		// Lyndon.doc");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-NK-Vugane
		// Karthik.docx");
		File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\QA-Auto-Hyder Shaafi.pdf");
		// File file = new File("C:\\Preeti\\testDateFormats.txt");
		// File file = new
		// File("C:\\Preeti\\jobpost2\\ResumeSort\\Resumes\\education\\Amy-Campbell.pdf");
		// File file = new File("C:\\Preeti\\QA Resume\\QAResumes\\");
		// GenerateFiles g = new GenerateFiles();
		// List<File> lstFiles = g.generateFiles(file);
		// long startTime = System.currentTimeMillis();
		// for (File f : lstFiles) {
		// System.out.println(f.getName() + ">>>>>>>>>>>>>>>>>>>>>>");
		//// textExtractor.process(f);
		//// DocumentRS doc
		// textExtractor.extractDates(textExtractor.getSentences(f),
		// f.getName());
		// ArrayList<DateObject> temp = textExtractor.calculateYrs();
		// for(DateObject s : temp){
		//// System.out.println(s.getOriginalLine());
		// System.out.println("Line Num: "+
		// s.getLineNum()+
		// "Line "+s.getLineNum()+"\t"+s.getEndPharagraph()+
		//// "\t"+s.getStartDate().toString()+" "+s.getEndDate().toString()+
		// "\t"+s.getLenOfTime()+ "\t"+ "Years:"+ s.getYears()+" month:"
		// +s.getMonths()+" Recent: "+
		// s.isCurrent());//+ "Line: "+s.getOriginalLine());//+"\tGap:
		// "+s.getGapYears()+" ,"+s.getGapMonth());
		// }
		// textExtractor.clearYrs();
		//// textExtractor.extractDates(textExtractor.getString(), f.getName());
		// System.out.println("====================================================");
		HttpClient client = new HttpClient();
		// System.out.println("URL=" + url);
		// GetMethod get = new
		// GetMethod("http://en.wikipedia.org/w/api.php?format=xml&action=query&titles=San_Francisco&prop=images&imlimit=20");
		// GetMethod get = new
		// GetMethod("http://en.wikipedia.org/w/api.php?format=xml&action=query&titles=Software_Engineering&redirects&format=xml");
		// GetMethod get = new
		// GetMethod("http://en.wikipedia.org/w/api.php?format=xml&action=query&titles=San_Francisco&rvprop=content&imlimit=20");
		// GetMethod get = new
		// GetMethod("http://en.wikipedia.org/w/api.php?action=opensearch&search=software&limit=10&namespace=0&format=jsonfm");
		// GetMethod get = new
		// GetMethod("http://en.wiktionary.org/w/api.php?action=opensearch&search=Software");
		// GetMethod get = new
		// GetMethod("http://en.wiktionary.org/wiki/Wikisaurus:sale");
		// GetMethod get = new
		// GetMethod("http://en.wikipedia.org/w/api.php?format=xml&action=query&titles=Software&prop=revisions&rvprop=content");
		// GetMethod get = new
		// GetMethod("http://en.wikipedia.org/w/api.php?action=query&list=search&search=software&limit=10&namespace=0&format=jsonfm");
		// GetMethod get = new
		// GetMethod("http://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=wikipedia&format=jsonfm");
		// GetMethod get = new
		// GetMethod("http://en.wikipedia.org/w/api.php?action=query&titles=Main%20Page%7CFksdlfsdss%7CTalk:&indexpageids");
		// GetMethod get = new
		// GetMethod("http://en.wikipedia.org/w/api.php?action=query&titles=Main%20page&prop=info");
		// client.executeMethod(get);
		//
		// String pageText = get.getResponseBodyAsString();
		// //System.out.println("Results:\n" + pageText);
		// System.out.println(get.getResponseBodyAsString());
		//// }
		long endTime = System.currentTimeMillis();
		// textExtractor.printToFile("Total Time: "+ ((endTime-startTime) *
		// .001));

	}

	public double getTotalTime() {
		return (totalTime);
	}

	private boolean endWith(String tempDate) {
		String[] tempArry = { "ent", "now", "tly", "ate" };
		boolean isEnd = false;
		for (String s : tempArry) {
			if (tempDate.endsWith(s)) {
				isEnd = true;
				break;
			}
		}
		return isEnd;
	}

	public void printToFile(String totalTime) {
		try {
			BufferedWriter bufWrt = new BufferedWriter(new FileWriter("C:\\Preeti\\Acronym\\Test7b.txt"));

			for (MatchedDateObject s : arryLstOfDates) {
				bufWrt.write(s.getDate());
				bufWrt.newLine();
			}
			bufWrt.write(totalTime);
			bufWrt.close();
		} catch (IOException ex) {
			Logger.getLogger(DateExtractor.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * get the sentences using jim's TikaSentenceDetector
	 * 
	 * @param file
	 * @return
	 */
	public ArrayList<String> getSentences(File file) {
		TikaSentenceDetector tika;
		ArrayList<Sentence> arryLstSen = new ArrayList<Sentence>();
		ArrayList<String> arryLstSentences = new ArrayList<String>();
		try {
			tika = new TikaSentenceDetector();
			arryLstSen = tika.split(file);
		} catch (DocumentParserException ex) {
			Logger.getLogger(DateExtractor.class.getName()).log(Level.SEVERE, null, ex);
		}
		for (Sentence t : arryLstSen) {
			arryLstSentences.add(t.toString());
		}
		return arryLstSentences;
	}

	/**
	 * Splits the date which doesnt have space or punctuation between month and
	 * year. ex : jan2000 or jan00
	 * 
	 * @param date
	 * @return
	 */
	private String splitDates(String date) {
		StringBuilder result = new StringBuilder();
		int j = 0;
		for (int i = 0; i < date.length(); i++) {
			if (Character.isLetter(date.charAt(i))) {

				result.append(date.charAt(i));
				j++;
				if (j == 3) {
					if (Character.isLetter(date.charAt(i + 1)) && Character.isDigit(date.charAt(i + 2))) {
						result.append(" " + date.substring(i + 2));
						break;
					}
				}

			} else {
				result.append(" " + date.substring(i));
				break;
			}
		}
		// System.out.println(date+":"+result.toString());
		return result.toString();
	}

	/**
	 * 
	 * @param lemmaSentences
	 */
	public void extractDates(List<Sentence> lemmaSentences) {
		String temp;
		int lineNum = 0;
		for (Sentence s : lemmaSentences) {
			temp = trimString(s.toString());
			matchAllPatterns(temp, lineNum);
			lineNum++;
		}
	}
}

class MatchedDateObject {

	private String matchedDate;
	private int patternID;
	private int lineNum;
	private String line;

	public MatchedDateObject(String mDate, int pID, int lNum, String l) {
		matchedDate = mDate;
		patternID = pID;
		lineNum = lNum;
		line = l;
	}

	public String getDate() {
		return matchedDate;
	}

	public int getPatternID() {
		return patternID;
	}

	public int getLineNum() {
		return lineNum;
	}

	public String getLine() {
		return line;
	}
}