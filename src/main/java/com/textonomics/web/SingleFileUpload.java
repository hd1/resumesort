/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textonomics.PlatformProperties;
import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderPool;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet for uploading resume files
 */
public class SingleFileUpload extends HttpServlet {
	static Logger logger = Logger.getLogger(SingleFileUpload.class);

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", -1);
		PrintWriter out = response.getWriter();
		File resumeDirectory = null;
		HttpSession session = request.getSession();
		boolean maxFilesExceeded = false;
		try {
			WordNetDataProviderPool.getInstance().acquireProvider();
			UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();

			User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			PlatformProperties.getInstance().loggedInUser.set(user); // Set
																		// currrent
																		// user
			String uploadDir = PlatformProperties.getInstance().getResourceFile(getUploadDir(user)).getAbsolutePath()
					+ "/";

			int ourMaxMemorySize = 10000000;
			int ourMaxRequestSize = 2000000000;

			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();

			// Set factory constraints
			factory.setSizeThreshold(ourMaxMemorySize);
			factory.setRepository(new File(uploadDir));

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);

			// Set overall request size constraint
			upload.setSizeMax(ourMaxRequestSize);
			logger.debug("request content type:" + request.getContentType());

			String folderName = request.getParameter("ResumeFolder");
			if (folderName != null && folderName.length() > 0) {
				File dir = new File(uploadDir, ResumeSorter.RESUME_FOLDER + "/" + folderName);
				if (!dir.exists())
					dir.mkdirs();
				resumeDirectory = dir;
			}
			// Parse the request
			if (request.getContentType() != null && request.getContentType().startsWith("multipart/form-data")) {
				List /* FileItem */ items = upload.parseRequest(request);
				// Process the uploaded items
				Iterator iter = items.iterator();
				FileItem fileItem;
				File fout;
				while (iter.hasNext()) {
					fileItem = (FileItem) iter.next();

					if (fileItem.isFormField()) {
						logger.debug("(form field) " + fileItem.getFieldName() + " = " + fileItem.getString());

						// If we receive the md5sum parameter, we've read
						// finished to read the current file. It's not
						// a very good (end of file) signal. Will be better in
						// the future ... probably !
						// Let's put a separator, to make output easier to read.
						if (fileItem.getFieldName().equals("md5sum[]")) {
							// logger.debug("------------------------------ ");
						}
						if (fileItem.getFieldName().equals("ResumeFolder")) {
							logger.info("Resume Folder:" + fileItem.getString());
							folderName = fileItem.getString();
							File dir = new File(uploadDir, ResumeSorter.RESUME_FOLDER + "/" + folderName);
							if (!dir.exists())
								dir.mkdirs();
							session.setAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString(), dir);
							resumeDirectory = dir;
						}
					} else {
						// Ok, we've got a file. Let's process it.
						// Again, for all informations of what is exactly a
						// FileItem, please
						// have a look to
						// http://jakarta.apache.org/commons/fileupload/
						//
						logger.debug("FieldName: " + fileItem.getFieldName());
						logger.debug("File Name: " + fileItem.getName());
						logger.debug("ContentType: " + fileItem.getContentType());
						logger.debug("Size (Bytes): " + fileItem.getSize());
						// If we are in chunk mode, we add ".partN" at the end
						// of the file, where N is the chunk number.
						String uploadedFilename = fileItem.getName();
						fout = new File(uploadDir + ResumeSorter.RESUME_FOLDER + "/" + folderName + "/"
								+ (new File(uploadedFilename)).getName());
						logger.debug("File Out: " + fout.toString());
						long oldsize = -1;
						if (fout.exists()) {
							oldsize = fout.length();
						}
						// write the file
						fileItem.write(fout);
						// If new file size different from old file size delete
						// the parsed file
						if (oldsize != -1 && oldsize != fout.length()) {
							File parsedFile = new File(fout.getParent(), fout.getName() + ".Object");
							if (parsedFile.exists())
								parsedFile.delete();
							parsedFile = new File(fout.getParent(), fout.getName() + ".tikaObject");
							if (parsedFile.exists())
								parsedFile.delete();

						}
						if (resumeDirectory == null) {
							File outdir = (File) session.getAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString());
							resumeDirectory = outdir;
						}
						List<File> resumeFiles = (List<File>) session
								.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
						if (resumeFiles == null) {
							resumeFiles = ProcessResumes.getResumeFiles(resumeDirectory);
						}
						if (fout.getName().toLowerCase().endsWith(".zip")) {
							FileUtils.openZipFile(fout, resumeDirectory.getAbsolutePath());
							resumeFiles = ProcessResumes.getResumeFiles(resumeDirectory);
							// if (resumeFiles.size() >
							// ProcessResumes.MAX_RESUMES_PER_FOLDER)
							fout.delete();
						} else if (!resumeFiles.contains(fout))
							resumeFiles.add(fout);
						session.setAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString(), resumeFiles);
						fileItem.delete();
						if (resumeFiles.size() > ProcessResumes.MAX_RESUMES_PER_FOLDER) {
							maxFilesExceeded = true;
							for (int i = ProcessResumes.MAX_RESUMES_PER_FOLDER; i < resumeFiles.size(); i++) {
								System.out.println("Removing resume file:" + resumeFiles.get(i).getName());
								resumeFiles.get(i).delete();
								resumeFiles.remove(i);
								i--;
							}
						}
					}
				} // while
			}
			logger.debug("" + "End of server treatment ");
			if (maxFilesExceeded)
				session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_ERROR.toString(),
						"Maximum number of files exceeded, only the first " + ProcessResumes.MAX_RESUMES_PER_FOLDER
								+ " will be processed");
			else
				session.removeAttribute(SESSION_ATTRIBUTE.UPLOAD_ERROR.toString());
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		} finally {
			out.close();
			WordNetDataProviderPool.getInstance().releaseProvider();
			UserDataProviderPool.getInstance().releaseProvider();
		}
		// if (resumeDirectory != null)
		// {
		// ZipfileProcessingThread zipThread = new
		// ZipfileProcessingThread(session, resumeDirectory, false);
		// zipThread.start();
		// }
	}

	// Method to get the Upload Directory of the user

	public static String getUploadDir(User user) throws IOException {
		String path = "uploads" + File.separator + user.getLogin() + File.separator;
		logger.debug("User Login: " + user.getLogin());
		File dir = PlatformProperties.getInstance().getResourceFile(path);

		if (!dir.exists())
			dir.mkdirs();

		return path;
	} // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click
		// on the + sign on the left to edit the code.">

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}
