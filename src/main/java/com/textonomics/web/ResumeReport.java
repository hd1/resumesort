/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textonomics.DocumentRS;
import com.textonomics.Phrase;
import com.textonomics.PhraseList;
import com.textonomics.SynonymPhrase;
import java.lang.Integer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Dev Gude
 */
public class ResumeReport {

	protected Set<Phrase> matchedPhrases;
	protected List<String> unmatchedPhrases;
	protected List<String> userSelectedPhrases;
	protected List<String> userEnteredPhrases;
	protected Map<String, Integer> phraseImportance;
	protected List<Boolean> required;
	protected DocumentRS sourceDocument;
	protected Collection<PhraseList> unlemmaPhrases;
	protected Set<String> tags;
	protected Map<String, Integer> resumePhrasesMap;
	protected Set<String> templatePhraseSet;

	protected boolean useStars = true;

	public static String[] starColors = { "#000000", "#66CCFF", // light blue -1
			"#0099FF", // blue - 2
			"#00FF00", // green - 3
			"#FFFF00", // yellow - 4
			"#FF9900", // orange-yellow - 5
			"#FF3399", // pink
			"#CCCCCC", // grey
			"#FF0000", // red
	};
	// green,yellow,red
	public static String[] starColors2 = { "#000000", "#00FF00", // green - 1
			"#66CCFF", // light blue -2
			"#FFFF00", // yellow - 3
			"#0099FF", // blue - 4
			"#FF9900", // orange-yellow - 5
			"#FF3399", // pink
			"#CCCCCC", // grey
			"#FF0000", // red
	};
	public static String[] starText = { "", "Nice to Have", // 1
			"Nice to Have", // 2
			"Good to Have", // 3
			"Good to Have", // 4
			"Should have", // 5
			"Must Have", // 6
			"#CCCCCC", // 7
			"Must Have", // 8
	};

	public static String RESUME_COMMENTS = "_RESUME_COMMENTS_";

	public ResumeReport(DocumentRS sourceDocument, Collection<PhraseList> unlemmaPhrases, Set<Phrase> matchedPhrases,
			List<String> unmatchedPhrases, List<String> userSelectedPhrases, Map<String, Integer> phraseImportance,
			List<Boolean> required, List<String> userEnteredPhrases, Set<String> tags, boolean useStars) {
		this.matchedPhrases = matchedPhrases;
		this.unmatchedPhrases = unmatchedPhrases;
		this.userSelectedPhrases = userSelectedPhrases;
		this.userEnteredPhrases = userEnteredPhrases;
		this.phraseImportance = phraseImportance;
		this.required = required;
		this.sourceDocument = sourceDocument;
		this.unlemmaPhrases = unlemmaPhrases;
		this.tags = tags;
		this.useStars = useStars;
	}

	public Map<String, Integer> getResumePhrasesMap() {
		return resumePhrasesMap;
	}

	public void setResumePhrasesMap(Map<String, Integer> resumePhrasesMap) {
		this.resumePhrasesMap = resumePhrasesMap;
	}

	public boolean isUseStars() {
		return useStars;
	}

	public void setUseStars(boolean useStars) {
		this.useStars = useStars;
	}

	public Set<String> getTemplatePhraseSet() {
		return templatePhraseSet;
	}

	public void setTemplatePhraseSet(Set<String> templatePhraseSet) {
		this.templatePhraseSet = templatePhraseSet;
	}

	public String generateReport(boolean print) {
		return generateReport(print, null);
	}

	public String generateReport(boolean print, Double score) {
		StringBuffer reportBuf = new StringBuffer();
		reportBuf.append("<center><table width=100% class=outertbl cellpadding=0 cellspacing=0>");
		String scoreStr = "";
		if (score != null)
			scoreStr = "&nbsp;&nbsp;(Textnomics Score:" + new DecimalFormat("########0.00").format(score) + ")";

		reportBuf.append(
				"<tr><th colspan=2 bg-color='lightblue' align=center><b>Resume Report</b>" + scoreStr + "</th></tr>\n");
		if (!print)
			reportBuf.append("<tr><td width=300 align=left valign=top>\n");
		else
			reportBuf.append("</table>\n");
		reportBuf.append("<table width=100% frame=box rules=none border=1 class='resumeReport' cellspacing=0>");
		reportBuf.append("<tr><th colspan=3 bg-color='lightblue' align=center><b>Matching Phrases</b></th></tr>\n");
		String br = "<br>";
		if (print)
			br = "";
		reportBuf.append("<tr><th width=200><b>Phrase</b></th><th width=60><b>Importance</b></th><th width=40><b>Occurr"
				+ br + "ences</b></th></tr>\n");
		Map<String, Integer> importance = new HashMap<String, Integer>();
		try {
			Map<String, List<Integer>> idxMap = getMatchesToPhraseIdxs();
			Map<String, String> contexts = getPhrasesToContextMap(sourceDocument, unlemmaPhrases);
			// System.out.println("Contexts:" + contexts);
			int idx = 0;
			for (String phrase : userSelectedPhrases) {
				if (required != null && required.size() > idx && required.get(idx))
					importance.put(phrase.toLowerCase().trim(), 6);
				else
					importance.put(phrase.toLowerCase().trim(), phraseImportance.get(phrase.toLowerCase().trim()));
				idx++;
			}
			Set<String> donePhrases = new HashSet<String>();
			for (String phrase : userEnteredPhrases) {
				if (!idxMap.containsKey(phrase.toLowerCase().trim()))
					continue;
				donePhrases.add(phrase.toLowerCase().trim());
				List<Integer> indexes = idxMap.get(phrase.toLowerCase().trim());
				String sentence = "";
				if (contexts != null)
					sentence = contexts.get(phrase.toLowerCase().trim());
				if (sentence == null)
					sentence = "";
				String srccount = "" + sentence.split("_LIMIT_").length;
				if (srccount.length() > 10)
					srccount = "10";
				String phraseIndexes = indexes.toString();
				phraseIndexes = phraseIndexes.substring(1, phraseIndexes.length() - 1);

				if (print)
					reportBuf.append("<tr><td>" + phrase + "</td>");
				else {
					String userIcon = "tinyInfoU.gif";
					String title = "User Entered Phrase";
					if (this.resumePhrasesMap != null && resumePhrasesMap.containsKey(phrase.trim())) {
						userIcon = "tinyInfoRU.gif";
						title = "Resume Phrase";
					}
					if (this.templatePhraseSet != null && templatePhraseSet.contains(phrase.trim())) {
						userIcon = "tinyInfoT.gif";
						title = "Template Phrase";
					}
					// System.out.println("Phrase:" + phrase + " ResumePhrases:"
					// + resumePhrasesMap);
					reportBuf.append("<tr><td><img  src='" + userIcon + "' title='" + title + "'> ");
					reportBuf
							.append("<img id='synonym' src='plus-square.png' height=12 title='Equivalent Phrases' onclick='getSynonyms(event, \""
									+ phrase.replace('\"', '`') + "\", this);'> ");
					reportBuf.append("<a class='turboLI' id='turboLi' href='#' onclick='selectHighLight(this, \""
							+ phraseIndexes + "\")'>" + phrase + "</td>");
				}

				Integer value = importance.get(phrase.toLowerCase().trim());
				if (value == null)
					value = phraseImportance.get(phrase.toLowerCase().trim());
				if (value == null || value == 0)
					value = 0;
				String col = starColors[value];
				if (!useStars)
					col = starColors[value];
				reportBuf.append("<td><font color=" + col + ">");
				if (value > 5) {
					reportBuf.append("Must have");
				} else {
					if (useStars) {
						for (int i = 0; i < value; i++) {
							reportBuf.append("&#9733");
						}
					} else
						reportBuf.append(starText[value]);
				}
				reportBuf.append("</font></td>");
				int num = indexes.size();
				if (num > 10)
					num = 10;
				if (print)
					reportBuf.append("<td align=center>" + indexes.size());
				else
					reportBuf.append("<td align=center><img src='tinyInfoyellow" + num
							+ ".gif' onclick='getTargetSentences(event,\"" + phrase.trim() + "\");'>");
				boolean tagged = false;
				for (String tag : tags)
					if (tag.trim().equalsIgnoreCase(phrase.trim()))
						tagged = true;
				if (tagged) {
					reportBuf.append("&nbsp;<font color='red'><b>T</b></font>");
				}
				reportBuf.append("</td></tr>\n");

			}
			if (userEnteredPhrases.size() > 0)
				reportBuf.append("<tr><td colspan=3><hr></td></tr>\n");
			int pi = 0;
			for (String phrase : idxMap.keySet()) {
				if (donePhrases.contains(phrase.toLowerCase().trim()))
					continue;
				List<Integer> indexes = idxMap.get(phrase);
				String sentence = "";
				if (contexts != null)
					sentence = contexts.get(phrase.toLowerCase().trim());
				if (sentence == null)
					sentence = "";
				String srccount = "" + sentence.split("_LIMIT_").length;
				if (srccount.length() > 10)
					srccount = "10";
				String phraseIndexes = indexes.toString();
				phraseIndexes = phraseIndexes.substring(1, phraseIndexes.length() - 1);

				if (print)
					reportBuf.append("<tr><td>" + phrase + "</td>");
				else {
					reportBuf.append("<tr><td><img  src='tinyInfo" + srccount
							+ ".gif' onmouseover='handleTooTip2(event, \"" + sentence.replace('\'', '`') + "\");'> ");
					reportBuf.append("<img id='synonym_" + pi
							+ "' src='plus-square.png' height=12 title='Equivalent Phrases' onclick='getSynonyms(event, \""
							+ phrase.replace('\"', '`') + "\", this);'> ");
					reportBuf.append("<a class='turboLI' id='turboLi' href='#' onclick='selectHighLight(this, \""
							+ phraseIndexes + "\")'>" + phrase + "</td>");
				}

				Integer value = importance.get(phrase.toLowerCase().trim());
				if (value == null)
					value = phraseImportance.get(phrase.toLowerCase().trim());
				if (value == null || value == 0)
					value = 0;
				reportBuf.append("<td><font color=" + starColors[value] + ">");
				if (value > 5) {
					reportBuf.append("Must have");
				} else {
					if (useStars) {
						for (int i = 0; i < value; i++) {
							reportBuf.append("&#9733");
						}
					} else
						reportBuf.append(starText[value]);

				}
				reportBuf.append("</font></td>");
				int num = indexes.size();
				if (num > 10)
					num = 10;
				if (print)
					reportBuf.append("<td align=center>" + indexes.size());
				else
					reportBuf.append("<td align=center><img src='tinyInfoyellow" + num
							+ ".gif' onclick='getTargetSentences(event,\"" + phrase.trim() + "\");'>");
				boolean tagged = false;
				for (String tag : tags)
					if (tag.trim().equalsIgnoreCase(phrase.trim()))
						tagged = true;
				if (tagged) {
					reportBuf.append("&nbsp;<font color='red'><b>T</b></font>");
				}
				reportBuf.append("</td></tr>\n");
				pi++;
			}
			// int empty = unmatchedPhrases.size() - idxMap.keySet().size() + 1;
			// for (int i = 0; i < empty;i++)
			// reportBuf.append("<tr><td colpspan=2>&nbsp;</td></tr>\n");

			reportBuf.append("</table>\n");
			if (!print)
				reportBuf.append("</td><td  width=300 align=center valign=top>\n");
			else
				reportBuf.append("<br>\n");
			reportBuf.append("<table width=100% frame=box rules=none border=1 class='missingtbl' cellspacing=0>");
			reportBuf.append(
					"<tr><th colspan=3 bg-color='lightblue' align=center><font color=red><b>Missing Phrases for Phone Interview</b></font></th></tr>\n");
			reportBuf.append(
					"<tr><th width=200><font color=red><b>Phrase</b></font></th><th width=100><font color=red><b>Importance</b></font></th><th>&nbsp;</th></tr>\n");

			boolean skipped = false;
			// Show only phrases with importance > 0
			for (String phrase : unmatchedPhrases) {
				Integer value = importance.get(phrase.toLowerCase().trim());
				if (value == null)
					value = phraseImportance.get(phrase.toLowerCase().trim());
				if (value == null || value == 0)
					value = 0;
				if (value == 0) {
					skipped = true;
					continue;
				}
				String sentence = "";
				if (contexts != null)
					sentence = contexts.get(phrase.toLowerCase().trim());
				if (sentence == null)
					sentence = "";
				String srccount = "" + sentence.split("_LIMIT_").length;
				if (srccount.length() > 10)
					srccount = "10";
				if (print)
					reportBuf.append("<tr><td><font color=red>" + phrase + "</font></td>");
				else if (sentence.equals("") && userEnteredPhrases.contains(phrase.trim())) {
					String userIcon = "tinyInfoU.gif";
					String title = "User Entered Phrase";
					if (this.resumePhrasesMap != null && resumePhrasesMap.containsKey(phrase.trim())) {
						userIcon = "tinyInfoRU.gif";
						title = "Resume Phrase";
					}
					if (this.templatePhraseSet != null && templatePhraseSet.contains(phrase.trim())) {
						userIcon = "tinyInfoT.gif";
						title = "Template Phrase";
					}
					reportBuf.append("<tr><td><img  src='" + userIcon + "' title='" + title + "'> <font color=red>"
							+ phrase + "</font></td>");
				} else
					reportBuf.append("<tr><td><img  src='tinyInfo" + srccount
							+ ".gif' onmouseover='handleTooTip2(event, \"" + sentence.replace('\'', '`')
							+ "\");'> <font color=red>" + phrase + "</font></td>");

				reportBuf.append("<td><font color=red>");
				if (value > 5) {
					reportBuf.append("Must have");
				} else if (print) {
					if (useStars) {
						for (int i = 0; i < value; i++) {
							reportBuf.append("&#9733");
						}
					} else
						reportBuf.append(starText[value]);
				} else {
					if (useStars) {
						for (int i = 0; i < value; i++)
							reportBuf.append("<img src='star_on.gif'>");
					} else
						reportBuf.append(starText[value]);
				}
				boolean tagged = false;
				for (String tag : tags)
					if (tag.trim().equalsIgnoreCase(phrase.trim()))
						tagged = true;
				if (tagged) {
					reportBuf.append("</font></td><td><font color='red'><b>T</b></font></td></tr>\n");
				} else
					reportBuf.append("</font></td><td>&nbsp;</td></tr>\n");
			}
			if (skipped)
				reportBuf.append("<tr><td colspan=3><hr></td></tr>\n");

			// Now Show tagged phrases with importance == 0
			for (String phrase : unmatchedPhrases) {
				Integer value = importance.get(phrase.toLowerCase().trim());
				if (value == null)
					value = phraseImportance.get(phrase.toLowerCase().trim());
				if (value == null || value == 0)
					value = 0;
				if (value != 0)
					continue;
				String sentence = "";
				if (contexts != null)
					sentence = contexts.get(phrase.toLowerCase().trim());
				if (sentence == null)
					sentence = "";
				String srccount = "" + sentence.split("_LIMIT_").length;
				if (srccount.length() > 10)
					srccount = "10";
				if (print)
					reportBuf.append("<tr><td><font color=red>" + phrase + "</font></td>");
				else if (sentence.equals("") && userEnteredPhrases.contains(phrase.trim())) {
					String userIcon = "tinyInfoU.gif";
					String title = "User Entered Phrase";
					if (this.resumePhrasesMap != null && resumePhrasesMap.containsKey(phrase.trim())) {
						userIcon = "tinyInfoRU.gif";
						title = "Resume Phrase";
					}
					if (this.templatePhraseSet != null && templatePhraseSet.contains(phrase.trim())) {
						userIcon = "tinyInfoT.gif";
						title = "Template Phrase";
					}
					reportBuf.append("<tr><td><img  src='" + userIcon + "' title='" + title + "'> <font color=red>"
							+ phrase + "</font></td>");
				} else
					reportBuf.append("<tr><td><img  src='tinyInfo" + srccount
							+ ".gif' onmouseover='handleTooTip2(event, \"" + sentence.replace('\'', '`')
							+ "\");'> <font color=red>" + phrase + "</font></td>");

				reportBuf.append("<td><font color=red>");
				if (value > 5) {
					reportBuf.append("Must have");
				} else if (print) {
					if (useStars) {
						for (int i = 0; i < value; i++) {
							reportBuf.append("&#9733");
						}
					} else
						reportBuf.append(starText[value]);
				} else {
					if (useStars) {
						for (int i = 0; i < value; i++)
							reportBuf.append("<img src='star_on.gif'>");
					} else
						reportBuf.append(starText[value]);
				}
				boolean tagged = false;
				for (String tag : tags)
					if (tag.trim().equalsIgnoreCase(phrase.trim()))
						tagged = true;
				if (tagged) {
					reportBuf.append("</font></td><td><font color='red'><b>T</b></font></td></tr>\n");
				} else
					reportBuf.append("</font></td><td>&nbsp;</td></tr>\n");
			}

		} catch (Exception x) {
			x.printStackTrace();
		}
		reportBuf.append("</table>\n");
		if (!print)
			reportBuf.append("</td></tr>\n");
		else
			reportBuf.append("<br>\n");
		if (print) {
			reportBuf.append(
					"<table width=100% frame=box rules=none border=1 cellspacing=0><tr align=center><td><b>Comments</b></td><tr>");
			reportBuf.append("<tr><td>");
			reportBuf.append(RESUME_COMMENTS);
			reportBuf.append("</td><tr></table></center><br><br>\n");
		}
		if (!print)
			reportBuf.append("</table></center><br><br>\n");

		return reportBuf.toString();
	}

	private Map<String, List<Integer>> getMatchesToPhraseIdxs() throws Exception {
		Map<String, List<Integer>> suggToPhrase = new TreeMap<String, List<Integer>>();
		int phraseIdx = 0;
		for (Phrase phrase : this.matchedPhrases) {
			String suggText = phrase.getBuffer().toLowerCase().trim();
			if (phrase instanceof SynonymPhrase)
				suggText = ((SynonymPhrase) phrase).getSourcePhrase().toLowerCase().trim();

			List<Integer> phraseIndices = suggToPhrase.get(suggText);
			if (phraseIndices == null) {
				phraseIndices = new ArrayList<Integer>();
				suggToPhrase.put(suggText, phraseIndices);
			}
			// if (suggText.toLowerCase().startsWith("shell"))
			// System.out.println("Phrase:" + suggText + " Sent:" +
			// phrase.getSentence() + " Offset: " +
			// phrase.getNgram().getFirst().getPosition() + " indx:" +
			// phraseIdx);
			phraseIndices.add(phraseIdx);
			phraseIdx++;
		}
		return suggToPhrase;
	}

	public static Map<String, String> getPhrasesToContextMap(DocumentRS source, Collection<PhraseList> unlemmaPhrases) {
		Map<String, String> suggToContext = new HashMap<String, String>();
		try {
			int phraseIdx = 0;
			for (PhraseList phrases : unlemmaPhrases) {
				Phrase phrase = phrases.getFirst();
				Phrase lemmaPhrase = ResumeSorter.getLemmatizedPhrase(phrases);
				String suggText = lemmaPhrase.getBuffer().toLowerCase().trim();

				String context = suggToContext.get(suggText);
				if (context == null) {
					String sentences = "";
					String phraseText = phrase.getBuffer().trim();
					List<String> contexts = source.getContexts(phraseText);
					if (contexts == null) {
						// try plural
						if (phraseText.endsWith("y"))
							phraseText = phraseText.substring(0, phraseText.length() - 1) + "ies";
						else if (phraseText.endsWith("um"))
							phraseText = phraseText.substring(0, phraseText.length() - 2) + "a";
						else
							phraseText = phraseText + "s";
						contexts = source.getContexts(phraseText);
						if (contexts == null) {
							phraseText = phraseText.substring(0, phraseText.length() - 1) + "es";
							contexts = source.getContexts(phraseText);
						}
					}
					if (contexts == null) {
						// try past tense
						if (phraseText.endsWith("es"))
							phraseText = phraseText.substring(0, phraseText.length() - 1) + "d";
						contexts = source.getContexts(phraseText);
					}
					if (contexts == null) {
						// try simple plural
						if (phraseText.endsWith("s"))
							phraseText = phraseText.substring(0, phraseText.length() - 1);
						contexts = source.getContexts(phraseText);
					}
					if (contexts == null) {
						// System.out.println("Phrase:" + phrase.getBuffer() + "
						// text:" + phraseText + " Phrase map:" +
						// source.getPhrasesMap());
					}

					if (contexts != null) {
						for (String sentence : contexts) {
							if (!sentences.contains(sentence))
								sentences = sentences + sentence + "_LIMIT_";
						}
					}
					if (sentences.length() == 0)
						sentences = phrase.getBuffer();
					// sentences = sentences.replace(phraseText, ">>" +
					// phraseText + "<<");
					sentences = sentences.replace(phraseText, "<font color=red>" + phraseText + "</font>");
					if (sentences.endsWith("_LIMIT_"))
						sentences = sentences.substring(0, sentences.length() - 7);
					suggToContext.put(suggText, sentences);
					suggToContext.put(phraseText, sentences);
				}
				phraseIdx++;
			}
		} catch (Exception x) {
			x.printStackTrace();
		}
		return suggToContext;
	}
}
