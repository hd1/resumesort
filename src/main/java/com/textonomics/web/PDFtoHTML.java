/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import org.apache.log4j.Logger;

/**
 *
 * @author Dev Gude
 */
public class PDFtoHTML {
	static Logger logger = Logger.getLogger(PDFtoHTML.class);

	public File convert(File inputPDF) throws Exception {
		String fileName = inputPDF.getName();
		if (fileName.lastIndexOf(".") != -1) {
			fileName = fileName.substring(0, fileName.lastIndexOf("."));
			fileName = fileName + ".html";
			System.out.println("New file name:" + fileName);
		}
		return convert(inputPDF, new File(inputPDF.getParent(), fileName));
	}

	public File convert(File inputPDF, File outputHTML) throws Exception {
		File tempOut = new File(outputHTML.getParent(), "_" + outputHTML.getName());
		String command = "pdftohtml -noframes " + inputPDF + " " + tempOut;
		logger.debug(command);

		Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec(command);

		InputStream input = process.getInputStream();
		StringBuffer buffer = new StringBuffer();
		int daLine = -1;
		while ((daLine = input.read()) != -1) {
			buffer.append(daLine);
		}
		input.close();
		if (!tempOut.exists())
			throw new Exception("Error converting pdf to html:" + buffer);
		if (buffer.indexOf("Error") != -1)
			logger.error(buffer);
		removeHtmlSpaceCode(tempOut, outputHTML);
		tempOut.delete();
		return outputHTML;
	}

	private void removeHtmlSpaceCode(File inputHtml, File outputHtml) throws Exception {
		BufferedReader in = new BufferedReader(new FileReader(inputHtml));
		BufferedWriter out = new BufferedWriter(new FileWriter(outputHtml));
		String line;
		try {
			while ((line = in.readLine()) != null) {
				line = line.replace("&nbsp;", " ");
				out.write(line + "\n");
			}
		} finally {
			in.close();
			out.close();
		}
	}
}
