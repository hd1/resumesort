package com.textonomics.web;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Servlet that creates a zip file containing the resume file and job ad file.
 * 
 *
 * @author Rodrigo Neves
 *
 */
public class SendFile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * The compression level for zip file creation (0-9) 0 = No compression 1 =
	 * Standard compression (Very fast) ... 9 = Best compression (Very slow)
	 */
	private static final int COMPRESSION_LEVEL = 1;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SendFile() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) {
		try

		{
			// Get parameters
			String absolutePath = (String) request.getParameter("absolutepath");
			String fileName = (String) request.getParameter("filename");

			// If not zip
			if (!fileName.equals("all")) {
				if (fileName.equals(""))
					return;

				File f = new File(absolutePath + fileName);

				String[] split = fileName.split("\\.");

				// Set Content type depending on file extension
				if (split[1].equals("doc")) {
					response.setContentType("application/msword");
					response.setHeader("Content-Disposition", "inline;filename=\"" + fileName + "\"");
					// response.setHeader("Content-Transfer-Encoding","binary");
				} else if (split[1].equals("txt")) {
					response.setContentType("text/plain");
					response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
				} else {
					response.setContentType("application/x-unknown");
					response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
				}

				// Open an input stream to the file and post the file contents
				// through the servlet output stream

				InputStream in = new FileInputStream(f);
				ServletOutputStream outs = response.getOutputStream();

				int bit = 256;
				try {
					while ((bit) >= 0) {
						bit = in.read();
						outs.write(bit);
					}

				} catch (IOException ioe) {
					ioe.printStackTrace(System.out);
				}

				outs.flush();
				outs.close();
				in.close();
			}
			// If is zip file request
			else {
				String files = (String) request.getSession().getAttribute("allFiles");
				String name = request.getParameter("name");

				ZipFile zipFile = new ZipFile(1, response);
				String[] fileNames = files.split("\\|");

				zipFile.ZipAndSendFiles(fileNames, absolutePath,
						name + "_" + files.substring(0, files.indexOf("_")) + "_All_Files");

			}

		} catch (FileNotFoundException e) {
			System.out.println("File not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while reading the file " + ioe);
		}

	}

}
