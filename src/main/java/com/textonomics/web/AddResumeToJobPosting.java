/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textnomics.data.JobPosting;
import com.textnomics.data.ResumeScore;
import com.textonomics.PlatformProperties;
import com.textonomics.db.UserDBAccess;
import com.textonomics.mail.GenericMailer;
import com.textonomics.subscription.SubscriptionService;
import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderPool;
import java.io.File;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * This class adds a new Resume (may have been emailed) to an existing Job
 * Posting/Resume Folder
 * 
 */
public class AddResumeToJobPosting extends Thread {

	static Logger logger = Logger.getLogger(AddResumeToJobPosting.class);

	JobPosting posting;
	File resume;
	String emailSubject;

	public AddResumeToJobPosting(JobPosting posting, File resume, String emailSubject) {
		try {
			this.posting = posting;
			this.resume = resume;
			if (emailSubject == null)
				emailSubject = "";
			this.emailSubject = emailSubject;
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
	}

	public void run() {

		String userName = posting.getUserName();
		FakeHttpSession session = new FakeHttpSession();
		JobPostingSession jps = new JobPostingSession(userName, posting.getId());
		String directory;
		UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();// Get
																									// a
																									// connection
																									// for
																									// this
																									// thread
		try {
			User user = userDataProvider.getUser(userName);
			directory = ResumeSorter.getUploadDir(userName);
			jps.restore(session, directory);
			List<File> resumeFiles = (List<File>) session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
			int fileIndex = resumeFiles.size();
			if (fileIndex >= ProcessResumes.MAX_RESUMES_PER_FOLDER) {
				resume.delete();
				String replySubject = emailSubject;
				if (!replySubject.startsWith("Re:"))
					replySubject = "Re:" + replySubject;
				String body = "Dear " + user.getName() + ",<br><br>Regarding the resume " + resume.getName()
						+ " that you emailed us. You have exceeded the maximum number resumes ("
						+ ProcessResumes.MAX_RESUMES_PER_FOLDER + ") for job posting: " + posting.getPostingId()
						+ ".<br><br>ResumeSort.com";
				GenericMailer.getInstance().sendMail(user.getEmail(), replySubject, body);
				return;
			}
			if (!resumeFiles.contains(resume)) {
				resumeFiles.add(resume);
				// Add a slot for new file in all file related attributes
				((List) session.getAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString())).add(null);
				((List) session.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString())).add(null);
				((List) session.getAttribute(SESSION_ATTRIBUTE.UNMATCHED_PHRASES.toString())).add(null);
				((List) session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString())).add(null);
				((List) session.getAttribute(SESSION_ATTRIBUTE.TAGGED_HTMLRESUMES.toString())).add(null);
				((List) session.getAttribute(SESSION_ATTRIBUTE.PROCESSED_BY_TIKA.toString())).add(false);
			} else {
				for (int i = 0; i < resumeFiles.size(); i++)
					if (resumeFiles.get(i).getName().equals(resume.getName()))
						fileIndex = i;
			}

			session.setAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString(), user);
			session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString(), directory);
			boolean useTika = "true".equalsIgnoreCase(
					PlatformProperties.getInstance().getUserProperty("com.textnomics.tika_sentence_detector"));
			boolean validSubscription = SubscriptionService.isValidSubscription(user, posting, resumeFiles.size(),
					true);
			DocumentAnalyzer.analyzeResumes(user, session, posting, resumeFiles, false, fileIndex, validSubscription);

			List<ResumeScore> scores = new ResumeScore()
					.getObjects("RESUME_FILE = " + UserDBAccess.toSQL(resume.getName()) + " and JOB_ID ="
							+ posting.getId() + " and user_name =" + UserDBAccess.toSQL(user.getLogin()));
			String rank = "";
			if (scores.size() > 0) {
				scores.get(0).setEmailSubject(emailSubject);
				scores.get(0).update();
				rank = "This resume ranked " + scores.get(0).getRank() + " out of a total of "
						+ scores.get(0).getTotal() + " resumes.<br><br>";
			}
			String replySubject = emailSubject;
			if (!replySubject.startsWith("Re:"))
				replySubject = "Re:" + replySubject;
			if (validSubscription) {
				File highlightedDoc = new HighlightedDocGenerator(session, fileIndex).processResume();
				String body = "Dear " + user.getName()
						+ ",<br><br>Attached is the processed and highlighted resume for job posting:"
						+ posting.getPostingId() + ".<br><br>" + rank + "Best Regards,<br><br>ResumeSort.com";
				GenericMailer.getInstance().sendMail(user.getEmail(), posting.getEmailAddress(), replySubject, body,
						highlightedDoc);
			} else {
				String body = "Dear " + user.getName() + ",<br><br>The resume that you mailed us for job posting:"
						+ posting.getPostingId() + ".<br><br>" + resume.getName()
						+ " has been processed. Please login and subscribe to see the results.<br><br>Best Regards,<br><br>ResumeSort.com";
				GenericMailer.getInstance().sendMail(user.getEmail(), posting.getEmailAddress(), replySubject, body,
						null);
			}
			jps = new JobPostingSession(user.getLogin(), posting.getId());
			jps.save(session);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
