package com.textonomics.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.textonomics.CAKE_WALK_TAG_TYPE;
import com.textonomics.DocumentRS;
import com.textonomics.Phrase;
import com.textonomics.PhraseList;
import com.textonomics.social.SocialProfile;
import com.textonomics.user.User;
import java.io.BufferedReader;
import java.io.FileReader;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.apache.log4j.Logger;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * The class is used to generate the HTML that will contain the dropdown and the
 * scrolling menu.
 * 
 *
 * @author Rodrigo Neves and Nuno Seco
 *
 */

public class ResultHTMLGenerator extends HTMLGenerator {
	static Logger logger = Logger.getLogger(ResultHTMLGenerator.class);

	/**
	 * Header HTML
	 */
	protected String headerHTML;

	/**
	 * Footer HTML
	 */
	protected String footerHTML;

	protected String debugFileName;

	protected int resumeFileIndex;

	protected double processingTime;

	protected Map<Phrase, Integer> matchedPhrases;

	protected Collection<PhraseList> unlemmaPhrases;

	protected Collection<PhraseList> lemmaPhrases;

	protected List<String> unmatchedPhrases;

	protected List<String> userSelectedPhrases;

	protected List<String> userEnteredPhrases;

	protected Map<String, Integer> phraseImportance;

	protected Set<String> tags;

	protected List<Boolean> required;

	protected DocumentRS sourceDocument;

	protected Double score;

	protected Map<String, Integer> resumePhrasesMap;

	protected Set<String> templatePhrases;

	protected boolean useStars;

	protected File resumeFile;

	protected String candidateName;

	/**
	 * 
	 * @param htmlFile
	 *            the html file containing the specific markup
	 * @param suggester
	 *            the suggester used
	 * @throws FileNotFoundException
	 */
	public ResultHTMLGenerator(File htmlFile, Map<Phrase, Integer> phrases, DocumentRS sourceDocument)
			throws FileNotFoundException {
		super(htmlFile);
		this.matchedPhrases = phrases;
		this.sourceDocument = sourceDocument;
	}

	protected User loggedInUser;

	public User getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	/**
	 * Alters the given HTML string so that it contains the necessary markup to
	 * work with the default.js javascript to show the dropdowns
	 * 
	 * @param html
	 *            the html to alter
	 * @return marked up string for using the dropdowns
	 */
	protected String format(String html) {
		StringBuilder builder = new StringBuilder();
		int counter = 0;
		int state = 0;
		char current;

		for (int i = 0; i < html.length(); i++) {
			current = html.charAt(i);

			if (state == 0 && isHtmlBodyStart(i, html)) {
				i += getBodyTagLength(i, html) - 1;
				state = 1;
				continue;
			}

			if (state == 0) {
				continue;
			}

			if (state == 1 && isHtmlBodyEnd(i, html)) {
				return builder.toString();
			}

			if (isTag(i, html, CAKE_WALK_TAG_TYPE.START_CAKE_WALK_TAG)) {
				i += CAKE_WALK_TAG_TYPE.START_CAKE_WALK_TAG.getTag().length() - 1;
				builder.append("<span id=\"spErr" + counter + "\"");

				// if (phrasesToHighLight.contains(counter))
				// builder.append(" class=\"spErr\">");
				// else
				builder.append(">");

				counter++;
				state = 2;
				continue;
			}

			if (isTag(i, html, CAKE_WALK_TAG_TYPE.END_CAKE_WALK_TAG)) {
				i += CAKE_WALK_TAG_TYPE.END_CAKE_WALK_TAG.getTag().length() - 1;
				builder.append("</span>");
				state = 1;
				continue;
			}

			builder.append(current);
		}

		return null;
	}

	/**
	 * Writes the footer of the HTML to the given stream
	 * 
	 * @param writer
	 */
	protected void writeFooter(FileWriter writer) throws IOException {
		if (footerHTML == null || footerHTML.trim().length() == 0) {

			writer.write("</td>");
			writer.write("</tr>");

			writer.write("</table>");

			writer.write("<br>");
			writer.write(newline);
			// writer.write("<center>");
			// writer.write(newline);
			//
			// writer
			// .write("<input type=\"button\" style=\"COLOR:black;
			// background:#A0FFA0;\"name=\"Submit\" onClick=\"submit();\"
			// value=\" Submit Corrections \"/>");
			// writer.write(newline);
			// writer.write("</center>");
			// writer.write(newline);
		}

		// writer.write("<center><input type=\"button\" style=\"COLOR:black;
		// background:#3c961e; \"name=\"Submit\" onClick=\"submit();\" value=\"
		// Submit Corrections \"/></center>");
		// <input type=\"button\" style=\"COLOR:black;
		// background:#A0FFA0;\"name=\"Get More\"
		// onClick=\"getMoreSuggestions(20)\" value=\" More Highlights \"/>
		// writer.write(newline);

		List<String> bodyStmts;
		if ((bodyStmts = statments.get(STATEMENT.BODY)) != null) {
			for (String body : bodyStmts) {
				writer.write(body);
				writer.write(newline);
			}
		}

		/*
		 * writer.write("<script type=\"text/javascript\">");
		 * 
		 * //Enter "frombottom" or "fromtop"
		 * writer.write("var verticalpos=\"fromtop\";");
		 * writer.write("var maxNumChoices=60;");
		 * writer.write("var numStepChoices=20;");
		 * 
		 * writer.write("if (!document.layers)");
		 * writer.write("document.write('</div>');");
		 * 
		 * writer.write("function getInnerMenu(){");
		 * 
		 * writer.write("var innerHTML=\"\";");
		 * writer.write("innerHTML=\"<tr>\"");
		 * writer.write("+\"<td width=\"100%\" bgcolor=\"#FFFFCC\">\""); writer.
		 * write("+\"<p align=\"center\"><b><font size=\"4\">HiLite</font></b></td>\""
		 * ); writer.write("+\"</tr>\""); writer.write("+\"<tr>\"");
		 * writer.write("+\"<td width=\"100%\" bgcolor=\"#FFFFFF\">\"");
		 * writer.write("+\"<p align=\"left\">\";");
		 * 
		 * writer.write(
		 * "for(i=numStepChoices;i<=maxNumChoices;i=i+numStepChoices){");
		 * writer.write("innerHTML+= \"<a href=\"\"> Showing \"+i+\"</a><br>\";"
		 * ); writer.write("}"); writer.write("innerHTML+=\"</tr>\";");
		 * 
		 * writer.
		 * write(" var table = document.getElementById(\"tableScrollMenu\");");
		 * writer.write("table.innerHTML=innerHTML;");
		 * 
		 * 
		 * writer.write("}");
		 * 
		 * writer.write("getInnerMenu();");
		 * 
		 * writer.write("function JSFX_FloatTopDiv()"); writer.write("{");
		 * writer.write("var startX = 3,"); writer.write("startY = 150;");
		 * writer.
		 * write("var ns = (navigator.appName.indexOf(\"Netscape\") != -1);");
		 * writer.write("var d = document;"); writer.write("function ml(id)");
		 * writer.write("{"); writer.
		 * write("var el=d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];"
		 * ); writer.write("if(d.layers)el.style=el;"); writer.write(
		 * "el.sP=function(x,y){this.style.left=x;this.style.top=y;};");
		 * writer.write("el.x = startX;");
		 * writer.write("if (verticalpos==\"fromtop\")");
		 * writer.write("el.y = startY;"); writer.write("else{"); writer.
		 * write("el.y = ns ? pageYOffset + innerHeight : document.body.scrollTop + document.body.clientHeight;"
		 * ); writer.write("el.y -= startY;"); writer.write("}");
		 * writer.write("return el;"); writer.write("}");
		 * writer.write("window.stayTopLeft=function()"); writer.write("{");
		 * writer.write("if (verticalpos==\"fromtop\"){");
		 * writer.write("var pY = ns ? pageYOffset : document.body.scrollTop;");
		 * writer.write("ftlObj.y += (pY + startY - ftlObj.y)/8;");
		 * writer.write("}"); writer.write("else{"); writer.
		 * write("var pY = ns ? pageYOffset + innerHeight : document.body.scrollTop + document.body.clientHeight;"
		 * ); writer.write("ftlObj.y += (pY - startY - ftlObj.y)/8;");
		 * writer.write("}"); writer.write("ftlObj.sP(ftlObj.x, ftlObj.y);");
		 * writer.write("setTimeout(\"stayTopLeft()\", 10);");
		 * writer.write("};"); writer.write("ftlObj = ml(\"divStayTopLeft\");");
		 * writer.write("stayTopLeft();"); writer.write("}");
		 * writer.write("JSFX_FloatTopDiv();"); writer.write("</script>");
		 */

		// writer.write("</center>");
		if (footerHTML != null && footerHTML.trim().length() > 0) {
			writer.write(footerHTML);
		} else {
			writer.write("</body>");
			writer.write("<br />");
			writer.write("<br />");
			writer.write(newline);
			writer.write("</html>");
		}
	}

	/*
	 * protected void writeHeader(FileWriter writer) throws IOException {
	 * writer.write("<html>"); writer.write(newline); writer.write("<head>");
	 * writer.write(newline); writer.
	 * write("<META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=utf-8\">"
	 * ); writer.write(newline);
	 * writer.write("<title>Resume Suggestions</title>"); writer.write(newline);
	 * writer.write("<meta http-equiv=\"expires\" content=\"0\">");
	 * writer.write(newline); writer.
	 * write("<link rel=\"stylesheet\" href=\"default.css\" type=\"text/css\">"
	 * ); writer.write(newline);
	 * 
	 * writer.write("<script language=\"JavaScript\">"); writer.write(newline);
	 * 
	 * writer.write("var i=0;"); writer.write(newline);
	 * 
	 * writer.write("var optionList  = new Array();"); writer.write(newline);
	 * writer.write("var helpList    = new Array();"); writer.write(newline);
	 * writer.write("var extrasIndex = new Array();"); writer.write(newline);
	 * 
	 * SuggestionSet sourcePhrases; String synonymString;
	 * 
	 * //Added by igo String tempOptionsList; String tempHelpList; String
	 * tempExtrasIndex;
	 * 
	 * for (Phrase phrase : suggestions.keySet()) { sourcePhrases =
	 * suggestions.get(phrase); synonymString =
	 * toCommaDelimitedSynonyms(sourcePhrases); String
	 * suggestionsString=toCommaDelimitedSuggestions(sourcePhrases);
	 * 
	 * tempOptionsList=phrase.getBuffer() + ", " + suggestionsString;
	 * 
	 * if (synonymString != null){
	 * tempOptionsList=tempOptionsList+", "+synonymString; //writer.write(", " +
	 * synonymString); }
	 * 
	 * //writer.write("optionList[i] = \"" + phrase.getBuffer() + ", " +
	 * suggestionsString);
	 * 
	 * //Added by igo writer.write("optionList[i] = \"" + tempOptionsList);
	 * optionsList.add(indexLists,tempOptionsList);
	 * 
	 * writer.write("\";"); writer.write(newline);
	 * 
	 * tempExtrasIndex= Integer.toString((synonymString == null ? 0 :
	 * (sourcePhrases.size() + 1))); extrasIndex.add(tempExtrasIndex);
	 * 
	 * writer.write("extrasIndex[i] = " + tempExtrasIndex + ";");
	 * writer.write(newline);
	 * 
	 * tempHelpList="|" + toPipeDelimitedContexts(sourcePhrases);
	 * helpList.add(tempHelpList); indexLists++;
	 * 
	 * writer.write("helpList[i] = \"" + tempHelpList + "\";");
	 * writer.write(newline); writer.write("i++;"); writer.write(newline);
	 * writer.write(newline);
	 * 
	 * }
	 * 
	 * writer.write("</script>"); writer.write(newline);
	 * 
	 * writer.
	 * write("<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"default.js\">"
	 * ); writer.write(newline); writer.write("</script>");
	 * writer.write(newline); writer.write("</head>"); writer.write(newline);
	 * writer.write("<body  onLoad=\"init();\">"); writer.write(newline); writer
	 * .write("<div id='tt' class=\"ttip\"><table border=0 cellpaddng=0 cellspacing=0 class=\"ttipBody\"><TBODY id=\"ttBody\"></TBODY></table>"
	 * ); writer.write(newline); writer.write("</div>"); writer.write(newline);
	 * writer.write("<div class=\"feedbackPane\" id='feedbackPane'>");
	 * writer.write(newline); writer.write("<table>"); writer.write(newline);
	 * writer.write("<tr><td class=\"header\">Feedback for");
	 * writer.write(newline); writer.
	 * write("<span class=\"header\" id=\"feedbackRegarding\"></span></td>");
	 * writer.write(newline); writer.write("<td class=\"feedbackLink\">");
	 * writer.write(newline); writer.
	 * write("<a href='void(0);' onclick='return hideFeedback();'>close</a></td>"
	 * ); writer.write(newline); writer.write("</tr>"); writer.write(newline);
	 * writer.write("<tr><td colspan=\"2\">"); writer.write(newline); writer.
	 * write("<textarea name=\"Comment\" id=\"feedbackTextArea\" COLS=\"40\" ROWS=\"10\"></textarea></td>"
	 * ); writer.write(newline); writer.write("</tr>"); writer.write(newline);
	 * writer.write("</table>"); writer.write(newline); writer.write("</div>");
	 * writer.write(newline);
	 * 
	 * writer
	 * .write("<table bordercolor=\"blue\" width=\"70%\" height=400 border=1 cellpadding=0 cellspacing=0 onclick=\"return handleClick(event)\">"
	 * ); writer.write(newline); writer.write("<tr valign=top>");
	 * writer.write(newline); writer.write("<td class=\"textRegion\">");
	 * writer.write(newline); }
	 */

	/**
	 * Write the header of the HTML to the given stream
	 * 
	 * @param writer
	 * 
	 */
	protected void writeHeader(FileWriter writer) throws IOException {
		String synonymString;

		// Added by igo
		String tempOptionsList;
		String tempHelpList;
		String tempExtrasIndex;

		// First add div with suggestions/phraseIndexes
		StringBuffer suggToPhraseBuf = new StringBuffer();
		try {

			// suggToPhraseBuf.append("\n<div id='turboTuner'>\n");
			// suggToPhraseBuf.append("<input type=button value='Email Resume'
			// onclick='sendResume()'>\n");
			suggToPhraseBuf.append("<table cellpadding=5 id='turboTable'>\n");
			suggToPhraseBuf.append("<tr><td align='left'><b>Enter a comment for the Resume:</b></td></tr>\n");
			suggToPhraseBuf.append(
					"<tr><td align='left' valign=top><textarea id='resumecomment' name=comment rows=4 cols=30></textarea></td></tr>\n");
			suggToPhraseBuf.append(
					"<tr><td align='center'><img id='savecomment' src='images/save_button.png' onclick='saveComment(true)' value='Save'></td></tr>\n");
			suggToPhraseBuf.append("<tr><td align='center'><hr></tr>\n");
			suggToPhraseBuf.append("<tr><td align='left'><b>Comment History:</b></td></tr>\n");
			suggToPhraseBuf.append(
					"<tr><td align='left' valign=top><textarea id='commenthistory' readonly rows=30 cols=30></textarea></td></tr>\n");
			suggToPhraseBuf.append("</table>\n");
			// int i = 0;
			// Map<String, List<Integer>> idxMap = getMatchesToPhraseIdxs();
			// Map<String, String> contexts = getPhrasesToContextMap();
			// //System.out.println("ContextMap:" + contexts);
			// for (String phrase: idxMap.keySet())
			// {
			// String phraseIndexes = idxMap.get(phrase).toString();
			// String sentence = "";
			// if (contexts != null)
			// sentence = contexts.get(phrase.toLowerCase().trim());
			// if (sentence == null) sentence = "";
			//
			// suggToPhraseBuf.append("<tr id='turboTR" + i +"'
			// onMouseOver=\"this.style.backgroundColor='#C8D8E8'\"
			// onMouseOut=\"this.style.backgroundColor=''\">\n");
			// String srccount = "" + sentence.split("_LIMIT_").length;
			// if (srccount.length() > 10) srccount = "10";
			// String targcount = "" + 1;
			// if (targcount.length() > 1) targcount = "10";
			// //suggToPhraseBuf.append("<td class='turboTD'><img id =
			// 'turboIconT" + i + "' src='tinyInfoyellow" + targcount + ".gif'
			// onmouseover='getTargetPhrases(event,\"" + phrase + "\");'>" +
			// "</td>\n");
			// suggToPhraseBuf.append("<td class='turboTD'><img id = 'turboIcon"
			// + i + "' src='tinyInfo" + srccount + ".gif'
			// onmouseover='handleTooTip2(event, \"" +
			// sentence.replace('\'','`') + "\");'>" + "</td>\n");
			// suggToPhraseBuf.append("<td class='turboTD'><a class='turboLI'
			// id='turboLi" + i + "' href='#' onclick='selectHighLight(this, \""
			// + phraseIndexes + "\")'>"
			// + phrase.replace('"',' ') + "</a></td>\n");
			// suggToPhraseBuf.append("<td class='turboTD'>" +
			// idxMap.get(phrase).size() + "</td>\n");
			// //suggToPhraseBuf.append("<td class='turboTD'><a
			// href='javascript:void(0);' onclick='javascript:deleteAll("+ i
			// +",\"" + phraseIndexes + "\",\"" + phrase.replace('"',' ') +
			// "\")'><img id = 'turboDel" + i + "' border='0' src='x.png'
			// title='Mark this suggestion as Invalid and Remove
			// it'></a></td>\n");
			// suggToPhraseBuf.append("</tr>\n");
			// i++;
			// }
			// suggToPhraseBuf.append("</table>\n");
			//
			// suggToPhraseBuf.append("<table
			// style='display:none;background-color:#d7ffd7' cellpadding=5
			// id='missingTable'>\n");
			// suggToPhraseBuf.append("<tr><td align='center'
			// colspan=3><b>Missing Phrases<br>from the Job
			// Posting</b></td></tr>\n");
			// i = 0;
			// for (String phrase: unmatchedPhrases)
			// {
			// String sentences = "";
			// if (contexts != null)
			// sentences = contexts.get(phrase.toLowerCase().trim());
			// if (sentences == null) sentences = "";
			// String srccount = "" + sentences.split("_LIMIT_").length;
			// if (srccount.length() > 10) srccount = "10";
			// suggToPhraseBuf.append("<tr id='missingTR" + i +"'
			// onMouseOver=\"this.style.backgroundColor='#C8D8E8'\"
			// onMouseOut=\"this.style.backgroundColor='#d7ffd7'\">\n");
			// suggToPhraseBuf.append("<td class='missing2TD'><img id =
			// 'turboIcon" + i + "' src='tinyInfo" + srccount + ".gif'
			// onmouseover='handleTooTip2(event, \"" +
			// sentences.replace('\'','`') + "\");'>" + "</td>\n");
			// suggToPhraseBuf.append("<td class='missing1TD' id='missingTD" + i
			// + "'>" + phrase.replace('\'',' ').replace('"',' ') + "</td>\n");
			// //suggToPhraseBuf.append("<td class='missing2TD'><a
			// href='javascript:void(0);' onclick='javascript:deleteMissing(" +
			// phrase.replace('\'',' ').replace('"',' ') + "\")'><img id =
			// 'missingDel" + i + "' border='0' src='x.png' title='Mark this
			// phrase as Invalid and Remove it'></a></td>\n");
			// suggToPhraseBuf.append("</tr>\n");
			// i++;
			// }
			//
			// suggToPhraseBuf.append("</table>\n");
			suggToPhraseBuf.append("<script>\n");
			suggToPhraseBuf.append("viewFileIndex = " + this.getResumeFileIndex() + "\n");
			int ind = 0;
			// This can be null if no matching/analysis done on the resume
			if (matchedPhrases != null) {
				for (Phrase phrase : matchedPhrases.keySet()) {
					suggToPhraseBuf.append("importance[" + ind + "]=" + matchedPhrases.get(phrase) + "; // "
							+ phrase.getBuffer() + "\n");
					// logger.debug("- Matched Phrase:" + phrase.getBuffer() + "
					// ind:" + ind + " Imp:" + matchedPhrases.get(phrase));
					ind++;
				}
			}
			suggToPhraseBuf.append("</script>\n"); // writer.write(suggToPhraseBuf.toString());
		} catch (Exception x) {
			System.out.println("Error generation turbo sugg/phrase map:" + x);
		}

		if (headerHTML != null && headerHTML.trim().length() > 0) {
			headerHTML = headerHTML.replace("REPLACE_TURBO_TUNER", suggToPhraseBuf.toString());
			headerHTML = headerHTML.replace("REPLACE_FULLNAME", getCandidateName());
			headerHTML = headerHTML.replace("REPLACE_PATH", Authenticator.requestURL);
			try {
				SocialProfile sp = new SocialProfile();
				String linkedInProfileURL = sp.getLinkedInProfileURL(getCandidateName());
				String twitterUserName = sp.getTwitterProfileURL(getCandidateName());
				headerHTML = headerHTML.replace("REPLACE_TWITTER_USERNAME", twitterUserName);
				headerHTML = headerHTML.replace("REPLACE_LINKEDIN_URL", linkedInProfileURL);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			if (score != null)
				headerHTML = headerHTML.replace("_SCORE_", new DecimalFormat("########0.00").format(score));
			if (false && loggedInUser.isTester()) {
				headerHTML = headerHTML.replace("REPLACE_TESTLINK", " <a id='testlink' href='downloadFile.jsp?fileName="
						+ URLEncoder.encode(getDebugFileName()) + "' target=_blank>Download Debug Files</a>");
			} else {
				headerHTML = headerHTML.replace("REPLACE_TESTLINK", "");
			}
			writer.write(headerHTML);
		} else {
			writer.write("<html>");
			writer.write(newline);
			writer.write("<head>");
			writer.write(newline);
			writer.write("<META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=utf-8\">");
			writer.write(newline);
			writer.write("<title>Resume Suggestions</title>");
			writer.write(newline);
			writer.write("<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">");
			writer.write(newline);
			writer.write("<META HTTP-EQUIV=\"Expires\" CONTENT=\"-1\">");
			writer.write(newline);
			writer.write("<link rel=\"stylesheet\" href=\"default.css\" type=\"text/css\">");
			writer.write(newline);

			writer.write(newline);

			writer.write("<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"default.js\">");
			writer.write(newline);
			writer.write("</script>");
			writer.write(newline);

			writer.write("</head>");
			writer.write(newline);
			writer.write("<body  onLoad=\"init();\" onclick=\"return handleClick(event);\">");
			writer.write(newline);

			// writer.write("<center>");

			writer.write(
					"<div id='tt' class=\"ttip\"><table border=0 cellpaddng=0 cellspacing=0 class=\"ttipBody\"><TBODY id=\"ttBody\"></TBODY></table>");
			writer.write(newline);
			writer.write("</div>");
			writer.write(newline);
			writer.write("<div class=\"feedbackPane\" id='feedbackPane'>");
			writer.write(newline);
			writer.write("<table>");
			writer.write(newline);
			writer.write("<tr><td class=\"header\">Feedback for");
			writer.write(newline);
			writer.write("<span class=\"header\" id=\"feedbackRegarding\"></span></td>");
			writer.write(newline);
			writer.write("<td class=\"feedbackLink\">");
			writer.write(newline);
			writer.write("<a href='void(0);' onclick='return hideFeedback();'>close</a></td>");
			writer.write(newline);
			writer.write("</tr>");
			writer.write(newline);
			writer.write("<tr><td colspan=\"2\">");
			writer.write(newline);
			writer.write("<textarea name=\"Comment\" id=\"feedbackTextArea\" COLS=\"40\" ROWS=\"10\"></textarea></td>");
			writer.write(newline);
			writer.write("</tr>");
			writer.write(newline);
			writer.write("</table>");
			writer.write(newline);
			writer.write("</div>");
			writer.write(newline);

			// Added by igo
			writer.write("<div id='menus' class=\"text\">");
			writer.write(newline);
			writer.write("<br />");
			writer.write(
					"<table border=\"0\" cellpadding=\"0\" width=\"70%\" cellspacing=\"0\" style=\"margin-left: 32px\">");
			writer.write("<tr>");
			writer.write("<td align=\"center\" colspan=\"6\">");
			writer.write("<div id=\"logo\" ><img src=\"images/logo3d.png\" alt=\"ResumeTuner\" border=\"0\"/></div>");
			writer.write("</td>");
			writer.write("</tr>");
			writer.write("<tr>");
			writer.write("<td colspan=\"6\"><br />");
			writer.write("<span style=\"font-family:Arial, Helvetica, sans-serif ;font-size:16px; \"> ");
			writer.write(
					"Press on the <span style=\"BACKGROUND-COLOR: yellow\">highlighted</span>  phrases to see suggestions from the job posting you submitted.  The scrolling menu allows you to display more or less highlights.<br /><br />");
			writer.write(
					"Remember, you can enter your own text, give us feedback, and tag errors all on the same drop down menu. The system learns and gets better over time.  We are also looking forward to your feedback.<br /><br />");
			writer.write(
					"Once you submit your changes at the end, we will email you your modified resume with your changes <span style=\"BACKGROUND-COLOR: yellow\">highlighted</span> again.  We will also have <i>more recommendations</i> and comments for you in the email.  Just remove the highlighting from the emailed resume, check its formatting, and you are ready to submit your resume.<br /><br />");
			writer.write("Don't forget the scrolling Menu;  it is watching you ;-)<br />");
			writer.write("</span>");
			writer.write("</td>");
			writer.write("</tr>");
			writer.write("<tr>");
			writer.write("<td>");
			writer.write("<br />");
			writer.write("<br />");
			writer.write("<b>Legend:</b>");
			writer.write("</td>");
			writer.write("</tr>");
			writer.write("<tr>");
			writer.write("<td align=\"center\" bgcolor=\"#FFFF00\">");
			writer.write("Original Text");
			writer.write("</td>");
			writer.write("<td align=\"center\" bgcolor=\"#92d050\">");
			writer.write("From the Job Ad");
			writer.write("</td>");
			writer.write("<td align=\"center\" bgcolor=\"#00b0f0\">");
			writer.write("Your direct text");
			writer.write("</td>");
			writer.write("<td align=\"center\" bgcolor=\"#D0EEB0\">");
			writer.write("From More link");
			writer.write("</td>");
			writer.write("<td align=\"center\" bgcolor=\"#ff3300\">");
			writer.write("Original text you kept");
			writer.write("</td>");
			writer.write("<td align=\"center\" bgcolor=\"#ffe0a0\">");
			writer.write("Suggested earlier");
			writer.write("</td>");
			writer.write("</tr>");
			writer.write("</table>");
			writer.write("<br />");
			writer.write("<br />");
			writer.write(
					"<table id=\"tableResume\" bordercolor=\"#ff9600\" width=\"70%\" height=400 border=\"2\" cellpadding=0 cellspacing=0 style=\"margin-left: 32px\">");
			writer.write(newline);

			writer.write("<tr valign=top>");
			writer.write(newline);
			writer.write("<td class=\"textRegion\">");
			writer.write(newline);
		}

		// This can be null if no matching/analysis done on the resume
		if (matchedPhrases != null) {
			System.out.println("Phrases to Report:" + matchedPhrases.keySet().size() + ":" + matchedPhrases);
			ResumeReport reporter = new ResumeReport(sourceDocument, unlemmaPhrases, matchedPhrases.keySet(),
					unmatchedPhrases, userSelectedPhrases, phraseImportance, required, userEnteredPhrases, tags,
					useStars);
			reporter.setResumePhrasesMap(resumePhrasesMap);
			reporter.setTemplatePhraseSet(templatePhrases);
			String resumeReport = reporter.generateReport(false);
			writer.write(resumeReport);
			String coverLetter = getCoverLetterHtml(resumeFile);
			if (coverLetter.length() > 0) {
				writer.write("<div style='width:600px' ><hr>" + coverLetter + "<hr></div>");
			}
		}
	}

	public Map<String, List<Integer>> getMatchesToPhraseIdxs() throws Exception {
		Map<String, List<Integer>> suggToPhrase = new TreeMap<String, List<Integer>>();
		int phraseIdx = 0;
		for (Phrase phrase : this.matchedPhrases.keySet()) {
			String suggText = phrase.getBuffer().toLowerCase();

			List<Integer> phraseIndices = suggToPhrase.get(suggText);
			if (phraseIndices == null) {
				phraseIndices = new ArrayList<Integer>();
				suggToPhrase.put(suggText, phraseIndices);
			}
			phraseIndices.add(phraseIdx);
			phraseIdx++;
		}
		return suggToPhrase;
	}

	public Map<String, String> getPhrasesToContextMap() {
		Map<String, String> suggToContext = new HashMap<String, String>();
		try {
			int phraseIdx = 0;
			for (PhraseList phrases : this.unlemmaPhrases) {
				Phrase phrase = phrases.getFirst();
				Phrase lemmaPhrase = ResumeSorter.getLemmatizedPhrase(phrases);
				String suggText = lemmaPhrase.getBuffer().toLowerCase().trim();

				String context = suggToContext.get(suggText);
				if (context == null) {
					String sentences = "";
					List<String> contexts = sourceDocument.getContexts(phrase.getBuffer().trim());
					if (contexts == null) {
						System.out.println(
								"Phrase:" + phrase.getBuffer() + " Phrase map:" + sourceDocument.getPhrasesMap());
					}
					// System.out.println("......Phrase:" + phrase.getBuffer() +
					// " sentence:" + contexts.get(0));
					if (contexts != null) {
						for (String sentence : contexts)
							sentences = sentences + sentence + "_LIMIT_";
					}
					if (sentences.length() == 0)
						sentences = phrase.getBuffer();
					// sentences =
					// sentences.replaceAll(phrase.getBuffer().trim(), ">>" +
					// phrase.getBuffer().trim() + "<<");
					sentences = sentences.replaceAll(phrase.getBuffer().trim(),
							"<font color='red'>" + phrase.getBuffer().trim() + "</font>");
					suggToContext.put(suggText, sentences);
				}
				phraseIdx++;
			}
		} catch (Exception x) {
			x.printStackTrace();
		}
		return suggToContext;
	}

	public String getFooterHTML() {
		return footerHTML;
	}

	public void setFooterHTML(String footerHTML) {
		this.footerHTML = footerHTML;
	}

	public String getHeaderHTML() {
		return headerHTML;
	}

	public void setHeaderHTML(String headerHTML) {
		this.headerHTML = headerHTML;
	}

	public String getDebugFileName() {
		return debugFileName;
	}

	public void setDebugFileName(String debugFileName) {
		this.debugFileName = debugFileName;
	}

	public double getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(double processingTime) {
		this.processingTime = processingTime;
	}

	public int getResumeFileIndex() {
		return resumeFileIndex;
	}

	public void setResumeFileIndex(int resumeFileIndex) {
		this.resumeFileIndex = resumeFileIndex;
	}

	public Map<Phrase, Integer> getMatchedPhrases() {
		return matchedPhrases;
	}

	public void setMatchedPhrases(Map<Phrase, Integer> matchedPhrases) {
		this.matchedPhrases = matchedPhrases;
	}

	public List<String> getUnmatchedPhrases() {
		return unmatchedPhrases;
	}

	public void setUnmatchedPhrases(List<String> unmatchedPhrases) {
		this.unmatchedPhrases = unmatchedPhrases;
	}

	public DocumentRS getSourceDocument() {
		return sourceDocument;
	}

	public void setSourceDocument(DocumentRS sourceDocument) {
		this.sourceDocument = sourceDocument;
	}

	public Collection<PhraseList> getUnlemmaPhrases() {
		return unlemmaPhrases;
	}

	public void setUnlemmaPhrases(Collection<PhraseList> unlemmaPhrases) {
		this.unlemmaPhrases = unlemmaPhrases;
	}

	public Collection<PhraseList> getLemmaPhrases() {
		return lemmaPhrases;
	}

	public void setLemmaPhrases(Collection<PhraseList> lemmaPhrases) {
		this.lemmaPhrases = lemmaPhrases;
	}

	public List<Boolean> getRequired() {
		return required;
	}

	public void setRequired(List<Boolean> required) {
		this.required = required;
	}

	public Map<String, Integer> getPhraseImportance() {
		return phraseImportance;
	}

	public void setPhraseImportance(Map<String, Integer> phraseImportance) {
		this.phraseImportance = phraseImportance;
	}

	public List<String> getUserSelectedPhrases() {
		return userSelectedPhrases;
	}

	public void setUserSelectedPhrases(List<String> userSelectedPhrases) {
		this.userSelectedPhrases = userSelectedPhrases;
	}

	public List<String> getUserEnteredPhrases() {
		return userEnteredPhrases;
	}

	public void setUserEnteredPhrases(List<String> userEnteredPhrases) {
		this.userEnteredPhrases = userEnteredPhrases;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Map<String, Integer> getResumePhrasesMap() {
		return resumePhrasesMap;
	}

	public void setResumePhrasesMap(Map<String, Integer> resumePhrasesMap) {
		this.resumePhrasesMap = resumePhrasesMap;
	}

	public boolean isUseStars() {
		return useStars;
	}

	public void setUseStars(boolean useStars) {
		this.useStars = useStars;
	}

	public File getResumeFile() {
		return resumeFile;
	}

	public void setResumeFile(File resumeFile) {
		this.resumeFile = resumeFile;
	}

	public Set<String> getTemplatePhrases() {
		return templatePhrases;
	}

	public void setTemplatePhrases(Set<String> templatePhrases) {
		this.templatePhrases = templatePhrases;
	}

	public String getCandidateName() {
		return candidateName;
	}

	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}

	public static String getCoverLetterHtml(File resumeFile) {
		StringBuffer buff = new StringBuffer();
		String fileName = resumeFile.getName();
		if (fileName.lastIndexOf(".") != -1) {
			fileName = fileName.substring(0, fileName.lastIndexOf("."));
		}
		File coverLetter = new File(resumeFile.getParent(), ResumeSorter.COVERLETTER_PREFIX + fileName + ".txt");
		boolean clhtml = false;
		if (!coverLetter.exists()) {
			coverLetter = new File(resumeFile.getParent(), ResumeSorter.COVERLETTER_PREFIX + fileName + ".html");
			clhtml = true;
		}
		try {
			if (coverLetter.exists()) {
				BufferedReader reader = new BufferedReader(new FileReader(coverLetter));
				String line;
				boolean bodytag = false;
				if (!clhtml)
					buff.append("<PRE>\n");
				while ((line = reader.readLine()) != null) {
					if (clhtml && line.indexOf("<body") != -1) {
						bodytag = true;
						line = line.substring(line.indexOf("<body"));
						line = line.replace("<body", "<div");
					}
					if (clhtml && !bodytag)
						continue;
					if (clhtml && line.indexOf("</body>") != -1)
						line = line.replace("</body>", "</div>");
					buff.append(line);
					if (!clhtml)
						buff.append("\n");
					else
						buff.append("<br>\n");
				}
				if (!clhtml)
					buff.append("</PRE>\n");

			}
		} catch (Exception x) {
			x.printStackTrace();
		}
		return buff.toString();
	}
}