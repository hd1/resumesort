/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textonomics.PlatformProperties;
import com.textonomics.user.User;
import com.textonomics.user.UserDataProviderPool;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet for uploading resume files
 */
public class GoogleLogin extends HttpServlet {
	static Logger logger = Logger.getLogger(GoogleLogin.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", -1);
		File resumeDirectory = null;
		HttpSession session = request.getSession();
		try {
			String access_token = request.getParameter("access_token");
			System.out.println(
					"URI:" + request.getRequestURL() + " pathinfo:" + request.getPathInfo() + " token:" + access_token);
			request.getRequestDispatcher("/google_login.jsp?access_token=" + access_token).forward(request, response);
			logger.debug("" + "End of server treatment ");
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
	}

}
