/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textnomics.data.DeletedPhrase;
import com.textnomics.data.JobPosting;
import com.textnomics.data.ResumeScore;
import com.textnomics.data.SharedJobPosting;
import com.textnomics.data.UserTemplate;
import com.textnomics.data.UserTemplatePhrase;
import com.textonomics.DocumentProcessorRS;
import com.textonomics.DocumentRS;
import com.textonomics.NumericFilter;
import com.textonomics.Phrase;
import com.textonomics.PhraseList;
import com.textonomics.PlatformProperties;
import com.textonomics.db.DBAccess;
import com.textonomics.db.UserDBAccess;
import com.textonomics.nlp.DefaultNLPSuite;
import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderPool;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ResumeSocial extends ResumeSorter {

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", -1);
		Map<String, String> phrasesSynonyms = new HashMap<String, String>();
		try {
			WordNetDataProviderPool.getInstance().acquireProvider();
			UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();

			HttpSession session = request.getSession();
			User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			PlatformProperties.getInstance().loggedInUser.set(user); // Set
																		// currrent
																		// user

			// CLear session; user may have used back button
			session.removeAttribute(SESSION_ATTRIBUTE.SUGGESTER.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.TARGET_FILE.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
			String postingId = request.getParameter("postingId");
			String inviter = request.getParameter("inviter");
			String requestStamp = "" + System.currentTimeMillis();
			String uploadDir = getUploadDir(inviter);

			// Getting the uploaded Job Post

			File sourceFile; // Job Post

			String email = user.getEmail();
			String selectedPosting = "";
			String selectedResume = "";
			StringBuffer userIndustry = new StringBuffer();
			List<File> resumeFiles = new ArrayList<File>();
			boolean postingIdEntered = true;

			List<JobPosting> postings = new JobPosting().getObjects(
					"POSTING_ID =" + UserDBAccess.toSQL(postingId) + " and user_name=" + UserDBAccess.toSQL(inviter));
			JobPosting posting = null;
			if (postings.size() == 0) {
				request.getRequestDispatcher("/home.jsp?error=Job Posting: " + postingId + " not found")
						.forward(request, response);
				return;
			}

			posting = postings.get(0);
			List<SharedJobPosting> shared = new SharedJobPosting()
					.getObjects("JOB_ID =" + posting.getId() + " and iduser=" + user.getId());
			if (shared.size() == 0 && !user.isAdmin()) {
				throw new Exception("You do not have permission to access Job Posting: " + postingId);
			}
			SharedJobPosting sjp = shared.get(0);
			List<ResumeScore> scores = new ResumeScore()
					.getObjects("JOB_ID=" + posting.getId() + " and user_name=" + UserDBAccess.toSQL(inviter));
			String resumeFolderName = posting.getResumeFolder();
			if (resumeFolderName != null) {
				File resumeDirectory = new File(PlatformProperties.getInstance().getResourceFile(uploadDir),
						ResumeSorter.RESUME_FOLDER + "/" + resumeFolderName);
				File[] files = resumeDirectory.listFiles();
				for (File file : files) {
					if (!file.getName().endsWith("Object") && !file.getName().startsWith(ResumeSorter.CONVERTED_PREFIX)
							&& !file.getName().startsWith(ResumeSorter.COVERLETTER_PREFIX)
							&& !file.getName().startsWith(".") && !file.getName().toLowerCase().endsWith(".zip"))
						resumeFiles.add(file);
				}
			} else {
				for (int i = 0; i < scores.size(); i++) {
					resumeFiles.add(PlatformProperties.getInstance().getResourceFile(uploadDir + "/" + RESUME_FOLDER
							+ "/" + scores.get(i).getResumeFolder() + "/" + scores.get(i).getResumeFile()));
				}
			}
			session.setAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString(), postingId);
			session.setAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString(), posting);
			// Adding filters
			keyTokenIdentifier.addKeyTermIdentifer(defaultKeyTokenIdentifier);

			sourceTokenFilterer.addTokenFilter(tokenListFilterMap.get("Default 300")); // Checks
																						// if
																						// the
																						// word
																						// is
																						// in
																						// the
																						// top
																						// 300
																						// words
																						// list.
			// sourceTokenFilterer.addTokenFilter(new StyleFilter());
			sourceTokenFilterer.addTokenFilter(new NumericFilter());

			// Extracting Phrases
			System.out.println("Source File:" + uploadDir + "/" + posting.getPostingFile());
			Collection<PhraseList> sortedPhrases = new LinkedHashSet<PhraseList>();
			Collection<PhraseList> nonLemmaPhrases = new ArrayList<PhraseList>();
			Collection<PhraseList> lemmaPhrases = new ArrayList<PhraseList>();
			if (posting.getPostingFile() != null && posting.getPostingFile().length() > 0) {
				sourceFile = PlatformProperties.getInstance().getResourceFile(
						uploadDir + "/" + ResumeSorter.JOBPOSTING_FOLDER + "/" + posting.getPostingFile());

				DocumentProcessorRS docProc = new DocumentProcessorRS(new DefaultNLPSuite(), keyTokenIdentifier,
						sourceTokenFilterer, 8);
				DocumentRS jobPost = docProc.process(sourceFile, false, true);

				nonLemmaPhrases = jobPost.getDocumentPhrases();

				lemmaPhrases = getLemmatizedPhrases(nonLemmaPhrases, jobPost); // Get
																				// lemmatized
																				// phrases

				// Sorting phrases:
				// 1. Proper Nouns appear first in the list of phrases being
				// displayed.
				// 2. Followed by Nouns phrases and then by Verb Phrases
				// 3. The phrases are sorted again within the group by the order
				// of length first and then alphabetical order using the
				// PhaseSorter internal comparator.

				// int i=0;

				List<PhraseList> properNouns = new ArrayList<PhraseList>();
				List<PhraseList> verbPhrases = new ArrayList<PhraseList>();
				List<PhraseList> nounPhrases = new ArrayList<PhraseList>();

				for (PhraseList phraseList : lemmaPhrases) {
					Phrase phrase = phraseList.iterator().next().getPhrase();
					String text = phrase.getBuffer().toLowerCase().trim();
					int count = new DeletedPhrase().getCount("phrase =" + UserDBAccess.toSQL(text));
					if (count > 0)
						continue;
					// String chunkTag = phrase.getChunkTag();
					String pos[] = phrase.getNgram().getPosSequence().split(" ");

					if (pos[0].startsWith("NNP")) // Extract Proper Nouns
					{
						properNouns.add(phraseList);
						// System.out.println("Chunk Tag: " + chunkTag + " for
						// phrase: " + phrase.getBuffer());
					} else if (pos[0].startsWith("NN") && pos.length > 1) // Extract
																			// Noun
																			// Phrases
					{
						nounPhrases.add(phraseList);
						// System.out.println("Chunk Tag: " + chunkTag + " for
						// phrase: " + phrase.getBuffer());
					} else // Extract the remaining
					{
						verbPhrases.add(phraseList);
						// System.out.println("Chunk Tag: " + chunkTag + " for
						// phrase: " + phrase.getBuffer());
					}
					// i++;
				}

				if (!"true"
						.equals(PlatformProperties.getInstance().getUserProperty("com.textonomics.use_wikisynonyms"))) {
					for (PhraseList phraseList : lemmaPhrases) {
						Phrase phrase = phraseList.iterator().next().getPhrase();
						String synonyms = getWikiSynonyms(user, phrase.getBuffer());
						phrasesSynonyms.put(phrase.getBuffer().trim(), synonyms);
					}
				}
				// System.out.println("Synonyms:" + phrasesSynonyms);

				// Collections.sort(properNouns, new PhraseSorter());
				// Collections.sort(nounPhrases, new PhraseSorter());
				// Collections.sort(verbPhrases, new PhraseSorter());

				List<PhraseList> tempPhrases = new ArrayList<PhraseList>();
				tempPhrases.addAll(properNouns);
				tempPhrases.addAll(nounPhrases);
				tempPhrases.addAll(verbPhrases);
				Collections.sort(tempPhrases, new PhraseSorter(jobPost, phrasesSynonyms));

				sortedPhrases = new LinkedHashSet<PhraseList>();
				sortedPhrases.addAll(tempPhrases);
				/*
				 * for(PhraseList p: sortedPhrases) {
				 * System.out.println("properNouns After sorted: " +
				 * p.iterator().next().getBuffer()); System.out.println("POS: "
				 * + p.iterator().next().getNgram().getPosSequence()); }
				 */

				// Sorting Phrases end

				System.out.println("Number of Job Post Phrases: " + jobPost.getDocumentPhrases().size());
				session.setAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString(), jobPost);
			}
			if (posting.getTemplateName() != null && posting.getTemplateName().length() > 0) {
				UserTemplate template = (UserTemplate) new UserTemplate()
						.getObject("name =" + DBAccess.toSQL(posting.getTemplateName()) + " and user_name="
								+ UserDBAccess.toSQL(posting.getUserName()));
				List<UserTemplatePhrase> templatePhrases = new UserTemplatePhrase()
						.getObjects("template_id=" + template.getId() + " order by phrase");
				session.setAttribute(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString(), templatePhrases);
				for (UserTemplatePhrase templatePhrase : templatePhrases) {
					try {
						String synonyms = getWikiSynonyms(user, templatePhrase.getPhrase());
						phrasesSynonyms.put(templatePhrase.getPhrase(), synonyms);
					} catch (Exception x) {
						x.printStackTrace();
					}
				}
			}
			session.setAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString(), phrasesSynonyms);
			session.setAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_DELETED_SYNONYMS.toString(),
					new HashMap<String, Set<String>>());
			session.setAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString(), sortedPhrases);
			session.setAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString(), lemmaPhrases);
			session.setAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString(), nonLemmaPhrases);
			session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString(), getUploadDir(user.getLogin()));
			session.setAttribute(SESSION_ATTRIBUTE.EMAIL.toString(), email);
			session.setAttribute(SESSION_ATTRIBUTE.KEY_PHRASE_IDENTIFIER.toString(), keyTokenIdentifier);
			session.setAttribute(SESSION_ATTRIBUTE.DEFAULT_PHRASE_FILTER.toString(), sourceTokenFilterer);
			Map<String, Integer> resumePhrases = new TreeMap<String, Integer>();
			session.setAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString(), resumePhrases);
			session.setAttribute(SESSION_ATTRIBUTE.SHARED_POSTING.toString(), sjp);
			RequestDispatcher rd = request.getRequestDispatcher("/select_phrases.jsp");
			System.out.println("Dispatcher:" + rd);
			session.setAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString(), resumeFiles);
			if (rd != null)
				rd.forward(request, response); // Display the results in
												// job_phrases.jsp
			else
				response.sendRedirect("/select_phrases.jsp");

			if (resumeFiles != null && resumeFiles.size() > 0) {
				ZipfileProcessingThread zipThread = new ZipfileProcessingThread(session, resumeFiles, false);
				zipThread.start();
			} else {
				session.removeAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
				String folderName = posting.getResumeFolder();
				File resumeFolder = PlatformProperties.getInstance()
						.getResourceFile(uploadDir + "/" + RESUME_FOLDER + "/" + folderName);
				ZipfileProcessingThread zipThread = new ZipfileProcessingThread(session, resumeFolder, false);
				zipThread.start();
			}

		} catch (Exception ex) {
			Logger.getLogger(ResumeSocial.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			WordNetDataProviderPool.getInstance().releaseProvider();
			UserDataProviderPool.getInstance().releaseProvider();
		}
	}

}
