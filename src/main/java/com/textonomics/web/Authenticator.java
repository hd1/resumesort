package com.textonomics.web;

import org.apache.log4j.*;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import com.textonomics.PlatformProperties;
import com.textonomics.subscription.SubscriptionService;
import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderException;
import com.textonomics.user.UserDataProviderPool;
import java.util.Date;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This servlet is used to authenticate users. It receives a login and password,
 * looks them up in the database, and the forwards the user to Upload page or
 * forwards the use to the login page again.
 *
 * @author Nuno Seco
 *
 */

public class Authenticator extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String requestURL = "";
	static Logger logger = Logger.getLogger(Authenticator.class);

	/**
	 * Cookie name that is created when user authenticates
	 */
	private final static String COOKIENAME = "rcwCookie";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Authenticator() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			BASE64Encoder encoder = new BASE64Encoder();
			UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();// Get
																										// a
																										// connection
																										// for
																										// this
																										// thread
			String login = request.getParameter("login");
			String password = request.getParameter("password");
			User user = null;
			String url = "http://";
			if (request.getProtocol().toUpperCase().startsWith("HTTPS"))
				url = "https://";
			String port = ":" + request.getServerPort();
			if (request.getServerPort() == 443)
				url = "https://";
			if (request.getServerPort() == 80 || request.getServerPort() == 443)
				port = "";

			url = url + request.getServerName() + port + request.getContextPath();
			requestURL = url;
			if (login != null && password != null)// data ok?
			{
				logger.debug("User:" + login.trim());
				user = User.getAuthenticatedUser(login.trim(), password.trim());
				if (user == null) {
					logger.debug("Authentication failed");
				} else {
					user.setLastLogin(new Date());
					userDataProvider.updateUserField(user, "lastLogin");
					userDataProvider.commit();
				}
			}
			if (user != null)// credentials ok?
			{
				// start the session
				HttpSession session = request.getSession(true);
				User olduser = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
				session.setAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString(), user);
				// Cleanup old stuff
				if (olduser == null || !olduser.getLogin().equals(user.getLogin()))
					JobPostingSession.remove(session);

				// Verify if the cookie exists
				Cookie cookies[] = request.getCookies();
				Cookie myCookie = null;
				boolean existsCookie = false;
				if (cookies != null) {
					for (int i = 0; i < cookies.length; i++) {
						if (cookies[i].getName().equals(COOKIENAME)) {
							myCookie = cookies[i];
							existsCookie = true;
							break;
						}
					}
				}

				int cookieCount;
				// Cookie exists
				if (existsCookie) {
					String value = "";
					BASE64Decoder decoder = new BASE64Decoder();

					value = new String(decoder.decodeBuffer(myCookie.getValue()));

					String[] split = value.split(":");
					// Verify if user login corresponds to the cookie login
					if (split[0].equals(login)) {
						myCookie.setMaxAge(Integer.MAX_VALUE);
						response.addCookie(myCookie);
					} else {

						cookieCount = 0;
						value = login + ":" + cookieCount;
						Cookie cookie = new Cookie(COOKIENAME, encoder.encode(value.getBytes()));
						cookie.setMaxAge(Integer.MAX_VALUE);
						response.addCookie(cookie);
					}

				}
				// Create cookie
				else {
					cookieCount = 0;
					String value = login + ":" + cookieCount;
					Cookie cookie = new Cookie(COOKIENAME, encoder.encode(value.getBytes()));
					cookie.setMaxAge(Integer.MAX_VALUE);
					response.addCookie(cookie);

				}

				// AFTER ALPHA REMOVE THIS LINE
				request.setAttribute("email", user.getEmail());

				// Get existing list of MyResumes
				String uploadDir = "uploads" + File.separator + user.getLogin() + File.separator;
				File userFolder = PlatformProperties.getInstance()
						.getResourceFile(uploadDir + File.separator + ResumeSorter.JOBPOSTING_FOLDER + File.separator);
				// Content of folder
				String[] fileNames = null;
				if (userFolder.exists()) {
					fileNames = userFolder.list();
					request.setAttribute("myPostings", fileNames);
				} else
					userFolder.mkdirs();

				String redirectURL = request.getParameter("redirectURL");
				logger.debug("Redirect URL:" + redirectURL);
				String html5upload = request.getParameter("html5upload");
				if ("true".equals(html5upload)) {
					session.setAttribute(SESSION_ATTRIBUTE.HTLM5_UPLOAD.toString(), true);
				}
				// if (!SubscriptionService.isValidSubscription(user))
				// {
				// redirectURL = "rt_request_payment.jsp";
				// }
				if (redirectURL != null && redirectURL.trim().length() != 0 && redirectURL.indexOf("signin") == -1) {
					String base = request.getContextPath();
					redirectURL = redirectURL.replace(base, "");
					logger.debug("Redirecting to" + redirectURL);
					request.getRequestDispatcher(redirectURL).forward(request, response);
				} else
					request.getRequestDispatcher("/home.jsp").forward(request, response);
			} else {
				request.getRequestDispatcher("/signin.jsp?error=Invalid email or password").forward(request, response);
			}
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			UserDataProviderPool.getInstance().releaseProvider();// Never forget
																	// to
																	// release
																	// the
																	// provider!!!!
																	// Other
																	// thread
																	// may
																	// starve!
		}
	}

}
