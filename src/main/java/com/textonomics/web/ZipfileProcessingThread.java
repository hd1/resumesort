/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textnomics.data.DeletedPhrase;
import com.textnomics.data.UserDeletedPhrase;
import com.textonomics.DocumentRS;
import com.textonomics.DocumentProcessorRS;
import com.textonomics.KeyPhraseIdentifier;
import com.textonomics.PhraseFilter;
import com.textonomics.PhraseList;
import com.textonomics.PlatformProperties;
import com.textonomics.nlp.DefaultNLPSuite;
import com.textonomics.nlp.NLPSuite;
import com.textonomics.user.User;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Vinod
 */
public class ZipfileProcessingThread extends Thread {

	static Logger logger = Logger.getLogger(ZipfileProcessingThread.class);
	public static String RESUME_THREAD_POOL = "RESUME_THREAD_POOL";
	boolean noPhrases;
	boolean useTika;

	HttpSession session;

	File resumes = null;
	List<File> resumeFiles = new ArrayList<File>();

	public ZipfileProcessingThread(HttpSession session, File resumes, boolean noPhrases) {
		try {
			useTika = "true".equalsIgnoreCase(
					PlatformProperties.getInstance().getUserProperty("com.textnomics.tika_sentence_detector"));
			logger.debug("USE TIKA FLAG:" + useTika);
			this.session = session;
			this.resumes = resumes;
			this.noPhrases = noPhrases;
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
	}

	public ZipfileProcessingThread(HttpSession session, List<File> resumeFiles, boolean noPhrases) {
		try {
			useTika = "true".equalsIgnoreCase(
					PlatformProperties.getInstance().getUserProperty("com.textnomics.tika_sentence_detector"));
			logger.debug("USE TIKA FLAG:" + useTika);
			this.session = session;
			this.resumeFiles = resumeFiles;
			this.noPhrases = noPhrases;
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
	}

	public void run() {

		WordNetDataProviderPool.getInstance().acquireProvider();

		process();

		WordNetDataProviderPool.getInstance().releaseProvider();
	}

	private void process() {
		long start = System.currentTimeMillis();
		int numResumes = 0;
		try {
			if (resumes != null) {
				String name = resumes.getName();
				String path = resumes.getParent();
				if (name.toLowerCase().endsWith(".zip")) {
					// Zip file output directory
					path = path + "/" + name + ".dir";
					File outdir = new File(path);
					if (!outdir.exists())
						outdir.mkdirs();
					session.setAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString(), outdir);
					resumeFiles.addAll(FileUtils.openZipFile(resumes, path));
				} else if (resumes.isDirectory()) {
					session.setAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString(), resumes);
					File[] files = resumes.listFiles();
					for (File file : files) {
						if (!file.getName().endsWith("Object")
								&& !file.getName().startsWith(ResumeSorter.CONVERTED_PREFIX)
								&& !file.getName().startsWith(ResumeSorter.COVERLETTER_PREFIX)
								&& !file.getName().startsWith("."))
							resumeFiles.add(file);
					}
					System.out.println("resumes is a directory, size:" + resumeFiles.size());
				} else {
					resumeFiles.add(resumes);
					System.out.println("resumes is not a directory, size:" + resumeFiles.size());
				}
			}
			session.setAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(), new Integer(resumeFiles.size()));

			Collection<PhraseList> jpPhrases = (Collection<PhraseList>) session
					.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
			Map<String, Integer> allResumePhrases = (Map<String, Integer>) session
					.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
			if (allResumePhrases == null) {
				allResumePhrases = new TreeMap<String, Integer>();
				session.setAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString(), allResumePhrases);
			}
			session.setAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString(), resumeFiles);
			session.setAttribute(SESSION_ATTRIBUTE.ANALYZER_WAITING.toString(), false);
			int num = 0;
			ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
			session.setAttribute(RESUME_THREAD_POOL, pool);
			numResumes = resumeFiles.size();
			for (File resume : resumeFiles) {
				num++;
				boolean waiting = (Boolean) session.getAttribute(SESSION_ATTRIBUTE.ANALYZER_WAITING.toString());
				// if waiting, phrase selection has already been done by user,
				// go again with matching
				// if (num > 5 && waiting) break;
				System.out.println(resume.toString());
				ResumePhraseThread rpt = new ResumePhraseThread(resume);
				pool.execute(rpt);
			}
			try {
				// Wait till all the threads in the pool are finished
				pool.shutdown();
				pool.awaitTermination(10, TimeUnit.DAYS);
				System.out.println("Threads completed");

			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.removeAttribute(RESUME_THREAD_POOL);
		}
		long end = System.currentTimeMillis();
		session.setAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(), 0);
		if (numResumes > 0)
			logger.info("Time to process " + numResumes + " for resume phrases :" + (end - start) / numResumes
					+ " msecs/resume");
	}

	public class ResumePhraseThread implements Runnable {
		File resume;

		public ResumePhraseThread(File resume) {
			this.resume = resume;
		}

		public void run() {
			WordNetDataProviderPool.getInstance().acquireProvider();
			User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			try {
				Collection<PhraseList> jpPhrases = (Collection<PhraseList>) session
						.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
				Map<String, Integer> allResumePhrases = (Map<String, Integer>) session
						.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
				DocumentRS resumeDoc = null;
				File serializedTikaResume = new File(resume.getPath() + ".tikaObject");
				if (resumeDoc == null && useTika && serializedTikaResume.exists()) {
					resumeDoc = (DocumentRS) FileUtils.deSerializeOject(new File(resume.getPath() + ".tikaObject"));
				}
				File serializedResume = new File(resume.getPath() + ".Object");
				if (resumeDoc == null && serializedResume.exists()) {
					resumeDoc = (DocumentRS) FileUtils.deSerializeOject(new File(resume.getPath() + ".Object"));
				}

				if (resumeDoc == null) {
					// Serialize Resume
					String serializedName = resume.getPath() + ".Object";
					DocumentProcessorRS resumeObject = null;
					try {
						NLPSuite nlpSuite = new DefaultNLPSuite();
						if (useTika) {
							serializedName = resume.getPath() + ".tikaObject";
							nlpSuite = new DefaultNLPSuite(false, true);
						}
						resumeObject = new DocumentProcessorRS(nlpSuite,
								(KeyPhraseIdentifier) session
										.getAttribute(SESSION_ATTRIBUTE.KEY_PHRASE_IDENTIFIER.toString()),
								(PhraseFilter) session.getAttribute(SESSION_ATTRIBUTE.DEFAULT_PHRASE_FILTER.toString()),
								8);
					} catch (IOException ex) {
						java.util.logging.Logger.getLogger(ZipfileProcessingThread.class.getName()).log(Level.SEVERE,
								null, ex);
						ex.printStackTrace();
						return;
					}
					if (!useTika && resume.getName().toLowerCase().endsWith(".pdf")) {
						String fileName = resume.getName();
						if (fileName.contains(" ")) {
							fileName = fileName.replace(' ', '_');
							File newFile = new File(resume.getParent(), fileName);
							resume.renameTo(newFile);
							resume = newFile;
						}
						if (fileName.lastIndexOf(".") != -1) {
							fileName = ResumeSorter.CONVERTED_PREFIX + fileName.substring(0, fileName.lastIndexOf("."));
							fileName = fileName + ".html";
							System.out.println("New file name:" + fileName);
						}
						try {
							resume = new PDFtoHTML().convert(resume, new File(resume.getParent(), fileName));
						} catch (Exception x) {
							logger.error("Error converting resume to pdf file:", x);
							String error = (String) session
									.getAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());
							if (error == null)
								error = "";
							error = error + "\nError converting pdf to html, resume : " + resume.getName();
							session.setAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString(), error);
							int numleft = (Integer) session
									.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
							session.setAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(),
									new Integer(--numleft));
							return;
							// throw new Exception("There was was an error
							// processing your .docx file. Please convert it to
							// .doc and retry.");
						}
					}
					try {
						logger.debug("Processing Resume file:" + resume.getName());
						// ArrayList<Sentence> nonLemmaSentences =
						// sentenceDetector.split(resume); // get non lemmatized
						// sentences from the resume
						// logger.debug("Returned from open office:" +
						// resume.getName() + " num sentences:" +
						// nonLemmaSentences.size());
						resumeObject.process(resume, true, true);
						// resumeObject.setSentences(nonLemmaSentences);
						resumeDoc = resumeObject.getDocument();
						// System.out.println("Num of Sentences:" +
						// resumeDoc.getSentences().size());
						FileUtils.serializeOject(resumeDoc, new File(serializedName));
					} catch (Exception x) {
						x.printStackTrace();
						logger.error("Error processing resume:" + resume.getName(), x);
						String error = (String) session
								.getAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());
						if (error == null)
							error = "";
						error = error + "\nError processing resume : " + resume.getName();
						session.setAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString(), error);
						int numleft = (Integer) session
								.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
						session.setAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(),
								new Integer(--numleft));
						return;
					}
				}
				// logger.debug("Sorting Resume Phrases");
				synchronized (allResumePhrases) {
					// In case of assignment, there is no need to extract
					// phrases
					if (!noPhrases) {
						Collection<PhraseList> resumePhrases = resumeDoc.getDocumentPhrases();
						Collection<PhraseList> lemmaPhrases = ResumeSorter.getLemmatizedPhrases(resumePhrases,
								resumeDoc); // Get lemmatized phrases
						for (PhraseList pl : lemmaPhrases) {
							String phrase = pl.getFirst().getBuffer().trim();
							if (Character.isDigit(phrase.charAt(0)))
								continue;
							if (!Character.isLetter(phrase.charAt(0)))
								continue;
							try {
								if (DeletedPhrase.isDeleted(phrase))
									continue;
								// if (!user.isAdmin() &&
								// UserDeletedPhrase.isUserDeleted(user,
								// phrase))
								// continue;
							} catch (Exception ex) {
								java.util.logging.Logger.getLogger(ZipfileProcessingThread.class.getName())
										.log(Level.SEVERE, null, ex);
							}
							phrase = phrase.replace('\n', ' ').replace('\r', ' ');
							if (allResumePhrases.containsKey(phrase)) {
								Integer count = allResumePhrases.get(phrase);
								allResumePhrases.put(phrase, count + pl.size());
							} else
								allResumePhrases.put(phrase, pl.size());
						}
						List<String> removeList = new ArrayList<String>();
						for (String phrase : allResumePhrases.keySet()) {
							if (Character.isUpperCase(phrase.charAt(0))) {
								String lower = phrase.toLowerCase();
								String upper = phrase.toUpperCase();
								String capitalize = phrase.charAt(0) + phrase.substring(1).toLowerCase();
								if (allResumePhrases.containsKey(lower)) {
									int count = allResumePhrases.get(phrase);
									int countl = allResumePhrases.get(lower);
									allResumePhrases.put(phrase, count + countl);
									removeList.add(lower);
								}
								if (!phrase.equals(upper) && allResumePhrases.containsKey(upper)) {
									int count = allResumePhrases.get(phrase);
									int countl = allResumePhrases.get(upper);
									allResumePhrases.put(upper, count + countl);
									removeList.add(phrase);
								}
								if (!phrase.equals(capitalize) && allResumePhrases.containsKey(capitalize)) {
									int count = allResumePhrases.get(phrase);
									int countl = allResumePhrases.get(capitalize);
									allResumePhrases.put(phrase, count + countl);
									removeList.add(capitalize);
								}
							}
						}
						for (String phrase : removeList) {
							allResumePhrases.remove(phrase);
						}
					}
					int numleft = (Integer) session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString());
					session.setAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(), new Integer(--numleft));
				}
			} finally {
				WordNetDataProviderPool.getInstance().releaseProvider();
			}
		}
	}
}
