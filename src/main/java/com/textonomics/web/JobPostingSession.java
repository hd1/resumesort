/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textnomics.data.JobPosting;
import com.textonomics.PlatformProperties;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dev Gude
 */
public class JobPostingSession {
	long jobPostingId;
	String userName;
	public static final String SAVED_NAME = "SavedSession";

	public JobPostingSession(String userName, long jobPostingId) {
		this.jobPostingId = jobPostingId;
		this.userName = userName;
	}

	public void save(HttpSession session) throws Exception {
		Map savedSession = new HashMap<String, Object>();
		String directory = (String) session.getAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString());
		savedSession.put(SESSION_ATTRIBUTE.JOB_POSTING.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString()));
		savedSession.put(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED.toString()));
		savedSession.put(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED_IMPORTANCE.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED_IMPORTANCE.toString()));
		savedSession.put(SESSION_ATTRIBUTE.TAGGED_HTMLRESUMES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.TAGGED_HTMLRESUMES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.RESUME_FILES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.RESUME_SCORES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.UNMATCHED_PHRASES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.UNMATCHED_PHRASES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString()));
		savedSession.put(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.SELECTED_PHRASES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.SELECTED_PHRASES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.ENTERED_PHRASES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.ENTERED_PHRASES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.TAGS.toString(), session.getAttribute(SESSION_ATTRIBUTE.TAGS.toString()));
		savedSession.put(SESSION_ATTRIBUTE.PHRASE_IMPORTANCE.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.PHRASE_IMPORTANCE.toString()));
		savedSession.put(SESSION_ATTRIBUTE.PHRASE_REQUIRED.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.PHRASE_REQUIRED.toString()));
		savedSession.put(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString()));
		savedSession.put(SESSION_ATTRIBUTE.RESUME_PHRASES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString()));
		savedSession.put(SESSION_ATTRIBUTE.REQUIRED_PHRASES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.REQUIRED_PHRASES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.USE_STARS.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.USE_STARS.toString()));
		savedSession.put(SESSION_ATTRIBUTE.PROCESSED_BY_TIKA.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.PROCESSED_BY_TIKA.toString()));
		savedSession.put(SESSION_ATTRIBUTE.MAX_SCORE.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.MAX_SCORE.toString()));
		savedSession.put(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.USER_SCORES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.USER_SCORES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.CANDIDATE_EMAILS.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_EMAILS.toString()));
		savedSession.put(SESSION_ATTRIBUTE.CANDIDATE_NAMES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_NAMES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.CANDIDATE_ADDRESSES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_ADDRESSES.toString()));
		savedSession.put(SESSION_ATTRIBUTE.CANDIDATE_DISTANCES.toString(),
				session.getAttribute(SESSION_ATTRIBUTE.CANDIDATE_DISTANCES.toString()));
		File serialFile = getSerializedFile(directory);
		FileUtils.serializeOject(savedSession, serialFile);
	}

	public void restore(HttpSession session, String directory) throws Exception {
		File serialFile = getSerializedFile(directory);
		Map savedSession = (Map) deSerializeOject(serialFile);
		session.setAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString(),
				savedSession.get(SESSION_ATTRIBUTE.JOB_POSTING.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED.toString(),
				savedSession.get(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED_IMPORTANCE.toString(),
				savedSession.get(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED_IMPORTANCE.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.TAGGED_HTMLRESUMES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.TAGGED_HTMLRESUMES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.RESUME_FILES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.RESUME_SCORES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.UNMATCHED_PHRASES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.UNMATCHED_PHRASES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString(),
				savedSession.get(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.SELECTED_PHRASES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.SELECTED_PHRASES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.ENTERED_PHRASES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.ENTERED_PHRASES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.TAGS.toString(), savedSession.get(SESSION_ATTRIBUTE.TAGS.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.PHRASE_IMPORTANCE.toString(),
				savedSession.get(SESSION_ATTRIBUTE.PHRASE_IMPORTANCE.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.PHRASE_REQUIRED.toString(),
				savedSession.get(SESSION_ATTRIBUTE.PHRASE_REQUIRED.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString(),
				savedSession.get(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.RESUME_PHRASES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString(),
				savedSession.get(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.REQUIRED_PHRASES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.REQUIRED_PHRASES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.USE_STARS.toString(),
				savedSession.get(SESSION_ATTRIBUTE.USE_STARS.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.PROCESSED_BY_TIKA.toString(),
				savedSession.get(SESSION_ATTRIBUTE.PROCESSED_BY_TIKA.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.MAX_SCORE.toString(),
				savedSession.get(SESSION_ATTRIBUTE.MAX_SCORE.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.USER_SCORES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.USER_SCORES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.CANDIDATE_EMAILS.toString(),
				savedSession.get(SESSION_ATTRIBUTE.CANDIDATE_EMAILS.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.CANDIDATE_NAMES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.CANDIDATE_NAMES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.CANDIDATE_ADDRESSES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.CANDIDATE_ADDRESSES.toString()));
		session.setAttribute(SESSION_ATTRIBUTE.CANDIDATE_DISTANCES.toString(),
				savedSession.get(SESSION_ATTRIBUTE.CANDIDATE_DISTANCES.toString()));
		JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
		session.setAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString(), posting.getPostingId());
	}

	public static void remove(HttpSession session) throws Exception {
		session.removeAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.PHRASES_TO_BE_MATCHED_IMPORTANCE.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.TAGGED_HTMLRESUMES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.MATCHED_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.UNMATCHED_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.SELECTED_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.ENTERED_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.TAGS.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.PHRASE_IMPORTANCE.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.PHRASE_REQUIRED.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.REQUIRED_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.USE_STARS.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.PROCESSED_BY_TIKA.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.MAX_SCORE.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.USER_SCORES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.CANDIDATE_EMAILS.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.CANDIDATE_NAMES.toString());
		session.removeAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString());
	}

	public boolean exists(String directory) {
		File serialFile = getSerializedFile(directory);
		return serialFile.exists();
	}

	public void delete(String directory) {
		File serialFile = getSerializedFile(directory);
		System.out.println("Deleting " + serialFile.getAbsolutePath());
		serialFile.delete();
	}

	private File getSerializedFile(String directory) {
		return PlatformProperties.getInstance()
				.getResourceFile(directory + File.separator + SAVED_NAME + "_" + jobPostingId + ".Object");
	}

	public Object deSerializeOject(File file) throws Exception {
		FileInputStream fis = null;
		ObjectInputStream in = null;
		try {
			fis = new FileInputStream(file);
			in = new ObjectInputStream(fis);
			Object object = in.readObject();
			return object;
		} finally {
			if (fis != null)
				fis.close();
			if (in != null)
				in.close();
		}
	}
}
