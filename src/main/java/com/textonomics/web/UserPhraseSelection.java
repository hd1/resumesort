/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textnomics.data.JobPosting;
import com.textnomics.data.SortPhrase;
import com.textonomics.Phrase;
import com.textonomics.PhraseList;
import com.textonomics.nlp.NGram;
import com.textonomics.nlp.Token;
import com.textonomics.user.User;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Vinod
 */
public class UserPhraseSelection {

	HttpServletRequest request;
	HttpServletResponse response;

	public UserPhraseSelection(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	public void save() throws Exception {
		try {
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			List<String> userSelectedPhrases = new ArrayList<String>(); // Stores
																		// the
																		// list
																		// of
																		// the
																		// user
																		// "selected"
																		// phrases
																		// index.
																		// Here
																		// the
																		// index
																		// is
																		// the
																		// phrase
																		// buffer
																		// itself.
			List<String> userEnteredPhrases = new ArrayList<String>(); // Stores
																		// the
																		// list
																		// of
																		// the
																		// user
																		// "entered"
																		// phrases
																		// text
			List<Boolean> requiredPhrase = new ArrayList<Boolean>();
			List<Integer> stars = new ArrayList<Integer>();
			Map<String, Integer> phraseImportance = new HashMap<String, Integer>();
			Set<String> tags = new HashSet<String>();
			JobPosting posting = (JobPosting) session.getAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());

			Collection<Phrase> phrasesToBeMatched = new ArrayList<Phrase>(); // Stores
																				// the
																				// original
																				// set
																				// of
																				// Phrases
																				// selected
																				// by
																				// the
																				// user
			List<Integer> phrasesToBeMatchedImp = new ArrayList<Integer>();

			// VINOD: Get the indexes of the phrases from the phrase selection
			// page
			Map<String, String[]> paramMap = request.getParameterMap();
			for (String param : paramMap.keySet()) {
				if (param.startsWith("suggphrases")) {
					String[] values = paramMap.get(param);
					String ind = param.substring(param.indexOf("_") + 1);

					if (request.getParameter("required_" + ind) == null
							&& getInt(request.getParameter("rating_" + ind)) == 0
							&& request.getParameter("tag_" + ind) == null)
						continue;
					userSelectedPhrases.add(values[0]);

					stars.add(getInt(request.getParameter("rating_" + ind)));
					phraseImportance.put(values[0].toLowerCase().trim(), getInt(request.getParameter("rating_" + ind)));

					if (request.getParameter("required_" + ind) != null
							&& "true".equals(request.getParameter("required_" + ind))) {
						requiredPhrase.add(true);
					} else {
						if ("maybe1".equals(request.getParameter("required_" + ind))) {
							stars.set(stars.size() - 1, 1);
							phraseImportance.put(values[0].toLowerCase().trim(), 1);
						} else if ("maybe3".equals(request.getParameter("required_" + ind))) {
							stars.set(stars.size() - 1, 3);
							phraseImportance.put(values[0].toLowerCase().trim(), 3);
						}

						requiredPhrase.add(false);
					}

					if (request.getParameter("tag_" + ind) != null) {
						tags.add(values[0]);
						if (phraseImportance.get(values[0].toLowerCase().trim()) == 0)
							phraseImportance.put(values[0].toLowerCase().trim(),
									DocumentAnalyzer.DEFAULT_TAG_IMPORTANCE);
					}
					System.out
							.println("Selected phrase:" + values[0] + " rating:" + request.getParameter("rating_" + ind)
									+ " required:" + request.getParameter("required_" + ind));
				} else if (param.startsWith("userphrase")) {
					String[] values = paramMap.get(param);
					String ind = param.substring(param.indexOf("_") + 1);
					for (String value : values) {
						value = value.trim();
						if (value.length() > 0) {
							userEnteredPhrases.add(value);
							phraseImportance.put(value.toLowerCase(), getInt(request.getParameter("rating_" + ind)));
							if ("maybe3".equals(request.getParameter("required_" + ind))) {
								phraseImportance.put(value.toLowerCase(), 3);
							} else if ("maybe1".equals(request.getParameter("required_" + ind))) {
								phraseImportance.put(value.toLowerCase(), 1);
							}
							if (request.getParameter("tag_" + ind) != null) {
								tags.add(value);
								if (phraseImportance.get(value.toLowerCase()) == 0)
									phraseImportance.put(value.toLowerCase(), DocumentAnalyzer.DEFAULT_TAG_IMPORTANCE);
							}
							System.out.println("user Phrase:" + value.trim() + " Importance:"
									+ request.getParameter("rating_" + ind));
						}
					}
				}
			}
			long timestamp = new Date().getTime();
			Collection<PhraseList> lemmaPhraseLists = (Collection<PhraseList>) session
					.getAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString());
			Collection<PhraseList> nonLemmaPhraseLists = (Collection<PhraseList>) session
					.getAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString());
			Iterator<PhraseList> iterator = nonLemmaPhraseLists.iterator();

			// // Delete any previously saved phrases
			// new SortPhrase().deleteObjects("JOB_ID = " + posting.getId() + "
			// and USER_NAME = " + DBAccess.toSQL(user.getLogin()));

			// VINOD: Get the user selected phrases from the original lemmatized
			// phrases list
			for (PhraseList lemmaPhraseList : lemmaPhraseLists) {
				PhraseList unlemma = iterator.next();
				SortPhrase sp = new SortPhrase();
				sp.setJobId(posting.getId());
				sp.setLemmatizedPhrase(lemmaPhraseList.iterator().next().getPhrase().getPhrase().getBuffer());
				String unlemmaText = unlemma.iterator().next().getPhrase().getBuffer();
				sp.setUserName(user.getEmail());
				sp.setSelected(false);
				sp.setSavedTime(timestamp);
				sp.setOriginalPhrase(unlemmaText);
				int i = 0;
				boolean required = false;
				int starRating = 0;
				boolean tagged = false;
				boolean selected = false;
				for (String selectedPhrase : userSelectedPhrases) // find out
																	// which one
																	// is
																	// suggested
				{
					Phrase phraseToBeCheckedForSelection = lemmaPhraseList.iterator().next().getPhrase();

					if (phraseToBeCheckedForSelection.getBuffer().equalsIgnoreCase(selectedPhrase)) { // if
																										// the
																										// phrase
																										// was
																										// checkmarked
																										// by
																										// the
																										// user

						phrasesToBeMatched.add(lemmaPhraseList.getFirst()); // Add
																			// it
																			// to
																			// the
																			// list
																			// of
																			// selected
																			// lemmatized
																			// phrases
						selected = true;
						required = requiredPhrase.get(i);
						starRating = stars.get(i);
						if (tags.contains(selectedPhrase)) {
							tagged = true;
						}
						if (required)
							phrasesToBeMatchedImp.add(6);
						else
							phrasesToBeMatchedImp.add(starRating);
					}
					i++;
				}

				// Save phrase to database
				try {
					if (selected) {
						sp.setSelected(true);
						sp.setStars(new Double(starRating));
						sp.setRequired(required);
						sp.setTag(tagged);
						if (sp.getId() > 0)
							sp.update();
						else
							sp.insert();
					}
				} catch (Exception x) {
					x.printStackTrace();
				}
			}

			// Construct phrase objects for user entered phrases and add them to
			// the set of selected phrases
			int phraseIndex = 0;

			for (String userEnteredPhrase : userEnteredPhrases) {
				phraseIndex++;

				int tokenPosition = 0;
				NGram ngram = new NGram();
				String[] tokenBuffers = userEnteredPhrase.split(" ");
				StringBuilder phraseBuffer = new StringBuilder();

				// System.out.println("user entered phrase: " +
				// userEnteredPhrase);
				for (String tokenBuffer : tokenBuffers) {
					Token token = new Token(tokenBuffer, tokenPosition, null);
					tokenPosition += tokenBuffer.length() + 1;

					token.setChunk("No Chunk");
					token.setPos("No POS");
					ngram.addElement(token);

					phraseBuffer.append(tokenBuffer + " ");
				}

				// System.out.println("ngram: " + ngram);
				Phrase phrase = new Phrase(phraseBuffer.toString(), ngram, phraseIndex);

				// System.out.println("Adding phrase: " +
				// phraseBuffer.toString());
				phrasesToBeMatched.add(phrase);
				if (phraseImportance.containsKey(phraseBuffer.toString().toLowerCase().trim())) {
					phrasesToBeMatchedImp.add(phraseImportance.get(phraseBuffer.toString().toLowerCase().trim()));
					System.out.println("Phrase:" + phraseBuffer.toString() + " Importance:"
							+ phraseImportance.get(phraseBuffer.toString().toLowerCase().trim()));
				} else
					phrasesToBeMatchedImp.add(0);
				SortPhrase sp = new SortPhrase();
				sp.setJobId(posting.getId());
				sp.setLemmatizedPhrase(userEnteredPhrase);
				sp.setOriginalPhrase(userEnteredPhrase);
				sp.setUserName(user.getEmail());
				sp.setUserentered(true);
				sp.setSavedTime(timestamp);
				// Save phrase to database
				try {
					sp.setSelected(true);
					Integer imp = phraseImportance.get(userEnteredPhrase.toLowerCase());
					System.out.println("UPhrase:" + userEnteredPhrase + " Imp:" + imp);
					if (imp != null && imp.intValue() != 0) {
						sp.setStars(imp.doubleValue());
					} else {
						System.out.println("UPhrase:" + userEnteredPhrase + " is required, Imp:" + imp);
						sp.setRequired(true);
					}
					if (tags.contains(userEnteredPhrase))
						sp.setTag(true);
					if (sp.getId() > 0)
						sp.update();
					else
						sp.insert();
				} catch (Exception x) {
					x.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("job_phrases.jsp?error=" + e.getMessage());
		}
	}

	private int getInt(String value) {
		try {
			return new Integer(value).intValue();
		} catch (Exception x) {
			return 0;
		}
	}

}
