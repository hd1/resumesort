/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

import com.textnomics.data.AddedSynonym;
import com.textnomics.data.DeletedPhrase;
import com.textnomics.data.JobPosting;
import com.textnomics.data.UserDeletedPhrase;
import com.textnomics.data.UserTemplate;
import com.textnomics.data.UserTemplatePhrase;
import com.textonomics.DocumentRS;
import com.textonomics.DocumentProcessorRS;
import com.textonomics.KeyPhraseIdentifierPipe;
import com.textonomics.NumericFilter;
import com.textonomics.Phrase;
import com.textonomics.PhraseFilterPipe;
import com.textonomics.PhraseList;
import com.textonomics.PhraseListFilter;
import com.textonomics.PhrasePositionComparator;
import com.textonomics.PlatformProperties;
import com.textonomics.datadriven.WikiPhrase;
import com.textonomics.db.UserDBAccess;
import com.textonomics.nlp.DefaultNLPSuite;
import com.textonomics.nlp.NGram;
import com.textonomics.nlp.Sentence;
import com.textonomics.nlp.Token;
import com.textonomics.openoffice.OODocumentConverter;
import com.textonomics.openoffice.OODocumentRSTagger;
import com.textonomics.openoffice.OOFILTER_TYPE;
import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderPool;
import com.textonomics.wordnet.DomainKeyPhraseIdentifier;
import com.textonomics.wordnet.model.Domain;
import com.textonomics.wordnet.model.POS;
import com.textonomics.wordnet.model.Word;
import com.textonomics.wordnet.model.WordNetDataProviderPool;
import com.textonomics.wordnet.model.WordnetDataProviderException;
import com.textonomics.wordnet.morphy.Morphy;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet to process uploaded job posting and resume zip file
 *
 */
public class ResumeSorter extends HttpServlet {

	static Logger logger = Logger.getLogger(ResumeSorter.class);
	public static final String RESUME_FOLDER = "MyResumes";
	public static final String JOBPOSTING_FOLDER = "MyPostings";
	public static String CONVERTED_PREFIX = "_CONV_";
	public static String COVERLETTER_PREFIX = "CoverLetter_";

	protected class PhraseSorter implements Comparator {

		DocumentRS jobPost;
		Map<String, String> phrasesSynonyms;

		public PhraseSorter(DocumentRS jobPost, Map<String, String> phrasesSynonyms) {
			this.jobPost = jobPost;
			this.phrasesSynonyms = phrasesSynonyms;
		}

		public int compare(Object o1, Object o2) {

			PhraseList pl1 = (PhraseList) o1;
			PhraseList pl2 = (PhraseList) o2;
			Phrase phrase1 = pl1.iterator().next();
			Phrase phrase2 = pl2.iterator().next();
			String phraseBuffer1 = phrase1.getBuffer().trim();
			String phraseBuffer2 = phrase2.getBuffer().trim();

			String synonymList1 = phrasesSynonyms.get(phraseBuffer1);
			String synonymList2 = phrasesSynonyms.get(phraseBuffer2);
			if (synonymList1 == null)
				synonymList1 = "";
			if (synonymList2 == null)
				synonymList2 = "";
			String[] synonyms1 = synonymList1.split("\\|");
			String[] synonyms2 = synonymList2.split("\\|");
			if (synonymList1.length() == 0)
				synonyms1 = new String[0];
			if (synonymList2.length() == 0)
				synonyms2 = new String[0];
			if (synonyms2.length > synonyms1.length)
				return 1;
			else if (synonyms1.length > synonyms2.length)
				return -1;
			if (phrase2.isProperNounPhrase() && (phrase1.isNounPhrase() || phrase1.isVerbPhrase()))
				return 1;
			if (phrase2.isNounPhrase() && phrase1.isVerbPhrase())
				return 1;
			if (phrase1.isProperNounPhrase() && (phrase2.isNounPhrase() || phrase2.isVerbPhrase()))
				return -1;
			if (phrase1.isNounPhrase() && phrase2.isVerbPhrase())
				return -1;

			// Sort first by context count (what a waste of CPU!)
			List<String> p1context = jobPost.getContexts(phraseBuffer1);
			List<String> p2context = jobPost.getContexts(phraseBuffer2);
			int p1count = 1;
			int p2count = 1;
			if (p1context != null)
				p1count = p1context.size();
			if (p2context != null)
				p2count = p2context.size();
			// System.out.println("Phrase 1:" + phraseBuffer1 + ":" + p1count +
			// " Phrase 2:" + phraseBuffer2 + ":" + p2count);
			if (p2count > p1count)
				return 1;
			else if (p1count > p2count)
				return -1;

			// Sort by length then by aphabetical order within the phrases of
			// same length.
			if (phraseBuffer1.split(" ").length > phraseBuffer2.split(" ").length)
				return -1;
			else if (phraseBuffer1.split(" ").length < phraseBuffer2.split(" ").length)
				return 1;
			else
				return phraseBuffer1.compareToIgnoreCase(phraseBuffer2);

		}
	}

	String tempDir;

	static final SimpleDateFormat TIMESTAMP = new SimpleDateFormat("yyyyMMddHHmmss");

	// WordNetPhraseFilter wordnetPhraseFilter = new WordNetPhraseFilter();
	KeyPhraseIdentifierPipe keyTokenIdentifier = new KeyPhraseIdentifierPipe();
	PhraseFilterPipe sourceTokenFilterer = new PhraseFilterPipe();
	DomainKeyPhraseIdentifier defaultKeyTokenIdentifier;
	// CompoundPhraseIdentifier compoundTokenIdentifier;
	// POSKeyPhraseIdentifier posKeyTokenIdentifier;
	HashMap<String, PhraseListFilter> tokenListFilterMap;

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */

	@Override
	public void init() throws ServletException {
		super.init();
		tempDir = System.getProperty("java.io.tmpdir");
		System.out.println("################################");
		System.out.println(tempDir);
		System.out.println("################################");

		try {
			WordNetDataProviderPool.getInstance().acquireProvider();

			defaultKeyTokenIdentifier = new DomainKeyPhraseIdentifier(Domain.getDomainByName("Generic"));
			// compoundTokenIdentifier = new CompoundPhraseIdentifier();
			// posKeyTokenIdentifier = new POSKeyPhraseIdentifier();
			// wordnetPhraseFilter = new WordNetPhraseFilter();

			tokenListFilterMap = new HashMap<String, PhraseListFilter>();
			tokenListFilterMap.put("Default 100",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt"), 100));
			tokenListFilterMap.put("Default 300",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt"), 300));
			tokenListFilterMap.put("Default 600",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt"), 600));
			tokenListFilterMap.put("Default All",
					new PhraseListFilter(PlatformProperties.getInstance().getResourceFile("1000WordList.txt")));
		} catch (WordnetDataProviderException e) {
			throw new ServletException(e);
		} finally {
			WordNetDataProviderPool.getInstance().releaseProvider();// Always
																	// release
																	// the
																	// provider
		}

	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", -1);

		boolean assignResumes = false;
		FileItem sourceItem = null;
		FileItem targetItem = null;
		String sourceText = null;
		DocumentRS jobPost;
		Map<String, String> phrasesSynonyms = new HashMap<String, String>();
		try {
			WordNetDataProviderPool.getInstance().acquireProvider();
			UserDataProvider userDataProvider = UserDataProviderPool.getInstance().acquireProvider();

			HttpSession session = request.getSession();
			User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			PlatformProperties.getInstance().loggedInUser.set(user); // Set
																		// currrent
																		// user

			// CLear session; user may have used back button
			session.removeAttribute(SESSION_ATTRIBUTE.SUGGESTER.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.TARGET_FILE.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.RESUME_SCORES.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_DOCRESUMES.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.HIGHLIGHTED_HTMLRESUMES.toString());
			// session.removeAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString());
			session.removeAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString());

			// Apache upload classes
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);

			String requestStamp = "" + System.currentTimeMillis();
			// directory based on username
			String uploadDir = getUploadDir(user);

			// Getting the uploaded Job Post

			File sourceFile = null; // Job Post

			String email = user.getEmail();
			String selectedPosting = request.getParameter("selectedPosting");
			String selectedResume = request.getParameter("selectedResume");
			String location = "";
			StringBuffer userIndustry = new StringBuffer();
			String postingId = request.getParameter("JobPostingId");
			String jobtitle = null;
			boolean createMailbox = false;
			boolean postingIdEntered = true;
			String templateName = null;
			// Check for form fields from the browser
			String type = request.getContentType();
			if (type != null && type.startsWith("multipart/form-data")) {
				List<FileItem> items = upload.parseRequest(request);

				for (FileItem fi : items) {
					if (fi.getFieldName().equals("sourcefile")) {
						sourceItem = fi;
						continue;
					}

					if (fi.getFieldName().equals("sourcetext")) {
						sourceText = fi.getString();
						continue;
					}
					// Resume zip file
					if (fi.getFieldName().equals("targetfile")) {

						targetItem = fi;
					}
					if (fi.getFieldName().equals("selectedPosting")) {
						selectedPosting = fi.getString();
					}
					if (fi.getFieldName().equals("selectedResume")) {
						selectedResume = fi.getString();
					}
					if (fi.getFieldName().equals("industry")) {
						userIndustry.append(fi.getString());
						userIndustry.append("\n");
						continue;
					}
					if (fi.getFieldName().equals("email")) {
						email = fi.getString();
						if (email.trim().length() == 0)
							email = user.getEmail();
						continue;
					}
					if (fi.getFieldName().equals("JobPostingId")) {
						postingId = fi.getString();
						continue;
					}
					if (fi.getFieldName().equals("JobTitle")) {
						jobtitle = fi.getString();
						continue;
					}
					if (fi.getFieldName().equals("JobLocation")) {
						location = fi.getString();
						continue;
					}

					if (fi.getFieldName().equals("Assign")) {
						if (fi.getString().equalsIgnoreCase("true"))
							assignResumes = true;
						continue;
					}
					if (fi.getFieldName().equals("Mailbox")) {
						createMailbox = true;
						continue;
					}
					if (fi.getFieldName().equals("selectedTemplate")) {
						templateName = fi.getString();
						continue;
					}
				}
			}

			// Default Job Posting Id - using username or company and current
			// timestamp
			if (postingId == null || postingId.trim().length() == 0) {
				postingIdEntered = false;
				postingId = user.getCompany();
				if (postingId == null || postingId.trim().length() == 0)
					postingId = user.getName();
				if (postingId.indexOf(" ") != -1)
					postingId = postingId.substring(0, postingId.indexOf(" "));
				if (postingId.length() > 4)
					postingId = postingId.substring(0, 4);
				postingId = postingId.trim().toUpperCase() + TIMESTAMP.format(new Date());
			}

			// Following code stores the resumes in user's directory on the
			// server.
			File resumes = null;
			if (selectedResume == null || selectedResume.equals("") || selectedResume.equals("---")) {
				// Get the uploaded zip file -- for multiple file upload has a
				// separate class
				if (targetItem != null && !targetItem.getName().equals("")) {
					resumes = PlatformProperties.getInstance()
							.getResourceFile(uploadDir + File.separator + RESUME_FOLDER + File.separator);

					if (!resumes.isDirectory())
						resumes.mkdir();

					resumes = PlatformProperties.getInstance().getResourceFile(uploadDir + File.separator
							+ RESUME_FOLDER + File.separator + FileUtils.removePathInfo(targetItem.getName()));

					if (targetItem != null)
						targetItem.write(resumes);
					else
						resumes = null;
				}
			} else if (selectedResume != null && selectedResume.length() > 0)// He
																				// is
																				// using
																				// a
																				// previously
																				// submitted
																				// resume
																				// file
			{
				// resumes = PlatformProperties.getInstance().getResourceFile(
				// uploadDir + File.separator + requestStamp + "_" +
				// FileUtils.removePathInfo(selectedResume));
				//
				File resume = PlatformProperties.getInstance()
						.getResourceFile(uploadDir + File.separator + RESUME_FOLDER + File.separator + selectedResume);
				if (resume.exists()) {
					resumes = resume;
					System.out.println("----------------resumes:" + resumes);
				}
				// copy(resume, resumes);//Create a new file with the expected
				// time stamp
			}
			session.setAttribute(SESSION_ATTRIBUTE.TARGET_FILE.toString(), resumes);
			// System.out.println(resumes.getPath());

			// Uploading the Job Post
			String originalFile = null;
			if (selectedPosting == null || selectedPosting.equals("") || selectedPosting.equals("null")
					|| selectedPosting.equals("---")) {
				if (sourceItem != null && !sourceItem.getName().equals("")) // upload
																			// jobposting
				{

					sourceFile = PlatformProperties.getInstance().getResourceFile(
							uploadDir + File.separator + ResumeSorter.JOBPOSTING_FOLDER + File.separator);

					if (!sourceFile.isDirectory())
						sourceFile.mkdir();
					String ext = ".txt";
					originalFile = sourceItem.getName();
					if (sourceItem.getName().lastIndexOf(".") != -1)
						ext = sourceItem.getName().substring(sourceItem.getName().lastIndexOf("."));
					if (postingIdEntered)
						sourceFile = PlatformProperties.getInstance().getResourceFile(uploadDir + File.separator
								+ ResumeSorter.JOBPOSTING_FOLDER + File.separator + postingId + ext);
					else
						sourceFile = PlatformProperties.getInstance().getResourceFile(uploadDir + File.separator
								+ ResumeSorter.JOBPOSTING_FOLDER + File.separator + sourceItem.getName());

					sourceItem.write(sourceFile);
				} else if (sourceText != null && sourceText.trim().length() > 0)// for
																				// cut
																				// and
																				// paste
																				// job
																				// posting
				{
					sourceFile = PlatformProperties.getInstance().getResourceFile(
							uploadDir + ResumeSorter.JOBPOSTING_FOLDER + File.separator + postingId + ".txt");
					OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(sourceFile),
							Charset.forName("ISO-8859-1"));
					w.write(sourceText);
					w.close();
				}
			} else// He is using a previously submitted posting
			{
				List<JobPosting> postings = new JobPosting()
						.getObjects("original_file =" + UserDBAccess.toSQL(FileUtils.removePathInfo(selectedPosting))
								+ " and user_name=" + UserDBAccess.toSQL(user.getLogin()));
				// sourceFile =
				// PlatformProperties.getInstance().getResourceFile(
				// uploadDir + File.separator + requestStamp + "_" +
				// FileUtils.removePathInfo(selectedPosting));
				if (!postingIdEntered) {
					postingId = FileUtils.removePathInfo(selectedPosting);
					if (postingId.lastIndexOf(".") != -1)
						postingId = postingId.substring(0, postingId.lastIndexOf("."));
				}
				// if (postings.size() > 0)
				// postingId = postings.get(0).getPostingId();
				sourceFile = PlatformProperties.getInstance().getResourceFile(
						uploadDir + File.separator + ResumeSorter.JOBPOSTING_FOLDER + File.separator + selectedPosting);

				// copy(posting, sourceFile);//Create a new file with the
				// expected time stamp
			}
			// Put job posting into database
			File resumeFolder = (File) session.getAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString());
			List<JobPosting> postings = new JobPosting().getObjects("POSTING_ID =" + UserDBAccess.toSQL(postingId)
					+ " and user_name=" + UserDBAccess.toSQL(user.getLogin()));
			JobPosting posting = null;
			if (postings.size() == 0) // if posting does not exist
			{
				posting = new JobPosting();
				posting.setPostingId(postingId);
				posting.setJobTitle(jobtitle);
				posting.setLocation(location);
				if (sourceFile != null)
					posting.setPostingFile(FileUtils.removePathInfo(sourceFile.getName()));
				if (resumeFolder != null)
					posting.setResumeFolder(FileUtils.removePathInfo(resumeFolder.getName()));
				posting.setUserName(user.getLogin());
				posting.setOriginalFile(originalFile);
				posting.setIndustry(userIndustry.toString());
				posting.setPostingDate(new Date());
				posting.setTemplateName(templateName);
				if (createMailbox)
					posting.setEmailAddress(postingId.toLowerCase().replace(' ', '_'));
				posting.insert();
			} else // job posting exists update database
			{
				posting = postings.get(0);
				if (jobtitle != null)
					posting.setJobTitle(jobtitle);
				if (location != null && location.length() > 0)
					posting.setLocation(location);
				if (sourceFile != null)
					posting.setPostingFile(FileUtils.removePathInfo(sourceFile.getName()));
				if (originalFile != null)
					posting.setOriginalFile(originalFile);
				if (userIndustry.length() > 0)
					posting.setIndustry(userIndustry.toString());
				if (posting.getResumeFolder() == null && resumeFolder != null)
					posting.setResumeFolder(FileUtils.removePathInfo(resumeFolder.getName()));
				if (createMailbox)
					posting.setEmailAddress(postingId.replace(' ', '_'));
				if (templateName != null)
					posting.setTemplateName(templateName);
				else
					templateName = posting.getTemplateName();
				posting.update();
			}

			session.setAttribute(SESSION_ATTRIBUTE.JOB_POSTING_ID.toString(), postingId);
			session.setAttribute(SESSION_ATTRIBUTE.JOB_POSTING.toString(), posting);

			// Adding filters
			keyTokenIdentifier.addKeyTermIdentifer(defaultKeyTokenIdentifier); // ??

			sourceTokenFilterer.addTokenFilter(tokenListFilterMap.get("Default 300")); // Checks
																						// if
																						// the
																						// word
																						// is
																						// in
																						// the
																						// top
																						// 300
																						// words
																						// list.
			// sourceTokenFilterer.addTokenFilter(new StyleFilter());
			sourceTokenFilterer.addTokenFilter(new NumericFilter());

			// Extracting Phrases
			jobPost = null;
			DocumentProcessorRS docProc = new DocumentProcessorRS(new DefaultNLPSuite(), keyTokenIdentifier,
					sourceTokenFilterer, 8);
			Collection<PhraseList> sortedPhrases = new LinkedHashSet<PhraseList>();
			Collection<PhraseList> nonLemmaPhrases = new ArrayList<PhraseList>();
			Collection<PhraseList> lemmaPhrases = new ArrayList<PhraseList>();
			//
			// SourceFile maybe null if user has selected a Phrase Template
			if (sourceFile != null) {
				File serializedJP = new File(sourceFile.getPath() + ".Object");
				if (serializedJP.exists()) {
					System.out.println("Reading:" + sourceFile.getPath() + ".Object");
					jobPost = (DocumentRS) FileUtils.deSerializeOject(new File(sourceFile.getPath() + ".Object"));
				}

				if (jobPost == null) {
					jobPost = docProc.process(sourceFile, false, true);
					if (jobPost == null) {
						// If document is html already, give an error. Otherwise
						// convert to html and try processing again
						if (sourceFile.getName().toLowerCase().indexOf(".html") != -1)
							throw new Exception("Unable to Process Submitted Job Posting.");
						// Try processing as html
						sourceFile = new OODocumentConverter().convertToHtml(sourceFile);
						jobPost = docProc.process(sourceFile, false, true);
					}
					FileUtils.serializeOject(jobPost, new File(sourceFile.getPath() + ".Object"));
				}

				nonLemmaPhrases = jobPost.getDocumentPhrases();

				lemmaPhrases = getLemmatizedPhrases(nonLemmaPhrases, jobPost); // Get
																				// lemmatized
																				// phrases

				// Sorting phrases:
				// 1. Proper Nouns appear first in the list of phrases being
				// displayed.
				// 2. Followed by Nouns phrases and then by Verb Phrases
				// 3. The phrases are sorted again within the group by the
				// number of occurrence in the Job posting
				// and then by the order of length first
				// and then alphabetical order using the PhaseSorter internal
				// comparator.

				// int i=0;
				List<UserDeletedPhrase> udps = new UserDeletedPhrase()
						.getObjects("USER_NAME =" + UserDBAccess.toSQL(user.getLogin()));
				Set<String> userDeletedPhrases = new HashSet<String>();
				for (int i = 0; i < udps.size(); i++) {
					userDeletedPhrases.add(udps.get(i).getPhrase().toLowerCase());
				}

				List<PhraseList> properNouns = new ArrayList<PhraseList>();
				List<PhraseList> verbPhrases = new ArrayList<PhraseList>();
				List<PhraseList> nounPhrases = new ArrayList<PhraseList>();

				for (PhraseList phraseList : lemmaPhrases) {
					Phrase phrase = phraseList.iterator().next().getPhrase();
					String text = phrase.getBuffer().toLowerCase().trim();
					if (DeletedPhrase.isDeleted(text))
						continue;
					if (!user.isAdmin() && userDeletedPhrases.contains(text))
						continue;
					// String chunkTag = phrase.getChunkTag();
					String pos[] = phrase.getNgram().getPosSequence().split(" ");

					if (pos[0].startsWith("NNP")) // Extract Proper Nouns
					{
						properNouns.add(phraseList);
						// System.out.println("Chunk Tag: " + chunkTag + " for
						// phrase: " + phrase.getBuffer());
					} else if (pos[0].startsWith("NN") && pos.length > 1) // Extract
																			// Noun
																			// Phrases
					{
						nounPhrases.add(phraseList);
						// System.out.println("Chunk Tag: " + chunkTag + " for
						// phrase: " + phrase.getBuffer());
					} else // Extract the remaining
					{
						verbPhrases.add(phraseList);
						// System.out.println("Chunk Tag: " + chunkTag + " for
						// phrase: " + phrase.getBuffer());
					}
					// i++;
				}

				if (!"true"
						.equals(PlatformProperties.getInstance().getUserProperty("com.textonomics.use_wikisynonyms"))) {
					for (PhraseList phraseList : lemmaPhrases) {
						try {
							Phrase phrase = phraseList.iterator().next().getPhrase();
							String synonyms = getWikiSynonyms(user, phrase.getBuffer());
							phrasesSynonyms.put(phrase.getBuffer().trim(), synonyms);
						} catch (Exception x) {
							x.printStackTrace();
						}
					}
				}
				// System.out.println("Synonyms:" + phrasesSynonyms);

				// Collections.sort(properNouns, new PhraseSorter());
				// Collections.sort(nounPhrases, new PhraseSorter());
				// Collections.sort(verbPhrases, new PhraseSorter());

				List<PhraseList> tempPhrases = new ArrayList<PhraseList>();
				tempPhrases.addAll(properNouns);
				tempPhrases.addAll(nounPhrases);
				tempPhrases.addAll(verbPhrases);
				Collections.sort(tempPhrases, new PhraseSorter(jobPost, phrasesSynonyms));

				sortedPhrases.addAll(tempPhrases);
				List<PhraseList> removeList = new ArrayList<PhraseList>();
				for (PhraseList pl : sortedPhrases) {
					String phrase = pl.getFirst().getBuffer();
					if (Character.isUpperCase(phrase.charAt(0))) {
						String lower = phrase.toLowerCase();
						for (PhraseList pl2 : sortedPhrases) {
							String phrase2 = pl2.getFirst().getBuffer();
							if (phrase2.equals(lower))
								removeList.add(pl);
						}
					}
				}
				for (PhraseList pl : removeList) {
					sortedPhrases.remove(pl);
				}
				/*
				 * for(PhraseList p: sortedPhrases) {
				 * System.out.println("Phrase: " +
				 * p.iterator().next().getBuffer() + " Proper Noun:" +
				 * p.iterator().next().isProperNounPhrase());
				 * System.out.println("POS: " +
				 * p.iterator().next().getNgram().getPosSequence()); }
				 */

				// Sorting Phrases end

				System.out.println("Number of Job Post Phrases: " + jobPost.getDocumentPhrases().size());
				session.setAttribute(SESSION_ATTRIBUTE.SOURCE_DOCUMENT.toString(), jobPost);
			} else if (posting.getTemplateName() == null || posting.getTemplateName().length() == 0) {
				request.getRequestDispatcher("/upload_jobposting.jsp?error=Invalid Job Posting").forward(request,
						response); // Missing Job Posting
				return;
			}
			if (posting.getTemplateName() != null && posting.getTemplateName().length() > 0) {
				UserTemplate template = (UserTemplate) new UserTemplate().getObject("user_name="
						+ UserDBAccess.toSQL(user.getLogin()) + " and name =" + UserDBAccess.toSQL(templateName));
				List<UserTemplatePhrase> templatePhrases = new UserTemplatePhrase()
						.getObjects("template_id=" + template.getId() + " order by phrase");
				session.setAttribute(SESSION_ATTRIBUTE.TEMPLATE_PHRASES.toString(), templatePhrases);
				for (UserTemplatePhrase templatePhrase : templatePhrases) {
					try {
						String synonyms = getWikiSynonyms(user, templatePhrase.getPhrase());
						phrasesSynonyms.put(templatePhrase.getPhrase(), synonyms);
					} catch (Exception x) {
						x.printStackTrace();
					}
				}
			}
			session.setAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_SYNONYMS.toString(), phrasesSynonyms);
			session.setAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES_DELETED_SYNONYMS.toString(),
					new HashMap<String, Set<String>>());
			session.setAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString(), sortedPhrases);
			session.setAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString(), lemmaPhrases);
			session.setAttribute(SESSION_ATTRIBUTE.UNLEMMA_JOBPOSTING_PHRASES.toString(), nonLemmaPhrases);
			session.setAttribute(SESSION_ATTRIBUTE.UPLOAD_DIR.toString(), uploadDir);
			session.setAttribute(SESSION_ATTRIBUTE.EMAIL.toString(), email);
			session.setAttribute(SESSION_ATTRIBUTE.KEY_PHRASE_IDENTIFIER.toString(), keyTokenIdentifier);
			session.setAttribute(SESSION_ATTRIBUTE.DEFAULT_PHRASE_FILTER.toString(), sourceTokenFilterer);
			// Map<String,Integer> resumePhrases = new
			// TreeMap<String,Integer>();
			// session.setAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString(),
			// resumePhrases);

			// If this is a rerun, the folder is already fixed
			String folder = "";
			if ("rerun".equals(request.getParameter("command")) && posting.getResumeFolder() != null
					&& posting.getResumeFolder().length() > 0) {
				int runs = posting.getNumRuns();
				posting.setNumRuns(runs - 1);
				if (posting.getNumRuns() < 0)
					posting.setNumRuns(0);
				folder = "resumeFolder=" + posting.getResumeFolder();
			}
			request.getRequestDispatcher("/upload_resumes.jsp?" + folder).forward(request, response); // Upload
																										// resumes
			try {
				String prevIndustry = user.getIndustry();
				String newIndustry = userIndustry.toString();
				if (prevIndustry != null) {
					String[] industries = prevIndustry.split("\n");
					for (String industry : industries)
						if (industry.length() > 0 && !newIndustry.contains(industry))
							newIndustry = newIndustry + industry + "\n";
				}
				user.setIndustry(newIndustry);
				userDataProvider.updateUserIndustry(user);
				userDataProvider.commit();
			} catch (Exception x) {
				x.printStackTrace();
			}

			ZipfileProcessingThread zipThread = null;
			logger.info("resumes:" + resumes);
			if (resumes != null) {
				session.removeAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
				zipThread = new ZipfileProcessingThread(session, resumes, assignResumes);
			} else {
				resumes = (File) session.getAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString());
				logger.info("Resume folder:" + resumes);
				// Resumes in a folder are now processed before coming here
				// if (resumes != null)
				// {
				// session.removeAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString());
				// zipThread = new ZipfileProcessingThread(session, resumes,
				// assignResumes);
				// }
				// else
				// {
				// List<File> resumeFiles =
				// (List<File>)session.getAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString());
				// //if (resumeFiles == null)
				// // throw new Exception("No resumes uploaded");
				// if (resumeFiles != null)
				// zipThread = new ZipfileProcessingThread(session, resumeFiles,
				// assignResumes);
				// }
			}
			if (zipThread != null)
				zipThread.start();
			if (false && assignResumes) {
				if (zipThread != null)
					zipThread.join();
				request.getRequestDispatcher("/ranked_resumes.jsp").forward(request, response);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
			throw new ServletException(ex);
		} finally {

			WordNetDataProviderPool.getInstance().releaseProvider();
			UserDataProviderPool.getInstance().releaseProvider();
		}
	}

	// Method to get the Upload Directory of the user

	public static String getUploadDir(User user) throws IOException {
		String path = "uploads" + File.separator + user.getLogin() + File.separator;
		System.out.println("User Login: " + user.getLogin());
		File dir = PlatformProperties.getInstance().getResourceFile(path);

		if (!dir.exists())
			dir.mkdirs();

		return path;
	}

	public static String getUploadDir(String userName) throws IOException {
		String path = "uploads" + File.separator + userName + File.separator;
		File dir = PlatformProperties.getInstance().getResourceFile(path);

		if (!dir.exists())
			dir.mkdirs();

		return path;
	}

	// VINOD: 1. The below code lematizes the phrase and add it to the list of
	// lemmatized phrases.
	// 2. Each non-lemmatized phrase is divided into tokens and each token is
	// lemmatized if necessary. Then, each lemmatized token
	// is added to the ngram.
	// 2.1 Note that the "buffer" or text of the phrase is constructed from the
	// original buffers of the tokens except the last token (or word?)
	// lemmatized. (As requested by EB).

	public static Collection<PhraseList> getLemmatizedPhrases(Collection<PhraseList> nonLemmaPhrases,
			DocumentRS jobPost) {
		Collection<PhraseList> lemmaPhrases = new LinkedHashSet<PhraseList>(); // Stores
																				// the
																				// lemmatized
																				// phrases.
		Set<String> uniqueLemmaPhrases = new HashSet<String>();
		List<PhraseList> removePhrases = new ArrayList<PhraseList>();
		for (PhraseList nonLemmaPhraseList : nonLemmaPhrases) {
			Phrase lemmaPhrase = getLemmatizedPhrase(nonLemmaPhraseList);
			if (uniqueLemmaPhrases.contains(lemmaPhrase.getBuffer())) {
				removePhrases.add(nonLemmaPhraseList);
				continue;
			}
			// Commenting out the remove phrase logic - does not seem to work
			// (Dev).
			// uniqueLemmaPhrases.add(lemmaPhrase.getBuffer());
			PhraseList lemmaPhraseList = new PhraseList(jobPost);
			lemmaPhraseList.add(lemmaPhrase);
			lemmaPhrases.add(lemmaPhraseList); // Add the lemmatized phrase to
												// the lemmatized phrases list.
		}
		for (PhraseList removePhrase : removePhrases) {
			// Remove does not seem to work - size before is always same as size
			// afterwards
			System.out.println("Removing nolemma phrase: " + removePhrase.getFirst().getBuffer() + " size:"
					+ nonLemmaPhrases.size());
			nonLemmaPhrases.remove(removePhrase);
			System.out.println("Remaining: " + nonLemmaPhrases.size());
		}
		return lemmaPhrases;
	}

	public static Phrase getLemmatizedPhrase(PhraseList nonLemmaPhraseList) {
		Phrase nonLemmaPhrase = nonLemmaPhraseList.iterator().next().getPhrase(); // Get
																					// non-lemmatized
																					// phrase.
		try {
			int posIndex = 0;
			int tokenIndex = 0;

			NGram nonLemmaNGram = nonLemmaPhrase.getNgram(); // Get the ngram of
																// the
																// non-lemmatized
																// phrase.
			int sentenceIndex = nonLemmaPhrase.getSentence(); // Get the
																// sentenceIndex
																// of the
																// non-lematized
																// phrase.

			StringBuilder lemmaPhraseBuffer = new StringBuilder(); // Stores the
																	// buffer of
																	// the
																	// lemmatized
																	// phrase
																	// buffer.
			NGram lemmaNGram = new NGram();

			List<Token> tokenList = nonLemmaNGram.getElements(); // Get the list
																	// of tokens
																	// of the
																	// non-lemmatized
																	// phrase
																	// ngram.
			String pos[] = nonLemmaPhrase.getNgram().getPosSequence().split(" "); // POS
																					// tags
																					// of
																					// tokens

			for (Token token : tokenList) {
				if (tokenIndex < tokenList.size()) // If tokenIndex did not
													// reach the end
					tokenIndex++; // Proceed
				else
					break;

				int tokenPosition = token.getPosition();

				Set<Word> lemmas = new HashSet<Word>();
				if (pos[posIndex].startsWith("VB") || pos[posIndex].startsWith("NN")) // Check
																						// if
																						// the
																						// token
																						// needs
																						// to
																						// be
																						// lemmatized
				{
					if (pos[posIndex].startsWith("VB") && !pos[posIndex].equalsIgnoreCase("VB")) {
						lemmas = Morphy.getInstance().getStems(token.getBuffer(), POS.VERB);
					}

					if (pos[posIndex].equalsIgnoreCase("NNS") || pos[posIndex].equalsIgnoreCase("NNPS")) {
						lemmas = Morphy.getInstance().getStems(token.getBuffer(), POS.NOUN);
					}

					if (pos[posIndex].equalsIgnoreCase("NNP")
							&& !token.getBuffer().equals(token.getBuffer().toUpperCase())) {
						lemmas = Morphy.getInstance().getStems(token.getBuffer(), POS.NOUN);
					}
				}

				if (lemmas != null && !lemmas.isEmpty()) // If the token is
															// lemmatized
				{
					String lemmaTokenBuffer = lemmas.iterator().next().toString().trim();
					if (Character.isUpperCase(token.getBuffer().charAt(0)))
						lemmaTokenBuffer = lemmaTokenBuffer.substring(0, 1).toUpperCase()
								+ lemmaTokenBuffer.substring(1);
					Token lemmaToken = new Token(lemmaTokenBuffer, tokenPosition, null);

					lemmaToken.setPos(token.getPos());
					lemmaToken.setChunk(token.getChunk());

					// NGram contains the lemmatized token irrespective of token
					// position.
					lemmaNGram.addElement(lemmaToken);

					// The phrase buffer have only the last word in it
					// lemmatized.
					if (tokenIndex == tokenList.size()) // If last token
						lemmaPhraseBuffer.append(lemmaTokenBuffer + " "); // Add
																			// the
																			// lemmatized
																			// token
																			// buffer
					else
						lemmaPhraseBuffer.append(token.getBuffer() + " ");
				} else {
					lemmaPhraseBuffer.append(token.getBuffer() + " "); // Else
																		// add
																		// the
																		// original
																		// buffer
																		// as it
																		// is.
					lemmaNGram.addElement(token);
				}

				posIndex++; // next token POS

			}

			Phrase lemmaPhrase = new Phrase(lemmaPhraseBuffer.toString(), lemmaNGram, sentenceIndex); // Basically,
																										// create
																										// a
																										// phrase
																										// with
																										// lemmatized
																										// buffer,
																										// lemmatized
																										// ngram
																										// and
																										// sentenceIndex.
			return lemmaPhrase;
		} catch (Exception x) {
			System.out.println("Error lemmatizing:" + nonLemmaPhrase.getBuffer() + " exp:" + x);
			x.printStackTrace();
			return nonLemmaPhrase;
		}
	}

	public static List<String> getHighlightedSentences(DocumentRS source, Collection<PhraseList> unlemmaPhrases) {
		List<Sentence> sentences = source.getSentences();
		List<String> texts = new ArrayList<String>();
		for (Sentence s : sentences) {
			texts.add(s.toString());
		}

		return texts;
	}

	public static File createTaggedPosting(HttpServletRequest request, User user, String postingFileName,
			Collection<PhraseList> unlemmaPhrases) throws Exception {
		HttpSession session = request.getSession();
		Collection<PhraseList> jobPhraseList = (Collection<PhraseList>) session
				.getAttribute(SESSION_ATTRIBUTE.JOBPOSTING_PHRASES.toString());
		Collection<PhraseList> lemmaPhrases = (Collection<PhraseList>) session
				.getAttribute(SESSION_ATTRIBUTE.LEMMA_JOBPOSTING_PHRASES.toString());
		List<UserDeletedPhrase> udps = new UserDeletedPhrase()
				.getObjects("USER_NAME =" + UserDBAccess.toSQL(user.getLogin()));
		Set<String> userDeletedPhrases = new HashSet<String>();
		for (int i = 0; i < udps.size(); i++) {
			userDeletedPhrases.add(udps.get(i).getPhrase().toLowerCase());
		}
		File jobpostingFile = PlatformProperties.getInstance().getResourceFile(
				ResumeSorter.getUploadDir(user) + "/" + ResumeSorter.JOBPOSTING_FOLDER + "/" + postingFileName);
		Set<Phrase> phraseSet = new TreeSet<Phrase>(new PhrasePositionComparator());
		Iterator<PhraseList> lemmaItr = lemmaPhrases.iterator();
		for (PhraseList phraseList : unlemmaPhrases) {
			if (lemmaItr.hasNext()) {
				PhraseList lemmaPhrase = lemmaItr.next();
				if (!jobPhraseList.contains(lemmaPhrase))
					continue;
			}
			List<Phrase> phrases = phraseList.getAll();
			String text = phrases.get(0).getBuffer();
			if (text.toUpperCase().equals(text) && text.length() > 4)
				continue;
			text = text.toLowerCase().trim();
			if (DeletedPhrase.isDeleted(text))
				continue;
			if (!user.isAdmin() && userDeletedPhrases.contains(text))
				continue;
			// System.out.println("Adding phrase:" + text);
			phraseSet.addAll(phrases);
		}
		File highlightedTemp = new File(request.getRealPath("temp") + "/" + "HIGHLIGHTED_" + postingFileName + ".temp");
		File highlightedHtml = new File(request.getRealPath("temp") + "/" + "HIGHLIGHTED_" + postingFileName + ".html");
		OODocumentRSTagger tagger = new OODocumentRSTagger(jobpostingFile, phraseSet);
		tagger.process(highlightedTemp, OOFILTER_TYPE.HTML);
		replaceTags(highlightedTemp, highlightedHtml);
		highlightedTemp.delete();
		return highlightedHtml;
	}

	private static void replaceTags(File inputHtml, File outputHtml) throws Exception {
		BufferedReader in = new BufferedReader(new FileReader(inputHtml));
		BufferedWriter out = new BufferedWriter(new FileWriter(outputHtml));
		String line;
		try {
			boolean checkpre = true;
			while ((line = in.readLine()) != null) {
				// Temporarily change to balck color, until we fix some bug
				line = line.replace("{j}", "<FONT color=black>").replace("{/j}", "</FONT>");
				if (line.indexOf("<PRE") != -1 && checkpre) {
					checkpre = false;
					line = line.replace("<PRE CLASS=\"western\">", "<div style='width:600px;'>");
				}
				if (!checkpre && line.indexOf("</PRE>") != -1) {
					checkpre = true;
					line = line.replace("</PRE>", "</div>");
				}
				if (!checkpre)
					out.write(line + "<br>\n");
				else
					out.write(line + "\n");

			}
		} finally {
			in.close();
			out.close();
		}
	}

	/**
	 * Copies the the src file to dst file
	 *
	 * @param src
	 * @param dst
	 * @throws IOException
	 */
	protected void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	public static String getWikiSynonyms(User user, String phrase) throws Exception {
		List<WikiPhrase> wikiPhrases = null;
		if (!"true".equals(PlatformProperties.getInstance().getUserProperty("com.textonomics.wikiphrases_birectional")))
			wikiPhrases = new WikiPhrase()
					.getObjects("wtext = " + WikiPhrase.toSQL(phrase.toLowerCase().trim()) + " and mainphrase = true");
		else
			wikiPhrases = new WikiPhrase().getObjects("wtext = " + WikiPhrase.toSQL(phrase.toLowerCase().trim()));
		String synonyms = "";
		Set<String> uniqueSyn = new TreeSet<String>();
		if (wikiPhrases.size() > 0) {
			for (WikiPhrase wikiTitle : wikiPhrases) {
				wikiPhrases = new WikiPhrase()
						.getObjects("code = " + wikiTitle.getCode() + " and id !=" + wikiTitle.getId());
				for (WikiPhrase wp : wikiPhrases) {
					if (wp.getText().trim().length() == 0)
						continue;
					// int count = new UserDBAccess().getCount(new
					// RejectedSynonym().getDB_TABLE(),
					// "phrase =" + UserDBAccess.toSQL(phrase.trim())
					// + " and synonym=" + UserDBAccess.toSQL(wp.getText())
					// + " and user_name=" +
					// UserDBAccess.toSQL(user.getLogin()));
					// if (count == 0)
					// This is now checked later and show to user as deleted
					// if (!RejectedSynonym.isRejected(user.getLogin(),
					// phrase.trim(), wp.getText()))
					{
						if (uniqueSyn.contains(wp.getText()))
							continue;
						uniqueSyn.add(wp.getText());
					}
				}
			}
		}

		List<AddedSynonym> addedSyns = new AddedSynonym()
				.getObjects("phrase like " + AddedSynonym.toSQL(phrase.toLowerCase().trim()) + " and (user_name = "
						+ AddedSynonym.toSQL(user.getLogin()) + " or user_name='system')");
		for (AddedSynonym syn : addedSyns) {
			// This is now checked later and show to user as deleted
			// if (!RejectedSynonym.isRejected(user.getLogin(), phrase.trim(),
			// syn.getSynonym()))
			{
				if (uniqueSyn.contains(syn.getSynonym()))
					continue;
				uniqueSyn.add(syn.getSynonym());
			}
		}
		for (String syn : uniqueSyn)
			synonyms = synonyms + "|" + syn;

		if (synonyms.length() > 1)
			synonyms = synonyms.substring(1);
		return synonyms;
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on
	// the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}
