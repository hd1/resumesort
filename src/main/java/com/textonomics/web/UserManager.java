package com.textonomics.web;

import com.textonomics.mail.RegistrationMailer;
import com.textonomics.mail.RegistrationMailerThread;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderException;
import com.textonomics.user.UserDataProviderPool;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This servlet is used to add, delete, update users. Very simple servlet that
 * receives the information from the UserManager.jsp
 *
 * @author Nuno Seco
 *
 */

public class UserManager extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserManager() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User systemUser;
		try {
			UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();

			if (request.getParameter("delete") != null) {
				provider.deleteUser(Integer.parseInt(request.getParameter("iduser")));
			} else {
				if (request.getParameter("iduser") != null && !request.getParameter("iduser").equals("")) {
					systemUser = User.getUser(Integer.parseInt(request.getParameter("iduser")));
					int prevState = systemUser.getState();
					fillUser(systemUser, request);
					provider.updateUser(systemUser);
					provider.updateUserField(systemUser, "freePostings");
					if (systemUser.getState() == 1 && prevState != 1) {
						System.out.println("Sending registration email");
						RegistrationMailer.getInstance().sendMail(systemUser.getEmail(),
								RegistrationMailer.REGISTRATION.ALPHA, systemUser.getEmail(), "");
					}
				} else {
					systemUser = new User();
					fillUser(systemUser, request);
					provider.insertUser(systemUser);
					provider.updateUserField(systemUser, "freePostings");
				}
			}
			provider.commit();

			response.sendRedirect("UserManager.jsp");
		} catch (UserDataProviderException ex) {
			throw new ServletException(ex);
		} finally {
			UserDataProviderPool.getInstance().releaseProvider();// Release
																	// it!!!!!!!!
		}
	}

	private void fillUser(User user, HttpServletRequest request) {
		user.setEmail(request.getParameter("email").trim());
		user.setName(request.getParameter("name").trim());
		user.setLogin(request.getParameter("login").trim());
		user.setPassword(request.getParameter("password").trim());
		user.setState(Integer.parseInt(request.getParameter("state").trim()));
		user.setFreePostings(Integer.parseInt(request.getParameter("freePostings").trim()));
	}

}
