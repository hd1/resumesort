/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.web;

/**
 *
 * @author Preeti Mudda
 */
import com.textonomics.PlatformProperties;
import java.util.logging.Level;
import javax.servlet.http.HttpSession;
import dk.safl.beanstemc.Beanstemc;
import dk.safl.beanstemc.BeanstemcException;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * This class splits the resume files into define chunkSize. It creates a job
 * for each chunkSize. Puts each job onto Beanstalkd. Starts the JobCheckThread
 * with jobID and listOfFiles map
 * 
 * @author Preeti Mudda
 */
public class BeanstalkClient {
	static Logger logger = Logger.getLogger(BeanstalkClient.class);
	public static boolean upFirst = true;
	public static boolean hasJobs = false;
	private int numOfInstances = 1; // number of instances based on no. of
									// files.
	private static Beanstemc beanStem; // Beanstemc is a client that connects to
										// Beanstalkd
	private static String msg_w = "rwork"; // tube name
	private HashMap<String, Object> sessionAttribute = new HashMap<String, Object>(); // stores
																						// session
																						// attributes
																						// into
																						// hashmap
	private Map<Long, List<File>> jobIdResumeMap = new HashMap<Long, List<File>>(); // stores
																					// <jobID
																					// listOfFile>
	private int chunkSize = 5; // number of resumes to pass in a job.
	private int mainServer_port; // main server port number
	private String mainServerID; // main server IP address
	private long priority = 1; // priority given for a job by beanstalkd
	private int delay = 0; // puts the job in delay by 0 seconds
	private int timeToRun = 10000; // the job is given 10000 seconds to be
									// executed by the worker.
	private static BeanstalkClient beanstalkClientInstance = null;

	/**
	 * Sets the Main Server IP and Port address from PlatfromProperties.
	 * 
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	private BeanstalkClient() throws UnknownHostException, IOException, BeanstemcException {
		mainServerID = PlatformProperties.getInstance().getProperty("com.textonomics.mainServer_IP");
		mainServer_port = Integer
				.parseInt(PlatformProperties.getInstance().getProperty("com.textonomics.mainServer_BeanstalkdPort"));
		beanStem = new Beanstemc(mainServerID, mainServer_port);
	}

	/**
	 *
	 * 1. Put the files in an List<File> listFile 2. Splits those files into
	 * chunkSize and creates a job 3. Puts the job on beanstalkd tube: rworker
	 * 4. Puts the <jobID, listOfFiles> in a HashMap 5. Calls the
	 * RemoteJobCheckThread and pass the jobIdResumeMap
	 * 
	 * @param session
	 *            : Http Session for each user
	 * @param resumeDirectory
	 *            : The resume directory where all the resume files resides
	 * @param noPhrases
	 *            : boolean - not required
	 * @throws IOException
	 * @throws BeanstemcException
	 * @throws Exception
	 */
	public void connect(HttpSession session, File resumeDirectory) throws IOException, BeanstemcException, Exception {
		List<File> listFiles = new ArrayList<File>(); // list of resume files
		String name = resumeDirectory.getName();
		String path = resumeDirectory.getParent();
		// 1.0 Checks whether resumeDirectory is a zip
		if (name.toLowerCase().endsWith(".zip")) {// Zip file output directory
			path = path + "/" + name + ".dir";
			File outdir = new File(path);
			if (!outdir.exists())
				outdir.mkdirs();
			session.setAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString(), outdir);
			listFiles.addAll(FileUtils.openZipFile(resumeDirectory, path));
		}
		// 1.1 Checks whether resumeDirectory is a directory
		// 1.1.1 extracts those file and adds in listFiles.
		else if (resumeDirectory.isDirectory()) {
			session.setAttribute(SESSION_ATTRIBUTE.RESUME_DIRECTORY.toString(), resumeDirectory);
			File[] files = resumeDirectory.listFiles();
			for (File file : files) {
				if (!file.getName().endsWith("Object") && !file.getName().startsWith(ResumeSorter.CONVERTED_PREFIX)
						&& !file.getName().startsWith("."))
					listFiles.add(file);
			}
		}
		// 1.2 resumeDirectory is a file and adds it to listFiles.
		else {
			listFiles.add(resumeDirectory);
		}

		// 1.3 Set the numOfInstance depending the number of files to process
		// 1.3.1 if listFiles size > 10 then set according to the formula
		// provided
		if (listFiles.size() > 10) {
			numOfInstances = 2;// To be determined: (listFiles.size()/5)/2;
								// //one job = 5 resume( assuming 1 processor).
								// 1 instancescan handle 2 jobs
		}
		// 1.3.2 Set the numOfInstance to 1
		else {
			numOfInstances = 1;
		}

		// 2.0 add session ID to session hashmap
		sessionAttribute.put("ID", session.getId());

		// 2.0.1 tell beanStem to use rworker tube for putting the jobs
		beanStem.use(msg_w);

		// 2.0.2 create a list files that will be used to put files for each job
		List<File> listOfFiles = new ArrayList<File>(); // list of files for

		// 2.0.3 initialze i = 0
		int i = 0;

		// 2.1 loop over list of files
		while (i < listFiles.size()) {
			logger.debug("Adding a job to tube " + listFiles.get(i).getName());
			// 2.1.1 initialze j = 0 each time i < listFiles.size
			int j = 0;
			// 2.1.2 Loop through till j < chunkSize or i < listFiles.size
			while (j < chunkSize) // chunSize = 5
			{
				// 2.1.3 check whether i is greater than listFiles size
				if (i >= listFiles.size()) {
					break;
				}
				// 2.1.4 Add the file to listOfFiles
				else {
					// 2.1.4.1 add the ith file to listOfFiles
					listOfFiles.add(listFiles.get(i));
					System.out.println("File Name " + i + ":" + listFiles.get(i).getName());
					// 2.1.4.2 increament i and j by one
					i++;
					j++;
				}
			} // while(j < chunkSize) ends
				// 3.0 create a MeesageObject that is sent to Beanstalkd.
				// 3.1 MessageObject has three objects:
				// sessionAttributes = sessionID.
				// listOfFiles = contains atmost 5 resume files
				// noPhrases = false. note: not required could remove it.
			MessageObject zipObj = new MessageObject(sessionAttribute, listOfFiles);

			// 3.2 Create a array byte of the MessageObject by serializing the
			// zipObj
			byte[] msgObj = MessageObject.serialize(zipObj);

			System.out.println("Size of the Message object:" + msgObj.length);

			// 3.3 put the job in the beanstalkd tube rworker.
			// 3.3.1 beanStem returns jobId.
			logger.info("Stats of the tube before put command : " + beanStem.statsTube(msg_w));
			long jobId = beanStem.put(msgObj, priority, delay, timeToRun);

			// 4 put the jobId and listOfFiles in the jobIdResumeMap and this is
			// passes on to RemoteJobCheckThread
			jobIdResumeMap.put(jobId, listOfFiles);

			System.out.println("stats " + beanStem.statsTube(msg_w) + "\n length of files" + listOfFiles.size()
					+ "Job id" + jobId);

			// clear the listOfFiles
			listOfFiles = new ArrayList<File>();
			System.out.println("length of files" + listOfFiles.size());
		} // while(i < listFiles.size()) ends

		// 5.0 create RemoteJobCheckThread by passing two arguments : session
		// -HTTP session, jobIdResumeMap - HashMap<jobIds, listOfFiles>
		RemoteJobCheckThread rJobCheckThread = new RemoteJobCheckThread(session, jobIdResumeMap);

		// 5.1 start the thread.
		rJobCheckThread.start();
	}

	/**
	 * returns the num of instances required to process n jobs.
	 * 
	 * @return n - num of instances required to process n jobs
	 */
	public int getNoOfInstances() {
		return numOfInstances;
	}

	/**
	 * 1. Checks if there are any job in the beanstalkd tube.
	 * 
	 * @return true- if there are any current-jobs-ready else false
	 */
	public static boolean hasJobs() {
		try {
			// 1.0 gets the stats fo the tube.
			HashMap<String, String> stats = beanStem.statsTube(msg_w);
			logger.info("The statsTube has : " + stats.size());
			// 1.1 Checks if there are any job in the beanstalkd tube then sets
			// hasJobs ture
			if (Integer.parseInt(stats.get("current-jobs-ready")) > 0)
				hasJobs = true;
			// 1.2 sets hasJobs false.
			else
				hasJobs = false;
		} catch (IOException ex) {
			java.util.logging.Logger.getLogger(BeanstalkClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (BeanstemcException ex) {
			java.util.logging.Logger.getLogger(BeanstalkClient.class.getName()).log(Level.SEVERE, null, ex);
		}
		return hasJobs;
	}

	/**
	 * 1.0 Checks whether instance is null 1.1 Creates a BeanstalkClient object.
	 * 1.2 returns the BeanstalkClient object
	 * 
	 * @return
	 */
	public static synchronized BeanstalkClient getInstance() {
		// 1.0 Checks whether instance is null than creates a new
		// BeanstalkClient object
		if (beanstalkClientInstance == null) {
			logger.info("BenastalkClient is null. creating now");
			try {
				// 1.1Creates a BeanstalkClient object.
				beanstalkClientInstance = new BeanstalkClient();
			} catch (BeanstemcException ex) {
				java.util.logging.Logger.getLogger(BeanstalkClient.class.getName()).log(Level.SEVERE, null, ex);
			} catch (UnknownHostException ex) {
				java.util.logging.Logger.getLogger(BeanstalkClient.class.getName()).log(Level.SEVERE, null, ex);
			} catch (IOException ex) {
				java.util.logging.Logger.getLogger(BeanstalkClient.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return beanstalkClientInstance; // 1.2
	}
}

// Set<String> keyPhrases = new HashSet<String>();
// keyPhrases.addAll(stats.keySet());
// Iterator iter = keyPhrases.iterator();
// String key = null;
// while (iter.hasNext())
// {
// key = (String) iter.next();
// logger.info("printing the stats: key "+key +" value"+ stats.get(key));
// }
// String dummyValue = stats.get("current-jobs-ready");
// logger.info("Dummy value of the current jobs "+ dummyValue + "integer value
// "+ Integer.parseInt(dummyValue));