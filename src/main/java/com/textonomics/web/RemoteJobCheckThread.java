/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.textonomics.web;

import com.textonomics.DocumentRS;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.Integer;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;

/*
 * Class to check if Remote BeanStalk jobs have been completed and to consolidate the Resume Phrase Map
 *
 *  EBB -- for each client, we start a new beanStalkd client and  that starts this thread.
 */
public class RemoteJobCheckThread extends Thread { // EBB -- Is this per client?

	static Logger logger = Logger.getLogger(RemoteJobCheckThread.class);
	Map<Long, List<File>> jobIdResumeMap; // Map <jobID from beanStalkD, List of
											// files (eg 5 filenames per jobID)
											// >
	HttpSession session;
	static final int SLEEP_INTERVAL = 5; // secs
	static final int TIMEOUT_PERRESUME = 20; // secs

	public RemoteJobCheckThread(HttpSession session, Map<Long, List<File>> jobIdResumeMap) {
		this.session = session;
		this.jobIdResumeMap = jobIdResumeMap;
	}

	public void run() {
		process();
	}

	//
	private void process() {
		List<String> allFiles = new ArrayList<String>();
		List<File> resumeFiles = new ArrayList<File>();
		int resumesPerJob = 0;
		for (Long jobId : jobIdResumeMap.keySet()) {
			List<File> files = jobIdResumeMap.get(jobId);
			if (resumesPerJob == 0) {
				resumesPerJob = files.size(); // grab the number of files for
												// the first job (which is max)
												// -- eg 5 resumes per job
			}
			for (File file : files) {
				allFiles.add(file.getName());
				resumeFiles.add(file);
			}
		}
		int timeout = resumesPerJob * TIMEOUT_PERRESUME; // EBB this seems
															// arbitrary -- 20
															// sec * 5
															// resumesPerJob =
															// 100 sec! why?

		session.setAttribute(SESSION_ATTRIBUTE.RESUME_FILES.toString(), resumeFiles);
		session.setAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(), allFiles.size());
		int numResumes = allFiles.size();
		long start = System.currentTimeMillis();
		while (true) {
			try {
				logger.debug("Waiting for remote job completion");
				sleep(SLEEP_INTERVAL * 1000);
			} catch (InterruptedException ex) {
				logger.error(ex);
			}
			timeout = timeout - SLEEP_INTERVAL;

			// accumulate the resume phrases from scratch each time we wake up
			// from sleep -- i.e., the while (true) is executed
			Map<String, Integer> allResumePhrases = new TreeMap<String, Integer>(); // EBB:
																					// To
																					// generate
																					// partial
																					// results--
																					// right?
																					// this
																					// map
																					// gets
																					// overwritten
																					// everytime
																					// the
																					// while(true)
																					// loop
																					// is
																					// executed!
			for (Long jobId : jobIdResumeMap.keySet()) {
				String phrasesKey = SessionKeyGenerator.getKeyForPhrases(session.getId(), jobId); // generate
																									// a
																									// Key
																									// that
																									// will
																									// be
																									// on
																									// MemCacheD
																									// --
																									// EBB
				TreeMap<String, Integer> jobResumePhrases = (TreeMap<String, Integer>) MemCacheClient.getInstance()
						.get(phrasesKey); // EBB -- if the treeMap has been grab
											// from MemCacheD it will be grabbed
											// again, everytime the while(true)
											// loop is executed!

				if (jobResumePhrases != null) // if the job is 5 resumes, this
												// tree will have the phrases
												// for all 5 of them.
				{
					jobResumePhrases = (TreeMap<String, Integer>) jobResumePhrases.clone(); // EBB
																							// --
																							// why
																							// are
																							// we
																							// copying
																							// the
																							// tree
																							// into
																							// ourselves?!!!
					for (String phrase : jobResumePhrases.keySet()) {
						Integer count = allResumePhrases.get(phrase);
						if (count == null) {
							count = 0;
						}
						count = count.intValue() + jobResumePhrases.get(phrase);
						allResumePhrases.put(phrase, count);
					}
				}
				// remove the files that have been processed. NB allFiles is set
				// outside of the while(true), so it is not overwritten. So will
				// be all the files that are not processed
				List<File> files = jobIdResumeMap.get(jobId);
				for (File file : files) {
					String fileName = file.getName();
					String fileKey = SessionKeyGenerator.getKey(session.getId(), fileName, jobId);
					DocumentRS resumeDoc = (DocumentRS) MemCacheClient.getInstance().get(fileKey);
					if (resumeDoc != null) {
						allFiles.remove(fileName); // All the files that are not
													// processed
					}
				}
			}
			session.setAttribute(SESSION_ATTRIBUTE.RESUME_PHRASES.toString(), allResumePhrases); // EBB
																									// --
																									// may
																									// be
																									// partial
																									// list,
																									// but
																									// it
																									// will
																									// be
																									// overwritten;
																									// I
																									// think
																									// this
																									// is
																									// where
																									// he
																									// grabs
																									// the
																									// partial
																									// list.
			session.setAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString(), allFiles.size()); // EBB
																										// --
																										// num
																										// files
																										// remaining
			if (allFiles.size() == 0) { // if all files are processed get out of
										// the while(true)
				break;
			}
			if (timeout <= 0) { // // if timeout reaches 0, then get out of the
								// while(true)
				logger.info("Timedout." + session.getAttribute(SESSION_ATTRIBUTE.NUM_RESUMES_TO_PROCESS.toString())
						+ "resumes left to process");
				break;
			}
		} // while (true)

		// log the time that it took to process these files.
		long end = System.currentTimeMillis();
		logger.info("Time to process " + numResumes + " resumes :" + (end - start) / numResumes + " msecs/resume"
				+ "total time took :" + (end - start));
		try {
			// Create file to store the resume processing time: average, total
			String filename = session.getId() + ".txt";
			FileWriter fstream = new FileWriter(filename);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write("Time to process\t" + numResumes + "\tresumes:\t" + (end - start) / numResumes + "\t msecs/resume"
					+ "\t Start Time " + start + "\t End Time" + end);
			// Close the output stream
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

		// all the files that are not processed during this time.
		String error = "";
		for (String fileName : allFiles) {
			error = error + "\nError processing resume : " + fileName;
		}
		session.setAttribute(SESSION_ATTRIBUTE.ERROR_PROCESSING_RESUMES.toString(), error);
	}
}
