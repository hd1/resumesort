package com.textonomics.checkout;

import org.jdom.Document;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Information about notification types can be found at
 * http://code.google.com/apis/checkout/developer/Google_Checkout_XML_API_Notification_API.html
 * 
 * @author Nuno Seco
 *
 */

public class AuthorizationAmountNotification extends StateChangeNotification {

	protected AuthorizationAmountNotification(Document doc) {
		super(doc);
	}

}
