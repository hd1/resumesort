package com.textonomics.checkout;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Information about notification types can be found at
 * http://code.google.com/apis/checkout/developer/Google_Checkout_XML_API_Notification_API.html
 *
 * @author Nuno Seco
 *
 */

public class MerchantInfo {
	public static MerchantInfo instance = new MerchantInfo();

	public static MerchantInfo getInstance() {
		return instance;
	}

	public String getMerchantId() {
		return System.getProperty("com.textonomics.checkout.merchantid");
	}

	public String getMerchantAuthToken() {
		return getMerchantId() + ":" + getMerchantKey();
	}

	public String getMerchantKey() {
		return System.getProperty("com.textonomics.checkout.merchantkey");
	}

	public String getMerchantUrl() {
		return System.getProperty("com.textonomics.checkout.url");
	}

}
