package com.textonomics.checkout;

import org.jdom.Document;
import org.jdom.Namespace;

import com.textonomics.user.CheckoutMessage;
import org.jdom.Element;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Information about notification types can be found at
 * http://code.google.com/apis/checkout/developer/Google_Checkout_XML_API_Notification_API.html
 *
 * @author Nuno Seco
 *
 */

public class NewOrderNotification extends Notification {
	protected NewOrderNotification(Document doc) {
		super(doc);
	}

	@Override
	protected void setCheckoutMessage(CheckoutMessage message) {
		message.setFinancialOrderState(getFinancialOrderState());
		message.setFullfillmentOrderState(getFullfillmentOrderState());
		message.setEmail(getEmail());
		message.setIdUser(getIdUser());
		message.setSubscriptionType(getSubscriptionType());
	}

	public String getFullfillmentOrderState() {
		Namespace namespace = doc.getRootElement().getNamespace();
		return doc.getRootElement().getChild("fulfillment-order-state", namespace).getText();
	}

	public String getFinancialOrderState() {
		Namespace namespace = doc.getRootElement().getNamespace();
		return doc.getRootElement().getChild("financial-order-state", namespace).getText();
	}

	public int getSubscriptionType() {
		try {
			Namespace namespace = doc.getRootElement().getNamespace();
			return Integer
					.parseInt(doc.getRootElement().getChild("shopping-cart", namespace).getChild("items", namespace)
							.getChild("item", namespace).getChild("merchant-item-id", namespace).getText());
		} catch (Exception x) {
		}
		return 0;
	}

	public String getSubscriptionPlan() {
		Namespace namespace = doc.getRootElement().getNamespace();
		return doc.getRootElement().getChild("shopping-cart", namespace).getChild("items", namespace)
				.getChild("item", namespace).getChild("item-name", namespace).getText();
	}

	public int getIdUser() {
		Namespace namespace = doc.getRootElement().getNamespace();
		return Integer.parseInt(doc.getRootElement().getChild("shopping-cart", namespace).getChild("items", namespace)
				.getChild("item", namespace).getChild("merchant-private-item-data", namespace)
				.getChild("user", namespace).getText());
	}

	public String getEmail() {
		Namespace namespace = doc.getRootElement().getNamespace();
		return doc.getRootElement().getChild("buyer-shipping-address", namespace).getChild("email", namespace)
				.getText();
	}

	public double getPrice() {
		Namespace namespace = doc.getRootElement().getNamespace();
		return Double.parseDouble(doc.getRootElement().getChild("shopping-cart", namespace).getChild("items", namespace)
				.getChild("item", namespace).getChild("unit-price", namespace).getText());
	}

	public boolean isRecurrent() {
		Element elm = null;
		try {
			Namespace namespace = doc.getRootElement().getNamespace();
			System.out.println("Subscr:" + doc.getRootElement().getChild("shopping-cart", namespace)
					.getChild("items", namespace).getChild("item", namespace).getChild("subscription", namespace));
			elm = doc.getRootElement().getChild("shopping-cart", namespace).getChild("items", namespace)
					.getChild("item", namespace).getChild("subscription", namespace)
					.getChild("recurrent-item", namespace);
		} catch (Exception x) {
			x.printStackTrace();
		}
		if (elm != null)
			return true;
		else
			return false;
	}

	public double getRecurrentPrice() {
		Element elm = null;
		Namespace namespace = doc.getRootElement().getNamespace();
		try {
			elm = doc.getRootElement().getChild("shopping-cart", namespace).getChild("items", namespace)
					.getChild("item", namespace).getChild("subscription", namespace)
					.getChild("recurrent-item", namespace);
		} catch (Exception x) {
		}
		if (elm != null) {
			return Double.parseDouble(elm.getChild("unit-price", namespace).getText());
		} else
			return 0.0;
	}

	public String getRecurrentPeriod() {
		Element elm = null;
		try {
			Namespace namespace = doc.getRootElement().getNamespace();
			elm = doc.getRootElement().getChild("shopping-cart", namespace).getChild("items", namespace)
					.getChild("item", namespace).getChild("subscription", namespace);
			return elm.getAttributeValue("period");
		} catch (Exception x) {
		}
		return "";
	}
}
