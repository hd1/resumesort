package com.textonomics.checkout;

import java.io.IOException;
import java.io.InputStream;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.textonomics.user.CheckoutMessage;
import com.textonomics.user.UserDataEntity;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This is a factory class for creating instances of Notifications. A
 * notification is any message sent from google. The XML data sent from google
 * is parsed into a object that is returned using the static factory method.
 *
 *
 * Information about notification types can be found at
 * http://code.google.com/apis/checkout/developer/Google_Checkout_XML_API_Notification_API.html
 * 
 * 
 * @author Nuno Seco
 *
 */

public abstract class Notification extends IncomingCheckOutMessage {

	public static Notification createInstance(InputStream input) throws JDOMException, IOException {
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(input);

		if (doc.getRootElement().getName().equals("new-order-notification"))
			return new NewOrderNotification(doc);
		else if (doc.getRootElement().getName().equals("risk-information-notification"))
			return new RiskInformationNotification(doc);
		else if (doc.getRootElement().getName().equals("order-state-change-notification"))
			return new OrderStateChangeNotification(doc);
		else if (doc.getRootElement().getName().equals("charge-amount-notification"))
			return new ChargeAmountNotification(doc);
		else if (doc.getRootElement().getName().equals("refund-amount-notification"))
			return new RefundAmountNotification(doc);
		else if (doc.getRootElement().getName().equals("chargeback-amount-notification"))
			return new ChargeAmountNotification(doc);
		else if (doc.getRootElement().getName().equals("authorization-amount-notification"))
			return new AuthorizationAmountNotification(doc);

		throw new JDOMException("Unknown Notification");

	}

	protected Notification(Document doc) {
		super(doc);
	}

	protected abstract void setCheckoutMessage(CheckoutMessage message);

	/**
	 * Converts a Notification (from google) to a {@link CheckoutMessage}. A
	 * checkout message is a {@link UserDataEntity} so it can be stored in the
	 * database.
	 * 
	 * @return a corresponding message
	 */
	public CheckoutMessage toCheckoutMessage() {
		CheckoutMessage message = new CheckoutMessage();
		message.setMessageName(getName());
		message.setOrderNumber(getOrderNumber());
		setCheckoutMessage(message);
		return message;
	}

}
