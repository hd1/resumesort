package com.textonomics.checkout;

import java.io.IOException;
import java.io.OutputStream;

import org.jdom.Document;
import org.jdom.output.XMLOutputter;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Information about notification types can be found at
 * http://code.google.com/apis/checkout/developer/Google_Checkout_XML_API_Notification_API.html
 *
 * @author Nuno Seco
 *
 */

public abstract class IncomingCheckOutMessage {
	protected Document doc;

	protected IncomingCheckOutMessage(Document doc) {
		this.doc = doc;
	}

	public void print(OutputStream out) throws IOException {
		XMLOutputter outputter = new XMLOutputter();
		outputter.output(doc, System.out);
	}

	public String getName() {
		return doc.getRootElement().getName();
	}

	public long getOrderNumber() {
		return Long.parseLong(
				doc.getRootElement().getChild("google-order-number", doc.getRootElement().getNamespace()).getText());
	}
}
