package com.textonomics.checkout;

import org.jdom.Document;

import com.textonomics.user.CheckoutMessage;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Information about notification types can be found at
 * http://code.google.com/apis/checkout/developer/Google_Checkout_XML_API_Notification_API.html
 * 
 * @author Nuno Seco
 *
 */

public class RefundAmountNotification extends Notification {

	protected RefundAmountNotification(Document doc) {
		super(doc);
	}

	@Override
	protected void setCheckoutMessage(CheckoutMessage message) {
		// DO NOTHING

	}

}
