package com.textonomics.checkout;

import com.textnomics.data.JobPosting;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.textonomics.user.User;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderPool;
import com.textonomics.web.SESSION_ATTRIBUTE;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sun.misc.BASE64Encoder;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This servlet is used by the Subscribe.jsp and allows the user to buy a
 * CakeWalk account using google checkout.
 * 
 * 
 *
 * @author Nuno Seco
 *
 */

public class Subscribe extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(Subscribe.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Subscribe() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Do Nothing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();
		try {
			BASE64Encoder encoder = new BASE64Encoder();
			// //Get the provider
			// UserDataProvider provider =
			// UserDataProviderPool.getInstance().acquireProvider();
			//
			// //get the data for the new user
			// User user = new User();
			// user.setEmail(request.getParameter("email").trim());
			// user.setLogin(request.getParameter("email").trim());
			// user.setName(request.getParameter("name").trim());
			// user.setPassword(request.getParameter("password").trim());
			// user.setState(1);//state to active by default
			// provider.insertUser(user);//add the user

			// Assume we already have a registered user. Now we want to ask him
			// to pay
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute(SESSION_ATTRIBUTE.CURRENT_USER.toString());
			String password = request.getParameter("password").trim();
			user = User.getAuthenticatedUser(user.getLogin().trim(), password.trim());
			if (user == null)
				throw new Exception("Invalid password");
			// lets get paid
			String planId = request.getParameter("plan");
			URL url = new URL(MerchantInfo.getInstance().getMerchantUrl());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Authorization",
					"Basic " + encoder.encode((MerchantInfo.getInstance().getMerchantAuthToken()).getBytes()));
			conn.setRequestProperty("Accept", "application/xml; charset=UTF-8");
			conn.setRequestProperty("Content-Type", "application/xml; charset=UTF-8");
			conn.setDoOutput(true);
			String postingId = request.getParameter("postingId");
			if (postingId != null && postingId.trim().length() > 0) {
				JobPosting posting = (JobPosting) new JobPosting().getObject("id=" + postingId);
				posting.setPaymentRequest(true);
				posting.update();
			}

			String subscrXML = "";
			if (planId != null) {
				subscrXML = new NewSubscriptionRequest(user).getXMLMessage(planId);
			} else {
				double price = getDouble(request.getParameter("price"));
				String desc = request.getParameter("description");
				if (price > 0)
					subscrXML = new NewSubscriptionRequest(user).getXMLMessage("Resume Sort Processing", desc, price);
			}
			logger.debug(subscrXML);
			conn.getOutputStream().write(subscrXML.getBytes());// Send message
																// to google
			CheckOutRedirectResponse redirect = CheckOutRedirectResponse.createResponse(conn.getInputStream());// get
																												// the
																												// response
			redirect.print(System.out);

			// provider.commit();//commit data
			response.sendRedirect(redirect.getRedirectUrl());// redirect the
																// user to the
																// google site

		}
		// catch (UserDataProviderException ex)
		// {
		// if (ex.getType() == USER_EXCEPTION_TYPES.USER_EXISTS)
		// {
		// request.getSession().setAttribute(SESSION_ATTRIBUTE.ERROR.toString(),
		// "Email, Login or Name already is signed up.");
		// response.sendRedirect(response.encodeRedirectURL("Subscribe.jsp"));
		// return;
		// }
		// else
		// {
		// throw new ServletException(ex);
		// }
		// }

		catch (Exception e) {
			throw new ServletException(e);
		} finally {
			UserDataProviderPool.getInstance().releaseProvider();// Release the
																	// provider
		}
	}

	private double getDouble(String value) {
		try {
			return new Double(value).doubleValue();
		} catch (Exception x) {
			return 0;
		}
	}

}
