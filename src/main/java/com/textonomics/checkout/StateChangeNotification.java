package com.textonomics.checkout;

import org.jdom.Document;

import com.textonomics.user.CheckoutMessage;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Information about notification types can be found at
 * http://code.google.com/apis/checkout/developer/Google_Checkout_XML_API_Notification_API.html
 * 
 * @author Nuno Seco
 *
 */

public abstract class StateChangeNotification extends Notification {
	protected StateChangeNotification(Document doc) {
		super(doc);
	}

	@Override
	protected void setCheckoutMessage(CheckoutMessage message) {
		message.setFinancialOrderState(getNewFinancialOrderState());
		message.setFullfillmentOrderState(getNewFullFillmentOrderState());
	}

	public String getNewFinancialOrderState() {
		return doc.getRootElement().getChild("new-financial-order-state", doc.getRootElement().getNamespace())
				.getText();
	}

	public String getNewFullFillmentOrderState() {
		return doc.getRootElement().getChild("new-fulfillment-order-state", doc.getRootElement().getNamespace())
				.getText();
	}

}
