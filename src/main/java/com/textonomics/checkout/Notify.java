package com.textonomics.checkout;

import com.textnomics.data.JobPosting;
import com.textnomics.data.ResumeScore;
import com.textnomics.data.SubscriptionPlan;
import com.textonomics.subscription.SubscriptionService;
import com.textonomics.user.Subscription;
import com.textonomics.user.User;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.misc.BASE64Decoder;
import com.textonomics.user.UserDataProvider;
import com.textonomics.user.UserDataProviderPool;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This servlet is used by the google checkout mechanism as call back. On the
 * google administrative web console the URL to the servlet should be
 * registered. Everytime an event happens regarding some purchase the servlet is
 * called to receive the information. At the moment everytime the servlet is
 * called all the information is stored in the database.
 *
 * @author Nuno Seco
 *
 */
public class Notify extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(Notify.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Notify() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Do Nothing
		logger.info("Get Notify servlet called");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDataProvider provider = UserDataProviderPool.getInstance().acquireProvider();
		try {
			logger.info("Notify servlet called");
			String authorization = request.getHeader("Authorization");
			if (authorization == null) {
				logger.error("Authentication Required");
				response.sendError(401);// Authentication Required
				return;
			}
			BASE64Decoder decoder = new BASE64Decoder();
			String token = new String(decoder.decodeBuffer(authorization.substring("Basic ".length())));
			if (!token.equals(MerchantInfo.getInstance().getMerchantAuthToken())) {
				logger.error("Invalid Merchant Token:" + token);
				response.sendError(500);// Internal Server error
				return;
			}

			Notification notification = Notification.createInstance(request.getInputStream());
			notification.print(System.out);

			// Add it to the database
			System.out.println("Inserting message into database");
			provider.insertCheckoutMessage(notification.toCheckoutMessage());
			if (notification instanceof NewOrderNotification) {
				NewOrderNotification newOrderNotification = (NewOrderNotification) notification;
				String email = newOrderNotification.getEmail();
				int id = newOrderNotification.getIdUser();
				User user = provider.getUser(id);
				Subscription subscription = new Subscription();
				subscription.setCheckoutOrderNo(String.valueOf(((NewOrderNotification) notification).getOrderNumber()));
				subscription.setUserId(user.getId());
				subscription.setSubscriptionPlan(((NewOrderNotification) notification).getSubscriptionPlan());
				subscription.setPaid(false);
				if (newOrderNotification.isRecurrent()) {
					subscription.setPrice(newOrderNotification.getRecurrentPrice());
					Calendar today = Calendar.getInstance();
					today.add(Calendar.MONTH, 1);
					today.add(Calendar.DATE, 1); // Google checkout is a day
													// later
					subscription.setExpirationDate(today.getTime());
				} else {
					subscription.setPrice(newOrderNotification.getPrice());
				}
				provider.insertSubscription(subscription);
			} else if (notification instanceof ChargeAmountNotification) {

			} else if (notification instanceof OrderStateChangeNotification
					&& ((OrderStateChangeNotification) notification).getNewFinancialOrderState().equals("CHARGED")
					&& ((OrderStateChangeNotification) notification).getNewFullFillmentOrderState()
							.equals("DELIVERED")) {
				Subscription subscription = provider.getSubscriptionByOrderNo(
						String.valueOf(((OrderStateChangeNotification) notification).getOrderNumber()));
				if (subscription != null) {
					User user = provider.getUser(subscription.getUserId());
					subscription.setPaid(true);
					Calendar expiration = Calendar.getInstance();
					String planName = subscription.getSubscriptionPlan();
					SubscriptionPlan plan = (SubscriptionPlan) new SubscriptionPlan()
							.getObject("name = '" + planName + "'");
					if (plan != null) {
						logger.info("Plan:" + plan.getName() + " Resumes:" + plan.getNumresumes() + " Postings:"
								+ plan.getNumpostings());
						subscription.setUnusedResumeCount(plan.getNumresumes());
						subscription.setUnusedJobPostings(plan.getNumpostings());
						if (plan.getDays() > 0) {
							expiration.add(Calendar.DATE, plan.getDays() + 1);
							subscription.setExpirationDate(expiration.getTime());
						}
					} else {
						logger.info("Plan not found:'" + planName + "'");
						int numResumes = (int) (subscription.getPrice() / SubscriptionService.PRICE_PER_RESUME);
						subscription.setUnusedResumeCount(numResumes);
						int numPostings = subscription.getUnusedJobPostings();
						subscription.setUnusedJobPostings(numPostings++);
					}
					// Send resume if pending
					List<JobPosting> postings = new JobPosting()
							.getObjects("payment_request=" + 1 + " order by posting_date desc");
					if (postings.size() > 0) {
						JobPosting posting = postings.get(0);
						posting.setPaid(true);
						posting.setNumRuns(1);
						posting.setPaymentRequest(false);
						posting.update();
						List<ResumeScore> scores = new ResumeScore().getObjects("JOB_ID=" + posting.getId());
						for (ResumeScore score : scores) {
							score.setPaid(true);
							score.update();
						}
						int remaining = subscription.getUnusedJobPostings() - 1;
						if (remaining < 0)
							remaining = 0;
						logger.info("Remaining postings:" + remaining);
						subscription.setUnusedJobPostings(remaining);
					}
					provider.updateSubscription(subscription);
					provider.commit();
				}
			}
			provider.commit();
		} catch (Exception e) {
			e.printStackTrace();
			response.sendError(500);// Internal Server error
			throw new ServletException(e);
		} finally {
			UserDataProviderPool.getInstance().releaseProvider();// Release
																	// it!!!
		}
	}

}
