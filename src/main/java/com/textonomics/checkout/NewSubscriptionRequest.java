package com.textonomics.checkout;

import com.textnomics.data.SubscriptionPlan;
import com.textonomics.user.User;
import java.text.DecimalFormat;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * The instance of this class creates the XML message to send to the google
 * checkout system. This class may be changed or subclassed for different
 * messages
 *
 * @author Nuno Seco
 *
 */

public class NewSubscriptionRequest {
	private User user;
	public static final String subscriptionFee = "19.95";

	public NewSubscriptionRequest(User user) {
		this.user = user;
	}

	public String getXMLMessage(String planId) throws Exception {
		SubscriptionPlan plan = (SubscriptionPlan) new SubscriptionPlan().getObject("id = " + planId);
		if (plan.isRecurring()) {
			return getRecurringXMLMessage(plan.getName(), plan.getDescription(), plan.getPrice());
		} else {
			return getXMLMessage(plan.getName(), plan.getDescription(), plan.getPrice());
		}
	}

	public String getXMLMessage(String name, String description, double price) throws Exception {
		StringBuilder builder = new StringBuilder();

		builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		builder.append("<checkout-shopping-cart xmlns=\"http://checkout.google.com/schema/2\">\n");
		builder.append("<shopping-cart>\n");
		builder.append("<items>\n");
		builder.append("<item>\n");

		builder.append("<merchant-item-id>1</merchant-item-id>\n");// User
		builder.append("<item-name>" + name + "</item-name>\n");
		builder.append("<item-description>" + description + "</item-description>\n");
		builder.append("<quantity>1</quantity>");
		builder.append("<unit-price currency=\"USD\">" + new DecimalFormat("##.00").format(price) + "</unit-price>\n");
		builder.append("<merchant-private-item-data>\n");
		builder.append("<user>" + user.getId() + "</user>\n");
		builder.append("</merchant-private-item-data>\n");

		builder.append("<digital-content>\n");
		builder.append("<display-disposition>OPTIMISTIC</display-disposition>\n");
		builder.append("<description>\n");
		builder.append("Please go to &lt;a href=&quot;http://resumesort.com&quot;&gt;Resume Sort&lt;/a&gt;,");
		builder.append("and start .....");
		builder.append("</description>\n");
		builder.append("</digital-content>\n");

		builder.append("</item>\n");
		builder.append("</items>\n");
		builder.append("</shopping-cart>\n");
		builder.append("<checkout-flow-support>\n");
		builder.append("<merchant-checkout-flow-support/>\n");
		builder.append("</checkout-flow-support>\n");
		builder.append("</checkout-shopping-cart>\n");

		return builder.toString();
	}

	public String getRecurringXMLMessage(String name, String description, double price) throws Exception {
		StringBuilder builder = new StringBuilder();

		builder.append("<?xml version=`1.0` encoding=`UTF-8` ?>\n");
		builder.append("<checkout-shopping-cart xmlns=`http://checkout.google.com/schema/2`>\n");
		builder.append("<shopping-cart>\n");
		builder.append("<items>\n");
		builder.append("<item>\n");
		builder.append("<merchant-item-id>1</merchant-item-id>\n");
		builder.append("<item-name>" + name + "</item-name>\n");
		builder.append("<item-description>" + description + "</item-description>\n");
		builder.append("<unit-price currency=`USD`>" + new DecimalFormat("##.00").format(price) + "</unit-price>\n");
		builder.append("<quantity>1</quantity>\n");
		builder.append("<merchant-private-item-data>\n");
		builder.append("<user>" + user.getId() + "</user>\n");
		builder.append("</merchant-private-item-data>\n");
		builder.append("<subscription type=`google` period =`MONTHLY`>\n");
		builder.append("<payments>\n");
		builder.append("<subscription-payment times=`12`>\n");
		builder.append(
				"<maximum-charge currency=`USD`>" + new DecimalFormat("##.00").format(price) + "</maximum-charge>\n");
		builder.append("</subscription-payment>\n");
		builder.append("</payments>\n");
		builder.append("<recurrent-item>\n");
		builder.append("<item-name>" + name + "</item-name>\n");
		builder.append("<item-description>" + description + "</item-description>\n");
		builder.append("<quantity>1</quantity>\n");
		builder.append("<unit-price currency=`USD`>" + new DecimalFormat("##.00").format(price) + "</unit-price>\n");
		builder.append("<digital-content>\n");
		builder.append("<display-disposition>OPTIMISTIC</display-disposition>\n");
		builder.append("<url>https://www.resumesort.com</url>\n");
		builder.append("<description>Please proceed to the website!</description>\n");
		builder.append("</digital-content>\n");
		builder.append("</recurrent-item>\n");
		builder.append("</subscription>\n");
		builder.append("<digital-content>\n");
		builder.append("<display-disposition>OPTIMISTIC</display-disposition>\n");
		builder.append(
				"<description>Congratulations! Your subscription is being set up.  log onto &amp;lt;a href='https://www.resumesort.com'&amp;gt;www.resumesort.com&amp;lt;/a&amp;gt; and try it out!</description>\n");
		builder.append("</digital-content>\n");
		builder.append("</item>\n");
		// builder.append("<item>\n");
		// builder.append("<item-name>Decoder Ring</item-name>\n");
		// builder.append("<item-description>One-time charge for the decoder
		// ring you (coincidentally) also ordered from
		// me.</item-description>\n");
		// builder.append("<unit-price currency=`USD`>5.00</unit-price>\n");
		// builder.append("<quantity>1</quantity>\n");
		// builder.append("</item>\n");
		builder.append("</items>\n");
		builder.append("</shopping-cart>\n");
		builder.append("</checkout-shopping-cart>\n");
		return builder.toString().replace('`', '\"');
	}
}
