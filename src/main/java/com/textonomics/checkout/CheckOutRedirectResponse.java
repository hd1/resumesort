package com.textonomics.checkout;

import java.io.IOException;
import java.io.InputStream;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Information about notification types can be found at
 * http://code.google.com/apis/checkout/developer/Google_Checkout_XML_API_Notification_API.html
 *
 * @author Nuno Seco
 *
 */

public class CheckOutRedirectResponse extends IncomingCheckOutMessage {

	public static CheckOutRedirectResponse createResponse(InputStream input) throws JDOMException, IOException {
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(input);

		if (doc.getRootElement().getName().equals("checkout-redirect"))
			return new CheckOutRedirectResponse(doc);

		throw new JDOMException("Unknown Response");
	}

	public String getSerialNumber() {
		return doc.getRootElement().getAttributeValue("serial-number");
	}

	public String getRedirectUrl() {
		return doc.getRootElement().getChild("redirect-url", doc.getRootElement().getNamespace()).getText();
	}

	protected CheckOutRedirectResponse(Document doc) {
		super(doc);
	}
}
