/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics;

import com.textonomics.nlp.NGram;

/**
 *
 * @author Dev Gude
 */
public class SynonymPhrase extends Phrase {

	private String sourcePhrase;

	public SynonymPhrase(String buffer, NGram ngram, int sentence, String sourcePhrase) {
		super(buffer, ngram, sentence);
		this.sourcePhrase = sourcePhrase;
	}

	public String getSourcePhrase() {
		return sourcePhrase;
	}

	public void setSourcePhrase(String sourcePhrase) {
		this.sourcePhrase = sourcePhrase;
	}

}
