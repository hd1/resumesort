package com.textonomics.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * Specific authentication class used for the resumecakewalk accounts created on
 * gmail
 *
 * @author Nuno Seco
 *
 */

class ResumeCakeWalkAuthenticator extends Authenticator {
	private String login;

	private String password;

	protected ResumeCakeWalkAuthenticator(String login, String password) {
		this.login = login;
		this.password = password;
	}

	protected String getLogin() {
		return login;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(login, password);
	}
}
