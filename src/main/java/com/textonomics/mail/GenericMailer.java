package com.textonomics.mail;

import java.io.File;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * A wrapper from the {@link Mailer} class thats is used when sending general
 * emails
 *
 * @author Nuno Seco
 *
 */

public class GenericMailer {

	/**
	 * Singleton
	 */
	private final static GenericMailer instance = new GenericMailer();

	/**
	 * Wrapped instance
	 */
	private final Mailer mailer;

	/**
	 * Singleton Accessor
	 * 
	 * @return the single instance
	 */
	public static GenericMailer getInstance() {
		return instance;
	}

	/**
	 * Private constructor
	 */
	private GenericMailer() {
		// This pool should be populated with accounts configured in conf file
		ResumeCakeWalkAuthenticatorPool pool = new ResumeCakeWalkAuthenticatorPool(3);
		pool.addAuthenticator(new ResumeCakeWalkAuthenticator("noreply2@resumesort.com", "textnrnomics888"));
		pool.addAuthenticator(new ResumeCakeWalkAuthenticator("noreply1@resumesort.com", "textnrnomics888"));
		pool.addAuthenticator(new ResumeCakeWalkAuthenticator("noreply2@resumesort.com", "textnrnomics888"));

		mailer = new Mailer(pool);

	}

	/**
	 * Wraps the sendEmail method
	 * 
	 * @param to
	 * @param subject
	 * @param message
	 */
	public void sendMail(String to, String subject, String message) {
		mailer.sendMail(to, message, subject, null);
	}

	/**
	 * Wraps the sendEmail method
	 *
	 * @param to
	 * @param subject
	 * @param message
	 */
	public void sendMail(String to, String subject, String message, File attachment) {
		mailer.sendMail(to, message, subject, attachment);
	}

	public void sendMail(String to, String from, String subject, String message, File attachment) {
		mailer.sendMail(to, from, message, subject, attachment);
	}
}
