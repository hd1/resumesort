package com.textonomics.mail;

import com.textonomics.user.User;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 * 
 * Ths class represents the mailer that is used to send the emails when a user
 * wants to reset his password
 *
 *
 */

public class PasswordMailer {
	/**
	 * Singleton
	 */
	private final static PasswordMailer instance = new PasswordMailer();

	/**
	 * Wrapped mailer
	 */
	private final Mailer mailer;

	/**
	 * Subject of email
	 */
	private final String subject;

	/**
	 * Singleton Acessor
	 * 
	 * @return the instance
	 */
	public static PasswordMailer getInstance() {
		return instance;
	}

	/**
	 * private constructor
	 */
	private PasswordMailer() {
		this.subject = "Your New Password From ResumeSort";

		// This pool should be populated with accounts configured in conf file
		ResumeCakeWalkAuthenticatorPool pool = new ResumeCakeWalkAuthenticatorPool(3);
		pool.addAuthenticator(new ResumeCakeWalkAuthenticator("noreply2@resumesort.com", "textnrnomics888"));
		pool.addAuthenticator(new ResumeCakeWalkAuthenticator("noreply1@resumesort.com", "textnrnomics888"));
		pool.addAuthenticator(new ResumeCakeWalkAuthenticator("noreply2@resumesort.com", "textnrnomics888"));

		mailer = new Mailer(pool);
	}

	/**
	 * Wrapped send mail
	 * 
	 * @param to
	 * @param attachment
	 * @param missingTerms
	 * @param missingSecondaryTerms
	 * @param diction
	 * @param spellingSuggestions
	 * @param header
	 */
	public void sendMail(User user) {
		mailer.sendMail(user.getEmail(), getMessage(subject, user.getName(), user.getPassword()), subject, null);
	}

	/**
	 * Generates the HTML body of the message
	 * 
	 * @return the body of the html message
	 */
	private String getMessage(String subject, String userName, String newPassword) {
		StringBuilder sb = new StringBuilder();
		sb.append("<HTML>\n");
		sb.append("<HEAD>\n");
		sb.append("<TITLE>\n");
		sb.append(subject + "\n");
		sb.append("</TITLE>\n");
		sb.append("</HEAD>\n");
		sb.append("<BODY>\n");
		// sb.append("<H1>" + subject + "</H1>" + "\n");
		sb.append("<br />");
		sb.append("Dear " + userName + ",");
		sb.append("<br />");
		sb.append("<br />");
		sb.append(
				"The password for your account at <a href=\"http://www.resumetuner.com\">www.ResumeTuner.com</a> has been reset.");
		sb.append("<br />");
		sb.append("<br />");
		sb.append("Your new password is " + newPassword + " . Please login and change your password.");
		sb.append("</BODY>\n");
		sb.append("</HTML>\n");
		return sb.toString();

	}
}
