package com.textonomics.mail;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * A wrapper from the {@link Mailer} class thats is used when sending emails
 * about a users registration for the alpha or alpta
 *
 * @author Nuno Seco
 *
 */

public class RegistrationMailer {
	public enum REGISTRATION {
		ALPHA, ALPTA;
	}

	/**
	 * Singleton
	 */
	private final static RegistrationMailer instance = new RegistrationMailer();

	/**
	 * Wrapped instance
	 */
	private final Mailer mailer;

	/**
	 * constant subject
	 */
	private final String subject;

	/**
	 * Singleton Accessor
	 * 
	 * @return the single instance
	 */
	public static RegistrationMailer getInstance() {
		return instance;
	}

	/**
	 * Private constructor
	 */
	private RegistrationMailer() {
		this.subject = "Your Registration at ResumeSort.com is Complete";
		// This pool should be populated with accounts configured in conf file
		ResumeCakeWalkAuthenticatorPool pool = new ResumeCakeWalkAuthenticatorPool(3);
		pool.addAuthenticator(new ResumeCakeWalkAuthenticator("noreply2@resumesort.com", "textnrnomics888"));
		pool.addAuthenticator(new ResumeCakeWalkAuthenticator("noreply1@resumesort.com", "textnrnomics888"));
		pool.addAuthenticator(new ResumeCakeWalkAuthenticator("noreply2@resumesort.com", "textnrnomics888"));
		mailer = new Mailer(pool);

	}

	/**
	 * Wraps the sendEmail method
	 * 
	 * @param to
	 * @param type
	 *            if alpha or alpta
	 * @param login
	 *            the login of the person is his email
	 * @param password
	 *            the password
	 */
	public void sendMail(String to, REGISTRATION type, String login, String password) {
		mailer.sendMail(to, getMessage(type, login, password), subject, null);
	}

	/**
	 * Proxy method to get the formatted method
	 * 
	 * @param type
	 * @param login
	 * @param password
	 * @return the body of html message
	 */
	private String getMessage(REGISTRATION type, String login, String password) {
		if (type == REGISTRATION.ALPHA)
			return getAlphaMessage(login, password);
		else if (type == REGISTRATION.ALPTA)
			return getAlptaMessage(login, password);

		return "";
	}

	/**
	 * Generates the html body of the alpta message
	 * 
	 * @param login
	 * @param password
	 * @return the body of the html message sent to the user
	 */
	private String getAlptaMessage(String login, String password) {
		StringBuilder builder = new StringBuilder();
		builder.append("<html>");
		builder.append("<head>");
		builder.append("</head>");
		builder.append("<body>");
		builder.append("<h2><span style=\"color: #3c961e;\">Congratulations!</span></h2>");
		builder.append("<br />");
		builder.append("You successfully completed your registration with ResumeSort.com.");
		builder.append("<br />");
		builder.append(
				"When we are ready to go with the <b>alpta</b> (between alpha and beta) version we'll let you know!");
		builder.append("<br />");
		builder.append("<br />");
		builder.append("<br />");
		builder.append("Always yours,<br />");
		builder.append("the Textnomics team. <br />");
		builder.append("<br />");
		builder.append(
				"<div id=\"logo\"><img src=\"http://www.resumesort.com/images/logo_resumeq.jpg\" alt=\"Textnomics\" border=\"0\"/></div>");
		builder.append("</body>");
		builder.append("</html>");
		return builder.toString();
	}

	/**
	 * Generates the html message of the alpha message
	 * 
	 * @param login
	 * @param password
	 * @return the body of the html message sent to the user
	 */
	private String getAlphaMessage(String login, String password) {
		StringBuilder builder = new StringBuilder();
		builder.append("<html>");
		builder.append("<head>");
		builder.append("</head>");
		builder.append("<body>");
		builder.append("<h2><span style=\"color: #3c961e;\">Congratulations!</span></h2>");
		builder.append("<br />");
		builder.append("You successfully completed your registration with ResumeSort.com.");
		builder.append("<br />");
		builder.append("<br />");
		builder.append("Please login at <a href=\"http://www.resumesort.com\">www.resumesort.com</a> using");
		builder.append("<br />");
		builder.append("<b><span style=\"color: #ff9600\">email: </span> <span style=\"color: #3c961e;\">" + login
				+ "</span></b>");
		if (password != null && password.length() > 0) {
			builder.append("<br />");
			builder.append("<b><span style=\"color: #ff9600\">password: </span> <span style=\"color: #3c961e;\">"
					+ password + "</span></b>");
		}
		builder.append("<br />");
		builder.append("<br />");
		builder.append("<br />");
		builder.append("<br />");
		builder.append("Always yours,<br />");
		builder.append("the Textnomics team. <br />");
		builder.append("<br />");
		builder.append(
				"<div id=\"logo\"><img src=\"http://www.resumesort.com/images/logo_resumeq.png\" alt=\"Textnomics\" border=\"0\"/></div>");
		builder.append("</body>");
		builder.append("</html>");
		return builder.toString();
	}

}
