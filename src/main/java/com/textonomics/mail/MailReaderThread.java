/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.mail;

import com.textnomics.data.JobPosting;
import com.textonomics.PlatformProperties;
import com.textonomics.web.AddResumeToJobPosting;
import com.textonomics.web.ProcessResumes;
import com.textonomics.web.ResumeSorter;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import org.apache.log4j.Logger;

public class MailReaderThread extends Thread {
	static Logger logger = Logger.getLogger(MailReaderThread.class);
	int interval = 15; // minutes
	public static int runcount = 0;

	@Override
	public void run() {
		try {
			sleep(1 * 20 * 1000); // Wait one minute before loop
		} catch (Exception e) {
		}

		int propInterval = getInt(PlatformProperties.getInstance().getProperty("com.textnomics.read_email_interval"));
		if (propInterval > 0)
			interval = propInterval;
		while (true) {
			try {
				if (logger == null)
					logger = Logger.getLogger(MailReaderThread.class);
				if (runcount % 100 == 0)
					logger.info("EMail Reader started...");
				runcount++;
				readMail();
				sleep(interval * 60 * 1000);
			} catch (Exception ex) {
				ex.printStackTrace();
				try {
					sleep(interval * 60 * 1000);
				} catch (Exception ex1) {
				}
			}
		}
	}

	String resumeFolder;
	String userName;
	StringBuffer coverLetter;
	String contentType;
	String subject;
	File attachmentFile;
	JobPosting posting;
	boolean deleteMsg;

	public void readMail() throws Exception {
		String host = PlatformProperties.getInstance().getProperty("com.textnomics.email_host");
		String username = PlatformProperties.getInstance().getProperty("com.textnomics.email_username");
		String password = PlatformProperties.getInstance().getProperty("com.textnomics.email_password");
		if (host == null) {
			System.out.println("Host is null");
			return;
		}
		if (logger != null)
			logger.info("Reading mail, host:" + host + " username:" + username);
		Session session = Session.getInstance(new Properties(), null);
		// Store store = session.getStore("pop3s");
		Store store = session.getStore("imaps");

		store.connect(host, username, password);
		// Folder[] folders = store.getDefaultFolder().list("*");
		// for (javax.mail.Folder folder : folders) {
		// if ((folder.getType() & javax.mail.Folder.HOLDS_MESSAGES) != 0) {
		// System.out.println("Folder: " + folder.getFullName() + ": " +
		// folder.getMessageCount());
		// }
		// }
		String[] folderNames = { "INBOX", "[Gmail]/Spam" };
		for (String folderName : folderNames) {
			Folder folder = store.getFolder(folderName);
			if (!folder.exists()) {
				System.out.println("Folder:" + folderName + " does not exists");
				continue;
			}

			folder.open(Folder.READ_WRITE);
			Message messages[] = folder.getMessages();
			System.out.println("Folder:" + folder.getName() + " Count:" + messages.length);
			for (int i = 0; i < messages.length; i++) {
				if (messages[i].isSet(Flags.Flag.SEEN))
					continue;
				try {
					Enumeration headers = messages[i].getAllHeaders();
					while (headers.hasMoreElements()) {
						Header h = (Header) headers.nextElement();
						// System.out.println(h.getName() + "---: " +
						// h.getValue());
					}
				} catch (Exception x) {
					continue;
				}
				// System.out.println(i + " From: " + message[i].getFrom()[0] +
				// "\t To: " + message[i].getAllRecipients() + "\t Subject: " +
				// message[i].getSubject());
				InternetAddress from = (InternetAddress) messages[i].getFrom()[0];
				Address[] recipients = messages[i].getAllRecipients();
				String emailTo = null;
				for (Address recipient : recipients) {
					InternetAddress to = (InternetAddress) recipient;
					if (to.getAddress().contains("resumesort.com")) {
						emailTo = to.getAddress();
						emailTo = emailTo.substring(0, emailTo.indexOf("@"));
					}
				}
				resumeFolder = null;
				coverLetter = new StringBuffer();
				contentType = null;
				deleteMsg = false;
				attachmentFile = null;
				posting = null;
				subject = null;
				if (emailTo != null) {
					List<JobPosting> postings = new JobPosting()
							.getObjects("email_address=" + JobPosting.toSQL(emailTo));
					if (postings.size() > 0) {
						posting = postings.get(0);
						// TODO: if email from some one other than job posting
						// owner, reject email - uncomment later
						// if (posting.getUserName().equals(from.getAddress()))
						// {
						// logger.info("Unsolicited email from " + from + "
						// Subject:" + message[i].getSubject() + " will be
						// deleted");
						// deleteMsg = true;
						// }
						// else
						{
							resumeFolder = posting.getResumeFolder();
							if (resumeFolder != null && resumeFolder.length() > 0)
								resumeFolder = "uploads" + "/" + posting.getUserName() + "/"
										+ ResumeSorter.RESUME_FOLDER + "/" + resumeFolder;
							userName = posting.getUserName();
							subject = messages[i].getSubject();
						}
					} else {
						deleteMsg = true;
						logger.info("Unsolicited email from " + from + " Subject:" + messages[i].getSubject()
								+ " will be deleted");
					}
				}
				Object content = messages[i].getContent();
				if (content instanceof Multipart) {
					handleMultipart((Multipart) content);
				} else {
					handlePart(messages[i]);
				}
				// TODO: Should we delete message after reading???
				messages[i].setFlag(Flags.Flag.SEEN, true);
				if (deleteMsg) {
					messages[i].setFlag(Flags.Flag.DELETED, true);
				} else {
					if (coverLetter.length() > 0) {
						String filename = ResumeSorter.COVERLETTER_PREFIX + attachmentFile.getName();
						if (filename.indexOf(".") != -1)
							filename = filename.substring(0, filename.lastIndexOf("."));
						if ("text/html".equalsIgnoreCase(contentType))
							filename = filename + ".html";
						else
							filename = filename + ".txt";
						try {
							File parentFolder = PlatformProperties.getInstance().getResourceFile(resumeFolder);
							File coverletter = new File(parentFolder, filename);
							FileWriter writer = new FileWriter(coverletter);
							writer.write(coverLetter.toString());
							writer.close();
							logger.info("writing coverletter: " + coverletter.getAbsolutePath());
						} catch (Exception x) {
							x.printStackTrace();
						}
						try {
							logger.info("calling AddResumeToJobPosting for: " + attachmentFile.getName());
							AddResumeToJobPosting addResumeThread = new AddResumeToJobPosting(posting, attachmentFile,
									subject);
							addResumeThread.start();
						} catch (Exception x) {
							x.printStackTrace();
						}
					}
				}

			}
			folder.close(false);
		}
		store.close();
	}

	public void handleMultipart(Multipart multipart) throws MessagingException, IOException {
		int length = multipart.getCount();
		// If alternative multipart, process only first one
		System.out.println("Multipart, contentType:" + multipart.getContentType());
		if (multipart.getContentType() != null && multipart.getContentType().toLowerCase().indexOf("alternative") != -1)
			length = 1;
		for (int i = 0; i < length; i++) {
			handlePart(multipart.getBodyPart(i));
		}
	}

	public void handlePart(Part part) throws MessagingException, IOException {
		String dposition = part.getDisposition();
		String cType = part.getContentType();
		if (Part.ATTACHMENT.equalsIgnoreCase(dposition)) {
			// System.out.println("Attachment: " + part.getFileName() + " : " +
			// cType);
			if (resumeFolder != null)
				saveFile(part.getFileName(), part.getInputStream(), part.getContentType());
		} else if (Part.INLINE.equalsIgnoreCase(dposition)) {
			System.out.println("Inline: " + part.getFileName() + " : " + cType);
			// if (resumeFolder != null)
			// saveFile(part.getFileName(), part.getInputStream());
		} else {
			Object o = part.getContent();
			System.out.println(
					"Disposition: " + dposition + " contentType:" + cType + " type:" + o.getClass().getSimpleName());
			if (o instanceof String) {
				coverLetter.append(o);
				if (contentType == null)
					contentType = cType;
			} else if (o instanceof InputStream) {
				if (contentType == null)
					contentType = cType;
				int j;
				while ((j = ((InputStream) o).read()) != -1) {
					coverLetter.append((char) j);
				}
			} else if (o instanceof Multipart) {
				handleMultipart((Multipart) o);
			}
		}
	}

	public void saveFile(String filename, InputStream input, String contentType) throws IOException {
		String ext = ".doc"; // default
		if (filename.indexOf(".") != -1) {
			ext = filename.substring(filename.lastIndexOf("."));
		} else {
			if (contentType != null) {
				if (contentType.indexOf("msword") != -1)
					ext = ".doc";
				else if (contentType.indexOf("pdf") != -1)
					ext = ".pdf";
				else if (contentType.indexOf("opendocument") != -1)
					ext = ".odt";
				else if (contentType.indexOf("rtf") != -1)
					ext = ".rtf";
				else if (contentType.indexOf("text/html") != -1)
					ext = ".html";
				else if (contentType.indexOf("text/plain") != -1)
					ext = ".txt";
			}
		}
		if (filename == null) {
			filename = "Resume_" + userName + "_" + new SimpleDateFormat("-yyyy-MM-dd-HHmm").format(new Date()) + ext;
		} else if (filename.indexOf(".") == -1) {
			filename = filename + ext;
		}
		File parentFolder = PlatformProperties.getInstance().getResourceFile(resumeFolder);
		File file = new File(parentFolder, filename);
		String name = filename;
		if (filename.indexOf(".") != -1) {
			name = filename.substring(0, filename.lastIndexOf("."));
			ext = filename.substring(filename.lastIndexOf("."));
		}

		for (int i = 0; file.exists(); i++) {
			filename = name + "_" + i + ext;
			file = new File(parentFolder, filename);
		}
		logger.info("downloading attachment: " + file.getAbsolutePath() + " contentType:" + contentType);
		if (attachmentFile == null)
			attachmentFile = file;
		FileOutputStream fos = new FileOutputStream(file);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		BufferedInputStream bis = new BufferedInputStream(input);
		int fByte;
		while ((fByte = bis.read()) != -1) {
			bos.write(fByte);
		}
		bos.flush();
		bos.close();
		bis.close();
	}

	int getInt(String value) {
		try {
			return new Integer(value).intValue();
		} catch (Exception x) {
			return 0;
		}
	}
}