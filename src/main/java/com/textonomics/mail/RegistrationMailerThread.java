package com.textonomics.mail;

import com.textonomics.mail.RegistrationMailer.REGISTRATION;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * The class implements the {@link Runnable} interface and can be instanced as a
 * thread. When the thread is executed it sends an email the given recipient.
 *
 * @author Nuno Seco
 *
 */

public class RegistrationMailerThread implements Runnable {
	private final String to;

	private final String login;

	private final String password;

	private final REGISTRATION type; // Alpha or Alpta

	public RegistrationMailerThread(String to, REGISTRATION type, String login, String password) {
		this.to = to;
		this.type = type;
		this.password = password;
		this.login = login;
	}

	@Override
	public void run() {
		RegistrationMailer.getInstance().sendMail(to, type, login, password);
	}

}
