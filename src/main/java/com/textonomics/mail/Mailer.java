package com.textonomics.mail;

import java.io.File;
import java.io.IOException;
import java.security.Security;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This class is used for sending email to some SMTP server. In the specific
 * case the class is configured to send emails using gmail.
 *
 * @author Nuno Seco
 *
 */

public class Mailer {
	/**
	 * Properties used to create an smtp session
	 */
	private final Properties props;

	/**
	 * Concurrency control
	 */
	private final Object mutex;

	/**
	 * Holds the information about the gmail accounts that can be used
	 */
	private final ResumeCakeWalkAuthenticatorPool authenticatorPool;

	/**
	 * constructor
	 * 
	 * @param pool
	 *            authenticated accounts
	 */
	public Mailer(ResumeCakeWalkAuthenticatorPool pool) {

		authenticatorPool = pool;
		mutex = new Object();
		props = new Properties();

		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		// set properties
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", "smtp.gmail.com");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.socketFactory.port", "587");
		// props.put("mail.smtp.socketFactory.class",
		// "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.quitwait", "false");
		props.put("mail.smtp.starttls.enable", "true");
		// props.put("mail.smtp.from", "noreply1@resumetuner.com");
	}

	/**
	 * Sends the email.
	 * 
	 * @param to
	 *            receiver
	 * @param message
	 *            boby of the message
	 * @param subject
	 *            the subject
	 * @param attachment
	 *            an attachment, may be null
	 */
	public void sendMail(String to, String message, String subject, File attachment) {
		sendMail(to, null, message, subject, attachment);
	}

	public void sendMail(String to, String from, String message, String subject, File attachment) {
		MimeMessage msg = null;
		synchronized (mutex) {
			try {
				ResumeCakeWalkAuthenticator authenticator = authenticatorPool.rotate();
				Session session = Session.getInstance(props, authenticator);
				msg = new MimeMessage(session);
				if (from == null)
					from = authenticator.getLogin();
				msg.setFrom(new InternetAddress(from.replace("resumecakewalk", "resumesort")));

				InternetAddress[] address = { new InternetAddress(to) };
				msg.setRecipients(Message.RecipientType.TO, address);
				msg.setSubject(subject);

				Multipart mp = new MimeMultipart();

				MimeBodyPart mbp1 = new MimeBodyPart();
				mbp1.setDataHandler(new DataHandler(new ByteArrayDataSource(message, "text/html")));
				mp.addBodyPart(mbp1);

				if (attachment != null) {
					MimeBodyPart mbp2 = new MimeBodyPart();
					mbp2.attachFile(attachment);
					mp.addBodyPart(mbp2);
				}

				msg.setContent(mp);
				msg.setSentDate(new Date());

				Transport.send(msg);

			} catch (AddressException e) {
				e.printStackTrace();
			} catch (MessagingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				System.out.println("Sent mail subject:" + subject + " to:" + to + " from:"
						+ ((InternetAddress) msg.getFrom()[0]).getAddress());
			} catch (MessagingException ex) {
				Logger.getLogger(Mailer.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
}
