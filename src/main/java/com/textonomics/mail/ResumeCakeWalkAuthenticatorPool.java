package com.textonomics.mail;

import java.util.ArrayList;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This class represents a pool of resume cake walk authenticators.
 * 
 * Every time a mailer class asks for an authenticator it gets a different one.
 * At the moment we have four noreply accounts, each account is used in a round
 * robin fashion.
 *
 * On gmail each account can send up to 500 emails per day. So by having four
 * accounts the system may send 2000 emails.
 * 
 * @author Nuno Seco
 *
 */

public class ResumeCakeWalkAuthenticatorPool {
	/**
	 * the pool
	 */
	private final ArrayList<ResumeCakeWalkAuthenticator> authenticators;

	/**
	 * counter used for round robin calculation
	 */
	private int counter;

	/**
	 * Concurrency control
	 */
	private final Object mutex;

	/**
	 * constructor
	 * 
	 * @param size
	 *            number of authenticators
	 */
	public ResumeCakeWalkAuthenticatorPool(int size) {
		authenticators = new ArrayList<ResumeCakeWalkAuthenticator>(size);
		mutex = new Object();
		counter = 0;
	}

	/**
	 * Add a new authenticator
	 * 
	 * @param authenticator
	 */
	public void addAuthenticator(ResumeCakeWalkAuthenticator authenticator) {
		synchronized (mutex) {
			authenticators.add(authenticator);
			counter = 0;
		}
	}

	/**
	 * 
	 * @return the next authenticator to be used for send an email
	 */
	public ResumeCakeWalkAuthenticator rotate() {
		synchronized (mutex) {
			int index = counter % authenticators.size();
			counter++;
			if (counter == Integer.MAX_VALUE)
				counter = 0;

			return authenticators.get(index);
		}
	}

}
