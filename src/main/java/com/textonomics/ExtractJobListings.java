package com.textonomics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.htmlparser.Node;
import org.htmlparser.Parser;
import org.htmlparser.lexer.Lexer;
import org.htmlparser.nodes.TextNode;
import org.htmlparser.util.NodeIterator;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

/**
 *
 * Get CraigList Job Listings
 */

public class ExtractJobListings {

	private static String getHtmlResults(String url) throws Exception {
		HttpClient client = new HttpClient();
		System.out.println("URL=" + url);
		GetMethod get = new GetMethod(url);
		int retry = 2;
		while (true) {
			try {
				client.executeMethod(get);
				String pageText = get.getResponseBodyAsString();
				// System.out.println("Results:\n" + pageText);
				return get.getResponseBodyAsString();
			} catch (Exception x) {
				retry--;
				if (retry < 0)
					throw new Exception(x.getMessage());
			}
		}
	}

	public static void main(String[] args) {
		new ExtractJobListings().process("sfbay");
	}

	public void process(String city) {
		String dir = "/textnomics/docs/jobpostings";
		if (city == null || city.trim().length() == 0)
			city = "sfbay";
		String baseUrl = "http://" + city.trim() + ".craigslist.org/";
		String[][] jobTypes = { { "acc/", "accounting+finance" }, { "ofc/", "admin / office" },
				{ "egr/", "arch / engineering" }, { "med/", "art / media / design" }, { "sci/", "biotech / science" },
				{ "bus/", "business / mgmt" }, { "csr/", "customer service" }, { "edu/", "education" },
				{ "fbh/", "food / bev / hosp" }, { "lab/", "general labor" }, { "gov/", "government" },
				{ "hum/", "human resources" }, { "eng/", "internet engineers" }, { "lgl/", "legal  /  paralegal" },
				{ "mnu/", "manufacturing" }, { "mar/", "marketing / pr / ad" }, { "hea/", "medical / health" },
				{ "npo/", "nonprofit sector" }, { "rej/", "real estate" }, { "ret/", "retail / wholesale" },
				{ "sls/", "sales / biz dev" }, { "spa/", "salon / spa / fitness" }, { "sec/", "security" },
				{ "trd/", "skilled trade / craft" }, { "sof/", "software / qa / dba" }, { "sad/", "systems / network" },
				{ "tch/", "technical support" }, { "trp/", "transport" }, { "tfr/", "tv / film / video" },
				{ "web/", "web / info design" }, { "wri/", "writing / editing" }, { "etc/", "[ETC]" }, };
		int max_pages = 5;
		for (int i = 0; i < jobTypes.length; i++) {
			System.out.println("Type:" + jobTypes[i][1] + " URL=" + baseUrl + jobTypes[i][0]);
			for (int j = 0; j < max_pages; j++) {
				String page = "index" + j + "00.html";
				if (j == 0)
					page = "index.html";
				String url = baseUrl + jobTypes[i][0] + page;
				String joblinks;
				try {
					joblinks = getHtmlResults(url);
				} catch (Exception ex) {
					Logger.getLogger(ExtractJobListings.class.getName()).log(Level.SEVERE, null, ex);
					if (j > 3)
						break;
					else
						continue;
				}
				int linkind1 = joblinks.indexOf("http://" + city + ".craigslist.org/");
				int k = 0;
				while (linkind1 != -1) {
					int linkind2 = joblinks.indexOf('"', linkind1);
					String jobUrl = "";
					String jobTitle = "";
					String jobCity = "";
					if (linkind2 == -1)
						break;
					jobUrl = joblinks.substring(linkind1, linkind2);
					int jobind2 = joblinks.indexOf("<", linkind2);
					jobTitle = joblinks.substring(linkind2 + 2, jobind2);
					int placeind1 = joblinks.indexOf('(', jobind2);
					int placeind2 = joblinks.indexOf(')', placeind1);
					jobCity = joblinks.substring(placeind1 + 1, placeind2);
					linkind1 = joblinks.indexOf("http://" + city + ".craigslist.org/", placeind2);
					System.out.println("JobType:" + jobTypes[i][1] + " Title:" + jobTitle + " City:" + jobCity
							+ " jobURL:" + jobUrl);
					String jobhtml;
					try {
						jobhtml = getHtmlResults(jobUrl);
					} catch (Exception ex) {
						Logger.getLogger(ExtractJobListings.class.getName()).log(Level.SEVERE, null, ex);
						continue;
					}
					int datestart = jobhtml.indexOf("Date:");
					if (datestart == -1)
						continue;
					int dateend = jobhtml.indexOf(",", datestart);
					if (dateend == -1)
						continue;
					int emailstart = jobhtml.indexOf("mailto:");
					int emailend = jobhtml.indexOf("subject", emailstart);
					if (emailend == -1)
						emailend = jobhtml.indexOf("?", emailstart);
					if (emailend == -1)
						continue;
					String dateStr = "";
					if (datestart != -1)
						dateStr = jobhtml.substring(datestart + 5, dateend);
					if (dateStr.length() > 11)
						dateStr = dateStr.substring(0, 11);
					String email = "NO EMAIL";
					if (emailstart != -1 && emailend != -1)
						email = jobhtml.substring(emailstart + 7, emailend - 1);
					int start = jobhtml.indexOf("userbody");
					if (start == -1)
						continue;
					int end = jobhtml.indexOf("<!-- START CLTAGS -->");
					if (end == -1)
						continue;
					String jobtext = jobhtml.substring(start + 10, end).replace("<br>", "\n").replace("\n\n", "\n");
					Lexer lx = new Lexer(jobtext);
					Parser parser = new Parser(lx);
					try {
						jobtext = _extractText(parser);
					} catch (ParserException ex) {
						Logger.getLogger(ExtractJobListings.class.getName()).log(Level.SEVERE, null, ex);
					}

					String fileName = dateStr.trim() + "_" + jobTypes[i][1].replace('/', '-').replace('+', ' ') + "_"
							+ city + "_" + jobTitle.replace('/', '-') + "_" + jobCity.replace('/', '-') + ".txt";
					fileName = fileName.replaceAll("^[.\\\\/:*?\"<>|]?[\\\\/:*?\"<>|]*", "").replace('"', '-')
							.replace('*', '-').replace('|', '-').replace('?', '-');
					String jobdir = dir + "/" + city + "/" + jobTypes[i][1].replace('/', '-').replace('+', ' ');
					System.out.println("Email:" + email + " fileName:" + fileName + " jobdir:" + dir);
					File folder = new File(jobdir);
					try {
						if (folder.exists() && !folder.isDirectory())
							folder = new File(jobdir + ".dir");
						if (!folder.exists())
							folder.mkdirs();
						File file = new File(folder, fileName);
						if (file.exists())
							file.delete();
						FileWriter writer = new FileWriter(file);
						writer.write(email + "\n\n");
						writer.write(jobtext);
						writer.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}

				}
			}
		}

	}

	private static String _extractText(Parser parser) throws ParserException {
		StringBuffer rslt = new StringBuffer(1024);
		NodeIterator ni = parser.elements();

		while (ni.hasMoreNodes()) {
			Node nd = ni.nextNode();
			rslt.append(_extractTextOnly(nd));
		}

		return rslt.toString();
	}

	private static StringBuffer _extractTextOnly(Node nd) {
		StringBuffer rslt = new StringBuffer(32);

		if (nd instanceof TextNode) {
			rslt.append(nd.getText() + "\n");
		}

		NodeList nlst = nd.getChildren();

		if (nlst == null) {
			return rslt;
		}

		for (int i = 0; i < nlst.size(); i++) {
			Node kid = nlst.elementAt(i);
			rslt.append(_extractTextOnly(kid));
		}
		return rslt;
	}
}
