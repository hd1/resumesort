/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textonomics.openoffice;

import com.sun.star.beans.PropertyValue;
import com.sun.star.document.XDocumentInsertable;
import com.sun.star.lang.XComponent;
import com.sun.star.text.XText;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextDocument;
import com.sun.star.uno.UnoRuntime;
import com.textonomics.openoffice.OpenOfficeException.OPENOFFICE_EXCEPTION_TYPES;
import java.io.File;

/**
 *
 * @author Dev Gude
 */
public class OODocumentInsertHtml implements OOClient {
	/**
	 * Broker to interface with open office
	 */
	private OOBroker broker;
	File document;
	File htmlFile;
	File resultFile;

	public OODocumentInsertHtml(File document, File htmlFile, File resultFile) {
		this.document = document;
		this.htmlFile = htmlFile;
		this.resultFile = resultFile;
		this.broker = OOBroker.getInstance();
	}

	public File process(OOFILTER_TYPE type) throws OpenOfficeException {
		try {
			XComponent doc;
			OODocumentHandler handler = broker.acquireHandler(this);
			doc = handler.loadDocument(document);

			XTextDocument textDoc = (XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, doc);
			XText text = textDoc.getText();
			XTextCursor textCursor = text.createTextCursor();
			// Getting the cursor on the document
			XTextCursor textCurs = text.createTextCursorByRange(textCursor.getEnd());

			XDocumentInsertable docInsertable = (XDocumentInsertable) UnoRuntime
					.queryInterface(XDocumentInsertable.class, textCurs);

			StringBuffer sUrl = new StringBuffer("file:///");
			String name = htmlFile.getCanonicalPath().replace('\\', '/');
			if (name.startsWith("/"))
				name = name.substring(1);
			sUrl.append(name);
			System.out.println("Html to insert:" + sUrl.toString());

			docInsertable.insertDocumentFromURL(sUrl.toString(), new PropertyValue[0]);
			handler.storeDocument(resultFile, doc, type, true);
			handler.closeDocument(doc);
			broker.releaseHandler(handler);

			return resultFile;
		} catch (Exception e) {
			System.out.println("Unable to insert HTML to the document.");
			e.printStackTrace();
			throw new OpenOfficeException(OPENOFFICE_EXCEPTION_TYPES.GENERIC_EXCEPTION, e);
		}

	}

}
