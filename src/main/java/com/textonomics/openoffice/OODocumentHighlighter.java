package com.textonomics.openoffice;

import java.io.File;
import java.util.Set;
import java.util.TreeMap;

import com.sun.star.beans.PropertyVetoException;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.lang.IllegalArgumentException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.text.XText;
import com.sun.star.text.XTextCursor;
import com.textonomics.Phrase;
import com.textonomics.Replacement;
import com.textonomics.openoffice.OpenOfficeException.OPENOFFICE_EXCEPTION_TYPES;
import java.util.Map;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This class is used to modify documents, i.e., replace certain phrases with
 * others ones that have been given in the construtor
 *
 * @author Nuno Seco
 *
 */

public class OODocumentHighlighter extends OODocumentProcessor {
	/**
	 * A reference to the map holding the replacements that should be done
	 */
	private Map<Phrase, Integer> phraseImportanceMap = null;
	boolean useStars = true;
	public static long[] starColors = { 0x00FFFF00, // yellow
			0x0066CCFF, // light blue -1
			0x000099FF, // blue - 2
			0x0000FF00, // green - 3
			0x00FFFF00, // yellow - 4
			0x00FF9900, // orange-yellow - 5
			0x00FF3399, // pink
			0x00CCCCCC, // grey
			0x00FF0000, // red
	};
	public static long[] starColors2 = { 0x00FFFF00, // yellow
			0x0066CCFF, // light blue -1
			0x000099FF, // blue - 2
			0x0000FF00, // green - 3
			0x00FFFF00, // yellow - 4
			0x00FF9900, // orange-yellow - 5
			0x00FF3399, // pink
			0x00CCCCCC, // grey
			0x00FF0000, // red
	};

	/**
	 * Constructor
	 * 
	 * @param textDocument
	 *            The document to be modified
	 * @param replacements
	 *            the replacements to conduct
	 */
	public OODocumentHighlighter(File textDocument, Map<Phrase, Integer> phraseImportanceMap, boolean useStars) {
		super(textDocument, phraseImportanceMap.keySet());
		this.phraseImportanceMap = phraseImportanceMap;
		this.useStars = useStars;
		// System.out.println("UseStars:" + useStars);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.textonomics.openoffice.OODocumentProcessor#processSentence(com.sun.
	 * star.text.XText, com.sun.star.text.XTextCursor,
	 * com.sun.star.beans.XPropertySet, java.util.Set)
	 */
	@Override
	protected void processSentence(XText text, XTextCursor textCursor, XPropertySet xCursorProps, Set<Phrase> phrases)
			throws OpenOfficeException {
		int previousEndOffset = 0;
		textCursor.collapseToStart();
		Replacement replacement;

		try {
			for (Phrase target : phrases) {
				// System.out.println("Phrase:" + target.getBuffer() + "
				// Sentence:" + target.getSentence() + " Offset:" +
				// target.getStartOffset());
				Integer importance = phraseImportanceMap.get(target);
				if (importance == null)
					importance = 0;

				// select the part to modify
				textCursor.goRight((short) (target.getStartOffset() - previousEndOffset), false);
				textCursor.goRight((short) target.getBuffer().trim().length(), true);
				// text.insertString(textCursor, target.getBuffer(),
				// true);//modifiy

				// if (importance == 0)
				// xCursorProps.setPropertyValue("CharBackColor", 0x00A0FFA0);
				// else if (importance == 1)
				// xCursorProps.setPropertyValue("CharBackColor", 0x00D0EEB0);
				// else if (importance == 2)
				// xCursorProps.setPropertyValue("CharBackColor", 0x0080EFEE);
				// else if (importance == 3)
				// xCursorProps.setPropertyValue("CharBackColor", 0x00FFFF00);
				// else if (importance == 4)
				// xCursorProps.setPropertyValue("CharBackColor", 0x00EEAA00);
				// else if (importance >= 5)
				// xCursorProps.setPropertyValue("CharBackColor", 0x0056C1E9);
				if (importance <= 0)
					xCursorProps.setPropertyValue("CharBackColor", 0x00FFFF00);
				else if (importance == 1)
					xCursorProps.setPropertyValue("CharBackColor", 0x0066CCFF);
				else if (importance == 2)
					xCursorProps.setPropertyValue("CharBackColor", 0x000099FF);
				else if (importance == 3)
					xCursorProps.setPropertyValue("CharBackColor", 0x0000FF00);
				else if (importance == 4)
					xCursorProps.setPropertyValue("CharBackColor", 0x00FFFF00);
				else if (importance == 5)
					xCursorProps.setPropertyValue("CharBackColor", 0x00FF9900);
				else if (importance == 6)
					xCursorProps.setPropertyValue("CharBackColor", 0x00FF3399);
				else if (importance == 7)
					xCursorProps.setPropertyValue("CharBackColor", 0x00CCCCCC);
				else
					xCursorProps.setPropertyValue("CharBackColor", 0x00FFFF00);
				// xCursorProps.setPropertyValue("CharUnderline", 1);
				textCursor.collapseToEnd();
				previousEndOffset = target.getStartOffset() + target.getBuffer().trim().length();
			}
		} catch (UnknownPropertyException e) {
			throw new OpenOfficeException(OPENOFFICE_EXCEPTION_TYPES.GENERIC_EXCEPTION, e);
		} catch (PropertyVetoException e) {
			throw new OpenOfficeException(OPENOFFICE_EXCEPTION_TYPES.GENERIC_EXCEPTION, e);
		} catch (IllegalArgumentException e) {
			throw new OpenOfficeException(OPENOFFICE_EXCEPTION_TYPES.GENERIC_EXCEPTION, e);
		} catch (WrappedTargetException e) {
			throw new OpenOfficeException(OPENOFFICE_EXCEPTION_TYPES.GENERIC_EXCEPTION, e);
		}
	}
	/*
	 * Available Properties "CharFontName" "CharFontStyleName" "CharFontFamily"
	 * "CharFontCharSet" "CharFontPitch" "CharColor" "CharEscapement"
	 * "CharHeight" "CharUnderline" "CharWeight" "CharPosture" "CharAutoKerning"
	 * "CharBackColor" "CharBackTransparent" "CharCaseMap" "CharCrossedOut"
	 * "CharFlash" "CharStrikeout" "CharWordMode" "CharKerning" "CharLocale"
	 * "CharKeepTogether" "CharNoLineBreak" "CharShadowed" "CharFontType"
	 * "CharStyleName" "CharContoured" "CharCombineIsOn" "CharCombinePrefix"
	 * "CharCombineSuffix" "CharEmphasis" "CharRelief" "RubyText" "RubyAdjust"
	 * "RubyCharStyleName" "RubyIsAbove" "CharRotation"
	 * "CharRotationIsFitToLine" "CharScaleWidth" "HyperLinkURL"
	 * "HyperLinkTarget" "HyperLinkName" "VisitedCharStyleName"
	 * "UnvisitedCharStyleName" "CharEscapementHeight" "CharNoHyphenation"
	 * "CharUnderlineColor" "CharUnderlineHasColor" "CharStyleNames"
	 * "CharHidden" "TextUserDefinedAttributes"groverblue
	 */
}
