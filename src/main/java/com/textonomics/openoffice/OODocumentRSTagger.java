package com.textonomics.openoffice;

import java.io.File;
//import java.util.HashSet;
import java.util.Set;

import com.sun.star.beans.XPropertySet;
import com.sun.star.text.XText;
import com.sun.star.text.XTextCursor;
import com.textonomics.CAKE_WALK_TAG_TYPE;
import com.textonomics.Phrase;

/**
 * 
 * Copyright 2009 Textonomics. Inc. <br>
 * <br>
 *
 * This class adds tags to the target document identifying the phrases that will
 * given alternative suggestions. The tags are added in XML like manner. But in
 * order not to conflict with HTML document, the tags are surrounded by curly
 * braces, e.g. ("{j}")
 *
 * @author Nuno Seco
 *
 */

public class OODocumentRSTagger extends OODocumentProcessor {

	/**
	 * Constructor
	 * 
	 * @param textDocument
	 *            the file to process
	 * @param suggestions
	 *            a map containing the target phrases and suggestions from the
	 *            source
	 */
	int pind = 0;
	String startTag = CAKE_WALK_TAG_TYPE.START_CAKE_WALK_TAG.getTag();
	String endTag = CAKE_WALK_TAG_TYPE.END_CAKE_WALK_TAG.getTag();

	public OODocumentRSTagger(File textDocument, Set<Phrase> phrases) {
		super(textDocument, phrases);
	}

	public OODocumentRSTagger(File textDocument, Set<Phrase> phrases, String startTag, String endTag) {
		super(textDocument, phrases);
		this.startTag = startTag;
		this.endTag = endTag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.textonomics.openoffice.OODocumentProcessor#processSentence(com.sun.
	 * star.text.XText, com.sun.star.text.XTextCursor,
	 * com.sun.star.beans.XPropertySet, java.util.Set)
	 */
	@Override
	protected void processSentence(XText text, XTextCursor textCursor, XPropertySet xCursorProps, Set<Phrase> phrases)
			throws OpenOfficeException {
		StringBuilder newSentence = new StringBuilder();
		String originalSentence = textCursor.getString();
		int previousEndOffset = 0;

		for (Phrase target : phrases) {
			int ind1 = previousEndOffset;
			int ind2 = target.getStartOffset();
			if (ind1 >= originalSentence.length() || ind2 > originalSentence.length() || ind1 > ind2) {
				System.out.println("Sentence:" + originalSentence);
				System.out.println("Phrase" + pind + " : " + target.getBuffer().trim());
				System.out.println("PrevOff:" + ind1 + " StartOff:" + ind2);
				if (ind1 < originalSentence.length())
					newSentence.append(originalSentence.substring(ind1));
				int ind = newSentence.toString().toLowerCase().lastIndexOf(target.getBuffer().toLowerCase().trim());
				if (ind != -1) {
					String newSentenceStr = newSentence.toString();
					newSentenceStr = newSentenceStr.substring(0, ind) + startTag
							+ newSentenceStr.substring(ind, ind + target.getBuffer().trim().length()) + endTag
							+ newSentenceStr.substring(ind + target.getBuffer().trim().length());
					newSentence = new StringBuilder(newSentenceStr);
				} else {
					newSentence.append(startTag);
					newSentence.append(endTag);
				}
				previousEndOffset = originalSentence.length();
				continue;
			}
			try {
				newSentence.append(originalSentence.substring(ind1, ind2));
				newSentence.append(startTag);
				int start = target.getStartOffset();
				if (start < 0)
					start = 0;
				int end = target.getStartOffset() + target.getBuffer().trim().length();
				if (end > originalSentence.length())
					end = originalSentence.length();
				int targetLength = target.getBuffer().trim().length();
				// This is done just in case there are extra spaces inside the
				// phrase
				if (!target.getBuffer().toLowerCase().trim()
						.startsWith(originalSentence.substring(start, end).toLowerCase())) {
					String targetInfFile = originalSentence.substring(start, end);
					int end2 = end + 1;
					int targetLength2 = targetLength + 1;
					targetInfFile = targetInfFile.replaceFirst("  ", " ");
					int retry = 5;
					try {
						while (retry > 0
								&& !target.getBuffer().toLowerCase().trim().startsWith(targetInfFile.toLowerCase())) {
							targetInfFile = targetInfFile.replaceFirst("  ", " ");
							end2++;
							targetLength2++;
							retry--;
						}
						end = end2;
						targetLength = targetLength2;
					} catch (Exception x) {
					}
				}
				newSentence.append(originalSentence.substring(start, end));
				newSentence.append(endTag);
				previousEndOffset = target.getStartOffset() + targetLength;
			} catch (Exception x) {
				System.out.println("SentenceX:" + originalSentence);
				System.out.println("PhraseX:" + target.getBuffer().trim());
				System.out.println("PrevOffX:" + ind1 + " StartOff:" + ind2);
				throw new RuntimeException(x);
			}
			pind++;
		}
		int start = previousEndOffset;
		if (start < 0)
			start = 0;
		if (start < originalSentence.length()) {
			newSentence.append(originalSentence.substring(start));
		} else {
			logger.info("Original Sentence:" + originalSentence + " Start:" + start);
		}
		text.insertString(textCursor, newSentence.toString(), true);
	}

}
