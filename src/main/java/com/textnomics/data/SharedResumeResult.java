/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;

/**
 *
 * @author Dev Gude
 */
public class SharedResumeResult extends AbstractUserPersistentObject {

	long id;
	long resumeScoreId;
	int userId;
	boolean perm1; // See JobPosting
	Integer userScore;
	String comments;

	public static String DB_TABLE = "SHARED_RESUME_RESULT";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "resumeScoreId", "long", "resume_score_id", "integer", "" }, { "userId", "int", "IDUSER", "integer", "" },
			{ "perm1", "boolean", "perm1", "boolean", "" }, { "userScore", "Integer", "user_score", "int", "" },
			{ "comments", "String", "comments", "longtext", "" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getResumeScoreId() {
		return resumeScoreId;
	}

	public void setResumeScoreId(long resumeScoreId) {
		this.resumeScoreId = resumeScoreId;
	}

	public boolean isPerm1() {
		return perm1;
	}

	public void setPerm1(boolean perm1) {
		this.perm1 = perm1;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Integer getUserScore() {
		return userScore;
	}

	public void setUserScore(Integer userScore) {
		this.userScore = userScore;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

}
