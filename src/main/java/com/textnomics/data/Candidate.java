/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;
import java.util.Date;

/**
 *
 * @author Dev Gude
 */
public class Candidate extends AbstractUserPersistentObject {

	long id;
	String name;
	String email;
	String industry;
	String address1;
	String address2;
	String city;
	String state;
	String zip;
	String phone;
	Date creationTime;
	Date modificationTime;

	public static String DB_TABLE = "CANDIDATE";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "name", "String", "name", "varchar", "255" }, { "email", "String", "email", "varchar", "255" },
			{ "industry", "String", "industry", "varchar", "255" },
			{ "address1", "String", "address1", "varchar", "255" },
			{ "address2", "String", "address2", "varchar", "255" }, { "city", "String", "city", "varchar", "64" },
			{ "state", "String", "state", "varchar", "64" }, { "zip", "String", "zip", "varchar", "10" },
			{ "phone", "String", "phone", "varchar", "16" },
			{ "creationTime", "Date", "creation_time", "timestamp", "" },
			{ "modificationTime", "Date", "modification_time", "timestamp", "" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

}
