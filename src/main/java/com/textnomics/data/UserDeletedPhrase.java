/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.UserDBAccess;
import com.textonomics.user.User;

/**
 *
 * @author Dev Gude
 */
public class UserDeletedPhrase extends DeletedPhrase {

	public static String DB_TABLE = "USER_DELETED_PHRASE";

	@Override
	public String getDB_TABLE() {
		return DB_TABLE;
	}

	public static boolean isUserDeleted(User user, String phrase) throws Exception {
		int count = new UserDeletedPhrase().getCount("phrase =" + UserDBAccess.toSQL(phrase.trim().toLowerCase())
				+ " and user_name=" + UserDBAccess.toSQL(user.getLogin()));
		if (count > 0)
			return true;
		else
			return isDeleted(phrase);
	}

}
