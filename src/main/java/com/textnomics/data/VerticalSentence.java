/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Dev Gude
 */
public class VerticalSentence extends AbstractUserPersistentObject {

	long id;
	long verticalId;
	long phraseId;
	int count;
	String sentence;

	public static String DB_TABLE = "VERTICAL_SENTENCE";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "verticalId", "long", "vertical_id", "integer", "" }, { "phraseId", "long", "phrase_id", "integer", "" },
			{ "count", "int", "count", "integer", "" }, { "sentence", "String", "sentence", "varchar", "255" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	public long getVerticalId() {
		return verticalId;
	}

	public void setVerticalId(long verticalId) {
		this.verticalId = verticalId;
	}

	public long getPhraseId() {
		return phraseId;
	}

	public void setPhraseId(long phraseId) {
		this.phraseId = phraseId;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getSentence() {
		return sentence;
	}

	public void setSentence(String sentence) {
		this.sentence = sentence;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

	public Map<String, List<String>> getVerticalPhraseSentences(long verticalId) throws Exception {
		List<VerticalPhrase> phrases = new VerticalPhrase().getObjects("vertical_id =" + verticalId);
		List<VerticalSentence> sentences = new VerticalSentence().getObjects("vertical_id =" + verticalId);
		Map<String, List<String>> sentMap = new HashMap<String, List<String>>();
		for (VerticalPhrase phrase : phrases) {
			List<String> psents = sentMap.get(phrase.getPhrase());
			if (psents == null) {
				psents = new ArrayList<String>();
				sentMap.put(phrase.getPhrase(), psents);
			}
			for (VerticalSentence sentence : sentences) {
				if (sentence.getPhraseId() == phrase.getId()) {
					psents.add(sentence.getSentence());
				}
			}
		}
		return sentMap;
	}
}
