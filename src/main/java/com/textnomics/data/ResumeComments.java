/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;

/**
 *
 * @author Dev Gude
 */
public class ResumeComments extends AbstractUserPersistentObject {

	long id;
	String resumeFolder;
	String resumeFile;
	String comments;
	String userName;

	public static String DB_TABLE = "RESUME_COMMENTS";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "resumeFolder", "String", "resume_folder", "varchar", "255" },
			{ "resumeFile", "String", "resume_file", "varchar", "255" },
			{ "comments", "String", "comments", "clob", "" }, { "userName", "String", "user_name", "varchar", "64" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getComments() {
		if (comments == null)
			comments = "";
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getResumeFile() {
		return resumeFile;
	}

	public void setResumeFile(String resumeFile) {
		this.resumeFile = resumeFile;
	}

	public String getResumeFolder() {
		return resumeFolder;
	}

	public void setResumeFolder(String resumeFolder) {
		this.resumeFolder = resumeFolder;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

}
