/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;

/**
 *
 * @author Dev Gude
 */
public class SharedJobPosting extends AbstractUserPersistentObject {

	long id;
	long jobId;
	int userId;
	boolean perm1; // See JobPosting
	boolean perm2; // See Resumes
	boolean perm3; // See all details (including other peoples scores)

	public static String DB_TABLE = "SHARED_JOB_POSTING";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "jobId", "long", "JOB_ID", "integer", "" }, { "userId", "int", "IDUSER", "integer", "" },
			{ "perm1", "boolean", "perm1", "boolean", "" }, { "perm2", "boolean", "perm2", "boolean", "" },
			{ "perm3", "boolean", "perm3", "boolean", "" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getJobId() {
		return jobId;
	}

	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	public boolean isPerm1() {
		return perm1;
	}

	public void setPerm1(boolean perm1) {
		this.perm1 = perm1;
	}

	public boolean isPerm2() {
		return perm2;
	}

	public void setPerm2(boolean perm2) {
		this.perm2 = perm2;
	}

	public boolean isPerm3() {
		return perm3;
	}

	public void setPerm3(boolean perm3) {
		this.perm3 = perm3;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

}
