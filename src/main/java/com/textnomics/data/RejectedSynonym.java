/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;
import com.textonomics.db.UserDBAccess;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Dev Gude
 */
public class RejectedSynonym extends AbstractUserPersistentObject {

	long id;
	String phrase;
	long code;
	String synset;
	String synonym;
	String userName;
	boolean notExact;
	boolean hyponym;

	public static String DB_TABLE = "REJECTED_SYNONYM";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "phrase", "String", "phrase", "varchar", "255" }, { "synonym", "String", "synonym", "varchar", "255" },
			{ "code", "long", "code", "bigint", "" }, { "synset", "String", "synset", "varchar", "32" },
			{ "userName", "String", "user_name", "varchar", "64" }, { "hyponym", "boolean", "hyponym", "boolean", "" },
			{ "notExact", "boolean", "not_exact", "boolean", "" }, };
	public static Map<String, Map<String, String>> rejectedSynonyms = new LinkedHashMap<String, Map<String, String>>();

	public static int MAX_USER_ENTRIES = 5;

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPhrase() {
		return phrase;
	}

	public void setPhrase(String phrase) {
		this.phrase = phrase;
	}

	public String getSynonym() {
		return synonym;
	}

	public void setSynonym(String synonym) {
		this.synonym = synonym;
	}

	public String getSynset() {
		return synset;
	}

	public void setSynset(String synset) {
		this.synset = synset;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public boolean isNotExact() {
		return notExact;
	}

	public void setNotExact(boolean notExact) {
		this.notExact = notExact;
	}

	public boolean isHyponym() {
		return hyponym;
	}

	public void setHyponym(boolean hyponym) {
		this.hyponym = hyponym;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

	public static Map<String, String> getSynonymCache(String userName) throws Exception {
		Map<String, String> synonymCache = rejectedSynonyms.get(userName);
		if (synonymCache == null) {
			if (rejectedSynonyms.keySet().size() >= MAX_USER_ENTRIES) {
				rejectedSynonyms.remove(synonymCache.keySet().iterator().next());
			}
			int offset = 0;
			synonymCache = new HashMap<String, String>();
			List<RejectedSynonym> synonyms = new RejectedSynonym()
					.getObjects("user_name =" + UserDBAccess.toSQL(userName));
			for (RejectedSynonym synonym : synonyms) {
				String prevSyn = synonymCache.get(synonym.getPhrase().toLowerCase());
				if (prevSyn == null)
					prevSyn = "";
				if (synonym.getSynonym() == null)
					continue;
				synonymCache.put(synonym.getPhrase().toLowerCase(), prevSyn + "|" + synonym.getSynonym().toLowerCase());
			}
			rejectedSynonyms.put(userName, synonymCache);
		}
		return synonymCache;
	}

	public static void clearCache() {
		rejectedSynonyms = new HashMap<String, Map<String, String>>();
	}

	public static boolean isRejected(String userName, String phrase, String synonym) throws Exception {
		Map<String, String> rejections = getSynonymCache(userName);
		if (rejections == null || !rejections.containsKey(phrase.toLowerCase()))
			return false;
		String rejList = rejections.get(phrase.toLowerCase());
		// System.out.println("synonym:" + synonym + " rejected list:" +
		// rejList);
		String[] rejs = rejList.split("\\|");
		for (String rejection : rejs)
			if (synonym.equalsIgnoreCase(rejection))
				return true;
		return false;
	}

	@Override
	public void insert() throws Exception {
		super.insert();
		String prevSyn = getSynonymCache(userName).get(getPhrase().toLowerCase());
		if (prevSyn == null)
			prevSyn = "";
		getSynonymCache(userName).put(getPhrase().toLowerCase(), prevSyn + "|" + getSynonym().toLowerCase());
	}

	public void removeFromCache() throws Exception {
		String rejList = getSynonymCache(userName).get(getPhrase().toLowerCase());
		if (rejList != null) {
			String[] rejs = rejList.split("\\|");
			String newList = "";
			for (String rejection : rejs) {
				if (!rejection.equalsIgnoreCase(getSynonym()))
					newList = newList + "|" + rejection;
			}
			getSynonymCache(userName).put(getPhrase().toLowerCase(), newList);
		}
	}
}
