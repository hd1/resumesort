/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;
import java.util.Date;

/**
 *
 * @author Dev Gude
 */
public class ResumeScore extends AbstractUserPersistentObject {

	long id;
	long jobId;
	String resumeFile;
	String resumeFolder;
	String resumeEmail;
	double score;
	Integer userScore;
	Double distance;
	String userName;
	String emailSubject; // if resume was received by email
	int rank;
	int total;
	String tagsFound;
	Date creationTime;
	Date modificationTime;
	boolean simpleRating;
	boolean byInvitation; // true, if not by owner of posting
	boolean paid;

	public static String DB_TABLE = "RESUME_SCORE";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "jobId", "long", "JOB_ID", "integer", "" }, { "resumeFile", "String", "RESUME_FILE", "varchar", "255" },
			{ "resumeFolder", "String", "RESUME_FOLDER", "varchar", "255" },
			{ "resumeEmail", "String", "RESUME_EMAIL", "varchar", "128" }, { "score", "double", "SCORE", "float", "" },
			{ "distance", "Double", "DISTANCE", "float", "" }, { "userScore", "Integer", "USER_SCORE", "int", "" },
			{ "rank", "int", "RANK", "integer", "" }, { "total", "int", "TOTAL", "integer", "" },
			{ "userName", "String", "user_name", "varchar", "128" },
			{ "emailSubject", "String", "email_subject", "varchar", "255" },
			{ "tagsFound", "String", "tags_found", "varchar", "255" },
			{ "creationTime", "Date", "creation_time", "timestamp", "" },
			{ "modificationTime", "Date", "modification_time", "timestamp", "" },
			{ "simpleRating", "boolean", "simple_rating", "boolean", "" },
			{ "byInvitation", "boolean", "by_invitation", "boolean", "" },
			{ "paid", "boolean", "paid", "boolean", "" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getJobId() {
		return jobId;
	}

	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	public String getResumeFile() {
		return resumeFile;
	}

	public void setResumeFile(String resumeFile) {
		this.resumeFile = resumeFile;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public boolean isSimpleRating() {
		return simpleRating;
	}

	public void setSimpleRating(boolean simpleRating) {
		this.simpleRating = simpleRating;
	}

	public boolean isByInvitation() {
		return byInvitation;
	}

	public void setByInvitation(boolean byInvitation) {
		this.byInvitation = byInvitation;
	}

	public String getResumeFolder() {
		return resumeFolder;
	}

	public void setResumeFolder(String resumeFolder) {
		this.resumeFolder = resumeFolder;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getTagsFound() {
		return tagsFound;
	}

	public void setTagsFound(String tagsFound) {
		this.tagsFound = tagsFound;
	}

	public Integer getUserScore() {
		return userScore;
	}

	public void setUserScore(Integer userScore) {
		this.userScore = userScore;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getResumeEmail() {
		return resumeEmail;
	}

	public void setResumeEmail(String resumeEmail) {
		this.resumeEmail = resumeEmail;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

}
