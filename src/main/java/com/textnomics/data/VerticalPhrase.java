/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;
import java.util.Date;

/**
 *
 * @author Dev Gude
 */
public class VerticalPhrase extends AbstractUserPersistentObject {

	long id;
	long verticalId;
	String phrase;
	int occurrence;

	public static String DB_TABLE = "VERTICAL_PHRASE";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "verticalId", "long", "vertical_id", "integer", "" }, { "phrase", "String", "phrase", "varchar", "255" },
			{ "occurrence", "int", "occurrence", "in", "" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPhrase() {
		return phrase;
	}

	public void setPhrase(String phrase) {
		this.phrase = phrase;
	}

	public long getVerticalId() {
		return verticalId;
	}

	public void setVerticalId(long verticalId) {
		this.verticalId = verticalId;
	}

	public int getOccurrence() {
		return occurrence;
	}

	public void setOccurrence(int occurrence) {
		this.occurrence = occurrence;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

}
