/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;
import java.util.Date;

/**
 *
 * @author Dev Gude
 */
public class SubscriptionPlan extends AbstractUserPersistentObject {

	long id;
	String name;
	String description;
	int days; // = 0, forever
	int numresumes;
	int numpostings;
	int order; // for display
	double price;
	boolean recurring;
	Date validFrom;
	Date validTo;

	public static String DB_TABLE = "SUBSCRIPTION_PLAN";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "name", "String", "name", "varchar", "64" }, { "description", "String", "description", "varchar", "128" },
			{ "days", "int", "days", "int", "10" }, { "order", "int", "display_order", "int", "10" },
			{ "price", "double", "price", "double", "" }, { "numresumes", "int", "num_resumes", "int", "10" },
			{ "numpostings", "int", "num_postings", "int", "10" }, { "recurring", "boolean", "recurring", "bit", "1" },
			{ "validFrom", "Date", "valid_from", "date", "" }, { "validTo", "Date", "valid_to", "date", "" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumresumes() {
		return numresumes;
	}

	public void setNumresumes(int numresumes) {
		this.numresumes = numresumes;
	}

	public int getNumpostings() {
		return numpostings;
	}

	public void setNumpostings(int numpostings) {
		this.numpostings = numpostings;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isRecurring() {
		return recurring;
	}

	public void setRecurring(boolean recurring) {
		this.recurring = recurring;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}
}
