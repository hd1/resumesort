/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractPersistentObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dev Gude
 */
public class TagList extends AbstractPersistentObject {

	long id;
	String tagName;
	String type;
	long code;
	String userName;
	String abbrev;

	public static String DB_TABLE = "TAG_LIST";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "tagName", "String", "tag_name", "varchar", "255" }, { "code", "long", "code", "bigint", "" },
			{ "type", "String", "type", "varchar", "64" }, { "abbrev", "String", "abbrev", "varchar", "16" },
			{ "userName", "String", "user_name", "varchar", "64" }, };

	public static Map<String, String> tagCache = null;
	public static final String TYPE_UNIVERSITY = "University";
	public static final String TYPE_COMPANY = "Company";

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAbbrev() {
		return abbrev;
	}

	public void setAbbrev(String abbrev) {
		this.abbrev = abbrev;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

	public static Map<String, String> getTagCache() throws Exception {
		if (tagCache == null) {
			int offset = 0;
			tagCache = new HashMap<String, String>();
			while (true) {
				List<TagList> tags = new TagList().getObjects("", offset, 1000);
				for (TagList tag : tags) {
					tagCache.put(tag.getTagName().toLowerCase(), tag.getType());
					if (tag.getTagName().indexOf('–') != -1) {
						tagCache.put(tag.getTagName().replace("–", ", ").toLowerCase(), tag.getType());
						String firstPart = tag.getTagName().substring(0, tag.getTagName().indexOf('–'));
						if (firstPart.indexOf(" ") != -1) // more than one word
							tagCache.put(firstPart.toLowerCase(), tag.getType());
					}
					if (tag.getTagName().indexOf('-') != -1) {
						tagCache.put(tag.getTagName().replace("-", ", ").toLowerCase(), tag.getType());
						String firstPart = tag.getTagName().substring(0, tag.getTagName().indexOf("-"));
						if (firstPart.indexOf(" ") != -1) // more than one word
							tagCache.put(firstPart.toLowerCase(), tag.getType());
					}
					if (tag.getTagName().indexOf(", ") != -1) {
						tagCache.put(tag.getTagName().substring(0, tag.getTagName().indexOf(", ")).trim().toLowerCase(),
								tag.getType());
					}
					if (tag.getTagName().indexOf("A&M") != -1) {
						tagCache.put(tag.getTagName().replace("A&M", "A & M").trim().toLowerCase(), tag.getType());
					}
					if (tag.getAbbrev() != null && tag.getAbbrev().length() > 0)
						tagCache.put(tag.getAbbrev(), tag.getType());
				}
				offset = offset + 1000;
				if (tags.size() < 1000)
					break;
			}
		}
		return tagCache;
	}

	public static void clearTagCache() {
		TagList.tagCache = null;
	}

	public static boolean isCompany(String testName) {
		String type = null;
		try {
			type = getTagCache().get(testName.toLowerCase());
		} catch (Exception ex) {
			Logger.getLogger(TagList.class.getName()).log(Level.SEVERE, null, ex);
		}
		if (type != null && type.equals(TYPE_COMPANY))
			return true;
		else
			return false;
	}

	public static boolean isUniversity(String testName) {
		String type = null;
		try {
			type = getTagCache().get(testName.toLowerCase());
			if (type == null)
				type = getTagCache().get(testName);
		} catch (Exception ex) {
			Logger.getLogger(TagList.class.getName()).log(Level.SEVERE, null, ex);
		}
		if (type != null && type.equals(TYPE_UNIVERSITY))
			return true;
		else
			return false;
	}
}
