/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;
import java.util.Date;

/**
 *
 * @author Dev Gude
 */
public class SurveyResult extends AbstractUserPersistentObject {

	long id;
	String question;
	int rating;
	Date creationDate;
	String userName;

	public static String DB_TABLE = "SURVEY_RESULT";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "question", "String", "question", "varchar", "255" },
			{ "userName", "String", "user_name", "varchar", "64" }, { "rating", "int", "rating", "int", "10" },
			{ "creationDate", "Date", "creation_date", "timestamp", "" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}
}
