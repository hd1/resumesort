/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;

/**
 *
 * @author Dev Gude
 */
public class AddedSynonym extends AbstractUserPersistentObject {

	long id;
	String phrase;
	String synonym;
	long code;
	String synset;
	String userName;
	boolean notExact; // hypernym??
	boolean hyponym;
	boolean comment;

	public static String DB_TABLE = "ADDED_SYNONYM";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "phrase", "String", "phrase", "varchar", "255" }, { "synonym", "String", "synonym", "varchar", "255" },
			{ "code", "long", "code", "bigint", "" }, { "synset", "String", "synset", "varchar", "32" },
			{ "userName", "String", "user_name", "varchar", "64" },
			{ "notExact", "boolean", "not_exact", "boolean", "" }, { "hyponym", "boolean", "hyponym", "boolean", "" },
			{ "comment", "boolean", "comment", "boolean", "" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPhrase() {
		return phrase;
	}

	public void setPhrase(String phrase) {
		this.phrase = phrase;
	}

	public String getSynonym() {
		return synonym;
	}

	public void setSynonym(String synonym) {
		this.synonym = synonym;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getSynset() {
		return synset;
	}

	public void setSynset(String synset) {
		this.synset = synset;
	}

	public boolean isNotExact() {
		return notExact;
	}

	public void setNotExact(boolean notExact) {
		this.notExact = notExact;
	}

	public boolean isHyponym() {
		return hyponym;
	}

	public void setHyponym(boolean hyponym) {
		this.hyponym = hyponym;
	}

	public boolean isComment() {
		return comment;
	}

	public void setComment(boolean comment) {
		this.comment = comment;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

}
