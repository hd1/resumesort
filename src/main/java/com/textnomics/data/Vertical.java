/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;
import java.util.Date;

/**
 *
 * @author Dev Gude
 */
public class Vertical extends AbstractUserPersistentObject {

	long id;
	String industry;

	public static String DB_TABLE = "VERTICAL";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "industry", "String", "industry", "varchar", "255" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getIndustry() {
		return industry;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

}
