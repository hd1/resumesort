/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;
import java.io.Serializable;

/**
 *
 * @author Dev Gude
 */
public class UserTemplatePhrase extends AbstractUserPersistentObject implements Serializable {

	long id;
	String phrase;
	boolean required;
	boolean tag;
	int score; // negative when 3 levels, positive 1-6 for 5 stars, 0
	long templateId;

	public static String DB_TABLE = "USER_TEMPLATE_PHRASE";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "templateId", "long", "template_id", "integer", "" }, { "phrase", "String", "phrase", "varchar", "255" },
			{ "required", "boolean", "required", "bit", "1" }, { "tag", "boolean", "tag", "bit", "1" },
			{ "score", "int", "score", "int", "" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPhrase() {
		return phrase;
	}

	public void setPhrase(String phrase) {
		this.phrase = phrase;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public boolean isTag() {
		return tag;
	}

	public void setTag(boolean tag) {
		this.tag = tag;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

}
