/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;
import com.textonomics.db.UserDBAccess;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Dev Gude
 */
public class DeletedPhrase extends AbstractUserPersistentObject {

	long id;
	String phrase;
	String userName;

	public static String DB_TABLE = "DELETED_PHRASE";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "phrase", "String", "phrase", "varchar", "255" },
			{ "userName", "String", "user_name", "varchar", "128" }, };
	public static Set<String> phraseCache = null;
	public static int totalPhrases = 0;

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPhrase() {
		return phrase;
	}

	public void setPhrase(String phrase) {
		this.phrase = phrase;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

	public static Set<String> getPhraseCache() throws Exception {
		if (phraseCache == null) {
			int offset = 0;
			phraseCache = new HashSet<String>();
			while (true) {
				List<DeletedPhrase> phrases = new DeletedPhrase().getObjects("", offset, 1000);
				for (DeletedPhrase phrase : phrases) {
					phraseCache.add(phrase.getPhrase().toLowerCase());
				}
				offset = offset + 1000;
				if (phrases.size() < 1000)
					break;
			}
		}
		return phraseCache;
	}

	public static void clearTagCache() {
		phraseCache = null;
	}

	public static boolean isDeleted(String phrase) throws Exception {
		if (totalPhrases == 0)
			totalPhrases = new DeletedPhrase().getCount("");
		if (totalPhrases < 5000) {
			Set<String> phrases = getPhraseCache();
			if (phrases.contains(phrase.trim().toLowerCase()))
				return true;
			else
				return false;
		}
		int count = new DeletedPhrase().getCount("phrase =" + UserDBAccess.toSQL(phrase.trim().toLowerCase()));
		if (count > 0)
			return true;
		else
			return false;
	}

	@Override
	public void insert() throws Exception {
		super.insert();
		getPhraseCache().add(phrase.trim().toLowerCase());
	}

}
