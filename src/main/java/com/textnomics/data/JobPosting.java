/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Dev Gude
 */
public class JobPosting extends AbstractUserPersistentObject implements Serializable {

	long id;
	String postingId;
	String postingFile;
	String resumeFolder;
	String jobTitle;
	Date postingDate;
	String originalFile;
	String industry;
	String userName;
	String emailAddress;
	String templateName;
	String location;
	boolean paid;
	boolean paymentRequest;
	int numRuns;
	int numInvited;

	public static final int MAX_POSTING_RUNS = 5;
	public static final int MAX_INVITEES = 2;

	public static String DB_TABLE = "JOB_POSTING";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "postingId", "String", "POSTING_ID", "varchar", "255" },
			{ "postingFile", "String", "POSTING_FILE", "varchar", "255" },
			{ "originalFile", "String", "ORIGINAL_FILE", "varchar", "255" },
			{ "resumeFolder", "String", "RESUME_FOLDER", "varchar", "255" },
			{ "userName", "String", "user_name", "varchar", "128" },
			{ "jobTitle", "String", "job_title", "varchar", "128" },
			{ "postingDate", "Date", "posting_date", "date", "" },
			{ "industry", "String", "industry", "varchar", "300" },
			{ "emailAddress", "String", "email_address", "varchar", "300" },
			{ "templateName", "String", "template_name", "varchar", "128" },
			{ "location", "String", "location", "varchar", "64" }, { "paid", "boolean", "paid", "boolean", "" },
			{ "paymentRequest", "boolean", "payment_request", "boolean", "" },
			{ "numRuns", "int", "num_runs", "int", "" }, { "numInvited", "int", "num_invited", "int", "" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPostingFile() {
		return postingFile;
	}

	public void setPostingFile(String postingFile) {
		this.postingFile = postingFile;
	}

	public String getPostingId() {
		return postingId;
	}

	public void setPostingId(String postingId) {
		this.postingId = postingId;
	}

	public String getOriginalFile() {
		return originalFile;
	}

	public void setOriginalFile(String originalFile) {
		this.originalFile = originalFile;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}

	public String getResumeFolder() {
		return resumeFolder;
	}

	public void setResumeFolder(String resumeFolder) {
		this.resumeFolder = resumeFolder;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	public boolean isPaymentRequest() {
		return paymentRequest;
	}

	public void setPaymentRequest(boolean paymentRequest) {
		this.paymentRequest = paymentRequest;
	}

	public int getNumRuns() {
		return numRuns;
	}

	public void setNumRuns(int numRuns) {
		this.numRuns = numRuns;
	}

	public int getNumInvited() {
		return numInvited;
	}

	public void setNumInvited(int numInvited) {
		this.numInvited = numInvited;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

}
