/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.textnomics.data;

import com.textonomics.db.AbstractUserPersistentObject;

/**
 *
 * @author Dev Gude
 */
public class SortPhrase extends AbstractUserPersistentObject {

	long id;
	long jobId;
	String originalPhrase;
	String lemmatizedPhrase;
	Double stars;
	boolean selected;
	boolean required;
	boolean userentered;
	boolean templatePhrase;
	boolean resumePhrase;
	boolean tag;
	String userName;
	String industry;
	Long savedTime; // timestamp from new Date().getTime();
	int score; // same as stars - negative when 3 levels, positive 1-6 for 5
				// stars, 0

	public static String DB_TABLE = "SORT_PHRASE";

	// Class fields to database column mapping
	// fieldName,fieldType,columnName,columnType,columnSize
	public static String[][] DB_COLUMNS = { { "id", "long", "id", "Id", "20" },
			{ "jobId", "long", "JOB_ID", "integer", "" },
			{ "originalPhrase", "String", "original_phrase", "varchar", "255" },
			{ "lemmatizedPhrase", "String", "lemmatized_phrase", "varchar", "255" },
			{ "stars", "Double", "stars", "double", "" }, { "selected", "boolean", "selected", "bit", "1" },
			{ "required", "boolean", "required", "bit", "1" }, { "userentered", "boolean", "user_entered", "bit", "1" },
			{ "resumePhrase", "boolean", "resume_phrase", "bit", "1" },
			{ "templatePhrase", "boolean", "template_phrase", "bit", "1" }, { "tag", "boolean", "tag", "bit", "1" },
			{ "userName", "String", "user_name", "varchar", "64" },
			{ "industry", "String", "industry", "varchar", "300" }, { "savedTime", "Long", "saved_time", "bigint", "" },
			{ "score", "int", "score", "int", "" }, };

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getJobId() {
		return jobId;
	}

	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	public String getLemmatizedPhrase() {
		return lemmatizedPhrase;
	}

	public void setLemmatizedPhrase(String lemmatizedPhrase) {
		this.lemmatizedPhrase = lemmatizedPhrase;
	}

	public String getOriginalPhrase() {
		return originalPhrase;
	}

	public void setOriginalPhrase(String originalPhrase) {
		this.originalPhrase = originalPhrase;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Double getStars() {
		return stars;
	}

	public void setStars(Double stars) {
		this.stars = stars;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isRequired() {
		return required;
	}

	public boolean isUserentered() {
		return userentered;
	}

	public void setUserentered(boolean userentered) {
		this.userentered = userentered;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public boolean isTag() {
		return tag;
	}

	public void setTag(boolean tag) {
		this.tag = tag;
	}

	public boolean isResumePhrase() {
		return resumePhrase;
	}

	public void setResumePhrase(boolean resumePhrase) {
		this.resumePhrase = resumePhrase;
	}

	public boolean isTemplatePhrase() {
		return templatePhrase;
	}

	public void setTemplatePhrase(boolean templatePhrase) {
		this.templatePhrase = templatePhrase;
	}

	public Long getSavedTime() {
		return savedTime;
	}

	public void setSavedTime(Long savedTime) {
		this.savedTime = savedTime;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	@Override
	public String getDB_TABLE() {

		return DB_TABLE;
	}

	@Override
	public String[][] getDB_COLUMNS() {
		return DB_COLUMNS;
	}

}
